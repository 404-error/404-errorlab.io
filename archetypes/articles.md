---
title: "{{ substr (replace .Name "-" " ") 7 | humanize }}"
date: "{{ now.Format "2006-01-02" }}"
images:
  - "img/{{ .Name }}.jpg"
gridimages:
  - "img/{{ .Name }}.jpg"
sourceimage: "https"
persona:
  - auteur
echo:
  - écho
notes: "possible note sur l'article"
summary: "résumé"
citation: "une citation de l'article"
---

