---
title: "contact.s."
slug: "contact"
weight: 5
type: menu
layout: single
---

# palabre

*Error* est une revue qui se prend pour un arbre à palabres.

*Error* peut être spammée à l'adresse suivante, notamment pour lui proposer des textes&nbsp;:

<p class="nohyphens">ecrire&nbsp;[arobase] error&nbsp;[point]&nbsp;re</p>

*Error* a même un ISSN : 2673-6993, pour mettre un peu d'*Error* dans les bases de données.

# orbite

*Error* est mise sur orbite par l'abrüpte équipe d'[Abrüpt](https://abrupt.cc), fabrique d'antilivres, sise à Zürich, et pourtant égarée quelque part dans le cosmos.

*Error* parasite les comptes d'Abrüpt sur les réseaux dits sociaux : [mstdn](https://mamot.fr/@cestabrupt) (notre préférence), [twttr](https://twitter.com/cestabrupt), [fcbk](https://facebook.com/cestabrupt) et [nstgrm](https://instagram.com/cestabrupt). *Error* squatte aussi sa [lettre d'information](https://abrupt.cc/lettre/).

# technicité

*Error* s'appuie sur la puissance de [l'outil libre Git](https://gitlab.com/404-error/404-error.gitlab.io) pour diffuser ses textes.

# liberté

*Error* place ses textes sous une [lecture libre](https://abrupt.cc/partage) de la licence Creative Commons Attribution — Pas d’Utilisation Commerciale — Partage dans les Mêmes Conditions 4.0 International (CC&nbsp;BY-NC-SA&nbsp;4.0), sauf indication contraire, notamment lorsqu'un texte est dédié au domaine public volontaire grâce à la licence [Creative Commons Zero](https://creativecommons.org/publicdomain/zero/1.0/). (*L'information veut être libre.*)
