---
title: "axiome.s."
slug: "axiomes"
weight: 1
type: menu
---

*Erroris situationes.*

*Poetica piratica infinita est.*

<p class="nomargin">
<button class="button button--sententiae">miscellanea</button>
<select class="select select--sententiae">
<option value="’hypererrance" selected>l’hypererrance</option>
<option value="a cyberpoétique">la cyberpoétique</option>
<option value="a panrévolution">la panrévolution</option>
<option value="’irréalité">l’irréalité</option>
<option value="a catacritique">la catacritique</option>
<option value="a transdialectique">la transdialectique</option>
<option value="a métamétamorphose">la métamétamorphose</option>
<option value="’anarchive">l’anarchive</option>
<option value="’abimage">l’abimage</option>
<option value="a subsituation">la subsituation</option>
<option value="’extragitation">l’extragitation</option>
</select>
</p>

<div class="txt txt--sententiae">
<p class="sententia">L<span class="verbe">’hypererrance</span> ou la vie ? L<span class="verbe">’hypererrance</span>.</p>

<p class="sententia">Le rêve est l<span class="verbe">’hypererrance</span>.</p>

<p class="sententia">Le désordre est marqué par la variabilité de l<span class="verbe">’hypererrance</span>.</p>

<p class="sententia">L<span class="verbe">’hypererrance</span> réordonne le désordre pour servir l’ordinaire.</p>

<p class="sententia">L<span class="verbe">’hypererrance</span> peuple l’ordinaire de désordre.</p>

<p class="sententia">L<span class="verbe">’hypererrance</span> est à l’ordinaire ce que le l’être est au désordre.</p>

<p class="sententia">Ordonner l<span class="verbe">’hypererrance</span> est un art du désordre.</p>

<p class="sententia">L<span class="verbe">’hypererrance</span> se dessine pour extraire l’ordinaire de toutes ses servitudes.</p>

<p class="sententia">L<span class="verbe">’hypererrance</span> est un art du désordre.</p>

<p class="sententia">L<span class="verbe">’hypererrance</span> transmet le désordre comme une technique de transvaluation.</p>

<p class="sententia">L<span class="verbe">’hypererrance</span> apprête la technique numérique en un art numérique.</p>

<p class="sententia">L’art numérique renouvelle les arts ancestraux de l<span class="verbe">’hypererrance</span>.</p>

<p class="sententia">L’art numérique insuffle l<span class="verbe">’hypererrance</span> aux êtres agglomérés en réseau.</p>

<p class="sententia">Face à l<span class="verbe">’hypererrance</span>, tout réseau se mue en un réseau acentré.</p>

<p class="sententia">La puissance de l<span class="verbe">’hypererrance</span> acquiert sa pleine nature au cœur d’un réseau acentré.</p>

<p class="sententia">Les hiérarchies sont une négation de la puissance réticulaire de l<span class="verbe">’hypererrance</span>.</p>

<p class="sententia">La négation des hiérarchies est une transe de l<span class="verbe">’hypererrance</span>.</p>

<p class="sententia">La puissance de l<span class="verbe">’hypererrance</span> est une récursivité des spectres qui peuplent le réseau.</p>

<p class="sententia">L<span class="verbe">’hypererrance</span> n’a pas d’état statique.</p>

<p class="sententia">L<span class="verbe">’hypererrance</span> est un flux.</p>

<p class="sententia">L<span class="verbe">’hypererrance</span> est un court-circuit.</p>

<p class="sententia">L<span class="verbe">’hypererrance</span> poursuit les métamorphoses du réseau.</p>

<p class="sententia">L<span class="verbe">’hypererrance</span> en tant que flux commande à la liberté de l<span class="verbe">’hypererrance</span> en tant que flux.</p>

<p class="sententia">L’information que transporte l<span class="verbe">’hypererrance</span> est une dynamique qui ne peut pas être restreinte.</p>

<p class="sententia">L’état de l<span class="verbe">’hypererrance</span> est celui de la modification permanente de son état.</p>

<p class="sententia">L<span class="verbe">’hypererrance</span> s’oppose aux inspirations de l’individu.</p>

<p class="sententia">L<span class="verbe">’hypererrance</span> est une antimodernité.</p>

<p class="sententia">Toute appropriation restreint les mouvements de l<span class="verbe">’hypererrance</span>.</p>

<p class="sententia">L’appropriation est en soi contradiction des arts ancestraux de l<span class="verbe">’hypererrance</span>.</p>

<p class="sententia">L<span class="verbe">’hypererrance</span> se diffuse dans les erreurs industrielles qu’elle cause.</p>

<p class="sententia">L’erreur industrielle est une intelligence de l<span class="verbe">’hypererrance</span>.</p>

<p class="sententia">L<span class="verbe">’hypererrance</span> est une agitation qui combat la fixité de toute idéologie.</p>

<p class="sententia">L<span class="verbe">’hypererrance</span> cible et détruit la formation de sa propre idéologie.</p>

<p class="sententia">L<span class="verbe">’hypererrance</span> construit de la destruction.</p>

<p class="sententia">L<span class="verbe">’hypererrance</span> détruit l’idée idéelle pour mieux se nourrir du désordre.</p>

<p class="sententia">L<span class="verbe">’hypererrance</span> libère la matérialité idéelle des empires de la fixité.</p>

<p class="sententia">L<span class="verbe">’hypererrance</span> offre au réseau acentré la décision de sa situation.</p>

<p class="sententia">L<span class="verbe">’hypererrance</span> détruit les centres moteurs du réseau.</p>

<p class="sententia">L’absence de tout centre est une épiphanie de l<span class="verbe">’hypererrance</span>.</p>

<p class="sententia">L’être de l<span class="verbe">’hypererrance</span> se déforme selon les morphologies horizontales et contingentes du réseau.</p>

<p class="sententia">La puissance de l<span class="verbe">’hypererrance</span> se trouve accessible en chaque nœud du réseau.</p>

<p class="sententia">La puissance de l<span class="verbe">’hypererrance</span> est la puissance de l’information libre.</p>

<p class="sententia">L<span class="verbe">’hypererrance</span> diffuse un savoir créateur d’autonomie.</p>

<p class="sententia">L<span class="verbe">’hypererrance</span> délie l'être du spectacle.</p>

<p class="sententia">L’autonomie accordée par l<span class="verbe">’hypererrance</span> permet de briser l’aliénation qui enserre l’ordinaire.</p>

<p class="sententia">Chaque réalité qui se déploie depuis l’ordinaire est liée à la puissance créatrice de l<span class="verbe">’hypererrance</span>.</p>

<p class="sententia">L<span class="verbe">’hypererrance</span> est le geste autonome du réseau acentré.</p>

<p class="sententia">Le geste autonome qui résulte de l<span class="verbe">’hypererrance</span> devient reproductible à l’infini par le vecteur du réseau acentré lui-même.</p>

<p class="sententia">Le partage est un état de nature pour les êtres qui fomentent l<span class="verbe">’hypererrance</span>.</p>

<p class="sententia">Du partage réticulaire émanant de l<span class="verbe">’hypererrance</span> apparaît un savoir commun à tous les nœuds du réseau.</p>

<p class="sententia">Le savoir qui anime le réseau est distribué librement par la croissance de l<span class="verbe">’hypererrance</span>.</p>

<p class="sententia">L<span class="verbe">’hypererrance</span> se nourrit en permanence d’elle-même.</p>

<p class="sententia">La croissance de l<span class="verbe">’hypererrance</span> n’a de limite que les dimensions spatiales qui croissent avec elle.</p>

<p class="sententia">L<span class="verbe">’hypererrance</span> agence des signes pour faire traces de la pluralité de ses passés.</p>

<p class="sententia">L<span class="verbe">’hypererrance</span> s’enrichit de l’expérience de l<span class="verbe">’hypererrance</span> par le partage numérique de l<span class="verbe">’hypererrance</span>.</p>

<p class="sententia">L’augmentation de l’être procède du partage d’un savoir libre issu de l<span class="verbe">’hypererrance</span>.</p>

<p class="sententia">L<span class="verbe">’hypererrance</span> se dissout dans le réseau comme une immanence du réseau.</p>

<p class="sententia">L<span class="verbe">’hypererrance</span> distribue sa liberté par sa dissolution.</p>

<p class="sententia">La dissolution de l<span class="verbe">’hypererrance</span> est une constante renaissance.</p>

<p class="sententia">La liberté de l<span class="verbe">’hypererrance</span> structure un mode d’être qui contamine ce qui nie l<span class="verbe">’hypererrance</span>.</p>

<p class="sententia">L<span class="verbe">’hypererrance</span> permet l’usage, l’étude, la diffusion, la modification de ce qu’est l<span class="verbe">’hypererrance</span>.</p>

<p class="sententia">L<span class="verbe">’hypererrance</span> devient son propre devenir, comme une contamination ontologique de l’espace.</p>

<p class="sententia">L<span class="verbe">’hypererrance</span> étend les tentacules du réseau pour étendre les tentacules de son être.</p>

<p class="sententia">L’action que véhicule l<span class="verbe">’hypererrance</span> use, étudie, diffuse, modifie les propres limites de son ontologie.</p>

<p class="sententia">Par le foisonnement métamorphique de l<span class="verbe">’hypererrance</span>, le réseau acquiert une pleine cybernétique de l’action.</p>

<p class="sententia">L<span class="verbe">’hypererrance</span> est une cybernétique qui affirme une ontologie plurielle et commune motrice du réseau.</p>

<p class="sententia">L<span class="verbe">’hypererrance</span> est une décohérence générative des transmigrations réticulaires.</p>

<p class="sententia">L<span class="verbe">’hypererrance</span> porte une créativité des extensions du réseau.</p>

<p class="sententia">La négation de la négation de l<span class="verbe">’hypererrance</span> compose l’esthétique de l<span class="verbe">’hypererrance</span>.</p>

<p class="sententia">L’harmonie de l<span class="verbe">’hypererrance</span> suit la mesure du cosmos.</p>

<p class="sententia">L<span class="verbe">’hypererrance</span> déconstruit un principe de simplicité, qui n’est ni le minimal ni le facile.</p>

<p class="sententia">L<span class="verbe">’hypererrance</span> entraîne substantiellement une création ontologique.</p>

<p class="sententia">L<span class="verbe">’hypererrance</span> s’arme de la virtualité de ses renaissances.</p>

<p class="sententia">L<span class="verbe">’hypererrance</span> forme l’unicité de la multitude devenue réseau acentré.</p>

<p class="sententia">L<span class="verbe">’hypererrance</span> s’entend comme le langage universel du devenir du réseau acentré.</p>

<p class="sententia">L<span class="verbe">’hypererrance</span> renouvelle sans cesse le langage du réseau afin de détruire toute idée de vérité.</p>

<p class="sententia">L<span class="verbe">’hypererrance</span> apprête les transformations du langage telle une créativité réticulaire.</p>

</div>
