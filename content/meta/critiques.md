---
menutitle: "critique.s."
title: "Kritik und Selbstkritik"
slug: "critiques"
images:
  - "img/error/kritik.jpg"
weight: 2
type: menu
layout: single
---

Le comité de lecture (*à la barre*). --- Nous œuvrons au désœuvrement. Sans émoi, nous y jetons la littérature et ce qu'elle peut encore avoir d'idées. Notre fabrique se place du côté des courts-circuits. Il faut que se dialectisent poétique et politique, dans un désordre de tous les sabbats, que la poétique se politise et que la politique se poétise, sans hiérarchie, sans distinction de ce que doit être socialement théorie ou praxis. Et il ne nous reste que l'étymon à gravir, à graver sous l'œil qui se retourne vers les échos de l'être : nous construisons une *poétique politique* pour *faire cité*. L'ontologie se fait programme, l'idée doit y être la plus brute et la plus matérielle possible, en roulement sur elle-même, en orbite autour d'elle-même, délaissant la raison qui la mettrait au pas. L'ontologie politique est une croyance au retour, à la révolution des temps, que définissent les plis et replis de nos espaces. Règne ainsi l'impérieuse nécessité de déformer les mondes que nous peuplons de notre errance. L'éthéré doit être abattu pour recouvrer le ciel, comme le territoire doit être détruit pour que nos mains se plongent à nouveau au cœur de la terre. Brûler la langue. Se couvrir de ses cendres. Y voir un langage, tout fait de stridences et d'entrechocs, s'extirper de la ruine, comme le matériau qui exhorte le geste au débordement du geste. Le marteau ne va pas à la pierre comme la pierre ne va pas au marteau, mais c'est dans l'insécabilité des jonctions de leur être que se tisse une communion volitive des transformations. La volonté n'est pas à la réaction. La volonté n'est pas au progrès. La volonté est à la brisure. À l'impermanence de la matière et aux érosions lentes. À la plasticité de nos connexions et à nos mutations clandestines. Nous nous devons d'habiter l'entropie. C'est elle qui fonde notre commune, là où la politique est une théorie des quanta. Et dans la recherche patiente des fissures de notre temporalité, l'errance compose des dimensions à explorer, où se réifie avec éclat notre unique mantique. Le présage a la saveur de l'épistémè suivante. Nous portons le feu contre l'esprit. Nos brûlures s'agitent sur l'horizon morne du social. Nous y rêvons de torsions spatiales et de grands incendies, et tout à nos songes, nous fomentons ensemble une rutilance que nous plaçons sous l'égide de l'extravagance : l'électricité est à ruminer. *Ex re et ex errore* --- nous le rêve, nous la révolte, nous la révolution, nous la littérature des courts-circuits, nous affirmons que le communisme est cosmique ou qu'il n'est pas.

Le peuple (*tohu-bohu*). --- Ennemis du peuple !

