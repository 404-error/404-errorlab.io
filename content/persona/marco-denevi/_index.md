---
type: "bio"
bio: "Denevi Marco"
title: "Marco Denevi"
images:
  - "img/persona/marco-denevi.jpg"
gridimages:
  - "img/persona/marco-denevi-grid.jpg"
sourceimage: "https://commons.wikimedia.org/wiki/File:MarcoDenevi2.jpg"
---

Romancier et dramaturge argentin, Marco Denevi (1922-1998) fut l’un des auteurs les plus prolifiques de l’Argentine du XX^e^ siècle. Ses œuvres, traduites en plusieurs langues et adaptées au cinéma, ont été récompensées par diverses distinctions tout au long de sa carrière. Pourtant, elles sont tombées dans l’oubli.

Les éditions Corregidor ont publié l'intégralité de ses écrits.
