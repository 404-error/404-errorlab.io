---
type: "bio"
bio: "Orsoni Jérôme"
title: "Jérôme Orsoni"
images:
  - "img/persona/jerome-orsoni.jpg"
gridimages:
  - "img/persona/jerome-orsoni-grid.jpg"
sourceimage: "https://commons.wikimedia.org/wiki/File:Caravaggio_-_Medusa_-_Google_Art_Project.jpg?uselang=fr"
summary: "Jérôme Orsoni est écrivain. Ses cahiers fantômes. Le reste n’ayant que peu d’importance."
---

Jérôme Orsoni est écrivain. Ses [cahiers fantômes](https://cahiersfantomes.com/). Le reste n’ayant que peu d’importance.
