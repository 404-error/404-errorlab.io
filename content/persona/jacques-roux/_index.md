---
type: "bio"
bio: "Roux Jacques"
title: "Jacques Roux"
images:
  - "img/persona/jacques-roux.jpg"
gridimages:
  - "img/persona/jacques-roux-grid.jpg"
sourceimage: "https://commons.wikimedia.org/wiki/File:Jacques_Roux_-_gravure_de_J.-Fr%C3%A9d%C3%A9ric_Cazenave.jpg"
---

Révolutionnaire français (1752-1794). Proche des sans-culottes et de la section des Gravilliers, ce prêtre constitutionnel catholique fut l'un des chefs de file du mouvement des Enragés et l'un des précurseurs de l'anarchisme.
