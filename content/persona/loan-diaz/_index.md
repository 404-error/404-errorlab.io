---
type: "bio"
bio: "diaz loan"
title: "loan diaz"
images:
  - "img/persona/loan-diaz.jpg"
gridimages:
  - "img/persona/loan-diaz-grid.jpg"
sourceimage: "loan diaz"
---

Rêveur mélancolique autoproclamé "Assembleur de nuées à la petite semaine" (à la petite semaine, parce que la poésie comme le crime ne paie pas).

Écrit comme on jette des "bouteilles à la mer" pour tisser les liens qui libèrent et rassembler les hémisphères autour d’utopies mobilisatrices.

Le chemin vers son <a href="https://loandiaz.tumblr.com/" class="shake" target="_blank" rel="noopener">site</a>.
