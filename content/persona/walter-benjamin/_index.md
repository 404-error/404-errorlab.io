---
type: "bio"
bio: "Benjamin Walter"
title: "Walter Benjamin"
images:
  - "img/persona/walter-benjamin.jpg"
gridimages:
  - "img/persona/walter-benjamin-grid.jpg"
sourceimage: "https://commons.wikimedia.org/wiki/File:BenjaminBnF.jpg"
---

Pessimiste révolutionnaire ayant poursuivi l'*Angelus novus*, inlassablement.
