---
type: "bio"
bio: "Labbé Olivier"
title: "Olivier Labbé"
images:
  - "img/persona/olivier-labbe.jpg"
gridimages:
  - "img/persona/olivier-labbe-grid.jpg"
sourceimage: "Olivier Labbé"
summary: "Olivier Labbé Né en 1969 Vit et travaille à Le Havre. Site web : olivierlabbe.fr Mène une recherche en art sonore depuis 2001. Compose des musiques électroacoustiques spatialisées sur acousmonium lors de concerts en public. Implante des installations et environnements sonores dans le cadre de manifestations culturelles. Réalise des bandes sons pour le cinéma expérimental. Instrumentiste improvisateur associé au collectif Piednu depuis 2015, questionne l’idée de « musique libre » et multiplie les expériences de concerts « uniques »."
---

Olivier Labbé\
Né en 1969\
Vit et travaille à Le Havre.

Site web :\
[https://www.olivierlabbe.fr](https://www.olivierlabbe.fr)

Mène une recherche en art sonore depuis 2001. Compose des musiques électroacoustiques spatialisées sur acousmonium lors de concerts en public.\
Implante des installations et environnements sonores dans le cadre de manifestations culturelles.\
Réalise des bandes sons pour le cinéma expérimental.\
Instrumentiste improvisateur associé au collectif Piednu depuis 2015, questionne l’idée de "musique libre" et multiplie les expériences de concerts "uniques".
