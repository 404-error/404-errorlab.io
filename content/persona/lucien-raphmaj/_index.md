---
type: "bio"
bio: "Raphmaj Lucien"
title: "Lucien Raphmaj"
images:
  - "img/persona/lucien-raphmaj.jpg"
gridimages:
  - "img/persona/lucien-raphmaj-grid.jpg"
sourceimage: "Lucien Raphmaj"
summary: "Spectre en perpétuelle métamorphose transmédiatique, il s'est inventé une latérature pour se perdre et se retrouver dans l'écriture."
---

Spectre en perpétuelle métamorphose transmédiatique, il s'est inventé une [latérature](https://lucienraphmaj.wordpress.com/) pour se perdre et se retrouver dans l'écriture.


