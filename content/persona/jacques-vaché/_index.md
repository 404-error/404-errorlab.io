---
type: "bio"
bio: "Vaché Jacques"
title: "Jacques Vaché"
images:
  - "img/persona/jacques-vache.jpg"
gridimages:
  - "img/persona/jacques-vache-grid.jpg"
sourceimage: "https://commons.wikimedia.org/wiki/File:Vach%C3%A9_anglais.jpg?uselang=fr"
---

Homme de lettres, homme des lettres (1895-1919). De la Grande Guerre au grand opium, spectre surréaliste qui sut mettre dans son écriture tout ce qui surabonde en la vie.

