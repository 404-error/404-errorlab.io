---
type: "bio"
bio: "Bourrit Bernard"
title: "Bernard Bourrit"
images:
  - "img/persona/bernard-bourrit.jpg"
gridimages:
  - "img/persona/bernard-bourrit-grid.jpg"
sourceimage: "Bernard Bourrit"
---

Bernard Bourrit, né en 1977, vit à Genève. Il est l’auteur de textes brefs sur l’art brut, les portraits funéraires, les reliques, les doubles dévorants, le scepticisme, l'anarchisme, la photographie documentaire, les arbres. Publié dans les revues *Critique*, *La Part de l’Œil*, *L’Homme*, *Revue d’histoire des religions*, *The Black Herald*, *Poétique*, *Littérature*, il est l’auteur de *Fautrier ou le désengagement de l’art* (L’Épure, 2006), *Montaigne. Pensées frivoles et vaines écorces* (Le Temps qu’il fait, 2018) et co-traducteur de Zheng Yi (Bleu de Chine, 2007), Tsering Woeser, Ge Fei et Jia Pingwa (Gallimard, 2010, 2012, 2017).
