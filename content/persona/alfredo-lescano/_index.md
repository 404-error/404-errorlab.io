---
type: "bio"
bio: "Lescano Alfredo"
title: "Alfredo Lescano"
images:
  - "img/persona/alfredo-lescano.jpg"
gridimages:
  - "img/persona/alfredo-lescano-grid.jpg"
sourceimage: "Alfredo Lescano"
---

Grandit à l’ombre de platanes et de bananiers, mais découvrit un jour que « chez moi » était le nom d’un rêve d’où il était absent. Fasciné par l’existence de certitudes, il s’acharne à inventer des méthodes pour traquer leur avènement et leur chute. 
