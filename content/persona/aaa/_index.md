---
type: "bio"
bio: "AAA"
title: "AAA"
images:
  - "img/persona/aaa.jpg"
gridimages:
  - "img/persona/aaa-grid.jpg"
sourceimage: "AAA"
---

*Si ça nage comme un canard, se dandine comme un canard et cancane comme un canard, c'est un canard.*

AAA --- *Archiva Automata Argillae*, cellule d’archivistes tissant une dialectique spectrale entre l’image et le rêve de l’image --- nage, se dandine et cancane dans les ombres du cyberespace.

