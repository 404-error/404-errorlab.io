---
type: "bio"
bio: "Yoyotte Simone"
title: "Simone Yoyotte"
images:
  - "img/persona/simone-yoyotte.jpg"
gridimages:
  - "img/persona/simone-yoyotte-grid.jpg"
sourceimage: "https://commons.wikimedia.org/wiki/File:The_National_geographic_magazine_(1902)_(14595270479).jpg"
---

Poétesse et intellectuelle martiniquaise (1910-1933). Elle fut la seule femme qui participa à la rédaction de la revue *Légitime Défense*.


