---
type: "bio"
bio: "Crevel René"
title: "René Crevel"
images:
  - "img/persona/rene-crevel.jpg"
gridimages:
  - "img/persona/rene-crevel-grid.jpg"
sourceimage: "https://commons.wikimedia.org/wiki/File:Ren%C3%A9_Crevel,_Nouvelles_litt%C3%A9raires,_3_juin_1933.jpg"
---

René Crevel, de l’écriture révoltée à la casse du roman, l’irraison surréaliste, et sans détour, l’inventeur des sommeils. De Babylone à la mort difficile. Dégoût.

