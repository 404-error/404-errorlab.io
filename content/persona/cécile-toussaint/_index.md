---
type: "bio"
bio: "Toussaint Cécile"
title: "Cécile Toussaint"
images:
  - "img/persona/cecile-toussaint.jpg"
gridimages:
  - "img/persona/cecile-toussaint-grid.jpg"
sourceimage: "https://commons.wikimedia.org/wiki/File:Egon_Schiele_058.jpg"
---

Date de naissance : mort du mur. Lieu de naissance : sud du mur, Marseille Nord. Origine : Haïti. Destination : néant moins un. Profession : luthière. Localisation : Ligurie. Littérature : tentative de dialogue avec les ruines.
