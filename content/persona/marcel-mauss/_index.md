---
type: "bio"
bio: "Mauss Marcel"
title: "Marcel Mauss"
images:
  - "img/persona/marcel-mauss.jpg"
gridimages:
  - "img/persona/marcel-mauss-grid.jpg"
sourceimage: "https://commons.wikimedia.org/wiki/File:M.Mauss_1872-1950.jpg"
---

Marcel Mauss (1872-1950), anthropologue français, eut une influence considérable sur l'anthropologie moderne, notamment par ses études sur la magie et le don.
