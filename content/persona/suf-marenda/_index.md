---
type: "bio"
bio: "marenda suf"
title: "suf marenda"
images:
  - "img/persona/suf-marenda.jpg"
gridimages:
  - "img/persona/suf-marenda-grid.jpg"
sourceimage: "suf marenda"
summary: "poétaire apoétisant, suf travaille ici : labos-de-la-realite.net."
---

poétaire apoétisant, suf travaille ici : <a href="https://labos-de-la-realite.net/" class="shake" target="_blank" rel="noopener">labos-de-la-realite.net</a>.
