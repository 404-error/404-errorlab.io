---
type: "bio"
bio: "Artaud Antonin"
title: "Antonin Artaud"
images:
  - "img/persona/antonin-artaud.jpg"
gridimages:
  - "img/persona/antonin-artaud-grid.jpg"
sourceimage: "https://commons.wikimedia.org/wiki/File:Antonin_Artaud_1926.jpg"
---

Antonin Artaud est une déchirure qui parcourt le réel. L'existence y est aveugle, mais subsistent des encres comme des drogues, qui ont le pouvoir révélateur de s'infiltrer sous les apparences, et c'est de cette puissance augurale qu'Antonin Artaud, de Rodez au pays des Tarahumaras, se revendique sans concession aucune.
