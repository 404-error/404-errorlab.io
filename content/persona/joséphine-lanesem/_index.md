---
type: "bio"
bio: "Lanesem Joséphine"
title: "Joséphine Lanesem"
images:
  - "img/persona/josephine-lanesem.jpg"
gridimages:
  - "img/persona/josephine-lanesem-grid.jpg"
sourceimage: "https://commons.wikimedia.org/wiki/File:Durer-ecureuil.jpg?uselang=fr"
summary: "Entre Orphée et Icare. Repeupler l'intériorité. Nervures et Entailles, le site de Joséphine Lanesem."
---

Entre Orphée et Icare. Repeupler l'intériorité.

<a href="https://josephinelanesem.com/" target="_blank" rel="noopener">Nervures et Entailles</a>, le site de Joséphine Lanesem.



