---
type: "bio"
bio: "Léro Étienne"
title: "Étienne Léro"
images:
  - "img/persona/etienne-lero.jpg"
gridimages:
  - "img/persona/etienne-lero-grid.jpg"
sourceimage: "https://commons.wikimedia.org/wiki/File:The_American_Museum_journal_(c1900-(1918))_(18159591355).jpg"
---

Écrivain martiniquais (1910-1939). Il fut un des cofondateurs du groupe *Légitime Défense*.

