---
type: "bio"
bio: "Schrager Frédérique"
title: "Frédérique Schrager"
images:
  - "img/persona/frederique-schrager.jpg"
gridimages:
  - "img/persona/frederique-schrager-grid.jpg"
sourceimage: "Frédérique Schrager"
---

Jamais de métaphysique, rien que de l’ontologie, et une philosophe qui fabrique des idées politiques à partir d’une pensée antique de l’être.
