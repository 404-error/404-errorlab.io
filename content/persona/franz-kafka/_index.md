---
type: "bio"
bio: "Kafka Franz"
title: "Franz Kafka"
images:
  - "img/persona/franz-kafka.jpg"
gridimages:
  - "img/persona/franz-kafka-grid.jpg"
sourceimage: "https://commons.wikimedia.org/wiki/File:Kafka1913.jpg"
---

Écrivain praguois, qui vivait sous la langue, sous le monde qu'elle impose, pour y creuser des rêves d'insectes et un temps sans Loi.
