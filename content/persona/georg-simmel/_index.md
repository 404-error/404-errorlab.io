---
type: "bio"
bio: "Simmel Georg"
title: "Georg Simmel"
images:
  - "img/persona/georg-simmel.jpg"
gridimages:
  - "img/persona/georg-simmel-grid.jpg"
sourceimage: "https://commons.wikimedia.org/wiki/File:Georg_Simmel.jpg"
---

Sociologue et philosophe allemand (1858-1918) ayant enrichi la sociologie par son approche philosophique, et inversement.
