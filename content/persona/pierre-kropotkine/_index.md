---
type: "bio"
bio: "Kropotkine Pierre"
title: "Pierre Kropotkine"
images:
  - "img/persona/pierre-kropotkine.jpg"
gridimages:
  - "img/persona/pierre-kropotkine-grid.jpg"
sourceimage: "https://commons.wikimedia.org/wiki/File:Kropotkin1.jpg"
---

Pierre Kropotkine (1842-1921), géographe anarchiste russe, a consacré sa vie à une politique de l'émancipation sociale, qui l'a conduit, au travers des liens entre pratique militante et théorie scientifique, à devenir l'un des principaux penseurs de l’anarcho-communisme.
