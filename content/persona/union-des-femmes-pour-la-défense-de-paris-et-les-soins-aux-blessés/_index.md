---
type: "bio"
bio: "Union des femmes pour la défense de Paris et les soins aux blessés"
title: "Union des femmes pour la défense de Paris et les soins aux blessés"
images:
  - "img/persona/union-des-femmes-pour-la-defense-de-paris-et-les-soins-aux-blesses.jpg"
gridimages:
  - "img/persona/union-des-femmes-pour-la-defense-de-paris-et-les-soins-aux-blesses-grid.jpg"
sourceimage: ""
---

L’*Union des femmes pour la défense de Paris et les soins aux blessés*, fondée le&nbsp;11&nbsp;avril&nbsp;1871, fut l’une des premières organisations féministes, qui participa activement à la vie de la Commune de Paris et s’occupa notamment de la cause des travailleuses parisiennes.
