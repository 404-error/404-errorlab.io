---
type: "bio"
bio: "Lampás Pier"
title: "Pier Lampás"
images:
  - "img/persona/pier-lampas.jpg"
gridimages:
  - "img/persona/pier-lampas-grid.jpg"
sourceimage: "Pier Lampás"
summary: "Lutte, vagabonde, écrit dans le sud de l'Europe."
---

Lutte, vagabonde, écrit dans le sud de l'Europe.

<a href="https://pierlampas.com" class="shake" target="_blank" rel="noopener">Un site</a> pour faire *livre d'image*.
