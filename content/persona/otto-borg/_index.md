---
type: "bio"
bio: "Borg Otto"
title: "Otto Borg"
images:
  - "img/persona/otto-borg.jpg"
gridimages:
  - "img/persona/otto-borg-grid.jpg"
---

Séance de lutte et thé dansant.

Pugiliste au chômage pour cause d'indolence.
