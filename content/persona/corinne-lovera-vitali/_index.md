---
type: "bio"
bio: "lovera vitali corinne"
title: "corinne lovera vitali"
images:
  - "img/persona/corinne-lovera-vitali.jpg"
gridimages:
  - "img/persona/corinne-lovera-vitali-grid.jpg"
sourceimage: "corinne lovera vitali"
summary: "Corinne Lovera Vitali s’appelle clv et tout ou presque de son travail se trouve sur son site et son site et son site où tout est gratuit ce qui est presque de l’anti-tout. Ses livres pas gratuits sont publiés depuis 1999 en voici quelques-uns complètement pas dans l’actualité la preuve celui-ci est épuisé : la Taille des hommes, celui-ci est stocké à la maison : Scrute le travail, mais ceux-là les derniers en date oui ils peuvent encore payer des croquettes : Ce qu’il faut et 78 moins 39 et Coupe-le. Elle écrit aussi des textes tout isolés comme des taupes, qui veulent parfois voir le jour comme ici même. Sinon je suis bien occupée à faire le parlé musique avec Fernand Fernandez. La plupart du temps nous nous tenons loin de la foule déchaînée, et quelquefois il y a du monde par exemple en 2023 il y a eu Richard Brautigan."
---

Corinne Lovera Vitali s’appelle clv et tout ou presque de son travail se trouve sur son <a href="https://corinne-lovera-vitali.net/" target="_blank" rel="noopener">site</a> et son <a href="https://soundcloud.com/loveravitali" target="_blank" rel="noopener">site</a> et son <a href="https://www.youtube.com/channel/UCG5IxtYDHLWltY9KGUTFbpQ" target="_blank" rel="noopener">site</a> où tout est gratuit ce qui est presque de l’anti-tout.

Ses livres pas gratuits sont publiés depuis 1999 en voici quelques-uns complètement pas dans l’actualité la preuve celui-ci est épuisé : <a href="https://lmda.net/2006-05-mat07330-la_taille_des_hommes?debut_articles=%405138" class="shake" target="_blank">la Taille des hommes</a>, celui-ci est stocké à la maison : <a href="http://non.ultra-book.com/portfolio#scrute_le_travail_clv__61788.jpg" class="shake" target="_blank">Scrute le travail</a>, mais ceux-là les derniers en date oui ils peuvent encore payer des croquettes : <a href="https://www.publie.net/2016/10/19/nouveaute-ce-quil-faut-de-corinne-lovera-vitali/" class="shake" target="_blank">Ce qu’il faut</a> et <a href="http://media.wix.com/ugd/e20eec_a018f92529174dcb8a8c148a5041d061.pdf" class="shake" target="_blank">78 moins 39</a> et <a href="https://www.editions-mf.com/produit/97/9782378040222/coupe-le" class="shake" target="_blank">Coupe-le</a>.

Elle écrit aussi des textes tout isolés comme des taupes, qui veulent parfois voir le jour comme ici même. 

Sinon je suis bien occupée à faire le <a href="https://soundcloud.com/les-fernandez-les-fernandez" class="shake" target="_blank">parlé musique</a> avec <a href="https://www.fernandfernandezpeinture.org/" class="shake" target="_blank">Fernand Fernandez.</a> La plupart du temps nous nous tenons loin de la foule déchaînée, et <a href="https://lesfernandez.bandcamp.com/album/richard-brautigan" class="shake" target="_blank">quelquefois il y a du monde par exemple en 2023 il y a eu Richard Brautigan.</a>

