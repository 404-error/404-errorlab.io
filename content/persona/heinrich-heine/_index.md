---
type: "bio"
bio: "Heine Heinrich"
title: "Heinrich Heine"
images:
  - "img/persona/heinrich-heine.jpg"
gridimages:
  - "img/persona/heinrich-heine-grid.jpg"
sourceimage: "https://commons.wikimedia.org/wiki/File:Heinrich_Heine-Oppenheim.jpg"
---

Poète allemand (1797-1856) qui traversa le romantisme pour placer au cœur de la langue poétique l'ordinaire et ses souffrances.
