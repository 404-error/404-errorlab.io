---
type: "bio"
bio: "Marx Karl"
title: "Karl Marx"
images:
  - "img/persona/karl-marx.jpg"
gridimages:
  - "img/persona/karl-marx-grid.jpg"
sourceimage: "https://commons.wikimedia.org/wiki/File:Karl_Marx.png"
---

Matérialiste allemand, dont l'existence prouve que quelques écrits peuvent créer quelques désordres.
