---
type: "bio"
bio: "Rojo Esperanza"
title: "Esperanza Rojo"
images:
  - "img/persona/esperanza-rojo.jpg"
gridimages:
  - "img/persona/esperanza-rojo-grid.jpg"
sourceimage: "https://commons.wikimedia.org/wiki/File:Odilon_Redon_-_Dante_et_B%C3%A9atrice.jpg"
---

Hurle sur les murs de l’urbain.

Théorie de l’art & des rêves-voltes.
