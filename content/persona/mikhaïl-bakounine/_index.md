---
type: "bio"
bio: "Bakounine Mikhaïl"
title: "Mikhaïl Bakounine"
images:
  - "img/persona/mikhaïl-bakounine.jpg"
gridimages:
  - "img/persona/mikhaïl-bakounine-grid.jpg"
sourceimage: "https://commons.wikimedia.org/wiki/File:Tournachon,_Gaspard-F%C3%A9lix_-_Michail_Bakunin_(1814-1876)_(Zeno_Fotografie).jpg"
---

Philosophe et révolutionnaire russe (1814-1876), qui fut l'un des principaux théoriciens de l'anarchisme, en particulier du collectivisme et du socialisme libertaire, et dont la pensée constitua une critique majeure de la structure étatique.
