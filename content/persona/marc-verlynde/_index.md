---
type: "bio"
bio: "Verlynde Marc"
title: "Marc Verlynde"
images:
  - "img/persona/marc-verlynde.jpg"
gridimages:
  - "img/persona/marc-verlynde-grid.jpg"
sourceimage: "Marc Verlynde"
summary: "Lit souvent, écrit parfois surtout pour s'inventer des viduités."
---

Lit souvent, écrit parfois surtout pour s'inventer des <a href="https://viduite.wordpress.com/" target="_blank" rel="noopener">viduités</a>.



