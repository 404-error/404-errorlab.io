---
type: "bio"
bio: "Engels Friedrich"
title: "Friedrich Engels"
images:
  - "img/persona/friedrich-engels.jpg"
gridimages:
  - "img/persona/friedrich-engels-grid.jpg"
sourceimage: "https://commons.wikimedia.org/wiki/File:Friedrich_Engels_(1891).jpg"
---

Matérialiste allemand, souvent dans l'ombre d'un autre matérialiste allemand, tout aussi barbu, mais dont l'œuvre théorique singulière mérite quelques lumières.
