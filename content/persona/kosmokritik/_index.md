---
type: "bio"
bio: "Kosmokritik"
title: "Kosmokritik"
images:
  - "img/persona/kosmokritik.jpg"
gridimages:
  - "img/persona/kosmokritik-grid.jpg"
sourceimage: "kosmokritik"
summary: "Kosmokritik est une entité cybernétique s'occupant de méontologie afin d'établir un retour critique au cosmos."
---

<a href="https://krtk.cc" class="shake" target="_blank" rel="noopener">Kosmokritik</a> est une entité cybernétique s'occupant de méontologie afin d'établir un retour critique au cosmos.
