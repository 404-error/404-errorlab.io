---
type: "bio"
bio: "Delabre Pierre-Aurélien"
title: "Pierre-Aurélien Delabre"
images:
  - "img/persona/pierre-aurelien-delabre.jpg"
gridimages:
  - "img/persona/pierre-aurelien-delabre-grid.jpg"
sourceimage: "Pierre-Aurélien Delabre"
summary: "Né en 1988 en région parisienne. Fabrique des textes et des images. Habite aujourd’hui à Naples. Son site : Esprit de l'utopie. Membre du groupe volodia : groupevolodia.com."
---

Né en 1988 en région parisienne.<br>
Fabrique des textes et des images.<br>
Habite aujourd’hui à Naples.

Son site : <a href="https://espritdelutopie.com/" class="shake" target="_blank" rel="noopener">Esprit de l'utopie</a>.<br>
Membre du groupe volodia : <a href="https://groupevolodia.com/" class="shake" target="_blank" rel="noopener">groupevolodia.com</a>.
