---
type: "bio"
bio: "Michel Louise"
title: "Louise Michel"
images:
  - "img/persona/louise-michel.jpg"
gridimages:
  - "img/persona/louise-michel-grid.jpg"
sourceimage: "https://commons.wikimedia.org/wiki/File:Image_appert_ernest_charles_portrait_de_michel_louise_1830-1905_pris_a_la_prison_des_chantiers_a_versaille_353512.jpg"
---

Louise Michel (1830-1905), institutrice et écrivaine anarchiste, fut l'une des figures principales de la Commune de Paris, ce qui lui valut d'être déportée en Nouvelle-Calédonie. Après son retour en France en 1880, elle continua la lutte politique, au péril de sa liberté et même de sa vie, à la seule fin de défendre le sort des prolétaires.
