---
type: "bio"
bio: "Malatesta Errico"
title: "Errico Malatesta"
images:
  - "img/persona/errico-malatesta.jpg"
gridimages:
  - "img/persona/errico-malatesta-grid.jpg"
sourceimage: "https://commons.wikimedia.org/wiki/File:ErricoMalatesta.gif"
---

Écrivain et anarchiste italien (1853-1932). Il fut un des théoriciens de l'anarcho-communisme.
