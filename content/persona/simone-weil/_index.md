---
type: "bio"
bio: "Weil Simone"
title: "Simone Weil"
images:
  - "img/persona/simone-weil.jpg"
gridimages:
  - "img/persona/simone-weil-grid.jpg"
sourceimage: "https://commons.wikimedia.org/wiki/File:SimoneWeil_(Spain).jpg"
---

1909, naissance, 1943, mort. Une vie brève mais qui se jeta intensément dans la question sociale du siècle. Simone Weil fait partie de ces rares penseurs qui accordent leur existence à leur pensée. Anarcho-syndicaliste et critique éclairée de Marx, christique et non chrétienne, de tous les combats pour défendre les sans-voix, malgré une érudition rare qui la mena de la mathématique au sanskrit, et qui aurait pu lui offrir la vie paisible du professorat, cette « martienne », selon le mot d’Alain, avait la merveilleuse intransigeance de la cohérence. Elle se rendit comme ouvrière à l’usine de 1934 à 1935, en dépit de sa santé fragile, puis en 1936, elle partit en Espagne pour soutenir ses camarades républicains, ne put accepter leurs écarts, continua à lutter, à écrire, développa une mystique de la justice et de la souffrance de l’absence de justice. Durant la guerre, elle se rendit à Londres pour rejoindre la France libre, ne pouvant supporter le confort américain. Elle voulut rentrer en France en tant que résistante, on le lui refusa. Tuberculeuse, à l’orée de la mort, par solidarité avec ses compatriotes subissant les restrictions de l’occupation, elle ne s’alimenta quasiment plus, et jusqu’à son dernier souffle, Simone Weil demeura fidèle à ce qu’elle défendait.
