---
type: "bio"
bio: "Michelet Étienne"
title: "Étienne Michelet"
images:
  - "img/persona/etienne-michelet.jpg"
gridimages:
  - "img/persona/etienne-michelet-grid.jpg"
sourceimage: "Étienne Michelet"
summary: "Né en 1984. Vit en Corée du Sud. Y écrit. Publie et dirige le projet Preta ainsi que la revue Memori. Travaille actuellement à une théorie vampire sur l'expérience méditative du Seon. Son site : DONGMURI."
---

Né en 1984. Vit en Corée du Sud. Y écrit. Publie et dirige le projet Preta ainsi que la <a href="https://www.pretamemori.com/" class="shake" target="_blank">revue Memori</a>.

Son site : <a href="https://www.dongmuri.net/" class="shake" target="_blank" rel="noopener">DONGMURI</a>.
