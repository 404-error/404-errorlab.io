---
type: "bio"
bio: "Persson Ann"
title: "Ann Persson"
images:
  - "img/persona/ann-persson.jpg"
gridimages:
  - "img/persona/ann-persson-grid.jpg"
sourceimage: "https://commons.wikimedia.org/wiki/File:Trophime_Bigot_-_Judith_Cutting_Off_the_Head_of_Holofernes_-_Google_Art_Project.jpg"
---

Disciple d’une méduse. Ouvrière des lettres et d’émeutes. De la traduction à ses heures perdues.
