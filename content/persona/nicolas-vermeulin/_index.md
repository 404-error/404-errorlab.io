---
type: "bio"
bio: "Vermeulin Nicolas"
title: "Nicolas Vermeulin"
images:
  - "img/persona/nicolas-vermeulin.jpg"
gridimages:
  - "img/persona/nicolas-vermeulin-grid.jpg"
sourceimage: "Nicolas Vermeulin"
---

Nicolas Vermeulin est un travailleur de l’art, manœuvre au 3x8 par choix d’une liberté, une allumette toujours prête à allumer une mèche. On dit de lui qu’il a perdu son temps, c’est vrai.
