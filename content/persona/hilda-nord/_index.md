---
type: "bio"
bio: "Nord Hilda"
title: "Hilda Nord"
images:
  - "img/persona/hilda-nord.jpg"
gridimages:
  - "img/persona/hilda-nord-grid.jpg"
sourceimage: "https://commons.wikimedia.org/wiki/File:Battleship_Potemkin.webm"
---

Ne s’occupe pas d’iconologie, d’herméneutique ou de méréologie. S’occupe du néant qui les nimbe. Quelque part à Berlin.
