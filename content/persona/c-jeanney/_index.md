---
type: "bio"
bio: "Jeanney C."
title: "C. Jeanney"
images:
  - "img/persona/c-jeanney.jpg"
gridimages:
  - "img/persona/c-jeanney-grid.jpg"
sourceimage: "C. Jeanney"
---

C. Jeanney est autrice, parfois traductrice, souvent artiste visuelle. Elle fait ce qu’elle a à faire, et ce qu’elle a fait parle pour elle. Pas besoin de chercher beaucoup plus.

Et ce qu’elle a fait ou fait ou fera se trouve notamment sur son site&nbsp;<a href="http://christinejeanney.net/" target="_blank" rel="noopener">tentatives</a>. Sur la <a href="http://www.maisonstemoin.fr/author/cjeanney/" target="_blank" rel="noopener">maison[s]témoin</a> aussi.

Il est possible de la suivre sur <a href="https://twitter.com/cjeanney" target="_blank" rel="noopener">twitter</a>. La liste de ses ouvrages se trouve sur <a href="http://christinejeanney.net/spip.php?article939" target="_blank" rel="noopener">la page bibliographique de son site</a>.
