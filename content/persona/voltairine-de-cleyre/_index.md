---
type: "bio"
bio: "Cleyre Voltairine de"
title: "Voltairine de Cleyre"
images:
  - "img/persona/voltairine-de-cleyre.jpg"
gridimages:
  - "img/persona/voltairine-de-cleyre-grid.jpg"
sourceimage: "https://en.wikipedia.org/wiki/File:Voltairine_de_Cleyre_(Age_35).jpg"
---

Écrivaine américaine (1866-1912). Ses écrits ainsi que ses engagements politiques contribuèrent à développer les théories anarchiste et féministe.
