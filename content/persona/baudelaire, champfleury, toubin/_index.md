---
type: "bio"
bio: "Baudelaire, Champfleury, Toubin"
title: "Baudelaire, Champfleury, Toubin"
images:
  - "img/persona/baudelaire-champfleury-toubin.jpg"
gridimages:
  - "img/persona/baudelaire-champfleury-toubin-grid.jpg"
sourceimage: "https://commons.wikimedia.org/wiki/File:Barricade_(Le_Salut_public)_Courbet.jpg"
---

Charles Baudelaire, Jules Champfleury, Charles Toubin : trois escarbilles des barricades. 1848 : février ou la révolution, l'enthousiasme contre tous les hivers. Et la pensée républicaine qui se fit feuille pour *Salut public* (deux fois).
