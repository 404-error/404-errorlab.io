---
type: "bio"
bio: "Capparos Olivier"
title: "Olivier Capparos"
images:
  - "img/persona/olivier-capparos.jpg"
gridimages:
  - "img/persona/olivier-capparos-grid.jpg"
sourceimage: "Olivier Capparos"
---

Olivier Capparos, peintre et philosophe né en 1968 à Lyon, sinue parmi les mots et les formes pour faire entendre et donner à voir ce qui se dissimule dans les ombres de nos réalités. 

