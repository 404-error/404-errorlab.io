---
persona:
  - Pierre-Aurélien Delabre
title: "Notes pour un communisme déjà existant"
slug: "notes-pour-un-communisme-deja-existant"
date: 2022-09-29
echo:
  - stratégique
images:
  - "img/220902-delabre-notes-pour-un-communisme-deja-existant.jpg"
gridimages:
  - "img/220902-delabre-notes-pour-un-communisme-deja-existant-grid.jpg"
sourceimage: "Pierre-Aurélien Delabre"
notes: ""
summary: "Notre communisme ne dénie pas la finitude : il s'en saisit comme
habitacle de nos errances, odeur d'un crépuscule cherchant à retenir le
jour, nuit sans mirages ou quelque festin païen."
citation: "la consistance ontologique d'un camion
qui nous écrase"
hangindent: false
---

{{%epigraphe%}}

Ce communisme en tant que naturalisme achevé = humanisme, en tant
qu'humanisme achevé = naturalisme; il est la vraie solution de
l'antagonisme entre l'homme et la nature, entre l'homme et l'homme, la
vraie solution de la lutte entre existence et essence, entre
objectivation et affirmation de soi, entre liberté et nécessité, entre
individu et genre. Il est l'énigme résolue de l'histoire et il se
connaît comme cette solution.

--- Karl Marx, *Manuscrits économico-politiques de 1844*.

{{%/epigraphe%}}

{{%asterisme%}}

Notre communisme ne dénie pas la finitude : il s'en saisit comme
habitacle de nos errances, odeur d'un crépuscule cherchant à retenir le
jour, nuit sans mirages ou quelque festin païen.

{{%asterisme%}}

C'est un ancrage transindividuel, un mode d'être collectif, la souche de
traditions profanes qui se transmettent par-delà toute commémoration
d'État, tout appareillage mémoriel, cette mémoire asséchée des
institutions et des livres d'Histoire.

{{%asterisme%}}

Par-delà le culte de l'Écrit, cette autolégitimation d'un pouvoir
colonial qui glane sa survie en écrasant toute culture spécifique :
idiomes, dialectes, lettres d'amour bourrées de fautes d'orthographe,
etc.

{{%asterisme%}}

Par-delà l'Histoire, cette collection de faits structurés
arbitrairement, selon le bon vouloir des puissants, de leurs intérêts
pragmatiques, de leurs caprices.

{{%asterisme%}}

Par-delà le culte des corps réifiés, cette fossilisation des êtres que
pratique le régime dominant de la *représentation de soi*, à grands
coups de chirurgie plastique et de dénégation morbide.

{{%asterisme%}}

Par-delà également l'idéal de l'Homme majeur, éclairé, conduisant
l'Humanité vers une improbable libération.

{{%asterisme%}}

Nous savons désormais que cette libération se confond toujours avec la
représentation d'un monde-déjà-mort, ayant aboli le spécifique en tant
que *fait positif* au nom d'un processus civilisateur génocidaire.

{{%asterisme%}}

Contre l'idée de Progrès, qui acte le développement infini des forces
productives aliénées, au nom d'une espérance de libération à venir, nous
affirmons la nécessité de nous réapproprier les notions de *croissance*
et de *sobriété*, de les articuler *autrement*, en refusant les écueils
petits-bourgeois du pragmatisme économique ou du fort naïf « retour à la
terre ».

{{%asterisme%}}

Cette *enfance du communisme*, telle que nous l'entendons, est celle de
ce vieillard infirme goûtant avec malice à la saveur sans cesse
régénérée des saisons, au caractère miraculeux des renaissances,
accueillant la vieillesse et la mort comme douces fatalités, en leur
inexorable jardin, sur cette terre qui nous a précédés et nous survivra.

{{%asterisme%}}

C'est l'éblouissement originel de l'enfant devant les couleurs du jour.

{{%asterisme%}}

C'est l'insistance forcenée des saisons.

{{%asterisme%}}

C'est l'abondance surnaturelle, éclatante.

{{%asterisme%}}

C'est l'épuisement des cycles mécaniques de répétition et de remplissage : l'acceptation du vide.

{{%asterisme%}}

C'est l'amour inconditionnel et le désir sans objet.

{{%asterisme%}}

C'est l'inadéquation de soi à soi.

{{%asterisme%}}

C'est un écart générateur d'agencements fertiles, clandestins,
périphériques.

{{%asterisme%}}

C'est un corps collectif tissé de tous les corps qui bravent l'irréalité
du monde.

{{%asterisme%}}

C'est un corps complexe qui endigue la logique économique produisant
l'individu en tant que *capital de lui-même* et *manager de soi*.

{{%asterisme%}}

C'est une réappropriation des espaces (matériels & immatériels)
colonisés par le Capital & l'État, en leurs marchandages constitutifs
d'un monde en sursis, ces producteurs effrénés de systèmes de valeurs
marchandes & morales qui se (re)présentent à nous sous les traits d'une
fausse éternité conquérante.

{{%asterisme%}}

Ce *communisme de l'enfance* n'est pas une régression, ou un reniement
de la technique *en soi* : c'est une nouvelle intelligibilité
matérielle, une adaptation lucide aux moyens que le Capital utilise pour
nous abolir, une appropriation consciente et raisonnée des flux qui nous
débordent. C'est un désarmement.

{{%asterisme%}}

Ainsi, nous inventons de nouveaux *communs techniques*.

{{%asterisme%}}

Ces derniers, de fait, s'inscrivent hors de l'opposition de l'Homme et
de la Nature --- division inaugurale de la modernité capitaliste --- qui
induit également l'opposition toujours bourgeoise, c'est-à-dire
abstraite, de la Ville et de la Campagne.

{{%asterisme%}}

Il ne s'agit pas d'une simple distorsion de la réalité par l'idéologie
bourgeoise, cette abstraction a la consistance ontologique d'un camion
qui nous écrase : elle régit l'organisation matérielle de la vie, en
divisant l'espace, en assignant au temps une fonction exclusivement
productive, en massacrant les cultures spécifiques, en défigurant le
visage de l'enfance.

{{%asterisme%}}

Nous n'avons plus le droit de nous complaire dans la représentation
d'une Multitude sans ancrages qui s'affranchirait des antagonismes de
classes --- l'interclassisme masque et justifie le rôle prépondérant
d'une avant-garde petite-bourgeoise au sein des mouvements
révolutionnaires. Nous lui opposons une autre éthique et une autre
subjectivité de combat.

{{%asterisme%}}

Notre point de vue ne peut être seulement celui des déshérités ou de la
« classe ouvrière blanche », il est d'abord celui des sous-prolétaires
du monde entier --- qui, par définition, sont relégués hors des rapports
légaux de production et d'échange, hors de toute reconnaissance
institutionnelle, matérielle et symbolique.

{{%asterisme%}}

Nous œuvrons pour la constitution d'une *classe en lutte*, en tant que
constellations de foyers insurrectionnels et de corps collectifs engagés
lucidement sur le terrain de la *lutte des classes*, et capables de
tisser, à partir des ramifications les plus souterraines des résistances
les plus silencieuses et les plus humbles --- une image de notre salut.

{{%asterisme%}}

Cette *image* n'est pas la *représentation* d'un Sujet révolutionnaire
triomphant. Le sujet révolutionnaire ne saurait se subsumer sous une
représentation homogène, et donc fausse, du mouvement révolutionnaire.
Le sujet révolutionnaire n'est jamais *déjà* constitué --- il se
fabrique sans cesse en réseaux ouverts, solidaires, ancrés.

{{%asterisme%}}

C'est une Internationale sous-prolétarienne des luttes et des
expériences concrètes de vies autonomes et contestataires.

{{%asterisme%}}

Car lutter, aujourd'hui, peut signifier simplement vivre.

{{%asterisme%}}

Quand la modernité capitaliste n'est que lumière retroussée, inversion
systématique des valeurs émancipatrices, instrumentalisation de la
Raison et des justes secousses qu'elle engendra dans l'Histoire
souffreteuse d'Occident...

{{%asterisme%}}

...culte du rendement, institutions centralisées et bureaucratiques,
barbarie policière, la modernité capitaliste a saccagé la lumière de
l'enfance, colonisé ses territoires, abruti sa sensibilité, balisé ses
audaces.

{{%asterisme%}}

C'est au nom de l'*éternelle enfance* --- invincible soleil qui
ensauvage nos joies, apaise nos angoisses --- que nous luttons.

{{%asterisme%}}

Habiter collectivement cette terre, la cultiver dans le respect de ses
lois autonomes, y aménager des espaces de vie et de partage, ne jamais
la cloisonner, résister aux sirènes de la Propriété comme aux tentations
du Savoir...

{{%asterisme%}}

...se réapproprier la technique, qu'elle soit artisanale ou machinique,
ancestrale ou numérique, refuser de choisir entre les forces de l'esprit
et celles de l'univers, libérer les puissances qui se logent en nous, en
utilisant tous les moyens dont nous disposons, matériellement et
poétiquement...

{{%asterisme%}}

...voici ce qu'il y a d'impérissable dans notre conception du
communisme.

{{%asterisme%}}

C'est un usage du monde qui n'est pas inféodé au *culte de la valeur*.

{{%asterisme%}}

Ce dernier s'appuie sur un enrichissement continu de notre pratique
théorique : de la critique interne à la modernité, allant de Spinoza à
Marx, aux déconstructions épistémologiques initiées par les mouvements
décoloniaux et les mouvements en lutte contre les oppressions
spécifiques --- dont nous pensons qu'elles peuvent soutenir le projet
originaire des modernes, tout en dépoussiérant ses abstractions, ses
angles morts, en spécifiant ses objets, en éclatant son cadre et en
repensant son sujet.

{{%asterisme%}}

Notre *communisme déjà existant* s'appuie conjointement sur un partage
perpétuel de nos expériences de lutte, une mise en commun de nos
pratiques, la déterritorialisation des espaces dédiés à la connaissance,
à la pensée et au langage.

{{%asterisme%}}

Ce *communisme déjà existant* agit également comme *sauvegarde* de nos
excédents culturels non assimilables par l'Histoire des vainqueurs.

{{%asterisme%}}

Il ne se constitue donc pas comme bloc théorique achevé et invariant.

{{%asterisme%}}

Il est ouvert à la contradiction, à la confrontation, à la différence.

{{%asterisme%}}

Il n'est pas une morale.

{{%asterisme%}}

Il n'est pas même un programme.

{{%asterisme%}}

Il trace un sillon dans le brouillard de l'époque.

{{%asterisme%}}

C'est une invitation.
