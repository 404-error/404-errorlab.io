---
persona:
  - Esperanza Rojo
title: "Triptyque Abysses"
slug: "triptyque-abysses"
date: 2020-10-10
echo:
  - rythmique
images:
  - "img/201010-rojo-triptyque-abysses.jpg"
gridimages:
  - "img/201010-rojo-triptyque-abysses-grid.jpg"
sourceimage: "https://www.flickr.com/photos/nlireland/9713428703/"
notes: ""
summary: "Notre image est une stridence, est une révolte, est une langue qui se glisse en la prise, l’électrique, en prise d’irréel, s’en prend, s’éprend de l’image, s’y déprend d’amour, de fol amour, ses surfaces d’œil, vingt-quatre fois, et se précipite, précipices de langue vers la flamme des représentations."
citation: "Notre image / Est une stridence, / Est une révolte, / Est une langue"
---

Notre image\
Est une stridence,\
Est une révolte,\
Est une langue\
Qui se glisse en la prise,\
L’électrique, en prise d’irréel,\
S’en prend, s’éprend de l’image,\
S’y déprend d’amour, de fol amour,\
Ses surfaces d’œil,\
Vingt-quatre fois, et se précipite,\
Précipices de langue\
Vers la flamme des représentations.\
Prisme sur consumations.\
Elle épingle la nuit du papillon,\
Place sa collection d’imagos sur le brasero.\
Rêve : naphtalène et palmitate.\
Incandescence des sous-peuples.\
Quartz pour lumière,\
Phalènes pour cerbères,\
Pour impermanence, pour soulèvement.\
Mue imaginale. Déracine. Sans-racines.\
Insinue l’insane, sinue de sens,\
Aux creux des évaporations universelles,\
Elle, l’offrande,\
Elle, l’iconoclasme,\
Elle, l’internationale des regards obliques,\
Et le signifié qui baise le front du signifiant\
Avant d’y placer l’arme,\
D’y tracer la lueur.

{{%asterisme%}}

M'arrachent l'œil, l'heure et\
L'or des lèvres\
S'y écoulent d'œil, d'heure et\
D'or jusqu'à l'onde les lames\
Soulèvent le parcours parmi mes\
Souterrains, ma sous-terre qui s'y\
Écroule

M'arrachent la voix, le ventre et\
La vêpre d'emmurée\
S'y écoulent de voix, de ventre et\
De vêpre jusqu'à la seconde les pavés\
Soulèvent ma division sur le\
Septième de leur souffle, le soufre qui s'y\
Soulève

M'arrachent l'aile, l'air et\
La nervure d'écume\
S'y écoulent d'aile, d'air et\
De nervure jusqu'à la muraille les guerres\
Soulèvent ma charité d'armes mais\
J'habite le faisil, encore le château, l'unique rage qui s'y\
Sépare

{{%asterisme%}}

Balade meurtre\
Sa chaleur épaisse\
Presse enserre l'émiette\
Dêmos démocratie\
Crime ou criminelle\
Asile alyssum\
Lobularia maritima\
Globularia naufragia\
L'odeur du miel\
Azazel\
Et l'azuré macchabée\
Sphères transferts\
Frontières d'eau\
Léviathan lévitique\
Qui s'éboule\
C'est l'ébullition d'être\
L'étrangère est la noyée\
L'être la noyade la Llorona\
L'escarpe sur\
Sous faussée\
Sous phocéenne\
Terrienne des sources\
Terre rien\
D'identitaire rien\
D'identique sang\
Charogne chers fétus\
Les mers et les tourismes\
Armes larmes noires\
Sur d'os fragiles\
Fractures\
Contre civilisation\
Expiation contre\
Ce petit bruit de l'heure\
Le cliquetis et les abysses
