---
persona:
  - Ann Persson
title: "ensauvagement"
slug: "ensauvagement"
date: 2022-03-23
poetry: true
hangindent: true
echo:
  - rythmique
images:
  - "img/220303-persson-ensauvagement.jpg"
gridimages:
  - "img/220303-persson-ensauvagement-grid.jpg"
sourceimage: "https://commons.wikimedia.org/wiki/File:Wattsriots-policearrest-loc.jpg"
notes: ""
summary: "m'ont dit nègre-négresse / kkk'entends-je moi nègre-SS / messe-de-triple-entente / et Q-reton sans tripes / en la porte la chapelle / kkki baise d'ensauvagement / afrikkka tu as vrillé ma sève / d'héroïne kkkouleur la brune / shoot- / shooteuse / schutzstaff’ de la kkkapitalosaynète / m'ont kkkru- / kkkrucifiée tête en bas / kkkomme sainte pierre / kkkoi dust d'host- / d'hostie / kkkoi d'autre distille l'hostile / l'eskkkort / sous sakkk plastikkk / osef ma gueule / pulvis pulverem / pulvérise et fixxx"
citation: "la moula la molaire les vers et les chimères"
---

m'ont dit nègre-négresse

kkk'entends-je moi nègre-SS

messe-de-triple-entente

et Q-reton sans tripes

en la porte la chapelle

kkki baise d'ensauvagement

afrikkka tu as vrillé ma sève

d'héroïne kkkouleur la brune

shoot-

shooteuse

schutzstaff’ de la kkkapitalosaynète

m'ont kkkru-

kkkrucifiée tête en bas

kkkomme sainte pierre

kkkoi dust d'host-

d'hostie

kkkoi d'autre distille l'hostile

l'eskkkort

sous sakkk plastikkk

osef ma gueule

pulvis pulverem

pulvérise et fixxx

ma déveine

mate la galette des kkkamgirls

les veuchs en kkkrebs

kkkrépus juskkk'auxxx enfers

sommes toujours nègre la SS

de kkkelle kkke soit l'ékkkorchure le kkkôté

par la kkkôte la pendue

à ce sky d'extinkkktion

m'ont-iels

kkkoikkke-t-iels

m'ont montée

les stars éteintes

ô ter-ter les astikkkots

kkk'iels m'astikkkotent

takkktiles ou teasent

par mon Q

l'ânonnent les trépanent panés

d'ragots d'rageuxxx

plus babtous kkk'la blanche

d'rage against

les narines chargées

décharge-gégène

mauvais gène sur front-

tierce de thermobarikkk

kkk'iels m'ont mise sous darkkk markkket

de la main l'invisible

pour te hang par la strange

’kkkolter les fruits

et aryen faire du spekkktr-

ou takkkle marxxx ou kkkrève ma michetonne

blackkk blokkk des petites gens

n'y songe pas

the library is open bitch

déjà la fosse kkkreusée

pour trans tranchée

kkkhala-SS

ma langue de tapis de prière

de kkkalach

de schlass

de passe-passe

avekkk le kkkanon du rif

scié

délikkkatement

sur ma tempe pose-posé

vois kkkomme iels me voient

avekkk mon sexxxe fendu des ciels

kkkokkk-

tail martov

blackkk powder trace l'horizon le trou noir

longtemps kkke les exxxé-Q-tions publikkks

plus publikkks

pas plus kkke

les pavés sous les filles

les gardiens ennuyés

privatisation du deep state

mon publikkk

je te souris

sans dents

kkkause les matrakkks

lbd'oeil

la politikkkaï

iels reviendront avec leurs kkklebs

cent fois

iels m'arracheront les doigts

grenade du neuvième désencerkkklement

’vant je sombre la fortune

mais nous sachions l'aiméxxx

à la kkkestion

ne dire kkki-les-kkklandés

kkkoi-les-bombes

’volution désastres

rév’ kkki l'ombre

et des peuples juskkk'à la kkkeue et les kkkamps

silence des kkkaves

toi ma pisse d'odeur d'espace kkklos

kkklose la maison cernée par paras

flash la memory

et l'âge des passeports-sous-citoyens

et les sous-meufs-sous-sous-les-citoyens

mâle sou-

venir sous porte-manteauxxx

les menottes les manches

s'emmanche

l'électrikkk parce kkk'à oilpé

derme deumer

horror sur marchandise

pas kkkomme il fauxxx faudrait

fait-il enkkkore chambre froide au matin

vitamine et sulfat-

teuf teuch tête sous skkkalp

faf faf faf

kkkantikkk des fusils

odeur d'huile

bris des perkkkises

dans d'exxxception la sentence

et tout kkkontre le mur

enkkkore de peu ou de peur

malgré les sillons malgré les immondes

cirkkkon-

volutes d'okkktobre

sixxx o'kkklockkk dans la matière blackkk

’venue grise

cerve-servile-d'elle

et parmi

la moula la molaire les vers et les chimères

kkkouve kkkouvre le euf et l'inkkkonnu

’gnore tout kkkomme kkkosme kkkosmos

t'as l'idée sous drapeau rouge

kkkrâne kkkontre gun

peloton.exe

font kkklickkk sur

bye, felicia
