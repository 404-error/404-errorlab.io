---
persona:
  - Marcel Mauss
title: "Les origines de la notion de monnaie"
slug: "les-origines-de-la-notion-de-monnaie"
date: 2021-01-07
echo:
  - anthropique
images:
  - "img/210107-mauss-les-origines-de-la-notion-de-monnaie.jpg"
gridimages:
  - "img/210107-mauss-les-origines-de-la-notion-de-monnaie-grid.jpg"
sourceimage: "https://commons.wikimedia.org/wiki/File:Edward_S._Curtis,_Kwakiutl_bridal_group,_British_Columbia,_1914_(published_version).jpg?uselang=fr"
notes: "Ce texte de Marcel Mauss est issu d'une communication prononcée à l'Institut français d'anthropologie lors de la séance du 14 janvier 1914."
summary: "En premier lieu, il est bien entendu que nous parlons ici de la notion de monnaie. La monnaie n'est nullement un fait matériel et physique, c'est essentiellement un fait social ; sa valeur est celle de sa force d'achat, et la mesure de la confiance qu'on a en elle. Et c'est de l'origine d'une notion, d'une institution, d'une foi, que nous parlons. En second lieu il ne s'agit pas de montrer une origine, c'est-à-dire un commencement absolu, une naissance pour ainsi dire ex nihilo. Contrairement à l'idée reçue, vous verrez en effet qu'il n'est pas certain qu'il y ait eu, parmi les sociétés que nous connaissons ou que nous nous représentons par hypothèse, aucune qui fût complètement démunie de notions au moins analogues à celle que nous désignons pratiquement maintenant sous le nom de monnaie."
citation: "la monnaie n'est nullement un fait matériel et physique, c'est essentiellement un fait social"
---

Je ne puis, après les instructions de M. le Président, entrer dans de
longues considérations --- telles qu'elles seraient nécessaires --- sur la
définition de la monnaie, et sur la façon dont, à mon sens, on pourrait
traiter des origines de cette notion. Je vais, cependant, avant de
passer aux faits et aux hypothèses que ces faits m'ont suggérées, me
permettre de vous donner quelques indications préliminaires.

En premier lieu, il est bien entendu que nous parlons ici de la notion
de monnaie. La monnaie n'est nullement un fait matériel et physique,
c'est essentiellement un fait social ; sa valeur est celle de sa force
d'achat, et la mesure de la confiance qu'on a en elle. Et c'est de
l'origine d'une notion, d'une institution, d'une foi, que nous
parlons.

En second lieu il ne s'agit pas de montrer une origine, c'est-à-dire
un commencement absolu, une naissance pour ainsi dire *ex nihilo*.
Contrairement à l'idée reçue, vous verrez en effet qu'il n'est pas
certain qu'il y ait eu, parmi les sociétés que nous connaissons ou que
nous nous représentons par hypothèse, aucune qui fût complètement
démunie de notions au moins analogues à celle que nous désignons
pratiquement maintenant sous le nom de monnaie. Nous ne cherchons donc
pas ici comment est survenue tout d'un coup dans l'humanité une idée
de monnaie qui lui aurait été d'abord étrangère. Nous cherchons sous
quelle forme la plus primitive, la plus simple, la plus élémentaire pour
mieux dire, on peut se figurer que s'est présentée, dans les sociétés
les plus basses que nous connaissions, la notion de monnaie.

Naturellement il ne s'agit ici que d'hypothèses, d'indications de
travail, de données provisoires. Mais une réunion comme la nôtre, si
amicale, a précisément pour but de nous permettre de nous communiquer
ces idées ébauchées à peine, ces preuves tout juste entrevues et encore
insuffisamment mûries dont se nourrit un travail scientifique en voie de
réalisation.

{{%asterisme%}}

Je travaillais, il y a environ quatre ans, sur les beaux documents que
les missionnaires allemands au Togo ont publiés sur les langues et les
nations ewhé de ces régions. Je ne me préoccupais à ce moment nullement
de la question des origines de la notion de monnaie. Sur ce sujet je ne
connaissais d'ailleurs que l'excellent petit livre du regretté
Schurtz[^1], plein de faits, sinon d'idées. Et si j'avais eu à me
préoccuper de la définition des phénomènes économiques, de la notion de
valeur et de celle de monnaie en particulier, je n'avais jamais fait de
ces questions un objet particulier de mes recherches.

C'est en lisant les documents ewhé, en maniant les textes traduits de
M. Spieth et le dictionnaire de M. Westermann que les hasards de
quelques remarques m'ont fourni l'hypothèse que je vais vous
présenter.

J'étudiais en particulier la notion de *dzó* équivalente à celle de
*mana*, qui est celle du pouvoir, des substances, de l'action magiques
chez les Ewhé. Et, parmi les dérivés du radical *dzó*, je trouvai dans
le dictionnaire de Westermann[^2], le mot *dzonú* (*Zauber ding*), chose
magique. "Toute sorte de perle, ou de chose en forme de perle, etc."
C'était un des noms des cauris d'ailleurs si utilisés dans la magie et
la religion[^3] des nations nègres en général.

Autour de ce fait, d'autres faits cristallisèrent très vite et qui
formèrent une sorte de système. En voici quelques-uns qui se rapprochent
comme d'eux-mêmes.

La notion de *mana* en Mélanésie, est directement reliée a la notion de
monnaie[^4]. Aux Îles Banks et à Santa-Cruz, on appelle *rongo*
(sacré rougé), la monnaie de coquillages qui ailleurs porte le nom de
*diwarra*[^5].

Un autre exemplaire de la notion de pouvoir magico-religieux, c'est la
notion de *manitou* (plus exactement *manido*) chez les Algonquins. Or
le P. Thavenet[^6] dit textuellement que les perles des trafiquants
étaient pour les Algonquins (Sauteux probablement) les écailles d'un
poisson *manitou*[^7].

Ailleurs la notion de monnaie est alliée à la notion plus précise de
sacré. En Nouvelle-Guinée, comme dans l'archipel Bismark la monnaie,
gardée dans les maisons des hommes, porte le titre de *tambu*. Il y a
sur ce point un ancien travail de Schurtz[^8].

Ailleurs elle est plus nettement en rapport avec la notion de talisman.
C'est le cas en particulier dans les tribus du nord-ouest américain, et
en particulier chez les Kwakiutl, où le nom de *logwa* talisman, être et
objet surnaturel en particulier, était le vrai nom des paraphernalia des
clans, couvertures et cuivres blasonnés, véritable monnaie utilisée au
cours des potlatch, de la série des échanges de clan à clan[^9]. Or
le sens primitif du mot *Lōgwa* se rattache à une racine *Lōg<sup>u</sup>*, que M.
Boas traduit par pouvoir surnaturel[^10].

En tous ces cas le caractère religieux et magique de la monnaie était
fort accusé et dans nombre de populations la notion de monnaie se
rattachait nommément expressément à celle de pouvoir magique.

Depuis nous avons poursuivi nos recherches et --- est-ce esprit de
système ? --- nous n'avons guère trouvé de société, suffisamment proche
des origines, où le culte et la magie des pierres, des coquillages, des
métaux précieux n'aient donné une vraie valeur à ces objets. Les usages
religieux de l'or dans l'antiquité, les lapidaires qui firent le tour
des civilisations de l'Ancien Monde, le nom de la perle en arabe,
*barakā* (bénédiction = *mana* bon), tous ces faits se pressent et sont
trop connus pour que nous y insistions.

Mais descendons plus bas dans l'échelle des sociétés. Nous avions été
depuis longtemps frappés de l'importance qu'ont prise dans un très
grand nombre de sociétés très primitives ou très civilisées, les
cristaux et en particulier les cristaux de quartz. Nous avions attiré
déjà l'attention sur les faits qui concernent l'acquisition de ces
cristaux par les magiciens australiens[^11]. Depuis, dans un très
mauvais livre, dans le récit, ancien il est vrai, d'une rencontre entre
une vieille sorcière et un lieutenant de vaisseau anglais en
voyage[^12] nous avons trouve confirmation de notre hypothèse, de la
raison pour laquelle les cristaux décomposant la lumière s'étaient
imposés à l'imagination primitive : de l'eau passée au feu et devenue
solide et froide, voilà un des premiers mystères que l'homme ait
rencontrés. Nous parlons nous-mêmes, de notre siècle, comme la vieille
sorcière du Bas Murray.

Mais faisons abstraction de cette anecdote et de cette hypothèse.
N'est-il pas frappant que le mythe du quartz, de la montagne de quartz,
source de talismans[^13] se retrouve au nord-ouest américain presque
dans des termes équivalents à ceux où on le trouve en Australie ?

Et d'autre part nous avons en Australie non seulement des faits qui ont
leur équivalent à ces faits d'ordre purement magique et religieux, mais
aussi à ces faits économiques. D'abord, le commerce de ces pierres de
quartz et d'autres talismans nous est attesté tout comme leur valeur.
Ainsi, chez les Aruntas, MM. Spencer et Gillen ont constaté l'usage des
*lonka-lonka*, de grands coquillages provenant du golfe de Carpentarie,
et où est censé descendu le tonnerre[^14]. Le mot *lonka-lonka* est
d'ailleurs un mot du sabir européen, et veut dire loin, loin[^15].

Et, fait plus remarquable encore, dans ces mêmes tribus, ce ne sont pas
seulement ces talismans magiques qui sont objets de commerce, mais aussi
les emblèmes sacrés des individus, les *churinga*[^16] sont objets
d'échange. Et nous avons la preuve qu'il faut voir non seulement des
faits religieux mais aussi des faits économiques dans les pèlerinages
avec échange et commerce de ces emblèmes totémiques dont Spencer et
Gillen nous ont donné des descriptions mouvementées[^17] ; ces
visites entraînent de nombreuses prestations : nourriture, jouissance des
femmes, etc., ou bien elles sont faites à leur occasion[^18]. Mais il
y a plus, un autre témoin que MM. Spencer et Gillen, M. Eylmann, nous
dit expressément, et sans l'ombre d'une idée préconçue, que les
*churinga*, les objets sacrés, car tel est le sens du mot, servent de
mesure de valeur dans ces tribus[^19]. Il raconte une anecdote où ses
guides, provenant de nations très distantes, lui dirent spontanément que
c'était là "l'argent des noirs[^19bis]".

Cela est, peut-être, le biais par où on peut se représenter les formes
primitives de la notion de monnaie. La monnaie, --- quelle que soit la
définition qu'on adopte --- c'est une valeur étalon, c'est aussi une
valeur d'usage qui n'est pas fongible, qui est permanente,
transmissible, qui peut être l'objet de transactions et d'usages sans
être détériorée, mais qui peut être le moyen de se procurer d'autres
valeurs fongibles, transitoires, des jouissances, des prestations. Or
le talisman et sa possession ont, quant à nous, très tôt, sans doute dès
les sociétés les plus primitives, joué ce rôle d'objets également
convoités par tous, et dont la possession conférait à leur détenteur un
pouvoir qui devint aisément un pouvoir d'achat.

Mais, au surplus, n'y a-t-il pas là quelque chose qui tient à la
nature des sociétés ? --- Prenons un exemple. Le mot de *mana* dans les
langues malayo-mélanéso-polynésiennes désigne non seulement le pouvoir
des substances et des actes magiques, mais aussi l'autorité des
hommes[^20]. Il désigne également les objets précieux, les talismans
de la tribu[^21], dont on sait de quels échanges, de quelles
batailles, de quels héritages ils furent l'objet. Qu'y a-t-il là
d'irrationnel, si nous savons nous représenter l'état d'esprit dans
lequel ces institutions ont fonctionné. La force d'achat de la monnaie
n'est-elle pas naturelle, quand elle est attachée au talisman qui, à la
rigueur, peut contraindre les subordonnés des chefs, les clients des
magiciens aux prestations qu'ils leur demandent ? Et, inversement, n'y
a-t-il pas nécessité, dès que la notion de richesse intervient, sous une
forme si vague que ce soit, que la richesse du chef et du magicien
réside avant tout dans les emblèmes qui incarnent leurs pouvoirs
magiques, leur autorité en un mot, ou qui symbolisent la force du clan ?

Schurtz remarque d'ailleurs très finement[^22], après Kubary qui
avait fait l'observation dans les Îles Palaos[^23], que l'argent
ne fut pas primitivement employé à l'acquisition des moyens de
consommation, mais à l'acquisition de choses de luxe, et à celle de
l'autorité sur les hommes. Le pouvoir d'achat de la monnaie primitive
c'est avant tout, selon nous, le prestige que le talisman confère à
celui qui le possède et qui s'en sert pour commander aux autres.

Mais n'y a-t-il pas là un sentiment encore très vivace chez nous ? Et
la vraie foi que nous nourrissons vis-à-vis de l'or et de toutes les
valeurs qui découlent de son estimation, n'est-elle pas en grande
partie la confiance que nous avons dans son pouvoir ? L'essence de la
foi en la valeur de l'or ne réside-t-elle pas dans la croyance que nous
pourrons obtenir, grâce à lui, de nos contemporains les prestations --- en
nature ou en services --- que l'état de marché nous permettra d'exiger ?

Telles sont, Messieurs, les quelques réflexions que je puis vous
présenter avec toutes les réserves que comportent de simples hypothèses
de travail, d'un travail auquel je vous prie de collaborer par vos
renseignements et par vos critiques.

[^1]: *Grundiss einer Entstehungsgeschichte des Geldes*, Weimar, 1898.

[^2]: *Wörterbuch der Ewhe Sprache, I. Ewe-Deutshe, Wörters*, p. 93.

[^3]: Que les cauris (*hotsui*) aient été avant tout des ornements talismans, c'est ce que prouve le fait que les colliers de cauris *ne sont portés que* par les prêtres, magiciens et enfants jumeaux des prêtres et magiciens (v. Westermann, *Ewe-Deut.*, p. 230, col. 1 s. v. *hotsui to-to*).

[^4]: Codrington, *The Melanesians*, p. 103, etc.

[^5]: Ibid., p. 325, sq.

[^6]: Tesa, *Studi del Thavenet*, Pise, 1881, p. 18.

[^7]: Le mot de mi'gis qui désigne les talismans, et plus spécialement les grandes écailles est aussi synonyme de perles (cf. Hoffman, *The Mide wiwin of the Ojibwa*, VIIth Ann. Rep. of. the Bur. of Ethno, 1891, p. 215, p. 219, p. 220). D'ailleurs le P. Cuoq (*Lexique de la langue algonquine*, p. 220), identifie *mikis à wampum*, le collier-monnaie des Iroquois.

[^8]: *Preuss. Jahrbücher*, 1895, p. 50 sq.

[^9]: Cf. par exemple F. Boas, *The Social Organization and the Secret Societies of the Kwakiutl*, 1897, les 3 vers de la p. 373 ; cf. *Kwakiutl Texts*, Memoirs of the Am. Mus. Nat. Hist. Jesup Expedit. 1re série, I, p. 355, l. 18--19. M. Boas a varié de graphies *Lōk*, *ļōg*, mais c'est le même mot et ce sont les mêmes choses qu'il désigne.

[^10]: V. Boas et Hunt, *Kwakiutl Texts*, III, p. 527, cf. I, p. 78, l. 2.

[^11]: Hubert et Mauss, *Mélange d'histoire des religions*, p. 155, p. 167 suiv.

[^12]: Leigh, *Reconnoiteing Voyages, in the New colonies of South Australia*, Londres, 1839, p. 160.

[^13]: Cf. Boas, *Social Organization*, p. 405 ; Boas et Hunt, *Kwakiutl Texts*, I, p. 111, 15, 20, 2<sup>e</sup> série, p. 29, l. 25--30, etc.

[^14]: V. entre autres la formule arunta, *Native Tribes of Central Australia*, p. 545, la formule est mal traduite, il y est sûrement parlé du tonnerre dans l'eau.

[^15]: Cf. Kempe, *Vocabulary of the Tribes inhabiting the Macdonnell Ranges*. Transact. Royal Society of South Australia, XIV. s v.

[^16]: Sur lesquels voyez Durkheim, *Formes élémentaires de la Vie Religieuse*, p. 168 sq.

[^17]: *Nat. Tribes*, p. 159 sq., *Northern Tribes of Central Australia*, p. 259 sq.

[^18]: Cf. la formule arunta, *Northern Tribes*, p. 263.

[^19]: *Die Eingeborenen Süd-Australiens*, 1908, p. 179.

[^19bis]: Ibid., p. 201.

[^20]: V. Tregear, *Maori Comparative Dictionary*, s. v.

[^21]: Cf. par ex. les textes traduits par Percy Smith, *The Aotea Canoe*, Journal of the Polynesian Society, IX, 1900, p. 220, en haut.

[^22]: *Entstehungsgeschichte*, p. 19.

[^23]: *Etnogr. Beiträge*, p. 9.
