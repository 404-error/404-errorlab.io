---
persona:
  - Lucien Raphmaj
  - Marc Verlynde
title: "À te souvenir de l'insomnie des mondes"
slug: "a-te-souvenir-de-linsomnie-des-mondes"
date: "2022-03-03"
echo:
  - esthétique
images:
  - "img/220302-lrmv-a-te-souvenir-de-linsomnie-des-mondes.jpg"
gridimages:
  - "img/220302-lrmv-a-te-souvenir-de-linsomnie-des-mondes-grid.jpg"
sourceimage: "lucien raphmaj"
notes: ""
summary: "Ici on dira ce que l'on ignore. Ce qui ne nous appartient pas et ce qui appartient à chacun. En commun. Retiré et en partage. Pour tous et pour personne, disait f.n. Peut-être parlera-t-on par énigme, par image, par rebond. Drôles d'insectes ou de champignons symbiotiques et définitivement sympathiques. En compagnie de tant et tant d'espèces, de paroles contre tous les parasites, contre tous les sacrifices. Ce serait bien. Peut-être cela sera-t-il écrit trop vite. On essaiera de nouvelles formes. De pensées. De critique. Qui jamais n'essaie meurt consumé par l'essaim. On l'a fait, on le fera. On sera critiqué, on ne le sera pas. On recommencera."
citation: "la critique comme la salive de l’idiot"
---

*Ici se lit un entretien ouvert sur la critique littéraire, sur ses puissances et ses impuissances, et dont la question de l'idiotie est comme la première stase, liminaire, l'incarnation de cet état dont il nous paraissait important de témoigner comme critique en approfondissant notre perplexité.*

---

{{%droite%}}
"*Et pourtant, toujours nous choisissons un compagnon : non pour nous,
mais pour quelque chose hors de nous, qui a besoin que nous manquions à
nous-mêmes pour passer la ligne que nous n'atteindrons pas.*" ([m.b.](/desidentite/#mb), *Le dernier à parler*)
{{%/droite%}}

Ici on dira ce que l'on ignore. Ce qui ne nous appartient pas et ce qui
appartient à chacun. En commun. Retiré et en partage.\
*Pour tous et pour personne*, disait [f.n.](/desidentite/#fn)\
Peut-être parlera-t-on par énigme, par image, par[<span id="rebond">rebond</span>](/stase-seconde/#rebond).\
Drôles d'insectes ou de champignons symbiotiques et définitivement
sympathiques.\
En compagnie de tant et tant d'espèces, de paroles contre tous
les parasites, contre tous les sacrifices.\
Ce serait bien.\
Peut-être cela sera-t-il écrit trop vite.\
On essaiera de nouvelles formes. De pensées. De critique.\
*Qui jamais n'essaie meurt consumé par l'essaim.*\
On l'a fait, on le fera. On sera critiqué, on ne le sera pas.\
On recommencera.

On dira ce que l'on découvre parce qu'on a su repérer dans la lecture
un mouvement d'ignorance, de non-savoir, qu'on a appelé *idiotie* et
qui permet toutes les découvertes, tous les savoirs. Qui permet de
former des dieux en forme de "i". De petits cris de souris dans le
néant.

Il y aurait tant à dire, à faire et à défaire dans l'histoire de la
critique, de la lecture qui s'écrit à partir de ses aveuglements et de
ses obsessions. Sûrement qu'il faut dire quelque chose d'assez simple
d'abord. Avec des images simples.

*La critique de jour* --- celle que l'on fait dans le présent immédiat,
la médiation qui indique l'ici-bas de l'œuvre.

*La critique de nuit* --- malgré toutes les difficultés, les errances, la
nuit terrible, aveuglante, critique qui cherche à dire et à disparaître,
à participer et à tracer de nouvelles constellations sur terre comme
dans le ciel des mots.

*La critique crépusculaire* --- comme on le dit des espèces vivants entre
le jour et la nuit, entre la grotte et son dehors, rêve d'une parole
neutre, lunatique, parole non pas entre deux, mais "entre tous les
deux" ([j.d.](/desidentite/#jd)), *parole d'entre* : qui fait entrer en elle tous les
possibles, ou qui rêve de le faire, qui s'ouvre à cette impossibilité,
à cette vulnérabilité, voulant faire visage avec l'oeuvre.

La lecture de la littérature comme ce point de vertige ou d'inquiétude,
d'incertitude que l'on hérite comme horizon à multiplier et à déformer,
à transformer :

"*[...] cette subtile distance dans laquelle l’œuvre se réfléchit et dont le critique est appelé à donner la mesure, n'est que la dernière métamorphose de l’œuvre en sa genèse, ce qu'on pourrait appeler sa non-coïncidence essentielle avec elle-même, tout ce qui ne cesse de la rendre possible-impossible. La critique ne fait donc que représenter et poursuivre en dehors ce qui, du dedans, comme affirmation déchirée, comme inquiétude infinie, comme conflit (ou sous de toutes autres formes), n'a cessé d'être présent à la manière d'une réserve vivante de vide, d'espace ou d'erreur ou, pour mieux dire, comme le pouvoir propre de la littérature de se faire en se maintenant perpétuellement en défaut.*" ([m.b.](/desidentite/#mb), Préface à *Lautréamont et Sade*)

Dissimilation de la critique : dissimulant et simulant. Entre \|\| l'écriture. Hybride toujours d'*autres choses*. Affirmant cette
hybridité, ce mélange certain d'amitié et de solitude, de lecture et
d'écriture, de littérature et d'autre chose. De ça et d'encore toujours
ce qui échappe --- ce qui altère, la faisant étrange, idiote car posant
des questions sans avoir la réponse, sans pouvoir avoir la réponse, sans
pouvoir comprendre la réponse. Impuissance qui ouvre le dialogue avec
l'oeuvre, un dialogue aux vecteurs immenses, transformatifs, dissimulant
et simulant l'écriture.

{{%asterisme%}}

Commencer autrement, toujours. Commencer par une parole, critique, qui
interroge sa possibilité même, sa nécessité, sa viabilité, son apport,
sa spécularité. Une parole comme imposture ; paradoxe idiot. De quoi
parle-t-on exactement quand on se prétend critique, d'où parle-t-on,
pour qui parle-t-on, quelle modification de la réalité, du vécu, du
sensible et de son expérience veut-on concourir à partager ?

On répond : *idiotie*. Décalage, décentrement, déconstruction : parler
du peu que l'on comprend, admettre autrement son immense ignorance,
l'étaler non comme fierté mais comme écart. Tout ce que l'on ne sait pas
serait simplement tout ce qui n'anime aucun fantôme, ne suscite aucune
revenance, aucune peur.

Une sorte d'imposture, un contact intime avec l'aporie d'une sensibilité
personnelle. Ce n'est pas en mon nom que je pense. Pourtant, je n'en
conserve qu'une appréhension (une peur préhensible) obstinément
subjective, si singulière qu'elle en deviendrait objective, commune par
idiotie.

Clownesque idiotie : au fond on recherche quelle langue on pourrait
écouter pour qu'elle puisse prétendre parler à notre place, mieux que
nous, idiots que nous sommes. On pourrait même esquisser un paradoxe :
quel insupportable apaisement apporte la critique ?

On veut des livres qui nous fassent sentir profondément idiots. Pas des
livres dont on reprendrait les défauts, pointerait les défaillances, où
l'on se sentirait être en sachant mieux que l'auteur ce qu'il aurait dû
dire, comment il aurait dû déconstruire paroles et soupirs. Non. Des
livres qui, par-delà l'esthétique, la réception, l'adhésion qui sait,
soudain signeraient, saisissants raccourcis, l'ombre de ce que nous
sommes, l'idiote imposture de ce que nous manifestons. Ou plus
idiotement encore : le rappel à l'évidence, au dénuement : que tu es con
avec ta prétention à mirer le monde, à y chercher le reflet de
l'inquiète transparence de ce fragment de sensibilité collective.

{{%asterisme%}}

Re-commencer, autrement. Toujours et autrement.\
On l'a dit ailleurs. On le redira. Toujours et autrement. Je l'ai
emprunté à d'autres langues. *[<span id="siempre">Explícitamente</span>](/ligne-dombre/#siempre)*.\
Mentalement, il faut être ailleurs. Être capable de se réinventer en
littérature, en critique, en théorie.\
Surtout en pratique.\
Parce que tout se lie. Les philosophes disent : ce sont des puissances
qui se lient. L'imagination s'emballe. On se croit magicien, magicienne,
enfin quelque chose de sorcier ayant oublié toutes ses formules.\
Ce n'est rien de tout ça. Au contraire.\
C'est ce qui nous retient et nous étoile.\
C'est difficile à dire.\
"Beau et difficile", disait [b.s.](/desidentite/#bs) On a souvent ça au cœur en écrivant.
Des petites phrases comme un *mémorial* à soi. Invisible. Qui ne
saignera pas.\
Des petits secrets. La phrase de [m.b.](/desidentite/#mb) (reprise de [m.h.](/desidentite/#mh)) sur la critique : *de la neige sur une cloche*. On imagine la critique comme la salive
de l'idiot sur la cloche, heureuse de voir la [<span id="neige">neige</span>](/desidentite/#neige) du commentaire faire
si peu de bruit en tombant sur la cloche. Fondre.\
Qu'importe.\
On le dira souvent.\
Se tromper est important.\
Qu'est-ce que la personne faisant de la critique sinon quelqu'un qui se
trompe ?\
Qu'est-ce, sinon quelqu'un qui a fait de l'erreur sa voie créative ?\
Errer, étoiler les perspectives, multipliant les lignes de fuites, voilà
son ciel théorique ne cessant de se métamorphoser.\
Souvent on c'est ça qu'on a en tête en écrivant, [g.d.](/desidentite/#gd), et ça vaut pour
la critique : quelle ligne de fuite je propose moi, quel horizon
s'inventera tandis que le mot se tord, que le cerveau se déforme à la
lecture ?\
À ce moment-là on fait le contraire d'un paysage. On se dépayse. C'est
donc ça aussi que doit être la critique ? Dépaysement. Oui, peut-être,
mais aussi une façon de *faire visage*. Dépayser, envisager.

Quand a-t-on découvert cette espèce que l'on appelle la critique ?\
Quel jour malformé de notre mémoire l'a illuminée d'un sourire idiot ?\
Je ne sais pas.\
Il y en a pour juger, d'autre pour "rendre grâce" ([m.n.](/desidentite/#mn)).\
L'expression est belle. Peut-être n'ose-t-on plus parler de grâce parce
que nous sommes tous et toutes athées, même les gens religieux --- telle est
la leçon du nihilisme. Peut-être est-ce une erreur.\
Notre grâce a plus de profondeur d'être habitée par tant de vide, de
tant se hâter vers sa fin.\
J'aime ce titre indomptable du recueil critique de [c.c.](/desidentite/#cc) : *Les
impardonnables*.\
Il y va de quelque chose, d'un geste d'un même mouvement *innocent et
impardonnable*. Et beau. Sincère et impardonnable.\
Livré à même l'intelligence.\
Donné de plein cœur.\
Même s'il faut en mourir.\
Quelque chose de la critique comme un acte excessif, absolu,
intransigeant qui m'a fait tant aimer [m.b.](/desidentite/#mb) Qui l'a tant fait détester.

"*La passion de la perfection arrive tard. Ou plutôt , c'est sur le tard
qu'elle se manifeste en tant que passion consciente. Si elle fut d'abord
une passion spontanée, et si toute vie connaît un jour "l'horreur
générale", face au monde qui meurt et se désagrège, cet instant fatal la
révèle à elle-même : réponse sauvage et digne*." ([c.c.](/desidentite/#cc), *Les
impardonnables*)

Sauvage et digne.\
Belle ligne de conduite. Car nous parlons ici d'éthique. De ce qui
inséparable de la critique. Et de l'amitié. De la théorie et de la
fiction. D'une idiotie sauvage. D'un entretien avec la fin. Avec ce qui
se termine avec la "critique littéraire".\
*(Note à nous-mêmes : faudra-t-il énumérer tout ce qui lie* la "mort"
du roman, *du
critique et de la littérature --- horrible trinité mal faite, duplicité
innomée, divinité morte, raidie dans son costume en papier acide du
19^e^ siècle à la conscience malheureuse, allez, adieu, adieu ! nous
allumons de grands feux, des contre-feux, qui de loin en loin, de
solitude en solitude, nous rendent sauvages et dignes.)*\
Continuer là, alors qu'il faudrait briser.\
Dire la parole et sa maîtrise, son dévers.\
Innocents bien qu'impardonnables.\
C'était beau. Suffisant et pourtant disant notre insuffisance en tant
que critique. Humilité dont l'idiotie témoigne.\
Innocents et impardonnables.\
Devant se donner rien de moins que l'absolu, que la justesse sèche de la
lame qui tranche la tête des saints --- et on dirait que je dis ça comme
~~[m.l.](/desidentite/#ml)~~ dans *L'âge d'homme*,
fascination de Judith coupant la tête d'Holopherne, ou celle de la
décapitation de saint Côme et saint Damien de [f.a.](/desidentite/#fa) dans le *Toucan
fantôme* d'[e.m.](/desidentite/#em)\
Parler sans tête.\
Idiote et tragique beauté.\
Intimité avec l'œuvre.\
Quelque chose oscille ici alors dans toutes les têtes, la grimace de
[g.b.](/desidentite/#gb) forme un sourire qui se dit : voilà le rat, voilà le rat qui révèle
sa nature. Le voici fait. Peut-être pas.\
Peut-être me suis-je perdu, oui. Un temps j'ai été la bête innocente qui
sourit.\
Celle-là.\
"La bête innommable".\
Il faut la devenir.\
Présentée depuis le fond des avenirs à inventer.\
Ainsi soyons souris.\
Alors faudrait-il élargir l'espèce que forme la critique. Lui donner ses
hiboux et ses souris, ses sirènes et ses lamies. Non. Ça ferait trop
bestiaire. Pourtant. Cette part sauvage de la critique, cette part
idiote et animale, c'est à elle que nous voulons nous mêler. C'est à
elle qu'il faut venir. Revenir ? Spectralement, oui. Encore, oui. D'une
parole éternellement affirmative.\
Nous avons à retirer à la critique-méduse son regard de pierre et à lui
rendre ses vipères et ses pierres réchauffées par le Soleil.

{{%asterisme%}}

Un critique médusé ? Bête bestiaire, s'y noyer.
Peut-être est-ce que nous voulons dire, de concert ou en gracieuse
syntonie, dans cette parole enfin plurielle, seconde, dans ce dialogue
de sourds : la critique vaut par redéfinition.

C'est "comme effort d'autonomie". ([g.b.](/desidentite/#gb)) Plus personne (athées hantés
que nous sommes par la grâce de la matière) ne peut aujourd'hui se
croire maître et possesseur de la nature, dominer par un esprit
cartésien les espèces ainsi classées, enserrées dans les limites d'une
taxinomie. Nous sommes d'idiotes éponges, sans surplomb, sans limites
non plus, parcelles perceptives ; nous espérons des altérations de la
compréhension. L'eau se trouble, on croit comprendre ; la vague reflue,
elle ensable l'espoir de la [<span id="vague">suivante</span>](/ligne-dombre/#vague). On aurait des compréhensions
lunaires, versatiles.

Alors surnage l'utopie d'une critique [<span id="spiralaires">spiralaire</span>](/stase-seconde/#spiralaires). Par excès de
savoir, de références, par cette dissipation qui caractérise aussi son
support essentiel (ce réseau où elle déploie ses constellations), la
geste critique doit repartir d'une ignorance non feinte, d'une sorte
d'humilité dans la sympathique sidération de la lecture où enfin se
confondre. S'absenter, exister.

On pourrait le dire le plus idiotement possible : il ne faudrait
parler des textes que lorsqu'il nous coupe la langue, contraignent et
expriment ce silence primal, touchent à la confusion. Non tant ne plus
savoir ce que l'on devrait en dire mais ce que l'on y a trouvé, rêvé,
déformé, altéré, admiré. Pas ce qui nous parle mais ce qui parle, parle
et prophétise, sidère et surpasse, un nous-mêmes qui serait notre part
de rêve, océan d'un inconscient collectif où nous appelle, sourdes
sirènes, celui qu'on aurait pu être, celui qui existe seulement dans le
dévoilement. Qui, sans doute, se dit détour et erreur, dédoublement. Un
"on" tout de partage et d'effacement.

On pourrait le dire plus idiotement encore, *por el analfabeto a
quien escribo*. Critique spiralaire, le partage d'une lecture qui
inventerait une parole seconde, qui se mord la queue, dessine anneau de
Moebius et anamorphose. Critique d'une critique, il en reste la parole
autre. "Je n'ai en moi, ou devant moi, ni Dieu, ni être : mais
d'incertaines *conjonctions*." ([g.b.](/desidentite/#gb)) La seconde guerre mondiale ;
*Aminadab et Le coupable*, invention par la réponse de l'essai
[<span id="troublement">troublement</span>](/ligne-dombre/#troublement) vécu à la fiction d'un trouble. Pas miroir ni modèle ; un
exercice d'admiration, d'amitié.

La parole, un dévoilement dérobé. Dis-moi qui tu hantes, je te dirai qui
tu es assénait [a.b.](/desidentite/#ab) Une critique vaut par les hantises qu'elle met à
jour, qu'elle partage. Pendant la guerre, [g.b.](/desidentite/#gb) lit [m.b.](/desidentite/#mb), une rencontre
s'esquisse, un dialogue naît. Il se construit sur la perpétuelle
imminence de la révélation, l'importance du dévoilement, (incertain
comme l'avenir, trouble comme ce qu'on ne saura, idiotement, en faire)
dans le nouveau statut de réalité à perpétuellement inventer, à
ressentir comme expérience intérieure. On commence par un voile, cette
idiote révélation de la transparence de nos irréfragables obscurités ;
critique, idiotie : une méduse. On flotte, on lance des filaments, on
renaît, sans cesse, on touche la côte avant qu'une vague nous repousse.
On aura la grâce de n'avoir ni fin ni finalité. Une seule éthique
(plurielle et pleine de métamorphoses) : partagé le peu que l'on
ressent, le peu qu'en commun on touche de nos tentacules. Savoir qu'à
côté de nous continue à parler, penser, écrire, ce camarade qui trouvera
ce que l'on cherche à exprimer, qui en relancera les mutations par un
mot ou un son, l'ombre voilée d'une idée, en relancera autrement
l'expression, dans cet inachèvement "la coïncidence d'une plénitude
intellectuelle et d'une extase" ([g.b.](/desidentite/#gb)), impardonnablement coupable
d'être innocente, idiote, critique.

{{%asterisme%}}

Un flottement, une idée.\
Ce n'est pas la dernière fois que l'on se trompe, ayant fait
profession de cette parole errante.\
Cela ne changera pas.\
Ni le monde ni son jugement.\
La critique et la fin du monde.\
La parole de crise : hyperventilation de la surmodernité.\
Et la littérature dans tout ça : encore du vent.\
Ce serait déjà bien.\
Ce n'est pas du tout ça.\
Et pourtant c'est de tout cela que l'on parle.\
Pas de chiffres de vente. De la postcritique --- et après quoi ? après
tout ces "après", que reste t-il ? somme négative de la critique.\
Rien à dire des Kilo-likes.\
De l'ontothéologie praxistique.\
On parle de la parole en crise.\
En crise parce qu'en littérature.\
Aussi.\
Mais il y a une critique à l'encéphalogramme presque plat.\
Il y en a plein.\
Critique parasitique pullulant sur le cadavre de la littérature faite de
cette seule idée : «&nbsp;La littérature est quelque chose
dont on doit parler sinon elle n'existe pas.&nbsp;»\
Cette idée je la connais.\
C'est celle de Dieu.\
Crois en Dieu et il existe.\
Cesse d'y croire et le Néant apparaît librement, comme un livre ouvert
fait du Ciel.\
Voilà.\
D'aussi loin que l'idiotie me laisse.\
Sentiment heureux de disparition.\
Il y a un beau livre là-dessus écrit par [p.z.](/desidentite/#pz) et sous-titré : "l'art de
disparaître".\
Il cite Maître Eckhart. Idiot devant Dieu, par-delà le tsimtsoum et le
tiqqun.\
*Apathéia*.\
Détachement.\
Sans salut.\
*Gelassenheit* dont il ne faut attendre aucun répit.\
Laisser-venir des choses.\
Ainsi du critique de son art.\
Quelque chose du laisser-venir aveugle.\
Idiotie.\
De l'avenir aux yeux bandés et disant le legs de tous les tremblements
du silence de sa lecture.\
Idiote. Trente-trois millions de fois idiote.\
Belle.\
Nourrie de sa perte.\
De son abandon.

On ne parle pas.

On parle du monde et du jugement. Et de la littérature qui est le
déchirement entre les deux.

Il faudrait des prophètes et des prophétesses de tous les genres, de
toutes les espèces.\
On ne s'y risquerait pas.\
Pourquoi ? Parce que c'est impossible et c'est pour ça qu'il faudrait
faire. Faire prophétie. Lier langue et langue.

On n'y croit plus. Ici je veux dire dans notre Metropolis westernisée.\
Angoisse et agonie sur lesquelles flotte et s'enfonce le paquebot de la
civilisation.\
Et tous les arrières-mondes, ceux sur lesquels *la poésie, la critique,
la littérature* s'étaient tenues au 20^e^ siècle Anno Domini, se sont
vaporisés.\
Partis dans l'air jusque dans l'espace, partis vers je ne sais quelle
partie du cosmos malheureux et endeuillé qui les recevra (rien ne se
perd, rien ne se crée).\
Peut-être ne faut-il pas faire le deuil de cette critique.\
Jeter un peu de cendre et inventer autre chose.\
Cette conception de la parole critique faite prophète par goût immonde
de la révélation. Ou du martyr. Je ne sais pas. Un reste d'enfance
aussi.\
De la parole de l'enfance et de la parole du prophète. Si proches.
Idiotes et vraies. Vraies car idiotes. On l'a beaucoup dit.\
On sauvera autre chose.\
On sauvera l'écart infini entre l'enfance et la prophétie.\
Sa désillusion.\
Son échec et sa latence qui fait tous les possibles.\
Sa bêtise faite de tous les malentendus et toutes les incompréhensions.\
La prophétie qui se tient dans son schibboleth.\
Dans son retrait.\
Ce qui n'est pas l'enfance, Dieu merci.\
*Schibboleth* en souvenir de [j.d.](/desidentite/#jd) De [j.d.](/desidentite/#jd) se souvenant de [p.c.](/desidentite/#pc) De [p.c.](/desidentite/#pc)
se retirant de la parole, de la parole des prophètes morts à Auschwitz.
De [j.d.](/desidentite/#jd) dont la parole ne cesse de commencer, de recommencer.
Commentaire sans fin. Refusant la clôture, faisant de la parole de
commentaire une promesse, une errance, un désert, un abandon, puis
l'espoir, le secret et le silence dont on se fait une parole dont la
beauté finit par nous y faire renoncer.\
Alors s'absoudre.\
Se couper, de quoi ? de qui ? de rien.\
*De la souveraineté*, dirait [g.b.](/desidentite/#gb)\
Enfin. Voilà.\
Rêve renouvelé d'acéphalité, d'une communauté critique, *amitologique*
diraient les stellatniks, communauté liée au monde sans religion,
inventant cette communauté anonyme et plurielle impossible à réaliser.
Depuis [g.b.](/desidentite/#gb) Depuis 68. Depuis tous les essais, les critiques, les
recommencements.\
Parole critique, acéphale, impuissante, aprophétique, renouvelée encore
et toujours.\
Mais impossible parce que touchant précisant à la fin de la civilisation
du nihilisme et au Surhumain.\
Et que cela justement "passe la force humaine" ([f.k.](/desidentite/#fk)). La communauté
désoeuvrée, la radicalité, le rêve d'une amitié pour le monde, mais dont
il faut témoigner.\
Malgré les échecs et la destruction qu'elle porte.\
Ce dont on ne peut pas témoigner.\
Schibboleth.\
Ce dont parle la critique.\
Conditions dans lesquelles vivre et penser redeviennent possible. Les
paroles qui nous aident à cela : voilà pourquoi il faut continuer la
parole critique.\
Pourquoi parler d'autres langues.\
Multiplier les erreurs. Et dans les erreurs, la vie.\
Et dans la vie seulement, le sens.\
On se bricole un vitalisme comme on peut, car le nihilisme est si
présent, si présent dans toutes les particules élémentaires qui nous
traversent en permanence.

Moi je voudrais témoigner, aussi pour ce *mal*, pour ce vide, et ce
contre tout le nihilisme, pour cette communauté *absolue*, détachée de
tout, solitaire et stellaire.\
Peut-être encore *désidérée*.\
Et ce n'est qu'une manière de répéter les échecs, les mouvements.
D'*Acéphale* à la *communauté inavouable* en passant par tous les
autres.\
Les rêves d'intellectuels de papier.\
D'intellectuels sans organes.\
Sans tête, sans corps, sans bras.\
Et la réalité des lettres.\
Des prix. Des mots.\
Des lettres composées pour appeler l'humanité à la nuit. Trop sagement
pour défaire le monde et le jugement.\
Être là pour témoigner --- est-ce encore de l'idiotie ? de la rage
incapable de s'exprimer ?\
Témoigner de ces livres faits pour défaire le monde, le jugement, la
pensée.\
Pour nous rendre idiots.

L'histoire n'enseigne rien que ça : que nous sommes des idiots.\
La littérature singe aussi ça.\
Mais la littérature, tu te le murmures secrètement, sait se faire
bestiaire. Sait, en se faisant bête, donner lieu à une autre parole.
C'est ce bestiaire que l'on aime, nous les idiots : toutes ces bêtes.\
Pas que l'*homo sapiens* dans son costume trois pièces.

Est-ce qu'on n'est plus là dans la critique, le monde, le jugement ?\
On est allés si loin.\
J'espère qu'on ne me déteste pas encore.\
Car je veux parler encore de la théorie --- de ce ciel, de tout ce que
l'on a abandonné.\
De la théorie, du ciel, de la vision, de la critique.\
Je regarde le ciel encombré. Un ciel aveugle. Le critique met le bandeau
sur ses yeux et replace ses constellations, fait tomber ses météores, il
fait s'écraser les satellites qui guident les drones, les satellites qui
surveillent tout le meilleur et le pire et on vous vend le marché ainsi.
CGU. On parle toujours de la critique là ? Oui.\
On parle comme si parler avait de l'importance.\
Comme si la critique n'avait pas à voir --- aveugle chose.\
Comme si la critique n'avait pas à penser --- dans le siècle de la bêtise.\
La bêtise et l'idiotie c'est bien différent, [g.d.](/desidentite/#gd) l'a dit mieux que
moi. On ne cesse de le répéter, c'est la vertu de la pédagogie, et sinon
tu fermeras les yeux deux trois lignes et tu rêveras à ta
critique-fiction préférée en attendant.\
Je dis "le siècle", je devrais dire la catastrophe. Ce qui arrive.
C'est [b.s.2](/desidentite/#bs2) qui a bien parlé de ça : *l'état de choc*, et ce que ça
fait au savoir. Le trauma du 20^e^ siècle. Tout ce qui s'est passé. Et
nous, là, stupéfaits, sidérés, en état de *déréliction* pour prendre le
mot médical qui ne soigne rien. Qui dit lui aussi, à sa manière, la
liquéfaction du réel. L'impossibilité de la théorie, de la vision, de la
fiction au-delà de la Littérature, du Capitalisme, de la Critique.

Quelque part ce qu'on voudrait atteindre ce n'est pas un Au-delà de la
Critique, mais une sorte de métamorphose. De métempsychose pourquoi pas.\
Et là, le Bestiaire.\
Se remettre à inventer.\
Critique-Sibylle dont on interprète les coulis de bave. Les tremblements
névrotiques, l'écume des paroles qu'aucun rêve n'a annoncées.\
La critique comme la littérature s'annonce depuis un silence futur.\
Depuis un désir, c'est-à-dire depuis une absence.

{{%asterisme%}}

Parce que ce n'était pas lui, parce que ce n'était [<span id="moi">pas moi</span>](/stase-seconde/#moi).

À se sentir [<span id="indigne">indigne</span>](/stase-seconde/#indigne). À se sentir obligé de recommencer autrement. Dans
la conscience de s'exprimer en regard de ce qui me manque : "Mes
phrases me semblent loin de moi : il y manque *la perte de [<span id="souffle">souffle</span>](/stase-seconde/#souffle)*." ([g.b.](/desidentite/#gb)) Dévisager cet autre, cet ami, qui te force à forcer ta voix pour
creuser le sillon. À te souvenir de l'insomnie des mondes si on la dit
par partition de "l'abîme d'insuffisance" de l'enfance, s'"il faut
encore exiger ceci de l'angoisse : ne jamais vouloir que l'enfant se
taise". ([g.b.](/desidentite/#gb))

Critique ou l'ouverture à une autre communication. Par l'humilité de
l'écoute, l'effacement. Alors le dissensus du dialogue comme forme
ultime d'une communication critique. Un truc un peu idiot dont il
faudrait se départir, une expérience assez commune. La spoliation du
rapport à autrui quand on l'écoute parler juste dans l'attente de
reprendre la parole, de placer notre pensée, d'imposer notre
subjectivité. On voudrait se souvenir, à chaque instant, de cette
disjonction essentielle à celui qui ici s'exprime : une critique assez
idiote pour n'avancer qu'en se critiquant.

"Et peut-on parler d'un livre comme si on ne l'avait pas écrit, comme
si on en était le premier critique ? Peut-on se défaire du dogmatisme
inévitable où se ramasse et se compasse un exposé poursuivant son thème
? \[\...\]. Elle \[la parole de l'auteur\] est seulement dans
l'essence même du langage qui consiste à défaire, à tout instant, sa
phrase par l'avant-propos ou l'exégèse, à [<span id="dedire">dédire</span>](/ligne-dombre/#dedire) le dit, à tenter de
redire sans cérémonies ce qui a déjà été mal entendu dans l'inévitable
cérémonial où se complaît le dit." ([e.l.](/desidentite/#el))

Alors, une critique spectrale, idiote, de n'avoir aucun ego à faire
entendre, plus le moindre avis à imposer, plus de logique ou de
rationnelle démonstration à faire valoir. Rien que des revenances, des
"expériences sensibles, des déchirements, poétiques de la richesse
infinie --- insensée --- des possibles." ([g.b.](/desidentite/#gb))

*J'ai pour me guérir du jugement d'autrui, toute la distance qui me
sépare de moi-même*, disait [a.a.](/desidentite/#aa), du moins dans la revenance que j'en ai.

L'idiotie ou le désir d'effarement, de ravissement. La lecture comme
impuissance, exigence de modestie, confrontation à cette bête expérience
d'autrui qui mieux que nous sait dire ce que nous ignorions penser. La
lecture ou la communion avec le silence : nous n'avons rien à dire, nous
différons ce constat, nous en creusons la résignation, nous faisons
communauté. Se taire pour laisser résonner les dissensions. Perte, échec
et ce qui en revient, en ressort, continue à parler à notre taiseuse
écoute.

On tâtonne ici vers un partage, on réenchante le spectre de la
communauté. Peut-être d'ailleurs seulement comme des images, une
fervente incroyance, le maintien désespérée d'un absolu sceptique.
Idiotement, ça donnerait ceci : on ne se résout pas au prévisible échec
de l'utopie. Juste d'autres formulations, de nouvelles prophéties.

Le désir idiot d'une communauté par tangence, par expropriation, une
de celles où l'on ne parlerait, écrirait, communiquerait, seulement à
cette part de nous hors de nous, celle fantomale, "nos nudités
nocturnales" ([p.c.](/desidentite/#pc)), animale, idiote. Par effleurement, préservation des
silences et des distances.

"Comme si l'on préparait une entrée de bal masqué à l'absent qu'on
sera." (~~[m.l.](/desidentite/#ml)~~) Une sorte alors de biffure de l'échec par ce qui en
revient. Le souvenir d'une douloureuse expulsion, le front de taureau
(mais avec le "goût ironique qu'aurait un taureau de s'appesantir sur
sa propre position" ([g.b.](/desidentite/#gb)) de la bêtise, du silence, la caresse et
le soutien. Vois déjà l'ange (~~[m.l.](/desidentite/#ml)~~ ). Une main sur le front de
Laure, en train de vomir. Souvenance d'un sentiment de communication,
une trouble séduction, la tangence à l'irréparable. Une image, trop
tard, du deuil d'*Acéphale* ?

Mais tes fantômes, dis-moi, ils font bouger quel voile ? Celui de
l'inexistence qui en nous parle, celui qui sait (souveraine tangence au
non-savoir, au neutre, à ce sacrifice souverain de soi) que le
maintenant d'où l'on parle est transitoire achèvement des métempsycoses
(traversée de toutes les identités d'emprunts dérobées dans la lecture)
qui ouvrent à une manière d'expulsion, néant et nirvana pour employer un
vocabulaire qui ne m'appartient pas.

Nous sommes des spectres idiots, on parle seulement de ce qui nous fait
revenir. Continuer à admirer tous ceux dont on peut dire "J'aimais
l'absence du monde en lui." ([p.c.2](/desidentite/#pc2))

{{%asterisme%}}

Est-on assez avisé ([f.n.](/desidentite/#fn)) pour faire des propositions surhumaines pour
la critique ?\
La critique doppleganger. Imitant son objet.\
Plutôt naviguer comme la *Stygiomedusa gigantea* dans des compressions
folles de textes et de syntextes.

La critique comme reprise.\
La critique comme méprise.\
La critique comme lâcher-prise --- sans mâchoires et sans dents. Avec
davantage d'abandons. Critique extatique.

La nuit j'écris\
Le matin aussi\
Le jour je rêve

La critique comme dédoublement. Ce qu'elle arrive à dire, son point
aveugle ([j.c.](/desidentite/#jc))\
Solitude et multiplicités\
Création

De la critique créative.\
Pas du divertissement. Frayer un chemin nouveau dans la forêt des mots.
Des chemins qui ne mènent nulle part. Regardent les cimes. La timidité
des cimes --- dont les branches ne se touchent pas. Nul arbre à abattre.
La forêt à écouter, à ressentir. Beauté de ces moments, je ne me lasse
pas de le dire : tenter le pas au-delà du sentir, du sentier et
ressentir. Arriver où seul on a cru voir une aurore, mille aurores
s'allumer, le crépuscule passer avec le jour en éclair, et la nuit
éclater d'un coup, d'un ciel plein d'étoiles. Et avec des battements de
cœur, et avec dans la main quelque chose comme ta main. Demain, je
saurais ce que c'est. Aujourd'hui je ne pense pas à demain. J'écris ma
critique. Ce n'est pas une nuit sacrée. C'est une autre nuit, une nuit
dans la nuit, composée d'autres choses que les atomes du mot nuit, mais
une nuit composée d'autre chose que de l'image de la nuit, sorte de
contre-nuit comme il est des contre-feux.

Dans l'ombre : la critique veille.\
Veilleuse de jour. Lanterne. Abandonner les images. Se faire apostat.
Pourtant on l'a vu : ne pas abandonner l'imaginaire. Au contraire, faire
récit. Faire débat interne. Faire organique l'activité critique. Pas de
gélatine sur la peau de notre critique.\
Elle s'électrise, s'ébroue.

Amitié de toute critique. À entendre avec force. Pas la *philia* des
Grecs anciens, mais comme un filum (*alma spina mater*), une *kinship*
d'une drôle de manière (*queerness of it all*). Être de ce "désêtre"
dont parle [p.c.2](/desidentite/#pc2) rejoint un peu la communauté négative de ceux qui n'ont
pas de communauté, celle que [m.b.](/desidentite/#mb) nommait du beau nom impossible à
prononcer sans tous les débats qu'il a suscités : *communauté
inavouable*.\
Comme ça déjà été dit, et que l'on continue de dire : innocente et
impardonnable.

Aveu : la critique est cet aveu et cette déclaration d'amitié à qui
veut, adresse sans objet, vœu à l'inconnu.\
À l'auteur, à celles et ceux qui lisent. Qui ne liront que ça. Des
lectures secondes. Des lectures en puissance. On en a fait. Des
catalogues et des listes. Des titres familiers à pouvoir les réciter
comme des comptines (mais on s'est juré ici de ne pas faire du [p.b.](/desidentite/#pb)).

À voir, donc.\
À redire.\
À recommencer toujours.\
Toujours et autrement.

<div style="text-align: right; margin-right: 5em;">

*To be continued.*

</div>
