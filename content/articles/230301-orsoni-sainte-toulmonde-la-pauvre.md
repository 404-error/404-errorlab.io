---
persona:
  - Jérôme Orsoni
title: "Sainte Toulmonde la pauvre"
slug: "sainte-toulmonde-la-pauvre"
date: 2023-03-24
echo:
  - stratégique
images:
  - "img/230301-orsoni-sainte-toulmonde-la-pauvre.jpg"
gridimages:
  - "img/230301-orsoni-sainte-toulmonde-la-pauvre-grid.jpg"
sourceimage: "https://commons.wikimedia.org/wiki/File:Protest\_Ends\_Riots\_Begin\_WTO\_1999\_by\_J.\_Narrin.jpg"
notes: |
  Ce texte, qui doit à peu près tout à l'ouvrage de Francesco Careri, *Walkscapes. La marche comme pratique esthétique*, et à mes discussions avec Rodhlann Jornod, s'inscrit dans le cadre d'un travail d'écriture plus vaste qui s'intitule, faute pour l'instant d'une idée plus originale, *projet Paris*.

  <a href="https://commons.wikimedia.org/wiki/File:Protest_Ends_Riots_Begin_WTO_1999_by_J._Narrin.jpg" target="_blank" rel="noopener">L'image originale</a> de couverture, photographie de la *bataille de Seattle* de 1999, est de J. Narrin. L'image dérivée est de AAA. L'image du monde est une dérive du monde lui-même. Brûlons l'image, puis le monde.
summary: "C'était à Paris, le 21 mars 2023, la veille au soir, quelques minutes après le passage d'un fragment de cortège spontané qui venait de quitter la place Vauban pour se rendre dieu sait où, au diable, peut-être, entre incendies miniatures et stupéfaction générale, l'ambiance était étrange sur le boulevard. Au milieu des véhicules qui tentaient de se frayer un chemin dans ce chaos microscopique, de braves gens tâchaient de ramasser ce qui traînait là, renversé, éventré, et puis d'éteindre les petits feux nauséabonds qui s'étaient déclarés en attendant l'arrivée prochaine des pompiers. Sur le trottoir, indifférents au désastre d'une ville couverte d'ordures, où la saleté le dispute à la pauvreté, étrangers aux événements nouveaux qui avaient pourtant lieu partout autour d'eux, des clients de bar descendaient leur lénifiante pinte d'afterwork."
citation: "l'espace de la frontière même entre les choses possibles et les choses impossibles"
poetry: false
hangindent: false
---


## 14 avril 1921 - 21 mars 2023 : généalogie d'un possible printemps

{{%epigraphe%}}
« Marchons, marchons ! »
{{%/epigraphe%}}

C'était à Paris, le 21 mars 2023, la veille au soir, quelques minutes
après le passage d'un fragment de cortège spontané qui venait de quitter
la place Vauban pour se rendre dieu sait où, au diable, peut-être, entre
incendies miniatures et stupéfaction générale, l'ambiance était étrange
sur le boulevard. Au milieu des véhicules qui tentaient de se frayer un
chemin dans ce chaos microscopique, de braves gens tâchaient de ramasser
ce qui traînait là, renversé, éventré, et puis d'éteindre les petits
feux nauséabonds qui s'étaient déclarés en attendant l'arrivée prochaine
des pompiers. Sur le trottoir, indifférents au désastre d'une ville
couverte d'ordures, où la saleté le dispute à la pauvreté, étrangers aux
événements nouveaux qui avaient pourtant lieu partout autour d'eux, des
clients de bar descendaient leur lénifiante pinte d'afterwork. À la
télévision, l'étonnement n'était pas moins frappant : parallèlement aux
discours absurdes qui tentaient d'expliquer quelque chose qui, par
nécessité, s'efforçait d'échapper aux concepts banals que mobilise pour
se rassurer une élite vaine et à court d'idées, on montrait les images
de ces manifestants, formant sortes de cortèges fantômes dont personne
ne savait où ils pouvaient bien aller. Tout ce que l'on pouvait dire,
c'est qu'ils avaient l'air jeunes, qu'ils marchaient vite et ne
semblaient pas suivre d'itinéraire précis. Indétermination qui, si on
avait eu quelque idée de ce qui peut bien se mettre en œuvre quand on
s'aventure à traverser l'espace public librement, aurait pu éclairer les
bonnes consciences, mais non : comme toujours, la lumière resta éteinte.

Pourtant, la déambulation spontanée, sans but, a une histoire, assez
bien connue d'ailleurs, et qu'on peut faire remonter au flâneur
baudelairien se promenant dans le Paris du XIX^e^ siècle. « Observateur,
flâneur, philosophe », Walter Benjamin, dans *Paris, capitale du XIX^e^ siècle*,
en donne la description que voici :

« Le génie de Baudelaire, qui trouve sa nourriture dans la mélancolie,
est un génie allégorique. Pour la première fois chez Baudelaire, Paris
devient objet de poésie lyrique. Cette poésie locale est à l'encontre de
toute poésie de terroir. Le regard que le génie allégorique plonge dans
la ville trahit bien plutôt le sentiment d'une profonde aliénation.
C'est là le regard d'un flâneur, dont le genre de vie dissimule derrière
un mirage bienfaisant la détresse des habitants futurs de nos
métropoles. Le flâneur cherche un refuge dans la foule. La foule est le
voile à travers lequel la ville familière se meut pour le flâneur en
fantasmagorie. Cette fantasmagorie, où elle apparaît tantôt comme un
paysage, tantôt comme une chambre, semble avoir inspiré par la suite le
décor des grands magasins, qui mettent ainsi la flânerie même au service
de leur chiffre d'affaires. Quoi qu'il en soit les grands magasins sont
les derniers parages de la flânerie.

Dans la personne du flâneur l'intelligence se familiarise avec le
marché. Elle s'y rend, croyant y faire un tour ; en fait c'est déjà pour
trouver preneur. Dans ce stade mitoyen où elle a encore des mécènes,
mais où elle commence déjà à se plier aux exigences du marché (en
l'espèce du feuilleton), elle forme la bohème. À l'indétermination de sa
position économique correspond l'ambiguïté de sa fonction politique.
\[...\] Le flâneur fait figure d'éclaireur sur le marché. En cette qualité
il est en même temps l'explorateur de la foule. »

Ce que Benjamin fait apparaître, c'est la dualité du flâneur, à la fois
éclaireur avant-gardiste qui explore le monde capitaliste en train de
prendre forme sous ses yeux et figure qui annonce l'auteur qui vend son
intelligence au marché comme si c'était une marchandise parmi d'autres
(ceux-là mêmes en somme qui, de nos jours, sont chargés par les médias
d'expliquer pour que nous nous dispensions de penser ce qui se passe
sous nos yeux et que nous sommes tout à fait en mesure de voir par
nous-mêmes). On sait que Benjamin a été marqué par le surréalisme, et
notamment par l'ouvrage d'Aragon, *Le paysan de Paris*, qui joua un rôle
décisif dans la genèse de son projet de *Livres des passages*. Cet
héritage surréaliste inscrit à rebours le flâneur baudelairien dans une
histoire qui en fait le précurseur des déambulations qui eurent lieu à
Paris au début des années 1920 et tout au long du XX^e^ siècle ensuite. De
ce premier temps qui va de Dada aux surréalistes, deux dates sont à
retenir. Tout d'abord, le 14 avril 1921, à trois heures précises de
l'après-midi, quand un groupe formé de Jean Crotti, Georges d'Esparbès,
André Breton, Jacques Rigaut, Paul Éluard, Georges Ribemont-Dessaignes,
Benjamin Péret, Théodore Fraenkel, Louis Aragon, Tristan Tzara et
Philippe Soupault, sortit de chez soi pour faire une singulière
promenade. Voici ce qu'ils déclaraient, invitant le public à les
rejoindre :

« Aujourd'hui, à 15 heures, dans le jardin de l'église
St-Julien-le-Pauvre, Dada, inaugurant une série d'excursions dans Paris,
invite gratuitement ses amis et adversaires à visiter avec lui les
dépendances de l'église. Il ne s'agit pas d'une manifestation
anticléricale comme on serait tenté de le croire, mais bien plutôt d'une
nouvelle interprétation de la nature appliquée cette fois non pas à
l'art, mais à la vie. »

De série d'excursions, il n'y en eut pas, il pleut, les présents
pontifient, c'est un échec ; mais l'art venait de sortir dans la rue et
il n'en partirait plus. Trois ans plus tard (c'est la deuxième date de
notre calendrier), en mai 1924, André Breton décida d'approfondir la
découverte de la vie qu'il avait faite à Paris en cette journée de
printemps, et de procéder à une déambulation où le hasard, l'absence de
destination, l'irréalité même de la chose qui se déroule cependant que
l'on va là où l'on ne sait, seraient les seuls guides. En 1952, Breton
décrira cette seconde entreprise dans les termes suivants :

« Tous alors, nous sommes d'accord pour penser qu'une grande aventure
est à courir. “Lâchez tout. Partez sur les routes !” : c'est mon thème
d'exhortation à cette époque. \[...\] Mais sur quelles routes partir ? Des
routes matérielles, c'est peu probable, des routes spirituelles, nous
les voyions mal. Toujours est-il que ces deux sortes de routes, l'idée
nous vint de les combiner. D'où une déambulation à quatre, Aragon,
Morise, Vitrac, et moi, entreprise vers cette époque à partir de Blois,
ville tirée au sort sur la carte.

Il est convenu que nous irons au hasard à pied, tout en devisant, ne
nous permettant de crochets volontaires que ce qu'il faut pour pouvoir
manger et dormir. Entreprise dont l'exécution s'avère très singulière et
même semée de périls. Le voyage, prévu pour une dizaine de jours et qui
sera abrégé, prend d'emblée un tour initiatique.

L'absence de tout but nous retranche très vite de la réalité, fait lever
sous nos pas des phantasmes de plus en plus nombreux, de plus en plus
inquiétants. L'irritation guette et même il advient, entre Aragon et
Vitrac, que la violence intervienne.

Tout compte fait, exploration nullement décevante, quelle qu'ait été
l'exiguïté de son rayon, parce qu'exploration aux confins de la vie
éveillée et de la vie de rêve, par là on ne peut plus dans le style de
nos préoccupations d'alors. » (Breton, *Entretiens*)

*Exploration aux confins de la vie éveillée et de la vie de rêve*, dit
Breton, et voici que s'ouvre sous nos pas un espace impossible à
parcourir autrement qu'en marchant, l'espace de la frontière même entre
les choses possibles et les choses impossibles, entre la veille et le
rêve, la réalité et l'utopie, ici et ailleurs, le présent et l'avenir.
L'individu qui se met en marche sans but découvre quelque chose qui
dépasse la pratique, la mise en œuvre de ce qu'il est en train de faire
concrètement, il découvre un univers indéterminé où tout s'avère
possible non en vertu d'une ontologie spéciale, mais en vertu même de ce
qu'il est en train de faire, du mouvement qui est le sien, de la
distance qu'il parcourt. L'absence de but à la déambulation ne réduit
pas cette déambulation à néant, ne la ruine pas, au contraire, elle
permet de révéler la nature de l'univers qui est le nôtre, un univers
qui n'est pas figé, qui n'est pas stable, qui est tout de possibilités
inexplorées explorables, de futurs qui ne demandent qu'à advenir,
d'alternatives auxquelles personne n'a encore songé. « Vent de
l'éventuel », comme l'écrit Breton dans *Les pas perdus*, la conquête de
la rue dans l'ambulatoire de la marche est une utopie en actes, car la
déambulation n'a pas de lieu propre, n'ayant pas de destination, elle ne
va nulle part, n'est nulle part, peut être partout, s'offrir comme une
vie nouvelle pour quiconque va.

Des années plus tard, quand en 1956 Guy Debord dans le n^o^ 9 des *Lèvres
nues* critiquera la déambulation surréaliste, il sera difficile de ne
pas voir dans sa propre « théorie de la dérive » une continuation de la
démarche de ces ancêtres honnis :

« Une insuffisante défiance à l'égard du hasard, et son emploi
idéologique toujours réactionnaire, condamnait à un échec morne la
célèbre déambulation sans but tentée en 1923 par quatre surréalistes à
partir d'une ville tirée au sort : l'errance en rase campagne est
évidemment déprimante, et les interventions du hasard y sont plus
pauvres que jamais. \[...\] Aux antipodes de ces aberrations, le caractère
principalement urbain de la dérive, au contact des centres de
possibilités et de significations que sont les grandes villes
transformées par l'industrie, répondrait plutôt à la phrase de Marx :
“Les hommes ne peuvent rien voir autour d'eux qui ne soit leur visage,
tout leur parle d'eux-mêmes. Leur paysage même est animé.” »

Si riche que soit cette dérive en ce qu'elle révèle l'impensé de la
ville, fait apparaître une géographie invisible sur la carte, laquelle
émerge et se rend manifeste uniquement quand on arpente la ville non
pour en faire le relevé géographique, mais pour la penser, la sentir, la
vivre comme un psychogéographe, Debord y pose toutefois une limite qui a
trait au nombre de participants :

« Au-dessus de quatre ou cinq participants, le caractère propre à la
dérive décroît rapidement, et en tout cas il est impossible de dépasser
la dizaine sans que la dérive ne se fragmente en plusieurs dérives
menées simultanément. »

Or, quiconque aura pris le temps de marcher avec Stalker, le groupe
cofondé par l'architecte et théoricien romain, Francesco Careri, sait au
contraire que le nombre n'est pas un obstacle. Si les interventions de
Stalker dans la ville ne sont pas à proprement parler « des dérives »,
il n'en demeure pas moins que le nombre n'est pas une limite en soi à la
possibilité de l'expérience commune que peut devenir la marche. En
vérité, on ne marche jamais seul. Symboliquement, comme le montre cette
généalogie sommaire de la déambulation urbaine, nous sommes précédés,
nous marchons toujours dans des pas qui nous précèdent, les pas des
flâneurs, des surréalistes, des situationnistes, des activistes qui ont
fait de la ville leur terrain d'enquête, d'exploration et
d'expérimentation. Et ainsi, mettant nos pas dans les pas des autres,
rien n'empêche que d'autres mettent leurs pas dans les nôtres, que la
ville devienne ce grand tracé impromptu de tous les pas qui y vont, qui
y passent, qui cherchent par leur mouvement à eux, sans autre force que
celle de leur propre corps, à découvrir quelque chose, à faire
apparaître quelque chose, à manifester quelque chose.

Quant à ce hasard, frappé de doute et sur lequel Debord semble même
jeter l'anathème de la réaction, ne nous semble-t-il pas désormais une
des rares façons, sinon la seule, d'échapper au contrôle généralisé, à
l'enrégimentement de toute initiative, à la récupération de tout
mouvement, au rachat de toute utopie par le marché ?

Qu'est-ce, au fond, que la spontanéité sinon cette ouverture à
l'imprévu, cette possibilité de rompre avec tout parcours délimité pour
que le bas-côté cesse de l'être, qu'il devienne le haut-côté, le
aux-côtés ? Au-delà des poubelles renversées et brûlées, ce qui saute
aux yeux dans les manifestations nocturnes de la nuit du 20 au 21 mars
2023, c'est que les piétons quittent le trottoir pour occuper la
chaussée : ce ne sont plus les véhicules qui occupent le maximum
d'espace, ce sont les gens, qui semblent trouver dans cette façon de
manifester une façon aussi de reprendre l'espace qui est le leur, de
reconquérir tout un pan de la ville qui leur est défendu, interdit, qui
les menace. Le piéton qui, par la force du nombre, fût-il relativement
petit, n'ayant plus peur de descendre du trottoir, s'approprie l'espace
dont, au sein même de l'espace public, il est privé, littéralement
*s'aventure*, fait une expérience à nulle autre pareille, l'expérience
de l'union du moi à l'univers, à tout le monde.

Toute la vie sociale est clivée au sens où le clivage est au fondement
même de la vie sociale --- « Ceci est à moi qui n'est pas à toi », ou
comme l'écrivait Rousseau dans son *Discours sur l'origine et les
fondements de l'inégalité entre les hommes* : « Le premier qui, ayant
enclos un terrain, s'avisa de dire, ceci est à moi, et
trouva des gens assez simples pour le croire, fut le vrai fondateur de
la société civile. Que de crimes, de guerres, de meurtres, que de
misères et d'horreurs n'eût point épargnés au
genre humain celui qui, arrachant les pieux ou comblant le fossé, eût
crié à ses semblables : Gardez-vous d'écouter cet imposteur ; vous êtes
perdus, si vous oubliez que les fruits sont à tous, et que la terre
n'est à personne. » ---, et la déambulation au hasard,
libérant l'espace de sa privatisation infinie, en finit avec ce clivage,
non par la réconciliation, mais par la conquête d'une aventure jamais
faite auparavant. Révéler la ville, révéler la rue, marcher dans Paris
la nuit sans limitation ni but, qu'est-ce sinon ouvrir grand la
possibilité de mettre en œuvre l'utopie ? Ce qu'il manque ainsi au
mouvement, ce n'est pas une conscience --- une conscience politique, une
conscience de classe, une conscience de soi ---, en fait, le mouvement a
encore trop de conscience, la marche est encore trop encadrée, le
parcours délimité, les autorisations déposées, la marche a encore trop
de conscience. Il faut que la marche cesse de demander la permission et
qu'ainsi, libre, imprévisible, entraînante, indéterminée, elle s'ouvre à
tout le monde, il faut qu'elle emmène tout le monde avec elle parce que
le monde n'est pas une franchise privatisée, le monde est à tout le
monde. Marcher, répliquera-t-on, ce n'est cela qui va changer la vie,
non, mais c'est un premier pas.


