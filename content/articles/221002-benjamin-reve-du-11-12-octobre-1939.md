---
persona:
  - Walter Benjamin
title: "Rêve du 11-12 octobre 1939"
slug: "reve-du-11-12-octobre-1939"
date: 2022-10-11
echo:
  - historique
  - psychique
images:
  - "img/221002-benjamin-reve-du-11-12-octobre-1939.jpg"
gridimages:
  - "img/221002-benjamin-reve-du-11-12-octobre-1939-grid.jpg"
sourceimage: "https://commons.wikimedia.org/wiki/File:Paul_Klee_~_Engel,_%C3%BCbervoll_~_1939.jpg?uselang=fr"
notes: "Ce texte figure notamment dans *Walter Benjamin, Gesammelte Schriften VI, Suhrkamp, Frankfurt am Main, 1991, p. 540*."
summary: "Je me trouvais avec Dausse en compagnie de plusieurs personnes dont je ne me souviens pas. À un moment donné, nous quittâmes cette compagnie, Dausse et moi. Après nous être absentés, nous nous trouvâmes dans un fouillis ; je m'aperçus que presqu'à même le sol, se trouvait un drôle de genre de couches. Ces couches étaient constituées par des constructions très basses."
citation: "Il s'agissait de changer en fichu une poésie."
---

Je me trouvais avec Dausse en compagnie de plusieurs personnes dont je ne me souviens pas. À un moment donné, nous quittâmes cette compagnie, Dausse et moi. Après nous être absentés, nous nous trouvâmes dans un fouillis ; je m'aperçus que presqu'à même le sol, se trouvait un drôle de genre de couches. Ces couches étaient constituées par des constructions très basses. Elles semblaient être en pierres, mais en m'y appuyant je m'aperçus qu'on s'y enfonçait mollement comme dans un lit ; elle était couverte d'une sorte de mousse et de lierres. Je m'aperçus que ces couches étaient distribuées deux à deux. À l'instant où je pensais m'étendre sur celle qui voisinait avec une couche que je pensais affectée à Dausse, je me rendis compte que le chevet de cette couche était déjà occupé par d'autres personnes. Nous quittâmes donc ces couches qui étaient des tombes et nous poursuivîmes notre chemin.

L'endroit ressemblait toujours à une forêt, mais il y avait dans la distribution des fûts et des branches quelque chose d'artificiel qui donnait à cette partie du décor une vague ressemblance avec une construction nautique. En longeant quelque poutre et en traversant quelques marches en bois, nous nous trouvâmes sur une sorte de pont de bateau minuscule, de petites terrasses en bois. C'était là que se trouvaient les femmes avec lesquelles Dausse vivait. Elles étaient trois ou quatre et me paraissaient d'une grande beauté. La première chose qui m'étonnait fut que Dausse ne me présenta pas. Cela ne me gêna pas plus que la découverte que je fis au moment de déposer mon chapeau sur un piano à queue. C'était un vieux chapeau de paille, un *panama* dont j'avais hérité de mon père. (Ce chapeau n'existe plus depuis longtemps.) Je fus frappé en m'en débarrassant, une large fente avait été appliquée dans la partie supérieure du chapeau. J'aperçus incidemment et sans m'en formaliser que les bords de cette fente présentaient des traces de couleur rouge.

Une des dames qui étaient assises s'était entre-temps occupée de graphologie. Je vis qu'elle avait en main quelque chose qui avait été écrit par moi et que Dausse lui avait donné. Je m'inquiétais un peu de cette expertise, craignant que mes goûts intimes puissent ainsi être décelés. Je m'approchais. Ce que je vis, était une étoffe qui était couverte d'images et dont les seuls éléments graphiques que je pus distinguer, étaient les parties supérieures de la lettre *d* qui décelaient dans leur longueur effilée une aspiration extrême vers la spiritualité. Cette partie de la lettre était, en surplus, munie d'une petite voile à bordure bleue, et cette voile se gonflait sur le dessin comme si elle se trouvait sous la brise. C'était là la seule chose que je pus *lire* --- le reste offrant des motifs indistincts de vague et de nuages. L'entretien tourne un moment autour de cette écriture. Je ne me souviens pas des opinions avancées, mais je sais très bien qu'à un moment donné, je disais textuellement (et en français ; c'est pourquoi j'écris ce rêve en français): « Il s'agissait de changer en fichu une poésie. »

J'avais à peine prononcé ces mots qu'il se passa quelque chose d'intrigant. Je m'aperçus qu'il y avait une parmi les femmes, très belle également, qui était couchée dans un lit. En entendant mon explication, elle eut un mouvement bref comme un éclair. Elle écarta parcimonieusement et de façon toute subite la couverture qui l'abritait dans son lit. Ce n'était pas pour faire voir son corps, mais le dessin de son drap de lit qui devait offrir une imagerie analogue à celle que j'avais dû *écrire* il y a bien des années, pour en faire cadeau à Dausse. Je sus très bien que la dame faisait ce mouvement. Mais ce qui m'en informait, était une sorte de vision supplémentaire. Car quant aux yeux de mon corps, ils étaient ailleurs, et je ne distinguais nullement ce que pouvait offrir le drap de lit qui s'était si fugitivement ouvert pour moi.
