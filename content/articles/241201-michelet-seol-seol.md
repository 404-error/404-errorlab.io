---
persona:
  - Étienne Michelet
title: "Seol Seol"
slug: "seol-seol"
date: 2024-12-05
echo:
  - rythmique
  - graphique
images:
  - "img/241201-michelet-seol-seol.jpg"
gridimages:
  - "img/241201-michelet-seol-seol-grid.jpg"
sourceimage: "https://commons.wikimedia.org/wiki/File:August_Macke_-_Couple_on_the_Forest_Track_-_Google_Art_Project.jpg"
notes: ""
summary: "la plante en rhizomes / sans fleurs ni graines / et tes yeux parmi les bambous / Seol Seol / sais-tu qu'aucun cercle / ne se brise ? / Seol Seol / quand les chiens / se sont mis à tourner / à mordre / la chienne seule / sous le ciel blanc / la lune ne bougeait pas / et ton œil ton visage / ne bougeaient pas / toi tu n'aurais même pas / écrasé avec tes doigts / l'insecte sur ta peau"
citation: "sais-tu qu’aucun cercle ne se brise ?"
poetry: false
hangindent: false
javascript:
  - "241201-vanilla-tilt.min.js"
  - "241201-michelet-seol-seol.js"
---


<audio loop id="fond-sonore">
<source src="/img/241201-michelet-seol-seol.mp3" type="audio/mpeg">
</audio>

<br>

<button class="button button--music music">
  <span class="music--on hidden" >MUSIQUE</span>
  <span class="music--off">MUSIQUE</span>
</button>

{{< figureglitch src="/img/241201-michelet-seol-seol-incendie.jpg" alt="Seol Seol" width="70%" margin="0 0" class="tilt" div-style="position: -webkit-sticky; position: sticky; top: 2em; opacity: 0.8;" >}}

<br>

<div>
<div class="text-over-image">

<span class="highlight highlight--red highlight--invisible">la plante en rhizomes</span><br>
<span class="highlight highlight--red highlight--invisible">sans fleurs ni graines</span><br>
<span class="highlight highlight--red highlight--invisible">et tes yeux parmi les bambous</span><br>
<span class="highlight highlight--red highlight--invisible">Seol Seol</span><br>
<span class="highlight highlight--red highlight--invisible">sais-tu qu'aucun cercle</span><br>
<span class="highlight highlight--red highlight--invisible">ne se brise ?</span><br>
<br>
<span class="highlight highlight--red highlight--invisible">Seol Seol</span><br>
<span class="highlight highlight--red highlight--invisible">quand les chiens</span><br>
<span class="highlight highlight--red highlight--invisible">se sont mis à tourner</span><br>
<span class="highlight highlight--red highlight--invisible">à mordre</span><br>
<span class="highlight highlight--red highlight--invisible">la chienne seule</span><br>
<span class="highlight highlight--red highlight--invisible">sous le ciel blanc</span><br>
<span class="highlight highlight--red highlight--invisible">la lune ne bougeait pas</span><br>
<span class="highlight highlight--red highlight--invisible">et ton œil ton visage</span><br>
<span class="highlight highlight--red highlight--invisible">ne bougeaient pas</span><br>
<br>
<span class="highlight highlight--red highlight--invisible">toi tu n'aurais même pas</span><br>
<span class="highlight highlight--red highlight--invisible">écrasé avec tes doigts</span><br>
<span class="highlight highlight--red highlight--invisible">l'insecte sur ta peau</span><br>
<br>
<span class="highlight highlight--red highlight--invisible">toi tu ne bougeais pas</span><br>
<span class="highlight highlight--red highlight--invisible">quand les chiens tournaient</span><br>
<span class="highlight highlight--red highlight--invisible">quand les chiens mordaient</span><br>
<span class="highlight highlight--red highlight--invisible">la chienne seule</span><br>
<span class="highlight highlight--red highlight--invisible">sous le ciel blanc</span><br>
<br>
<span class="highlight highlight--red highlight--invisible">la langue dans la bouche</span><br>
<span class="highlight highlight--red highlight--invisible">il fallait la tourner mille fois</span><br>
<span class="highlight highlight--red highlight--invisible">Seol Seol</span><br>
<span class="highlight highlight--red highlight--invisible">avant de tout brûler</span><br>
<span class="highlight highlight--red highlight--invisible">et la retourner encore mille fois</span><br>
<span class="highlight highlight--red highlight--invisible">avant d'allumer l'incendie</span><br>
<span class="highlight highlight--red highlight--invisible">ou encore regarder mille fois</span><br>
<span class="highlight highlight--red highlight--invisible">les oiseaux qui remuent</span><br>
<span class="highlight highlight--red highlight--invisible">comme des automates</span><br>
<span class="highlight highlight--red highlight--invisible">dans les branches souples</span><br>
<br>
<span class="highlight highlight--red highlight--invisible">Seol Seol</span><br>
<span class="highlight highlight--red highlight--invisible">regarde le chien qui fixe</span><br>
<span class="highlight highlight--red highlight--invisible">un certain temps les oiseaux</span><br>
<span class="highlight highlight--red highlight--invisible">ou regarde encore</span><br>
<span class="highlight highlight--red highlight--invisible">le chien qui écoute</span><br>
<span class="highlight highlight--red highlight--invisible">un certain temps</span><br>
<span class="highlight highlight--red highlight--invisible">les pleurs du nourrisson</span><br>
<span class="highlight highlight--red highlight--invisible">les reptiles méritent tes caresses</span><br>
<span class="highlight highlight--red highlight--invisible">comme on caresse</span><br>
<span class="highlight highlight--red highlight--invisible">comme on effleure les cordes</span><br>
<span class="highlight highlight--red highlight--invisible">d'un instrument à cordes</span><br>
<span class="highlight highlight--red highlight--invisible">Seol Seol</span><br>
<span class="highlight highlight--red highlight--invisible">tes cordes vocales</span><br>
<span class="highlight highlight--red highlight--invisible">sont de la magie pour le monde</span><br>
<br>
<span class="highlight highlight--red highlight--invisible">ça te fait sourire</span><br>
<span class="highlight highlight--red highlight--invisible">toutes ces flammes en l'air</span><br>
<span class="highlight highlight--red highlight--invisible">Seol Seol</span><br>
<span class="highlight highlight--red highlight--invisible">les flammes levées</span><br>
<span class="highlight highlight--red highlight--invisible">comme des langues</span><br>
<span class="highlight highlight--red highlight--invisible">dans le ciel blanc</span><br>
<span class="highlight highlight--red highlight--invisible">les flammes dans tes yeux</span><br>
<span class="highlight highlight--red highlight--invisible">ça te fait sourire</span><br>
<span class="highlight highlight--red highlight--invisible">Seol Seol</span><br>
<span class="highlight highlight--red highlight--invisible">de voir le monde brûler</span><br>
<span class="highlight highlight--red highlight--invisible">dans tes yeux</span><br>
<br>
<span class="highlight highlight--red highlight--invisible">les flammes dansent</span><br>
<span class="highlight highlight--red highlight--invisible">dans le champ de canne à sucre</span><br>
<span class="highlight highlight--red highlight--invisible">toi tu danses sur le chemin</span><br>
<span class="highlight highlight--red highlight--invisible">Seol Seol</span><br>
<span class="highlight highlight--red highlight--invisible">quand les chiens aboient</span><br>
<span class="highlight highlight--red highlight--invisible">les flammes dans tes yeux</span><br>
<span class="highlight highlight--red highlight--invisible">et dans ton sourire le monde brûle</span><br>
<span class="highlight highlight--red highlight--invisible">et dans ta danse le monde brûle</span><br>
<br>
<br>
<br>
<br>
<br>
<br>
</div>
</div>

