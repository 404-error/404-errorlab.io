---
persona:
  - Jérôme Orsoni
title: "Ruissellements"
slug: "ruissellements"
date: 2021-01-14
echo:
  - rythmique
images:
  - "img/210114-orsoni-ruissellements.jpg"
gridimages:
  - "img/210114-orsoni-ruissellements-grid.jpg"
sourceimage: "https://commons.wikimedia.org/wiki/File:Crossing_the_River_Styx.jpg"
notes: ""
summary: "Rêves raides dans nos sillages / trop de temps dépensé / temps à imaginer / le bruit que ferait la mort / si on l’écoutait / rêves étrusques qui se fissurent / et les statuaires figées / sédiments d’actualité peut-être / accumulés / mais qui sait pour quoi ? / à quel demain / voués ? / un vivant vient / et tout est à recommencer / à-coups / ou le progrès qui se fait / oui mais par dégoût / un vivant vient / et tous les rivages sont inondés / rives pauvres avant / rimes pauvres après / il faut qu’un os se justifie / en quelque matière / manière de retour à la terre / qui nourrit qui / et jusques à quand ? /"
citation: "désirer la métamorphose des flammes / destruction de la destruction / feu à volonté."
---

Rêves raides dans nos sillages\
trop de temps dépensé\
temps à imaginer\
le bruit que ferait la mort\
si on l’écoutait\
rêves étrusques qui se fissurent\
et les statuaires figées\
sédiments d’actualité peut-être\
accumulés\
mais qui sait pour quoi ?\
à quel demain\
voués ?\
un vivant vient\
et tout est à recommencer\
à-coups\
ou le progrès qui se fait\
oui mais par dégoût\
un vivant vient\
et tous les rivages sont inondés\
rives pauvres avant\
rimes pauvres après\
il faut qu’un os se justifie\
en quelque matière\
manière de retour à la terre\
qui nourrit qui\
et jusques à quand ?\
en arrière\
je jette un regard détourné\
il en faut des regrets à entasser\
les règles sont l’infraction\
sinon on les suivrait\
joueur de flûte ivre\
ou alors naïf\
il ignore la différence\
entre une chose et son contraire\
le passé et le passage\
l’après et puis l’ennui\
solitude flasque\
on dirait sa vitalité aspirée\
engloutie dans un trou\
enfouie dans une poche\
on ne sait\
on n’en trouve plus l’entrée\
là où sont conservés les os des âmes\
et les âmes des os\
une chose donc\
et son contraire\
toujours l’eau coule\
rigole qui à force cause\
la destruction\
la fin de toutes choses\
avec le temps\
ou bien de lyre\
quatre notes dans le champ chromatique\
qui pourraient vouloir dire aussi\
le ciel est bleu\
le ciel est gris\
c’est le moment\
la suite hors des idées\
le creux qui se forme sur la peau des choses\
sur la peau de nos peaux\
rides ridules\
occasions\
quatre notes qui se suivent\
et ne veulent rien dire\
raison de l’écoute\
maintenant et à l’heure de notre mort\
une flûte ou une lyre\
en sortant de la cavité\
où s’illustre la nécessité de vivre\
les yeux fermés\
pas la cécité\
mais quelque chose masquée\
aux sauvages que nous sommes\
que nous serons\
que nous devrons être\
la flûte s’oppose-t-elle au vivant\
ou le piano qui sait ?\
les siècles se collent les uns aux autres\
et l’on baptise cela d’un nom grossier\
délirant\
histoire\
qui pourrait encore vouloir vivre dans l’histoire\
après tout ce qu’il s’est passé ?\
n’est-ce pas là\
la plus immense\
la plus ridicule\
ridule à la surface\
de nos paradoxes ?\
vivre une vie dans cela pour quoi\
nul n’a plus de désir\
personne n’eut jamais désir de l’histoire\
la faire c’est la subir\
mais l’inverse s’annule\
se retourne contre soi-même\
colérique chimère\
tous les temps ajoutés\
ne vaudront jamais quatre notes\
les unes à la suite des autres\
quatre notes dans le champ chromatique\
quatre notes simples\
quatre notes insensées aussi bien\
le ciel est bleu\
le ciel est gris\
quelle différence cela fait-il\
si c’est toujours le même ciel ?\
chansons tristes en plein été\
insectes prophètes\
sous le soleil\
chantons ivres en plein hiver\
sur le mur de la mer\
il y a des signes illisibles\
glyphes\
que j’essaie de déchiffrer\
comme le premier bateau venu dans la baie\
la conquête de la calanque\
j’imagine\
le premier meurtre sans doute\
et ses torrents de sang\
origine sainte\
de notre unique histoire\
aujourd’hui et depuis l’heure de notre mort\
de la rive jusqu’à la montagne\
la présence est barbare\
que sont ces formes\
sont-elles des gens\
pourquoi bougent-elles\
et pour combien de temps ?\
musique continue\
milliards de mélodies\
quatre notes dans le champ chromatique\
entrer dans l’antre\
oracles dans les feuilles\
temples en ruines\
jusques en haut de la colline\
tout part toujours en fumée\
toxique\
au bout du temps\
l’air devient irrespirable\
et les corps figés\
la civilisation même\
écoutant la voix\
les visages se crispent\
on dirait des statues de pierre\
tendues vers l’avenir\
mais incapables d’y atteindre\
plantées là\
graines malades d’arbres\
qui ne pousseront jamais\
avancer à reculons\
profil grec du monde\
quatre notes dans le champ cosmique\
la route qui monte est la même que celle qui descend\
est-ce pensant à Sisyphe\
me dis-je\
qu’Héraclite eut la présence d’esprit de l’affirmer ?\
il n’y a pas d’énigme\
qui pourrait se retenir de rire ?\
qui pourrait se retenir de recommencer ?\
qui pourrait se retenir de mourir ?\
trait contre un peuple sans esprit\
puritain\
on cherche des volumes de peur\
de finir écrasé\
par tout le poids du passé\
toute la masse de l’avenir\
et se contente d’une architecture passable\
pour qui n’en peut plus d’espérer\
cependant que la voix\
elle\
parle\
elle que personne ne comprend\
le vent souffle\
mais la brume ne se dissipe pas\
au-dessus du volcan\
semble au contraire s’y arrimer\
nuage de fumée\
langue de feu\
mur de pollution\
tout se confond dans l’épaisseur de l’espace\
moins les choses sont claires\
et plus on s’efforce de les comprendre\
la voix parle\
et on n’en distingue pas le corps\
une idée\
une divinité\
l’âme\
qui traverse les chairs de la génération\
bien plus vieille que le bien plus vieux\
du premier de nos cadavres\
un essoufflement qui transperce le temps\
inconsistant\
le chant des pierres\
à se rendre fou\
s’égosiller\
comme si l’on n’en finissait jamais\
d’ergoter\
de croire\
de pleurer\
insolence du futur\
nom du devenir\
pour qui l’innocence ?\
exister est suave\
le point aveugle de l’horizon\
contraste des couleurs\
degrés de chaleur\
aveuglé par quelque chose qu’on ne voit pas\
qu’on pourrait voir ?\
peut-être\
aveuglé par ce que la langue charrie\
et qu’on pourrait appeler si l’on voulait\
âme\
pas de quelque chose d’autre qu’elle-même\
tout court\
âme\
tout coule\
tout ce qui nous pénètre passe à travers nous\
et nous délaisse\
laisse un peu moins mièvres\
un peu plus fous\
chaleur à en concevoir la fièvre\
dans les anfractuosités de l’être\
creusent les fables\
parler pour le plaisir de parler\
et puis se taire\
attendre en forme d’invoquer\
sortant de la cavité\
qui à la flûte\
à la lyre\
au piano\
la pensée\
sa respiration\
et chaque inspiration est une phrase\
une invocation\
vocalises sérielles\
une note après une note\
quelques sentences dans le champ érotique\
semences augurales\
est-ce qu’ensuite on tombe à genoux\
effondrement\
ou bien continue-t-on d’avancer\
épuisement ?\
qui se vide de sa substance\
délivre une parole\
où enter une science sans exemple\
pur murmure\
vrombissement\
comme le vent qui s’engouffre\
dans l’interstice\
trop étroit pour lui\
mugit toute la nuit et après le matin\
savoirs antiques quand le vent fécondait les femelles\
nos ancêtres\
la terre était plus légère alors\
un souffle\
une voix\
une exhalaison\
rien de pesant\
ici-bas devait se penser la tête en l’air\
force du vent\
tout pourrait être autrement\
les pieds sur terre\
la tête dans les nuages\
alcool la nuit\
sans nul dieu à qui dédier\
les égoïstes libations\
de nos peuplades onanistes\
je chante la noyade des naïades\
des ménades\
le suicide collectif\
canal historique\
dans la rivière\
ou dans le lac\
corps noyés\
sous le soleil qui les calcine\
met le feu aux cheveux\
eau bouillante\
éclaboussé\
nulle part où aller\
c’est l’univers embrasé\
impossible à embrasser\
antiques croyances\
quand on pensait\
qu’à la fin il y aurait\
un grand brasier\
incendie\
et qu’alors tout recommencerait\
da capo\
comme le chante la musique\
que la fin sera le début\
et que reviendront\
les choses\
les êtres\
les nuages\
l’orage\
explosions\
les morts et les vivants\
les vivants et les morts\
ne seront jamais qu’une seule\
et même personne\
da capo\
quelques notes dans le champ chromatique\
partition de l’univers\
tant qu’on ne sait faire\
la différence entre ce qui est grand\
et petit — infiniment\
je chante la noyade des naïades\
ces corps nus\
dans le ruisseau\
piscine naturelle et au-dessus\
un pont\
je marche\
mais je ne sais pas si je rêve\
ou si c’est la réalité\
corps nus dans le vide\
partout à la surface de la terre\
divinités débiles\
qu’on entend à peine\
respirer\
nature rature\
il y a tout un espace à parcourir\
un chien aboie\
et on presse le pas\
cours plus vite\
à en perdre haleine\
avant de s’effondrer quelque part\
dans les bras décharnés\
d’une déesse droguée\
antidépresseurs\
regard vide\
teint hâlé\
morsures\
brève histoire de notre humanité\
je chante la noyade des naïades\
qui peut se satisfaire de la vérité ?\
les rites ont perdu le don de parler\
même plus rien à inventer\
un dimanche\
dans un cimetière désert\
sous un soleil dur\
impeccable\
paysage de la méditerranée\
bleu vert ocre\
je contemple une tombe\
à laquelle je voudrais dire\
quelque chose\
à laquelle je voudrais\
avoir à dire\
quelque chose\
mais à laquelle je ne trouve rien\
que des banalités\
dans ma mémoire\
dans mon histoire\
dans mon corps\
la tombe est à l’ombre\
d’un olivier\
et moi\
je cherche un geste qui me précède\
un geste qui ait du sens\
mais il n’y en a pas\
un arrosoir à la main\
j’abreuve une plante qui se trouve là\
dans un pot laurier\
le jardin des pierres\
sous le regard excisé de la Méduse\
continue de pousser\
infinie croissance d’un peuple de déments\
on n’en finit pas de piétiner\
les cadavres\
telle est notre raison d’être\
notre raison de vivre\
un vivant vient\
et tous doivent mourir\
la chaleur me brûle\
dans le jardin des pierres\
je ne suis pas un homme\
je suis une rivière\
et je ruisselle\
me coule dans le devenir calcaire\
de la terre\
une pierre à la main\
sans nul geste antique\
primitif\
à imiter\
sans nul crâne à fracasser\
l’histoire à ma place s’en étant chargé\
à défaut de la chose en soi\
je charge la chose en moi\
oreille fétiche à qui me confier\
dans le jardin des pierres\
je devise dans l’assemblée des monstres\
déclame échevelé\
l’adieu aux membres inutiles\
et savoure la portée du drame\
il pleuvra le dernier jour de l’année\
je le sais\
nous viderons nos sacs\
dissipant le brouillard\
dans le ressac de phrases insensées\
mots d’ordre inaudibles\
à la pointe armée de notre avenir\
feu les signaux incultes\
feu les masses acharnées à l’espoir de continuer\
les monstres ont le temps pour eux\
qui contemplent impassibles\
l’histoire à l’arrêt\
j’ai chanté la noyade des naïades\
la dissolution de mon corps\
la dissolution de toutes choses\
on ne se baigne jamais deux fois dans le même fleuve\
raconte-t-on encore qu’Héraclite disait\
et c’est vrai\
qui voudrait se couler\
dans le moule informe de l’époque ?\
j’ai trop chanté\
quatre notes dans le champ hérétique\
maintenant\
laisse-moi exploser\
désirer la métamorphose des flammes\
destruction de la destruction\
feu à volonté.

