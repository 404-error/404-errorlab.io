---
persona:
  - Lucien Raphmaj
  - Marc Verlynde
title: "Désidentité dénominateur nom commun"
slug: "desidentite"
date: "2022-03-02"
echo:
  - esthétique
images:
  - "img/220301-lrmv-desidentite.jpg"
gridimages:
  - "img/220301-lrmv-desidentite-grid.jpg"
sourceimage: "https://commons.wikimedia.org/wiki/File:Angelico,_tebaide.jpg"
notes: "Ici se lit un entretien ouvert sur la critique littéraire, sur ses puissances et ses impuissances, et dont la question de l'idiotie est comme la première stase, liminaire, l'incarnation de cet état dont il nous paraissait important de témoigner comme critique en approfondissant notre perplexité."
summary: "Tant de fois cité, répété, si difficile à entendre. Pourtant sa parole critique est simple à résumer : sa formule est celle de l'amitié. Sans adresse. Il faudrait dire aussi l'inquiétude : être inquiet pour l'autre désigne l'ami. Une amitié de la bactérie, certes. De celle qui parfois phagocyte le discours d'autrui pour alimenter son vide affamé. Pourtant d'ordinaire la critique a horreur du vide, c'est même précisément sa nature. Son rôle a été de faire exister quelques livres face à la masse de plus en plus massive des livres publiés."
citation: "Une amitié de la bactérie, certes."
---

<div class="headers--onespace">

# f.a. --- fra angelico {#fa}

Dans l'idiotie des anges. J'ai toujours trouvé les anges idiots.
L'inverse n'étant pas toujours vrai. L'idiotie des anges --- leur part
d'absence et de silence, leur retrait silencieux dans lequel se murmure
la parole divine. Ce qui s'est nommé depuis Hölderlin l'ouvert on le
sent souffler sur les fresques de f.a., entre l'épée et la tête du
saint.

# a.a. & a.a.2 --- antonin artaud {#aa #aa2}

Le critique comme suicidé de la société. Une anecdote pour situer un
rapport critique. Quand il traduit *Le Moine* ou Lewis Carroll, il sait
être l'auteur, qu'on lui vole sa voix, on lui usurpe son corps.
Peut-être faudrait-il pousser la critique jusqu'à cette folie qui
libère. a.a un jour intime à [~~m.l.~~](#ml) de consigner son glossaire,
que son internité la plus radicale tient comme pratique de radieuse
amitié : le don d'une intuition. a.a. la timidité de l’amitié, on contemple chez lui le paroxysme et ses abîmes. On ne saurait l’évoquer que dans la réserve et le silence.

# r.b. --- roland barthes {#rb}

"J'ai varié, j'ai changé", obsession barthésienne de l'infidélité. Comme
si la lecture était une infidélité fidèle semblable à celle de la
traduction. De la lecture comme ambivalence comme art de l'ambiguïté :
apprendre à vivre cet indécidable du texte, son trouble et sa jouissance
provenant de ce qu'il est un désir inépuisable. Lecture et désir,
érotique de la lecture : le scandale que craignait r.b. n'a jamais
éclaté. On s'est détournés de la mythologie de la lecture pour
s'absorber dans sa consommation, on s'est détournés de la rhétorique,
des arts de lire qu'il professait. Apprendre à lire : voilà dont on
s'occupe toute sa vie quoique toute la vie ne s'y résume pas.

"Aujourd'hui, il m'est indifférent d'être moderne." Le deuil : un inouï
déjà advenu qui s'étend : la seule chose qui se partage. Sémiologie :
structurer les fragments d'un discours de ce qui a déjà été vécu (le
deuil et l'amour) et qui toujours survient comme l'épreuve d'une
singularité. La préparation du roman : jusqu'au bout l'espoir d'une
autre forme pour répondre à son oscillation entre la mort de l'auteur et
le retour, endeuillé et photographique, du sujet. Une vie, en amateur,
dans le signe : tout contre ce suprême organe d'incompréhension que
serait le Je.

# g.b. --- georges bataille {#gb}

"De cet ami, comment accepter de parler ?" ([m.b.](#mb), "L'amitié")
Comment s'égaler au défi que nous lance Bataille à la suite de
Nietzsche, celle d'une parole décapitée, ouverte au divin sans
fondement, à la souveraineté ? Que Bataille ait cherché ces "expériences
limites" des rires et des larmes, du meurtre et de l'angoisse, de
l'érotisme et de la poésie, il ne fait pas de doute. Il en tire une
critique orientée sur la possibilité même de parler : ses saintes et ses
prostituées sont comme la Monelle de Schwob, de sublimes idiotes.
Irrécupérable pourtant, clandestin (si peu de son œuvre à son nom), g.b.
radicalement non exemplaire, dont il n'est au fond guère possible de se
réclamer. Souillure et transgression sont si loin.

# c.b. --- charles baudelaire {#cb}

L'escrime et la flânerie : flairer les vers au ras du pavé. La liberté
dans ses contradictions : modernité. De la concentration à la
vaporisation du Moi. Mon cœur mis à nu \|\| les salons : le poète
seulement dans sa poétique, ça vous file un de ces spleens. Vœu de faire
non seulement de l'art, mais de sa vie même cette insolente pointe à
laquelle on se pique ("la plaie et le couteau..."). On ne dira jamais
assez combien on revient sans cesse à c.b. comme à une rage de dent et
de beauté, à ce *drama queen* excité par la mélancolie, critique de tous
les temps, lui de l'éternel commencement, des départs et des rêveries :
"À quoi bon exécuter des projets, puisque le projet est en lui-même une
jouissance suffisante ?" De l'art de l'ellipse et du suspens.

# p.b. --- pierre bayard {#pb}

De la lecture comme jeu défaisant le jeu élitiste et universitaire ---
donnant à l'invention, à l'hypothèse toute sa place en critique
littéraire. Lire sans lire, lire à travers les livres, défaire les noms
de l'auteur en les réattribuant, refaire les œuvres, écrire en
réécriture, autant de mouvements dont cependant il faudrait accomplir le
programme avec un sérieux qui fait de ce jeu l'essentiel de la
littérature : cet écart producteur de sens, éclair dans la matrice du
langage. Étrangement du langage à la Dinepr Voloust ([a.v&m.p.](#avmp)).

# w.b. --- walter benjamin {#wb}

On suit toujours son ange de l'histoire, tournés vers les textes passés,
mais poussés par le souffle des textes à venir. On modifie pour
l'occasion l'histoire. On aime à se rappeler ainsi des anges que l'on
croise sans cesse, à l'instar du critique, intercesseur fantôme, absent,
retiré du sens, idiot toujours, et terrible ("Jeder Engel ist
schrecklich"). Le regard de w.b. est celui aussi d'une autre forme
d'essai, celui de l'*Einbahnstraße*, dessinant la forme d'une vie, la
forme de tant de villes labyrinthes, de paroles et de silences, à
travers ces images rapides, ces clichés dont [c.b.](#cb) disait qu'ils
étaient la réalisation ultime de la poésie.

# m.b. --- maurice blanchot {#mb}

Tant de fois cité, répété, si difficile à entendre. Pourtant sa parole
critique est simple à résumer : sa formule est celle de l'amitié. Sans
adresse. Il faudrait dire aussi l'inquiétude : être inquiet pour
l'autre, désigne l'ami. Une amitié de la bactérie, certes. De celle qui
parfois phagocyte le discours d'autrui pour alimenter son vide affamé.
Pourtant d'ordinaire la critique a horreur du vide, c'est même
précisément sa nature. Son rôle a été de faire exister quelques livres
face à la masse de plus en plus massive des livres publiés. Paradoxe de
ce critique d'[<span id="regime">Ancien Régime</span>](#terre), souverain au-dessus de la masse,
jugeant ce qui doit vivre ou mourir, et désirant qu'on le révère, lui,
qu'on le lise et que l'on publie moins (ce qui le renverrait à sa
profonde et véritable inutilité).

m.b. le premier --- mais cette prééminence ne veut rien dire lui qui
sait l'importance d'être "le dernier à parler" --- m.b., au contraire, a
donc pris au sérieux le vide de la critique et le vide de sa parole. Il
en fait l'essence même, vide en mouvement, de l'écriture --- mêlant en
cela écriture critique et écriture de fiction. On ne peut pas dire qu'il
ait fait œuvre de "théorie", de spéculation, de vision transcendantale
sur la littérature et son ciel. Au contraire : un ciel vide, une parole
errante, une absence d'essence et d'origine vers lesquelles on se
tourne, telles sont plutôt les figures de sa pensée métalittéraire.
Pensée aussi de l'effacement du critique et parole de neutralisation de
l'auctorialité (cf. *L'espace littéraire*). Dans cette œuvre critique,
d'article en article, de loin en loin, on repère et apprend l'amitié
dont il parle, amitié jamais dans la hauteur mais dans un "communisme de
pensée", dans la distance, dans une "amitié stellaire" ([f.n.](#fn)),
voire galactique : abreuvons-nous encore longtemps de ce lait.

# p.b.2 --- patrice blouin {#pb2}

Au forceps, p.b. s'exorbite, s'invente un genre à Soi, des voyages dans
le temps, des *buffers zone*s. Popeye revenant, dans l'ailleurs de
l'anamorphose, tente d'enterrer ses fantômes pour, mouvement même de
l'écriture, qu'ils reviennent. À la découpe, *cut-up* et *match-up*, il
dessine une auto-science-fiction ; proposition théorique (au sens
d'autant intime que collectif) dont certaines revenances tant nous
parlent.

# a.b. --- andré breton {#ab}

Malgré tout. J'écris pour trouver des hommes (et les meufs, t'en fais
quoi, foutu phallocrate ?) disait-il. Vaché, Soupault, Aragon :
l'émulation de l'amitié dans sa scission. Mais surtout l'infortune
continue de son écriture qui ne survient que sous sa forme critique.

# c.c. --- cristina campo {#cc}

Il y a chez c.c. quelque chose *Grand Siècle*, une superbe intempestive,
une Madame de Lafayette du 20e siècle italien. Autre chose donc. Quelque
chose d'une résistance et d'une mystique absolue dont la parole est si
sûre d'elle, si brève et intense. Une pesée d'or et de silence. C'est à
cette aune qu'elle juge dans *Les impardonnables*, qu'elle se met
elle-même du côté des impardonnables. Quelque chose de nietzschéen dans
cette cruauté. "La tigresse absence", ce titre féminisé d'un de ses
recueils de poèmes lui va comme un gant, un gant qu'on aimerait lui
emprunter.

# j.-p.c. --- jean-philippe cazier {#jpc}

Poète diacritique en un sens ambivalent, en un signe vers son absence,
en un régime de sens permettant le suspens et la distinction ---
peut-être aussi la désextinction de la critique avec quelques autres
revues en ligne manifestant l'envie d'essayer de nouvelles formes de
pensée à l'essai dans la critique. Par-delà la critique au jour le jour
--- *hodie mihi, cras tibi* --- à travers le jour sombre de tous les
jours, j.-p.c. a aussi invoqué le chaos sensible (*Das Sensible Chaos*)
de John Lovecraft-Carter et sa *Théorie des MultiRêves* dont les
prolongements tentaculaires viennent nous enlacer de leurs petits
cerveaux nerveux jusque dans ce texte.

# p.c. --- paul celan {#pc}

De la parole Celan il faut dire qu'elle interdit, qu'elle semble si
concentrée, si acharnée à se retirer du poème, du commentaire, de la
beauté qu'elle aussi nous invite à penser, à faire du poème l'expérience
radicale du retrait de la poésie. La critique à ce point nous apprend à
défaillir.

# p.c.2 --- pierre cendors {#pc2}

D'un nom qui s'est fait visage de nuit à travers ses livres. Une
incontinuité. Ses livres soufflent et parlent dans un état indécis de
rêverie. Peut-on parler des livres comme l'on parle des rêves ? En
refusant tout symbolisme, tout biographisme, privilégiant les
transformations intimes de l'espace et du temps, les flous et les
bonheurs des désidentifications. Être et "désêtre", comme il dit d'un
même mouvement, cherchant une origine toujours raturée, biffée
([~~m.l.~~](#ml)), laissant place à la trace et au spectral
([j.d.](#jd)).

# j.c. --- javier cercas {#jc}

Romancier, critique, et mieux encore que le milan de Tchécoslovaquie.
Une autre vision de l'engagement. Et pourtant le même amour du roman
dans son ambiguïté, dans son origine don quichottesque. Dans *Le point
aveugle*, j.c. donne des leçons de littérature, c'est-à-dire
d'aveuglement, évidemment. Toute vision occulte un point aveugle que le
cerveau comble en permanence. La personne qui écrit cherche en vain à
rétablir la vérité, à défaire sa vision : seule l'action de lire
décidera de cet indécidable dont surgissent les possibles du texte.

# h.c. --- hélène cixous {#hc}

Prométhéa, dieu enfin au féminin. Écrire : confondre ce que l'on admire,
attraction et amour, ce dont on parle et celle qui en parle.

Elle l'angoisse, le messie, la parole errante et revenante, revenant au
point de poignance de Montaigne, à ce qui s'écrit autrement qu'en
biographie, qu'en essai, qu'en théâtre, qu'en philosophie, choisissant
de mêler ces lignes mélodiques de la pensée dans un entrelacs vivant où
la pensée trouve ses chemins, par d'infinis détours --- infini détour :
écriture.

# m.c. --- marie cosnay {#mc}

À notre humanité dans l'accueil inconsidéré de l'étranger. Pour que les
histoires comme elle dit deviennent autres, hôtes les unes des autres.
C'est ceci qui ici se joue : aucunement une biographie, la fixation d'un
auteur à son identité, mais plutôt la capture des liens, des amitiés,
qu'il entretient avec les autres évoqués, avec nous.

# r.c. --- rené crevel {#rc}

Gueule d'ange, cœur bizarre, hôte d'une bactérie tueuse mais aussi
d'idées brillantes et éclectiques, r.c. joue frénétiquement dans notre
imaginaire de son clavecin, réarrangeant et dérangeant l'essai
ronronnant sur les genoux du seigneur de Montaigne. Art du toucher comme
art de lecture : façon de découper le texte : presto, allegro, lento.
Vitesse musicale. Détours, critique, corps difficile, folie, r.c. a su
former lui aussi autre chose qu'une pose faite de la prose surréaliste
et a témoigné d'une expérience vitale de la littérature comme expérience
qui nous engage dans l'impossible --- qui nous engage tout entier à
transformer le monde. Communiste des cœurs, schismatique.

# m.g.d. --- maurice g. dantec {#mgd}

Dante(s)c : vortex. Peut-être dire d'abord comment l'amitié que l'on
porte à tous les auteurs écoutés ici est aussi une façon d'éprouver ce
que furent les figurations d'auteurs dont peu à peu on se détache. Tout
simplement : m.g.d., très fin de siècle, ou l'exaltation hallucinée de
l'écriture. La technique et sa peur. L'incarnation douteuse, verbeuse à
l'occasion, d'une littérature au futur.

Dante(s)c : cortex. Façon dont l'intelligence déconne à fond, vampirise
tout, machine qui se grippe, délire d&g, la patristique, combine,
complote, trafique, thrille, devient illisible, reprend, mélange tous
les genres, se trompe sans cesse et gravement. Anges électriques,
auréoles de néons, mythomanie du monde. Et dans ce théâtre des
opérations désastreuses, dans ces ratages, quelque chose d'essentiel :
que l'on peut lire et critiquer grâce à cette différence, grâce à ce
vertige, à l'échec de sa pensée, de sa littérature, grâce à ce quelque
chose d'autre que de l'amitié (car sur fond de baston idéologique).

# g.d. --- gilles deleuze {#gd}

On lui doit tant dans notre critique. Pour débarrasser de la table
clinique de la critique tous les outils de dissection et pour installer
le banquet où les mots et les corps, où les strates et les liquides ont
leur place. Faire de la critique, c'est se situer, se déplacer, faire de
la création, des séries, des feuilletons, des schizes. Tant de choses
qu'on devrait expliquer ici. Tout l'apport de la philosophie de g.d. à
tous les arts. Et à la critique comme le reste. Qu'on relise *Proust et
les signes*. Quelle merveille de lecture. On se sent idiot et heureux
devant tant d'idiotie..

# d&g --- deleuze&guattari {#dg}

Parmi les repères intensifs de notre contemporain, le couplage d&g a
produit tant de concepts, de plateaux de notre disque-monde
philosophique qu'il faut considérer tout ce que la littérature et la
critique leur doivent aujourd'hui : prolifération des séries, agencement
machinal, littérature mineure, visagéité, plan d'immanence, ritournelle,
devenirs. Avec eux on voudrait produire des langues schizos, et faire de
la critique autre chose que le commentaire racinaire, mais une création
ne cessant à son tour de susciter des drageons. Pourtant nos stases ne
sont pas ces labyrinthes de prose, ce sont plutôt des entretiens, des
divagations au sens du reclus de la rue de Rome : considérations
théorico-poétiques.

# j.d. --- jacques derrida {#jd}

Qu'ici le spectre de j.d. vienne et ne cesse de revenir n'est pas dire
que la critique s'identifie à la déconstruction. Les malentendus qui ont
fait de la déconstruction une méthode critique de lecture sont
innombrables. La pensée de Derrida, nous la conjurons pour en hériter à
notre manière, pour la faire travailler à autre chose qu'elle-même, à la
faire déjouer tout le cercle herméneutique --- à faire que la critique
soit toujours inquiète, toujours ouverte, ne pouvant s'arrêter au
commentaire, à la structure et à son renversement, interrogeant
toujours.

# a.d. --- anne dufourmentelle {#ad}

Puissance du rêve, intelligence de la douceur, et l'inverse, bien sûr.
Ces textes ont une manière d'insister en nous par une sensibilité qui
défait un certain nombre de partages que l'on avait établis pour
produire une fluidité lucide qui se joue l'essai et à la psychanalyse,
de la confession et à la philosophie. Ce qu'elle invente par courts
textes c'est une rhapsodie mélodique en guise d'essai, suite de vagues
avançant comme une marée sur notre *grève obscure et désolée*.

# m.d. --- marguerite duras {#md}

m.d. est presque l'inventrice de ce retrait du nom générique dans la
lettre. Ravissement de Lol V Stein. Un monde dans un V. Un vent de
folie, une attente, un passé. Des paysages et des intonations.
Dépersonnaliser pour passer à travers le neutre de la langue, et accéder
à ces purs mouvements de désirs, de corporéités, de mots et d'avenir.

# m.f. --- michel foucault {#mf}

Qu'est-ce que l'actualité, le maintenant, le suspens de la critique ?
Peut-être quelque chose qui interroge l'éthique, le rapport à l'autre
--- nous disions : le rapport à l'amitié, à l'écoute. Foucault se place
dans cette même attention aux discours des autres, de ceux rejetés dans
une altérité radicale (anormaux, malades, fous, prisonniers), exclus des
discours --- et par les discours. À ce langage dominant, il oppose cette
parole glissante, discrète ([p.z.](#pz)). "Dans le discours
qu'aujourd'hui je dois tenir, et dans ceux qu'il me faudra tenir ici,
pendant des années peut-être, j'aurais voulu pouvoir me glisser
subrepticement. Plutôt que de prendre la parole, j'aurais voulu être
enveloppé par elle, et porté bien au-delà de tout commencement possible.
J'aurais aimé m'apercevoir qu'au moment de parler une voix sans nom me
précédait depuis longtemps : il m'aurait suffi alors d'enchaîner, de
poursuivre la phrase, de me loger, sans qu'on y prenne bien garde, dans
ses interstices, comme si elle m'avait fait signe en se tenant, un
instant, en suspens. De commencement, il n'y en aurait donc pas ; et au
lieu d'être celui dont vient le discours, je serais plutôt au hasard de
son déroulement, une mince lacune, le point de sa disparition possible."
(*L'ordre du discours*). Ce qui se poursuit dans son rêve d'une critique
effacée, végétale, dont la douceur ne nous est pas étrangère : "Je ne
peux m'empêcher de penser à une critique qui ne chercherait pas à juger,
mais à faire exister une œuvre, un livre, une phrase, une idée ; elle
allumerait des feux, regarderait l'herbe pousser, écouterait le vent et
saisirait l'écume au vol pour l'éparpiller. Elle multiplierait non les
jugements, mais les signes d'existence ; elle les appellerait, les
tirerait de leur sommeil. Elle les inventerait parfois ? Tant mieux,
tant mieux. La critique par sentence m'endort ; j'aimerais une critique
par scintillements imaginatifs. Elle ne serait pas souveraine ni vêtue
de rouge. Elle porterait l'éclair des orages possibles." ("Le philosophe
masqué").

# r.g.-l. --- roger gilbert-lecomte {#rgl}

Du Grand Jeu toujours rappeler la puissance existentielle contre une
esthétique surréaliste héritière du post-symbolisme. Et puis l'amitié
comme une pensée magique, mystique des phrères simplistes. "Vous allez
*souffrir* sur le rythme de la respiration cosmique." Et ce sera tant
mieux --- inventer un casse-dogme, avec Daumal --- pour qu'enfin nous
fassions face à notre désespoir. De Daumal, par ce détour, rappeler
aussi ce titre qui nous sert de tutélaire hantise : *tu t'es toujours
trompé*. La drogue aussi. Entrer dans l'histoire des cataclysmes plutôt
que celle des manuels d'histoire littéraire. À ta clandestinité continue
r.g.-l.

# e.g. --- edouard glissant {#eg}

Penser en archipels et en Relation est bien l'ontologie vers laquelle on
se tourne en se détournant de la pensée de l'être, de l'identité, de
l'ontologie enracinée. Critique de la métaphysique occidentale qui n'est
pas un simple discours mais une forme conjointe de politique de l'amitié
et un décentrement de notre façon d'interagir, de lire et de conter.
C'est renoncer à un ensemble de perspectives qui nous permettent de
parler "en présence de toutes les langues du monde" pour faire œuvre de
paysage avec sa langue. Critique-artiste mais pas impressionniste.
Oeuvre d'attachement et de détachement, de processus vitaux et mentaux,
par le détour de l'autre et du Divers. "c'était déjà le sourd désir de
partir, de participer, d'épuiser la diversité irrémédiable --- mais qui
sans cesse provoque à la réduire en unique vérité --- du monde."

# d.h. --- donna haraway {#dh}

Un glossaire sans d.h. est triste. Avec elle les singes, les chiens et
les cyborgs, les poulpes et les féministes du Chtulucène entrent avec
une joie tout animale. Idiote. Donna, Donna, Donna, je me demande
pourquoi il n'y a pas de chansons sur toi. Ici on peut parler de toi à
tant d'égards. Pour la lecture généreuse. Pour le bestiaire qu'on ne
cesse de convoquer. Pour les savoirs situés s'énonçant depuis notre
désastre où tu souris à pleines dents. Serait-ce le soleil de Californie
? Dans les câbles, nous nous inventons des sommeils électriques où l'on
rêve à ces vies multiples et symbiotiques dont tu nous parles, amie toi
aussi de la bactérie.

# m.h. --- martin heidegger {#mh}

Trop de philosophie par ici. Air trop rare. Pas de parole possible.
Parole de maîtrise sur le haut de sa Montagne Noire. On ne cite m.h. ici
que pour le critiquer. Pour critiquer ce qu'il a dit de Hölderlin, ce
qu'il a dit de Nietzsche, pour se mettre à l'écoute de la rencontre avec
[p.c.](#pc) --- de cette impensable et de cet impardonnable. De cette
philosophie on aura affaire qu'avec détour --- par [m.b.](#mb), par
[e.l.](#el), par [r.c.](#rc), par tant d'autres.

# f.h. --- friedrich hölderlin {#fh}

La parole en faille, en éclair d'Hölderlin. Les mots de la critique
adorent en sortir.

# u.j. --- uwe johnson {#uj}

Un silence, une trahison. Les présences les plus fortes seraient-elles
celles dont on ne saurait rien dire ? Taisons-nous tant il ne s'agit de
prendre à défaut, de se cacher derrière l'autorité que les noms ne
sauraient avoir. Une des vies (la plus méconnue) du *Monde des amants*
de [m.s.](#fn) qui comme [l.t.2](#lt2), comme [f.n.](#fn), comme tant d'autres se lit chez lui
depuis sa fin (thanatologie plutôt que biographie), depuis l'impossible,
le décept, l'excession, la consolation (inaccessible)  : « Des trois
passions que j'ai eues, a dû se dire Johnson, \[...\] artistique,
amoureuse, politique, la passion politique est la seule qui ait
constitué pour moi une déconvenue, ou qui dût en constituer une, la
déconvenue ou la malédiction de mon existence. La malédiction de mon
existence a été politique, de part en part. \[...\] À la vérité, parce
que Johnson s'est interdit de penser ce qu'il aurait dû penser, qui
s'est interdit de penser que la politique est toujours une malédiction
et n'est que malédiction. »

# r.j. --- rodhlann jornod {#rj}

Grâce lui soit rendue ([m.n.](#mn)). Toute tentative critique se nourrit
de cette absence-là, de ce miracle-là, de cette confiance-là, semblable
à celle que l'abrupt ouvrier rend possible par ses inventions, ses
suggestions, par notre correspondance qui à elle seule forme une œuvre
secrète, souterraine, amicale.

# f.k. --- franz kafka {#fk}

On s'est écrit des lettres fantômes. Mais qui n'a pas correspondu avec
f.k. en pensée, en rigolant à s'en faire mal aux élytres ? Il demeure un
de mes morts préférés. Quelqu'un avec qui on peut rire. Avec qui on peut
se taire sur l'énigme de l'écriture, de sa loi. Le signe qu'on s'entend
bien même quand il prend ses airs tragiques qu'on lui connaît bien et
qui nous correspondent bien aussi. f.k l'ami aussi. Max, Felice Milena :
l'enthousiasme d'un rapport magnifié à l'autre. Excessif qui sait mais
ici se préserve l'espoir. L'amitié comme pratique de l'[<span id="excedentb">excédent</span>](#excedenta).
Comment peut-on en faire trop dans la confiance accordée à l'autre ?

# j.k. --- john keats {#jk}

"*Here lies One Whose Name was writ in Water*". L'épitaphe de j.k. coule
à travers les siècles jusqu'à nous pour nous dire à quel point
l'attention à l'autre du monde, au ruisseau du temps, des mots, nous
porte à nous défaire du nom, du moi, pour se porter vers cet impersonnel
traversant les sensations, les bêtes, les pierres --- n'étant ni énergie
cosmique ni fantasme, mais simplement l'infinie perspective du réel dans
sa matérialité absolue.

# ~~m.l.~~ --- michel leiris {#ml}

Une constellation de mots dont il déplie, et biffe, les tangages. Au
début, une erreur. Un mot malentendu, un soldat qui se brise. En faire
une intenable règle du jeu, une marge d'inachèvement. Un coma après
avoir reçu le prix des Critiques, décide de ne pas suivre de cure
psychanalytique avec j.l. : quels calembours révélateurs
en auraient été inventés ! Pourtant lucidité et exigence à son plus
haut. Parler de soi, après l'inatteignable âge d'homme seulement comme
un essai de self-fabrication toujours critique d'une perception d'un Soi
(vide --- on le sait) qui partirait du summum du subjectif (l'*ombre*
d'une corne de taureau) pour atteindre à l'objectif. À cette carte sur
table du réalisme comme le dira celui qui toujours se réclamera du
surréalisme. ~~m.l.~~ l'ami. [g.b.](#gb) en lui voyait l'initié. Il
s'efface derrière ses nuits sans nuit et quelques jours sans jour. Ses
gloses hantent. Un rapport aussi à la pensée, à la philosophie : rien
que l'hermétisme de ses formules. On veut une philosophie qui soit
amalgame : magie de ce qu'on y trouve qui, peut-être, n'y était pas.

# e.l. --- emmanuel levinas {#el}

Souvent on n'y a rien compris. On le lit comme on est fasciné par un
visage. e.l. Le visage de la couverture de *Silens Moon* de
[p.c.2](#pc2) Une question où se cernent les ombres de l'amitié : ils
causaient de quoi, lui et [m.b.](#mb), à la fac de Strasbourg ? e.l. par
le détour imagé de l'amitié. D'un visage jamais on oublie la première
rencontre. *Comment je me suis disputé...* : « C'est Kippour, si tu veux
me pardonner, j'accepte tes excuses. » / « Je te connais pas. » Un doigt
coincé dans la porte. Desplechin aurait emprunté à e.l. cette relecture
de la Torah. Douloureuse incarnation de l'amitié.

Humanisme de l'autre homme, et au-delà : souvenir du chien Bobby dans le
camp, le seul à leur reconnaître un visage quand leurs tortionnaires le
leur ont dénié.

# c.l. --- clarice lispector {#cl}

Passion selon c.l. : de cette littérature où le monde et le rêve
s'emmêlent en tant de perspectives divines,

# h.p.l. --- howard philip lovecraft {#hpl}

*Weird Tales*. Grand Ancien, irrécupérable comme la peur de l'autre.
Cthulhu, t'en souviens-tu ? Une de mes premières lectures : fidélité
indéfectible. Un pote un peu *creepy* pour l'adolescent étrange que nous
fûmes. On n'ose pas le relire --- rien que des revenances. Nos mauvaises
références. h.p.l. l'isolé de Providence, sa correspondance : les grandes
amitiés ainsi formées, à l'écrit. Tout aussi irrécupérable, on pense à
r.e.h. : Conan et Solomon Kane. Un autre suicidé, à trente-cinq ans.
Irrécupérable. On se souvient du rejet de la civilisation qui m'a fondé.
h.p.l. et sa renaissance virtuelle, au jour le jour, sur le Tiers Livre.

D'h.p.l. retenir aussi ce fantastique renversement de perspective : nous
sommes l'espèce invasive d'une Terre qui appartient aux Grands Anciens,
arrivés bien avant nous, mais depuis un ailleurs incommensurable. Nous
sommes de ces lectures arrivées bien avant nous, venant d'un dehors
insensé. Nous sommes de ce [langage](/stase-seconde/#fantastique), de cette angoisse qui ne nous appartient pas, qui nous
désapproprie, étant arrivés bien avant nous. Angoisse du langage qui
devrait s'écrire aussi à la manière d'une cosmologie métacritique. Et
pourtant, cette divinité du noir passé et de l'ignoble avenir, c'est en
nous qu'elle réside, divinité mutante et éclatée, c'est à nous d'en
porter et d'en manifester la folie, jusqu'à ce que la lecture et
l'écriture, passé et avenir, deviennent la métamorphose de notre présent
et de notre pensée dans la stase transitoire de l'instant (stase-seconde
de la lecture).

# m.l.2 --- malcom lowry {#ml2}

Alchimique amalgame entre l'auteur et ses personnages ou simple
expression de la permanence de hantises contradictoires. Tenace
fantomale présence du Consul. Nos fantômes peut-être ne sont que cela :
itérations de l'insuffisance de leurs apparitions. Tenace panique de la
revenance. Rêvons-en les interstices. Écrire c'est revenir, sous une
lune caustique, contempler nos désirs prophétiques, politiques. De m.l.2,
on pourrait dire la magie et autres kabbales, les signes enfouis,
l'ivresse d'une glose infinie, on devrait surtout spéculer l'ombre de la
guerre d'Espagne. Ou autre question fantomale : quel sens aurait une
présence à laquelle on ne saurait entièrement croire, mais qui revient ?
La part de collectif d'un désir de rédemption individuelle portée, comme
on dit, à sa dernière extrémité, incarnée dans l'alcool, ses manques et
ses hallucinations. Spectrales présences. 

# j.m. --- joyce mansour {#jm}

Il y a chez elle le cri des oiseaux, le rire vertigineux de la femelle
du requin, le soie des orchidées ultranoires nées dans les nuits
brouillées par le blizzard spatial. L'interruption du corps dans la
parole et l'image surréaliste en fait une poésie. Scandale de son
effacement de l'histoire.

# e.m. --- étienne michelet {#em}

Confrère abrüpt. La poésie défait souvent la critique. La déchire. Car
plus que tout résiste au commentaire, au résumé, à l'intrigue. On en
parle avec les invisibles. Avec le reste : la littérature, la mort, les
anges. Non pas qu'on croie à un arrière-monde, mais que quelque chose du
sacré et du démoniaque, de l'ouvert et de l'impur soient bien de ce
monde, ce seul monde et ce monde seul. Cela dont nous parle la bouche,
le son, les mots d'e.m.

# m.n. --- maurice nadeau {#mn}

On oublie souvent ces passeurs discrets que sont les directeurs de
revues, les éditeurs et pourtant, leur part dans les lettres est
incroyable. On doit tant aux amitiés incroyables de m.n. pour faire
exister des livres difficiles et beaux, limpides comme le ciel, rapides
comme le soleil. Tant de noms qu'on pourrait citer, éberlué.e.s. *Grâces
leur soient rendues* a été le titre de la somme de réflexions qu'il a
eues à la fin de sa propre vie, rassemblant ce qu'il, dans sa vie, doit
aux autres, et nous livrant ainsi, ce que nous lui devons de
reconnaissance. On peut aussi dire sa fidélité non alignée. : m.n., lit
tout, dans la somme de ces écrits critiques se devine un curieux
portrait de lui en absent. Un parcours critique qui ne soit pas
l'histoire d'une contestation perpétuelle n'aurait que peu de valeur.
Lire m.n., c'est aussi éprouver la manière dont un critique, [<span id="terre">terre à
terre</span>](/stase-seconde/#terre), fait de sa vie un roman : le résumé de roman dont il ne
reste que cela.

# j.-l.n.&p.l.-l. --- jean-luc nancy & philippe lacoue-labarthe {#jlnpll}

Couplage d'esprit à la d&g produisant un ensemble de propositions où
l'allemand sourd dans le texte, et où "l'absolu littéraire" se redonne
au-delà du kitsch romantique comme vertige de la synthèse et de la
transfiguration de la pensée. Projet monstrueux dont émergera --- se
détachera --- Hegel. Mais le soleil de minuit de l'Athenaeum, les
pensées des Schlegel, Novalis, Schelling, Fichte, nous donne grâce à eux
encore beaucoup à réfléchir.

# f.n. --- friedrich nietzsche {#fn}

"Pourquoi je suis si avisé ?" f.n. défie les critiques. Les trompe. Les
induit en erreur. "Qui pense un peu profond sait bien qu'il aura
toujours tort, qu'il agisse et juge comme il veut." Fait trembler l'idée
de vérité, faisant apparaître la vérité comme idole. La ramenant à la
Terre. Lui comme l'insensé qui annonce la mort de dieu. Autre idiot.
Même son Zarathoustra est un idiot enseignant un détachement de toutes
les paroles de maîtrise et d'évangile. De même la littérature, la
critique, ne sont pas dans le ciel du système solaire, mais ici-bas, et
dans l'erreur --- dans l'erreur qui cependant a pour valeur (il faut, on
se le rappelle se créer soi-même de nouvelles valeurs, superbement,
égoïstement nous dit-il) la probité. De cette probité qui n'exclut pas
l'amitié (cf. [m.b.](#mb)) : car au contraire à l'amour du prochain
(article / de foi) il substitue "l'amitié du plus lointain" (*Ainsi
parlait Zarathoustra*), amitié stellaire disait-il dans le *Gai savoir*.

# n. --- novalis {#n}

"Rêver de la fleur bleue n'est plus de saison" avertissait [w.b.](#wb) en
parlant du "kitsch onirique" du 20e siècle et cette prophétie
benjaminienne était ô combien bien vue alors qu'il analysait la
naissance du surréalisme et son rapport à l'image. D'*Henri
d'Ofterdingen*, on a retenu de la "fleur bleue" non cet absolu
cristallin et presque divin, cette plante de diamant et de sève, de
passé et d'avenir, mais la caricature d'un amour ridiculement ivre de
poésie. "Die Poesie ist das *echt* absolut Reelle", ce principe de vie
et de folie, d'incandescence, n. l'a porté très loin dans sa courte vie :
*Brouillon général ; Encyclopédie* et *Pollens* devant diffuser son
*witz* et croître dans nos esprit en de nouvelles forêts.

Conjurer le destin du romantisme, du surréalisme, avec les penseurs dit
postmodernes est ce que l'on doit essayer de faire.

# a.p. --- alejandra pizarnik {#ap}

On se consume en sa poésie d'une flamme lilas. On voudrait vivre et lire
et écrire que dans les ombres projetées de ce feu, dans le tourbillon de
cendre s'envolant à travers les langues, le temps et l'espace.

# p.q. --- pascal quignard {#pq}

Non pas qu'on prise tant que cela le latin chinois du grand baroqueux
devant l'Éternel, mais pourtant s'y reconnaissant malgré tout. Les
Boutès de la littérature, plongeant retrouver les absentes sirènes qui
n'étaient "que des bruits naturels" ([m.b.](#mb)), cette passion triste
de la perdition n'est pas la nôtre : que le critique prophétise le passé
et l'avenir dans une solitude essentielle venue du fond de l'œuvre c'est
bien plutôt cette joie, cette ardeur, cette passion de la lecture qu'il
faut manifester. Les ombres parlent dans les textes de p.q., instaurant
une sorte de ventriloquie qui n'est pas inintéressant, traçant aussi un
art sorcier du spicilège, un art du fragment et de l'esquisse, de
l'évocation et du suspens qui nous est familier. Langues-fantômes et
vivantes et mortes d'un même mouvement.

# l.r. --- lucien raphmaj {#lr}

Autre spectre hanté par la littérature. Ne se reconnaissant pas.
Cherchant des voies de travers dans l'écriture telle qu'elle est
perpétuelle métamorphose transmédiatique : *latérature*, dit-il. Dans
la contre-nuit où parfois il transparaît, collecte des comètes,
diagnose, lapidaire, des météores. Certains se vantent d'avoir trouvé
des objets, des photographies, des dessins où se devine ce qu'il sera.

# a.r. --- agnès rouzier {#ar}

Exercice d'admiration. "Peut-être faut-il disparaître en quelque sorte
pour rentrer en rapport avec ce texte", disait [m.b.](#mb) de son texte
*Non, rien*. Mettant au jour ce qu'est la littérature comme la lecture :
hantise, revenance de certaines phrases, configurant d'un signe secret
notre imaginaire.

# l.a.s. --- lou andreas-salomé {#las}

Nom de tentatrice. Mais la tentation n'est pas là. La morale, la faute,
la tradition et la trahison ne sont pas dans les Tables où elle inscrit
les aventures de sa vie. Sa tentation n'est pas là. Sa tentation est la
vie et la pensée. La vie et la pensée tenues ensemble dans le mouvement
du devenir, dans ces grands courants qui emportent les révolutions
mentales de la fin du XIXe siècle A.D. et la naissance de l'affreux
XXe siècle (Anno Terroris). Alors il faudrait entendre son nom comme
celui d'une éducatrice. Éducatrice, elle l'était, elle l'est encore,
elle qui est vraiment tragique, puisqu'elle aura voulu tout, et le rire
et les gouffres, et la pensée et l'amour, et l'indépendance et son prix.
On y revient à l'occasion du livre de [m.s.](#ms), depuis le rapport de
l'amour à la pensée. Que l'œuvre de l.a.s. ayant survécue dans la
postérité soit *Ma vie*, donne à penser combien, mieux qu'une
prophétesse, elle aura été force d'accomplissement et de vie auquel le
penseur du devenir lui-même n'a pas su consentir jusqu'au bout, jusqu'à
l'amour.

Dire aussi, en même temps, l'inachèvement d'un nom : tenace fantôme.
l.a.s., une rouge couverture, son livre sur [f.n.](#fn) dont rien, ou si peu,
nous revient. La désappartenance de la lecture est sans doute aussi
ceci : des souvenirs abolis de lectures à moitié faites, de tacites
réserves. Une image, pourtant, une des rares de [f.n.](#fn) transite à travers
l.a.s. : un chariot et un fouet. Notre difficulté à ne pas voir le
kitsch de cela. Un mythe pour se débarrasser des mythes dirait [m.s.](#ms) ?
L'horizon, malgré tout, par désidentification, de s'excepter de ces
mythes un peu trop grands. 

# c.s. --- camille sova {#cs}

Que la poésie soit la synthèse, c'est-à-dire le dépassement, de la
philosophie, c'est ce que l'on doit reprendre des romantiques en
découpant les journaux de bien-être pour mettre en pièce cet inconscient
collectif inconsistant et reformuler depuis notre étrangeté fondamentale
ces phrases de rêves, ces sensations parlantes, ces silences d'oursins
qui nous sont frères et sœurs, toutes les saisons où la fleur bleue
n'est plus de mise, sauf les lilas, les merveilleux lilas d'[a.p.](#ap)

# b.s. --- baruch spinoza {#bs}

Étrange de le citer, lui, et non pas l'auteur de la *Critique de la
faculté de juger* (et toute sa réception postmoderne). Car la Critique,
c'est lui --- la critique entendue comme *transcendantale*, c'est-à-dire
ce qui examine les conditions de possibilités du savoir. De cela (de la
critique comme examen des conditions de possibilité de la littérature),
[m.b.](#mb) en parle dans un article à l'occasion des *Fleurs de Tarbes*,
"Le mystère dans les lettres" : "Peut-être existe-t-il pour la
critique une *via negationis*, s'il existe aussi dans la littérature des
problèmes qu'on ne peut évoquer sans les faire s'évanouir et qui
demandent une explication, capable de confirmer, par l'éclaircissement
même qu'elle apporte, la possibilité d'échapper à toute explication".
Voilà notre critique idiote et qui a si peu parlé de b.s. C'est injuste.
Il a dans l'histoire de l'idiotie la part majeure : celle de la joie.
Celle que [g.d.](#gd) nous a enseignée dans son corps, sa voix, ses
cours, ses façons d'interrompre idiotement "qu'est-ce que c'est que ça
une pensée ?" Il nous fait le coup de l'idiot --- celui de Socrate ---
mais il lui donne cette force majeure où b.s. se joint à [f.n.](#fn)
pour faire de l'affirmation, de la critique comme puissance toujours
affirmative y compris comme négation --- disons résistance (cette
résistance essentielle à la création, à l'intempestif, à la pensée ---
décrochage, décalage que doit aussi permettre la critique vis-à-vis du
texte).

# b.s.2 --- bernard stiegler {#bs2}

Désolé, Bernard, mais tu viens après [b.s.](#bs) et on pense que tu ne
nous en voudras pas. On est un peu impressionnés parce que ta tombe est
encore fraîche. Pourtant on te serre la main, l'ami. On te ramène parmi
nous pour nous reparler encore des protensions et des rétensions, de
toute ta pensée de la technique qui fait de nous des êtres cyborgs.

# m.s. --- michel surya {#ms}

D'un nom qui a toujours voulu se retirer (« n'as-tu pas écrit que tu
écrivais pour oublier ton nom ? »). Qui s'est souvent écrit dans les
vies des autres : [g.b.](#gb), [f.n.](#fn), [m.b.](#mb) Dans les livres autant qu'ils sont
des vies non pas imaginaires. Des réelles pensées engageant toute
l'existence dans le processus de la pensée. Écrivant sur les autres et
témoignant qu'un livre s'écrit toujours depuis sa fin et ses paradoxes,
depuis nous-mêmes et depuis un impossible en acte
(« hétérautothanalogie »). Grande leçon et leçon trop oubliée que la
littérature doit être non pas un art mais une pensée et une exigence.
[f.n.](#fn), [s.w.](#sw), et m.s. se rejoignant dans cette éthique : « La question n'a
jamais été pour moi de savoir si j'étais capable de grandes pensées,
mais si la pensée était capable que je sois grand ». Alors récit,
critique, philosophie, politique, littérature se mêlent et se modulent.
On lit, on écrit ainsi des livres et des vies qu'on devient pour les
faire imploser en mille autres vies, mille autres questions. « Être tous
les noms de l'histoire » ([f.n.](#fn)). Jusqu'à cette folie, oui. Lire. Lire
donc. D'un nom toujours double. Surya, divinité solaire hindoue, miroir
du Dianus bataillien, rupture et lien à la tradition où le nom et la
parole, par dissension, se révèlent diaboliquement double. La nécessité
de l'antagonisme, de cette intime contradiction qui commanderait
l'écriture, trouve ici son expression. m.s. biographe des mythes, des
figures toutes extérieures d'auteurs dont il écrit le roman, la
figuration toujours un peu trop grande. Figuration, défiguration.
Répétant encore les paradoxes et « à la fin, si l'art n'a pas d'auteur,
ou s'il n'en a plus, c'est pour qu'il n'y ait pas d'histoire, ou pour
qu'il n'en ait plus. »


# l.t. --- lucie taïeb {#lt}

*Les échappées* --- cela peut être un de ces mots de passe de la poésie
[<span id="echappe">critique</span>](/stase-seconde/#echappe). Des échappées comme mouvement d'évasion (e.l.) et
échappées comme personnages. Une poétesse-traductrice-romancière : de
combien de personnes, d'hybride façon de lire et d'écrire sommes-nous
tous ainsi faits ?

# l.t.2 --- lev tolstoï {#lt2}

Un mythe trop grand. Jusque dans l'enfermement, la statue du romancier
devenue à l'image de ses personnages : irrationnelle. L'icône du
patriarche reste enfoncée dans la neige des ans qui nous séparent de sa
lecture. L'or a déjà noirci le fond de l'icône, la barbe a pris la
couleur des nuages, et pourtant ce visage garde ses yeux émouvants. Des
yeux clairs dans ce visage ridé. Ce visage, ce n'est pas celui de
*Guerre et Paix* ou d'*Anna Karénine*, c'est celui de *La mort d'Ivan
Illitch*. Peut-être n'a-t-on le visage que d'un seul livre, celui-là qui
nous revient quand on découvre le portrait fait de l.t.2 par [m.s.](#ms), qui
n'en parle pas de ce livre sur la mort, lui qui pourtant parle
uniquement de la mort de Tolstoï, de cette fuite en avant animale et
décisive, solitaire et dernière, loin de sa femme. "Tu veux croire
qu'il a fui pour cela qui compte le plus, qui compte seul maintenant
selon toi : écrire, écrire librement ; être libre d'écrire, écrire pour
être libre." La liberté et la mort. Un peu de
[<span id="neige">neige</span>](/a-te-souvenir-de-linsomnie-des-mondes/#neige)
qu'un souffle déplace et qui finit par retomber.

# m.t. --- marina tsvetaïeva {#mt}

L'ardeur, l'exil. La confusion des sentiments dans sa plus juste
expression : la poésie. On cherche ici des incarnations inachevées,
imparfaites, de ce que serait une amitié critique. Reflet un peu idiot,
au sens d'essai de singularité, de celle qui se joue ici entre
[l.r.](#lr)\|\|[m.v.](#mv) : la désindentité par décorporisation. On
nous excusera de nous en tenir, parfois, à ce terre-à-terre parfaitement
anecdotique. Mais, l'amitié peine à échapper à sa matérialité. Il
importe peu de savoir que nous essayons ici une amitié en absenciel,
dans la virtualité d'un texte-à -texte, *[<span id="textus">textus invenio</span>](/stase-seconde/#textus)*. Un
détour peut-être nécessaire pour dire m.t., pour mettre à l'essai ce que
l'on cherche dans ce processus d'effacement des noms d'auteurs. Écrire,
de la critique en premier lieu, c'est inventer des liens, les croire
irréfragables. D'autres [<span id="correspondances">correspondances</span>](/stase-seconde/#correspondances) où par la distance même
nous nous effleurons. Dans la correspondance de m.t., spécialement dans
celle avec Rilke et Pasternak, se lit une amitié comme un [<span id="excedenta">excédent</span>](#excedentb)
dont on ne se remet pas. Saluons-en le perpétuel possible.

# l.v. --- laura vazquez {#lv}

"Pour qu'une chose soit intéressante, il suffit de la regarder
longtemps" disait l'ermite de Croisset. C'est aussi le moment où le réel
se révèle dans son étrangeté, dans son idiotie : une singularité
intransigeante, une contingence faite de racines de marronnier.
L'inéluctable du réel. L'inexorable ([a.r.](#ar)). La poésie de l.v. se
déploie jusqu'au roman en passant par la chanson en déployant toujours
l'incroyable rhizomatique qui couvre les visages du monde. Elle nous
donne à lire non pas la merveille, la recherche d'individuation, mais la
phénoménologie de l'idiotie considérant chaque chose. C'est peut-être
aussi un art de lire qui se donne ici, attentif au réel du texte, à
l'étrangeté des mots, aux dérives de la conscience.

# m.v. --- marc verlynde {#mv}

"Dis-moi qui tu hantes et je te dirai qui tu es" dit Breton au début de
*Nadja*. Il y a une matrice de fantômes qui s'agite autour de m.v. et
des marges du surréalisme : quelque chose de [r.c.](#rc) et de
[~~m.l.~~](#ml) qui s'aventure jusque dans le 21e siècle pour se
métamorphoser. Dans le labyrinthe arachnoïde du web aux 33 milliards de
nichées, il est difficile de distinguer quelque chose comme une voix,
pourtant de texte en texte, de fiction en critique, de critique en
fiction, m.v. élabore une œuvre.

# a.v.&m.p. --- antoine volodine & marcel proust {#avmp}

"Les beaux livres sont écrits dans une sorte de langue étrangère." m.p.,
*Contre Sainte-Beuve*

"J'ai eu pour souci d'écrire mes livres dans cette langue de
traduction." a.v. "Écrire en français une littérature étrangère" (*/Chaoïd/*, n^o^6).

Écrire-traduire, défaire sa langue en une autre langue, façon de la
soustraire pour l'un à l'imaginaire national, pour l'autre de l'ouvrir
aux contresens et aux belles infidèles de la traduction.

Fera-t-on de Dinepr Voloust le personnage matriciel d'une théorie du texte comme radicale étrangeté, à l'instar de Blandine
Volochot ? Il se joue au
moment où s'invente la "littérature mondiale" quelque chose qui résiste
au nationalisme de la langue --- l'Internationale alien des écrivaines
et des écrivains. La langue est fasciste, disait [r.b.](#rb) Celui ou
celle qui écrit a pour tâche de résister à cette injonction à dire ---
que la langue se fasse secret, se fasse impouvoir, mineure (d&g), c'est
bien plutôt ce qui, toujours, devrait être écrit.

# s.w. --- simone weil {#sw}

Elle apprend le sanskrit avec Daumal. Anecdotique ? Sans doute. Voir
pourtant dans cette communication distanciée une pure épreuve de
l'amitié. Quelque chose qui s'incarne aussi dans cette reconnaissance
manquée avec [g.b.](#gb) On dit qu'elle n'apparaît pas assez belle, trop
cérébrale dans *Le bleu du ciel*. Mais quelle perte, quel fantôme de
Laure y voyait [g.b.](#gb) ? Peut-être aussi un peu de ce que nous
tentons ici dans une des ses formules : on dialogue, jamais nous serons
"professionnels de la parole" On détourne des formules : la pesanteur et
la grâce. Pour une critique dans cette oscillation : entre le détail et
l'envolée non tant lyrique (quel sujet musicalement s'y exprimerait ?)
que stellaire. Même si on n'en parle pas, absolue nécessité de sa pensée
politique, complexe. On continue à écouter sa traversée des ersatz.
Décréation : "Nous participons à la création du monde, en nous décreant
nous-mêmes."

# v.w. --- virginia woolf {#vw}

Le critique-Orlando, cela serait si beau, cette fluidité, ce bonheur et
cette mélancolie, cette façon de traverser le temps et les époques, de
se mêler au monde et au rêve, attentif à l'extrême détail du monde en
même temps qu'à ses brouillards insistants. Oui, la critique-Orlando est
une des plus belles choses qui soient.

# m.z. --- maria zambrano {#mz}

*L'inspiration continue*. L'expiration aussi. Écriture au secret, du
secret. On voudrait faire rencontrer m.z. et O sur la jetée d'Orly. Leur
faire écrire un commentaire d'autre chose que de la Bible. "Aujourd'hui
poésie et pensée nous apparaissent comme deux formes insuffisantes, deux
moitiés de l'homme: le philosophe et le poète. L'homme entier n'est pas
dans la philosophie ; la totalité de l'humain n'est pas dans la poésie.
Dans la poésie, nous trouvons directement l'homme concret, individuel.
Dans la philosophie, l'homme dans son histoire universelle, dans son
vouloir être. La poésie est rencontre, don, découverte par la grâce. La
philosophie quête, recherche guidée par une méthode." Le critique émerge
entre les deux faces, tel un songe inattendu.

# p.z. --- pierre zaoui {#pz}

Un contemporain de plus dans la galerie. Philosophe ami de la
disparition et du refus du déni des temps de crise --- car c'est une
parole critique à bien des égards que sa *Traversée des catastrophes*.
Dans *La discrétion, ou l'art de disparaître*, il donne à penser, par
touches délicates, par profils fugitifs mais si clairs, toutes les
pensées et les acteurs d'une autre histoire que celle aveuglante de
l'auto-affirmation qui guide l'individualisme contemporain devant se
faire *projet*, devant se faire populaire, devant se rendre visible par
tant de dispositifs (on rejoint ici Foucault), entraînant cette grande
fatigue que [f.n.](#fn) nommait déjà du nom de "nihilisme". Au
contraire, la discrétion porte cet idéal de petite santé, de réserve du
souffle où se renverse le monde, respiration de la nuit qui est le lieu
de lutte vitale contre le nihilisme de l'époque.

</div>
