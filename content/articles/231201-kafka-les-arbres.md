---
persona:
  - Franz Kafka
title: "Les arbres"
slug: "les-arbres"
date: 2023-12-27
echo:
  - ontique
images:
  - "img/231201-kafka-les-arbres.jpg"
gridimages:
  - "img/231201-kafka-les-arbres-grid.jpg"
sourceimage: "AAA"
notes: "Cette nouvelle traduction de *Die Bäume* se place dans le domaine public volontaire. Ce texte est la plus petite pièce en prose écrite par Franz Kafka, publiée en 1912 dans le recueil *Betrachtung*."
summary: "Car nous sommes comme des troncs d'arbres dans la neige. En apparence, ils reposent bien à plat et, d'une petite impulsion, on devrait pouvoir les repousser. Non, on ne le peut pas, car ils sont fermement reliés au sol. Mais regarde, même cela n'est qu'apparence."
citation: "même cela n'est qu'apparence"
poetry: false
hangindent: false
---


## Les arbres

Car nous sommes comme des troncs d'arbres dans la neige. En apparence, ils reposent bien à plat et, d'une petite impulsion, on devrait pouvoir les repousser. Non, on ne le peut pas, car ils sont fermement reliés au sol. Mais regarde, même cela n'est qu'apparence.

## Die Bäume

Denn wir sind wie Baumstämme im Schnee. Scheinbar liegen sie glatt auf, und mit kleinem Anstoß sollte man sie wegschieben können. Nein, das kann man nicht, denn sie sind fest mit dem Boden verbunden. Aber sieh, sogar das ist nur scheinbar.
