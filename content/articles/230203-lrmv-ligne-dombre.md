---
persona:
  - Lucien Raphmaj
  - Marc Verlynde
title: "Ligne d'ombre"
slug: "ligne-dombre"
date: "2023-02-15"
echo:
  - esthétique
images:
  - "img/230203-lrmv-ligne-dombre.jpg"
gridimages:
  - "img/230203-lrmv-ligne-dombre-grid.jpg"
sourceimage: "lrmv"
notes: ""
summary: "Écrire c'est revenir. Et p.b.2 revient. Fait de la revenance, du retour même un motif complexe, un programme littéraire et métalittéraire. Retour dans le temps. Retour dans l'espace. Retour dans l'écriture. Avec *Popeye par les pieds*, p.b.2 semble littéralement revenir sur son livre précédent d'auto-science-fiction *Popeye de Chypre*, récit de l'enterrement de sa mère morte en 2013 au Mans elle qui rêvait d'être enterrée dans une Algérie fantasmatique, celle d'avant 1962. Retour impossible qui force au détour. Chypre 1979. Autre temps, autre souvenir. Critiquer, alors, ce serait revenir sur la possibilité de retour."

citation: "écrire c'est revenir"
---

*Ici se lit la poursuite d'un entretien ouvert sur la critique littéraire, sur ses puissances et ses impuissances, elle a tenté de trouver --- après celle de l'amitié dans [Stase-Seconde](/stase-seconde/), celle de l'idiotie dans [À te souvenir de l’insomnie des mondes](/a-te-souvenir-de-linsomnie-des-mondes/), celle du rêve dans [Le Tiers essai](/le-tiers-essai/) --- des traductions, des horizons et des retours à notre perplexité.*

# accord, un retour

## Miroir Face A

Écrire c'est revenir.

Et [p.b.2](/desidentite/#pb2) revient.

Fait de la revenance, du retour même un motif [<span id="complexe-b">complexe</span>](#complexe-a), un programme
littéraire et métalittéraire.

Retour dans le temps.

Retour dans l'espace.

Retour dans l'écriture.

Avec *Popeye par les pieds*, [p.b.2](/desidentite/#pb2) semble littéralement
[<span id="revenir-b">revenir</span>](#revenir-a) sur son livre précédent d'auto-science-fiction
*Popeye de Chypre*, récit de l'enterrement de sa mère morte en 2013 au
Mans elle qui rêvait d'être enterrée dans une Algérie fantasmatique,
celle d'avant 1962. Retour impossible qui force au détour. Chypre 1979.
Autre temps, autre souvenir.

Critiquer, alors, ce serait revenir sur la possibilité de retour.

Et quand l'auteur revient lui-même sur son précédent texte, dans son
texte, se fait critique de lui-même, auteur au miroir, et nous-mêmes
alors critiques de la critique critique, nous devenons, dans un jeu de
miroir, non pas la répétition vertigineuse à l'infini, mais l'aventure à
travers le texte, *Through the Looking Glass*, Alice(s) démultipliées et
hybridées.

*Popeye par les pieds* est donc un retour critique, commentaire,
repentir comme disent les peintres, faisant de la revenance, du retour même
un motif complexe, un programme littéraire et métalittéraire. Mais
comment commenter ce commentaire, [<span id="evidences-b">échapper aux évidences</span>](#evidences-a) et attendus de
la critique ? Comment parler d'un livre sans
revenir sur les pas de l'auteur ?

Trouver autre chose, projeter d'autres images.

Rejouer les paradoxes apparents. Disparaissant. On aime l'idée que, lors
de la conférence qui deviendra l'objet *Popeye par les pieds*, l'auteur
projette des images aléatoires, des vérifications de son vécu seulement
quand elles passent par un récapitulatif spéculaire.

Une pensée pour Sebald. Une pensée aussi pour l'autobiographie : l'un de
ses buts ne serait-il pas que le lecteur écrive sa propre autobiographie ?
S'invente sa théorie à chaque fois. Inventer la littérature [<span id="flottement-a">à chaque
fois que l'on écrit</span>](#flottement-b).

On voudrait une logique concentrique, à toi lectrice & lecteur
d'inventer la critique de cette critique critique. Toujours par
dérivatif, pour documenter l'absence du sujet.

Écrire c'est revenir.

Revenir : retour réflexif. Comment faire la critique d'un genre dont
l'essentiel est retour réflexif, portant sa part de critique, pointant
lui-même ses insuffisances. Plus que n'importe quel genre de texte,
critiquer un exercice autobiographique revient à valider une expérience,
en valoriser une perception. En creux, on pourrait penser que faire
œuvre de critique consisterait d'abord à [<span id="vertige-b">situer</span>](#vertige-a) sa propre
parole. Revenir, comme le fait [<span id="rieusement-b">rieusement</span>](#rieusement-a) [p.b.2](/desidentite/#pb2), sur les dispositifs qui
la construisent. On peut penser que nous parlons d'ailleurs, que nous
cherchons dans cette parole duelle, un écart au déterminisme du
contexte. Dériver alors du texte : revenir sur ses possibilités de
bifurcation, à nous d'inventer, théoriser (en son sens le plus
stellaire, un rapprochement de points qui dessine un motif qui dépend
beaucoup de la perception : [<span id="etoilement-a">étoilement</span>](#etoilement-b)), un genre à
soi. Temps à notre tour d'opérer un retour. Ni toi ni moi : on veut
parler de cet autre en Soi, de cet autre impensé qu'invente toujours la
lecture, de celui que Toi aussi tu peux [<span id="noyer-b">reconnaître</span>](#noyer-a). On l'a
dit. On revient toujours à l'anamorphose, traits concentriques,
spiralaires, pour cerner cette absence de sujet. On en reprend
l'absence, on en négocie la mort. On commente pour ne pas laisser au
silence cette part essentielle, indicible ou ineffable dont on est
surpris d'en voir partout une traduction dans nos lectures. Critique,
toujours témoigner du pas que l'on n'a pas su faire, des frontières dont
le franchissement revient.

Ce qui revient avec ce livre, c'est le plaisir d'aller à l'encontre des
évidences trompeuses de notre rapport au temps, de défaire une
littérature d'autofiction, de destituer le sujet par l'invention, de
reprendre l'ivresse spéculative qui n'est pas délire, mais résistance et
remise en cause de nombreux partages trompeurs : social / esthétique,
autofiction bourgeoise / documentaire transclasse, science-fiction /
littérature, spéculation / expérience, universel / singulier, passé /
futur, possible / impossible, etc.

Foin de [<span id="decept-a">dichotomies</span>](#decept-b) et que vivent les
glitchs. Écrire, susciter des passages secrets. Des portes dérobées
auxquelles on ne croit pas tout à fait. Une indéniable distance au sujet
par un jeu sur le burlesque. L'essai serait une fiction : on aime l'idée
de cette conférence ultime voyage dans le temps. Une des questions
centrales de *Popeye de Chypre* est comment parler aux autres, dans la
vraie vie, celle dite réelle ? Mal, sans doute quand ce n'est pas par
une spéculation, comprendre un miroir déformant.

C'est sur ces mouvements que ce livre revient dans ce qui n'est pas un
commentaire, mais quelque chose d'une revenance, d'une spectrologie.

Sans doute par une sorte de vacance à soi. L'auto-science fiction
pratiquée par l'auteur part de ce paradoxe : un voyageur dans le temps
ne peut rien changer. Ça ne sert à rien, c'est inévitable. C'est ainsi
que [p.b.2](/desidentite/#pb2) dessine un autre rapport au sujet. Le narrateur n'est pas
transparent, il est spectral, miné et minable, traversé d'influences,
enfin sans pouvoir.

Parce qu'écrire, décidément, c'est revenir.

Revenir parce qu'il y a quelque chose d'incessamment inadvenu, d'ouvert,
par lequel l'écriture arrive, revient.

Parce que le temps est ouvert.

Perdu et retrouvé en même temps, on le sait, les
[<span id="madeleines-a">madeleines</span>](#madeleines-b) nous le murmurent.

Écrire c'est revenir

Dans tous les sens.

C'est revenir hanter son futur, inventer son passé, transcender son
présent.

Écrire c'est revenir.

Écrire c'est revenir aussi à la manière du "[Regard d'Orphée](/desidentite/#mb)" sur
Eurydice pour cet "Orpheus McFly". Écrire devant perdre ce qui est
évoqué pour le porter à l'écriture, le rendre vivant dans sa mort
présente.

Alors écrire ce serait perdre, et revenir sur cette [<span id="perte-a">perte
impossible</span>](#perte-b). Pourtant rien que de très jouissif et
lumineux dans ce texte de [p.b.2](/desidentite/#pb2), travail à partir d'un lapsus oculaire
(*Popeye / Pop-eye* : yeux exorbités), à partir des images qui nous
échappent et nous fascinent. Peut-être est-ce l'occasion de dire que
toute mélancolie n'est pas triste. Qu'elle est plutôt la prise en
considération de l'impossible, de la nécessité de l'impossible, de sa
puissance (bien qu'il puisse, comme la mélancolie, être source
d'impuissance). De ce qui reste. De l'indestructible en même temps que
de l'irrémédiable. [p.b.2](/desidentite/#pb2) ne se résout pas à la perte. Comme sa mère avec
l'Algérie. Alors il l'enterre ailleurs. En ailleurs. Là où elle pourra
revenir. Dans les souvenirs, dans l'écriture, dans les lieux.

La mélancolie comme alternative, un retour, sans nostalgie, vers un pays
perdu pour que l'on se trouve contraint, par superposition (passage
entre le *cut-up* et le *mash-up*) de s'inventer le climat du sien
propre. L'ineffable mélancolie du souvenir : le laisser échapper, le
froisser et, trop tard, en entendre le frêle bruit. Mélancolie sans
nostalgie, saturée du désir de corriger ce qui sera, par essence, mal
perçu. Documentaire sans sujet. Revenir en cercles concentriques --- un
des motifs discrets de *Popeye de Chypre* --- sur l'image qui par
collage et amalgame finit par faire sens. Des strates : un voyage dans
le temps par des tableaux et des poèmes, puis par des images et des
commentaires spéculatifs. Au milieu, pur *medium*, l'écriture.

{{%asterisme%}}

L'espace-temps n'existe pas. Voilà l'incroyable leçon de ce livre.
L'éblouissante leçon qui n'est pas une provocation, mais la lumière
brute du réel. L'espace-temps n'existe pas.

Pas seulement, en tout cas.

Peut-être faut-il le déplier (juste avant la corbeille) --- exister un
instant dans la sidération de l'image, l'œil exorbité par la panique. Il
ne s'agit plus de valoriser la vision du poète. Plutôt ce qu'il a mal
vu, qui peut-être existe car cela ne correspond pas à sa vision
initiale. L'impossible réel.

Nous sommes des créatures extratemporelles : un être cher disparaît et
c'est l'ensemble de l'espace-temps qui est modifié. Ce n'est pas là de
la SF, mais la réalité psychique telle qu'on la vit dans les pertes
traumatiques comme dans les rêves (et les cauchemars) : le passé, le
présent et le futur se chevauchent, s'altèrent, se combinent. Et le
temps de l'écriture est aussi sûrement de cette même nature, que celle
de la disparition, que celle des rêves et des états de transe où le
temps reprend un aspect unitaire où les distinctions n'ont plus de sens.

Cependant, voir la difficulté du retour dans *Popeye de Chypre*. La
littérature c'est peut-être l'histoire d'une [<span id="mal-a">acclimatation
ratée</span>](#mal-b), toujours recommencée.

Le réel est alors dédoublé. Si le réel d'un espace-temps relativiste,
suivant une chronologie existe toujours, il existe aussi, de manière
tout aussi réelle, un double objectif, celui de l'esprit créant un
espace et un temps alternatifs où l'Algérie pré-1962 peut communiquer
avec le Chypre de 1979.

Une question de zone frontière. La perception comme abolition des
frontières. Perméables un instant : voir. Un endroit où un passage
serait possible.

Ce passage aux frontières, il est significatif qu'il se fasse au moment
de la mort. Dans l'arrêt de mort dans la fiction, dans cet état en
réalité jamais déterminé d'absence (qu'on se souvienne de
l'impossible [<span id="madeleines-b">mort d'Albertine</span>](#madeleines-a) dans le souvenir du narrateur
de *La Recherche*). La mort est ce creusement de la fiction et du sujet
--- et sûrement l'exposition aussi de la fiction du sujet --- où
s'installe la possibilité de voir la porosité du possible et de
l'impossible, du passé et du présent. Il y a comme une
"connaissance située" de ce "conte théorique" dans ce
flottement des catégories liées à la mort. Mais, [p.b.2](/desidentite/#pb2) montre que la
situation existe dans la coupure, retour & perte. Orphée chez lui vole,
en voiture, mais ne saurait vivre. Qui veut-on vraiment enterrer : "Tu
es devenu un spécialiste de la coupe. Tu aimes te couper des autres. Des
mille embranchements qui te relient aux autres. Tu te dis que la taille
promeut la repousse. Tu voudrais aussi te débarrasser de toi."
Autobiographie, cénotaphe circulaire pour montrer le vide radical de
l'endroit d'où l'on part, où l'on arrive. La description, surtout quand
comme dans *Popeye par les pieds*, elle est retour réflexif, doit être
une échappatoire. Un ailleurs, l'imaginaire défaillance des images.

## Miroir Face B

### Brisures, sutures

On aime l'idée : on en revient toujours à l'anamorphose pour échapper
aux évidences et aux attendus --- alors à nous d'inventer, de
théoriser[^1] --- autres ciels, autres temps --- la critique de la
critique qui deviendra possibilité de bifurcation.
Rétro-auto-science-fiction de *Popeye de Chypre*, *Popeye par les
pieds*[^2] demande littéralement de revenir sur le réflexif.

Or on aime l'idée de revenir sur le réflexif.

Critique de la critique critique.

{{%asterisme%}}

Comment faire de la critique une [<span id="silence-a">lecture-silence</span>](#silence-b),
cette part essentielle, indicible ou d'autres images ? Rejouer les
paradoxes apparents, le plus stellaire, [<span id="etoilement-b">critique-constellation</span>](#etoilement-a) :
un étoilement de perspectives.

Parler d'un ni toi ni moi : d'un inconnu commun[^3]. On veut penser
que nous parlons d'ailleurs, que la lecture est l'occasion d'opérer un
retour. De faire apparaître l'absence du sujet.

{{%asterisme%}}

Revenir, pour Sebald. Pas pour Sebald lui-même. Pour la pensée. Pour le
geste spéculaire. Une pensée aussi que nous cherchons dans cette parole
duelle que porte la critique et dont on est surpris d'en faire le sujet
de qui n'a plus d'objet. On en reprend l'absence, l'expérience, pour en
valoriser ce qui est une perception. Valoriser une perception.
L'autobiographie d'une perception. D'une lecture. La différance d'une
lecture. Écrire c'est revenir[^4]. Écrire c'est revenir. Dériver alors
du texte : revenir écrit sa propre autobiographie.

{{%asterisme%}}

On voudrait écrire la critique de *Popeye par les pieds* comme le retour
des [<span id="dispositifs-b">dispositifs</span>](#dispositifs-a)[^5] qui le construisent. On négocie la
mort de l'auteur. Le "On" de l'auteur-lecteur-auteur (complexe critique)
écrit : on trouve autre chose, on projette des cercles concentriques,
spiralaires, pour cerner cette absence de la lecture. Cette absence de
l'autre, du corps de la mère auquel on revient. La critique serait alors
toujours de témoigner de cet autre en Soi, de ce creux, de ce passé à
venir et on pourrait penser que cet autre impensé invente toujours
l'écriture, dans une logique concentrique, et un genre à soi.

{{%asterisme%}}

Pour documenter ses insuffisances, écrire. Dans les failles de ce que
l'on sait. Nous aussi. N'importe le temps. Retour dans l'espace. Écrire
c'est revenir. Programme littéraire et métalittéraire. Un récapitulatif
spéculaire. Une pensée fantasmatique. Retour impossible et qui revient
pourtant, sans cesse. Fait de la revenance, de ce qui force au détour,
au commentaire, au repentir à la manière du peintre, mais fait critique.
Comment commenter ce que tu peux reconnaître ? On ne doit parler que de
l'inconnu, on l'a dit.

{{%asterisme%}}

L'écriture est spectrale, minée et minable, traversée par les autres, la
vraie vie, faite d'influences et sans pouvoir. Cela [p.b.2](/desidentite/#pb2) l'a bien
compris. Parce qu'écrire est l'ultime voyage dans le temps. Revenance et
spectrologie. Discours sur le spectral. Sans doute cela permet-il de
perdre la bourgeoisie documentaire transclasse qui lit la littérature
comme de la science-fiction, autre chose que la vie. Ce qu'elle est,
réellement, mais autrement[^6]. Dans la littérature il y a quelque
chose d'incessamment inadvenu, remettant en cause de nombreux partages
dans le temps et l'espace qui murmure. Écrire c'est revenir.

{{%asterisme%}}

Destituer le sujet par l'auto-science-fiction. L'auteur part de *Chypre*
et va voir comment on parle de ce qui revient et ne
revient pas, dans ce qui ne peut pas, décidément, revenir. Décidément,
c'est revenir. Revenir sur ces mouvements trompeurs : le social et
l'esthétique, l'autofiction burlesque. L'essai serait une fiction --- littérature,
spéculation, expérience. Universelle spéculation :
comprendre est un miroir déformant. Et vivent les glitchs.

{{%asterisme%}}

Écrire, délire, dé-lire, délier, relier. Le narrateur n'est pas
transparent, il est changé. Une indéniable distance se fait par
l'invention, atteinte par l'ivresse de l'écriture qui est une sorte de
vacance à soi. Perdu et retrouvé en même temps se dessine un autre
rapport au sujet, au futur, au passé. Et transcender le commentaire.
Quelque chose de singulier.

{{%asterisme%}}

Écrire devant des évidences trompeuses de notre lecture. Évidences
auxquelles on ne croit pas, qu'il faut rendre ouvertes. L'écriture
arrive, revient. Le Temps est ouvert. Passé, présent. Écrire c'est
inévitable. Écrire c'est [<span id="dedire-a">dédire</span>](#dedire-b) l'inévitable et le
faire advenir. Autrement C'est ainsi que [p.b.2](/desidentite/#pb2) le fait. Dans tous les
sens. Revenir, depuis l'impossible du temps.

{{%asterisme%}}

*Popeye de Chypre* ou le réel. La nostalgie est impossible. On corrige
alors la mélancolie. Sous un œil exorbité, on en donne des visions, des
strates et des tableaux comme autant d'approches. Des images de
substitution, ce qui revient.

Chypre. L'indestructible vacance d'une zone tampon. Superposition
d'instants qui ne se confondent pas entièrement. Le voyage,
l'explication de ce voyage, la mélancolie des images qui en dévient, en
reviennent.

Ce qu'il faut préserver. Le frêle bruit de l'instant juste avant la
perte. [p.b.2](/desidentite/#pb2) nous en propose plusieurs variations. De numériques
superpositions. D'un auteur revient la fausse évidence de son
esthétique. Le sens qu'il faudrait déplier au moment de le jeter dans la
corbeille, la possibilité --- par traduction automatique --- d'écrire
sans contexte. Alors mettre en scène cette parole réflexive sur cette
imitation qu'est souvent la critique[^7].

Lapsus oculaire où l'amalgame du fond et de la forme. Le miroir et
l'absent, mélancolique, qui s'y miroite. L'ineffable mélancolie du
souvenir qui fait le sens, c'est la vision vue dans la panique. Sorte de
voyage entre différentes strates d'images. Critiquer, refaire le voyage
autrement ; recoller au hasard les images rêvées à la lecture du rêve.

Revenir en cercles concentriques[^8] sur l'image qui, par ailleurs,
nous guide vers là où elle pourra revenir : le souvenir, et la perte.
L'ailleurs de l'anamorphose. Désir lumineux de ce qui enfin aura un
lieu. Un froissement de paupière, une vision. Miroiter, repousser,
rendre possible un enterrement. Le souvenir et la perte. Une séparation
en silence. L'Algérie par ce que l'on ne saurait en dire ou comment
documenter l'absence sans virtualité. Aujourd'hui, maman est morte ou
peut-être était-ce hier. Les formules reviennent. Mélancolie
irrémédiable. Accouchement au forceps : dans la disparition des images,
dans leur perpétuel collage, se dessine un cénotaphe pour sa mère.
Popeye est né ; mélancolie, plus personne ne l'appelle Patrice. Des
commentaires spéculatifs. Toujours.

{{%asterisme%}}

L'espace-temps n'existe pas. Ce qui revient serait l'absence de sujet.
Les formes sans singularité qu'il faut donner à leur imagée disparition.
Rêves et cauchemars, ultimes réalités psychiques des créatures
extratemporelles que nous prétendons être. Le sujet ? Un temps
alternatif. La critique ? l'écoute des futurs qui se chevauchent. Avant
la corbeille, en déplier le sens, en déployer les promesses. Chypre
1979, un instant tampon, communication de la lumière brute du réel.

Dédoublement pour dessiner des zones frontières. Nous sommes ce qui est
modifié. Ce qui retient et revient d'un passage, des frontières
perméables. Espace-temps perçu dans ses [<span id="troublement">troublements</span>](/a-te-souvenir-de-linsomnie-des-mondes/#troublement), dans les interférences dans
les correspondances. On ne choisit pas l'endroit où va échouer le voyage
dans le temps. Approximation, acclimatation : on ne saurait s'installer
dans l'instant, dans l'illusion de sa linéarité. Le temps est un
collage, une saturation des sujets séparés qui n'ont pas su l'habiter,
qui alors le laisse revenir, devenir, s'altérer.

{{%asterisme%}}

Si on attend la repousse, quand et comment couper. Ça repousse. Revenir
sur une histoire, s'en séparer, lui inventer d'autres [<span id="liens">liens</span>](/stase-seconde/#hypertexte) : ce qu'on lit, ce qui nous lie.
Par la taille, comprendre (souvent trop tard, spectres que nous sommes),
ce qui nous relie aux autres. La connaissance située, peut-être, mais
pour lui inventer d'autres embranchements. Un endroit d'où partir,
peut-être est-ce cela l'invention du sujet. Celle de la critique serait
de continuer à spéculer, *[<span id="siempre">siempre</span>](/a-te-souvenir-de-linsomnie-des-mondes/#siempre)*, sur ces transitoires spéculations. On est lié,
indéfectiblement, par tout ce qui nous sépare.

# Conversation d'une harmonie désaccordée

Chercher à inventer une forme nouvelle pour la pensée. N'est-ce pas ce
tout ce que nous avons fait jusque-là ? C'est pourquoi je t'ai proposé
ce livre qui pose précisément cette question. Parce que [m.s.](/desidentite/#ms), dans les
lettres échangées entre Boèce et Dagerman dans *Le Monde des amants* et
*L'Éternel retour*, ne dit pas moins que ça : qu'il en va de la
biographie pour lui comme de la critique pour nous : d'être capable de
penser, de penser et de vivre, à tel point qu'écrire la biographie de
[f.n.](/desidentite/#fn) revient, pour lui, pour Boèce, pour nous, pour toute personne
lisant [f.n.](/desidentite/#fn), revient à vivre cette vie, rien de moins. Reprise, en
dissonance, d'un paradoxe qu'[<span id="revenir-a">écrire c'est revenir</span>](#revenir-b).
Folie, aussi, bien sûr, folie d'écrire sur les autres non pas en les
faisant revenir (hantologie) mais en les devenant à notre tour, en nous
engageant dans leur devenir, vertige auquel le texte nous engage sans
cesse dans son style fait d'incessantes reprises, de développement et de
reprises, qui, dans leur processus mettent à nu rien de moins que la
pensée, dans une exigence de singularité qui soit toujours à la limite
excès, [<span id="vertige-a">pensée et vertige</span>](#vertige-b) --- au point où se formule
le "pas au-delà". Mais alors cette singularité, cette singularité par
l'écriture, ce devenir par l'écriture, il faut l'imaginer paradoxalement
comme ce qui fait défaut, comme le défaut même. Elle est singularité non
parce que préoccupée d'un centre (ce centre qui ne cesse de se déplacer
--- [f.n.](/desidentite/#fn), [f.k.](/desidentite/#fk), [m.l.2](/desidentite/#ml2), [u.j.](/desidentite/#uj), [l.a.s.](/desidentite/#las), [l.t.2](/desidentite/#lt2), [g.b.](/desidentite/#gb), mais singularité en
se faisant exigence d'altération, exigence de dépossession en guise de
vérité. Ce qui revient serait mis à l'essai de cette impossible
singularité. C'est peut-être la double postulation à laquelle s'affronte
quiconque lit --- et lit ce livre singulièrement --- que la pensée est
toujours [<span id="impropre-b">impropre</span>](#impropre-a), que le sujet même fait une bien piètre illusion,
illusion démoniaque disait-on avant, avant [f.n.](/desidentite/#fn) peut-être, et pourtant
toujours encore, préoccupés par les mouvements d'ombre et de grammaire
qui font de "la mort de Dieu" une pensée si vivante, et du sujet une
illusion si tenace. Qu'est-ce qui nous ferait croire une chose pareille,
que la pensée n'est pas toujours l'autre, ne vient pas toujours
d'ailleurs ? La pensée est toujours déjà hantée, est toujours déjà
impersonnelle, façonnée par des codes, des langages qui n'appartiennent
pas seulement à d'autres, à d'autres auteurs, mais qui n'appartiennent
à personne, voix des morts mâchée par les vivants, signes où le sujet
devrait reconnaître cette radicale et essentielle impersonnalité. Comme
s'il fallait pour tenir ce discours en revenir à cette première phrase,
celle du *Monde des amants* comme celle de *L'Éternel retour*, cette
phrase qui revient et qui promet, et qui pour moi ne cesse d'être ce
genre d'énigme et de folie, entre la sainteté et l'idiotie : "Je
cherche à penser, dis-je à Dagerman, je cherche à penser que penser peut
décider de tout." Grotesque simultanément. Ce qui
revient a dû se produire : de la pensée. Qu'est-ce à dire, [<span id="comment-b">comment</span>](#comment-a) ça
s'écrit ? Nous la critiquons, la mettons à l'essai, songeuse
spéculation, nous interrogeons --- comme fiction de ce que nous serons,
de ce qu'en commun nous devenons par cette conversation --- ce qui
revient, en examinons la teneur, peut-être même nous distancions-nous,
en [<span id="spiralaires">spirale</span>](/a-te-souvenir-de-linsomnie-des-mondes/#spiralaires) de ce qui revient, de ce que l'on pourrait prendre
pour nous-mêmes. Et pourtant disant cela, ne reviens-tu
pas toi-même à tes obsessions, à ce qui se joue, pour toi, dans la
critique, d'une puissance de désidentification ? Or renoncer au Moi et à
son illusion substantielle, n'est-elle pas déjà la démarche de Nietzsche ?
N'est-elle pas la démarche folle de Boèce voulant écrire
"l'autobiographie de Nietzsche", en première personne plutôt qu'en
biographe distinct de son sujet, déclarant "je suis moi-même cette
fiction que j'ai écrite, fiction devenue elle-même biographique, il me
faut maintenant être ce que j'ai écrit *à ce point*". [<span id="flottement-b">Flottement</span>](#flottement-a) des
obsessions miennes --- sans doute --- retour. Oui, toujours, comme si
l'écriture était ce piège fait de miroirs, et nous, de pauvres
alouettes. Hantise et [<span id="revenance-b">revenance</span>](#revenance-a) ? "Tout semble sauvé, et l'on est
seul en fait à l'être. Et on est le seul à l'être. Et on est seul à
l'être tout le temps que dure ce miracle dure. Pire : on veut que dure
ce miracle qui fait qu'on est seul." Ce qui revient serait-ce la
matérialité du livre ? Sa langue. *Le Monde des amants* tiendrait-il par
ces [<span id="dispositif-a">dispositifs</span>](#dispositif-b) de distanciation ? Un vertige --- et
peut-être ne s'approche-t-on de la pensée de [f.n.](/desidentite/#fn) que lorsque l'on ne
sait plus qui parle, qui se souvient, qui rapporte, cite,
[<span id="mime-a">mime</span>](#mime-b) peut-être une pensée qui ne lui appartient pas.
Brouillage comme le point d'incandescence des instances narratives, de
Singularité mathématique, là où le roman existe : quand il perd sa forme
romanesque, sa forme critique, quand il nous perd. C'est vrai, c'est ce
qu'il y a de plus beau et de plus fou --- que Boèce, Dagerman,
Nietzsche, [m.s.](/desidentite/#ms), dans un désordre, échangent leurs voix. Que le *il*
glisse dans un *tu*, qu'un je soit un autre, que les adresses se
multiplient, se mêlent à l'horizon où le [<span id="eau-b">ciel et la mer</span>](#eau-a) se confondent
dans la ligne d'ombre de l'écriture. Reviennent les
réticences où soudain, on rêverait ce que l'on croit savoir. Main
négative de l'écriture, critique. Alors, critique par ellipses, par
diplomatie. Passer [<span id="silence-b">sous silence</span>](#silence-a) ce qui, dans une lecture
illusoire de se croire personnelle, nous aurait manqué : la vie
matérielle de [f.n.](/desidentite/#fn) Banalité du quotidien, le silence de la pensée qui
se fait, l'à côté de l'anecdotique trop connu de "la" vie (singulière
quand [m.s.](/desidentite/#ms) parvient à écrire "des" vies) de [f.n.](/desidentite/#fn) À notre tour,
inventons-nous des vies, la pluralité des regards. C'est ainsi que
nous avons lu de nos quatre yeux. J'ai lu les marges du livre et tu y as
lu le corps du texte. Du rapport à [f.n.](/desidentite/#fn), je n'ai rien lu --- je n'ai
rien attendu. Comment cela pouvait-il en être autrement ? Nous ne sommes
pas entièrement capables de parler (moins encore d'[<span id="limpides">exister</span>](/stase-seconde/#limpides)) 
d'une seule [<span id="voix-b">voix</span>](#voix-a). On tait les désaccords, on s'en dépossède pour ne plus
s'y reconnaître, pour essayer l'altération de la pensée. On parle de ce
qui éloigne, ce qui nous sort du livre. D'except, dit-il. Tout ce qui,
dans la parole, ferait exception. [<span id="dedire">Dédire</span>](/a-te-souvenir-de-linsomnie-des-mondes/#dedire) le dit disait-on
avec [e.l.](/desidentite/#el). Un except donc dont
[m.s.](/desidentite/#ms) nous aide à rêver l'aspect de trahison nécessaire à toute
révélation. On rêve de la contradiction qui reviendrait,
mimer, trahir, révéler, chacune des sentences ici assénées. Ce n'est que
cela qui revient dans la critique et non pas la jauge de ce qui serait
réussi ou raté, de ce que platement on aurait voulu lire. Le synonyme de
la force de devenir dont est porteur le livre de [m.s.](/desidentite/#ms)
s'appellerait écriture. C'est cet appel, cette trahison,
ce doute profond, dont rêve notre part commune de critique. Devines-en,
lectrice & lecteur, les interstices, viens t'y [<span id="noyer-a">noyer</span>](#noyer-b) :
il en reviendra d'[<span id="mime-b">intranquilles incertitudes</span>](#mime-a), d'autres vies dont [m.s.](/desidentite/#ms)
fait des vies antérieures, mémoires oblitérées, romancées de ce qui a
été, révolte et inquiétude aussi. Nous sommes allés si loin ensemble à
travers les textes que nous abordons maintenant la haute mer et la
tempête : comment être en désaccord sans cesser de se parler ? La
critique déploie alors cette œuvre nouvelle qui semble aujourd'hui plus
que jamais devoir s'étendre au-delà de la seule amitié. Qui demande une
mutation de la conception de l'amitié où l'on fait fond commun d'une
étrangeté au monde. Amitié encore, encore plus que diplomatie, car
gardant cette fragilité, cette capacité de se projeter sans cesse à la
place de l'autre pour comprendre quelque chose de l'harmonie d'un
désaccord. D'une diplomatie [<span id="interstice">interstitielle</span>](/le-tiers-essai/#interstice) alors. On y essaierait la définition d'une idéale
lecture : l'endroit et moment où le désaccord de soi à soi se manifeste,
trouve une forme, s'exprime en altération. Sujet essentiel du *Monde des
amants*. Revient alors cette affirmation : l'[<span id="ecrire">écriture</span>](/stase-seconde/#ecrire]) est devenue
intransitive, la critique
reviendrait à lui inventer, à lui substituer, différents prédicats. À
qui parle-t-on quand on écrit ? Avant tout à soi-même semble suggérer
[m.s.](/desidentite/#ms) Avant l'intromission de la fiction, avant, pour la critique de la
critique, la radicale remise en cause de l'auctorialité. On écrirait
alors pour l'absence en soi. Critiquer serait peut-être en cerner les
différents [visages](/desidentite/#e.l.). Critiquer, serait-ce
interroger le silence entre deux livres, les interstices
où se construisent, dans un silence dont il ne reste que douteuses
interprétations. La saturation de silences de toute conversation. La
musique des désaccords exceptés. [m.s.](/desidentite/#ms) en donne une image parfaite,
dédoublée : il imagine ce qu'auraient pu se dire [g.b.](/desidentite/#gb) et [w.b.](/desidentite/#wb), à Ibiza.
Il n'en reste rien, magnifique vide. La critique serait de lire les
[<span id="livres-b">livres</span>](#livres-a) qui n'ont pas été encore écrits. Peut-être n'est-ce que cela qui
a été écrit. Alors la critique est cette fiction, cette vérité, cet
excès, qu'elle est cet except, puissance qui revient à l'écriture par la
critique, d'un retour qui met en jeu l'inéluctable, c'est-à-dire la
question : est-il seulement possible de revenir ? D'en revenir. De
l'écriture et de la vie. Question de hantise, toujours, de
"l'irréméable" pour désigner ce qui ne peut plus faire retour. Et qui
pourtant revient. Est-il possible de revenir pour toi, qui es revenu (de
[g.b.](/desidentite/#gb), de [f.n.](/desidentite/#fn), du tragique de la pensée) ? Est-il possible de
revenir --- de réfléchir, d'accomplir un pas au-delà, de se distinguer ?
N'est-ce pas cela la question qui devrait inquiéter la personne qui se
met à faire de la critique ? Livre des questions qui nous interroge dans
ces livres, dans notre vérité changeante faite de notre confusion ?
Vérité dramatique (peut-être), pathétique (assurément), romantique par
moment, dans cette vérité romanesque qui veut que le roman soit tout,
soit *toi* en tout cas, que le sujet et son objet existent sur le même
plan et se confondent, le faisant et ne le pouvant pas, de même que
l'amour et la révolution, le biographe et le sujet de sa biographie --- se
confiant pourtant à cet impossible comme à la révolution, à l'amour.
Peut-être lisons-nous surtout, malgré tout, des désaccords. L'ombre
portée de l'incompréhension jamais ne se [<span id="complexe-a">ressemble</span>](#complexe-b).
L'amour et révolution comme incompréhensibles, détachés de toute
contingence, ne deviennent-ils pas concept dangereux ? Reste, et c'est
ce que nous mettons à l'essai, plutôt que l'inéluctable (idiot que je
suis, [<span id="rieusement-a">pratiquement</span>](#rieusement-b) aucune idée de ce que c'est),
l'invention des instants. Revenance de l'éternisation de la momentanéité
selon la formule d'Unamuno citée par [m.s.](/desidentite/#ms) Ainsi dans ce livre qui
revient sur le livre, rien ne revient et rien ne se répète. Vérité qui
nous dit que rien ne revient quand bien même tout se répète. Mer, mer.
Et je comprends ce qui te retient : que l'on s'invente dans l'écart de
soi à soi-même, comme dans l'écart du texte à sa critique où toutes les
puissances nocturnes du possible brillent de mille feux. De mille mers.
La [<span id="vague">vague</span>](/a-te-souvenir-de-linsomnie-des-mondes/#vague) nous submerge, nous porte vers un ailleurs qui, peut-être, diffère par sa
[<span id="derangement">dérangeante</span>](/stase-seconde/#derangement) part de retour. La
part passionnée de la lecture reste celle où soudain malgré soi se
devine un désaccord, une désunion avec ce que l'on pensait penser.
Penser par ailleurs, par divergence plutôt que par opposition ou par
discorde. Comme si le texte par sa résistance créait l'étoilement
d'autres textes, nous conduisait nous-mêmes, et lui-même avec, à des
métamorphoses. Mettre à mort toutes les postures, mythe nietzschéen pour
détruire les mythes : [<span id="messianisme-b">messianisme</span>](#messianisme-a), le mot radicalement dérange. Le
dialogue du *Monde des amants*, un pas au-delà de soi, de la concrète
matérialité de ce que l'on croit comprendre. [m.s.](/desidentite/#ms) donne voix à
l'impossible. Peut-être n'est-ce que cela qui revient. Selon [f.k.](/desidentite/#fk), dans
une sentence d'un délicieux [<span id="hermetisme">contre-hermétisme</span>](/stase-seconde/#hermetisme), le messie viendra le jour
d'après. Peut-être est-ce ceci l'irrémédiable. Peut-être est-ce là aussi
l'impensable. Que la littérature soit l'absolu. Qu'elle soit donc après
Dieu, non pas le faux espoir d'une consolation, mais la vérité de
l'inconsolable : "parce qu'il faut, pour que la consolation existe,
que tout, *absolument tout* puisse être consolé, n'est-ce pas ?" Nous
qui voudrions être extérieurs à la littérature, nous nous voyons ramenés
à cette athéologie de la littérature : "parce qu'il faut, puisque le
roman existe, que tout, absolument tout, puisse être représenté, et par
le roman. Tout, pour que tout puisse être consolé, et par lui \[...\] et
c'est ce qu'il faut que fasse la littérature, si Dieu ne le fait plus."
Mais cette conversion littéraire qui nous est demandée, est aussi, et au
même moment, aussitôt, une impossibilité, une impossibilité sur laquelle
le texte ne cesse de faire retour : impossible représentation,
impossible description, impossible roman, impossible "hétérobiographie",
impossible révolution, et bien sûr impossible salut. Voilà ce dont
s'occupe le texte à travers les vies dont il hante les pensées ("roman
de pensée", dit-il). Décept, dit-il à un moment. Le mystère n'est que
cela, "l'expérience de la pensée", l'expérience réelle de la pensée.
"Mais personne pour s'en tenir à cette hypothèse qu'il arrive de bien
plus grandes choses en pensée, dans la pensée, par la pensée, bien plus
d'événements, dont tout ce qui mérite d'être dit dépendrait --- dépend."
Et de l'expérience de l'amour comme expérience de la pensée.
De l'expérience de l'amour et de la révolution comme expérience décisive
de la pensée au siècle dernier. Un siècle auquel on a appartenu, et dont
les révolutions au cœur de l'expérience intellectuelle n'ont cessé
d'être manquées et de devenir notre kitsch --- écart de soi à soi quand
sentimentalité, idéalité nous deviennent étrangères. À l'instar de [m.s.](/desidentite/#ms),
la critique opère un dialogue avec celui que l'on ne peut plus être,
celui que l'on ne reconnaît plus que dans un rêve et qu'ici nous
transmuons en fiction, en unité, un dialogue amical où, avec
l'éloignement, le temps, se lisent des dissensions, reviennent ce que
l'on n'avait pas compris. Oserait-on comme une sorte de trahison
révolutionnaire ? Les mots parfois semblent trop [<span id="indigne">grands</span>](/a-te-souvenir-de-linsomnie-des-mondes/#indigne), ils attendent que nous revenions,
rarement, à leur hauteur. Épiphanie, disait-on au vieux vingtième
siècle. Comme une question : *Le Monde des amants* évoque sans cesse la
révolution. On est dérangé, dans un premier temps (avant donc cette mise
en dialogue avec soi pour véritablement l'être ensuite avec l'ami), par
le singulier de cette révolution. Penser, idiotement, que chaque
révolution fait l'épreuve de son défaut de forme singulière, se
différencie au moins par les souterrains fantômes qui font revenir la
mémoire des luttes. Comme une intranquille mauvaise conscience, une
révolte, revient cette question : qu'est-ce à dire, une critique
révolutionnaire ? Une lutte, *a minima*, contre sa circularité : la
critique fait revenir ce qui a déjà été dit. Elle contraint à revenir à
cette obsédante question d'un éternel retour : comment parler d'un livre
pourrait composer pour en faire tout revenir, [<span id="evidences-a">qui est-on pour en faire
revenir une partie</span>](#evidences-b), en pointer les détails désaccordés ?
Parce que, peut-être, parler d'une œuvre, au mieux, serait la
poursuivre, en saisir les révélations l'instant d'après. Peut-être
est-ce le sens du roman biographique sur [f.n.](/desidentite/#fn) entrepris par [m.s.](/desidentite/#ms), une
suite de strates, une confusion d'instances narratives, de citations, la
mise en dialogue, en contestation, de ce qui revient de [f.n.](/desidentite/#fn), du reste,
du tout. Et la révolution, déjà toujours à faire, à trahir ?
C'est à ce genre de tragique que nous amène cette pensée. Non pas à la
résignation mais à l'acceptation de la totalité de l'expérience que
constitue la pensée et "que la politique est toujours une malédiction
et n'est que malédiction. Exactement comme le sexe est une malédiction
et n'est qu'une malédiction. Exactement comme la mort est une
malédiction"  : malédiction, c'est-à-dire ce qui [<span id="mal-b">se dit mal</span>](#mal-a), ce dont la
part maudite tient en ce défaut de la parole et qui fait, peut-être, de
la littérature ce qui tente, à nouveau dans un langage athéologique, de
"relever" dit [m.s.](/desidentite/#ms) De sauver le sachant perdu. "Il n'y a
pas de politique, *a fortiori*, de politique révolutionnaire,
que grotesque ou maudite *pour finir*." [<span id="perte-b">Se perdre</span>](#perte-a) à
critiquer, à penser, une aspiration, un idéal, auxquels on ne saurait se
soustraire. [m.s.](/desidentite/#ms) parvient à donner à entendre cette contestation
interne, dans la reprise dont procède chacune de ses phrases : "Tu
regardes plus longuement l'immensité du ciel ce soir, qui n'appartiens à
aucune, à qui aucune immensité n'est donnée, qui le voudrais pourtant,
qui l'as toujours voulu, qui t'es toujours cru à la mesure des
immensités, quelles qu'elles soient, qui en as en tout cas toujours
rêvé, qui as toujours rêvé qu'une immensité te soit donnée pour ne pas
rester sans, c'est-à-dire pour ne pas rester sans consolation, qui en as
créé pour, qui t'en es créé pour, qui toutes t'ont dépité, les
immensités politiques après les immensités religieuses, et pour les
mêmes raisons." Entends-tu, lectrice & lecteur, en échos à [<span id="impropre-a">tes
impropres</span>](#impropre-b) désaccords, dans cette mer de textes, au bord
de la noyade, l'aspiration à l'immensité dont, peut-être, revient sans
trêve le dépit, l'exigence, qui sait, d'en inventer d'autres modalités,
d'autres demains où revenir. Peut-être (affirmation volontairement
interrogative) serait-ce celle, comme [m.s.](/desidentite/#ms) et [f.n.](/desidentite/#fn), qui parviendrait (le
[<span id="comment-a">comment</span>](#comment-b) ne reste-t-il pas toujours --- un mot après
l'autre ; une variation formelle après l'autre --- à inventer ?) à
parler en apparence d'autre chose, à toucher ainsi, qui sait,
[l'irréfragable](/desidentite/#ab) mystère de ce qui s'écrit.
Maintenir par dissension la virtualité du pas de côté, l'écart. Façon,
qui sait, de laisser revenir la jouissance du texte pour parler avec
[r.b.](/desidentite/#rb), l'instant où le Moi se perd. Serait-ce une forme de messianisme
dont, sans cesse, nous pointons le [<span id="decept-b">décept</span>](#decept-a) ?

[^1]: Théoriser dans un écart qui devienne appropriation. Approche anamorphique de l'absence de sujet à l'œuvre, incomplètement, chez [p.b.2](/desidentite/#pb2) Inventer un genre à soi, essayer une autre forme. Une forme qui soit, comme pour les deux versants de Popeye, une reprise, une réponse. La continuation de la forme dialogique ici à l'essai. Une théorie, des rapprochements, des rebonds. Étoiles dans le ciel comme des points à (im)possiblement relier.

[^2]: La continuation de l'écart, donc. On l'a dit, il convient de parler avec et non pas de parler de. Parler à partir d'une résistance, d'une imparfaite adéquation : écrire. Échouer mieux, vieille lapalissade, s'en écarter. Un genre à soi. Critique de la critique. Ce qui ici est mis en jeu est la valeur logique de l'exemple. Quand on dit spéculaire, on veut une autre façon de réfléchir. Prétention. On essaie, on [<span id="messianisme-a">travaille l'échec</span>](#messianisme-b). On a parlé de ce que l'on voudrait faire, de ce que l'on voudrait lire. On tente ici de lui trouver une incarnation. Pas une illustration. Pas une preuve non plus. Un accompagnement, une parallaxe, des itinéraires miroirs à placer en regard.

[^3]: Un genre à soi. Une forme qui en étoile les contradictions. [<span id="voix-a">Deux voix, quoiqu'on y fasse</span>](#voix-b). Peut-être est-ce cela, la critique : pointer l'endroit où la proximité fait défaut. Un scrupule, un repentir. Ce n'est pas exactement cela. On s'en approche. Il faudrait, pourtant, comme dans l'autobiographie, le dire autrement pour s'approcher un peu plus de cette absence. Nous sommes ce qui nous [<span id="revenance-a">absentifie</span>](#revenance-b). Peut-être ne parlons-nous pas exactement de la même chose, peut-être ainsi se dessinent des rapprochements.

[^4]: Revenir, reprendre, inventer une autre origine. Pas seulement, on l'a dit derrière la parole d'un autre, [<span id="dedire-b">dédire</span>](#dedire-a) le dit, mais tenter d'écouter en quoi il concerne ce plus que nous-mêmes dont la lecture offre des tangences. En quoi et non comment. On ne saurait t'offrir un manuel d'absence. Rien que des rêveries, des traces, des passages dérobés. Autobiographie de l'autre en soi. Revenir, bien sûr, ce n'est plus de *Popeye de Chypre* que nous parlons. Nous y revenons seulement pour nous en éloigner.

[^5]: Ce qui revient. Des vestiges de dispositifs. [<span id="astroblematique">Astroblématique</span>](/stase-seconde/#astroblematique). La discordance de la matérialité aussi. Une évidence trompeuse de la critique sur laquelle il faut revenir : elle devrait nous faire toucher la matière même du livre. Par de subtiles paraphrases, nous éviter le souci de la lire ou, ce qui revient au même, oriente la lecture au point d'empêcher de faire la sienne propre. Oui, mais quelle matérialité a ce qui revient de *Popeye de Chypre* ?

[^6]: Ce qui revient. L'impossible. Autobiographie de nos altérations : lecture. Image la plus immédiate. Tu relis un livre cher. Tu n'y reconnais plus rien, le texte a changé, c'est certain. Une illusion d'optique. C'est de cela qu'il est question dans les voyages de Popeye. On ne reconnaît rien, on compose une anamorphose, des miroirs menteurs, des reconnaissances de ce que l'on n'est pas encore. Parler de l'intermède (dont jamais on ne saura rien) [<span id="livres-a">entre deux livres</span>](#livres-b). Il s'invente quels retours désormais Popeye ?

[^7]: On l'aura compris. L'utilisation de la note de bas de page est elle-même parodique. Une façon de se situer. Un écart à la preuve qui voit le monde universitaire. *Passim*. Mais une imitation qui exprime aussi un certain doute. *Mash-up*, ça ne *matche* pas exactement. Interroger l'amalgame, le mélange de toutes formes d'expressions culturelles. Interroger, au-delà de *Popeye de Chypre* cette facilité critique à ne parler que des endroits de dissemblances. On veut autre chose qu'une critique qui, en pointant, les dissemblances avec son point de vue, impose une subjectivité, fasse du critique un auteur.

[^8]: Terme employé ici pour s'écarter du critique qui fait son malin quand il croit avoir décelé un motif explicite dans un texte. Nous ne voulons pas révéler les circulations dans *Popeye de Chypre* ou dans *Popeye par les pieds* (un peu plus avouons, le vide entre les deux œuvres) que ceux que l'on peut s'inventer. Un exemple (comme s'il fallait incarner le propos) puisqu'on n'en a jamais fini avec le relevé des coïncidences. [p.b.2](/desidentite/#pb2) parle de trouver une "[<span id="eau-a">eau seconde</span>](#eau-b)", une "seconde immersion" qui serait l'écriture. En parler seulement par écho, rattrapage, de la tierce du [<span id="support">rêve</span>](/le-tiers-essai/#support).
