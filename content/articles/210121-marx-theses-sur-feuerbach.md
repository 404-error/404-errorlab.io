---
persona:
  - Karl Marx
  - Friedrich Engels
title: "Thèses sur Feuerbach"
slug: "theses-sur-feuerbach"
date: 2021-01-21
echo:
  - stratégique
  - ontique
images:
  - "img/210121-marx-theses-sur-feuerbach.jpg"
gridimages:
  - "img/210121-marx-theses-sur-feuerbach-grid.jpg"
sourceimage: "https://commons.wikimedia.org/wiki/File:Jean-Fran%C3%A7ois_Millet_-_The_Angelus_-_Google_Art_Project.jpg"
notes: "Les *Thèses sur Feuerbach*, intitulées à l'origine *ad Feuerbach*, existent en deux versions : celle issue des notes manuscrites de Karl Marx datant de 1845 et celle corrigée par Friedrich Engels, publiée en 1888, après la mort de Marx, et accompagnant son ouvrage *Ludwig Feuerbach et la fin de la philosophie classique allemande*. Les versions de Marx et d'Engels se trouvent notamment dans Marx-Engels-Werke, tome 3, Dietz Verlag, Berlin, 1978, p. 5 et p. 533.<br>
<br>
Cette nouvelle traduction d'Ann Persson a pour objectif, outre d'être dédiée au domaine public selon les termes de la Licence Creative Commons Zero (CC0 1.0 Universel), de faire sentir la force laconique du texte allemand, tout en respectant le vocabulaire de Marx aux teintes quelque peu hégéliennes. Elle tente également de montrer les nuances apportées par la version d'Engels --- souvent celle retenue au détriment de la version originelle de Marx."
summary: "Le principal défaut de tout matérialisme jusqu'à présent (y compris celui de Feuerbach) est que la chose, l'effectivité, la sensibilité, n'est saisie que sous la forme de l'objet ou de l'intuition ; mais non pas en tant qu'activité humaine sensible, non pas en tant que pratique ; non pas subjectivement. C'est pourquoi le côté actif fut développé de manière abstraite, au contraire du matérialisme, par l'idéalisme --- qui naturellement ne connaît pas l'activité effective, sensible comme telle. Feuerbach veut des objets sensibles --- effectivement distincts des objets de la pensée : mais il ne saisit pas l'activité humaine elle-même en tant qu'activité concrète."
citation: "Les philosophes ont seulement interprété différemment le monde, il importe de le transformer."
---

# I

{{%gauche%}}
Le principal défaut de tout matérialisme jusqu'à présent (y compris celui de Feuerbach) est que la chose, l'effectivité, la sensibilité, n'est saisie que sous la forme *de l'objet ou de l'intuition* ; mais non pas en tant qu'*activité humaine sensible*, non pas en tant que *pratique* ; non pas subjectivement. C'est pourquoi le côté *actif* fut développé de manière abstraite, au contraire du matérialisme, par l'idéalisme --- qui naturellement ne connaît pas l'activité effective, sensible comme telle. Feuerbach veut des objets sensibles --- effectivement distincts des objets de la pensée : mais il ne saisit pas l'activité humaine elle-même en tant qu'activité *concrète*. C'est pourquoi il ne considère, dans *L'essence du christianisme*, que le comportement théorique comme vraiment humain, tandis que la pratique n'est saisie et fixée que dans sa manifestation sordidement juive. C'est pourquoi il ne comprend pas la signification de l'activité "révolutionnaire", de l'activité "pratique-critique".&nbsp;(Marx)
{{%/gauche%}}

{{%droitecit%}}
Der Hauptmangel alles bisherigen Materialismus (den Feuerbachschen mit eingerechnet) ist, daß der Gegenstand, die Wirklichkeit, Sinnlichkeit, nur unter der Form des *Objekts oder der Anschauung* gefaßt wird; nicht aber als *sinnlich menschliche Tätigkeit, Praxis*; nicht subjektiv. Daher die *tätige* Seite abstrakt im Gegensatz zu dem Materialismus von dem Idealismus --- der natürlich die wirkliche, sinnliche Tätigkeit als solche nicht kennt --- entwickelt. Feuerbach will sinnliche --- von den Gedankenobjekten wirklich unterschiedne Objekte: aber er faßt die menschliche Tätigkeit selbst nicht als *gegenständliche* Tätigkeit. Er betrachtet daher im „Wesen des Christenthums“ nur das theoretische Verhalten als das echt menschliche, während die Praxis nur in ihrer schmutzig jüdischen Erscheinungsform gefaßt und fixiert wird. Er begreift daher nicht die Bedeutung der „revolutionären“, der „praktisch-kritischen“ Tätigkeit.&nbsp;(Marx)
{{%/droitecit%}}

{{%gauche%}}
Le principal défaut de tout matérialisme jusqu'à présent --- y compris celui de Feuerbach --- est que la chose, l'effectivité, la sensibilité, n'est saisie que sous la forme de l'*objet* ou de l'*intuition* ; mais non pas en tant qu'*activité humaine sensible*, non pas en tant que *pratique*, non pas subjectivement. C'est pourquoi il advint que le côté *actif* fût développé, au contraire du matérialisme, par l'idéalisme --- mais seulement de manière abstraite, puisque l'idéalisme ne connaît naturellement pas l'activité effective, sensible comme telle. Feuerbach veut des objets sensibles effectivement distincts des objets de la pensée ; mais il ne saisit pas l'activité humaine elle-même en tant qu'activité *concrète*. C'est pourquoi il ne considère, dans *L'essence du christianisme*, que le comportement théorique comme vraiment humain, tandis que la pratique n'est saisie et fixée que dans sa manifestation juive sordide. C'est pourquoi il ne comprend pas la signification de l'activité "révolutionnaire", de l'activité "pratique-critique".&nbsp;(Engels)
{{%/gauche%}}

{{%droitecit%}}
Der Hauptmangel alles bisherigen Materialismus --- den Feuerbachschen mit eingerechnet --- ist, daß der Gegenstand, die Wirklichkeit, Sinnlichkeit, nur unter der Form des *Objekts* oder der *Anschauung* gefaßt wird; nicht aber als *menschliche sinnliche Tätigkeit, Praxis*, nicht subjektiv. Daher geschah es, daß die *tätige* Seite, im Gegensatz zum Materialismus, vom Idealismus entwickelt wurde --- aber nur abstrakt, da der Idealismus natürlich die wirkliche, sinnliche Tätigkeit als solche nicht kennt. Feuerbach will sinnliche, von den Gedankenobjekten wirklich unterschiedene Objekte; aber er faßt die menschliche Tätigkeit selbst nicht als *gegenständliche* Tätigkeit. Er betrachtet daher im „Wesen des Christenthums“ nur das theoretische Verhalten als das echt menschliche, während die Praxis nur in ihrer schmutzig-jüdischen Erscheinungsform gefaßt und fixiert wird. Er begreift daher nicht die Bedeutung der „revolutionären“, der „praktisch-kritischen“ Tätigkeit.&nbsp;(Engels)
{{%/droitecit%}}

# II

{{%gauche%}}
La question de savoir si une vérité concrète incombe à la pensée humaine --- n'est pas une question de théorie, mais une question *pratique*. Dans la pratique, l'humain doit prouver la vérité, c'est-à-dire l'effectivité et la puissance, l'ici-bas de sa pensée. La querelle au sujet de l'effectivité ou de l'ineffectivité de la pensée --- qui est isolée de la pratique --- est une question purement *scolastique*.&nbsp;(Marx)
{{%/gauche%}}

{{%droitecit%}}
Die Frage, ob dem menschlichen Denken gegenständliche Wahrheit zukomme --- ist keine Frage der Theorie, sondern eine *praktische* Frage. In der Praxis muß der Mensch die Wahrheit, i.&nbsp;e. Wirklichkeit und Macht, Diesseitigkeit seines Denkens beweisen. Der Streit über die Wirklichkeit oder Nichtwirklichkeit des Denkens --- das von der Praxis isoliert ist --- ist eine rein *scholastische* Frage.&nbsp;(Marx)
{{%/droitecit%}}

{{%gauche%}}
La question de savoir si une vérité concrète incombe à la pensée humaine n'est pas une question de théorie, mais une question *pratique*. Dans la pratique, l'humain doit prouver la vérité, à savoir l'effectivité et la puissance, l'ici-bas de sa pensée. La querelle au sujet de l'effectivité ou de l'ineffectivité d'une pensée, qui s'isole de la pratique, est une question purement *scolastique*.&nbsp;(Engels)
{{%/gauche%}}

{{%droitecit%}}
Die Frage, ob dem menschlichen Denken gegenständliche Wahrheit zukomme, ist keine Frage der Theorie, sondern eine *praktische* Frage. In der Praxis muß der Mensch die Wahrheit, das heißt die Wirklichkeit und Macht, die Diesseitigkeit seines Denkens beweisen. Der Streit über die Wirklichkeit oder Nichtwirklichkeit eines Denkens, das sich von der Praxis isoliert, ist eine rein *scholastische* Frage.&nbsp;(Engels)
{{%/droitecit%}}


# III

{{%gauche%}}
La doctrine matérialiste de la transformation des circonstances et de l'éducation oublie que les circonstances sont transformées par les humains et que l'éducateur lui-même doit être éduqué. C'est pourquoi elle doit examiner la société en deux parties --- dont l'une est élevée au-dessus d'elle.

La coïncidence du changement des circonstances et de l'activité humaine ou autotransformation ne peut être saisie et comprise rationnellement qu'en tant que *pratique révolutionnaire*.&nbsp;(Marx)
{{%/gauche%}}

{{%droitecit%}}
Die materialistische Lehre von der Veränderung der Umstände und der Erziehung vergißt, daß die Umstände von den Menschen verändert und der Erzieher selbst erzogen werden muß. Sie muß daher die Gesellschaft in zwei Teile --- von denen der eine über ihr erhaben ist --- sondieren.

Das Zusammenfallen des Ändern\[s\] der Umstände und der menschlichen Tätigkeit oder Selbstveränderung kann nur als *revolutionäre Praxis* gefaßt und rationell verstanden werden.&nbsp;(Marx)
{{%/droitecit%}}

{{%gauche%}}
La doctrine matérialiste, selon laquelle les humains sont des produits des circonstances et de l'éducation, et que des humains transformés sont par conséquent des produits d'autres circonstances et d'une éducation transformée, oublie que les circonstances sont justement transformées par les humains et que l'éducateur lui-même doit être éduqué. C'est pourquoi elle en arrive nécessairement à séparer la société en deux parties, dont l'une est élevée au-dessus de la société. (Par exemple chez Robert Owen.)

La coïncidence du changement des circonstances et de l'activité humaine ne peut être saisie et comprise rationnellement qu'en tant que *pratique renversante*.&nbsp;(Engels)
{{%/gauche%}}

{{%droitecit%}}
Die materialistische Lehre, daß die Menschen Produkte der Umstände und der Erziehung, veränderte Menschen also Produkte anderer Umstände und geänderter Erziehung sind, vergißt, daß die Umstände eben von den Menschen verändert werden und daß der Erzieher selbst erzogen werden muß. Sie kommt daher mit Notwendigkeit dahin, die Gesellschaft in zwei Teile zu sondern, von denen der eine über der Gesellschaft erhaben ist. (Z.&nbsp;B.&nbsp;bei Robert Owen.)

Das Zusammenfallen des Änderns der Umstände und der menschlichen Tätigkeit kann nur als *umwälzende Praxis* gefaßt und rationell verstanden werden.&nbsp;(Engels)
{{%/droitecit%}}

# IV

{{%gauche%}}
Feuerbach prend comme point de départ le fait de l'autoaliénation religieuse, du redoublement du monde dans un monde religieux et un monde séculier. Son travail consiste à résoudre le monde religieux en sa base séculière. Mais que la base séculière se détache d'elle-même et se fixe dans les nuages en un royaume autonome n'est à expliquer que par l'autodéchirement et l'autocontradiction de cette base séculière. Celle-ci même doit donc en elle-même être tant comprise dans sa contradiction que révolutionnée pratiquement. Ainsi dès lors, par exemple, que la famille terrestre est découverte comme le secret de la sainte famille, la première elle-même doit désormais être anéantie théoriquement et pratiquement.&nbsp;(Marx)
{{%/gauche%}}

{{%droitecit%}}
Feuerbach geht von dem Faktum der religiösen Selbstentfremdung, der Verdopplung der Welt in eine religiöse und eine weltliche aus. Seine Arbeit besteht darin, die religiöse Welt in ihre weltliche Grundlage aufzulösen. Aber daß die weltliche Grundlage sich von sich selbst abhebt und sich ein selbständiges Reich in den Wolken fixiert, ist nur aus der Selbstzerrissenheit und Sichselbstwidersprechen dieser weltlichen Grundlage zu erklären. Diese selbst muß also in sich selbst sowohl in ihrem Widerspruch verstanden als praktisch revolutioniert werden. Also nachdem z.&nbsp;В. die irdische Familie als das Geheimnis der heiligen Familie entdeckt ist, muß nun erstere selbst theoretisch und praktisch vernichtet werden.&nbsp;(Marx)
{{%/droitecit%}}

{{%gauche%}}
Feuerbach prend comme point de départ le fait de l'autoaliénation religieuse, du redoublement du monde dans un monde religieux, représenté et un monde effectif. Son travail consiste à résoudre le monde religieux en sa base séculière. Il ne voit pas qu'après l'accomplissement de ce travail l'essentiel reste encore à faire. En effet, le fait que la base séculière se détache d'elle-même et se fixe, dans les nuages, en un royaume autonome n'est justement à expliquer que par l'autodéchirement et l'auto-contradiction de cette base séculière. Celle-ci même doit donc premièrement être comprise dans sa contradiction et ensuite être révolutionnée pratiquement au travers d'une élimination de la contradiction. Ainsi, par exemple, dès lors que la famille terrestre est découverte comme le secret de la sainte famille, la première elle-même doit désormais être critiquée théoriquement et renversée pratiquement.&nbsp;(Engels)
{{%/gauche%}}

{{%droitecit%}}
Feuerbach geht aus von dem Faktum der religiösen Selbstentfremdung, der Verdopplung der Welt in eine religiöse, vorgestellte und eine wirkliche Welt. Seine Arbeit besteht darin, die religiöse Welt in ihre weltliche Grundlage aufzulösen. Er übersieht, daß nach Vollbringung dieser Arbeit die Hauptsache noch zu tun bleibt. Die Tatsache nämlich, daß die weltliche Grundlage sich von sich selbst abhebt und sich, ein selbständiges Reich, in den Wolken fixiert, ist eben nur aus der Selbstzerrissenheit und dem Sichselbst-Widersprechen dieser weltlichen Grundlage zu erklären. Diese selbst muß also erstens in ihrem Widerspruch verstanden und sodann durch Beseitigung des Widerspruchs praktisch revolutioniert werden. Also z.&nbsp;B., nachdem die irdische Familie als das Geheimnis der heiligen Familie entdeckt ist, muß nun erstere selbst theoretisch kritisiert und praktisch umgewälzt werden.&nbsp;(Engels)
{{%/droitecit%}}

# V

{{%gauche%}}
Feuerbach, point satisfait de la *pensée abstraite*, veut l'*intuition* ; mais il ne saisit pas la sensibilité en tant qu'activité humaine-sensible *pratique*.&nbsp;(Marx)
{{%/gauche%}}

{{%droitecit%}}
Feuerbach, mit dem *abstrakten Denken* nicht zufrieden, will die *Anschauung*; aber er faßt die Sinnlichkeit nicht als *praktische* menschlich-sinnliche Tätigkeit.&nbsp;(Marx)
{{%/droitecit%}}

{{%gauche%}}
Feuerbach, point satisfait de la *pensée abstraite*, en appelle à l'*intuition sensible* ; mais il ne saisit pas la sensibilité en tant qu'activité humaine-sensible *pratique*.&nbsp;(Engels)
{{%/gauche%}}

{{%droitecit%}}
Feuerbach, mit dem *abstrakten* Denken nicht zufrieden, appelliert an die *sinnliche Anschauung*; aber er faßt die Sinnlichkeit nicht als *praktische* menschlich-sinnliche Tätigkeit.&nbsp;(Engels)
{{%/droitecit%}}

# VI

{{%gauche%}}
Feuerbach résout l'essence religieuse dans l'essence *humaine*. Mais l'essence humaine n'est pas une abstraction inhérente au seul individu. Dans son effectivité, elle est l'ensemble des rapports sociaux.

Feuerbach, qui n'aborde pas la critique de cette essence effective, est ainsi contraint :

1. de faire abstraction du cours de l'histoire et de fixer pour soi la nature religieuse, et de présupposer un individu humain abstrait --- *isolé*.
2. L'essence ne peut en cela qu'être saisie qu'en tant que "genre", qu'en tant que généralité interne, muette, liant *naturellement* le nombre des individus.&nbsp;(Marx)
{{%/gauche%}}

{{%droitecit%}}
Feuerbach löst das religiöse Wesen in das *menschliche* Wesen auf. Aber das menschliche Wesen ist kein dem einzelnen Individuum inwohnendes Abstraktum. In seiner Wirklichkeit ist es das ensemble der gesellschaftlichen Verhältnisse.

Feuerbach, der auf die Kritik dieses wirklichen Wesens nicht eingeht, ist daher gezwungen:

1. von dem geschichtlichen Verlauf zu abstrahieren und das religiöse Gemüt für sich zu fixieren, und ein abstrakt --- *isoliert* --- menschliches Individuum vorauszusetzen.
2. Das Wesen kann daher nur als „Gattung“, als innere, stumme, die vielen Individuen *natürlich* verbindende Allgemeinheit gefaßt werden.&nbsp;(Marx)
{{%/droitecit%}}

{{%gauche%}}
Feuerbach résout l'essence religieuse dans l'essence *humaine*. Mais l'essence humaine n'est pas une abstraction inhérente au seul individu. Dans son effectivité, elle est l'ensemble des rapports sociaux.

Feuerbach, qui n'aborde pas la critique de cette essence effective, est ainsi contraint :

1. de faire abstraction du cours de l'histoire et de fixer pour soi la nature religieuse et de présupposer un individu humain abstrait --- *isolé* ;
2. ainsi, chez lui, l'essence humaine ne peut qu'être saisie qu'en tant que "*genre*", qu'en tant que généralité interne, muette, liant de façon simplement *naturelle* le nombre des individus.&nbsp;(Engels)
{{%/gauche%}}

{{%droitecit%}}
Feuerbach löst das religiöse Wesen in das *menschliche* Wesen auf. Aber das menschliche Wesen ist kein dem einzelnen Individuum innewohnendes Abstraktum. In seiner Wirklichkeit ist es das Ensemble der gesellschaftlichen Verhältnisse.

Feuerbach, der auf die Kritik dieses wirklichen Wesens nicht eingeht, ist daher gezwungen:

1. von dem geschichtlichen Verlauf zu abstrahieren und das religiöse Gemüt für sich zu fixieren und ein abstrakt --- *isoliert* --- menschliches Individuum vorauszusetzen;
2. kann bei ihm daher das menschliche Wesen nur als „<em>Gattung</em>“, als innere, stumme, die vielen Individuen bloß *natürlich* verbindende Allgemeinheit gefaßt werden.&nbsp;(Engels)
{{%/droitecit%}}

# VII

{{%gauche%}}
C'est pourquoi Feuerbach ne voit pas que la "nature religieuse" elle-même est un produit social et que l'individu abstrait, qu'il analyse, appartient à une forme sociale déterminée.&nbsp;(Marx)
{{%/gauche%}}

{{%droitecit%}}
Feuerbach sieht daher nicht, daß das „religiöse Gemüt“ selbst ein gesellschaftliches Produkt ist und daß das abstrakte Individuum, das er analysiert, einer bestimmten Gesellschaftsform angehört.&nbsp;(Marx)
{{%/droitecit%}}

{{%gauche%}}
C'est pourquoi Feuerbach ne voit pas que la "nature religieuse" elle-même est un *produit social* et que l'individu abstrait, qu'il analyse, appartient en vérité à une forme sociale déterminée.&nbsp;(Engels)
{{%/gauche%}}

{{%droitecit%}}
Feuerbach sieht daher nicht, daß das „religiöse Gemüt“ selbst ein *gesellschaftliches Produkt* ist und daß das abstrakte Individuum, das er analysiert, in Wirklichkeit einer bestimmten Gesellschaftsform angehört.&nbsp;(Engels)
{{%/droitecit%}}

# VIII

{{%gauche%}}
Toute vie sociale est essentiellement *pratique*. Tous les mystères, qui entraînent la théorie vers le mysticisme, trouvent leur solution rationnelle dans la pratique humaine et dans la compréhension de cette pratique.&nbsp;(Marx)
{{%/gauche%}}

{{%droitecit%}}
Alles gesellschaftliche Leben ist wesentlich *praktisch*. Alle Mysterien, welche die Theorie zum Mystizism\[us\] veranlassen, finden ihre rationelle Lösung in der menschlichen Praxis und in dem Begreifen dieser Praxis.&nbsp;(Marx)
{{%/droitecit%}}

{{%gauche%}}
La vie sociale est essentiellement *pratique*. Tous les mystères, qui entraînent la théorie vers le mysticisme, trouvent leur solution rationnelle dans la pratique humaine et dans la compréhension de cette pratique.&nbsp;(Engels)
{{%/gauche%}}

{{%droitecit%}}
Das gesellschaftliche Leben ist wesentlich *praktisch*. Alle Mysterien, welche die Theorie zum Mystizismus verleiten, finden ihre rationelle Lösung in der menschlichen Praxis und im Begreifen dieser Praxis.&nbsp;(Engels)
{{%/droitecit%}}

# IX

{{%gauche%}}
Le summum, auquel arrive le matérialisme intuitif, c'est-à-dire le matérialisme qui ne comprend pas la sensibilité en tant qu'activité pratique, est l'intuition des seuls individus et de la société bourgeoise.&nbsp;(Marx)
{{%/gauche%}}

{{%droitecit%}}
Das Höchste, wozu der anschauende Materialismus kommt, d.&nbsp;h. der Materialismus, der die Sinnlichkeit nicht als praktische Tätigkeit begreift, ist die Anschauung der einzelnen Individuen und der bürgerlichen Gesellschaft.&nbsp;(Marx)
{{%/droitecit%}}

{{%gauche%}}
Le summum, auquel mène le matérialisme *intuitif*, c'est-à-dire le matérialisme qui ne comprend pas la sensibilité en tant qu'activité pratique, est l'intuition des seuls individus dans la "société bourgeoise".&nbsp;(Engels)
{{%/gauche%}}

{{%droitecit%}}
Das Höchste, wozu der *anschauende* Materialismus es bringt, d.&nbsp;h. der Materialismus, der die Sinnlichkeit nicht als praktische Tätigkeit begreift, ist die Anschauung der einzelnen Individuen in der „bürgerlichen Gesellschaft“.&nbsp;(Engels)
{{%/droitecit%}}

# X

{{%gauche%}}
Le point de vue de l'ancien matérialisme est la société bourgeoise, le point de vue du nouveau la société humaine ou l'humanité sociale.&nbsp;(Marx)
{{%/gauche%}}

{{%droitecit%}}
Der Standpunkt des alten Materialismus ist die bürgerliche Gesellschaft, der Standpunkt des neuen die menschliche Gesellschaft oder die gesellschaftliche Menschheit.&nbsp;(Marx)
{{%/droitecit%}}

{{%gauche%}}
Le point de vue de l'ancien matérialisme est la société "*bourgeoise*" ; le point de vue du nouveau la société *humaine* ou l'humanité socialisée.&nbsp;(Engels)
{{%/gauche%}}

{{%droitecit%}}
Der Standpunkt des alten Materialismus ist die „<em>bürgerliche</em>“ Gesellschaft; der Standpunkt des neuen die *menschliche* Gesellschaft, oder die vergesellschaftete Menschheit.&nbsp;(Engels)
{{%/droitecit%}}

# XI

{{%gauche%}}
Les philosophes ont seulement *interprété* différemment le monde, il importe de le *transformer*.&nbsp;(Marx)
{{%/gauche%}}

{{%droitecit%}}
Die Philosophen haben die Welt nur verschieden *interpretiert*, es kömmt drauf an, sie zu *verändern*.&nbsp;(Marx)
{{%/droitecit%}}

{{%gauche%}}
Les philosophes ont seulement *interprété* différemment le monde ; mais il importe de le *transformer*.&nbsp;(Engels)
{{%/gauche%}}

{{%droitecit%}}
Die Philosophen haben die Welt nur verschieden *interpretiert*; es kommt aber darauf an, sie zu *verändern*.&nbsp;(Engels)
{{%/droitecit%}}
