---
persona:
  - C Jeanney
orthopersona:
  - name: C. Jeanney
    link: C Jeanney
title: "EN RÉSUMÉ"
slug: "en-resume"
date: 2021-01-28
echo:
  - rythmique
images:
  - "img/210128-jeanney-en-resume.jpg"
gridimages:
  - "img/210128-jeanney-en-resume-grid.jpg"
sourceimage: "C. Jeanney"
notes: "Les encres de Chine sont de C. Jeanney."
summary: "CE QUE TU DOIS APPRENDRE / ET RETENIR / majuscules toutes serrées / en bataillons / immenses / ROUGES / saut de ligne / et un extrait pointu / formulé comme une maxime / la cruchaleau tanva / fêlée / remplie d'eaufailles / --- des foyes d'automne, dit ma mère / qui époussette les mobles car / elle ne sait pas dire les eu / de l'encrier dans les toilettes / rincer la poix collante au robinet / àcroucrou / s'essuyer du revers de main / ma bouche tachée provoque / tout autour / la terreur d'avoir été empoisonnée / réprouvée / poisonneuse"
citation: "sans rien attendre d'autre / qu'explosion imminente / (encore j'attends)"
---

CE QUE TU DOIS APPRENDRE\
ET RETENIR\
majuscules toutes serrées\
en bataillons\
immenses\
ROUGES\
saut de ligne\

et un extrait pointu\
formulé comme une maxime\
la cruchaleau tanva\
fêlée\
remplie d'eaufailles\
--- des foyes d'automne, dit ma mère\
qui époussette les mobles car\
elle ne sait pas dire les eu\

de l'encrier dans les toilettes\
rincer la poix collante au robinet\
àcroucrou\

s'essuyer du revers de main\
ma bouche tachée provoque\
tout autour\
la terreur d'avoir été empoisonnée\

réprouvée\

poisonneuse\

les mots encore étrangers à ma race\

traverser la cour des garçons\
yeux au sol\
--- qui ? partout court --- qui ? partout hurle\
moi dans l'espace en avancée\
délibérée\
sur le fil vide que\
les pieds qui me précèdent tracent\
sans rien attendre d'autre\
qu'explosion imminente\
(encore j'attends)\

accrocher mon manteau babar&célestine\
tenter de lire avec le doigt\
quand toutes entrent, connaissent les règles\
chuis pas une spécialiste\
chuis --- curieuse comme une chèvre\
chuis --- paresseuse comme une couleuvre\
chuis --- bête comme un âne\

la vieille madamelavieille n'aime ni les animaux ni moi\
je animal docile\

la première phalange de mon majeur\
tatouée d'un rond d'encre violette\
éternellement\

je lis la montagne et le loup\

j'apprends à lire les batailles\
marquée à peau\
*voix de son maître*\

aragon proteste contre l'invasion\
de la tchécoslovaquie\
on rouvre la sorbonne\
personne ne me demande si j'ai\
peur des boucliers de crs en noir & blanc\
des voitures ventres à l'air à table\
le dimanche dracula\
à la séquence du spectateur\
deux canines en mie de pain je pleure\

à table\
chuis animal\
qui ne mange pas\
sauf la croûte\
du pain\
et encore\

hélène sent mauvais\
je m'assois près d'elle dans la classe\
fille de gitans\
--- mais je ne connais pas ce mot\
des gens pas intéressants\
dit ma mère\
ma mère sait\
ma mère s'intéresse\
au prix des cerises l'été sur le\
marché --- je crie la mort\

j'admire les poules\
qui savent quoi regarder et où\
leur œil gros souligné\
chuis animal qui tire la main le bras\
en criant Je ne veux pas\
je crie la mort la tête\
cognée\
contre le mur du lit la nuit\

le train amène du fer à la fonderie\
où papa travaille\
la nuit le bruit du train\
s'entend de loin\

un tambour dans les tempes\
comme\
un enfant harnaché\
marcherait au pas dans la rue la nuit\
la mort vient\
peut-être prendre tous les animaux\

le samedi on se repose\
la banquette en velours marron\
tout est semblable\
les œillets d'inde alignés à intervalles réguliers\
régulièrement\
et tout ce qui n'est pas plié à angle droit\
n'est pas intéressant\

chuis chiffon\
que je ne sais pas repasser\
avoir le geste et la technique\
je dessine des taches\
exprès\
le fil des pieds qui me précèdent\
s'est séparé en deux\
aiméetdétesté\
qu'ils soient eux\
dézangles\
--- dézingue\

chuis tordue\
ainsi le chien de pompéï\
brûlé de lave en plâtre\
chuis qui partout cours et crie et ne sais rien\
gratte au carreau la buée\
la vue bouchée\
ensuite un grand canal\
vidé de l'eau moi cruche\
je marche au fond\
sans fil ni\
pieds à suivre\
les arbres à intervalles réguliers\
apaisent\
lesaimeetlesdéteste\
les écluses sont des accidents\
qui reproduisent la fosse d'avant\
heureusement que le pont de van g o g h\
mais à part lui\
je ne sais rien chuis\
animal décrit imprécisément\
tâtonnant\
je dessine des écritures\
pour m'entraîner\

Maintenant je colle sur du carton\
la carte de la lune\
mer des humeurs\

maintenant quand j'ai peur je dis\
j'ai peur\

maintenant j'ai gratté la buée\
je vois par flashs des hommes tomber\
des toits\
tomber des volcans\
des rizières\
tomber des métros\
des trottoirs\
des routes\
tomber du pont des soupiraux\
du caporal tomber des berges\
du canal\
--- là où je marche, dans la trouée\

le choléra\
la maladie de peau la terre\
la maladie de crasse les hommes\
qui ne protègent pas leurs petits\
--- ne travaillent pas à la fonderie\

maintenant j'écoute\
qu'est-ce que j'entends\
la liste des désolations\
*e mille tre e mille tre*\
s'allonge\
--- leporello\
ne revient pas le prix des cerises\

alors j'alerte autour\
je demande Vous aussi ?\
vous aussi ?\
et vous ?\

j'attrape des manches à retenir\
je place ma paume sur des joues\
je caresse des tempes\
je dis Et vous ? et vous ?\

je serre dans mes bras\
les museaux des ragondins à la surface\
l'eau qui ride mon gros ventre\
je serre dans mes bras\
le duvet sur le carrelage\
le soleil qui ride le ciment\

je serre dans mes bras\
le train arrêté trois fois\
de l'oncle communiste\
c'est noté sur la liste : Do Da\
--- liste de guerre ---\
Do pour Dora --- Da pour Dachau\
dorée la truite et ses viscères\
les os à fleur de peau\

je suis\
animal\
spongieux\
amorphe\
soudain réveil\

quand je me lève je nettoie la douche\
les grandes arabesques de mousse\
les vitres les ciels\
les cumulus qui creusent la poitrine\
je nettoie\
je nettoie les émotions brusques\
le brutal\
les larmes\
les petits garçons assis après les bombes\
les petites filles qui portent des bébés\
je nettoie\
sans rien laisser paraître\

je prends dans mes bras\
la petite balançoire\
la petite piscine\
la petite bille\
le petit bouton\
la petite égratignure\
le petit caillou\
la petite plume\
le petit grain de sable\
où toutes les couleurs fusexplosent\
je ne fais pas d'effets de manche\
j'alerte autour\
je demande Et vous ?\
et quand il se fait grand silence\
j'oublie les larmàdracula\

un homme dit\
C'est dieu qui nous donne des crabes\
ce même homme croit en donald trump\
un homme dit\
La tempête souffle les maisons\
histoire de loups & montagne violette\
et des récits à plus finir au bord de la piscine\
petite\
petit trottoir et petit soupirail\

un homme dit Vous êtes des laquais\
chuis une femme\
je dis les petits poissons morts\
sur le sable multicolore\
l'émerveilleux\

ma bouche tachée empoisonnée\
je la savonne\

quelqu'un gratte la surface du verre à la truelle\
c'est un portrait\
il dit C'est notre identité\

je suis quelqu'une qui savonne sa bouche\

alors je récupère du carton\
pour y coller la carte\
la lune mer des humeurs\
cratère des tempes grises\
marais de la putréfaction\
je savonne les grandes arabesques\
les douches\
les joints jamais assez blanchis\
jamais assez grisés\
ni raturés\
jamais assez violents\
ni estompés\

je savonne les mains faute de savoir sauf\
je suis quelqu'une qui\
savonne\
à grands traits au fusain\
savonne\
avec la colle de farine\
simple\
comme le pain\
savonne les briques de l'usine\
la fonderie\
savonne les enfants mangés\
fixateur on the wall\
la bétonnière\
la bétaillère\
savonne CE QUE TU DOIS APPRENDRE\
ET RETENIR la mousse redonne de l'épaisseur\
comme la buée réinvente les traces sur la vitre\

il n'y a pas de ruptures\
mais des entailles ça oui\
ça saigne\
ce qui saigne --- savonner et un œil s'étale sur la façade\
ce qui saigne --- savonner et un œil roule les boucliers\
ce qui saigne --- savonner aux ventres des voitures carcasses\
savonner ce qui saigne\
la pietà\
est ici\
visible\
partout sur la planète\
et là\
et là\
et là\

ENTRÉE GRATUITE\

quelqu'une entre\
elle va tomber\

harnachée\
petit tambour femelle\

moi 3·SINGES\
·yeux cachés\
·bouche fermée\
·oreilles closes\

·yeux cachés quand les hommes tombent\
les ils les elles et les petits\

·bouche fermée quand je voudrais dire\
ce qui n'a aucune importance\
car je ne suis pas spécialiste\

·oreilles closes quand criepartoutcrie\

moi animal 3·SINGES\
mais je ne suis pas seule\
quelqu'une quelqu'un se cache aussi les yeux\
car c'est trop de douleur\
se bande aussi la bouche des cris lacérations\
bouche aussi ses oreilles les enroule compassion\
et alternativement\
nos mains travaillent\

masquer\
ouvrir\
entendre\
fermer\
chacun chacune à notre tour\

en ordre d'apparition\

au bord de l'étagère chacun chacune\
poussant les pieds qui nous précèdent\
chacun chacune privé d'émerveilleux et le cherchant\
en alternant\

oreilles closes c'est dommage\
3·SINGES m'entendez-vous ?\

·yeux cachés c'est dommage : les bêtes de la mer\
resteront inconnues\
--- celle-ci agite\
son appât luminescent, celle-ci se déplace en\
coulant comme une\
goutte, celle-ci est\
transparente, ses tentacules\
pourraient\
fleurir\

·bouche fermée c'est dommage : ça empêche\
de remercier\

je voudrais dire merci\

merci pour les tentacules fleurs\
merci pour papier volant en forme de coquelicot\
merci pour le fil électrique\
qui pend en question posée à l'envers à qui veut bien\
passer\
et merci pour les confettis\
tous\

--- les graffitis\
merci\
merci à qui essuie les larmes avec sa chemise\
merci à une\
qui porte les sacs trop lourds pour des bras trop petits\

merci pour le dieu de l'olympe\
son faisceau derrière le nuage\
merci pour la foye et le moble\
l'incendie les fournaises et le métal poli\
en arrondi\
parfait\

merci à qui répare les chaussures\
merci pour\
la dentelle\
et les gravats\
pour l'alarme\
des enfants de graines mangées\
merci pour les pigeons chamaillent\
et le caillou à tête humaine\

merci pour le lait ciel inaccessible\
pour le ronron de la turbine\
qui réchauffe et protège\

moi animal\
qui remercie\
chaque passant passante\
tournant en orbite autour de l'arbre\
(pour les écritures je m'entraîne)\

je ne suis pas spécialiste\
de la vie\
mais je suis spécialiste\
des remerciements\

merci le vol du merle\
qui virgule le fauteuil roulant\
merci l'hésitation\
à mettre la main sur l'épaule\

merci le pas identique\
et le sommeil\
merci pour LA PHOTO DE GROUPE\

![encre de Chine de C. Jeanney](/img/210128-jeanney-en-resume-2.jpg)
