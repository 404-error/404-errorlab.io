---
persona:
  - Ann Persson
title: "faire du sale"
slug: "faire-du-sale"
date: 2021-09-06
poetry: true
hangindent: true
echo:
  - rythmique
images:
  - "img/210906-persson-faire-du-sale.jpg"
gridimages:
  - "img/210906-persson-faire-du-sale-grid.jpg"
sourceimage: "https://www.flickr.com/photos/usnationalarchives/8675975532/"
notes: ""
summary: "m'ont mis terter cage ouverte placage sac saccage mortuaire koi moi ma gueule sous cagoule sous-peuple qui se pique lourd et roule avec moi la tass s'enroulent koi les bleus leur ciel éteint le garrot la rouille plein le bras savent plus faire les keufs matraque et crache-crache ma gueule la bleue sur l'œil commune communarde connarde la cocarde l'unique de l'œil en moins et le bédo sur l'oreille la gauche en moins"
citation: "c'est pas moi le sexe je suis la supernova aux mauvaises mœurs"
---

m'ont mis terter cage ouverte

placage sac saccage mortuaire

koi moi ma gueule sous cagoule

sous-peuple qui se pique lourd

et roule avec moi la tass

s'enroulent koi les bleus leur ciel éteint le garrot la rouille plein le bras

savent plus faire les keufs

matraque et crache-crache ma gueule la bleue sur l'œil

commune communarde connarde la cocarde

l'unique de l'œil en moins

et le bédo sur

l'oreille la gauche en moins

derder koi c'est moi je rêve ma langue coupe-coupe et fume l'église et les flics

se fourrer les maîtres la poussière

sortir les kro les vaisseaux

mais n'extravague sous néant

et benzodiazépine ce que tu peux

pour trip pourtour d'entrailles qui crack sous colline ou sous acides

moi-ma-gueule-président-sans-dents vais en cercle jusqu'à l'oubli

fleuve en berne et y dégueule neuve

neuf fois m'y terre miteuse démissionne

le direkkkt koi ma sale gueule d'exode

l'exotique au direkkkt neuf heures moins une

le vingt heures qui te fourre vingt fois ma came rebeu jusqu'à l'hématome

vingt fois la pisse des colonies sur nerf optique

et coups de trique les bleus les loin

s'enfilent les sirènes avant les fourgons

mais fourgue la norme l'anormalité bicrave ton cul avant qu'on

me colle l'éthique ou l'étiquette

moi ma gueule je n'habite qu'en terminus ligne de brune

je suis tout entière à ma nébuleuse des lacrymogènes

protozoaire des mauvais trafics

avec ses tropiques tatoués sous les paupières

la rate explosée à tonfa

à donf sur les rondes et les tropismes

ratpi jamais finie et tarpé d'étoilée

koi moi la noiraude la maraude

la ratée coin coincer entre les dents la dentelle

j'emmerde dante et ses neuf putains de rondes

lui qui connaît que dalle au trente-sixième dessous de la chapelle

le parking et ses schlass plus brillants que l'orient

quand t'as ta dose ô ma descente

on se fout raide moi ma gueule le bazar d'antibrachial

le délire face à ce qu'il reste de quasar en nous

sous l'anthropoceci le capitalocela

joibour à la con

à la colle qui se met en cène

moi ma gueule je te fais du sale

de l'extinction de l'holocène dans ta face

sans barbus et sans traître

héroïsation de mes heures

jusqu'à ce qu'on renifle au cœur des choses

face à ceux qui me refusent la pièce pour le ou la taf

la suivante l'errance le long des pipes à crack

l'ouvre les lèvres avec

du kif un peu le rance la suée

et levant lévite lévitique

l'œil dans la tombe plus grand que tous les trous noirs

des speedballs à la place des pupilles

dilatées commac

en manque en coma

de lbd ou de libido

m'ont dit koi les keufs à koi ma gueule elle ressemble

bicot bicocotte la mi-meuf mi-chienne

et que koi moi ma gueule ça parle pas comme ça devrait

que ça parle derrière les périphériques

avec des manières sans-chic-chicots

urbi et orbi c'est pas michto la vie satellite

si j'aurais t'aurais koi t'aurais pas su la tainp

moi en culte moab d'époque

à la muette la daronne de toutes les bombes

sans si sans souci j'aurais si j'aurais su sué mes miettes

jeté au vent pissé contre et contre le mur

l'arrière ça tripe tripote contre le mur les mains

l'arrière-goût qui claque le coffre

coffrée pour comico jamais loin avec moi mon sexe des fractions

sexe rogné la rogne

et ça grogne au-dedans des cervelles les très blanches

division d'identité sans-dents et sans-sexe

ils disent koi les keufs avec leur matraque enfoncée dans mes soupirs

ils disent sur koi je dois vivre faire vivre et tapiner

sous-vie et sous-vide

ils disent komment et komment m'y mettre

ils disent koi et komment ils vont me mettre

les kondés moi ma gueule aux songes pourpres

je connais le secret des constellations et la cabale des cailloux

mais pourkoi il y a encore le drone plus bas que la mouette

au-dessus de la colline du crack son chant sorcière

spirale de grise ou d'idée fixe pour promesse du large

et la volaille qui continue à faire dire

et à faire fermer enfermer la vaste la vacante

ils disent pas ma gueule à ma gueule son trottoir

va me le faire plus loin

et moi d'esseulée je fais et le trottoir et la ville et le cosmos tout entier

type t tauri tapine

avec mes sœurs et mes mœurs grisailles

diptère de rien j'ai soif de mes clones

un peu cave la langue-langage généreuse

qui pourlèche ses shooteuses

se lèche les légionnaires sans peloton

les déglingués du piston

de koi se refaire la claque

pour un seul bifton

claquer le garrot sans pareil

ils koi ils pèlent dépècent pelotent la lopette

les rires toujours les rires et ça frappe entre les rires

les molaires en éventail ô ma descente tu t'en souviens

et le sang pour glu sur crâne

sous koi qui coagule

l'hémo l'héro plus dense que plasma quarks-gluons

le soir blédard connard

savent pas trop le genre où qu'il bascule le sexe

et frappent koi ma gueule par ma chatte hissée

drapeau menstrue autour du cou

c'est pas moi le sexe je suis la supernova aux mauvaises mœurs

crie koi sur les trottoirs je vais et je fais la rémanence de nos lumières

neuf fois les meufs inversées

zéro point neuf neuf poing coquarde dans ma gueule

blédarde connarde

l'hétéro le vilci et vide sa vessie

avant que je dévide mes tripes ma galette

eucharistie des putes

mon hétérocère du stellaire l'abîmé des petites gens

qui vacille neuf par dix-neuf para bellum lumen vide

qui vaque rien par dix fois la vitesse

et tube tubulaire pour faire bonhomme

du mec du vrai du lâche

tu lâches les paras dans tes ciels

sans géantes rouges pour faire la passe la messe

contre toute élégance les étoiles binaires vident leurs poches

les naines et leurs keums

leurs guns et leurs reums

et scintillent à nouveau tes nuits tes pulsars éteints

mon exobase ocre pour lame-lame la c

lamine les narines

free base de mes neurones

moi qui mâche qui machine moi moab ô mère

et mon amour d'intraveineuse là où ça peut

sovietwave la veine

way trop fast pour trip sous war machine

et mon congrès d'antique s'en souvient ma grève

le billot le billard et la débine

je mange mes cendres

et rumine l'aiguille jusqu'au neuf ceintures les débris

proto ou prolo la planète effondrée

rien de mon rien circumstellaire avant le premier métro

dernière passe passage à tabac la venelle

mise en ou à sac

de tiéquar sac de la rom

sur saccade droite et passage vide

droite d'extrême la milice palatine

sidi à l'os comme la petite l'histoire qui s'en revient

neuf ou trente-neuf plus trois le retour

la révolution et les camps

et la révolution des astres

encore un peu libre koi chelou ce stardust de mes azurs

faf et désastre trouve pas l'artère

comète des crimes mes muscles piquetés d'empyrée

under & underground & cie

d'empire ou du rab avec gunther et ses untermenschs

commode imperator du con la commodification comme et comme si commotion

rencontre troisième type de troisième kiss le kisdé file la clef la beigne

et je baigne moi k social en jus

ou kkkalibre ta

bpm ma gueule

suis flex

danse danse seule danse tout le soir et l'ère de planck

chuté-je

planquée déter avec mes disques d'accrétion

direkkkt les cieux mes en bas

mes bas filés

mes aïeux rebeu des autres

artena aux yeux pers

direkkkt droite gauche gyrophare droite et droite brune

gyrovague moi ma frappe moitié moite

mi-meuf mi-chienne sous-bourre sous-mâle

sur l'autre et l'autre rive sans âge koi l'esclave

l'enclave des clandés et des nuées

de oim mon visage mec mectonne t'étonne si la pilule je le porte haute

sous boutonnière la tox sa horse et ses hordes

la karba d'horreur faveur et vertu

tue la vertu la répu la faf

pour l'ouvre l'ouvreuse qui se rouvre sa veine

louvoie sur ou sur soir

peau brown-sugar

mi-noire mi-pute

et sans-terre sous-terter m'y mettre

ô zone zonarde ô marée marie mariejeanne

tout ça toussa pour oit matraque en croix

en creux de mes reins la hagra ça fout la sainteté au plus près de la lymphe

la suie sous les ongles

là suis rouge et mes ongles mon œil de bronze

un peu la moula au coin des mythes

la chimère a une gueule de tchoin

et l'écume de sphère en multiple du globule

multiplie l'image sa brûlure

celle qui traque avec l'arme la blanche

la traque à la carte le parti la matraque

contre le travail le trottoir

téci d'éternité

surseoir pleine la peine

et l'humble l'humide l'humiliation

la théorie des cordes pour se pendre

seul l'humus encore pour me répondre

pour me répandre ô les grands murs

la cloison de mon œil

percé

l'humeur schmitt sur ma face

face contre terre

sa garde droite gauche

garde haute l'extrême la droite

à vue ça tire ô la garde à vue

ma vue plus longue que l'éther l'éternité

mais koi moi ma gueule je vais je vois je vague

me pique avec de la matière noire

que ne vaincs-je

que ne venge

en arrière des grands murs

la nature est morte

et la sphinge somnole

la côte ouverte

au côté du siècle

et des seringues vides
