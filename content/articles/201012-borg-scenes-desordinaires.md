---
persona:
  - Otto Borg
title: "Scènes désordinaires"
slug: "scenes-desordinaires"
date: 2020-10-12
echo:
  - rythmique
images:
  - "img/201012-borg-scenes-desordinaires.jpg"
gridimages:
  - "img/201012-borg-scenes-desordinaires-grid.jpg"
sourceimage: "https://commons.wikimedia.org/wiki/File:Long_Island_duck,_Long_Island,_New_York_LCCN2017708800.tif"
notes: ""
summary: "Une femme attend dans une salle d'attente. Une femme attend dans une salle d'attente lasse d'attendre dans sa vie. Une femme attend dans une salle d'attente son rendez-vous pour remédier à sa lassitude d'attendre. La femme lasse d'attendre dans la vie est lasse d'attendre son rendez-vous auprès de l'association d'assistance au suicide. La lassitude d'attendre répond à la lassitude d'attendre. La femme n'attend plus. Elle ouvre la fenêtre de la salle d'attente. Elle se défenestre."
citation: "L'homme communie avec le kérosène"
---

*Mon bulletin de vote a pris parti pour la poubelle. Je me désolidarise de mon bulletin de vote.*

{{%asterisme%}}

Une femme attend dans une salle d'attente. Une femme attend dans une salle d'attente lasse d'attendre dans sa vie. Une femme attend dans une salle d'attente son rendez-vous pour remédier à sa lassitude d'attendre. La femme lasse d'attendre dans la vie est lasse d'attendre son rendez-vous auprès de l'association d'assistance au suicide. La lassitude d'attendre répond à la lassitude d'attendre. La femme n'attend plus. Elle ouvre la fenêtre de la salle d'attente. Elle se défenestre.

{{%asterisme%}}

*Je me sens plus frère d'un frêne que d'un gardien de prison.*

{{%asterisme%}}

Les gens n'ont pas beaucoup d'importance pour les gens

Les gens se lèvent pour les gens

Les gens travaillent pour les gens

Il arrive même quelquefois que les gens aiment ou pleurent pour les gens

Mais les gens traversent les gens sans y regarder

Les gens balaient les gens de leur chemin pour que leur chemin demeure ouvert à d'autres gens

Les gens licencient les gens

Les gens suicident les gens

Les gens silencent les gens

Les gens sont des gens

Parce que les gens acceptent de ne pas entendre le bruit que font les gens quand ils tombent

{{%asterisme%}}

*Je pense à mes ancêtres décomposés qui ont permis de produire le sac plastique dans lequel ma tête est enfoncée.*

{{%asterisme%}}

L'homme fait du kérosène

Et le kérosène fait voler des étoiles

Les étoiles de kérosène font briller le ciel des villes

Et cela coûte à l'homme quarante heures répétées quatre fois par mois

Si l'homme souhaite faire traverser le ciel à l'homme

Si l'homme souhaite habiter l'habitacle de l'étoile

Si l'homme souhaite briller parmi la nuit de l'homme

L'homme a dépensé quatre fois quarante heures

L'homme communie avec le kérosène

L'homme a fait le tour du ciel

L'homme revenu en son endroit se remet à son travail

L'homme se remet à faire quarante heures quatre fois par mois

Et kérosène contre salaire l'homme rêve

Le ciel est à son trafic de kérosène

L'homme rêve de trafic

L'homme rêve de retourner à son tourisme

L'homme rêve de kérosène

L'homme travaille ses mois de quatre fois quarante heures

L'homme travaille ses années de mois de quatre fois de quarante heures

L'homme prend sa retraite

Et sa retraite va au kérosène

Le rêve de l'homme rêve jusqu'à ce que le rêve s'épuise

Jusqu'à ce que l'homme ne rêve plus de kérosène

Et l'homme fermente les sols avec ses rêves évanouis

L'homme est la puissance du kérosène

Inutile à l'homme qui souhaite un prix soldé pour trafiquer son tourisme

L'homme ne veut plus une dernière minute

L'homme veut un vol de dernière minute

L'homme ne veut plus être un homme

L'homme veut être du kérosène

{{%asterisme%}}

*Je fais le trottoir pour me rincer la dalle.*

{{%asterisme%}}

Un pavé qui s'envole vers les forces de l'ordre : une roche plutonique magmatique à texture grenue, taillée en cube de neuf centimètres d'arête et pesant un virgule quatre-vint-huit kilogramme, se déplace vers son objectif compte tenu de la vitesse initiale, de la résistance de l'air, de l'angle de portée ou encore de l'accélération de la pesanteur terrestre. Le pavé n'atteint pas sa cible.

{{%asterisme%}}

*Ma vie est sans salut. Je cherche le pardon auprès de chaque passant que je bouscule.*
