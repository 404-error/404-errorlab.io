---
persona:
  - Walter Benjamin
title: "Poème triste"
slug: "poeme-triste"
date: 2023-10-16
echo:
  - rythmique
images:
  - "img/231001-benjamin-poeme-triste.jpg"
gridimages:
  - "img/231001-benjamin-poeme-triste-grid.jpg"
sourceimage: "https://commons.wikimedia.org/wiki/File:Vista_de_Portbou_des_d%27una_embarcaci%C3%B3_enmig_del_mar_(Extacted_-_Restored).jpg"
notes: "Cette nouvelle traduction se place dans le domaine public volontaire. Le texte allemand se trouve dans Walter Benjamin, *Gesammelte Schriften, VI*, Frankfurt am Main, Suhrkamp, 1985, p. 520.<br><br>L'image est une image de Portbou. XX<sup>e</sup> siècle, première décennie. L'image cherche une négativité de son instant."
summary: "On est assis sur une chaise et on écrit. / On est de plus en plus fatigué et de plus en plus fatigué. / On se couche à l'heure, / On mange à l'heure. / On a de l'argent, / C'est le bon Dieu qui l'a offert. / La vie est merveilleuse ! / Le cœur bat de plus en plus fort et de plus en plus fort, / La mer est de plus en plus calme et de plus en plus calme / Jusqu'au fond."
citation: "jusqu'au fond"
poetry: true
hangindent: true
---

## Poème triste

On est assis sur une chaise et on écrit.

On est de plus en plus fatigué et de plus en plus fatigué.

On se couche à l'heure,

On mange à l'heure.

On a de l'argent,

Le bon Dieu l'a offert.

La vie est merveilleuse !

Le cœur bat de plus en plus fort et de plus en plus fort,

La mer est de plus en plus calme et de plus en plus calme

Jusqu'au fond.

 

{{%alignement-d%}}
San Antonio 11.4.33
{{%/alignement-d%}}

## Trauriges Gedicht

Man sitzt im Stuhle und schreibt.

Man wird müder und müder und müder.

Man legt sich zur richtigen Zeit,

Man ißt zur richtigen Zeit.

Man hat Geld,

Das hat der liebe Gott geschenkt.

Das Leben ist wunderbar!

Das Herz klopft lauter und lauter und lauter,

Das Meer wird stiller und stiller und stiller

Bis auf den Grund.

 

{{%alignement-d%}}
San Antonio 11.4.33
{{%/alignement-d%}}

