---
persona:
  - Lucien Raphmaj
  - Marc Verlynde
title: "Stase-Seconde"
slug: "stase-seconde"
date: "2022-04-14"
echo:
  - esthétique
images:
  - "img/220401-lrmv-stase-seconde.jpg"
gridimages:
  - "img/220401-lrmv-stase-seconde-grid.jpg"
sourceimage: "lucien raphmaj"
notes: ""
summary: "Nous sommes partis de cette pensée en commun : ce n'est pas nous qui nous exprimons. Dès lors, plus qu'un dialogue, nous avons opté pour la figuration transitoire du dédoublement de personnalité (doppelgänger). Essai d'une pensée en regard plutôt qu'en miroir. Espoir aussi de résoudre des contradictions, de nous extraire des oppositions binaires. Allez savoir si nous y sommes parvenus, mais nous essayons d'offrir d'autres liens. Voici donc cet autre nom de l'amitié."
citation: "trouver une langue pour parler de ce qui nous échappe"
---

*Ici se lit la poursuite d'un entretien ouvert sur la critique littéraire, sur ses puissances et ses impuissances,
et dont la question de l’amitié, après celle de l'idiotie dans [À te souvenir de l’insomnie des mondes](/a-te-souvenir-de-linsomnie-des-mondes/),  est comme la seconde stase, en regard, l’incarnation de ce dédoublement dont il nous paraissait important de témoigner comme critique en approfondissant notre perplexité.*

---

*Nous proposons ici une pratique de l'amitié. Elle se déploie en échange
afin de laisser entendre qu'elle est à la recherche de son objet, de son
sujet. Nous ne proposons pas ici de définition : nous les essayons. Au
risque de la complexité. Nous proposons un égarement.*

## 1

Nous sommes partis de cette pensée en commun : ce n'est pas nous qui
nous exprimons. Dès lors, plus qu'un dialogue, nous avons opté pour la
figuration transitoire du dédoublement de personnalité (doppelgänger).
Essai d'une pensée en [<span id="regarda">regard</span>](#regardb) plutôt qu'en miroir. Espoir aussi de
résoudre des contradictions, de nous extraire des oppositions binaires.
Allez savoir si nous y sommes parvenus, mais nous essayons d'offrir
d'autres liens. Voici donc cet autre nom de l'amitié.

## 2

Au nom de l'autre, toujours, ni consacré ni sacrifié, et plutôt que
voulant faire communauté, voulant accomplir cette prose solitaire mais
adressée, une sorte d'amitié aveugle et plurielle pour laquelle on n'a
pas de mot, parce que cette relation toujours à réinventer pour faire de
la critique autre chose qu'un commentaire : une affirmation créatrice de
valeur portant elle-même à créer --- vertu de la critique.

## 3

Nous nous sommes élancés de ce qu'en commun nous faisons : la critique
de la critique. L'amitié comme partage d'expérience se poursuit dans la
réflexion, toujours dans une définition à l'essai. Nous tentons d'en
offrir ici une plus généreuse, ardente : une manière d'être au monde par
et dans la critique.

## 4

La critique est alors passage de témoin. Plus que des mots de passe
faits de noms fameux, quelque chose qui se donne, quelque chose qui se
perd, dans une parentèle sans parenté. Renaissance et reconnaissance par
la critique d'une dimension fondamentale de la parole comme milieu
indéterminé, surdéterminé, demandant toujours de défaire et refaire sa
parole, une parole qui vient toujours d'ailleurs (et c'est cela que l'on
est amenés à reconnaître).

## 5

Il faudrait critiquer la critique. Dans un repentir, interroger la
clarté de nos propositions. Nous convoquons des voix, nous empruntons
des pensées, nous en interrogeons la possibilité de coïncidences. C'est
l'autre stase du dialogue offert ici : on parle avec des auteurs et non
d'eux. On écoute tout ce qu'en nous ils peuvent transformer.

## 6

La critique n'a pas d'objet, n'a pas de sujet, car elle est relation,
c'est-à-dire, littéralement, *internet*, milieu réticulé où les
critiques-araignées rejoignent un bestiaire interspécifique que l'on a
de cesse de multiplier. Il y aurait à dire encore sur le milieu et le
média, l'immédiat et l'extase de la critique, les modes de
désubjectivation qu'elle nous semble mettre en place dans notre
expérience. Pour nous la critique est la possibilité de cette expérience
du texte désubjectivé, mélange toujours impur, fiction et analyse, mots
et imaginaires se croisant pour produire des possibles. Folie
merveilleuse que ce creux de la parole.

## 7

La critique ne rejoindra donc jamais son objet, elle se refusera à
l'amour (l'admiration) et à la haine (nihilisme) : elle devra inventer
de nouvelles proximités et de nouvelles distances. Quelque chose comme
de l'amitié, oui, à nouveau, toujours, soustraite à la présence, au
genre, au désir --- désir, neutralité, absence composant une adelphité
donnant à la différence (devenir-étrange) toutes ses dimensions.

## 8

Nous nous ouvrons alors aux contradictions et aux paradoxes. Nous
voulons la clarté et l'obscurité, les évidences et les ombres. Nous
touchons alors à une définition de la critique essayée ici : nous ne
pouvons pas être plus [<span id="limpides">limpides</span>](/ligne-dombre/#limpides). L'amitié est elle aussi réserve et
déplacement de sens. Tentative toujours reprise d'éclaircissement.

## 9

Nous nous donnons à la critique à corps perdu, dans un don et un abandon
faisant de l'impatience et de la *différance* des modalités de cette
parole urgente à se déclarer et ne cessant de faire différer l'esprit du
texte, d'ouvrir des horizons, de former des liens, une vaste amitié de
texte et de monde. Critique par sympathie. Par folie. La critique comme
[<span id="derangement">dérangement</span>](/ligne-dombre/#derangement), comme décentrement, comme *dégenrement*, car n'ayant jamais
plus de genre, traversant les genres, ne s'y arrêtant pas : romantisme
de l'essai, sympoésie de la philosophie, transfiguration de la prose ---
il faut être attentif à ce qui, en littérature, déplace les classes,
nous faisant transfuges du passé, du présent et de l'avenir.

## 10

La critique est cet exercice de nos identités plurielles se donnant à
penser dans l'échange. Pensée spéculative d'autant plus efficace que
généreuse, d'autant plus universelle que spécifique, d'autant plus vraie
que paradoxale.

## 11

La critique n'a fait qu'interpréter le monde littéraire, ce qui importe
maintenant c'est de le transformer.

{{%asterisme%}}

{{%colonnes%}}

{{%colonnes-gauche%}}

"*Brusque illumination : moi tout entière (non, ma moitié), mon second
moi-même, mon autre moi-même, mon moi-même terrestre, alors que j'aurais
vécu pour on ne sait quoi --- je ne me connais pas.*" ([m.t.](/desidentite/#mt))

{{%/colonnes-gauche%}}

{{%colonnes-droite%}}

"*La vie de l'esprit entre amis, la pensée qui se forme dans l'échange
de parole, par écrit ou de vive voix, sont nécessaires à ceux qui
cherchent. Hors cela, nous sommes par nous-mêmes sans pensée.*" ([f.h.](/desidentite/#fh)).

{{%/colonnes-droite%}}

{{%/colonnes%}}

## 1 --- D'un clivage critique

<div class="colonnes">

<div class="colonnes__gauche">

**À défaut d'être acéphale**, elle sait seulement simuler les cris de
l'idiot, la critique est **<span class="repere" data-repere="repere--ami">amitologie</span>**, (entendre une trouée du texte
par des affabulatrices béances, en attente d'une mythologie sans dieu ni
tête). Entendons-nous. La critique est une invention, une *persona*
dépersonnalisée : [<span id="moi">ni moi ni toi</span>](/a-te-souvenir-de-linsomnie-des-mondes/#moi) ; des initiales d'auteurs, des auteurs
initiatiques. La tangence d'une absence qui parfois, histrionique
**gauchissement**, semble [<span id="coincidera">coïncider</span>](#coinciderb). Parole double qui se trouble
par les sympathies, les revenances, que toi qui lis tu y places. Une
figure et une **fiction**. Un individu clivé dans cette retentissante
inexistence de l'écoute, troublé par cette voix entendue, parfois, comme
un dédoublement de ce qu'il appréhende [<span id="etrea">être</span>](#etreb).

Fiction donc d'un sujet critique, réflexif. Au commencement, s'il
fallait avoir recours aux passages obligés du récit, cette parole
critique se colorerait de la volition d'un autoportrait en lecteur, en
lectrice. Anamorphose --- c'est moi-même que je peins. Fût-ce à sauts et
gambades, dans la conscience de ce que nous avons de magnifiquement vain
et ondoyant, serait-ce aussi dans l'espoir du suffisant lecteur, tout
ceci fut **<span class="repere" data-repere="repere--essai">essayé</span>** ([m.m.](/desidentite/#mm)). Désormais, dans un clivage incroyant, tout
ce qui est humain, **idiotement**, m'est étrangement étranger ; on ne
naît pas humain, on ne le devient pas. On s'altère ; on porte un
**masque** qui s'efforce de manifester une spécieuse singularité.

On métamorphose alors (littéralement : on essaie), la critique en une
écriture délibérément, éthiquement mis en échec grâce à sa propension à
être à côté, à l'écart, de ce qu'elle ne démontre pas ; en retrait de
toute posture d'auteur, d'autorité. La critique serait écoute : une
parole [<span id="neutrea">sans pouvoir</span>](#neutreb). Entre humilité et effacement, elle l'emprunte
et l'emploie. Une autre façon d'être au monde : on existe par
usurpation. Qui écrit est alchimique amalgame : désidentification.

La critique est **clivée**, mise en échec, elle exprime l'inadéquation
de ses aspirations. Elle regarde cette constante affirmation de
l'éloignement à ce qu'elle retranscrit comme une nouvelle distance, une
nouvelle inadéquation. La critique, parfois, désarçonnée ([p.q.](/desidentite/#pq)),
surprend de **troubles reflets** de ce qu'elle n'est pas, inquiète
souvent elle contemple l'image que son lectorat croit deviner d'elle.
Elle est ailleurs, déjà. Dans un autre livre qui déformera la conception
protéiforme d'une identité qu'elle veut réengager à chaque lecture. Une
fiction informe.

La critique clivée entre l'esprit et la lettre. L'attention aux [<span id="detailb">détails</span>](#detaila),
aux rêves qu'ils suggèrent. La critique s'extasie, au cœur de ses
contradictions, de la question idiotement centrale du comment ça
**s'écrit.** Rien qu'un hasardeux amoncellement de mots qui prétendent
disséquer ceux d'autrui. Une critique mot à mot sape (habiller et
creuser) la matière pour mieux, par [<span id="dissonanceb">dissonance</span>](#dissonancea), entendre
l'antimatière fuligineuse, songes et autres revenances, qui nous
désidentifient, nous lisent.

Par ce clivage, par la conscience de sa fondamentale insuffisance, on
rêve ici, ensemble, d'une critique comme exercice d'amitié. Une
disjonction, des mots placés en parallaxes pour effleurer une tangence
commune. On se lit, on repart autrement. On laisse parler l'autre, en
Soi, l'ami qui pour nous franchira la ligne, le lire et le relire comme
trace patente, reflet évident de celui qu'on n'est jamais tout à fait.
Parce qu'ici s'expriment deux voix, malgré tout, et que l'on croit en
faire une **<span class="repere" data-repere="repere--seule">seule</span>, expression** de cet être profondément clivé, trouble,
critique, comme on voudra, celui qui sait n'exister que dans la
contemplation (invention et retour) du visage de l'Autre, de la
totalité.

"Mes écrits ne parlent que de mes victoires : j'y suis “moi”, avec tout
ce qui m'était contraire" ([f.n.](/desidentite/#fn))

</div>

<div class="colonnes__droite">

***<span class="repere" data-repere="repere--ami">Amitier</span>*** comme l'invention nécessaire par la critique d'un verbe
sans sujet et intransitif, semblable à ce qui est arrivé au verbe
[<span id="ecrire">écrire</span>](/ligne-dombre/#ecrire]) dans notre littérature moderne : *Écrire*, dit-elle ([m.d.](/desidentite/#md)). C'est cet
intransitif fait de noms défaits de leur propriété [<span id="sacreb">sacrée</span>](#sacrea) qui dit
ce qui nous est commun dans l'écart.<br><br>
La lecture est peut-être tout simplement ce ***différentiel***. Écart
d'écart. Sorte de dialogue sur fond de perpétuels et vertigineux écarts
--- rencontre du 3e genre ([m.b.](/desidentite/#mb)).<br><br>
"C'est important de diviniser un monde sans divinité ; de trouver des
mythes dans un monde vide." ([a.p.](/desidentite/#ap))<br><br>
Ainsi faisons-nous jamais que nous **inventer** à chaque lecture, que de
restituer quelque chose de cette ontologie de la Relation : "changer en
échangeant sans se perdre ou se dénaturer" ? ([e.g.](/desidentite/#eg))

**<span class="repere" data-repere="repere--essai">Essayons</span>** encore de penser cela[^1].<br><br>
Singulièrement, c'est-à-dire toujours **idiotement** (ἴδιος : ce qui se
singularise; et ici en se désubjectivant).<br><br>
"Tout ce qui est profond aime le **masque**. Tout ce qui est profond a
besoin d'un masque, je dirais plus : un masque se forme sans cesse
autour d'un esprit profond." ([f.n.](/desidentite/#fn))<br><br>
Pourtant cette critique se démasque et cherche un visage, une promesse
fragile faite de ces mots, de ces existences précaires faites de mots.
Conception non pas tragique mais éthique de la critique et de la
lecture. Comme s'il en allait de la lecture comme d'un rapport à l'autre
--- d'une adresse infinie, destinerrance ([j.d.](/desidentite/#jd)) retirée de toute
assimilation, de toute admiration --- égalité et sympathie de toute
amitié.<br><br>
**Clivée**, schizo, l'instance critique est elle aussi un "poète
caméléon" affecté d'une "*negative capability*" ([j.k.](/desidentite/#jk)). Critique-caméléon
amie de la folie car se faisant parole de l'autre, de l'autre en
soi-même annulé, parole en creux (*folia* : folie, feuille creuse ;
parole en arborescence tout autant, folie, folie).<br><br>
Mets-toi les pierres dans les poches et rentre dans **les vagues**
([v.w.](/desidentite/#vw)) appréhende cette multiplicité de points de vue, ces mouvements
infinis du réel, de la perception et de l'imaginaire. Ventriloquie à la
manière du monde et des textes à la manière d'[a.r.](/desidentite/#ar), travaillant avec des
textes amis, de secrets et des mots, des paroles hantées par d'autres
paroles.<br><br>
Il faut donc surprendre ce "méchant hybride de lecture et
d'**écriture**" ([m.b.](/desidentite/#mb)). Apprendre à constituer une ressemblance sans
image, une ressemblance informe. Inventer, découvrir des textes :
toujours la même ambivalence.<br><br>
*[<span id="textus">Textus invenio</span>](/desidentite/#textus)*. Des textes comme des rencontres.

**Des <span class="repere" data-repere="repere--seule">contradictions</span>**. Des manières de penser avec l'autre et contre
soi.<br><br>
Exercice de patience et d'[<span id="seula">étrangement</span>](#seulb).

</div>

</div>

## 2 --- D'une traduction du nom de l'autre

{{%colonnes%}}

{{%colonnes-gauche%}}

**Être à l'écoute des voix de l'autre**, celle qui écrit, celle qui lit
--- chaque instance se multipliant, visage dans un visage dans un
visage... comme ce moine chinois que nous affichions lors de notre
[première stase idiote](/a-te-souvenir-de-linsomnie-des-mondes).

"Non pas arriver au point où l'on ne dit plus je, mais au point où ça
n'a plus aucune importance de dire ou de ne pas dire je. Nous ne sommes
plus nous-mêmes. Chacun connaîtra les siens. Nous avons été aidés,
aspirés, multipliés." ([g&d](/desidentite/#gd))

Perdre le nom, proliférer, amitié de la [bactérie](/desidentite/#dh). Nous ne parvenons à penser la critique que dans ce double
mouvement : perdre le nom et "être tous les noms de l'histoire" ([f.n.](/desidentite/#fn)).

Que la lecture soit suspension de l'illusion du Moi, que la littérature
soit écrire sa langue comme <span class="repere" data-repere="repere--trad">une</span> **[<span id="langueb">langue étrangère</span>](#languea)** ([a.v.&m.p.](/desidentite/#avmp)),
c'est encore trop peu dire.

Souscrire : écrire en retirant, et faisant de ce retrait une
affirmation. Oui, encore faut-il la critique comme affirmation, une
affirmation de l'impureté de cette solution, comme re-présentation de ce
phénomène étrange : que les noms sont toujours impropres, des lectures
toujours imaginaires, que l'échange est infini.

La critique est quelque chose de la traduction entre des idiomes inouïs.
Elle (doit) (donc) aussi invente(r) sa propre langue dans la langue.

{{%/colonnes-gauche%}}

{{%colonnes-droite%}}

La critique comme une **<span class="repere" data-repere="repere--trad">traduction</span>**, une trahison révélatrice. Un
rapport d'amitié textuelle, d'appropriation de ce que l'ont su dire. [c.b.](/desidentite/#cb)
et l'auteur du combat avec l'ange de William Williamson, le seul à
traduire les glissements de sens d'une langue sinon mal maîtrisée du
moins non parlée et comprendre ainsi que le [<span id="fantastique">fantastique</span>](/desidentite/#hpl) est
avant tout verbal. Expression primale des limites de la perception
personnelle. Critiquer : trouver une langue pour parler de ce qui nous
[<span id="echappe">échappe</span>](/desidentite/#echappe). Et [a.a.](/desidentite/#aa), bien sûr.

{{%/colonnes-droite%}}

{{%/colonnes%}}

## 3 --- Des sosies et des simulacres

{{%colonnes%}}

{{%colonnes-gauche%}}

**Être critique pour être l'auteur et soi**, radicalement mis à
distance.

*[<span id="clivagea">Mon corps difficile</span>](#clivageb)* ([**r.c.**](/desidentite/#rc))

et ma psyché folle ?

et qu'y voit-on donc ?

Des lacs d'ombres où l'on s'enfonce, pâte obscure et transparente du
langage où telle une larve on prend toutes les formes lactées de la
métamorphose.

On avait dit : la *critique-doppelgänger*.

Avoue que c'est substituable aux Vampires, et moins utilisé que les
[<span id="personnea">Sirènes</span>](#personneb) ([m.b.](/desidentite/#mb)).

La *critique-**simulacre*** serait encore plus sidérant aussi.

*De rerum littera*, où des petits phénomènes entrent dans la tache
aveugle de ta vision. Tu essaies d'en comprendre les dessins
labyrinthiques, les [<span id="couleursb">couleurs indécidables</span>](#couleursa), tu écris ta critique
poursuivi par ce point aveugle qui hante le texte lui aussi. C'est ce
que disait [j.c.](/desidentite/#jc) : écrire, c'est poursuivre ce [<span id="regardb">point aveugle</span>](#regarda).

Critique et écrivain se confondent alors, atteints d'un syndrome de
Capgras :

"Il va sans dire que je ne suis pas de ceux qui croient que les
écrivains sont les meilleurs critiques ; je crois néanmoins que tout bon
écrivain est, qu'il le sache ou non, un bon critique" ([j.c.](/desidentite/#jc))

La <span class="repere" data-repere="repere--sosie">*critique-**sosie***</span> aussi alors, pourquoi pas, si on exclut la
vieille idée romantique de l'herméneute (herménaute ?
semi-divinité psychopompe faisant l'anabase dans le texte ?)
s'assimilant à son modèle.<br><br>
Au contraire, la critique doit garder l'écart à soi-même et au texte
pour se produire en des milliards de clones différents : pensées, rêves
et hypothèses.<br><br>
Faux jumeaux, [<span id="simulacrea">faux semblants</span>](#simulacreb) --- beauté de la critique qui
appartient à la figure de l'auteur, retirée de son existence ([m.f.](/desidentite/#mf)).<br><br>
Critiquer pour se multiplier --- vieille histoire. On lit pour accumuler
les expériences, pour se morpher en d'autres temps, d'autres lieux.<br><br>
On critique en se mettant à la place de la personne qui a écrit non pour
la juger, mais sur un fond d'amitié, de sympathie, en essayant de
comprendre ce qui se traduit d'[<span id="hapaxa">unique</span>](#hapaxb) dans ce qui est raconté.<br><br>
Singularité idiote. Ainsi de la poétique de *La semaine perpétuelle* de
[l.v.](/desidentite/#lv), litanie déployant l'étrangeté de toute chose.

Mais de ce **<span class="repere" data-repere="repere--reel">réel-là</span>**, de cette singularité, de cette pauvreté en
esprit, on a déjà parlé, parlant de la nécessité de se défaire de tous
les arrières-mondes pour ouvrir ce [<span id="sacrea">sacré</span>](#sacreb) sans fond, appel à ce qui
vient, à tout l'avenir de la parole.

Critiquer est s'adresser. S'inquiéter, s'enquérir du texte, lui poser
des questions peut-être sans réponse. C'est respecter son silence.

{{%/colonnes-gauche%}}

{{%colonnes-droite%}}

De la paranoïa critique (**[r.c.](/desidentite/#rc)**) : "une méthode spontanée de
connaissance irrationnelle, basée sur l'objectivation critique et
systématique des associations et interprétations délirantes". Peut-être.
La peur comme ferment --- ses métamorphoses pour détourner cette
révélation : nous n'existons pas. D'ordinaire, on y oppose des
substituts, des activités, des réussites. On vous propose une parole qui
passe comme le rêve. Faites-en ce que vous voyez. Des ressemblances, des
simulacres.

**Simulation** & émulation : une vague de rêve. Et si nous parlions des
livres lus comme dans un rêve, le [<span id="simulacreb">simulacre</span>](#simulacrea) de notre vie diurne,
l'achèvement de notre vie éveillée. J'avance masqué mais je pointe du
doigt mon masque ([g.b.](/desidentite/#gb)) : critique. Et dans le simulacre, la survivance
de la pensée magique ?

**Sosie**, <span class="repere" data-repere="repere--sosie">soit</span>, mais par pastiche. Déchiré entre le désir de ne pas se ressembler, renaître ailleurs dans chaque lecture, et celui inconscient, voire informe, de reproduire la logique verbale dans laquelle la lecture nous assimile. Ensuite, laminoir de l'écriture, exercice de dissemblance : ça ne ressemble à rien. On est déjà dans la lecture suivante. Ou peut-être encore à éclaircir ce qu'on surprend de celle passée. Distillation, décantation. On se réveille, on ne reconnaît le sosie qui est tout aussi nous-mêmes que celui qui subit le rêve. On ne se voit pas rêver, pourquoi faudrait-il se regarder lire, se voir en prolonger l'expérience dans l'écriture ?

Mais ce **<span class="repere" data-repere="repere--reel">réel</span>** revient, impossible. "Tout a déjà été dit. Mais comme
personne n'écoutait..." se murmure-t-il encore. Parler alors de ce qui
revient sous une autre forme, nous sommes hantises irrésolues. Idiots
alors par une conscience palimpsestueuse : inscription, une seconde,
dans ce qui s'efface, se confond, se lit comme une inscription seconde.<br><br>
L'amitié comme silence --- "Il ne faut pas parler de ses amis :
autrement, on trahit par des paroles le sentiment de l'amitié.".
L'amitié entretenue ici s'exprime donc dans cette virtualité du silence.
La critique, ce serait comment taire.

{{%/colonnes-droite%}}

{{%/colonnes%}}

## 4 --- Du doppelgänger à la désindentification

{{%colonnes%}}

{{%colonnes-gauche%}}

**Peut-être mon ami, pourriez-vous m'appeler William Williamson**. Il
paraît que nous faisons œuvre de critique. Comment cela, je parle tout
seul ? Mais puisque je vous entends, c'est que vous devez m'écouter.
Sophisme et paradoxe, je n'en disconviens pas, continuons à nous parler
mon ami. S'appeler Blandine Volochot, comme la fiction du nom, identité
dédoublée qui finit par nous dévorer ? Oui peut-être pouvez-vous nous
appeler ainsi. Si ça vous amuse d'attraper des mues, des méduses.

"L'auteur doit se taire lorsque son œuvre se met à parler." ([f.n.](/desidentite/#fn))

**<span class="repere" data-repere="repere--monstre">Doppelgänger</span>.** Inquiétude, incontinuité ; intranquillité,
non-conformité. Le fantastique par le doute, l'existence d'une autre
voix qui toujours intime, dans la défaillance d'une perception
personnelle, une autre explication, la possibilité que tout ne [<span id="coinciderc">coïncide</span>](#coincidera)
pas exactement. L'accès à la réalité, par dévoilements
successifs, par troublements. Une marge d'inachèvement (~~[m.l.](/desidentite/#ml)~~), une
suspension de sens, un tremblement dans la rationalité pour y laisser
entrer des revenants, les souvenirs de ce qui n'appartient à personne, à
tous, tout aussi bien nous les nommerons imagination. Peut-être
pourrait-on lui trouver de nouvelles virtualités, d'anciennes
dénominations détournées.

Une heureuse et ardente inadéquation. Une sorte de schize en mineur.
Sans **<span class="repere" data-repere="repere--ouvrir">rabaisser</span>** le réel à sa vérification factuelle, le texte à sa
platitude matérielle, espérer donc en faire entendre une autre voix.
Épreuve alors de la clarté, une sorte de limpidité pour ne pas sombrer
dans la sophistication verbale. Ici s'exprime, s'expulse et s'invente un
Nous sans majesté : une sorte de pluralité à l'essai pour s'extraire,
rêve-t-elle, de la binarité abolie [l.r.](/desidentite/#lr)\|\|[m.v.](/desidentite/#mv) : l'un ou l'autre, l'un et
l'autre, aucun des deux, une entité mixte, une sorte de dédoublement.
Doppelgänger, disait-il alors on écoute ce que l'on ne savait pas avoir,
caché, au plus profond d'un nous qui échapperait aux limites du Moi, aux
contraintes de l'identité. Désidentité.

Désidentitaires ([r.j.](/desidentite/#rj)) nous sommes. Politiquement, poétiquement. On
**<span class="repere" data-repere="repere--opera">opère</span>** ici une partition. Sans ligne ni scission définies.
Improvisation : écouter, puis rejouer. Déformer, s'approprier au sens où
ici se fait sien surtout ce qui ne se comprend pas. On opère des
rapprochements pour mieux sentir les tangences, les endroits où l'on
s'effleure, ceux où une pensée pourrait presque dire Nous : les
instants, où Toi (lecteur & lectrice) avec ferveur nous le voulons, tu
te désidentifieras dans ce **<span class="repere" data-repere="repere--nous">Nous</span>**. On est idiots : précisons au risque
de la simplicité, de l'évidence déjà entendue par Toi. Le Nous est
politique : vecteur d'une communauté (dans le trouble et le doute) où la
somme des individus la composant n'apparaîtrait que dans ce mouvement de
désidentification. Un individu poétique partant. Le poète comme
indicateur de l'avenir ([f.n.](/desidentite/#fn)) : le rêve d'un individu nouveau, toujours
**à <span class="repere" data-repere="repere--je">venir</span>**.

Doppelgänger : le critique se distingue dans ce type de pétition de
principe. Nous soulignons des accointances et l'essai alors, une
nouvelle fois, se dédouble. Une question de proximité dans la glose pour
ce qui demeure, pour nous, des stases d'amitié. Le critique glose comme
pour essayer des identités, pour ne pas entièrement s'y reconnaître.
Sans doute aussi fluctuer dans les dichotomies. On le pose trop
franchement pour mieux les troubler. Un spectre au sens optique et
sonore : une grisaille. Ici la théorie, là, dans un [glossaire de
désidentification massive](/desidentite), la pratique. À
moins que ce ne soit l'inverse, que l'on préserve ainsi la marge d'une
autre chose, **<span class="repere" data-repere="repere--toujours">toujours</span>**.

{{%/colonnes-gauche%}}

{{%colonnes-droite%}}

"Faute de miroirs, j'aurais les faces des bêtes changeantes de mes
désirs, et certains jours où le miracle me touche, je n'aurais plus de
face." ([r.g.-l.](/desidentite/#rgl))

**<span class="repere" data-repere="repere--monstre">Monstruosité</span>** de la critique, elle dont on pense qu'elle ne peut pas
exister dans un monde de pure présence au texte, métaphysique dont on
pensait s'être débarrassée.

*Re-enter ghosts*.

Ce dont on s'est débarrassés : la critique de surplomb.

La critique qu'il reste à inventer : celle qui se réinvente avec le
texte, qui réinvente la figure de l'auteur, l'écarte, la fait proliférer
de l'intérieur, multiplicité qui finit par la faire éclater. Pas un
sosie : des différences créatrices.

"**<span class="repere" data-repere="repere--ouvrir">Ouvrir</span>** toutes les choses, même les quotidiennes, sur une conscience
aiguë, *seconde*. Impossibilité de se satisfaire de l'automatisme.
L'intensité peut être partout, il suffit pour cela de prendre, en tout
acte, son souffle ... Ce qui te définit soudain comme pur hiatus" ([a.r.](/desidentite/#ar))

On rêve à ces **<span class="repere" data-repere="repere--opera">opéras</span>** à venir de la critique, suite de
textes-glissades, de mouvements de dérives portés par les vitesses et
les suspens initiaux de l'œuvre.

**<span class="repere" data-repere="repere--nous">Nous</span>** sommes à l'heure de transformation de nos [<span id="cosmovisionsb">cosmovisions</span>](#cosmovisionsa).
Des imaginaires des textes et du sujet. Du sujet et du texte. De notre
trouble décomposant le romantisme néocapitaliste. Recomposant des atomes
de sens un ciel idiot, une vacance où l'on se retrouve souffler avec le
[<span id="venta">vent</span>](#ventb) et nous imaginer dans ce souffle.

**<span class="repere" data-repere="repere--je">Je</span>** est un Multiple de toi. On lui trouvera le plus grand
dénominateur commun : l'absence du nom. "Ça n'a, comme toi, pas de nom.
Peut-être/êtes-vous la même chose. Un jour peut-être/toi aussi, tu
m'appelleras/ainsi." ([p.c.](/desidentite/#pc))

**<span class="repere" data-repere="repere--toujours">Promesse</span>**.

{{%/colonnes-droite%}}

{{%/colonnes%}}

## 5 --- De l'entente de l'absence dans la critique

{{%colonnes%}}

{{%colonnes-gauche%}}

**La beauté de la lecture est sûrement cette amitié** en l'absence de
l'autre et fondamentalement une écoute qui se peuple de [<span id="voixa">voix</span>](#voixb).

Peut-être s'agit-il comme [f.n.](/desidentite/#fn) d'être fier de sa petite oreille, de son
écoute. Ainsi se lit son "[<span id="etreb">otobiographie</span>](#etrea)" pour le dire avec [j.d.](/desidentite/#jd) :
"Ah qu'il me répugne d'*imposer* aux autres mes pensées ! Combien je
trouve de joie dans chacune des humeurs, chacun des secrets revirements
intimes où je rends justice aux pensées des *autres* au détriment des
miennes ! Mais de temps en temps il y a des fêtes encore plus hautes,
lorsqu'il est permis de faire don de sa demeure et de son avoir
spirituels ... Non seulement il refuse d'en tirer gloire : il voudrait
même échapper à la reconnaissance, car elle est importune et ne respecte
pas la solitude et le silence. Vivre sans nom, ou même exposé à des
railleries légères, trop humblement pour susciter l'envie ou
l'hostilité..." ([f.n.](/desidentite/#fn))

Se faire l'oreille pour l'autre, à l'écoute de son idiotie, de son
acuité, de son [<span id="detaila">détail</span>](#detailb), de son souffle qui ne dit peut-être rien.
Ami, ennemi : en deçà, au-delà, d'un **<span class="repere" data-repere="repere--potlatch">rapport</span> particulier** envers ces
instances étrangères. Amitié stellaire, disait-il ([f.n.](/desidentite/#fn)). On veut bien y
croire à cette posture éthique, difficile, généreuse de la critique :
indigence et accueil, amitié la plus dénudée, la plus vulnérable et
retirée du [<span id="noma">nom</span>](#nomb).

Car oui, la lecture est aussi une politique. La critique qui se retire
du jugement et de la valeur est une contestation, un refus, une décision
d'accueillir aussi parfois en silence la parole d'écriture.

"Il ne faut parler que lorsque l'on n'a pas droit de se taire."
[<span id="maximea">Maxime</span>](#maximeb) exigeante pour la critique autant que pour la littérature.

Parlant de cette littérature d'ébranlement, de joie et de terreur. Là se
trouvent "la mer gelée", le brouillard arctique, les têtes aux
cicatrices intérieures, les golems défaits, les blizzards furieux des
hurlements de l'époque, le craquement des banquises perdues.

{{%/colonnes-gauche%}}

{{%colonnes-droite%}}

Pour une critique **<span class="repere" data-repere="repere--potlatch">potlatch</span>**, potache.

{{%/colonnes-droite%}}

{{%/colonnes%}}

## 6 --- Critique cicatrice, critique créatrice

{{%colonnes%}}

{{%colonnes-gauche%}}

Parler en pure perte.

Sauver.

*Hic rosa, hic salta*.

Danser sur ces **<span class="repere" data-repere="repere--roses">roses</span>**, les pétales et les épines, le bonheur et le
malheur.

Autre chose que la vérité : inventer un rapport d'efficience de la
critique.

Une intelligence aussi. Une intelligence stratégique, une politique de
l'écriture, de la littérature, de la langue, une lutte affirmative au
sein même de nos textes, de notre axiologie critique disant cet effort
*sympathétique* dans un champ de la connaissance divisé en tant de
spécialités : relier ensemble des textes, former les attractions et des
constellations de connaissances, reconnaître de nouvelles figures, de
nouveaux visages, un nouveau bestiaire dans ces archipels galactiques de
textes, voilà bien notre affaire.

Entre la critique de recension et la critique savante, entre la critique
objectiviste et la critique affective, trouver la bonne distance
(dilemme du hérisson) -- peut-être est-ce cela dont on s'occupe en
critique : trouver des modes de relation, des distances, rendre visible
l'invisible. Être capable d'invention.

On pense.

On écrit pour **<span class="repere" data-repere="repere--penser">penser</span>**.

La critique fait autre chose que réfléchir son objet. Il le déforme et
le met dans des perspectives où il s'anamorphose et se métamorphose.

On voudrait croire à cette critique créatrice et transformatrice.

Critique transmédiatique à la manière <span class="repere" data-repere="repere--cb">de</span> **[c.b.](/desidentite/#cb)**. :

"Je crois sincèrement que la meilleure critique est celle qui est
amusante et poétique ; non pas celle-ci, froide et algébrique, qui, sous
prétexte de tout expliquer, n'a ni haine ni amour, et se dépouille
volontairement de toute espèce de tempérament ; mais -- un beau tableau
étant la nature réfléchie par un artiste -- celle qui sera ce tableau
réfléchi par un esprit intelligent et sensible. Ainsi le meilleur compte
rendu d'un tableau pourra être un sonnet ou une élégie." ("À quoi bon la
critique ?", *Salon de 1846*).

Trouver alors toujours des moyens de montrer ce que les œuvres
transforment, et nous avec. Dire de quoi elles nous rendent capables. De
quelles espèces elles nous rapprochent. Quels espaces elles créent où le
possible fait irruption dans l'impossible présent. Dans la stase
fulgurante de la lecture. Ici, parler des éclairs comme ce qui anime la
soupe primitive de notre cerveau. Ce qui arrange soudain une certaine
configuration de l'imaginaire. *De l'impossible, sinon on s'**<span class="repere" data-repere="repere--etouffe">étouffe</span>***.

{{%/colonnes-gauche%}}

{{%colonnes-droite%}}

"Je veux **<span class="repere" data-repere="repere--roses">semer</span>.** Il me faut d'abord me [<span id="terre">terre</span>](#regime)." ([h.c.](/desidentite/#hc))
L'amitié comme forme de silence. Ami celui qui entend dans nos silences,
nos disjonctions, dans les dédoublements de la parole, ce qui nous
*terrastres* ([p.c.2](/desidentite/#pc2)). Fleurs sans terreur, sans rhétorique. *Jacob Cow
le pirate*, la traduction en fiction des *Fleurs de Tarbes.*

*Stat rosa pristina nomine, nomina nuda tenemus*.

**<span class="repere" data-repere="repere--penser">Ardeur</span>** ; [<span id="maximeb">aphorisme</span>](#maximea). [<span id="rebond">Rebondir</span>](/a-te-souvenir-de-linsomnie-des-mondes/#rebond) dans la disjonction. Spontanéité et
hasard.

**[<span id="correspondances">Correspondance</span>](/desidentite/#correspondances)**, <span class="repere" data-repere="repere--cb">cénesthésie</span>. Voir le "[frou-frou des étoiles](https://www.antilivre.org/rimbaud.zap/rimbaud.zap.pdf)"
à l'aune des trous noirs. Le critique se fait aveugle par un
dérèglement fragmentaire des sens, une morphe du sens. Un autre support
pour de nouveaux liens, des associations nouvelles. Il nous faut des
œuvres nouvelles, d'autres images pour mirer nos mutantes conceptions.
Ici se fait la critique d'œuvres à venir, dont nous pressentons la
grammaire, rêvons l'altérité de support. Alors, comme une proposition,
on emprunte à l'ami ses constellations sémantiques : [<span id="astroblematique">astroblématique</span>](/ligne-dombre/#astroblematique) ou
la connaissance réticulaire de ce qui n'est pas advenu, reste à venir,
comme ce qui serait, à travers d'autres images, de la diagnose. Il nous
faudrait les archives (du vent) d'objets pas encore construits qui nous
permettraient de faire entendre le souvenir, le fantôme, la survie hors
du livre, de Radio Levania.

Expiration plutôt qu'inspiration : poétique <span class="repere" data-repere="repere--etouffe">du</span> **[<span id="souffle">souffle</span>](/a-te-souvenir-de-linsomnie-des-mondes/#souffle)**.

{{%/colonnes-droite%}}

{{%/colonnes%}}

## 7 --- Du déplacement critique, cosmique

{{%colonnes%}}

{{%colonnes-gauche%}}

**On voudrait inventer ici un écart cosmique**. Assez idiot *in fine*.
Une critique amicale par détournement de la métaphore. Peut-être
d'ailleurs que toute invention se limite à cela : un nouveau déplacement
de sens, une écoute nouvelle des associations sonores et sémantiques
ainsi entendues. Faire société dans le malentendu des métaphores qui
nous unissent. On ferait, une seconde, ainsi entendre l'instant. Le
contemporain n'est que l'altération par le rêve des changements
techniques. Pour approcher de cette solitude dont rien ne nous
départira, on disait, tout homme est une île. S'inventaient alors des
archipels ([e.g.](/desidentite/#eg)). [<span id="cosmovisionsa">Cosmovisions</span>](#cosmovisionsb) : solipsisme de l'univers,
**communisme des multivers**. Nous sommes alors, au secret de ce que
nous ne savons manifester, non tant multitude qu'ubiquité quantique,
possibilité difficilement représentable, hors du chat de Schrödinger,
d'être ici & de n'être pas. "l'avènement d'une autre cosmologie du
visible, et d'une approche insolite de l'invisible" ([l.r.](/desidentite/#lr)) . Dans quel
[<span id="hapaxb">hapax</span>](#hapaxa) revenants et revenances se présenteront-ils ? À quels
individus désidentifiés nous feront-ils rêvés ?

Ça serait toujours autre chose, à l'image du nom que nous portons en
souvenir d'une identité plurielle, la somme de ces lectures parfois
partagées, parfois seulement effleurées, souvent dessinant les
**<span class="repere" data-repere="repere--beances">béances</span>**, leurs hantises, d'un imaginaire collectif. Critique et
amitié se rejoignent dans l'emprunt faussaire de métaphores biaisées.

Ça serait des stases en instance : une traversée de l'intermédiaire.
Intermède et intermittence. Dans nos stances, une **désistānce**,
devinée, à la généralité. Des nuées et percées, en guise de cieux
nouveaux. Un [<span id="ventb">vent</span>](#venta) qui dissipe, momentanément, les temps mauvais.

{{%/colonnes-gauche%}}

{{%colonnes-droite%}}

"Appelle-moi par mon dernier nom. / Accroche mes vêtements **aux
planètes aux étoiles**." ([j.m.](/desidentite/#jm)) Appel toujours dernier, salut sans fin
adressé à l'ailleurs, voilà ce qu'est l'image métaphorique, la société
aux langues toujours étrangères et pleines de contresens ([a.v.&m.p.](/desidentite/#avmp)),
faisant avec [f.n.](/desidentite/#fn) fables et figures ("au détour de quelque coin de
l'univers inondé des feux d'innombrables systèmes solaires, il y eut un
jour une planète sur laquelle des animaux intelligents inventèrent la
connaissance") manière de déjouer la vérité du rapport social et de la
langue par quelque chose du **cosmunisme** alien de Blandine Volochot.

"Mais la métaphore --- le **renvoi à l'<span class="repere" data-repere="repere--beances">absence</span>** --- peut-être
considérée comme une excellence relevant d'un ordre tout différent de la
réceptivité pure." ([e.l.](/desidentite/#el)) Passer de la *métaphore* à *l'horizon du
monde*.

Dans ce **Vide en Soi** résonne alors cet appel à reconnaître cette
situation à front retourné : le Vide en Soi, le Vide hors de Terre.

"D'une façon inactuelle, c'est-à-dire *contre* le *temps*, et par là
même, sur le *temps, en faveur*, *je l'espère*, d'un *temps* à venir.
([f.n.](/desidentite/#fn))"

{{%/colonnes-droite%}}

{{%/colonnes%}}

## 8 --- Des clivages d'une critique par-delà la solitude

{{%colonnes%}}

{{%colonnes-gauche%}}

**L'amitié comme recommencement du clivage**. Dans l'amitié, un désir de
solitude. Il ne saurait s'agir de réactiver cette disjonction
anthropophage : "Dans la solitude, le solitaire se ronge le cœur, dans
la multitude, c'est la foule qui le lui ronge. Choisis donc !" ([f.n.](/desidentite/#fn))
L'aphorisme et sa mécanique contradictoire. [c.b.](/desidentite/#cb) fait pourtant de cette
aspiration à la solitude l'humeur biliaire de sa poétique. On ne
voudrait pas non plus renfiler le manteau de cette "cette solitude
essentielle" où [r.c.](/desidentite/#rc) se cachait dans ce cassant [<span id="clivageb">clivage</span>](#clivagea), mon corps
et moi, dont enfin, par bactériologie métaphorique, il faudrait se
départir. On rêve à une solitude sans individualité. On l'appellera
lecture. Activité solitaire, faite par des solitaires où l'on ne se
résume pas à ce que l'on perçoit, mais aux strates de distance entre
l'auteur et lui-même, entre ce qu'il a voulu écrire et ce que l'on veut
lire, entre l'idée et sa traduction verbale. L'amitié, **<span class="repere" data-repere="repere--ardeur">ardente</span>**
distance, est un bruit. Une perturbation linguistique, oserait-on une
*[différance](/desidentite/#jd)* ? On nous y parle une [<span id="languea">langue étrangère</span>](#langueb) ---
je connais plus de livres que de gens disait le très snob [m.p.](/desidentite/#mp) Reste ces
livres refuges où enfin sans cassure être [<span id="seulb">seul</span>](#seula). Dans la [<span id="nomb">dénomination</span>](#noma)
de leur auteur, c'est d'eux que l'on parle. Sans [<span id="voixb">Soi</span>](#voixa). En toutes
lettres puisse-t-on leur redonner leur titre d'absence.

{{%/colonnes-gauche%}}

{{%colonnes-droite%}}

Préserver l'enthousiasme, **l'<span class="repere" data-repere="repere--ardeur">ardeur</span>** d'une seconde, la joie de la
rencontre, l'exaltation de la retrouvaille. Tacite tangence comme avec
un ami : illumination, on croit avoir compris ce que l'on ne savait
vouloir dire, ce que l'on ne savait pouvoir vivre. Alors on s'invente
des compagnons de route, des auteurs que l'on retrouve avec impatience :
on comble les silences traversés sans eux, on confronte, poursuit et
amalgame deux identités fictives.

{{%/colonnes-droite%}}

{{%/colonnes%}}

## 9 --- Des mains tendues de Kali

{{%colonnes%}}

{{%colonnes-gauche%}}

**Hors de soi, vers l'autre** --- il y a, à travers cette reconnaissance
de l'altération et de l'altérité du texte, une sorte de générosité
terrible qui s'instaure contre la généralité de la lecture du sens
(contre la génialité tout autant : on a dit combien notre lecture se
revendique d'une certaine idiotie, d'un certain réel du texte tout
autant que de ses virtualités).

Hors de soi, vers l'autre, chronique désaffectée dans ce *milieu*
aveugle de la lecture. Milieu par lequel tout arrive, le sens et le
suspens. La stase de la lecture. Cette amitié serait ce *milieu* de la
lecture.

Générosité --- nous voulons dire impatience et intuition. Nous voudrions
toujours que cette lecture critique instaure cette forme de passion
suspendue, stase-seconde, appréhension amicale qui dit le bon comme le
mauvais, amitié sincère, contre les lectures d'amour et de haine. D'une
lecture qui partage tout mais ne délimite pas : une lecture ouverte,
jusqu'au silence.

Plutôt qu'une lecture de "**deuxième <span class="repere" data-repere="repere--main">main</span>**", cette lecture devrait
ainsi se faire multivers vivant, mort et renaissant, étreint dans les
mille bras de Kali.

Voilà plutôt mon image, ma terreur et mon bonheur. Les mille bras de
Kali. Orphisme du 3e œil de la critique démembrée.

On esquisse ainsi une autre approche de la critique comme jugement de
goût, par-delà bien et mal, extension et intensité : générosité et
généralité.

Alors la lecture devrait trouver ce point G de la lecture, le point de
générosité où le livre s'excède, déborde, se tait, se fait extase, se
singularise en se retirant pour instaurer une étrange neutralité en
commun --- sans commune mesure et sans partage --- neutralité qui
s'investit d'une relation formée d'autre chose que du désir. Non qu'il y
ait dissymétrie mais asymétrie : une absence rentrant en rapport avec
une absence.

{{%/colonnes-gauche%}}

{{%colonnes-droite%}}

Ou alors, une lecture **d'<span class="repere" data-repere="repere--main">occasion</span>**. Une chance pour les mauvaises
rencontres sur des chemins de traverse. C'est si idiot, souvent, ce qui
nous effleure. On résonne aussi par [<span id="dissonancea">dissonance</span>](#dissonanceb), par désaccord. Rien
de commun avec un livre, ses personnages, voire pire avec les doublures
envahissantes de son auteur et pourtant une sentence fait déclic, amitié :
derrière d'obscures formules mathématiques, on se souvient que nous
sommes multivers, que nous nous devons écoute et accueil ---
échappement.

Parce que Toi (lecteur & lectrice) tu l'auras compris, déchiré par les
mille mains de Kali, dans la traversée de nos [<span id="hermetisme">hermétismes</span>](/ligne-dombre/#hermetisme), ici on
dessine un intermonde (on peut aussi le nommer Critique pour sa
révolution cosmique), insomnieux, idiot qui serait refuge. Nous voulons
des cosmogonies au conditionnel de leur virtualité. Terrain neutre
d'être un *no man's land*, terre originatrice quand on comprend que, pas
plus que la fin, le commencement n'a eu lieu.

Peut-être, par amitié, on pourrait proposer une autre métaphore biaisée
de ce Neutre où par ébranlement, tangence, troublement, [l.r.](/desidentite/#lr)\|\|[m.v.](/desidentite/#mv) font
comme si, généreusement, utopiquement (lieu par définition qui n'est pas
car toujours à venir), ils se neutralisaient, ne savaient plus
exactement qui parle. [<span id="personneb">Personne</span>](#personnea), ruse homérique trop connue. Et
pourtant. On prend en charge cette contradiction : la désidentité en jeu
ici serait un écart aux moyens de production qui déterminent, certes, la
valeur et l'endroit d'où l'on s'exprime. Des mues, collectives,
désidérées. Le [<span id="neutreb">Neutre</span>](#neutrea) c'est [m.b.](/desidentite/#mb) dans *L'écriture du désastre* mais
c'est encore Orze arpenté par [p.c.2](/desidentite/#pc2) dans *L'énigmaire.* Le Neutre c'est
aussi Internet : celui qui y écrit n'est plus un *lumpenprolétaire*, il
s'affirme dans l'appropriation de ses moyens de production. Il importe
de préciser que si nous pouvons nous cacher derrière le confort du
Neutre, de la dépersonnalisation, c'est seulement sur ce support d'une
virtualité toute numérique, à partir d'un partage qui ne se revendique
d'aucune [<span id="proprieteb">propriété intellectuelle.</span>](#proprietea)

{{%/colonnes-droite%}}

{{%/colonnes%}}

## 10 --- De l'écoute éclair des signes

{{%colonnes%}}

{{%colonnes-gauche%}}

**De l'amitié, de la lecture, de l'ardeur, de la métamorphose** ---
comme s'il fallait réinventer sans cesse cette révolution où le monde
change de signe, de référentiel, non pas simple retournement de l'amour
en amitié, de l'écriture en lecture, de l'apathie en ardeur, du
progressisme en métamorphose (mais à tout prendre dans sa simplicité
cardinale, c'est cette nouvelle boussole qu'il nous faut adopter),
pensant plutôt les nuances nuageuses et numineuses de ce réagencement
psychochimique des affects et de notre sensibilité en crise (tant
esthétique et politique, [<span id="coinciderb">décoïncidence</span>](#coinciderc) terrible).

D'une conception absolue de la littérature qui remonte peut-être (pour
nous en Europe faut-il préciser), à ce groupe de jeunes esprits
rassemblés à Iéna à la fin du 18e siècle. "Ils n'ont produit que des
œuvres critiques" ([j-l.n.&p.l-l.](/desidentite/#jlnpll)). Critique, c'est-à-dire théorique.
Vision et ciel, mouvement et cosmos. Mais on a perdu ce ciel comme on a
perdu de vue la nuit et les étoiles : vieilles lunes et vieux démons
morts dans la lumière des néons.

Ce qui s'est dit en 1798 (*Athenaeum*), c'est ce qui s'est redit en 1968
(*Comité étudiants-écrivains*), en 1999 (*Tiqqun*), ce qui peut se
répéter et se transformer une nouvelle fois, sous d'autres
configurations (d'autres cieux aussi, désidérés) ce sont ces œuvres sans
œuvre, sans auteur, de cette parole qui vient.

La critique comme reprise et comme déprise.

<span class="repere" data-repere="repere--reprise">Reprise</span> et réparation, reprise **et** transformation.

Déprise du sujet, déprise du temps, intempestif présent.

Notre besoin de révolution n'est plus romantique --- il est
cosmologique. Il est aussi critique. Critique à tous points de vue.

Du point de vue de l'urgence mais aussi de la pensée. En même temps. Et
de la pensée dans la lecture.

La lecture et la critique, opératiques oniriques du changement.

D'un changement de regard, d'une poétique du regard, de l'attention ---
car c'est sûrement cela aussi la critique : une attention.

Une cosmologie de l'attention.

Le romantisme triomphant comme sympoétique, comme passion de la Terreur
dans les Lettres (*Les Fleurs de Tarbes*), cette conception absolue de
la littérature et de la communauté d'exception : c'est cela qui doit se
transformer pour trouver de nouveaux modes d'intelligibilité pour notre
temps de désastre, des modes d'intuition et de divination (sans dieu,
juste des oiseaux qui se perdent dans le ciel). En critique aussi, faire
autre chose que des essais érudits ("l'érudition est le contraire de la
lecture" disait [r.b.](/desidentite/#rb)) et que des *dramas* de l'intellect en crise ---
des opérations de métamorphose, des créations de pensées et de nouvelles
valeurs.

En cela la lecture est une "révolution de l'amitié". Anagnostes,
esclaves de la lenteur du lire, "depuis des siècles nous nous
intéressons démesurément à l'auteur et pas du tout au lecteur" ([r.b.](/desidentite/#rb)).
Nous voulons cette lenteur et cette urgence, ces stases-secondes, ces
univers multipliés qui se donnent dans la lecture comme une **<span class="repere" data-repere="repere--exp">expérience</span>
autre** qu'esthétique. Expérience-limite aussi. Grand Jeu. Grande Ourse.
Nous ne voulons pas lire, nous voulons être lus, l'œil renversé, au-delà
de toute lecture projective.

*Zeugen. Zeugma. Zeignis*.

Forme de signes en éclairs.

Ne pas faire des images --- mystère de nommer la parole dans son
ébranlement.

La parole nous vient toujours d'ailleurs.

Critique-**extra-terrestre**.

Parlant par voix.

À travers un cosmos de texte.

Fulgurant de tous les côtés du temps.

Critiquer c'est ouvrir le texte à l'avenir.

À d'autres ciels encore, toujours.

À des battements de cils sous les paupières de notre imagination.

Car on ne fait pas de la critique sans une idée de la littérature.

Sans une théorie. Une critique.

Autre chose qu'un champ de bataille intellectuel. Don de prophétie qui
ignore le reste du monde parce qu'il s'adresse à un *à venir* qui ne
le/la concerne plus. "Rien n'est plus poétique que le souvenir et
l'anticipation ou la représentation de l'avenir", disait n.

Chaque critique doit inventer la littérature.

Doit se faire un ciel, des soleils, des orages.

Chaque texte doit réinventer la littérature ou n'est que bavardage
(quoique de cette parole errante, de rumeur excessive il faille aussi
parler, dans ses contours collants à **l'<span class="repere" data-repere="repere--epoque">époque</span>**, à ses sudations
sauvages, à ses odeurs moribondes).

Critiquer c'est ouvrir le texte à l'avenir.

Parler de ce mouvement qui se déporte en avant de soi. Parler de cette
fuite perpétuelle du texte ne cessant de se métamorphoser *en des corps
nouveaux*. Alors critiquer est la suite de ces métamorphoses que l'on
espère retirées de toute violence, poétique de l'étoile nous venant non
du passé immémorial, mais des distances instantanées dans l'intuition
d'un dire à venir.

*Zeugen. Zeugma. Zeignis.*

Forme de signes en éclairs.

{{%/colonnes-gauche%}}

{{%colonnes-droite%}}

De l'<span class="repere" data-repere="repere--reprise">écoute</span> **à** l'entente : l'attention de l'amitié, de la lecture.

Parole critique : relever les contradictions non comme des défaillances,
mais comme des promesses de bifurcations. La critique finit par
l'outre-littérature, par parler de ce qui un peu plus pourrait la
regarder : le vécu. Peut-être partageons-nous en lisant une
**<span class="repere" data-repere="repere--exp">expérience</span>** d'une simplicité vertigineuse. Même sans connaître le
latin, viens t'asseoir à côté de toi, écoute les contradictions, les
tangences, les dédoublements, sois aussi pluriel que si tu n'existais
pas. En dépit de nos grands mots, on finit à la seconde où tout commence :
on fixe des vertiges, similaires sans être singuliers. Et toi, ça
change quoi chez celui que tu n'es pas tout à fait ?

Disjonction critique. Par substitution, simultanément (tant c'est --- au
moins --- le dédoublement de personnalité qui ici se cherche des
stases-secondes, immédiates comme une intenable proposition), nous
sommes aussi infiniment **terre à terre**. Critiques d'[<span id="regime">Ancien
Régime</span>](/desidentite/#regime). Concrets parfois même concrets comme le vécu auquel on
n'échappe pas. Idiots, incertains, glissants, [<span id="spiralaires">spiralaires</span>](/ligne-dombre/#spiralaires) tant toute innovation est retour. Lire c'est
se demander, sans fin, quel ami serait l'auteur pour Toi. Des masques
peut-être où pourtant se projettent tant d'aspirations. Un visage
([e.l.](/desidentite/#el)), la possibilité d'un silence, d'une amitié, d'une écoute de
l'absent que nous sommes. Alors dans cette rencontre manquée, chargée
comme l'amitié des fantômes de ce qui aurait pu avoir lieu sous son nom,
on se fait des idées. Dans ces disjonctions, l'amitié dit l'idée de
littérature par la conception de son incontinuité ([p.c.2](/desidentite/#pc2))

Échapper alors à la déploration. Idiots, on veut une amitié **au
<span class="repere" data-repere="repere--epoque">présent</span>**. Une façon, comme l'amour qui sait, de vivre dans l'écart.
Désormais, s'écarter de l'importune nostalgie pour une époque où le
critique était engagé et pourquoi pas efficace tant qu'on y est. Une
critique radicalement contestataire serait celle qui fasse amitié avec
cette sentence, avec ses échappées, de [l.t.](/desidentite/#lt) : "le pouvoir est vacant et
chacun veillera, désormais, à ce qu'il le reste." Par contamination,
nous espérons transmettre cette vacance du pouvoir qui contraint à
déconstruire, à désidentifier. Te voilà bombardé critique, refais le
chemin. Si tu veux, on t'accompagne. Sinon, la solitude, belle illusion,
c'est bien aussi.

{{%/colonnes-droite%}}

{{%/colonnes%}}

## 11 --- S'il ne fallait se taire

{{%colonnes%}}

{{%colonnes-gauche%}}

**Si encore il faut écrire** à quoi ressemble cette critique en guise
d'amitié, il nous faut parler autrement de *communauté* et de
*révolution*, lourdes des charniers du 20e siècle. Inventer à chaque
fois l'essai littéraire en se demandant de quoi il est capable : de
quelles transformations la critique et la fiction sont-elles capables,
de quels nouveaux agencements à partir des mêmes composants composant le
réel ?

Le 21e siècle appelle à redonner des images et des désirs pour ces
transformations. Adelphité de la critique. Peut-être dans ce neutre qui
la soustrait au couplage amitié/amour pour accomplir une neutralisation
où il faut repenser ce lien de désœuvrement à l'œuvre dans la critique.
Prenant aussi en considération ce que la critique *met en valeur*
implicitement : part des autrices, présence ou absence des petites
maisons d'édition, système et **<span class="repere" data-repere="repere--champ">champ sociologique</span>** de la [<span id="proprietea">littérature</span>](#proprieteb)
, mais en faveur de l'horizon d'une pensée du *neutre.* Le neutre
de [r.b.](/desidentite/#rb) comme ce qui conteste l'idéologie et revendique les paradoxes.
Force faible de la critique pour changer nos représentations.

Lisant et critiquant, faisant notre Sabbat et notre Camarilla en guise
de proximité distante, façon de détourner le vocabulaire et
l'imaginaire, de former dans la nuit une association coupable de
vampires solitaires ([p.q.](/desidentite/#pq)) mais symbiotiques, bactéries passant le code
phagocyté des citations dans un nouvel organisme. "Communication par
contamination" "on ne veut plus d'histoire unilatérale. On veut
alternatives et changements, variations et coïncidences. *Des motifs,
des [<span id="couleursa">couleurs</span>](#couleursb), des glissements et des substitutions*" ([m.v.](/desidentite/#mv)) : en
un mot, *onirocritique*, glissement et substitution, *Théorie des
MultiRêves* ([j-p.c.](/desidentite/#jpc)).


Critique et *kinship* ([d.h.](/desidentite/#dh)) pour être autre chose qu'une parentèle avec
les autrices et les auteurs, mais de ce qui est sans commun, car
profondément dissymétrique et voulant cet *alter non ego*, irréductible.
Quelque chose de la sympathie universelle, sans retour. **<span class="repere" data-repere="repere--lien">Des liens</span>
invisibles, dans l'invisible.**

つづく...

{{%/colonnes-gauche%}}

{{%colonnes-droite%}}

Faire attention, façon de ne pas se limiter. Au risque de se répéter, la
critique est l'écoute de nos contradictions. Expression de tout ce à
quoi, sans y échapper, nous ne saurons renoncer. La critique ? prêter
attention à cette certitude: un autre que moi, trop coincé dans mes
**<span class="repere" data-repere="repere--champ">déterminismes sociaux</span>**, saura dire la part qui échappe, se soustrait
pour ne pas souscrire. "L'identité ne se résume pas à ce que nous
sommes, comment nous vivons, en quoi nous croyons. Mieux qu'une
nationalité, une profession, une opinion, nous habite une horde mouvante
venue d'une incontinuité des profondeurs." ([p.c.2](/desidentite/#pc2)) Par peur, prudences
et silences, c'est à cette horde que nous prêtons amicale attention.

D'autres **<span class="repere" data-repere="repere--lien">liens</span>** et correspondances : de <button class="button--link button--hypertexte" id="hypertexte">l'hypertexte</button>. Une
autre lecture, toujours, est possible.

{{%/colonnes-droite%}}

{{%/colonnes%}}

[^1]: Ce qui se pense ici reste une autre forme, ardente, amicale, d'essai dans son engagement le plus absolu : une redéfinition de celui qui s'y écrit. Essai comme une enquête, dire les frontières pour accueillir tous ceux qui les outrepassent, y disparaissent aussi. Les spectres comme autre régime du réel, sans abstraction. Hommage à [m.c.](/desidentite/#mc) qui donne tout son sens à l'hospitalité sans condition qui, seule, fait signe vers le réel.
