---
persona:
  - Georg Simmel
title: "Sur quelques relations de la pensée théorique avec les intérêts pratiques"
slug: "sur-quelques-relations"
date: 2021-04-01
echo:
  - ontique
  - anthropique
images:
  - "img/210401-simmel-sur-quelques-relations.jpg"
gridimages:
  - "img/210401-simmel-sur-quelques-relations-grid.jpg"
sourceimage: "https://commons.wikimedia.org/wiki/File:Pieter_Brueghel_the_Elder_-_The_Dutch_Proverbs_-_Google_Art_Project.jpg"
notes: "Ce texte de Georg Simmel, traduit par Élie Halévy, a paru en 1896 dans le tome 4 de la *Revue de métaphysique et de morale*."
summary: "J’ai récemment essayé de déduire la connaissance théorique de l’action pratique, et le concept de \"vérité\" du concept d’utilité évolutionniste. Je discutais la conception très répandue selon laquelle la vérité nous est en quelque sorte imposée, parce que l’erreur aurait engendré en nous un mode d’action funeste et destructif pour le sujet agissant ; et j’arrivais à la conclusion que cette conception, quoique partiellement fondée, demeurait néanmoins tout à fait impuissante à rendre compte du concept de vérité."
citation: "l’utile c’est le vrai"
---

J’ai récemment essayé de déduire la connaissance théorique de
l’action pratique, et le concept de "vérité" du concept d’utilité évolutionniste[^1].
Je discutais la conception très répandue selon laquelle
la vérité nous est en quelque sorte imposée, parce que l’erreur
aurait engendré en nous un mode d’action funeste et destructif pour
le sujet agissant ; et j’arrivais à la conclusion que cette conception,
quoique partiellement fondée, demeurait néanmoins tout à fait impuissante
à rendre compte du concept de vérité. II paraissait plus
vraisemblable d’admettre qu’il n’existe absolument pas de vérité
objective, qu’il nous serait, dans l’hypothèse, avantageux de nous
approprier : seulement, parmi les représentations innombrables
dont l’origine est purement psychologique, quelques-unes, par les
mouvements qu’elles mettent en branle et par la réaction du monde
extérieur sur elles, deviennent causes de relations pratiques favorables
au progrès de la vie. Nous *appelons* ces représentations "vraies".
La vérité n’est donc pas une qualité objective des représentations,
mais un simple nom pour désigner celles qui, dans le mécanisme
psycho-physiologique, se sont manifestées comme occasions d’actions
utiles et se sont par là fixées dans l’espèce. Je me propose ici de
compléter et de développer ces remarques par quelques éclaircissements
nouveaux, et de rechercher, conformément à la même
méthode, en quel point et de quelle manière notre connaissance
prend racine dans notre pouvoir de vouloir et d’agir.

On a objecté à cette théorie qu’elle rabaisse au niveau des besoins
pratiques, et, par suite, avilit et déclasse l’autonomie de notre vie
spirituelle, c’est-à-dire notre idéal le plus sublime. C’est le même
sentiment qui répugnait aussi à la théorie de la descendance, sous
prétexte que cette théorie ignorait le caractère relevé de l’existence
spirituelle de l’homme, lorsqu’elle faisait de celui-ci le rejeton d’une
espèce animale inférieure. Mais c’est, il me semble, une des conquêtes
importantes du sens historique, un des plus grands progrès
de l’esprit humain, d’avoir compris que l’on peut rattacher les biens
les plus hauts de notre nature, par un développement historique,
aux débuts les plus humbles, sans compromettre par là le moins du
monde leur valeur intrinsèque et effective. En réalité le principe de
l’évolution, avec le nombre infini de périodes de développement
qu’il implique, avec la profusion de circonstances que chaque période
suppose, avec la lente venue au jour des forces latentes, place les
derniers degrés atteints à une hauteur qui ne peut être bien appréciée
que si précisément on les range sur la même échelle que les
plus humbles phénomènes de la vie. Je prétends d’ailleurs que toute
valeur, comme aussi bien toute créature humaine, ne doit sa signification
effective qu’à son essence propre et à ses œuvres : une descendance
plus relevée n’est pas un titre à la possession d’un tel
droit, une origine trop humble n’est pas un motif pour la révocation
de ce droit.

Un autre malentendu doit être évité, au sujet de la théorie en
question. Quand le phénomène de sélection dont nous parlions plus
haut a définitivement fixé les représentations pratiquement utiles
et postérieurement désignées comme "vraies", lorsque, le temps
des indécisions et des erreurs sans nombre étant enfin passé, il a
transmis, de génération en génération, comme un trésor assuré, les
plus universelles et les plus importantes de ces représentations,
alors il devient possible pour l’individu de se placer au point de vue
inverse. Il lui paraît dès lors non plus que le vrai c’est l’utile, mais
au contraire que l’utile c’est le vrai. Mais ce phénomène doit d’autant
moins, quant à l’origine du concept de vérité, nous induire en
erreur, que l’on reconnaît, dans de pareils retournements de concepts,
une forme très caractéristique des relations qui règnent
entre l’individu et l’ensemble. Si par exemple l’adaptation a fait en
sorte que les aliments nutritifs et utiles à la vie deviennent causes
aussi de sensations agréables et soient recherchés comme tels, c’est
une conception qui peut se produire, et, se produisant, être, en
général, vérifiée par les faits que celle-ci : cela convient à chacun,
qui est agréable au goût de chacun. De même, l’utilité matérielle
de telle répartition définie de possessions et de telles garanties définies
a commencé par fixer certaines formes juridiques instituées en
vue de ces fins ; mais plus tard, inversement, c’est la conservation
de ces formes juridiques elles-mêmes qui devient désormais la
condition de toute acquisition et de toute possession. Lorsque le
mariage monogame eut été introduit, ce fut évidemment par lui
d’abord que se développa dans notre espèce cette forme de l’amour
qui individualise son objet et n’est plus purement sensuelle ; inversement
c’est cet amour qui maintenant est pour l’individu une raison
de conclure un mariage. Bref, après que l’espèce a constitué des liaisons
définies entre des états, des représentations, des actions, l’individu
n’est plus astreint à suivre l’ordre dans lequel se sont succédé
les relations à l’origine ; ce qui était cause pour l’espèce peut assez
souvent se présenter à l’individu comme effet, et réciproquement.
Et peut-être ce phénomène se rattache-t-il, en dernière analyse, à
une relation très générale entre l’individu et l’ensemble. Les phénomènes
qui servent de contenu à la vie de l’espèce n’ont évidemment
de fondement et d’origine que dans les individus : cristallisation des
phénomènes qui sont le contenu de chaque vie individuelle, ils sont
néanmoins cristallisés de telle sorte que l’on ne peut plus distinguer
l’apport de l’individu, et qu’ils se posent maintenant, en face
de l’individu, comme une masse de formations objectives, sources
pour lui des déterminations de son existence. À mesure que ces
apports des individus s’écoulent dans le grand vase de la vie de l’espèce,
et se fixent sous forme durable, ils perdent aussi l’ordre dans
lequel ils se succédaient à l’origine. La direction du développement
qui les liait au début devient indifférente, parce que le cas particulier,
dans lequel le développement s’est réalisé, est oublié, absorbé
dans l’universalité. La liaison prend aussi une valeur indépendante
de la forme de son origine, la valeur pure et simple d’un fait : d’où
la possibilité pour cette liaison, lorsqu’elle se transmet à d’autres
individus, de leur parvenir dans un ordre de succession temporelle
ou causale tout à fait altéré.

Même après que le concept de vérité a déjà acquis des conditions
d’application pleinement solides et une valeur indépendante, les
complications infinies de la pratique ne laissent pas d’imposer
encore à la fin pratique de la vérité des exigences trop diverses pour
que la prétendue unité de signification de ce concept n’en devienne
pas très douteuse. J’emprunte un exemple à la vie familière. Les certificats
que délivrent les maîtresses à leurs domestiques en leur donnant
congé sont, au moins dans l’Allemagne du Nord, presque universellement
contraires à la vérité. Est-ce un effet de cet optimisme,
qui nous fait perdre le souvenir des fautes commises, dès l’instant
où nous avons cessé d’en souffrir ? ou, comme on dit, pour ne pas
compromettre l’avenir des femmes de chambre ? ou encore pour
éviter des scènes, et faute d’un sentiment assez prononcé de notre
responsabilité vis-à-vis de successeurs inconnus ? Le fait est que
presque toutes les femmes écrivent pour leurs domestiques des certificats
mensongèrement flatteurs, soit qu’elles présentent des affirmations
fausses, soit tout au moins qu’elles dissimulent des faits
vrais. Or si une femme voulait constater par écrit, dans le certificat,
avec une scrupuleuse vérité, toutes les imperfections d’une femme
de chambre d’ailleurs satisfaisante, elle n’atteindrait pas son but,
au cas où elle croirait par là donner au nouveau maître un portrait
ressemblant du domestique. Car on est habitué à ce que des domestiques
tout à fait ordinaires aient de bons certificats et à ce que les
plus mauvais seuls reçoivent des blâmes sur le certificat : en conséquence,
le certificat en question donnerait à coup sûr l’impression
d’une incapacité de beaucoup supérieure à la réalité. On pourrait,
étant donnée la façon dont les usages sont établis, ne pas hésiter à
dire qu’un tel certificat est directement contraire à la vérité : car
évidemment cette expression-là seule est vraie qui produit chez
l’auditeur une représentation en accord avec la vérité. II y a donc en
ce cas deux vérités : l’une qui est en harmonie avec les représentations
universelles, celles du cercle le plus étendu, et, à laquelle ce
serait se conformer que d’attribuer au domestique les qualités *a*, *b*, *c* ; l’autre qui n’a de valeur que pour un cercle limité, et selon laquelle
on doit lui attribuer les qualités *d*, *e*, *f*, qualités qu’il ne possède
pas du tout, mais dont l’énonciation produit sur ce cercle restreint le
même effet que si, en d’autres circonstances générales, les qualités
*a*, *b*, *c*, étaient affirmées de lui. Ainsi l’obtention d’effets pratiques
désirés est ce qui donne ici au mensonge la valeur de la vérité :
preuve nouvelle que c’est là qu’il faut chercher le critérium proprement
dit de la vérité. Si cependant nous persistons à appeler erronée
la seconde façon de s’exprimer, c’est que nous érigeons une différence
de degré en différence absolue : en réalité la différence est seulement
entre les besoins pratiques du cercle le plus étroit et ceux du
cercle le plus vaste. C’est celui-ci qui donne à ses conceptions théoriques,
issues de ses besoins pratiques, le sceau de l’"objectivité".
Mais un examen plus minutieux révèle des sphères particulières
d’intérêt qui n’en sont pas encore venues à s’accorder avec la
sphère la plus vaste et frappent, par suite, chacune pour soi, sa
"vérité" particulière. Phénomène analogue à celui qui se produirait
si un cercle fermé d’hommes intéressés dans une affaire commune
convenait de frapper, pour servir au commerce réciproque des
membres de l’association, une monnaie qui, à l’intérieur du cercle,
serait unité de mesure et instrument d’échange à valeur pleine,
mais qui, dès qu’elle se répandrait dans le cercle officiel environnant,
serait fausse monnaie : et cependant la "vraie" monnaie de
ce dernier cercle ne devrait pas sa légitimité et sa valeur à une cause
autre que d’avoir été frappée et reconnue de plein cours dans les
limites d’un cercle social. Toute la différence entre la "vraie" et la
fausse monnaie consiste non dans une différence interne
des monnaies elles-mêmes --- car la monnaie n’est dans les deux cas qu’un
*signe* de valeur, sur lequel un groupe social s’est mis d’accord, --- mais
exclusivement en ceci que le consentement s’est produit dans
le groupe le plus large ou dans un groupe plus étroit. Si telle est
l’origine sociale de la vérité, on comprend pourquoi la vérité, lorsqu’elle
se réalise dans le caractère des individus comme *véracité*,
est le lien qui fait l’unité du cercle social ; à l’intérieur du cercle, et
surtout en ce qui concerne les objets d’intérêt public, c’est une obligation
stricte, tandis que par rapport aux individus situés en dehors
du cercle, c’est une obligation sur laquelle nul n’insiste, et très indifférente.
Saint Paul fonde sa recommandation aux Éphésiens de dire
seulement la vérité sur ce motif, expressément énoncé : "parce qu’ils
sont membres d’une communauté".

Si l’hypothèse selon laquelle l’origine de la vérité c’est son utilité
pour l’espèce est une hypothèse légitime, alors on tient l’explication
de toute une série de phénomènes d’ordre théorique ou pratique. Entre
les règles que l’expérience a démontré être appropriées à l’espèce, la
logique formelle énonce celles qui sont les plus simples, celles aussi
d’après lesquelles la pensée se dirige, en fait, dans le nombre de cas
de beaucoup le plus considérable ; en d’autres termes, les principes
logiques donnent une expression abstraite, fixée en formules, aux
forces réelles, inconscientes en soi, qui sont le fond de notre pensée.
Ils sont les principes intellectuels les mieux faits pour l’espèce, et
possèdent ainsi la marque de ce qui est purement social par opposition
à ce qui est individuel. En effet ce qui convient au tout comme
tel, ce au sujet de quoi l’ensemble du cercle social est d’accord, ne
peut jamais être qu’un élément intellectuel relativement inférieur,
incolore, schématique : ce qui appartient à tous doit être accessible
à celui de tous qui est le plus bas placé. De même que l’on a défini
comme un "minimum moral" ces règles dont le tout social commande
aux individus le respect, à savoir les règles de droit, de
même on peut caractériser les lois théoriques, que la totalité de l’espèce
dicte à la pensée de l’individu --- si encore le tout reconnaît la
pensée individuelle comme possédant une vérité, --- comme constituant
un "minimum théorique" : analogie dans laquelle, pour le dire
en passant, la relation du droit avec la logique trouve son explication
profonde. C’est pourquoi la pure logique ne saurait nous acquérir un
contenu déterminé de la connaissance, une satisfaction réelle du
savoir : la logique achète son infaillibilité et son universalité par le
vide et la pauvreté de son contenu. Par là elle tient de la nature de
ces instincts pratiques qui, fixés par l’expérience de l’espèce, sont
donnés à chaque individu qui vient au monde, comme le cadeau du
parrain au nouveau-né : de tels instincts, ceux qui se rattachent,
par exemple, à la faim et au penchant sexuel, possèdent, par le fait
de leur origine sociale, beaucoup de sûreté et de fixité ; ils ne constituent
cependant que les assises les plus profondes de notre nature,
sur lequel s’élève ensuite l’édifice plus délicat du caractère individuel.
De même que, dans ces formes inférieures de la perception et
de l’action, est le point de départ commun de toutes les tendances
qui d’ailleurs, dans le développement des vies individuelles, divergent
largement les unes des autres, de même la logique représente
la possession commune sur laquelle tous se sont mis d’accord, quand
bien même, sur les autres domaines de la connaissance, ils s’écartent
autant qu’il est possible les uns des autres. Elle est ainsi le critérium
absolument objectif, puisque "objectif" signifie seulement
"valable pour tous" ; quant aux données particulières du savoir, elles
appartiennent, au contraire, à l’espèce des connaissances subjectives
et discutables, dont on appelle assez souvent au tribunal de
la logique, tout comme les intérêts particuliers trouvent la règle qui
les contrôle dans l’intérêt de tous, c’est-à-dire dans le droit. Servant,
dans le même instant, aux usages les plus divers, la logique mérite
à la fois le jugement désapprobatif porté sur elle par Kant d’une
part, et, d’autre part, l’adoration mystique de Hegel. --- Enfin si la
connaissance est, dans la totalité de son essence et de ses formes, le
produit des exigences de la pratique et de l’utilité, la logique n’est
alors que la plus condensée, la plus universelle, la plus fondamentale
de ces formes ; et ainsi apparaît la possibilité d’atténuer la différence
qui sépare les deux principes de la morale, le principe rationaliste
et le principe utilitaire. Nous pouvons, dans la direction de
notre vie pratique, nous abandonner à la pensée logique et rationnelle,
car celle-ci n’est que le résultat et le produit des moyens
intellectuels employés en vue de fins pratiques : par où l’on voit
pourquoi la morale empirique et la morale rationnelle finissent
généralement par tomber sur les mêmes prescriptions particulières.

L’observation qui précède n’est vraie cependant que de ces règles
de vie tout à fait générales dont l’établissement suffit à la morale
vulgaire. On comprend d’autre part, puisque la logique est issue de
l’action de l’espèce, qu’elle soit impuissante, aux points les plus
élevés et dans les détails les plus complexes des phénomènes de la
volonté et de la sensibilité, et que certains penchants pratiques et
affectifs soient trop forts pour s’incliner devant une conviction logique
quelconque. Ici se manifeste l’opposition de l’individu au tout social,
opposition qui, s’expliquant par la force de l’individu aussi bien que
par la complexité de sa nature, nous fournit encore l’exemple d’un
développement social très caractéristique. Les forces, les tendances,
les propriétés que le tout a formées et communiquées à l’individu,
sont fréquemment transformées par celui-ci en produits dont la
direction est exactement opposée à la direction des éléments primitifs :
l’individu résiste au groupe avec les armes mêmes que le
groupe lui avait confiées. Ici, comme dans le cas cité plus haut, les
faits sociaux perdent, d’une part, par là même qu’ils sont devenus
propriété sociale, certaines formes et certaines directions de leurs
parties constitutives, et, par ce fait même, l’individu, d’autre part,
gagne la faculté de les employer à ses fins dans des circonstances
nouvelles et opposées. C’est sous deux formes que l’opposition de
l’individu au groupe se réfléchit dans le divorce qui a lieu, à l’intérieur
même de l’individu, entre ses tendances logiques et ses tendances
hostiles à la logique. Le cas le plus immédiat et le plus simple
est celui où domine en nous une façon de sentir ou d’agir logiquement
inconciliable avec la multiplicité de nos autres fins et de nos
autres convictions. Si par exemple une passion s’est emparée de
nous --- passion amoureuse ou religieuse, vocation irrésistible ou
folie du jeu, --- nous avons assez souvent la conscience claire que,
si nous dirigeons nos forces en ce sens, nous contredisons les autres
dispositions, morales et égoïstes, de notre tempérament et de notre
intelligence. Et en vérité nous ne voulons en aucune façon ni
renoncer à ces dernières, ni les condamner comme erronées, pour
ériger la nouvelle tendance en critérium suprême du vrai et du faux,
du bien et du mal ; mais, tout en les tenant pour parfaitement
bonnes, tout en reconnaissant pleinement qu’elles sont justes, et
que les raisonnements en forme sont justes aussi, qui, partis de ces
données, devraient conclure à la condamnation de la passion maîtresse,
nous ne rompons cependant pas avec celle-ci, tout au contraire
nous l’émancipons, elle et son droit, de l’autorité de la vérité
qui reste d’ailleurs inébranlée. Mais, en second lieu, de pareilles
passions, à peine apparues, ne restent pas toujours sur le domaine
de la sensation et de l’action, où elles sont nées. Il arrive bien
plutôt qu’elles se changent en théories, prennent la forme de représentations
intellectuelles et deviennent une nouvelle vérité théorique
qui, prétendant à la possession du même domaine que les vérités
établies, entre avec celles-ci en conflit immédiat. Il s’agit, en pareils
cas, sans ambages, d’une extirpation de l’ancienne vérité, vérité
logiquement et socialement fixée, née d’ailleurs elle-même d’un
besoin pratique, comme le prouve le fait que maintenant ce sont
des besoins pratiques nouveaux qui vont développer et bientôt
achever sa transformation. Si c’est une réforme profonde ou même
une révolution de la conception courante des choses qui est en
question, alors le problème sociologique à résoudre est évidemment
celui-ci : afin de ne pas dépenser sans profit les forces sociales,
employer autant que possible, pour fonder l’édifice des connaissances
nouvelles, les connaissances antérieurement acquises. Il
faudra donc, de l’édifice de la vérité établie, abattre seulement ce
qu’il est absolument nécessaire d’abattre. Et puisque maintenant le
fondement le mieux assis et le plus profond de cette vérité consiste
dans les propositions formelles de la logique, puisque les connaissances
sont d’autant plus sujettes à évoluer, la négation des connaissances
d’autant moins dangereuse pour l’équilibre de l’édifice
qu’elles concernent des hypothèses plus expérimentales, s’appliquant
plus directement aux objets, au lieu d’hypothèses abstraites
et logiques : pour toutes ces raisons les réformes apportées à la
connaissance humaine suivront la méthode, en quelque sorte, la
plus économique quand elles supposeront seulement le changement
des connaissances positives et empiriques plutôt qu’elles n’exigeront
une altération ou une négation des régies de la pure logique.

À cette théorie s’oppose un fait historique de la plus haute gravité.
II semble que le christianisme, à son apparition, ait voulu se mettre,
tout à l’inverse, pour manifester son essence, en contradiction
logique, absolue avec toutes les connaissances antérieurement
acquises et c’est ce dont saint Paul, dans la première épître aux
Corinthiens, a la conscience la plus claire. "Aimez vos ennemis" ---
mais le concept de l’ennemi c’est le concept d’un homme que l’on
hait. "Cherchez la douleur" --- mais la douleur est cependant,
par définition, ce que l’on évite. --- "Seigneur, que ta volonté soit
faite, non la mienne !" --- pourtant ma volonté est cela même dont
je veux qu’elle soit faite, et c’est une contradiction logique de vouloir
que cela soit fait que je ne veux pas qui soit fait. --- "Heureux les
pauvres d’esprit !" --- mais l’affirmation d’une liaison du savoir
avec la constitution d’une humanité idéale était une proposition
absolument analytique pour Socrate et, pour les penseurs qui vinrent
ensuite, relativement analytique encore. "Heureux les malheureux" ---
mais c’est la définition même du malheur qu’il exclut la
félicité. --- Qu’il y ait un ordre idéal des choses qui soit exactement
le monde renversé, où les premiers soient les derniers et les derniers
les premiers, que celui à qui a été donnée la puissance sur le
ciel et sur la terre ait dû errer dans la misère et dans l’humilité ---
tout cela devait paraître un contresens direct, une *contradictio
in adjecto*, et explique les risées et les huées que l’apparition du
christianisme excita chez les esprits cultivés. En ce cas, assurément,
la nécessité pratique et morale qui commandait que la conception
antique de l’univers fût renversée paraissait attaquer les
couches les plus profondes de la pensée, les postulats logiques eux-mêmes.
Mais il est, d’autre part, manifeste que les conditions faisaient
ici défaut pour l’application du principe, plus haut énoncé,
d’économie des forces intellectuelles et sociales. En premier lieu, il
fallait précisément ébranler d’une façon aussi radicale que possible
toutes les pensées dont l’importance avait été antérieurement
vitale : et l’on ne pouvait accomplir une pareille révolution
d’une manière plus absolue qu’en niant les fondements logiques
de ces pensées --- sans se soucier de calculer combien de forces
sociales capables et dignes de durer devaient s’en trouver gaspillées,
et c’est un fait qu’un nombre considérable de ces forces se
trouva perdu par l’avènement du christianisme. En second lieu, ce
furent des cercles étroits qui consommèrent au début le mouvement,
et dans lesquels, en raison de leur étroitesse même, il ne pouvait
s’élever d’opposition sociale très prononcée contre la violence faite,
par les idées nouvelles, aux possessions acquises de l’intelligence
sociale. La chose se modifia lorsque s’imposa la nécessité d’adapter
le christianisme à des conditions de vie, internes et externes, moins
prochaines ; et, lorsque le christianisme se fut étendu à un cercle
social plus large, et finalement au cercle le plus large, la conservation
sociale exigea que, tout en détruisant l’ancienne conception de
l’univers, on réintégrât néanmoins ces notions qui étaient à la fois
le produit et la condition du travail social, à savoir les postulats logiques,
dans la jouissance au moins partielle de leurs droits, et que,
pour amener à leur perfection les conceptions réclamées maintenant
par une nécessité morale, on inventât des hypothèses nouvelles plutôt
que de nier, purement et simplement, les formes antérieurement
existantes de la pensée. C’est peut-être de cette nécessité sociologique
que sont nées la philosophie gnostique et la philosophie
scolastique. Chez toutes deux on discerne clairement l’effort pour
sauver des formes logiques de la pensée tout ce qu’il est possible de
sauver. La première, en particulier, ne recule pas devant la métaphysique
la plus abstruse quant à son contenu, afin de parvenir à
une conception religieuse de l’univers par voie de déduction
logique : et ces déductions prennent, chez les philosophes du moyen
âge, une conscience encore plus exagérée de leur rigueur qu’elles
n’avaient chez les gnostiques. Ainsi le pédantisme dialectique des
scolastiques, leur manie de couper les cheveux en quatre, peuvent
bien avoir reposé sur une base sociologique très solide : avec la
logique il s’agissait de sauver, parmi toutes les acquisitions sociales
en matière intellectuelle, la plus profonde et la plus vaste qui,
après s’être consolidée à travers des milliers d’années, était maintenant
menacée par la transformation que le christianisme opérait
dans la conception passée de l’univers, et dans plusieurs
parties de la conception encore subsistante. Que d’ailleurs la scolastique
ait conclu par le *credo quia absurdum*, cela prouve précisément
qu’il y avait divorce entre les exigences nouvelles que la
religion dictait à l’intelligence et les autres critériums de la connaissance
logique : l’adaptation des deux critériums l’un à l’autre,
tentée par la scolastique, ne réussissait pas dans tous les cas ; et,
lorsqu’il y avait scission, il ne demeurait plus à la piété d’autre
ressource que de *croire* à ce qu’elle ne pouvait *connaître*. Un
homme aussi versé que Stubbs dans la connaissance du moyen âge
affirme que le droit, placé sous la dépendance directe de la logique,
fut l’idée dirigeante de la vie publique au moyen âge. "Ce qu’était
la logique pour le philosophe, la jurisprudence l’était pour l’homme
d’État et le moraliste --- un effort pour justifier toutes les conclusions
en les rapportant directement à de premiers principes." Il
émet l’opinion qu’à cette époque les guerres elles-mêmes et les actes
de violence cherchaient toujours un fondement juridique intelligible,
tandis que l’histoire des temps modernes jusqu’à la Révolution
française est une simple histoire de la violence, et, depuis la Révolution,
une histoire d’idées régnantes. Si cette observation est juste,
on y trouve une preuve de la peine prise, au milieu des agitations
confuses du moyen âge, pour conserver, en soumettant les actes à
des règles logiques, un fondement dernier et inébranlable de l’organisation
de la vie.

De la conception que nous nous faisons de la relation qui règne
entre la vérité théorique et la pratique de la vie, découle encore une
autre maxime dont le sens est analogue. Si vraiment toute pensée
est originairement issue de l’action, cette relation d’effet à cause est
néanmoins renversée dans la mesure où, à présent, c’est la pensée
qui, de cent façons, détermine l’action : mais la relation première
manifeste encore sa réalité en ceci que les fondements de la connaissance,
les axiomes de notre représentation de l’univers, les points
de départ derniers de nos convictions ne sont pas susceptibles de
démonstration théorique, et, à cause de cela, ne sont pas discutables.
Là où règne l’accord sur les derniers principes, là se concilient les
divergences d’opinion qui pourraient se produire, parce que ce sont
ces principes eux-mêmes qui servent à démontrer d’une façon convaincante,
avec le secours de la logique formelle et des faits d’expérience,
la vérité ou la fausseté de toute affirmation particulière. Mais
en ce qui concerne les dispositions qui tiennent au caractère pratique
des individus, s’il éclate une dissension se manifestant dans la contradiction
des principes derniers de ces individus, aucune discussion ne
sert plus de rien parce qu’il manque à une telle discussion une base
commune et que chacun nie justement la force démonstrative des
axiomes qui prouvent tout pour l’autre. Il semble, par exemple,
impossible que les défenseurs de la philosophie individualiste
puissent convaincre ceux de la philosophie socialiste ou réciproquement :
il semble qu’on atteigne ici le point où les convictions
adverses doivent ou bien employer, pour se combattre, soit la violence
extérieure, soit les moyens purement psychologiques de la
persuasion et de l’appel aux sentiments, ou bien se respecter l’une
l’autre, faute d’un accès à des principes théoriques et logiques
admis de part et d’autre. Et cependant où trouver en pareil cas la
preuve que l’on a réellement atteint le sommet où la discussion
expire ? Qui sait si, au delà du point dont chacun fait dépendre ses
convictions, et qui est en apparence le plus élevé de tous, il n’en est
pas un autre encore plus haut placé, dont chacun part, en fait,
inconsciemment, ou du moins jusqu’auquel on *peut* contraindre
chacun à remonter ? Alors les partis pourraient toujours espérer
trouver le terrain commun sur lequel il serait possible de ramener
leurs opinions divergentes à l’unité par voie logique si jusque-là
ils étaient restés absolument séparés les uns des autres, c’est qu’ils
avaient érigé en principes absolus les conclusions diverses tirées
d’un même principe. Je crois que la connaissance joue sous ce
rapport un rôle pratique particulièrement important. Si, en effet,
tout système complexe de tendances pratiques suppose un acte
primitif de volonté comme sa base --- acte primitif dont la pensée
ne peut que déduire les conséquences, --- c’est aussi une règle de
méthode à suivre de ne jamais tenir une volonté particulière donnée
pour absolument dernière tant qu’une autre volonté particulière
s’oppose à elle. Pour atteindre ce que l’on nomme en général les
fins morales de l’humanité, il faudrait persister à concevoir toujours
comme une chose possible que les oppositions qui se produisent
entre des formes collectives d’action soient seulement de nature
théorique ; en d’autres termes, que les maximes fondamentales
suprêmes, points de départ des divergences en question, et sur
lesquelles nul accord n’apparaît jusqu’à présent comme logiquement
possible, puissent encore se déduire théoriquement de principes
plus reculés, et que ceux-ci seuls expriment la volonté absolue à
partir de laquelle la divergence des tendances n’apparaît plus dès
lors que comme une divergence d’opinions théoriques dérivées.
Car, sitôt que le plus haut point pratique est commun, il devient
possible, sur tous les autres points, de "s’entendre", de se livrer à
des argumentations et à des réfutations réciproques. Le mot d’Aristote :
"lorsque nous veillons, nous avons un univers commun ;
lorsque nous rêvons, chacun de nous a le sien propre", s’applique
tout aussi bien à la représentation et à la volonté considérées dans
leurs relations réciproques. Que la volonté de plusieurs hommes
puisse être une, c’est en quelque sorte l’effet d’un hasard heureux :
car chacun doit créer en soi et manifester hors de soi l’impulsion
décisive que donne le caractère sans que nul autre soit capable de
l’y contraindre. Au contraire, sur le domaine de la connaissance,
une telle contrainte peut être exercée. L’univers de la connaissance
pure, au sens fort qui exclut tout élément volontaire, nous est
commun à tous. Aussi l’organisation de la vie pratique requiert-elle
seulement que l’unité existe au point le plus élevé et suprathéorique
c’est assez pour mettre fin à toutes les scissions secondaires ou
particulières. La relation entre connaissance et volonté pris comme
éléments de la tendance générale de notre vie est donc définie par
ces deux principes. S’il se produit, en fait, un antagonisme entre
des individus ou des partis, il faut comprendre que cet antagonisme
a pour point de départ des oppositions de caractère, des appréciations
primitives de valeurs qui, développées logiquement, appliquées
à la connaissance théorique et au monde comme représentation,
doivent produire des systèmes téléologiques et des intuitions
pratiques mutuellement hostiles. En chercher la réconciliation par
voie de démonstration théorique et logique, c’est méconnaître ce fait
que les connaissances, même générales, ne sont que des moyens et
des matériaux pour l’organisation de ces tendances originelles : la
similitude des formes logiques et des connaissances particulières
dissimule aisément les différences radicales entre les jugements de
valeur qui servent de principes supérieurs. Dans ces différences, qui
sont réelles, se manifeste la primauté des valeurs pratiques sur les
valeurs théoriques et l’impossibilité, quel que soit l’accord sur
celles-ci, de passer outre au *veto* de celles-là.

Aussi l’approfondissement des bases de la vie sociale, la substitution,
à celles qui sont d’ordre volontaire, d’autres sur lesquelles un
accord théorique est possible, est un progrès que l’on conçoit pouvoir
être continué à l’infini. Or, si nous restons fidèles à cette conception,
nous sommes en possession d’un principe pour comprendre
la relation pratique que soutiennent l’un avec l’autre des partis
opposés. Le principe logique et relatif à la connaissance, étant né
d’un accord entre un nombre infini de besoins individuels, doit à
son origine sociale un caractère plus conciliant que n’ont les
maximes dernières dans l’organisation de la vie qui s’offrent constamment
au choix individuel de chacun. Sans doute ces maximes
n’ont pas encore été réduites à l’unité, sans doute les partis suivent
encore chacun sa route sur les points où la décision théorique
n’atteint pas, --- néanmoins la *voie* qui mène à la réconciliation,
c’est l’effort continu pour reconnaître dans les différences pratiques
des différences théoriques, c’est-à-dire dans des différences irréductibles
entre des opinions tenues pour contradictoires des différences
relatives et discutables. Cet effort consiste, ou trouve sa forme la
plus achevée, dans la découverte constante de maximes supérieures
à celles qui, à chaque instant, paraissent concluantes : par où la
possibilité reste ouverte, et la vraisemblance accrue, de découvrir
plus tard ce qui unit par delà ce qui divise aujourd’hui. Ici se développe,
en outre de notre théorie primitive, selon laquelle la connaissance
a une origine pratique, une antithèse particulière qui semble
presque factice : tout ce que la connaissance théorique peut constater,
c’est que nos tendances vitales aboutissent à des différences
d’opinion uniquement pratiques et sur lesquelles le raisonnement
est sans action, tandis qu’inversement l’effort pratique et moral
doit viser à tenir de plus en plus pour théoriques ces différences
pratiques qui séparent les maximes dernières.

Dans la mesure où la connaissance se dégage des fins pratiques
dont elle est issue, dans la mesure où elle devient un domaine autonome,
un système fermé de relations internes, elle acquiert par là le
caractère psychologique d’une valeur absolue, c’est-à-dire d’une fin
dernière qui est désirée en et pour soi, et non comme moyen en vue
de fins extérieures, qui, bien au contraire, implique que l’on fait
acte de renoncement formel à tous les autres biens. De tous nos
intérêts réunis, intérêts que nous devons assurément concevoir, sans
exception, comme produits par des nécessités vitales, égoïstes et
sociales, l’intérêt pris à la vérité est celui qui s’est le plus complètement
affranchi de cette origine pratique. La valeur immédiate et
objective que nous assignons à la vérité, nous la plaçons tout à fait
au delà des alternatives qui se posent soit entre nos motifs individuels
et nos motifs sociaux, soit encore entre nos motifs égoïstes et
nos motifs moraux. Sans doute nous avons l’habitude de définir un
effort qui, dans la vérité, ne poursuit d’autre fin que la vérité seule
comme un effort moral. Mais, après un examen plus attentif, je crois
découvrir que cet intérêt est d’une espèce tout à fait spéciale et ne
saurait rentrer, comme les autres intérêts dont l’objet n’est relatif
à aucune fin égoïste, sous la catégorie morale. Car, s’il en était
ainsi, il devrait pouvoir être mis en balance avec ces autres intérêts,
et, en cas de conflit, être parfois vaincu par ceux-ci. Mais ce n’est
pas là ce qui arrive, au moins dans des cas fréquents. L’intérêt pris
à la vérité est, chez celui qui l’éprouve en général, et pour qui il
s’est dégagé de l’intérêt pris aux objets particuliers qui sont le contenu
de la connaissance, tout à fait incomparable avec la série des
autres valeurs : la considération du résultat matériel ne saurait
influer sur l’intérêt que la connaissance inspire ; si nous éprouvons
cet intérêt, nous sommes pleinement indifférents à la question de
savoir si la connaissance acquise est en soi belle ou laide, utile ou
nuisible, source de joie ou de tristesse --- au sens égoïste comme au
sens moral. Ainsi est démontré le caractère tout spécial de cet intérêt
et l’absence d’une mesure qui lui soit commune avec les autres
intérêts, permettant de déterminer qu’il possède plus ou moins de
valeur relative que ceux-là. Il manque à la vérité, considérée comme
idéal, ce ton chaud qui agit sur les sentiments et qui caractérise les
autres idées morales : la soif de la vérité a beau être brûlante, le
travail pour l’atteindre a beau être passionné, l’affirmation de la
vérité atteinte, énergique --- la vérité elle-même, comme idéal
immobile et comme donnée fixe, demeure froidement inaccessible à
ces passions, et nous prescrit, au delà de tous les mouvements subjectifs
qui peuvent accompagner sa réalisation chez les individus,
l’observation de cette règle spinosiste : *res humanas nec ridere nec
lugere sed intelligere*. Quelque complets que soient le sacrifice et
l’abandon de tous les intérêts inférieurs, superficiellement désignés
comme égoïstes, au sens particulier du mot, qu’exige de nous l’effort
vers la connaissance, à ce renoncement aux intérêts du moi manque
cependant le caractère altruiste qui fait la relation proprement
morale. Dans tout l’ensemble de nos fins idéales, il n’en est pas une
autre qui, une fois atteinte, absorbe et anéantisse à ce point tous
les mouvements et toutes les agitations de l’âme qui accompagnent
l’effort fait pour l’atteindre. La loyauté, l’amour, l’abnégation sociale,
le perfectionnement intérieur, l’adoration religieuse --- ce sont
autant d’exigences morales qui conservent, même à leur plus haut
point de réalisation, un caractère encore relativement subjectif, en
elles se manifeste la personnalité morale, tandis que la vérité, dans
la paix et l’isolement de son être, fait appel, en quelque sorte, aux
éléments impersonnels de la personnalité ; son objectivité et son
immutabilité suscitent d’autres énergies et une autre espèce de devoir
que ne font ces fins idéales, intimement fondues en soi avec le
développement psychologique dans lequel elles se réalisent. Comme
la chose en et pour soi, comme l’être est au delà du bien et du
mal --- "*nothing is good or bad, but thinking makes it so*", --- de
même aussi le reflet de l’être dans l’esprit, que nous appelons
vérité. Dans la mesure où celle-ci est pleinement, réellement pure
image de l’être, elle emprunte à l’être la pure existence de fait, par
rapport à laquelle toutes les impulsions, tous les jugements de
valeur sont un élément proprement étranger. La théorie de la connaissance
peut bien nous convaincre que ce mode objectif, en
quelque sorte substantiel, d’existence de la vérité n’empêche pas
qu’elle consiste, en dernière analyse, en simples fonctions psychiques,
et que l’image de l’être n’existe pas au même sens que l’être ;
il est cependant permis de laisser ces considérations de côté, parce
qu’il n’est question ici que du caractère psychologique de l’idéal de
la vérité. Pour mettre cet idéal à part de tout ce que l’on appelle
proprement idéal moral, on peut alléguer cette différence que l’idéal
en question contient en soi son propre critérium ; et voici ce que
j’entends par là. Soit que l’existence objective des choses soit posée
comme réelle et extérieure à la représentation, de sorte que la connaissance
consiste dans le reflet de cette existence objective ; soit
que nous concevions la vérité comme une simple relation réciproque
définie des éléments représentatifs --- dans les deux cas, c’est toujours
par des qualités de la représentation, c’est par la relation de
la représentation à soi-même que se déterminent sa valeur et son prix,
en tant que vérité. La valeur de la représentation comme telle
ne se mesure pas aux effets qui peuvent en résulter, ni aux fins auxquelles
elle peut conduire ; comme vérité, elle n’a de rapport qu’à
soi, elle a atteint son but, aussitôt qu’elle a réalisé la structure
interne de la vérité, et sa valeur est alors tout à fait indépendante
de la question de savoir si, dans la suite, la connaissance en général,
dans sa perfection purement immanente, se manifeste comme ayant
ou n’ayant pas une valeur extrinsèque. Les valeurs proprement
morales, au contraire, doivent, dans chaque cas particulier, être
mesurées par leur fin : les actes de loyauté et d’abnégation, de religiosité
et de patriotisme n’ont en général de valeur, pris en soi, que
comme déterminations particulières d’une fin morale suprême ; en
tant que valeurs morales, ils ne constituent pas des valeurs que l’on
doive poursuivre pour elles-mêmes, et dont on n’ait à se demander
qu’ensuite si elles s’accordent avec la fin dernière de la volonté
bonne c’est au contraire cet accord lui-même qui est la condition
de leur valeur. Mais, sans qu’il soit question d’un pareil accord, la
vérité se propose à nous comme ayant une valeur, alors même qu’elle
ne satisfait qu’à son critérium interne propre.

Parmi les fins morales, une seule me semble participer du même
caractère psychologique, une seule me semble être un but absolument
désirable, la source d’une satisfaction absolue, sans que le
résultat de sa réalisation, non plus que toute autre espèce de relations
extérieures et de conséquences, puissent nous faire douter de ce
qui en constitue la valeur vraie : c’est la justice. Ici encore, comme
en ce qui concerne la vérité, ce sont effectivement les mêmes propriétés
de l’idéal lui-même qui produisent les mêmes conséquences psychologiques.
Car la justice contient, elle aussi, en soi-même, d’une façon
immédiate, la règle dont l’exécution fait sa valeur. Étant donnée une
rencontre de prétentions définies, la justice exige qu’un équilibre se
produise entre ces prétentions adverses, équilibre dont la nécessité
se déduit exclusivement des données du cas en litige, nécessité absolument
indépendante de toutes les circonstances ou conséquences
extérieures. Tout ce qu’il y a d’impersonnel et de dur dans la vérité
provient de ce qu’un rapport objectif de deux facteurs, l’être et la
pensée, est nécessaire pour que cet idéal soit atteint, et de ce que
cet idéal nous paraît avoir une valeur indépendante de tout rapport
avec les circonstances du cas individuel où il se réalise. Or la même
observation s’applique exactement à la justice : une fois que l’âme
s’est livrée à elle, la justice ne tolère plus de compromis, son caractère
absolu et son indifférence vis-à-vis de toutes les considérations
de personnes exige qu’elle soit accomplie, dût l’univers tomber
en ruines. Il y a par conséquent la même relation entre l’idéal
de la connaissance et l’idéal de la justice, en général, qu’entre
ces parties spéciales de l’une et de l’autre, plus haut mentionnées :
la logique et le droit public. D’ailleurs la psychologie individuelle
rend cette affinité manifeste : car la domination absolue de l’amour
de la vérité coïncide souvent dans la même âme avec un sens également
intense de la justice, de même qu’avec une certaine insensibilité
vis-à-vis de tous les besoins et de tous les rapports moraux
dont la nuance individuelle est plus accusée, parfois aussi avec un
dédain exclusif des points de vue les plus hauts, à la valeur irréductible
desquels vérité et justice doivent seulement la signification
de leurs prescriptions plus formelles. Je désignerais volontiers ces
natures-là sous le nom de natures analytiques, par opposition aux
natures synthétiques, qui participent d’une façon plus active à créer
le procès, à soulever la prétention, à poser le problème lui-même.
Celui qui place dans la vérité l’idéal absolu et celui qui le place dans
la justice ne se proposent, ni l’un ni l’autre, la production d’une
réalité substantielle nouvelle : l’un trouve toute faite la réalité qu’il
veut représenter, l’autre toutes données les réclamations qu’il veut
satisfaire. Vérité et justice ont seulement une exigence donnée de
la pensée formelle à satisfaire --- ni plus ni moins, --- et, dans une
certaine mesure, n’ont qu’à *expliquer* ce qui est impliqué dans l’objet.

À la rigueur, la loyauté pourrait encore apparaître comme constituant
un idéal de la même forme. Car elle aussi n’a que des engagements
donnés à respecter, des relations déjà formées à continuer,
des résolutions antérieurement prises à tenir ; à l’instant où elle
entre en action, elle trouve déjà en face de soi l’objet idéal proposé,
auquel elle n’a rien de nouveau à ajouter, mais dont elle doit seulement
faire passer à l’acte les prétentions à l’être et les potentialités.
Mais il subsiste ici cette différence, que la position de la donnée ou
de la circonstance, par rapport à laquelle la loyauté est dans une
relation purement analytique, suppose déjà elle-même un acte moral
antérieur, et cela dans chaque cas particulier --- ce qui n’est plus
vrai en matière de justice, tout au moins lorsqu’il s’agit de justice
égalitaire. La loyauté est d’ailleurs bien une attitude formelle de la
pensée, mais cette attitude n’est telle que par rapport à un contenu
dont la nature est créatrice et morale ; elle est la prolongation d’un
acte synthétique, ou encore la forme durable de cet acte. Dans l’idéal
de la vérité nous trouvons une exigence qui n’invoque, pour être
satisfaite, que sa valeur propre et peut être mise en pendant avec
les exigences morales, ou du moins être rapprochée de celles-ci. Il
faut avoir pesé toutes les valeurs entre elles pour que se produise le
devoir pratique, le devoir légitime ; jusque-là chaque devoir particulier
n’est pas vraiment obligatoire : ou bien ce n’est qu’un devoir
provisoire, qui ne fonde pas sa légitimité sur des raisons, ou bien
il a déguisé sa légitimité de circonstances en légitimité absolue[^2].
L’idéal de la vérité est affranchi de ce caractère conditionnel ; s’il est
seulement admis d’une façon générale, sa valeur ne saurait plus ni
croître ni décroître, quelles que soient les circonstances, extérieures
ou intérieures, qui d’ailleurs l’accompagnent.

[^1]: Über eine Beiziehung der Selectionslehre zur Erkenntnisstbeorie. *Archiv fur systematische Philosophie*, Heft 1.

[^2]: Cf. mon "Einleitung in die Moralwissensehaft", chap. I.

