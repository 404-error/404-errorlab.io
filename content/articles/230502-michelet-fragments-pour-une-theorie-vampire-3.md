---
persona:
  - Étienne Michelet
title: "Fragments pour une théorie Vampire III"
slug: "fragments-pour-une-theorie-vampire-3"
date: 2023-05-15
echo:
  - ontique
images:
  - "img/230502-michelet-fragments-pour-une-theorie-vampire-3.jpg"
gridimages:
  - "img/230502-michelet-fragments-pour-une-theorie-vampire-3-grid.jpg"
sourceimage: "Étienne Michelet"
summary: ""
notes: ""
citation: "La figure est dans l’œil de la pierre."
poetry: false
hangindent: false
---

## Pulgok Seonbawi

Sur les flancs de la montagne Namsan à Gyeongju, J.S. a localisé son
emplacement : le rocher se trouve là, juste derrière la haie de bambous
noirs. L'image photographique n'en donne qu'une représentation mais non
la présence. La figure respire là, dans le silence de la pierre.

{{%asterisme%}}

Certain.es y voient une femme, *grand-mère* selon la tradition... Cette
idée stupidement confucéenne n'a pas lieu d'être. Il ne s'agit pas non
plus d'un bouddha, qu'il soit du passé ou de l'avenir. Il ou elle est de
tous les temps à la fois, passés et futurs réunis dans un seul présent.

{{%asterisme%}}

Le visage effacé par l'érosion n'a pourtant pas souffert de cette
*disparition*. Bien au contraire, la figure s'est révélée en s'effaçant.
Son effacement était sa condition de révélation.

{{%asterisme%}}

Plus que *figure assise*, elle fait partie de la matière, *prise* dans
un ensemble de roches granitiques. Il faut ainsi voir ce qui, invisible,
vibre dans la matière, c'est-à-dire son magnétisme.

{{%asterisme%}}

La figure représente la posture méditative du *seon*[^1]. Son immobilité
est aussi celle de la pierre, mais elle n'en finit pas de se reformer.
Cette immobilité *figurée* devenue ici organique, minérale, entre en
communion silencieuse avec la présence immobile de la matière. Le
souffle de la figure méditative transcende la pierre, lui donne sa
pulsation secrète.

{{%asterisme%}}

Il n'y a rien à dire et pourtant tout est dit. Le langage se heurte au
silence de la figure ou de la pierre. Le vent qui, aux abords du rocher,
plie les bambous noirs, porte en lui cette vérité, celle de la présence
du vide, de ce silence qui donne voix à toute chose dans la continuité
des transformations, dans le devenir du monde et de ses phénomènes.

{{%asterisme%}}

Le silence anime la matière. La sculpture parle avec cette logique de
silence. Son idiome est le vide.

{{%asterisme%}}

Dans la cavité creusée, la niche est une enveloppe, elle y garde un
secret. La cavité est la paupière de l'œil effacé par l'érosion.
Entrouvert, il garde le secret ouvert.

{{%asterisme%}}

La figure est dans l'œil de la pierre. Le rocher est l'œil de celui ou
celle qui regarde le rocher. Nous pouvons voir et entendre le silence
dans le creux de l'œil. Le rocher est aussi le pavillon de l'oreille.
Nous pouvons alors déceler dans chaque vibration la manifestation du
silence.

{{%asterisme%}}

La figure sculptée dans le rocher n'est pas seulement la représentation
d'un corps. Elle est avant tout de la matière qui vibre. S'il y a une
image du corps, elle n'est que la limite qui permet de circonscrire la
vibration dans la forme. Ici, la vibration est celle du souffle, du sang
qui procède de la vascularisation, du mouvement invisible qui anime le
vivant. Mais le magnétisme, son énergie, traverse chaque chose, comme
chaque pierre. Les énergies se répondent, toutes interdépendantes. La
figure ne renvoie non plus à l'idée d'un double du réel représenté.
Incrustée dans la matière, révélée par la lumière, elle véhicule par son
immobilité parfaite le magnétisme de la présence pure. Son visage défait
le visage humain de la pensée. Son visage est le visage de
l'*inenvisageable*. Son détachement d'avec la pensée discriminante, de
celle qui fige l'humain dans le canevas du langage et de la
représentation du langage, lui permet enfin d'*être* au monde sans être
représentée par la pensée.

{{%asterisme%}}

Ce visage est dépourvu du visage de la pensée, de l'humain catégorisé
par le conditionnement des phénomènes, de la pensée discriminante qui
lui façonne ses traits. Le visage érodé échappe au conditionnement de la
pensée humaine. Il rejoint l'inhumain dans son effacement, et par ce
même effacement est lié au *devenir* sans nom, étant ainsi libéré de
tout assujettissement phénoménique.

{{%asterisme%}}

Rares sont les images qui abolissent la représentation et ouvrent ainsi
à la reconnaissance de l'*être* sans passer par le filtre de la
représentation de l'*être*.

{{%asterisme%}}

J'ai de la gratitude, et même pour le néant. Surtout pour le néant...
Parce que malgré tout, il nous laisse la possibilité d'apprécier le
silence, ou la lumière...

{{%asterisme%}}

Devant le rocher, assis sur le sol, un vieil homme récite des sutras, sa
voix est un mouvement, tout comme le vent, tout comme les insectes ;
tout comme ce qui se meut dans chaque particule du vivant. Cette énergie
a un cœur mais ne se nomme pas. Elle n'a pas de lieu mais elle est
partout. Elle est invisible parce que présente dans chaque phénomène du
vivant. *L'occulte est au cœur du manifeste et non dans son contraire.*
Quand le mouvement s'éteint et que l'immobilité du *seon* demeure, le
cœur est reconnu comme étant ce vide sans nom ; dans le devenir
apatride.

{{%asterisme%}}

Ne faites pas une idole de cette figure, et ne lui donnez surtout pas de
visage, que ce soit celui d'une divinité ou d'une vieille femme. Arrêtez
de tout recouvrir de votre humanité, de votre pensée qui fige le vivant
dans la figure d'une momie.

{{%asterisme%}}

Nous avons cessé de voir parce que nos yeux ne posent plus un regard
dépossédé des phénomènes, *reposé* dans le vide. Nos yeux sont possédés
par l'agitation et le trouble, ils voient tout, donc ils ne voient plus
rien. L'immobilité elle, efface, dissimule et ainsi préserve l'*étant*
comme un refuge. Elle réalise cette distance qui est aussi la condition
de souveraineté du vivant. Le rocher qui dissimule la figure dans son
sein, en la dissimulant, lui redonne sa pleine visibilité dans le monde
de l'hypervisibilité. Ainsi, ce qui est caché apparaît, ce qui est
immobile se meut véritablement.

{{%asterisme%}}

Dans le rocher de *Pulgok*, la figure n'est pas la face d'une
représentation, elle n'est plus montrée à la surface du visible. Elle
est une figure intérieure et présente au-delà du champ du visible, étant
placée à l'intérieur du rocher, dans une niche ou une cavité creusée. Sa
surface incurvée est comme une peau retournée. C'est l'envers du monde
qui est ainsi montré, ou sa face interne, dans un retournement du
visible procédant d'une nouvelle dimension, celle de l'invisible
dévoilé. Nous sommes face à l'envers du monde, face au cœur organique
qui bat dans ce refuge de silence. La niche granitique en est la cage
thoracique et la nervure minérale, ou le nerf dans la chair. Cette
profondeur est la profondeur du commencement. Car *la profondeur est au
commencement*. Toutes les eaux y confluent, le végétal s'y fortifie.

{{%asterisme%}}

L'eau qui devient humaine, cela s'appelle le sang. Qui sait cela
commande bien au-delà de sa mort, jusque dans le vivant.

[^1]: Zen.
