---
persona:
  - Étienne Léro
title: "Misère d'une poésie"
slug: "misere-dune-poesie"
date: 2020-10-22
echo:
  - esthétique
images:
  - "img/201022-lero-misere-dune-poesie.jpg"
gridimages:
  - "img/201022-lero-misere-dune-poesie-grid.jpg"
sourceimage: "https://commons.wikimedia.org/wiki/File:Mount_Pel%C3%A9e_1902.jpg"
notes: "Ces textes ont paru en 1932 dans le numéro unique de Légitime Défense, revue regroupant de jeunes écrivains marxistes martiniquais, qui marqua les prémices du mouvement de la négritude."
summary: "Il est profondément inexact de parler d'une poésie antillaise. Le gros de la population des Antilles ne lit pas, n'écrit pas et ne parle pas le français. Quelques membres d'une société mulâtre, intellectuellement et physiquement abâtardie, littérairement nourrie de décadence blanche se sont faits, auprès de la bourgeoisie française qui les utilise, les ambassadeurs d'une masse qu'ils étouffent et, de plus, renient parce que trop foncée."
citation: "marinés dans l'alcool rouge, l'amour africain de la vie, la joie africaine de l'amour, le rêve africain de la mort"
---

# Misère d'une poésie

Il est profondément inexact de parler d'une poésie antillaise. Le gros de la population des Antilles ne lit pas, n'écrit pas et ne parle pas le français. Quelques membres d'une société mulâtre, intellectuellement et physiquement abâtardie, littérairement nourrie de décadence blanche se sont faits, auprès de la bourgeoisie française qui les utilise, les ambassadeurs d'une masse qu'ils étouffent et, de plus, renient parce que trop foncée.

Là-bas, le poète (ou le "barde", comme ils disent), se recrute, en fait, exclusivement dans la classe qui a le privilège du bien-être et de l'instruction. (Et s'il fallait chercher la poésie là où on la contraint à se réfugier, c'est dans le créole qu'il faudrait puiser qui n'est point un langage écrit, c'est dans les chants d'amour, de tristesse et de révolte des travailleurs noirs.)

Le caractère exceptionnel de médiocrité de la poésie antillaise est donc nettement lié à l'ordre social existant.

On est poète aux Antilles comme l'on est bedeau ou fossoyeur, en ayant une "situation" à côté. Tel médecin, tel professeur, tel avocat, tel président de république, se fait une petite notoriété parmi la bourgeoisie mulâtre en lui servant son visage et ses goûts en vers alexandrins.

L'antillais, bourré à craquer de morale blanche, de culture blanche, d'éducation blanche, de préjugés blancs, étale dans ses plaquettes l'image boursouflée de lui-même. D'être un bon décalque d'homme pâle lui tient lieu de raison sociale aussi bien que de raison poétique. Il n'est jamais à son goût assez décent, assez empesé. --- "Tu fais comme un nègre", ne manque-t-il pas de s'indigner si, en sa présence, vous cédez à une exubérance naturelle. Aussi bien ne veut-il pas dans ses vers "faire comme un nègre". Il se fait un point d'honneur qu'un blanc puisse lire tout son livre sans deviner sa pigmentation. De même que, honteux de ce qui subsiste en lui de polygamie africaine, il couche en cachette avec ses bonnes, de même, il a soin de s'expurger avant de "chanter" (*sic*), soin de bien se mettre dans la peau du blanc, de ne rien lâcher qui le trahisse. Invariablement, il vous décrit des paysages ou vous raconte de petites histoires où l'hypocrisie le dispute au diffus et au Louis-Napoléon. (La France n'a peut-être dû son fastidieux Parnasse qu'au fait que Leconte de Lisle et Dierx étaient Réunionnais, Herédia Cubain.)

Il y a, comme on voit, pas mal d'humour dans le cas du bourgeois mulâtre, mais cet humour lui échappe, et en même temps, tout le bénéfice poétique de son rôle de singe. Il tiendra que c'est son droit de ne montrer de lui que ce qu'il estime bon à montrer, car, élevé chez les pères, ou indirectement inbu de leur religion importée, c'est un affreux casuiste.

L'étranger chercherait vainement dans cette littérature un accent original ou profond, l'imagination sensuelle et colorée du noir, l'écho des haines et des aspirations d'un peuple opprimé. Un des pontifes de cette poésie de classe, M. Daniel Thaly, a célébré la mort des Caraïbes (ce qui nous est indifférent, puisque ceux-ci ont été exterminés jusqu'au dernier), mais il a tu la révolte de l'esclave arraché à son sol et à sa famille.

Pauvres sujets, mais non moins pauvres moyens poétiques.

Le bourgeois antillais est ici plus méfiant que jamais. Son complexe d'infériorité le pousse dans les sentiers battus "Je suis nègre", vous dira-t-il, "il ne me sied point d'être extravagant[^1]."

De même qu'il se refuse à voir dans la France d'aujourd'hui autre chose que la France de 89, le bourgeois antillais se refuse à adopter toute règle poétique que cent ans d'expériences blanches n'aient point sanctionnée. L'audacieuse neutralité de M. Gilbert Gratiant est très caractéristique à cet égard quand il nous dit : "Des règles du jeu, beaucoup sont connues, ou bien traditionnelles, ou bien révolutionnaires : toutes sont fécondes. Mais pourquoi subir le règne d'une seule ? Tyrans plus nombreux et de rechange, dirait-on, tyrannie moindre."

Je vous le disais plus haut : le bourgeois antillais a toujours de bonnes raisons.

Non content d'user d'une prosodie, et d'une prosodie surannée, l'antillais l'agrémentera d'un soupçon d'archaïsme : cela fait "vieille France".

Une indigestion d'esprit français et d'humanités classiques nous a valu ces bavards et l'eau sédative de leur poésie, ces poètes de caricature dont je ne vous citerai que quelques noms : Vieux et Moravia en Haïti, Lara en Guadeloupe, Salavina, Duquesnay, Thaly, Marcel Achard en Martinique.

Nous serions impardonnables de ne pas mentionner également deux poètes antillais que leurs trente ans, leur souci de respectabilité, leur conformisme impénitent, Ieur fonds universitaire gréco-latin, leur passeïsme, leur compasseïsme, nous désignent comme les dignes successeurs de leurs prétentieux ainés.

Du poème récemment publié de M. Henri Flavia-Léopold, "Le Vagabond", que chacun sache qu'il est une gratuite rédaction en vers alexandrins.

M. Gilbert Gratiant nous offre plus épaisse matière dans son livre intitulé : "Poèmes en vers faux."

M. Gratiant triche, quant au titre. Ses vers ne sont, hélas, pas faux, et nous tenons à lui faire honneur de son tour de force de deux cent-cinquante pages d'honnête versification. Les vers de M. Gratiant ne traduisent ni les iniquités sociales de son pays, ni les passions de sa race, ni sa valeur propre de désordre et de rêve. Chacune de ses pièces nous paraît un intempérant commentaire autour d'un poème qui était à faire. On n'y traverse pas un éclair d'innocence, pas un instant de courage, pas une tentative vers l'expression, passagère et détournée, de la violence humaine, qu'est la poésie.

M. Gratiant nous ressert tout le bric-à-brac de ces cent-cinquante dernières années, "les ailes d'or", "le diaphane", les cygnes, les lunes et les zig-zagances. Le meilleur vers du volume est à coup sûr le mot "d'Yves, cinq ans" : "Les Allées sont comme un bifteck bien grillé" que M. Gratiant a cru devoir alourdir d'un texte explicatif.

L'auteur a précédé ses vers de ce qu'il nomme "Cent-sept sous-évidences concernant l'art du Poète". La prose de M. Gratiant nous rappelle désagréablement le style d'Alain, théoricien de la liberté intérieure, grand maître en pantalonnades. Une lecture préalable de ces "sous-évidences" offre l'inconvénient (ou l'avantage, comme vous voudrez) de vous indisposer à l'égard des vers. L'auteur nous y dit, entre autres choses, qu'un poème doit être compris, goûté, senti, etc. Mais non. Ça entre ou ça n'entre pas, et c'est un ruban de dynamite qui finit plus ou moins tôt, plus ou moins tard, d'exploser à l'intérieur d'un individu donné. M. Gratiant nous donne le ruban sans la poudre. Ce n'est pas du jeu.

J'admire le détachement très universitaire de l'auteur à l'égard des écoles en "isme". Il craint sans doute de perdre, à s'y frotter, sa préciosité douairière, sa gentillesse et son prime-saut. C'est l'honneur et la force du surréalisme d'avoir intégré toujours plus à fond la fonction poésie, d'avoir mis à poil la poésie.

Une fillette, avant d'avoir vu son père nu, l'a toujours confondu avec le vêtement qui l'habille. Celui-ci, nu, lui devient tout de suite obscur et incompréhensible. Ainsi en est-il des pudibonds et de la poésie surrréaliste.

Nous voulons voir en MM. Flavia-Léopold et G. Gratiant les deux derniers représentants antillais d'un lyrisme de classe condamné.

Le vent qui monte de l'Amérique noire aura vite fait, espérons-le, de nettoyer nos Antilles des fruits avortés d'une culture caduque. Langston Hughes et Claude Mac-Kay, les deux poètes noirs révolutionnaires, nous ont apporté, marinés dans l'alcool rouge, l'amour africain de la vie, la joie africaine de l'amour, le rêve africain de la mort. Et déjà de jeunes poètes haïtiens nous livrent des vers gonflés d'un futur dynamisme.

Du jour où le prolétariat noir, que suce aux Antilles une mulâtraille parasite vendue à des blancs dégénérés, accédera, en brisant ce double joug, au droit de manger et à la vie de l'esprit, de ce jour-là seulement il existera une poésie antillaise.

# S.O.S.

Que le soir meure sur la ville\
Et sur l’obscène exploit\
Mise en scène du cœur cinéma\
Cœur simulacre du voyage\
Pour celle qui a peur du paysage\
Et que lasse l'exclusive image\
Si l’incendie éclate\
Il n’y a pas de sortie de secours\
Une oreille pour le dernier appel\
À qui déjouant le péril\
Prendra le cœur à louer\
Rien n’est demandé\
Que le don du bonheur puéril.

[^1]: Tel professeur appartenant à cette bourgeoisie n'a-t-il pas déclaré que Blanche, étant nègre, n'eût pas dû plaquer l'École Normale si cela lui chantait et "trouver mauvais des haricots qu'ont mangés Taine et Renan" ? (Apprendrai-je à ce professeur de physique que l'heureux défroqué que fut Renan n'a jamais tâté des haricots de l'École ?)
