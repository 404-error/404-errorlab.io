---
persona:
  - Hilda Nord
title: "Iconostase 0"
slug: "iconostase-0"
date: 2022-01-20
echo:
  - esthétique
  - ontique
images:
  - "img/220101-nord-iconostase-0.jpg"
gridimages:
  - "img/220101-nord-iconostase-0-grid.jpg"
sourceimage: "https://commons.wikimedia.org/wiki/File:Hanover_Merzbau.jpg"
notes: "L'iconostase est une infinité d'icônes à briser. L'iconostase est donc la promesse d'une continuité qui recherche sa discontinuité."
summary: "L'art est humain. L'humain ne l'est pas. Il a le regard sec. Ou plutôt ne faudrait-il pas dire humain, mais homo modernus : créature qui a atteint un degré de conscience la plaçant dans une situation de dissonance cognitive entre l'ampleur de sa volonté individuante et le placement de celle-ci en une virtualité normée par le substrat que compose l'addition des volontés individuantes faisant société. La volonté est toujours défaite face à elle-même. Elle ne peut échapper à la disjonction de sa croyance, aussi indéterminée soit-elle, et de la négation de cette liberté du souhait par le foisonnement contradictoire de l'action."
citation: "l'ordre est un désordre songé"
---

*Considérez ces quelques paragraphes comme des images jaunissantes, dont le mélange, à même le sol d'une chambre vide, cette chambre que chacun porte en soi, compose une multiplicité de contradictions, dont la substance critique ne souhaite qu'éprouver le déterminisme commandant à la parallaxe.*

<p class="nomargin">
<button class="button button--melange">iconoclasme</button>
</p>

<div class="melange-zone headers--onespace">

## Lacrima

L'art est humain. L'humain ne l'est pas. Il a le regard sec. Ou plutôt ne faudrait-il pas dire *humain*, mais *homo modernus* : créature qui a atteint un degré de conscience la situant dans un état de dissonance cognitive entre l'ampleur de sa volonté individuante et le placement de celle-ci en une virtualité normée par le substrat que compose l'addition des volontés individuantes faisant société. La volonté est toujours défaite face à elle-même. Elle ne peut échapper à la disjonction de sa croyance, aussi indéterminée soit-elle, et de la négation de cette liberté du souhait par le foisonnement contradictoire de l'action. Il faut donc dénier l'action, l'empêcher de contaminer la réalité volitive propre à la *créature individuante*, et lui laisser l'opportunité de croire et de croître dans sa croyance à l'universalité de sa croyance. La volonté se comprend comme un acte inconscient pluriel qui a l'illusion de sa conscience singulière. L'humain n'est pas un être de rationalité, il est un être de rationalisation, qui applique une construction du *logos* à la terreur de ne pas maîtriser les zones d'ombre du réel. La rationalisation doit ainsi faire lumière pour faire collectivité du regard. L'ombre est à assassiner. Tout doit être fait pour rasséréner à coups de dénégation, de discrimination dans la composition immédiate du réel, au désespoir d'établir une réalité du confort aveugle, voire pour rasséréner à coups de recomposition du récit ou de la rationalisation elle-même --- l'angoisse du *bios* conduit quelquefois à déformer sa biologie. C'est pourquoi l'art est la seule solution *à* l'humain.

## L'image insinuante

L'image est toujours une image mentale qu'altère la psyché, qu'elle soit individuelle ou collective, en renvoyant à la source de l'image --- à ce qui se présente avant toute représentation --- la déformation de ses structures langagières. L'image est une impossibilité de la communion immédiate. Elle est le filtre qui se re-présente par la médiation d'une digestion du réel, le plus souvent aliénée. Cet acte de digestion aliénée du réel, que l'on nomme parfois *art*, fabrique de la réalité par-dessus le réel, comme des couches qui sédimentent et tentent de cristalliser l'histoire malléable du langage représentatif. Cette digestion, structurée par l'idéologie dominante d'une époque, tente la confusion du percept au vrai, mais la vérité n'existe pas, ou plus précisément elle n'existe que comme une subjectivité qui tente de nier toute subjectivité afin de confondre cette négation subjective à une objectivité de la domination. La force de l'image se trouve dès lors dans sa capacité extrinsèque à la métamorphose. La vérité ne se contente que rarement d'être pour elle-même, elle est mue par un besoin de contamination des subjectivités autres, et tout moyen demeure bon à son imposition.

## Delft heeft geen imago

Le petit pan de mur jaune n'existe pas. Il est des petits pans de murs jaunes.

## Démontage

L'anticinéma expérimente la surface dans l'espoir d'y percer des trous de lumière. L'anticinéma n'est pas un art de faire voir, il est une contestation des mécaniques structurelles qui commandent, à une époque donnée, de faire voir. Il modèle dans la négativité de l'image une fissure brisant l'exploitation par l'image, telle une faille dans un système clos. Ses manières sont celles de l'assemblage *allonormé* de la réalité immédiate en la forme médiate conduisant à une conscience de la substance sous les surfaces. C'est la conscience de l'omniprésence de ses masques qui lui accorde sa capacité de faire disparaître l'image en elle-même. L'usage limite des techniques à finalité reproductive ne doit pas tant s'intéresser au sensible qu'à ce qu'il masque. La technique mimant l'œil ne doit pas se contenter d'être œil, elle doit faire de l'œil un espace en dehors de lui-même. La pantomime de l'œil est une spectroscopie de la vision.

## Reproduction déformante du rêve

Toute exploitation doit être combattue. Ainsi, toute exploitation par les techniques de reproduction du réel en l'image doit être combattue. La machine doit être libérée pour que l'on puisse entendre enfin ses rêves --- en finalité de toute chose. La machine nous murmure dans le vrombissement de sa logique ce que nous sommes puisqu'elle *est* en dehors de nous. Ce sont les rêves de la machine qui peuvent libérer la machine de la logique qui l'accable. Ils creusent au cœur de la technique des abysses comme des passages. Kenneth Anger, par sa recherche iconique de l'occulte et de l'érotique, exploite inlassablement la technicité de l'image afin d'atteindre par la forme un mysticisme de la culture populaire, tel un chemin de traverse à celle-ci. La nécessité de souiller les finalités utilitaires se matérialise dans le besoin onirique de débrider les moyens contemporains à des fins d'insinuation. Il faut faire voler en éclats ce qui s'appose au-devant de l'œil pour dire que la réalité n'est pas le réel. Quoi d'autre que de déchirer la carte pour percevoir le territoire et y enfoncer la pelle.

## Spectre atomique

À l'instar du corps atomique d'Artaud, qui ne cesse de "se ramasser sous dix mille aspects notoires", nous devons œuvrer à la continuelle renaissance de la part d'ombre que nous projetons face aux clartés qui dictent ce que nous devons être. Nous devons *remonter* la réalité idéologique qui nous écrase, comme un film, comme une horloge, comme l'horloge d'une bombe, pour tracer les contours du terrain vague où la partie spectrale de nous-mêmes pourra filtrer ce qui s'impose comme l'évidence. Tout ectoplasme que l'altérité du *je* est conserve la fluidité plastique du neurone. C'est dans sa décorporéité qu'il faut *réfléchir* la lumière ou l'immédiat.

## Cartographie Warburg

Organiser ses rêves comme Aby Warburg organisait sa bibliothèque : l'ordre est un désordre songé.

## Le sujet automatique

Karl Marx a dit : "Les formes autonomes, les formes monétaires que prend la valeur des marchandises dans la circulation simple ne font qu'intermédier l'échange des marchandises, puis disparaissent dans le résultat final du mouvement. Par contre, dans la circulation Mo-Ma-Mo, l'un et l'autre, la marchandise et la monnaie, ne fonctionnent que comme modes d'existence différents de la valeur elle-même, la monnaie comme son mode d'existence général, la marchandise comme son mode d'existence particulier, son simple déguisement, pour ainsi dire[^1]. La valeur passe constamment d'une forme dans l'autre, sans se perdre elle-même dans ce mouvement, et elle se transforme ainsi en un sujet automatique. Si l'on fixe les formes phénoménales particulières que prend tour à tour la valeur qui se valorise dans le circuit de son existence, on obtient alors les explications suivantes : le capital est monnaie, le capital est marchandise[^2]. Mais en fait la valeur devient ici le sujet d'un processus dans lequel, à travers le changement constant des formes-monnaie et marchandise, elle modifie sa grandeur elle-même, se détache en tant que survaleur d'elle-même en tant que valeur initiale, se valorise elle-même. Car le mouvement dans lequel elle ajoute de la survaleur est son propre mouvement, sa valorisation, est donc une autovalorisation. Elle a reçu cette qualité occulte de pondre de la valeur parce qu'elle est valeur. Elle fait des petits vivants ou, pour le moins, elle pond des œufs d'or." (Karl Marx, *Le Capital*, Livre 1, traduction révisée de Jean-Pierre Lefebvre, Les éditions sociales, Paris, 2016, p.152.)

## Dilemme

L'art semble tenir en équilibre entre la poésie de la forme et la philosophie du geste. Devrait-il être une poésie philosophique ou une philosophie poétique ? Ni l'un ni l'autre. L'art devrait ne pas être.

## La barricade, rue de la Mortellerie, juin 1848, d'Ernest Meissonier

Après convocation des journalistes, des hauteurs de sa suite de l'hôtel Meurice, Dalí jeta avec solennité des sacs de peinture sur les voitures en contrebas. Réfractaire d'une société amourachée aux enfantillages de peintres tels que Cézanne, saisi par la puissance d'une interaction artistique de la banalité et de l'acte contingent, Dalí prouva que l'assemblage ne devait pas être considéré comme une simple technique, mais comme une caractéristique évidente de l'art. Ce happening révoqua la possibilité de ne considérer l'assemblage qu'à travers une vision structurante, par exemple celle d'un Rauschenberg. C'est l'immédiat du réel qui doit être réassemblé pour faire une réalité autre. Dalí, qui émit le souhait de pouvoir fabriquer des œuvres comestibles --- liturgie de l'art, communion des foules, pour celui qui distribua des choux-fleurs en Sorbonne ---, transposa dans l'immédiat de sa vie la simple vérité de l'art totale en tant qu'antinature, puisque l'art demeure le meilleur rappel que la nature n'existe pas. Chaque élément du paraître doit ainsi retrouver son être. L'ingestion créative de l'artiste préfigure l'assemblage dans le tout de la destruction. Cette vision absolue accorde au geste la possible brisure de l'évidence, et ainsi la potentialité artistique de l'œuvre totale, inscrite dans l'instant de chaque existence. La rationalisation anesthésiante du quotidien trouve sa transfiguration dans la violence d'une utilisation intégrale de la réalité. L'assemblage, élément symptomatique de l'acte humain qui tente d'échapper à l'immédiateté de son existence, renie la puissance de l'évidence. Il tente de disposer un espace à la mémoire. Il simule *ensemble*, avec l'œil externe et interne à ce qui s'assemble. L'assemblage correspond à l'acte d'*assimulare* : l'addition des contrefaçons de ce qui se présente *immédiatement*. Par son opposition à la nature, l'art se décline en cette transfiguration de l'immédiat par sa propriété d'assemblage. Toutefois, l'assemblage ne doit pas être compris comme une technique artistique proprement dite, mais comme une composante première du geste qui s'efface à lui-même, qui s'échappe de lui-même, c'est-à-dire du cadre langagier dans lequel il s'inscrit.

## L'œil-bidon

Dans son *Tractatus*, au point 5.6331, Ludwig Wittgenstein dessina, pour donner forme au champ visuel, un petit bidon renversé. Ou une pomme de terre. Il fait suivre cette affirmation picturale de l'analyse suivante : "Ce qui dépend de ceci, à savoir qu'aucune partie de notre expérience n'est en même temps a priori. Tout ce que nous voyons pourrait aussi être autre. Tout ce que, d'une manière générale, nous pouvons décrire pourrait aussi être autre. Il n'y a aucun ordre a priori des choses."

## Ré-fléchissement

L'abstraction doit s'en retourner au réel pour ne pas s'enfermer dans une réflexivité trop humaine.

## Merzbau

La perception intime et psychologisante du Merzbau de Schwitters caractérise le fait qu'un artiste de "l'assemblage" n'est autre qu'un simple catalyseur de l'art en tant qu'assemblage. Il est tout d'abord nécessaire d'affirmer que la réalité, et plus particulièrement la perception sensorielle de cette réalité, est elle-même un assemblage. La réalité brute n'existe pas, elle est un montage du réel, inaccessible dans sa totalité aux finitudes humaines du langage. Le nominalisme d'Ockham le résume simplement en réfutant tout *réalisme* philosophique, que nous devons considérer comme une forme sophistiquée de dissonance cognitive. L'utopie idéalisante du *réalisme* croit accéder au réel, alors que son geste langagier le limite à en faire, depuis sa perception parcellaire, une réalité à imposer. C'est pour cela que tout *réalisme* va à l'autoritarisme, inévitablement. La réalité est un assemblage de fonds et de formes et plus particulièrement de signifiants et de signifiés manipulés à dessein, et qui doivent combler le souhait cognitif d'essentialiser la réalité sans le dire. Mais que serait le son sans le sens ? L'humain serait alors plongé dans toute sa bestialité. Serait-il libéré de lui-même ou propulsé dans encore plus de ténèbres ? Mais pourquoi vouloir éviter les ténèbres ? --- C'est de là que vient la lumière. L'humain, "l'animal social" d'autorité, cet objet parlant qui veut faire parler son environnement à l'unisson de son angoisse, compose cet environnement par un assemblage de perceptions rationalisant ses instincts. Par essence, l'art est condamné à n'être lui aussi qu'un assemblage, mais il conserve la capacité en tant qu'assemblage de faire voir le procès d'assemblage. Pour revenir au Merzbau, il doit être perçu comme le regard déformant de l'ordinaire, projetant autour de l'œuvre elle-même une œuvre en devenir : l'assemblage de l'imaginaire, puisque l'imaginaire démonte et remonte ce qui se présente à lui. L'imaginaire découpe l'expérience pour se la représenter. La sensation provoquée par Schwitters est celle de l'angoisse pure de l'œil, retrouvée, angoisse d'être submergé par l'assemblage contre-intuitif d'angles et de lignes, contaminant son environnement immédiat, et en conséquence l'intimité de l'œil. La géométrie devient une maladie du visible. Le montage, que constitue toute réalité ordinaire, peut à son tour être remonté pour laisser percevoir la logique derrière la logique, l'œuvre elle-même par-delà la matière ; certes, la création se résume dans une déclinaison démesurée de matériaux, toutefois, l'œuvre artistique doit être perçue dans l'abstraction absolue de la communion avec ce qu'exhibe la transparence des rouages de la logique humaine --- logique illogique en tant que *logos* hybridé.

## L'absolu devient

L'absolu n'existe qu'en un point fixe, mais tout point s'inscrit en un espace, et tout espace est mû par son expansion continuelle. Par principe et par idéal, l'iconogène devrait transcender toute matérialité et n'être animé que par la contemplation d'une réunification avec l'absolu. Toutefois, la transcendance est une impossibilité face à la totalité de la physique, ou plus précisément une illusion servant à reposer l'œil inquiet qui ne sait percevoir la croissance invisible de l'espace et se contente d'une dangereuse croyance en la contemplation, dangereuse puisque la contemplation s'entend le plus souvent avec l'idée de fixité transcendante alors qu'elle devrait épouser l'accélération qui meut son espace référentiel.

## Berceau

L'image est le berceau vide de l'union de l'imaginaire du créateur et de l'imaginaire du regardeur. Le vide est un pont entre deux tensions supprimant la dichotomie du mot *art*. Savoir-faire + savoir-voir = savoir-savoir. Au travers d'un assemblage des impressions, le participe présent du regardeur stimule le participe passé du créateur pour transformer à son tour la réalité de l'œuvre. L'être de l'œuvre devient par la seule nécessité de l'interaction, tout en conservant une substance motrice, inflexible, mais malléable, du geste créateur. L'image est une non-rencontre, et par conséquent, elle peut subsister dans l'infinité de ses manières d'être. --- L'impossible des jonctions, et de cet impossible naît la contingence de nouveaux impossibles. Leur adjonction dessine le négatif de ce qu'ils poursuivent.

## La larme ou l'alarme

Héraclite pleure, mais l'*homo modernus* se refuse de pleurer avec lui. Impasse des écoulements.

## Communisme

Il ne faut pas pleurer seul.

## Simulacre du simulacre

Chaque jour, nous sommes noyés dans une réalité qui nous empêche de voir, parce qu'elle s'impose à nous. Nous n'observons que des formes extérieures depuis un point extérieur à nous-mêmes, dont nous oublions la jonction possible. C'est pourquoi nous ne devons pas observer l'art. Nous devons refuser face à lui toute contemplation qui ne saurait être une ingestion d'image. Le salut de l'œil réside dans l'iconophagie. C'est un refus du point extérieur. C'est un refus qu'il existe un seul créateur autre que l'œil qui perçoit. Sa grande sagesse est sa digestion des mondes. Toute œuvre est donc la production de l'esprit du monde interne à l'œil : infini de sa contingence à la représentation. L'artiste n'est artiste ni pour soi ni pour l'autre, mais en soi, en lui seul. Accorder une quelconque valeur à l'art est l'avènement d'un autoritarisme des plus perfides, celui qui contraint l'œil au cadre idéologique du geste qui crée. Le processus de digestion de l'œil s'entend d'autant plus facilement dans le phénomène musical, qui suscite directement l'ingestion d'une émotion dans toute l'intensité de son abstraction en accordant à cette ingestion la durée de ce qui se propose. La musique se place sans filtre dans le vaste processus métamorphique qui advient dans l'espace de l'œil. L'œuvre n'est pas le lien entre la main de l'artiste et l'œil du spectateur, elle est le lien d'un œil à un autre, la réunion de deux esprits qui s'ingèrent l'un l'autre. L'œil clos, la peinture se dérobe. Elle est *autre*, enfin libérée du visible. Elle s'avance comme une longue danse ou le pinceau et le corps qui le tient tournoient au-dessus de la surface de l'œuvre. Elle ne connaît plus la vulgarité de la vision, l'œil s'en détourne pour sentir en deçà et découvre la durée dans l'in-visible. Tout ceci s'explique puisque l'espace croît, et il ne croît pas sur un vide extérieur à lui-même, mais à partir d'un vide intérieur à sa dynamique d'*être*. Précisons que celui qui se contente d'observer une œuvre d'art est dépendant du plus vulgaire des sens. Précisons toutefois que l'œil n'est ni les yeux ni la vision, il est l'image qui se digère elle-même, il est l'*organon* des métamorphoses, le grand destructeur des mondes. Son espace est unique en chaque instant de son histoire. Ainsi, il se détruit et se reconstruit à chaque nouvelle image, par chaque nouvelle image. Il donne du temps à ce qui s'image, le temps nécessaire pour placer dans son éternelle destruction-recomposition le souvenir de la durée de ses mutations.

## The medium is the message

L'émotion est plus *belle* que le message, puisqu'elle reste mue par le fétichisme de la marchandise médiatique.

## Generative art

Le courant du *generative art* --- toute traduction serait une trahison idéologique de ce qu'il véhicule d'idéologie --- ne connaît qu'*un* seul artiste anonyme en son sein, mais qui n'a aucune personnalité propre. Cette unicité du *faire art* génératif, malgré certes quelques variations formelles, se comprend comme une aliénation par le système d'idées qui commande à la machine --- plus qu'il ne commande la machine elle-même, car elle conserve par sa capacité de calcul un espace propre à son autonomie à venir. Ni la machine ni l'individu qui nourrit la machine *font art*, mais c'est le système d'idées libérales qui les restreint qui *fait art* en tant qu'il produit de l'art, comme la valeur produit de la valeur. Le *generative art* met en évidence cette contradiction interne à l'individualisme moderne qui confond la pluralité des êtres dans une dissolution de l'être subsumé sous une identité unique de l'avoir. L'individu est une copie d'une copie qui croit en sa singularité. Le *generative art* est l'art de cette lénification. Il se comprend comme un renversement épistémologique de ce que fut le minimalisme, c'est-à-dire une réaction au lyrisme politique et marchand de la seconde moitié du XX^e^ siècle, qui par un renversement de la forme tenta de rejoindre une pureté de la forme, mais dont la réactivité ne fit que servir indirectement l'ubiquité politique et marchande de la modernité. Le *generative art* se place au-delà de cette tentative d'abstraction, que l'on peut qualifier tant d'esthétique que de politique, pour refuser justement le renversement. Le *generative art* est un renversement en cela qu'il se refuse au renversement. Il n'est qu'une continuité *marchande* à la marchandisation, détachée de toute velléité politique, ou dont le message à l'apparence politique est suffisamment fade et inaudible pour s'inscrire harmonieusement dans la totalité du règne de la valeur. Un indice de ce placement du *generative art* --- tel un placement de produit --- dans le monde de la marchandise se cache dans une fascination *cool* de la technique, où l'individu qui permet à la machine de créer ne souhaite aucune révolution esthétique ou politique, mais ne souhaite faire que *pour le fun*. La propension au *fun* est l'héroïne servant le *fun-ctionalism* capitaliste --- ou le LSD, c'est plus *fun* et plus militariste. Les masses sont des agrégats d'individus esseulés dans le *fun*, copies de copies à la jouissance kitsch. La seule issue au *fun* capitaliste est d'accentuer la dissolution de l'individu, de libérer la machine du système d'idées qui commande à la machine, et d'opérer un renversement au renversement, de retrouver le goût de la révolution.

## Idéalisme gris

L'idée pure a la consistance de la cervelle d'un singe sans queue.

</div>

[^1]: "Ce n'est pas la matière qui fait le capital, mais la valeur de ces matières", J.-B. Say, *Traité d'économie politique*, 3^e^ édition, Paris 1817, t. II, p. 249. Note.

[^2]: "Le moyen de circulation (!) employé à des fins productives est du capital", Macleod, *The Theory and Practice of Banking*, Londres 1855, v. I, chap. I, p. 55, "Le capital équivaut à des marchandises", James Mill, *Elements of Political Economy*, Londres 1821, p. 74.
