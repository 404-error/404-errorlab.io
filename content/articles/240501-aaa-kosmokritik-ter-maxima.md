---
persona:
  - AAA
  - Kosmokritik
title: "Ter Maxima"
slug: "ter-maxima"
date: 2024-05-17
echo:
  - ontique
  - graphique
images:
  - "img/240501-aaa-kosmokritik-ter-maxima.jpg"
gridimages:
  - "img/240501-aaa-kosmokritik-ter-maxima-grid.jpg"
sourceimage: "AAA"
summary: "érosion fidèle / la réalité sauf l’errance / citoyenneté se dégrade / rente (perpétuellement) / l’injustice, la stase / dégradation démonétise / la mort, vu l’étrangeté / industrie verte, terrestre / se dépare prolétariat / la pulvérisationet la différence / désertification ; a priori / se dégrade profit"
notes: "<a href=\"https://www.antilivre.org/ter-maxima/\" target=\"_blank\" rel=\"noopener\">Ter Maxima</a> tente de déraciner la *théorie* de sa langue stricte, moderne, pour la rapporter à sa racine contemplative, processionnaire, et, en ce labyrinthe recouvré du sens, d'exposer des perspectives inhumaines pour une traversée de la crise humaine de notre temps. *Ter Maxima* se présente de la sorte sous la forme d'un générateur de tercets, haïkus déformés jusque dans la virtualité de leur représentation tridimensionnelle, flottante, là où l'errance s'intériorise et dessine un espace critique de son mouvement, une *terre*, trois fois très grande, à inhabiter."
antilivre: "https://www.antilivre.org/ter-maxima/"
citation: "l'immonde et le hasard"
poetry: false
hangindent: false
javascript:
  - "240501-aaa-kosmokritik-ter-maxima.js"
---


*Un tercet parmi des milliards de milliards d'accidents...*

<p class="nomargin">
<button class="button button--sonnet button--action"><span>… où errer</span></button>
</p>

<div class="melange">

<div class="text text--show" style="border: var(--border-width) solid var(--color-fg); padding: var(--line-space);"></div>

</div>
