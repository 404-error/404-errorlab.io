---
persona:
  - Nicolas Vermeulin
title: "Rage Culture"
slug: "rage-culture"
date: 2021-03-01
echo:
  - rythmique
images:
  - "img/210301-vermeulin-rage-culture.jpg"
gridimages:
  - "img/210301-vermeulin-rage-culture-grid.jpg"
sourceimage: "Nicolas Vermeulin"
notes: ""
summary: "Imaginez, quand nous écouterons la voix de ces nouvelles, et se rappelant ces objets d’une terreur volontaire, que toute armée et que toute police sont dissoutes, s’infiltrent alors les cris entre les pierres. Haletant, tremblant encore de colère, songez à cela aussi, le vide du ποιέω. Oui. C’est possible, ajouta-t-il en penchant la tête vers une maison inachevée, glissant sa main lentement vers les fondations."
citation: "Prisonnier de la guerre d’un autre"
---

<div class="two-columns two-columns--shift">

<p>
Imaginez, quand nous écouterons la voix de ces nouvelles, et se rappelant ces
objets d’une terreur volontaire, que toute armée et que toute police sont
dissoutes, s’infiltrent alors les cris entre les pierres. Haletant, tremblant
encore de colère, songez à cela aussi, le vide du ποιέω. Oui. C’est possible,
ajouta-t-il en penchant la tête vers une maison inachevée, glissant sa main
lentement vers les fondations.
</p>

<p>
Supposez que la terre a tremblé... supposez. Balles, obus et éclats
allaient briller ne serait-ce qu’un instant, mais soudain là, fiché là
dans ma chair, au milieu de mon ventre s’écoulait l’éclat du soleil
couchant. Songez surtout que personne ne devait oser en parler ; que cet
écoulement ne ferait pas marée, et que croyez-vous, ne serait-ce qu’une
lueur, je m’y attache, et cette course la faire durer jusqu’à ce
qu’elle en vînt à me quitter. Je m’y attache, vous dis-je, tant qu’une
seule, même minuscule, je m’y attache.
</p>

<p>
Accoutumé à camper sur le bord de la société. Renoncer à nous battre et
ne plus donner matière à nous comparer, et ne plus être bornages au sol
de cette culture et auprès desquels j’ai vu vos lèvres remuer, habiter
dans des grottes ou des cairns oubliés, et on me dit qu’il faut demander
pardon, et pourquoi le faut-il après tout ?
</p>

<p>
Comme les mots, même si effacés, illisibles, même si un jour il n’en
reste plus qu’un, il comportera dans sa chair, dans sa matrice
l’ensemble de tous les mots connus et inconnus. Je m’y attache, je ne
sais pas quel mot restera le dernier, le dernier à sortir, peu importe,
et sûrement pas les bras levés. Et il sera suffisant pour attraper un
morceau de charbon, de l’entendre se consumer, à vous, autres joueurs
de dés !
</p>

<p class="dialogue">
&mdash; Ayez-en une connaissance exacte, de ces faces, mais sachez que l’on
    ne regarde point que le haut, mais nous habitants des cairns oubliés
    regardons toutes les faces sauf une, la vôtre.
</p>

<p>
Malheur sur moi n’est que bonheur, j’absorbe, aujourd’hui bien du temps
perdu que vous nommez productivité, efficience. Pourtant que les crânes
célèbres, conservés comme les joyaux de pierres précieuses ruisselaient
de sciences et sur les tables garnies pourrissaient. Vous étiez trop
lisses, trop fades.
</p>

<p>
Aspirez-les, ces purs esprits ! À l’homme à qui l’on implore dans la
même intention sa peur et ses craintes. Cessez donc de nous aimer, nous
n’en valons pas la peine. Aux créateurs des possibles, rien n’est
impossible. Et en eux-mêmes ne voient pas toujours les meilleurs ennemis
du monde. Regardez-les donc ces ombres du matin, ne les repoussez pas !
</p>

<p>
Ulysse et ses matelots, comment furent-ils enrôlés, par classes, par
forces, par soumissions, par origines ? Prisonnier de la guerre d’un
autre ; vous deviez fuir, déserter, le laisser finalement seul et voyez
au final que l’on n’a retenu ni vos formes ni vos noms. Il ne reste de
vous qu’un sillage, des seconds rôles flous.
</p>

<p>
Resterait alors comme une ombre. Mais que reste-t-il de ça, que ces
jours passaient en lisant du soir au matin.
</p>

<p class="dialogue">
&mdash; Dirons-nous à leurs ossements, levez-vous et suivez-nous ? Devais-tu
    me donner toute ta jeunesse, cette insouciance d’inexpérience, et tu
    répondais d’une façon particulière que pour ressentir il faut
    toucher, cette viande, sentir l’odeur du sang et des profits,
    malaxer cette chair à canon.
</p>

<p>
Saluez-vous les uns les autres, s’étendant du nord au midi, mer létale,
sec de vent, affamé de vagues. Que disiez-vous ?
</p>

<p class="dialogue">
&mdash; Laisse-moi, car je dois apprendre à travailler dans une entreprise
    qui n’est mienne et dont la réussite repose sur un rapport de force.
</p>

<p>
Tout entretien d’embauche devrait commencer par la recherche des motifs
pour lesquels l’on s’enchaîne. 
</p>

<p>
Ne pas terminer, se lever. Après notre long voyage, ceux qui restèrent
partirent dans les grottes. Suivi du bruit des écluses fracassées, les
argonautes dans un dernier assaut eussent disparu à l’horizon. Tenter
cet inconnu, l’ouragan des balles s’était perdu au loin, les éclats
retombaient ; chairs, lambeaux, os, que sais-je, firent pour un régime
discret par délégation des nourritures exquises.
</p>

<p>
Nombreuses sont les actions passées dont nous ayons quelque mémoire et
ce goût dans la bouche qui n’est nôtre. Ruisselant de sueur, à casser du
caillou, il essaya d’avaler quelques bouchées de ces statues brisées,
sans les preuves historiques que nous possédons nous n’aurions pas eu
trace de ces prisons.
</p>

<p>
Et pourtant ils les hachèrent, les rangeant dans des tunnels profonds,
dans des caves sombres, ils effacèrent de nos mémoires ces massacres de
nos différences. Ce autrement ne fut qu’un parfum lointain, réprimé à
chaque fois qu’une créativité le faisait remonter. À coups de matraque,
gaz, il était repoussé dans le tunnel. Ils avaient détruit toutes les
fondations, chaque semaine de multiples thèses s’opposaient, sapant
lentement l’humain en nous, se nourrissant des violences pour générer la
violence qui nourrissait à son tour la violence. Une démocratie qui
refusait ce autrement était-elle une démocratie ?
</p>

<p>
Envieux nous étions et à n’en point murmurer et de recevoir à l’épaule
ce sac de grains, et je, enfin tu, nous... Déplorables débuts dans la
carrière soit le temps imparti. Réellement mon mot a de l’espoir que
peut-être cet homme, si étonnant soit-il, y trouverait un moyen de
rupture si désiré. Mais que dois-je à ce nom que je ne porte pas ?
Vingt-quatre heures plus tôt, il était près de midi lorsque l’on a pour
ainsi dire extrait sa sensibilité. Quelques-uns apportaient avec eux le
froid du matin.
</p>

<p>
Cadeaux mains vides. Plairait-il encore à monsieur de la ville pour
échapper aux poursuites d’une vie insipide, de partir. N’oublions pas
que toutes les précautions que l’on doit au roi sont de le
déshabiller...<br>
Accordons ce nombre, c’est pourquoi je suis ici pour me consoler, je
disais l’autre jour à cause de toi, je corrige aujourd’hui par, de moi.
Se consoler de soi-même, de lui-même, de ce multiple intérieur de notre
nous, et l’autre cela fait longtemps que je l’ai recraché. Tu disais ?
</p>

<p>
Accoutumé à débrouiller des chaos, il a découvert sa demeure, il l’a
creusée, et l’use par le fond de culotte. Accumulant ordinateurs et tout
ce qui tient à la révolution ; mais il ne veut pas que cela change il
veut que cela s’arrête.
</p>

<p class="dialogue">
&mdash; Rattachez ma robe, et donnez-moi un joli poignard. Crois-moi, pour
    toi et par toi seul que je ne pourrai jamais l’oublier, d’essayer
    les clefs les unes après les autres.
</p>

<p>
Maintenant, on comprend sa stupéfaction, sa rage.<br>
Pourvu que l’homme ici sans culture ne soit qu’un Roi nu.
</p>

<p class="dialogue">
&mdash; Il était sur moi, suivait ma piste, l’air portait mon parfum plus
    que d’habitude, et là à l’angle d’une rue sa silhouette morte, je
    ne sais si cela fut moi ou sera moi qui de mon poignard ouvrit son
    cœur en deux.
</p>

<p>
Analysons ce raisonnement, le bing bang n’est pas encore intervenu, mais
quand cela se produira sa puissance sera telle que le temps se
recourbera sur lui-même et nous l’imaginerons comme un possible. Je ne
parvins plus à me relever !<br>
Plutôt que de leurs raisonnements linéaires ne sont que de fugitives
caresses, transports et soupirs venus de fort loin au milieu de cette
poussière, je vis en cela la vraie noblesse de sa course mais aussi sa
ruine.
</p>

<p>
Appuyé sur ses mains croisées s’étaient rouvertes les plaies, à tendre
et retendre les cordes, les voiles. Imagine-toi, c’est fini, là,
maintenant : donne-moi une poignée de voyous ou d’invisibles, lesquels
s’expriment vite, tissent, enroulent et serrent. Il est étonnant qu’une
même matière serve à deux opposés.
</p>

<p>
Mal, murmura-t-il, si vous en avez encore. Quatre ou cinq contemporains
d’une longue et odieuse année à vous porter. Étourdi que vous êtes, par
les brises, complètement cinglé d’embruns, mais ça n’alla pas plus
loin... Arrêtés devant un fast-food à lécher les vitres.
</p>

<p class="dialogue">
&mdash; Prenez-moi avec vous ; je vois que tu veux réaliser tes rêves,
    illusions, tu ne cherches qu’à le remplacer par un tour de
    passe-passe. Tu les porteras les morts ? Papier gras, huiles
    saturées. Puissent comme toutes les autres choses et le raisonnement
    même où l’idée d’un espoir n’est pas comme le fruit criminel d’une
    terre ; ils s’assèchent.
</p>

<p>
Tel était du moins là un abri, et évitez autant que vous me refusez...
Vous souhaitez savoir les noms des voyageurs descendus à l’étage.
</p>

<p class="dialogue">
&mdash; Rassure-toi, j’y mettrai celui qui m’avait coûté vingt ans de
    mépris et d’isolement.
</p>

<p>
Sollicité par tant d’hommages, mais bientôt il fut évident qu’on avait
monté la tête de deux cents livres. Quitte à renoncer à sa liberté, elle
a des quais disposés pour les bagnards et l’attaque répétée, mer
déchaînée rouleaux et lames s’écrasent depuis mon enfance.
</p>

<p>
Miséricorde vous-même, attaché, vraiment, si vous voulez le savoir ? Par
les pieds, et vous pourrirez, comme tout, ni plus, ni moins. Vois-tu, ce
ne peut pas y avoir de l’argent dans ses cheveux flottants, et je pris
la fuite, et après mains vides cœur léger et ventre de pierre.
</p>

<p>
Cherchez bien, vous découvrez ces nœuds et cordages, je vous prierai
seulement de mettre par écrit ces tracés et de les pousser à l’horizon.
Vous n’y êtes pour rien, enfin si peu, peut-être déjà beaucoup. Vous
disiez ?
</p>

<p class="dialogue">
&mdash; Éloignons-nous, il est plein de domaines riches et jardins nocturnes
    et, surtout, l’air et les manières se nourrissent de ces éclats.
</p>

<p>
Choisissez surtout ce que savait l’aveugle, vous dînerez avec moi et le
fou. Nous ne formons qu’un je. Parfaitement, il s’agit bien de ces
histoires de la mer où les gens coulent, où les crabes pullulent, où un
petit prince attend au fond, sa planète.
</p>

<p>
«&nbsp;Réduire l’unité du pur raisonnement est une protestation contre
l’autorité&nbsp;», disait-il. Des fosses communes le confirment. Écolier
exécrable, il s’imaginait n’en conserver sur lui aucune trace. Il
refusa de faire allégeance, et cela fut terminé ainsi. Il poussa
l’histoire dans les tunnels profonds, émietta les utopies en fines
lamelles, et apprit à aboyer.
</p>

<p>
Rivés à leur folie généreuse, aux grâces sans fin, ils attendaient dos
tournés. Réservez vos foudroyants anathèmes pour ces «&nbsp;être-là&nbsp;», les
lisses à la vie, la lie des bureaux.
</p>

<p>
Promettez-moi de l’interroger à nouveau sur ses ombres, celles qui se
cabraient vives et prêtes à en découdre, au milieu desquelles se
trouvent ses idées dont il faisait librement la censure pour ne pas vous
choquer. Il arrêta un jour après un petit matin, laissant sa lueur s’en
aller.
</p>

<p class="dialogue">
&mdash; Et alors ! Vous aimait-il ?
</p>

</div>
