---
persona:
  - Baudelaire, Champfleury, Toubin
title: "Le Salut public"
slug: "le-salut-public"
date: 2020-12-17
echo:
  - historique
  - stratégique
images:
  - "img/201217-le-salut-public.jpg"
gridimages:
  - "img/201217-le-salut-public-grid.jpg"
sourceimage: "https://commons.wikimedia.org/wiki/File:Barricade_(Le_Salut_public)_Courbet.jpg"
notes: "*Le Salut public* fut un journal révolutionnaire fondé par Charles Baudelaire, Jules Champfleury et Charles Toubin en février 1848 à la suite de la Révolution française de 1848. Il ne connut que deux numéros. Le dessin, qui servit de vignette au journal, fut réalisé par Gustave Courbet."
summary: "VIVE LA RÉPUBLIQUE ! AU PEUPLE. On disait au Peuple : défie-toi. Aujourd’hui il faut dire au Peuple : aie confiance dans le gouvernement. Peuple ! Tu es là, toujours présent, et ton gouvernement ne peut pas commettre de faute. Surveille-le, mais enveloppe-le de ton amour. Ton gouvernement est ton fils. On dit au Peuple : gare les conspirateurs, les modérés, les rétrogrades ! Sans doute il faut veiller, les temps sont chargés de nuages, quoique l'aurore ait été resplendissante."
citation: "Tout par les peuples ! Tout pour les peuples !"
---

# Premier numéro

VIVE LA RÉPUBLIQUE !

AU PEUPLE.

On disait au Peuple : défie-toi.

Aujourd’hui il faut dire au Peuple : aie confiance dans le gouvernement.

Peuple ! Tu es là, toujours présent, et ton gouvernement ne peut pas commettre de faute. Surveille-le, mais enveloppe-le de ton amour. Ton gouvernement est ton fils.

On dit au Peuple : gare les conspirateurs, les modérés, les rétrogrades ! Sans doute il faut veiller, les temps sont chargés de nuages, quoique l'aurore ait été resplendissante. Mais que le Peuple sache bien ceci, que le meilleur remède aux conspirations de tout genre est LA FOI ABSOLUE dans la République, et que toute intention hostile est inévitablement étouffée dans une atmosphère d’amour universel.

## Aux chefs du gouvernement provisoire

Honneur à vous qui avez pris l’initiative et l’embarras des premiers jours.

Le peuple a confiance en vous. Ayez confiance en lui !

La confiance réciproque sauvera tout. Honte à qui n’est pas bon républicain ! il n’est pas de ce siècle ! Honte à qui se défie. Il est donc faible !

Soyez grands, soyez forts dans le gouvernement, et ne doutez jamais de l’intelligence du peuple qui vous suit.

Il aime ceux qui l’aiment. Ne craignez donc rien.

Ne faites jamais un pas en arrière. Marchez plutôt comme le vent. Nous savons maintenant que les heures sont des années.

Honneur donc à vous qui avez pris sur vos épaules le rude poids des premières journées ! Vous tenez l’Europe entre vos mains. Nous savons que vous serez dignes de votre tâche. Car une commune expérience qui nous a été léguée par nos pères, nous enseigne que HORS DE L’ASSEMBLÉE NATIONALE, IL N’Y A POINT DE SALUT !

Et enfin, ce grand remède une fois appliqué par vos soins sur nos longues souffrances, déposant votre haute magistrature, vous emporterez le souvenir d’une grande action et la pieuse reconnaissance de tous, qui est l’unique *décoration* et l’unique récompense digne des grands citoyens.

## Les étoiles filent, et les réputations aussi

Deux hommes sont bien bas à cette heure, les sieurs Thiers et Odilon Barrot.

Le premier a toujours été un singe plein de malice, riant, criant, gesticulant, sautant, ne croyant à rien, écrivant sur tout.

Ne croyant pas à la Révolution, il a écrit la Révolution.

Ne croyant pas à l’Empire, il a écrit l’Empire.

Savez-vous ce qu’il aimait ?

Les singes. Il leur a fait bâtir un palais.

Le second était son compère, un homme sérieux, une contrefaçon de tribun ; il avait toute la gravité d’un montreur d’ours, le sieur Barrot ; toute sa vie il l’a passée à montrer un singe. Pendant dix ans la France a cru à un grand orateur, au sieur Barrot.

Il est vrai qu’il entrait à l’ex-chambre des députés avec une provision de mots plein ses poches.

Dans la poche droite il mettait : *Mon pays, mon patriotisme*. Dans la poche gauche, *honneur et vertu*. (Sa famille touchait *cent trente mille francs* de places.)

{{%asterisme%}}

La Garde nationale est ivre de joie ; elle accueille partout avec enthousiasme les cris de : Vive la République ! C’est un fait accompli ; il n’y a plus que des républicains en France.

## Le 24 février

Le 24 février est le plus grand jour de l’humanité ! C’est du 24 février que les générations futures dateront l’avènement définitif, irrévocable, du droit de la souveraineté populaire. Après trois mille ans d’esclavage, le droit vient enfin de faire son entrée dans le monde, et la rage des tyrans ne prévaudra pas contre lui. Peuple français, sois fier de toi-même ; tu es le rédempteur de l’humanité.

Ayez à vos ordres quatre-vingt mille baïonnettes et des caissons par milliers, et des canons mèche allumée, si vous avez contre vous le droit et la volonté du Peuple, vous êtes un gouvernement perdu, et je ne vous donne pas vingt-quatre heures pour décamper. Voilà ce que le 24 Février vient d’enseigner au monde. Désormais toute nation qui demeurera esclave, c’est qu’elle ne sera pas digne d’être libre : avis aux Peuples opprimés !

## Les presses mécaniques

Quelques frères égarés ont brisé des presses mécaniques. Vous cassez les outils de la Révolution. Avec la liberté de presse, il y aurait vingt fois plus de presses mécaniques qu’il n’y aurait peut-être pas encore assez de bras pour les faire fonctionner.

Toute mécanique est sacrée comme un objet d’art.

L’intelligence nous a été donnée pour nous sauver.

Toute mécanique ou tout produit de l’intelligence ne fait du mal qu’administré par un gouvernement infâme.

Les autres ouvriers ont protesté, entre autres les rédacteurs du journal l’*Atelier*. Nous attendions cela d’eux.

## La reine d’Espagne a la colique

On dit même qu’à cette heure elle ne l’a plus.

Si quelques soupçons disaient juste, ce ne serait qu’une preuve nouvelle que le crime lui-même sert les bonnes causes.

Allons, Espagne ! vite à l’œuvre !

## Trois mots sur trois gouvernements

Depuis soixante ans, la France allait en fait de gouvernements de mal en pis. Napoléon lui avait donné un despotisme oint de suie de poudre, mais scintillant de gloire ; la France lui pardonna. La Restauration lui avait ramené le privilège et les coups de cravache des gentilshommes ; mais elle était franche d’allures et sans hypocrisie ; quelques domestiques fidèles la suivirent sur la terre d’exil. L’infâme gouvernement qui vient de tomber voulut tenter sur la nation l’astuce, l’hypocrisie, la cupidité et toutes les basses passions ; un croc-en-jambe du Peuple a suffi pour le jeter dans la boue.

## Un mot de l’ex-roi

Quand ça commençait à chauffer, l’ex-roi riait en sournois et disait en se frottant les mains : "moi aussi j’aurai ma journée des dupes !" --- Quand on démolissait Charles X, il chassait gaiement à Saint-Cloud. Toujours le même esprit de vertige et d’erreur ! Sont-ils si décrépits, ces pauvres rois, que l’aveuglement soit chez eux maladie héréditaire ?

## La République française et l’Europe

Les traités de 1815 viennent pour la seconde fois depuis dix-sept ans d’être lacérés par l’épée du Peuple français. Proclamons haut, bien haut, ces trois grands principes de politique républicaine.

Plus de conquêtes ! Les conquêtes sont un attentat contre les droits des peuples, et tôt ou tard les nations soumises réagissent contre leurs conquérants.

La République française s’assimilera dans la limite de ses frontières naturelles les provinces qui se donneront à elle LIBREMENT ET SPONTANÉMENT. En dehors de ses frontières naturelles, qui sont le Rhin et les Alpes, elle renonce solennellement à posséder jamais un pouce de terrain.

La France prend sous sa protection tous les peuples opprimés par un gouvernement tyrannique, étranger ou indigène, mais elle ne tirera son épée que pour défendre les principes et les institutions révolutionnaires.

Au dedans, la devise de la République française est : Tout par le peuple ! Tout pour le peuple !

Au dehors : Tout par les peuples ! Tout pour les peuples !

## Bon sens du peuple

Il y a des hommes qui sont pleins de phrases toutes faites, de mots convenus et d’épithètes creuses comme leur tête. --- Le sieur Odilon Barrot, par exemple.

Quand on leur parle de 89, ces gens vous disent, c’est Voltaire qui a fait la Révolution ; ou bien c’est Rousseau qui a fait la Révolution ; ou bien c’est Beaumarchais qui a fait la Révolution.

Imbéciles ! Niais ! Doubles sots !

Michelet l’a dit : "La Révolution de 89 a été faite par le peuple." Là, Michelet avait raison.

Le peuple n’aime pas les gens d’esprit ! Et il donnerait tous les Voltaire et les Beaumarchais du monde pour une vieille culotte.

Ce qui le prouve, aux Tuileries rien n’a été saccagé comme sculpture et peinture que l’image de l’ex-roi et celle de Bugeaud ; un seul buste a été jeté par les fenêtres !… Le buste de Voltaire !

## Respect aux arts et à l’industrie

Un brave citoyen s’est porté hier à Meudon pour avertir le commandant de la garde nationale Amanton de protéger les objets d’arts contre les envahissements de la garde qui devait, dit-on, se porter sur le château de l’ex-Roi. Le gouvernement provisoire a dû délivrer une sauvegarde.

Ne cessons pas de le répéter ; respect aux objets d’arts et d’industrie, et à tous les produits de l’intelligence !

## La beauté du peuple

Depuis trois jours la population de Paris est admirable de beauté physique. Les veilles et la fatigue affaissent les corps ; mais le sentiment des droits reconquis les redresse et fait porter haut toutes les têtes. Les physionomies sont illuminées d’enthousiasme et de fierté républicaine. Ils voulaient, les infâmes, faire la bourgeoisie à leur image, --- tout estomac et tout ventre, --- pendant que le Peuple geignait la faim. Peuple et bourgeoisie ont secoué du corps de la France cette vermine de corruption et d’immoralité ! Qui veut voir des hommes beaux, des hommes de six pieds, qu’il vienne en France ! Un homme libre, quel qu’il soit, est plus beau que le marbre, et il n’y a pas de nain qui ne vaille un géant quand il porte le front haut et qu’il a le sentiment de ses droits de citoyen dans le cœur.

## Le constitutionnel est scandalisé !

*Le Constitutionnel* se résigne ; c’est bien de sa part ; c’est généreux. *Le Constitutionnel* promet d’être bon citoyen.

Odilon Barrot, la grosse poupée de carton, et Thiers, ce singe de foire, pardonnent au Peuple de n’avoir pas voulu se laisser voler. Que pense le Peuple de leur pardon ?

## Les artistes républicains

Les peintres se sont bravement jetés dans la Révolution ; ils ont combattu dans les rangs du Peuple.

À l’Hôtel-de-Ville des artistes portaient sur leurs chapeaux, écrit en lettres de sang, le titre d’ARTISTES RÉPUBLICAINS ; deux d’entre eux sont montés sur une table et ont harangué le Peuple.

On parlait d’une manifestation qui devait se produire au Louvre contre l’Académie de peinture qui, depuis dix-huit ans, a bu tant de larmes, a tué tant de jeunes talents par la faim et la misère. Mais les sots vieillards, architectes, musiciens, arpenteurs et géomètres sont à bas aujourd’hui.

Ne leur donnons pas le coup de pied de l’âne.

## Réouverture des théâtres

Les théâtres rouvrent.

Nous avons assez des tragédies ; il ne faut pas croire que des vers de douze pieds constituent le patriotisme ; ce qui convenait à la première révolution ne nous suffit plus.

Les intelligences ont grandi. Plus de tragédies, plus d’histoire romaine. Ne sommes-nous pas plus grands aujourd’hui que *Brutus*, etc. ?

## Bonnes nouvelles

L’ex-roi et sa famille voguent vers l’Angleterre. Ils y sont sans doute arrivés. Que le Peuple n’ait pas peur, l’Angleterre n’osera rien pour *le dernier des Bourbons*.

--- Pour de bon, les rois s’en vont ! Léopold est en fuite. La Belgique s’est proclamée française.

--- On voulait intimider le citoyen Rotschild et le faire fuir : comme si le Peuple souverain volait des écus. Il ne prend que ses droits. Rotschild a répondu : "J’ai confiance dans le nouveau Gouvernement et je reste." Bravo !

--- Une Assemblée nationale sera convoquée aussitôt que le Gouvernement provisoire aura réglé les mesures d’ordre et de police nécessaires POUR LE VOTE DE TOUS LES CITOYENS.

--- La République française est proclamée à Dijon.

--- Honneur a Pie IX ! Voici de grandes paroles qu’il a prononcées récemment : "Ce sont les édifices anciens qui ont besoin de fondements nouveaux."

--- Hier, deux prêtres enjambaient une barricade ; des hommes du Peuple les insultent ; un plus grand nombre les défend. Cette haute raison du Peuple est merveilleuse.

--- Plus beau encore. On trouve dans la chapelle des Tuileries un remarquable Christ en bois. Quelqu’un s’écrie : C’est notre maître ! chapeau bas ! --- Tout le monde se découvre et on porte le christ en triomphe à Saint-Roch.

Décidément la Révolution de 1848 sera plus grande que celle de 1789 ; d’ailleurs elle commence où l’autre a fini.

VIVE LA RÉPUBLIQUE !

# Deuxième numéro

VIVE LA RÉPUBLIQUE !

*Les rédacteurs-propriétaires du* SALUT PUBLIC*,* Champfleury, Baudelaire, *et* Toubin *ont retardé à dessein l’envoi du journal à leurs abonnés, afin de faire graver une vignette qui servira à distinguer leur feuille d’une autre qui s’est emparée du même titre.*

## Les châtiments de dieu

L’ex-roi se promène.

Il va de peuple en peuple, de ville en ville.

Il passe la mer ; --- au-delà de la mer, le peuple bouillonne, la République fermente sourdement.

Plus loin, plus loin, au-delà de l’Océan, la République !

Il rabat sur l’Espagne, --- la République circule dans l’air, et enivre les poumons, comme un parfum.

Où reposer cette tête maudite ?

À Rome ?… Le Saint-Père ne bénit plus les tyrans.

Tout au plus pourrait-il lui donner l’absolution. Mais l’ex-roi s’en moque. Il ne croit ni à Dieu, ni à Diable.

Un verre de Johannisberg pour rafraîchir le gosier altéré du Juif errant de la Royauté !… Metternich n’a pas le temps. Il a bien assez d’affaires sur les bras ; il faut intercepter toutes les lettres, tous les journaux, toutes les dépêches. Et d’ailleurs, entre despotes, il y a peu de fraternité. Qu’est-ce qu’un despote sans couronne ?

L’ex-roi va toujours de peuple en peuple, de ville en ville.

Toujours et toujours, vive la République ! vive la Liberté ! des hymnes ! des cris ! des pleurs de joie !

Il court de toutes ses forces pour arriver à temps quelque part avant la République, pour y reposer sa tête, c’est là son rêve. Car la terre entière n’est plus pour lui qu’un cauchemar qui l’enveloppe. Mais à peine touche-t-il aux barrières, que les cloches se mettent gaiement en branle, et sonnent la République à ses oreilles éperdues.

La tête de Louis-Philippe attire la République comme les paratonnerres servent à décharger le Ciel.

Il marchera longtemps encore, c’est là son châtiment. Il faut qu’il visite le monde, le monde républicain, qui n’a pas le temps de penser à lui.

## Aux prêtres

Au dernier siècle, la loyauté et l’Église dormaient fraternellement dans la même fange, quand la révolution fondit sur elles et les mit en lambeaux.

--- Inconvénient des mauvaises compagnies, se dit l’Église ; on ne m’y reprendra plus.

L’Église a eu raison. Les rois, quoi qu’ils fassent, sont toujours rois, et le meilleur ne vaut pas mieux que ses ministres.

Prêtres, n’hésitez pas : jetez-vous hardiment dans les bras du peuple. Vous vous régénérerez à son contact ; il vous respecte ; il vous aimera. Jésus-Christ, votre maître, est aussi le nôtre ; il était avec nous aux barricades, et c’est par lui, par lui seul que nous avons vaincu. Jésus-Christ est le fondateur de toutes les républiques modernes ; quiconque en doute n’a pas lu l’Évangile. Prêtres, ralliez-vous hardiment à nous ; Affre et Lacordaire vous en ont donné l’exemple. Nous avons le même Dieu : pourquoi deux autels ?

## Ce pauvre Metternich

La France est République.

La Suisse est République, vraie République depuis quatre mois.

L’Angleterre, l’Espagne et la Belgique sont à la veille d’être Républiques.

L’Autriche, monstre à trois têtes, disparaîtra de la carte. La République Allemande prendra sa tête allemande ; la République Italienne prendra sa tête Italienne, la République Polonaise --- une bonne celle-là ! --- prendra sa tête slave. Qui de trois ôte trois, reste ce pauvre M. Metternich, qui ne mourra pas dans son lit.

Il y a donc une justice divine !

## Des mœurs ou tout est perdu

Des mœurs, des mœurs, il nous faut des mœurs ! Régénérer les institutions, très-bien, mais régénérons aussi les mœurs, sans lesquelles il n’y a pas d’institutions. Le nom de Républicain est beau et glorieux, mais plus il est glorieux, plus il est difficile à porter. Effaçons donc de nos cœurs tous les instincts avilissants, toutes les passions abjectes que l’impur gouvernement de Louis-Philippe a cherché à y faire germer. La vertu est le principe vivifiant, la conservatrice des républiques.

La Convention avait mis la vertu à l’ordre du jour.

## L’ami du peuple de 1848

Le citoyen Raspail, médecin comme Marat, et comme lui médecin malheureux et plein de disputes, fait comme lui l’*Ami du Peuple*. Les deux premiers numéros sentent le Marat d’une lieue. Même défiance, même talent, même ferveur ! --- Mais est-il bien temps ? Ces défiances accusées déjà si nettement ont leur danger. Toutes les nominations seront révisées, et il ne faut pas semer la peur.

Le citoyen Raspail, comme son illustre chef de file, est un parfait honnête homme, et il a le droit d’être très-sévère ; nous adjurons seulement le citoyen Raspail de ne pas encore user de son droit.

De grâce, de grâce, ne préjugeons rien contre le gouvernement. Surveillons-le sévèrement et que les millions d’yeux de la Nation soient nuit et jour braqués sur lui ; mais ne troublons pas son action par des défiances prématurées. S’il ne va pas droit, haro ! s’il va droit, bravo ! dans un cas comme dans l’autre, ne le jugeons que sur ses actes, il y va du salut public. Les accusations de tendances, laissons-les à l’immoral gouvernement que nous venons de jeter à bas ; elles sont indignes de Républicains. Des hommes de 93, ne prenons que leur foi ardente à la République et leur admirable dévouement à la patrie ; surtout ne recommençons ni Marat, ni Chabot, ni aucun de ces infatigables flaireurs de mauvaises intentions. C’est ainsi seulement que nous préserverons notre jeune République des mille périls qui menacent son berceau.

## Le journal conservateur de la République

Il faut rendre justice à qui de droit, maintenant que nous avons le temps.

Le citoyen Girardin se conduit admirablement. Au milieu du trouble, du désordre qui envahissent momentanément toutes choses publiques et particulières, le journal du citoyen Girardin est mieux fait que jamais. Cette habileté connue, cette aptitude rapide et universelle, cette énergie excessive, tout cela tourne au profit de la République.

Tous les jours les questions importantes et actuelles sont mâchées dans *la Presse*.

Le citoyen Girardin prend pour devise : UNE IDÉE PAR JOUR !

Son journal, jusqu’à présent, dit ce que tout le monde pense.

Lundi le citoyen Girardin a été le premier au rendez-vous sur la tombe d’Armand Carrel.

## La curée

Indignation ! Nous venons des ministères, de l’Hôtel-de-Ville et de la préfecture de police : les corridors sont remplis de mendiants de place. On les reconnaît à la bassesse de leurs figures empreintes de servilisme.

Non, ce ne sont pas là des Républicains ; un Républicain s’attache à mériter les emplois et ne s’inquiète pas de les obtenir. Les pavés de nos rues sont encore rouges du sang de nos pères morts pour la liberté ; laissons, laissons au moins à leurs ombres généreuses un instant d’illusion sur nos vertus. Encore si ces insatiables dévoreurs de la République avaient combattu avec nous pour son triomphe ; mais celui qui gravit si lestement l’escalier d’un ministre, celui-là, soyez-en sûrs, n’était pas aux barricades.

Patience ! Nous vous arracherons le masque, hommes infâmes ; vous ne jouirez pas longtemps du prix de vos bassesses.

## La première et la dernière

En 89, l’éducation morale du peuple était nulle ou à peu près. --- Aujourd’hui le peuple connaît et pratique ses devoirs à faire honte à bien des ex-nobles et à bien des bourgeois.

En 89, la noblesse et le clergé combattirent avec fureur la révolution. --- Aujourd’hui, jusqu’à fait contraire, il n’y a que des républicains en France.

En 89, une fraction de la nation émigra et prit les armes contre la République. --- Aujourd’hui personne n’émigre, pas même le sieur Thiers, dont la République se passerait cependant bien volontiers.

En 89, la société était rationaliste et matérialiste. --- Aujourd’hui elle est foncièrement spiritualiste et chrétienne.

Voilà pourquoi 93 fut sanglant. --- Voilà pourquoi 1848 sera moral, humain et miséricordieux.

{{%asterisme%}}

Il y avait en Allemagne au duché de quatre sous, grand comme la main, qui s’appelait le duché de Cobourg-Gotha. C’était pour ainsi dire un haras royal, une écurie de *beaux* hommes, tous taillés en tambours-majors qui étaient destinés aux princesses de l’Europe.

Maintenant qu’il n’y a plus de princesses, à quoi vont s’occuper ces hommes entiers ?

## Sifflons sur le reste

Sous l’ex-roi, il y avait une *pairie*, c’est dire des vieillards impotents pleins de serments, et de rhumatismes.

Il n’y a plus de pairie : sifflons sur le reste !

Sous l’ex-roi, il y avait des soldats barbares, ivres de sang, les *municipaux* dont la joie était de *descendre* un homme du peuple.

Il n’y a plus de municipaux : sifflons sur le reste !

Sous l’ex-roi, il y avait un *cens électoral* ; moyennant 500 francs un imbécile avait le droit de parler à la chambre ; moyennant 200 francs un bourgeois avait le droit de se faire représenter par un imbécile.

Il n’y a plus de cens : sifflons sur le reste !

Sous l’ex-roi, il y avait un *timbre* ; une petite gravure large comme un sou qui empêchait les citoyens intelligents d’éclairer leurs frères.

Il n’y a plus de timbre : sifflons sur le reste !

Sous l’ex-roi, il y avait un *impôt sur le sel* qui empêchait la fertilisation des terres, qui enrayait les socs de charrues.

Il n’y a plus d’impôt sur le sel : sifflons sur le reste !

Sous l’ex-roi, il y avait des tas de *foutriquets*, une légion de *ventrus*, des armées de *bornes* ; tous puisaient à pleines mains dans le coffre des fonds secrets et s’enrichissaient aux dépens du peuple.

Il n’y a plus de foutriquets, il n’y a plus de ventrus, il n’y a plus de bornes que celles des rues.

Sifflons sur le reste !

{{%asterisme%}}

--- L’Odéon représenta quelque temps avant la Révolution le *Dernier Figaro*, du sieur Lesguillon. Cet auteur de bas étage fit une pièce contre-révolutionnaire ; sous l’ex-roi il en avait le droit ; d’ailleurs la censure n’eût pas permis de montrer les hommes de 89 à 93 sous leur vrai jour. Mais aujourd’hui il est question de remonter cette misérable pièce avec des replâtrages républicains.

Les Écoles qui ont sifflé et resifflé le Figaro révolutionnaire ne doivent pas davantage laisser revenir Figaro avec ses bandages, ses compresses, ses béquilles républicaines.

Le peuple saurait bien se conduire si le citoyen Alexandre Dumas tentait de *républicaniser* son immorale pièce des *Girondins*.

--- Le sieur Châtel a fait four. Personne ne veut entendre parler de son Église française. Voyez-vous, du reste, le lendemain de la prise des Tuileries, le religionnaire idiot qui croit qu’on a le temps de penser à ses messes en mauvais français !

Le peuple a lui-même déchiré toutes les proclamations et placards de ce nigaud de primat des Gaules.

--- Quelqu’un court dans le quartier latin pour récolter des signatures au bas d’une pétition à cette fin de garder le sieur Orfila à la Faculté.

Ce vendeur de perlimpinpin, ce chanteur bouffon se sent donc destitué ; il est donc coupable.

En toute matière de ce genre, prenons garde à l’indulgence !

--- À bientôt la reprise, au Théâtre de la République, du *Roi s’amuse*, une des grandes œuvres du citoyen Victor Hugo. Il faut que le théâtre de la Porte-Saint-Martin reprenne au plus vite et l’*auberge des Adrets*, et *Robert-Macaire*, et surtout cette belle pièce de *Vautrin* de notre grand romancier, le citoyen Balzac.

On parle de jouer *Pinto*. À quoi bon s’ennuyer pendant trois heures pour entendre crier : *à bas Philippe* ! allusion très-significative sous l’ex-roi, mais sans portée aujourd’hui.

Que les citoyens ne croient pas aux dames Hermance Lesguillon, aux sieurs Barthélemy, Jean Journel et autres qui chantent la République en vers exécrables.

L’empereur Néron avait la louable habitude de faire rassembler dans un Cirque tous les mauvais poètes et de les faire fouetter cruellement.
