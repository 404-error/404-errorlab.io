---
persona:
  - Bernard Bourrit
title: "Apories vénitiennes"
slug: "apories-venitiennes"
date: 2024-02-05
echo:
  - esthétique
images:
  - "img/240201-bourrit-apories-venitiennes.jpg"
gridimages:
  - "img/240201-bourrit-apories-venitiennes-grid.jpg"
sourceimage: "Bernard Bourrit"
notes: ""
summary: "Il y a des années, je suis parti à Venise avec la vague volonté de voir l'emplacement d'où Denis Roche avait pris les images jumelles de la pancarte indiquant la direction de la tombe d'Ezra Pound. Deux photos, visuellement très pauvres, au cadrage presque identiques composées à presque sept ans d'intervalle. Deux photos conçues comme une entreprise plutôt naïve, admet-il, de manifester dans le médium photographique la présence de la durée que par principe ce médium n'incorpore pas."
citation: "réfléchir sur la vie en tant que processus d'extinction"
poetry: false
hangindent: false
---

## 1

Il y a des années, je suis parti à Venise avec la vague volonté de
voir l'emplacement d'où Denis Roche avait pris les images jumelles de
la pancarte indiquant la direction de la tombe d'Ezra Pound. Deux
photos, visuellement très pauvres, au cadrage presque identiques
composées à presque sept ans d'intervalle. Deux photos conçues comme
une entreprise plutôt naïve, admet-il, de manifester dans le médium
photographique la présence de la durée que par principe ce médium
n'incorpore pas.

La définition donnée de l'amour par le photographe, au départ en tout
cas, avait guidé mon projet vénitien parce qu'elle décrivait, en le
réduisant à sa simple évocation, un état passif-extatique dont
l'étrange énoncé grammatical, son flottement, paraissait contenir
l'amène recette de l'expérience photographique : *regarder l'être aimé
être l'être aimé*.

Au départ, c'était ça. Je désirais faire l'expérience d'un lieu à
travers le regard d'un autre. Partir en quête de cet endroit qu'aucun
relevé n'indique, où sur place il n'y aurait rien à voir non plus,
sinon mentalement peut-être, mais dont la particularité était d'ouvrir
un certain rapport imaginaire à l'indication fléchée balisant le
chemin menant à la tombe de Pound, et, par la répétition produite
aussi bien que dans le suspens de ce retour, au temps et à la mort. Je
voulais arpenter l'île San Michele à la recherche de ce point zéro, et
tenter en quelque sorte d'y installer mon regard.

## 2

Je me rappelle m'être alors souvenu, un peu avant cette excursion, de
ce que disait le réalisateur de *Vacances prolongées*, qu'au cinéma,
pour prévenir l'impatience du spectateur et aussi soulager son ennui,
le personnage voyage systématiquement sans fatigue grâce aux ellipses
qu'admet l'illusion narrative. Joignant le geste à la parole, il
baisse à cet instant-là son caméscope de poing vers le sol, l'image se
brouille, on n'entend plus ensuite que le souffle crissant du cinéaste
ripant sur les éboulis quelque part au fond de l'Himalaya. Sa
respiration devient le centre de l'image. Commence alors une
singulière épreuve cinématographique puisque la durée du film se fond,
durant un long laps de temps, dans le présent de son visionnage.

Je me suis souvenu aussi que le premier plan de ce même film
consistait en une séquence qui montre sur fond noir deux tasses
émaillées couleur chair empilées de manière bancale. Celle qui se
trouve au-dessus oscille légèrement. La cause de ce ballottement ? On
l'ignore. Elle bat comme un petit métronome, la tasse, puis ralentit
jusqu'à arrêt complet. Cela, sous plusieurs angles, suivis d'un gros
plan sur le reflet nacré, presque organique, du fond de la coupe. Pour
moi, l'intention ne laisse aucun doute : il s'agit de saisir au plus
près le processus de l'extinction --- extinction du mouvement,
extinction du temps, autrement dit la mort à l'œuvre ; et de filmer
cet amenuisement comme s'il était possible d'assister à son dernier
instant.

Mon projet de ratisser le cimetière San Michele à la découverte de ce
lieu-témoin choisi au-delà de toute motivation esthétique pour devenir
quelque chose comme le signe arbitraire de la durée, une indication
sans autre qualité que celle de figurer la flèche du temps, était lié,
je crois, à la résurgence spontanée de ces bribes de film, aussi
fidèlement que le temps à la mort. C'était un désir sans objet précis,
une idée de circonstance, une sollicitation simple, sourde, insistante
cependant, au-devant de quoi j'ai fini par aller.

## 3

Dans la répétition de la photo vénitienne, l'écoulement du temps
s'illustre par quelques détails négligeables qui viennent s'ajouter ou
se soustraire à l'image-témoin, dérisoires différences que je me suis
amusé à identifier d'après la description du photographe. Ici, du
lierre qui part à l'assaut du poteau indicateur, des arbres abattus à
l'arrière-plan, rien d'autre, dit Denis Roche. Rien, en effet, qui
saurait donner la mesure de presque sept années écoulées, excepté la
légende, les deux dates qui renvoient fatalement aux inscriptions des
pierres tombales : 1975-1981. Sept saisons, c'est le temps nécessaire
au lierre pour conquérir un territoire vierge, mais aussi le peu qu'il
faut à l'antique, au fier cyprès pour dépérir. Les tombes sont sur la
photo, bien sûr, puisqu'on est dans un cimetière, mais aussi bien il
faut comprendre que la photo est une tombe. Je veux dire, pas
seulement une image qui montre une flèche indiquant la tombe, mais
dans une réitération propre au geste artistique, cette indication
même.

Méditer sur la photographie revient toujours d'une manière ou d'une
autre à réfléchir sur la vie en tant que processus d'extinction. Je ne
sais plus où un auteur romantique allemand recourt à une image
inattendue pour suggérer la nature de ce processus que l'esprit ne
peut s'appliquer à lui-même : il le compare à une aiguille pointue. Je
ne sais même plus si ce sont ses mots, mais l'analogie était saugrenue :
comment un objet, statique, pourrait-il symboliser une dynamique, un
progrès vers la fin ?

À la réflexion, je pense qu'il a pu vouloir dire que la vie, comme une
épingle, va s'amincissant jusqu'à son extrémité où elle finit par
cesser tout à fait. La pensée est certainement incapable de se
représenter le fait de l'être qui cesse, et cette métaphore, comme
celle de la tasse, lui substitue une vision symbolique, apaisée. C'est
peut-être pour ça que j'ai voulu retrouver ce lieu, pour essayer de
refaire la même prise, pour avoir la même vision.

## 4

Quelque temps avant de partir, j'ai eu un rêve, un rêve banal, commun.
C'est curieux comme un récit anodin peut déclencher parfois une
sensation très profondément déstabilisante. Voici : tu arrives chez
toi devant la porte de ton logis. Pourtant tu ne la reconnais pas.
Cette porte te paraît étrangère, mais en même temps elle t'est
familière. Puis, en te tournant, tu découvres une autre porte que tu
n'avais jamais encore vue. Elle ressemble à ta porte, d'ailleurs c'est
peut-être la tienne. Mais à cet instant tu n'es plus, mais plus du
tout sûr, de ce qui va se trouver derrière.

Qu'est-ce qu'un récit anodin ? me suis-je demandé d'ailleurs. Un
récit, peut-être, dont la structure d'attente fait fi de l'idée de
progression, un récit où comme dans le travail onirique la
distribution de l'information n'est pas calculée. Plus simplement, une
histoire qui ne se laisse pas raconter.

Rappelle-toi par exemple le film *Mulholland Drive*. Attablé dans une
enseigne de restauration rapide, le personnage confie à son acolyte
qu'il a rêvé du restaurant même où il est assis en train de raconter
son rêve (pas exactement le même, en fait, tout était identique sauf
la lumière ; et un homme qui était là, se tenant devant lui, à la
place du caméraman donc, un homme au visage inconnu, un homme horrible
et effrayant). Puis le personnage sort, et l'homme hideux apparaît
devant lui, en vrai, créant un pénible malaise sans suspense ni effet
superflu. Il est évident que le trouble de cette apparition naît de
l'ignorance du niveau de réalité où se trouve le spectateur, du
glissement entre ces niveaux. Ou encore, mais cela revient au même, de
l'incapacité à savoir ce qui est réel, à établir ce qui est vrai. Elle
est très difficile à raconter, cette scène, parce que l'émotion
qu'elle provoque (se dire que ça ne peut pas arriver alors que c'est
en train d'arriver) n'est pas *matière à narration*. Pour narrer, il
faut insérer le récit dans une structure d'attente. À rebours, les
songes ou les peurs intimes ne se racontent qu'au péril de leur
intelligibilité.

Je crois que ce jour-là, par surprise, j'ai entrevu le motif qui
deviendrait plus tard le cœur de mon voyage : les deux photographies,
les deux portes qui ouvraient peut-être sur deux niveaux de réalité.
Et, comment ne pas y penser, la possibilité de déléguer mon récit à un
autre qui ne serait pas exactement moi-même.

## 5

Au départ, c'était ça. Aller à Venise et retrouver ce lieu, et dans ce
lieu retrouver la flèche indiquant la tombe, et me dire devant ce lieu
voici la flèche du temps, voici le Temps lui-même, et me dire : Je
suis ici pour cela.

Je rapproche la photo vénitienne d'un autre doublet photographique,
trouvé celui-là dans un grenier, parmi un lot d'images datant de la
fin du dix-neuvième siècle découvert par hasard. J'ai essayé
autrefois, en commentant ces images, de montrer en quoi la vie, telle
qu'elle apparaissait dans le viseur de ces nantis, heureux possesseurs
d'un "boîtier de mélancolie", était conforme à la conformité
bourgeoise qui tend toujours à naturaliser sa propre perception du
monde ; comment ils représentaient les espaces sauvages, indéfrichés,
hostiles, de la montagne à la manière d'un jardin ou d'une propriété
privée --- pré carré dans lequel on ne s'allongeait, ne se vautrait
jamais assez.

Avec du recul, je considère que j'ai manqué de vigueur à restituer
l'horreur que m'inspire ce point de vue, que j'ai manqué de colère
aussi. Mais ce n'était pas si simple. La construction de la normalité
reste un continent inexploré, un continent invisible, et les outils
pour l'appréhender n'ont pas encore été inventés.

En l'occurrence, la difficulté venait de l'ambivalence de ces images,
émouvantes, forcément, comme le serait n'importe quelle manifestation
inattendue en provenance de l'au-delà, mais en même temps violentes,
terriblement choquantes, puisqu'elles organisaient, de manière presque
transparente, une fois déployées à la façon d'un éventail, le récit de
l'appropriation bourgeoise de l'espace. Je sais que je n'en aurai
jamais fini avec ces images, avec ce que disent ces images. Car elles
contiennent le principe imaginaire (je ne dis pas : idéologique) d'un
rapport au monde placide, simple, jamais inquiet, jamais inquiété,
"harmonieux" aurait dit l'auteur de *Mars*, c'est-à-dire d'un rapport
négateur de la vie, un principe mortifère.

C'est aussi pour ça, pour me confronter à ça, que je suis allé à
Venise.

## 6

L'auteur de *Mars* est sans conteste celui qui a poussé le plus loin,
du point de vue de la vie, l'examen de conséquences du mode
d'existence bourgeois en disséquant son propre processus
d'anéantissement. Pourtant, aujourd'hui encore, son livre, écrit au
prix de sa vie, fait figure de météore, et sa lecture reste
confidentielle (même retraduit). Il n'en demeure pas moins que
l'apport essentiel de son ouvrage réside dans la description du
sentiment aristocratique conféré par la grande aisance matérielle dans
ses liens avec la physiologie, avec la façon dont cette hauteur qui
entend se donner pour universelle impose à la vie de rentrer en
elle-même jusqu'à extinction.

Donc parmi les clichés retrouvés, il y avait un doublon. Chose rare,
inédite, que ces deux prises à quelques moments d'écart effectuées
depuis le même point de vue, si on la rapporte aux conditions
matérielles, techniques et symboliques, de l'époque considérée. La
répétition témoigne, au minimum, de la volonté de réussir cette image,
de réussir la composition de cette image, et donc l'image que
véhiculera cette composition, elle témoigne d'un souci de soi et de la
mise en scène de soi, à mettre sur le même plan que le souci, pour ces
gens, de réussir dans la vie plutôt que de réussir leur vie. Pour
aller un peu plus loin, je dirais que la vie, pour eux, s'organise du
point de vue de sa représentation, c'est-à-dire que la représentation
est, pour eux, un principe supérieur à la vie, laquelle ne peut
s'exprimer que sous le rapport, et dans la mesure, où elle accepte
d'être vue, c'est-à-dire d'entrer dans une structure.

## 7

Je voudrais à présent te décrire cette image double pour tenter
d'épingler ce que trahit, ce qui s'ouvre dans ce bégayement. Il y a
d'abord ça avec évidence : trois hommes, trois alpinistes, alignés
devant une cabane, un chalet. La neige est partout aveuglante. Les
hommes, les deux premiers, jeunes, chemise, gilet, veston et chapeau à
large bord, avec, pour l'un, à la main ce qui semble être un cigare
fin ; le troisième plus âgé. Tous portent des guêtres retenues par une
simple ficelle, un équipement léger que nous jugerions inadapté à
l'environnement de haute montagne que le décor, les piolets et les
cordes roulées, nattées, font deviner. Des lunettes de soleil
semblables à nos lunettes de natation sont relevées sur leur chapeau.
Les deux images ont été prises à quelques dizaines de secondes
d'intervalle, aussi rapidement que la technique d'alors permettait
d'enchaîner deux prises.

Cette photo est un trophée, n'en doute point. Ni plus ni moins que la
corne de rhinocéros que ces deux mêmes auraient tenu, en d'autres
circonstances, à rapporter. Vois-les serrer vigoureusement le piolet
dans leur main droite, fléchir le genou, chercher des yeux l'objectif.
D'une prise à l'autre, les regards se réajustent ; des sourires, des
expressions s'essaient.

Mais voilà le détail qui me frappe. Le troisième homme, je suppose que
c'est le guide : différence d'âge, de tenue vestimentaire, d'attitude.
Il ne prend pas la pose, il a le regard vide, la main molle. Cette
nécessité de mettre en structure le récit de soi, il ne l'éprouve pas.
Recru de fatigue, d'avoir guidé, repéré les obstacles, contourné les
crevasses, interprété les crissements des séracs, veillé à la sécurité
de ces vies confiées à lui, il revient, las d'avoir promené ces deux
braves aux airs ravis.

## 8

Et voici ce qui me frappe : dans l'intervalle, lui ne bouge pas. Là où
les deux promeneurs s'offrent une seconde chance de faire passer leur
grande aventure à la postérité, reconfigurent les traits de leur
visage ; le guide, le montagnard, l'ancien, l'enfant du pays,
indifférent à ces raisons ou simplement trop las, trop épuisé, reste
immobile.

Cela en dit long sur cette emprise, sur sa puissance, sur l'envergure
de cette construction imaginaire plaquée sur l'épiderme des choses et
sur l'ahurissante dépense psychique qu'il en coûte à l'Homme aux
sentiments aristocratiques pour donner consistance à la fiction qu'il
habite, sur l'étendue de l'hallucination collective qui, seule,
explique que ces gens puissent voir dans le chaos des jours un cosmos
heureux.

En réalité, la naturalité de ce mode d'existence (par naturalité,
entends le fait de trouver normal, naturel, évident, d'évoluer dans un
monde de sa façon), se paie au prix d'une double négation : négation
de la vie et négation de soi. D'une part, les moyens de produire ce
style de vie et de la maintenir constamment à un tel niveau de plaisir
(ou d'indécence si on les regarde d'en bas) sont unanimement occultés,
remplacés par un songe total qui réduit le monde à un office dont les
rapports naturels sont des rapports de service. D'autre part,
l'expression de la personne passe très tôt par l'acquisition d'une
structure d'expression qui met l'intimité, le frémissement des nerfs,
les émois fragiles encore à peine déclos, au secret dans le tombeau du
corps, à l'écart à tout jamais de tout énoncé qui pourrait les dire et
qui les transforme ainsi lentement mais sûrement en noirs essaims de
frelons.

## 9

Alors je me suis demandé en relisant *Mars*, si c'était le fait d'une
certaine nature de recevoir si mal les préceptes de l'éducation
grand-bourgeoise qui réussit si bien partout ailleurs. On est
désormais habitué à penser que la classe dominante, occupée à
maintenir l'étendue de ses privilèges, se complaît dans l'entre-soi,
qu'elle travaille activement à la reproduction de son capital
symbolique, etc. Tout ça a été quadrillé par la sociologie, essoré,
pillé, archi su. Quand bien même, cela n'explique pas pourquoi son
ambitieux programme bute contre certains individus. Pourquoi
échoue-t-il à propulser quelques-uns de ses rejetons dans le tant
convoité jardin des délices où tout n'est qu'ordre, chance et légèreté ?
Pourquoi les broie-t-il, ceux-là, dans de longues souffrances ?

Trois des frères de Wittgenstein ont mis fin à leur jour.

La maladie de l'écrivain zurichois aussi est un suicide.

Quoique considérable assurément, le nombre de ces âmes tourmentées
mais trop éduquées pour faire de leur suicide un coup d'éclat, une
action mémorable et enfin digne d'eux-mêmes, fuyant, méprisant comme
une coquetterie cette ultime occasion d'attirer le regard sur leur
détresse abyssale, chose parfaitement conforme, d'ailleurs, et c'est
l'aspect le plus triste, à l'opinion qui leur fut inculquée ; quoi
qu'il en soit, leur nombre reste et restera toujours marginal par
rapport à l'ensemble des cas de réussite engendrés par l'instruction
libérale de ces familles adossées à une fortune colossale. Dans cette
perspective de la mise en orbite des générations nouvelles, ces
suicides sont simplement considérés comme des ratés de la mécanique.
Des rebuts d'exploitation.

## 10

Imagine maintenant (rêve banal, commun) que tu cries sans parvenir à
émettre le moindre son ou bien que tu hurles à t'en déchirer les
poumons au beau milieu d'une assistance indifférente. L'un ou l'autre.
C'est indifférent. L'un et l'autre, si tu veux. C'est égal. Personne
ne t'entend, de toute façon. Oui, imagine ça.

Imagine-le un moment.

Nous pataugeons maintenant en plein cauchemar. Et ce cauchemar, à ceci
près qu'il ne ruse pas avec les niveaux de réalité, est au fond peu
différent de celui de *Mulholland Drive*. On peut même avancer l'idée
qu'ils partagent deux aspects, qu'ils se ressemblent par contiguïté :
la difficulté à intégrer une structure narrative et l'émotion centrale
qui les constitue. Distinct de la peur et de ses déclinaisons, le
sentiment que ça (l'événement-noyau du cauchemar, son point
d'articulation, son pli) ne peut pas advenir --- alors même que ça
advient --- est une émotion qui relève d'une désarmante incrédulité.

Je crois que la puissance horrifiante de ces songes s'explique par le
fait, notable, que c'est la candeur enfantine ou si tu préfères, la
part intacte du dormeur, lieu préservé des corruptions adultes et des
avanies, son point le plus vulnérable qui se retrouve martyrisé par
les épouvantables images qu'il a, comble de l'incompréhension,
lui-même sécrétées.

Ordre des choses, harmonie des relations sociales, structure des
récits : c'est sur chaque strate le minimum de stabilité requis pour
les opérations mentales quotidiennes, ce petit coin propre censé tenir
à l'écart sa vie intérieure. Un petit carreau ripoliné qui protège
l'homme vigile de ses propres cauchemars comme une vitre derrière
laquelle s'agitent des insectes fourchus.

Dans le cauchemar, la vitre cède ; en réalité cela arrive aussi.

## 11

Lorsque je lis le Horla avec mes élèves, j'essaie de les rendre
attentifs à la dimension cauchemardesque de la nouvelle. À vrai dire,
aucun des raisonnements formulés par le narrateur n'est irrecevable
philosophiquement parlant, au contraire ils sont admirablement
chevillés. La réalité excède nos sens : il y a bien du réel en dehors
de ce que nous percevons là, en un mot --- le *hors là*. Alors pourquoi
pas des êtres ? Tout ceci est rigoureusement compatible avec l'état de
nos connaissances. Seulement, pour préserver l'esprit de sa propre
imagination, c'est-à-dire pour ne pas verser dans le délire, il faut
reléguer le songe de cette omniprésence derrière une vitre, le plus
loin possible.

Cette idée d'ordre, d'harmonie, de structure qui se retrouve à toutes
les échelles est absolument distincte de celle, grecque, de cosmos.
Pour les Anciens, la régularité de l'univers, son contour, était le
fruit de lois naturelles, la traduction visible d'une perfection
invisible. *A contrario*, l'architecture dont je parle est une
production collective purement conventionnelle destinée à
cartographier le territoire de la normalité en sédimentant à la
manière d'une carapace, peut-être, un havre de tranquillité où tout
s'imbrique, s'emboîte comme dans une horloge merveilleuse ; une
architecture dont je ne peux démontrer positivement l'existence, mais
que je perçois à travers la multiplicité d'actions, de gestes, de
paroles qui exigent de prendre forme pour advenir ou simplement avoir
le droit d'être considérés.

Voici ce que je pense : la pensée de ce qui nous excède nous paralyse,
tandis que l'excès rend fou. Coincé entre les deux, il y a un
minuscule creux habitable : trou de roche où s'accroche l'unique
crampon des coraux, coque vide où s'enroule l'abdomen d'un crabe mou.
C'est là que logent ceux qui risquent de mettre à nu leur regard,
comme Denis Roche, ceux qui prennent le risque de l'inintelligibilité,
comme Johan van der Keuken, le risque de demeurer inaudible. J'émets
donc l'hypothèse que tous les Zorn inconnus furent de même
sensibilité, mais qu'à la différence du vrai, ils ne purent, tant leur
déroute fut cinglante, trouver refuge dans la zone provisoirement
habitable d'un texte, d'une image, et, qu'ainsi, privés de tout moyen
d'expression, en proie à ce même désarroi, acculés à la même
impuissance, ils tombèrent sous les coups de leur propre fureur.

## 12

Est-ce autre chose que cela, la fureur ? Autre chose qu'un
vouloir-dire dépossédé d'un pouvoir-dire ? N'est-ce pas ça : crier à
tue-tête au milieu d'une assistance muette ?

La fureur ne doit pas être confondue avec la colère avec laquelle elle
n'a que l'irritation en partage ; elle enfle, s'amplifie, veut
déborder, se fortifie dans son creux, comble le moindre de ses
interstices ; puis au moment précis où elle atteint son paroxysme,
incapable de débonder ou de s'épancher, contentionnée, la voilà qui
entre dans une nouvelle phase, passe dans un nouvel état et prend la
dimension si particulière de ce cri muet dont je t'ai parlé,
remplissant l'espace de son silence assourdissant ; et le désir de
mort dont elle s'était initialement nourrie éclate enfin, crève comme
une inoffensive bulle translucide sur le miroir d'une eau lisse.

Je me rappelle alors m'être tu un instant, peut-être le temps de
regarder par la fenêtre si le taxi était en bas, et qu'ainsi Hélène se
trouva dans mon dos. Hélène qui ne disait rien. Hélène qui baissait
les yeux dans le demi-jour de l'appartement sans savoir que faire de
ce départ ni de quoi meubler cette attente. Et la valise à mes côtés
devenant subitement le symbole du reproche qu'elle s'interdisait de
m'adresser.

## 13

J'étais sur le point de partir à Venise à la recherche de ce "je ne
sais trop quoi", fallacieusement guidé par l'espoir de retrouver ce
lieu qui n'était rien, ni pour moi ni pour personne. Je laissais
Hélène derrière moi. Je disais, plutôt je lui avais dit que je
désirais partir à la rencontre de ce signe, de ce troublant totem pour
m'en approcher aussi près que possible comme si cette flèche n'avait
pas seulement acquis par le montage fulgurant du photographe la force
d'un *memento mori*, mais qu'elle était devenue le nœud du temps, son
commencement et sa fin. J'avais dit que j'espérais remettre là, ma vie
à zéro. Même si cela signifiait détruire l'espoir : car cela
signifiait aussi reprendre espoir.

J'en étais déjà venu à penser quelque temps plus tôt que si ce panneau
indicateur avait pris une place aussi exorbitante dans mon puzzle
intérieur, assez exorbitante en tous cas pour que je lui accorde par
anticipation ce pouvoir d'absolution qui aurait dû me permettre --- je
le croyais vraiment --- de reprendre ma vie comme un dé pour la
relancer encore une fois, c'est qu'il ouvrait dans l'image une série
de reflets emboîtés à l'infini pareils, peut-être, à ceux de deux
glaces réfléchissantes tenues l'une devant l'autre. Prises dans la
répétition d'un temps dont elles se veulent l'épure, les deux photos
font naître, repliées l'une sur l'autre et non plus disposées côte à
côte, une bifurcation en jeux de vertige permettant, peut-être,
encore, de s'introduire dans l'incroyable jardin aux sentiers qui
bifurquent et d'y trouver le chemin du recommencement. Entrer dans le
foyer de l'image, relancer la flèche ; supposer cela possible.

## 14

J'en étais venu à penser cela à cause d'un bête lapsus, simple
relâchement, distraction passagère de la part de l'auteur dont la
vigilance --- son contrôle de la structure, son emprise sur le récit ---
fut un court instant trompée, une microseconde, au moment où
l'écriture s'emballait parce qu'elle essayait de courir le rythme de
la pensée, écriture qui, par un débord où s'exprimait librement la
pensée, avait entrouvert une porte dérobée, jeté un léger trouble sur
la réalité de ce qui était montré. Roche raconte ainsi la reprise en
1981 de la photo de 1975 (ça va très vite, comme si le souvenir était
restitué par un instinct conquérant, superbement exempt de doute) :
"Je trouvai la pancarte de bois au tournant d'une allée avec ces mots :
EZRA POUND, et une flèche."

Mais voici ce qui avait retenu mon attention : le panneau est là,
l'indication aussi. Les majuscules reproduites en majuscules sont
fidèlement tracées sur la pancarte de bois. Mais voici que Roche
ajoute : "et une flèche". Il l'ajoute avec un "et" signifiant "en
plus". Or, à San Michele, il n'y a pas une pancarte "et" une flèche :
la pancarte *est* la flèche. À lire le récit de Roche, il n'y a donc
pas seulement reprise du même objet dans le même cadre mais ajout dans
l'opération photographique d'un supplément imaginaire introduit lors
de la remémoration de la prise, comme s'il y avait le désir que la
photo produise ce supplément, pas n'importe quel supplément, la forme :
une flèche.

Voilà donc un diptyque qui se donne pour thème de retrouver le même
objet à distance de temps afin de l'envisager sous le même jour ; avec
ceci que l'objet envisagé pour rendre visible la mesure de temps
écoulé est une indication de mort, elle-même symbolisée comme une
flèche symbolisant le temps, et que dans la répétition désirée
s'ajoute au moment du récit de sa production le fantasme de l'objet
représenté, délié de sa représentation : une flèche flottante, vue,
voulue, invisible et pourtant là, comme un gué entre les deux images,
flèche qui transperce la représentation jumelle, ouvrant un passage,
une voie secrète entre deux points du temps, lançant un pont entre les
deux, ou pour dire mieux : matérialisant cet entre-deux, l'écart, le
cours du temps qui connaît à ce moment-là un flottement capable en
puissance d'inverser le signe d'un événement.

## 15

J'arrivais à la conclusion que les deux images jumelles figuraient
deux mondes possibles ; plus exactement deux versions possibles du
même monde entre lesquelles existait un point de passage sous forme
d'une flèche symbolisant le temps. Pour Hélène, qui savait de longue
date que j'inclinais, sitôt que se présentait une amorce de rêverie, à
me couler dedans, à dériver dans sa forme comme si c'était une gondole
glissant le long de canaux fins aux coudes imprévisibles, jusqu'à
finalement me recomposer un être autre que mien, cela, cette capacité
à me mouler dans d'autres élans --- c'était justement *cela* qui était
insupportable à Hélène.

Hélène qui ne supportait pas que je veuille ma vie autrement. Ne le
supportait pas, car elle comprenait : autrement sans elle ; alors que
c'était : autrement sans moi.

En allant à Venise, je ne prétendais pas explorer pas une possibilité
logique. C'eût été, comme parfois le craignait Hélène, un premier
signal de déraison. Je ne voulais pas vérifier s'il était possible,
matériellement possible de déporter le destin. Je n'ai jamais douté
qu'il fût inconcevable, rationnellement, de passer d'une version à
l'autre de l'histoire, de déjuger son passé ou de faire advenir
l'inaccompli.

Je voulais seulement explorer la possibilité sensible d'une
alternative qui semblait ouverte. Saisir, au moins tenter de saisir
cette possibilité qui me tendait les bras. Et, plus j'étais happé par
les jeux de reflets des photos, plus se fortifiait en moi la certitude
illégitime que ce lieu devait devenir un lieu de non-retour.

Qu'avais-je à perdre ? Et ce peu, ne fallait-il pas le perdre
justement ?

## 16

Dans la nouvelle *La Mort à Venise*, le personnage du compositeur
Gustav von Aschenbach est la proie d'une fascination morbide, d'une
fascination qui causera lentement, mais insidieusement, sa perte. De
même que Hans Castorp est sidéré par la maladie et la guerre,
Aschenbach est fasciné par Venise et la mort ; toutefois la
fascination de ce dernier, qui ne transparaît qu'au miroir d'une
idéalisation érotique, est une attraction oblique, biaisée : elle
déplace pour mieux s'en accommoder l'authentique foyer de la
séduction. Le soleil ni la mort, répète-t-on *ad libitum*, ne peuvent
se regarder en face ; mais on oublie de rappeler que leur reflet fut
une fois surpris sur le poli d'un bouclier convenablement incliné,
soutenu par un bras audacieux, lui-même porté par l'unique souci de
vaincre ; cette occurrence mythique affleure chaque fois que l'exploit
se réédite à travers le temps. Aussi n'est-il pas exagéré, je crois,
de déclarer que Venise incarne l'image de la mort dans la figure du
désir.

La plage du Lido sur laquelle meurt Aschenbach est bien davantage
qu'une métaphore, c'est l'expression la plus littérale du dernier
rivage, ce point extrême, cette pointe d'aiguille pour reprendre la
suggestion de Moritz (l'auteur romantique dont j'avais oublié le nom),
au-delà de quoi : rien --- l'instant où se confondent le regard et la
perte comme le préfigure si clairement le mythe d'Orphée. L'ultime
vision (ici, la silhouette psychopompe du jeune Tadzio) est une
dilatation du regard s'abolissant. À ce point, le mot "regarder"
coïncide avec le moment de la perte, strictement. Regarder, c'est
perdre ce que l'on voit.

Et songe qu'à l'instant de tes dernières paroles, par une
essentialisation identique, les mots les plus communs, galvaudés parce
que communs et donc creux, deviennent seuls assez poignants pour
signifier quelque chose. Par exemple, dans l'affolante carlingue d'un
avion propulsé à la vitesse du son contre un obstacle prémédité, en un
sursaut de lucidité, une mère qui écrit à ses enfants "je vous aime"
--- et ces mots deviennent le substrat, la substance de toute une vie
latente, interrompue, siphonnant la totalité des gestes affectueux et
des échanges émus de centaines de millions de secondes partagées pour
la condenser, vitrifiée, en un unique diamant, transparent à qui doit
le recevoir.

Mais voilà qui est suffisant à ce sujet.

## 17

Je voulais juste comparer Aschenbach et Zorn sur un point afin de
faire observer ceci : l'artiste hisse la beauté au-dessus de sa propre
vie de telle sorte que, chez le premier, le désir porté à son extrême
se transforme naturellement en motif de mort. Mais chez le second, le
fils de bonne famille, éduqué à obéir et devant apprendre à aimer
cette soumission, ce fils humilié jusque dans ses modestes
revendications de liberté, de geste, de parole, de sentiment, dans la
moindre manifestation de ses goûts ou encore dans la genèse de son
ambition, dans l'attrait de la beauté justement, chez lui, le désir
s'est transformé en motif de persécution. On a si habilement, si
tranquillement joué de son désir qu'il s'est introverti en fléau qui
tance, qui tanne, en inavouables culpabilités qui, pareilles à ces
Érinyes fouettardes qu'aucune prière n'infléchit jamais, ont renversé
contre lui systématiquement et négativement les élans, les
aspirations, les nobles élévations qui peut-être, peut-être seulement,
lui auraient permis de s'extraire de ce nœud-ventouse l'aspirant vers
l'abîme.

Je crois seulement que Zorn aurait voulu supplier les vivants d'être
plus attentifs. Attentifs à sa constitution, à sa si curieuse
disposition intérieure, quoique j'aie encore maintenant de la peine à
discerner en quoi consiste cette spécificité, en quoi Zorn était
différent de l'infinie cohorte de ces semblables. C'est peut-être
d'avoir toujours à faire autrement qu'il ne faisait, d'avoir toujours
à être autrement qu'il n'était, d'être constamment dévié de son
orbite, délogé de son verbe, cela, et non l'austérité de principes
profitables, non plus que l'exercice d'une autorité sans
contradiction, qui l'a poussé dans la fosse.

Je pense juste que Zorn aurait voulu être un homme de verre :
visiblement transparent et fragile. Il n'était pas suffisant que sa
famille le sût mortel ; cassable, elle en eût pris soin davantage.
Mais à dépeindre le jeune homme sous le jour de son intime faiblesse
ne recule-t-on pas l'horizon du problème ? Car ensuite ? Faudrait-il
la supposer de naissance cette faille, ou bien croire qu'elle survint
accidentellement, comme un accroc ? Et si ce fut un accident, pourquoi
advint-il sans fracas, quoique profondément douloureux ?

Et encore : comment fut-il possible qu'un accident devînt si essentiel ?

## 18

Je dirais volontiers que Zorn était travaillé tout entier par cette
tension : la volonté, d'une part, d'en finir avec une sensibilité
exogène qui l'entravait au sein de son propre groupe, soit un effort
toujours accru de dissimulation pour contrer le conformisme ambiant ;
le tropisme, d'autre part, qui lui faisait désirer renouer avec cette
sensibilité pure d'avant la catastrophe, comme si renouant avec cet
antécédent majeur, qui contenait en germe son être vrai (comprends :
celui qu'il aurait dû devenir), il y avait encore moyen d'être fidèle
à lui-même, réconcilié, sauvé peut-être. Voilà sa tenaille à lui : un
mouvement régressif qui l'a poussé à l'effacement de soi et au mutisme ;
un mouvement involutif qui cultivait en secret le mythe de l'unité
perdue, de cet autre soi-même, plus vrai, plus authentique, plus
invraisemblablement beau --- préservé de la souillure.

Ce dont il est question au fond, c'est de cette impuissance à grandir
qui ne le quitta pas. De son sous-développement affectif. Par exemple,
l'impossibilité d'aimer, que Zorn tenait pour la conséquence suprême
de son malheur, était liée sans aucun doute au stade infantile (honte
du corps, peur de l'autre, rejet du plaisir) où il s'était englué pour
survivre dans cette société adulte qui lui avait assigné
implicitement, sans le verbaliser jamais, le rôle de l'enfant sage,
fidèle aux valeurs dynastiques. Paralysé dans son repli, travaillant à
se faire oublier autant qu'à s'oublier soi-même, il espérait avec un
discernement tout relatif qu'une âme aimante, à condition d'être
vraiment pure, aurait eu l'audace de descendre dans les limbes où il
fermentait pour le tirer de là et dissiper son cauchemar. Naïvement
sans doute, il voyait dans l'amour sa planche de salut. À vrai dire,
quoi d'autre ?

Mais l'ange ne vint pas.

Et s'il était venu ? demandes-tu. S'il était venu, Hélène, alors Zorn
aurait pu disposer d'un monde et il aurait été capable pour ce monde
de forger des promesses, et ainsi de s'arrimer à un avenir plus, un
peu plus, glorieux. Ça ne l'aurait pas apaisé. Ça ne l'aurait pas
réconcilié. Contrairement à ce qu'il imaginait. Mais ça l'aurait doté
d'une certaine force. D'une certaine endurance, oui.

## 19

Le temps reversait à Zorn jour après jour le même présent. Il vivait
dans le repli, à l'abri de la violence dans un coin obscur de sa chair ;
et pourtant il n'était pas libre de grandir, se développer et
croître. Retourné sur lui-même à l'image d'une bête en cage. Te
rappelles-tu combien t'avait bouleversé le triste spectacle de cet
orang-outang en captivité que nous vîmes, ne sachant pas être triste
d'une tristesse qui nous soit commune, être triste de tristesse
au-delà de tout savoir ? Le corps flamboyant du fier animal
croupissant derrière des barreaux, de sales barreaux, te fit l'effet,
avais-tu dit, d'un miroir cruel. Ainsi va le temps qui n'ouvre sur
aucun possible, qui n'apporte aucun élan, ni aucune douceur, ni joie,
ni surprise, ainsi va le temps des résignés.

Mais le plus terrible sans doute, c'est que Zorn en était venu à se
convaincre que s'il était enfermé, si tous les muscles bandés de sa
volonté s'efforçaient de contenir sa vigueur, c'est qu'il était brutal
et très certainement dangereux. Venu à croire à la réalité de ce fond
de méchanceté qui exigeait d'être réprimé violemment ; à croire qu'il
méritait d'être écrasé d'un coup de talon sec comme un vulgaire
insecte.

Il avait donc acquis, par lentes concrétions, la certitude de son
infériorité. Retiré dans sa coque, tremblant de tous ses membres,
respirant à peine derrière sa fine, trop fine cloison pour ne pas
laisser deviner sa présence, tapi dans l'obscurité derrière la paroi
qui le défendait, Zorn redoutait par-dessus tout de la voir se
déchirer, aussi se cramponnait-il désespérément à sa construction
intérieure, à son minable édicule, à son fortin d'impuissance, de peur
d'en être délogé, y compris par l'amour. Arrivé à ce point
d'étranglement où l'on souffre de tout, mais où la cessation de la
souffrance représenterait une souffrance encore plus grande, quelle
porte y a-t-il ?

Il n'y en a pas.

## 20

Et voici ce que j'ai pensé : dans le rêve, on se persécute soi-même.
On intercale des portes entre sa raison et la perception qu'on a de
soi-même de sorte qu'on ne reconnaît plus vraiment les objets éclairés
sous cette lumière instable, ce qui revient secondairement à admettre
qu'on se dissocie en s'assoupissant, même s'il est difficile de nommer
avec certitude les entités que le sommeil sépare. Il semble toutefois
que tout le temps que la vie lucide se retire, le corps du rêveur, tel
un baigneur hors de l'eau, frissonne d'une autre vie qu'il serait
peut-être bon d'appeler, par opposition, végétative. Le commutateur de
la conscience poussé sur "off", c'est cette vie végétative,
corporelle, qui perce enfin dans cette expression mimoplastique que
sont les songes, vie intime qui, à travers des thèmes puisés dans
l'histoire personnelle de chaque rêveur, modélise sa propre réalité
infra-sensible, sans traduction possible.

Je crois que la vie de Zorn fut identique à un long rêve douloureux,
dominée à la fois par un tragique sentiment d'irréalité (n'avoir pas
vécu) et d'imposture (n'être pas soi). Un long rêve où cherchant à
fuir ce qu'il était, il était sans cesse ramené à son point de départ :
tournant en rond de manière inintelligible, revenant toujours à lui,
suscitant pour se consoler l'épouvantail même qui le hantait. Et cela
sans fin, jusqu'à la fin.

Disant cela, je me rappelle avoir observé dans la rue pavée l'arrivée
du taxi qui d'en haut ressemblait à un modèle réduit, avoir suivi le
chauffeur manœuvrer pour ranger son véhicule en double file, allumer
les feux de détresse ; puis m'être tourné doucement vers Hélène.
Immobile dans la pénombre, Hélène et son beau ventre rond. J'ai vu
alors le visage si confiant d'Hélène, si rempli de forces, sa
silhouette émouvante, et je me suis avancé.
