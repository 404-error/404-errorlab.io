---
persona:
  - Errico Malatesta
title: "Un peu de théorie"
slug: "un-peu-de-theorie"
date: 2020-12-25
echo:
  - stratégique
images:
  - "img/201225-malatesta-un-peu-de-theorie.jpg"
gridimages:
  - "img/201225-malatesta-un-peu-de-theorie-grid.jpg"
sourceimage: "https://commons.wikimedia.org/wiki/File:Barricade18March1871.jpg"
notes: "Ce texte a été publié le 19 janvier 1924 dans *Le Réveil Communiste-Anarchiste* (année XXIV, n<sup>o</sup> 632). Il a paru pour la première fois le 21 août 1892 dans le journal *L’En&nbsp;Dehors*."
summary: "La révolte gronde partout. Elle est ici l’expression d’une idée, là le résultat d’un besoin ; plus souvent la conséquence de l’entrelacement des besoins et des idées qui s’engendrent et se renforcent réciproquement. Elle s’attache aux causes du mal ou frappe à côté, elle est consciente ou instinctive, elle est humaine ou brutale, généreuse ou étroitement égoïste, mais elle grandit et s’étend toujours. C’est l’histoire qui marche : inutile de s’attarder à se plaindre des voies qu’elle choisit, puisque ces voies ont été tracées par toute l’évolution antérieure."
citation: "La révolte gronde partout."
---

La révolte gronde partout. Elle est ici l’expression d’une idée, là le résultat d’un besoin ; plus souvent la conséquence de l’entrelacement des besoins et des idées qui s’engendrent et se renforcent réciproquement. Elle s’attache aux causes du mal ou frappe à côté, elle est consciente ou instinctive, elle est humaine ou brutale, généreuse ou étroitement égoïste, mais elle grandit et s’étend toujours.

C’est l’histoire qui marche : inutile de s’attarder à se plaindre des voies qu’elle choisit, puisque ces voies ont été tracées par toute l’évolution antérieure.

Mais l’histoire est faite par les hommes ; et puisque nous ne voulons pas rester spectateurs indifférents et passifs de la tragédie historique, puisque nous voulons concourir de toutes nos forces à déterminer les événements qui nous semblent plus favorables à notre cause, il nous faut un critérium pour nous guider dans l’appréciation des faits qui se produisent, et surtout pour choisir la place que nous voulons occuper dans le combat.

La fin justifie les moyens. On a bien médit de cette maxime. En réalité, elle est le guide universel de la conduite.

On pourrait dire mieux : chaque fin comporte son moyen. Il faut la chercher dans le but ; le moyen est fatal.

Étant donné le but qu’on se propose, par volonté ou par nécessité, le grand problème de la vie c’est de trouver le moyen qui, selon les circonstances, conduit le plus sûrement et le plus économiquement au but convoité. De la manière dont on résout ce problème dépend, autant que peut dépendre de la volonté humaine, qu’un homme ou un parti atteigne ou non son but, qu’il soit utile à sa cause ou serve, sans le vouloir, la cause ennemie. Avoir trouvé le bon moyen, c’est tout le secret des grands hommes et des grands partis, qui ont laissé leurs traces dans l’histoire.

Le but des jésuites c’est, pour les mystiques, la gloire de Dieu ; pour les autres, la puissance de la Compagnie. Ils doivent donc tâcher d’abrutir les masses, de les terroriser, de les soumettre.

Le but des jacobins et de tous les partis autoritaires, qui se croient en possession de la vérité absolue, c’est d’imposer leurs idées à la masse des profanes. Ils doivent pour cela tâcher de s’emparer du pouvoir, d’assujettir les masses et de fixer l’humanité sur le lit de Procuste de leurs conceptions.

Quant à nous, c’est autre chose : bien différent est notre but, donc bien différents doivent être nos moyens.

Nous ne luttons pas pour nous mettre à la place des exploiteurs et oppresseurs d’aujourd’hui, et ne luttons pas non plus pour le triomphe d’une abstraction. Nous ne sommes pas comme ce patriote italien qui disait : "Qu’importe si tous les Italiens crèvent de faim, pourvu que l’Italie soit grande et glorieuse !" Ni, non plus, comme ce camarade qui avouait qu’il lui serait égal de massacrer trois quarts des hommes, pourvu que l’Humanité soit libre et heureuse.

Nous voulons le bonheur des hommes, de tous les hommes sans exception. Nous voulons que chaque être humain puisse se développer et vivre le plus heureusement possible. Et nous croyons que cette liberté et ce bonheur ne peuvent pas être donnés aux hommes par un homme ou par un parti, car tous les hommes doivent eux-mêmes en découvrir les conditions et les conquérir. Nous croyons que seulement la plus complète application du principe de solidarité peut détruire la lutte, l’oppression et l’exploitation --- et que la solidarité ne peut être que le résultat de la libre entente, que l’harmonisation spontanée et voulue des intérêts.

Pour nous, tout ce qui cherche à détruire l’oppression économique, politique, tout ce qui sert à élever le niveau moral et intellectuel des hommes, à leur donner la conscience de leurs droits et de leurs forces et à les persuader de faire leurs affaires eux-mêmes, tout ce qui provoque la haine contre l’oppression et l’amour entre les hommes, nous approche de notre but et par conséquent est bien --- sujet seulement à un calcul quantitatif pour obtenir avec des forces données le maximum d’effet utile. Et au contraire, est mal, parce qu’en contradiction avec le but, tout ce qui tend à conserver l’état actuel, tout ce qui tend à sacrifier, contre sa volonté, un homme au triomphe d’un principe.

Nous voulons le triomphe de la liberté et de l’amour.

Mais renonçons-nous pour cela à l’emploi des moyens violents ? Pas le moins du monde. Nos moyens sont ceux que les circonstances nous permettent et nous imposent.

Certainement nous ne voudrions arracher un cheveu à personne ; nous voudrions sécher toutes les larmes et n’en faire répandre aucune. Mais il nous faut bien lutter dans le monde tel qu’il est, sous peine de rester des rêveurs stériles.

Nous croyons fermement que le jour viendra, où il sera possible de faire le bien des hommes sans faire du mal ni à soi-même ni aux autres. Aujourd’hui, ce n’est pas possible. Même le plus pur et le plus doux des martyrs, celui qui se ferait traîner à l’échafaud pour le triomphe du bien, sans résistance, en bénissant ses persécuteurs comme le Christ de la légende, celui-là encore ferait bien du mal. Outre le mal qu’il ferait à soi-même, ce qui doit aussi compter pour quelque chose, il ferait répandre des larmes amères à tous ceux qui l’aiment.

Il s’agit donc, toujours, dans tous les actes de la vie, de choisir le moindre mal, de tâcher de faire le moins de mal pour la plus grande somme de bien possible.

L’humanité se traîne péniblement sous le poids de l’oppression politique et économique ; elle est abrutie, dégénérée, tuée (et pas toujours lentement) par la misère, l’esclavage, l’ignorance et leurs résultantes.

Pour la défense de cet état de choses existent de puissantes organisations militaires et policières, qui répondent par la prison, l’échafaud, le massacre à toute tentative sérieuse de changement. Il n’y a pas de moyens pacifiques, légaux, pour sortir de cette situation, et c’est naturel parce que la loi est faite exprès par les privilégiés pour défendre les privilèges. Contre la force physique qui nous barre le chemin, il n’y a que l’appel à la force physique, il n’y a que la révolution violente.

Évidemment, la révolution produira bien des malheurs, bien des souffrances ; mais si elle en produisait cent fois plus, elle serait encore une bénédiction relativement à ce qu’on endure aujourd’hui.

On sait que dans une seule grande bataille, on tue plus de gens que dans la plus sanglante des révolutions ; on sait les millions d’enfants qui meurent en bas âge chaque année faute de soins ; on sait les millions de prolétaires qui meurent prématurément du mal de misère ; on sait la vie rachitique, sans joie et sans espoir, que mène l’immense majorité des hommes ; on sait que même les plus riches et les plus puissants sont bien moins heureux qu’ils pourraient l’être dans une société d’égaux ; et l’on sait que cet état de choses dure depuis un temps immémorial. Il durerait indéfiniment sans la révolution, tandis qu’une seule révolution, qui s’en prendrait résolument aux causes du mal, pourrait mettre à jamais l’humanité sur la voie du bonheur.

Vienne donc la révolution ; chaque jour qu’elle tarde c’est une masse énorme de souffrances infligées aux hommes. Travaillons à ce qu’elle vienne vite et soit telle qu’il faut pour en finir avec toute oppression et toute exploitation.

C’est par amour des hommes que nous sommes révolutionnaires : ce n’est pas notre faute si l’histoire nous a acculés à cette douloureuse nécessité.

Donc pour nous, les anarchistes, ou du moins (puisque finalement les mots ne sont que des conventions) pour ceux d’entre les anarchistes qui voient les choses comme nous, tout acte de propagande ou de réalisation, par la parole ou par le fait, individuel ou collectif, est bien quand il sert à assurer à la révolution le concours conscient des masses et à lui donner ce caractère de libération universelle, sans lequel on pourrait bien avoir une révolution, mais pas la révolution que nous désirons. Et c’est surtout en fait de révolution qu’il faut tenir compte du principe du moyen le plus économique, parce qu’ici la dépense se totalise en vies humaines.

Nous connaissons assez les affreuses conditions matérielles et morales dans lesquelles se trouve le prolétariat pour ne pas nous expliquer les actes de haine, de vengeance, voire même de férocité qui pourront se produire. Nous comprenons qu’il y ait des opprimés qui, ayant été toujours traités par les bourgeois avec la plus ignoble dureté, ayant toujours vu que tout était permis au plus fort, un beau jour, quand ils se trouvent pour un moment les plus forts, se disent : "Faisons, nous aussi, comme les bourgeois." Nous comprenons qu’il peut arriver que dans la fièvre de la bataille des natures originairement généreuses, mais non préparées par une longue gymnastique morale, très difficile dans les conditions présentes, perdent de vue le but à atteindre, prennent la violence comme fin à soi-même et se laissent entraîner à des transports sauvages.

Mais une chose est comprendre et pardonner, autre chose est revendiquer. Ce ne sont pas là les actes que nous pouvons accepter, encourager, imiter. Nous devons être résolus et énergiques, mais nous devons tâcher de ne jamais outrepasser la limite marquée par la nécessité. Nous devons faire comme le chirurgien qui coupe quand il faut, mais évite d’infliger d’inutiles souffrances : en un mot, nous devons être inspirés par le sentiment de l’amour des hommes, de tous les hommes.

Il nous paraît que ce sentiment d’amour soit le fond moral, l’âme de notre programme : il nous paraît que seulement en concevant la révolution comme le grand jubilé humain, comme la libération et la fraternisation de tous les hommes, à n’importe quelle classe ou quel parti ils aient appartenu, notre idéal pourra se réaliser.

La révolte brutale se produira certainement, et elle pourra même servir à donner le grand coup d’épaule qui doit ébranler le système actuel ; mais si elle ne trouvait pas le contrepoids des révolutionnaires qui agissent pour un idéal, elle se dévorerait elle-même.

La haine ne produit pas l’amour ; par la haine on ne renouvelle pas le monde. Et la révolution de la haine, ou échouerait complètement, ou bien aboutirait à une nouvelle oppression, qui pourrait bien s’appeler anarchiste, comme on appelle libéraux les gouvernements actuels, mais qui n’en serait pas moins une oppression et ne manquerait pas de produire les effets que produit toute oppression.

