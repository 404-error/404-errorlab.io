---
persona:
  - Pierre-Aurélien Delabre
title: "Une pierre dans la bouche du monde"
slug: "Une pierre dans la bouche du monde"
date: 2024-02-20
echo:
  - esthétique
images:
  - "img/240202-delabre-une-pierre-dans-la-bouche-du-monde.jpg"
gridimages:
  - "img/240202-delabre-une-pierre-dans-la-bouche-du-monde-grid.jpg"
sourceimage: "Pierre-Aurélien Delabre"
notes: ""
summary: "le rusé Archimède / inventa le feu par réflexion optique / jetant sur la flotte de Rome / le vif éclat de nos résistances / ... / mais la ruse ayant fait son temps / ... / et les armées de Carthage et de Syracuse / dé fai tes / ... / et les cités de Carthage et de Syracuse / con qui ses / ... / LISTE [NON EXHAUSTIVE] DE NOS PERTES : / ..."
citation: "et l'informe également s'acharne"
poetry: true
hangindent: true
---

{{%epigraphe%}}

Irrécupérable est, en effet, toute image du passé qui menace de disparaître avec chaque instant présent qui, en elle, ne s'est pas reconnu visé.

 

*--- Walter Benjamin,* Sur le concept d'histoire, *1940.*

{{%/epigraphe%}}

## 1. Syracuse contre Rome

le rusé Archimède

inventa le feu par réflexion optique

jetant sur la flotte de Rome

le vif éclat de nos résistances

...

mais la ruse ayant fait son temps

...

et les armées de Carthage et de Syracuse

dé fai tes

...

et les cités de Carthage et de Syracuse

con qui ses

...

<span class="underline">LISTE \[NON EXHAUSTIVE\] DE NOS PERTES :</span>

...

\- ~~jardins de Hamilcar~~

...

\- ~~temples païens d'Ortygia~~

...

\- ~~fantômes de Tanit et d'Aréthuse~~

...

\- ~~premières sectes féministes~~

...

\- ~~traités de protosocialisme~~

...

\- ~~insurgés d'Akragas et de Sélinonte~~

...

\- ~~Pausanias et ses larmes~~

...

\- ~~etc.~~

## 2. géographie des grands ensembles

aucun Empire ne fut

à ce point empire

par la force de l'identique

succession d'images

d'emblèmes et de vocations

générant partout en son sein

de jolies miniatures de lui-même

ainsi Nîmes Narbonne ou Lyon

la Toscane des fiers Étrusques

la Macédoine d'Andriscos

les plaines d'Anatolie

la Mésopotamie à peine

mais tout le croissant fertile

Alexandrie et sa gloire livresque

Cyrène Tripoli Meknès

devinrent une triste banlieue romaine

et ses lois et ses préfectures

emportèrent le flux et le reflux

de nos épopées minuscules

croissance inexorable de l'Amour

poème des derniers troyens

dans le cœur de tous les vaincus

subsiste une faim d'égalité

que les cartographies

de Théodose ou d'Auguste

ont savamment reléguée

que l'immensité

pleine et orgueilleuse

des grands ensembles

RENVERSER LA TABLE DE PEUTINGER !!!

## 3. désubjectivation de classe

ma poésie se trouvant kamikaze

dans de nobles carnets d'écriture manuscrite

Segalen + Leiris ou cette quête fébrile du Divers[^1]

Pasolini abjurant le réalisme des années 50

« AH, AH, LA GARA À ESSERE

UNO PIÙ POETA RAZIONALE DELL'ALTRO[^2] »

muni de ses petits crocs dantesques

armé \[je le confesse\] d'un matérialisme plutôt baroque

ne sachant me passer

ni du lyrisme ni de son absence

c'était il y a huit ans \[bien avant tout ça donc\]

« face à la si noble Afrique »

écrivais-je alors \[dans un très mauvais livre[^3]\]

*Afrique fantôme* ou emphatique

me croyant toujours très innocent

comme tous ces petits voyous d'Occident

en quête de grands horizons

ouverts-azurés-généreux-etc.

mais l'innocence est une entourloupe

seuls les oppresseurs ont le privilège de l'être

il n'y a pas moins innocent que l'innocence

outre celle des enfants pyromanes bien sûr

mais ceci est une autre histoire \[nul besoin d'y revenir\]

d'ailleurs Freud et Rolland se sont plantés[^4]

le nez dans la cartographie des vainqueurs

ni le sentiment océanique

ni la Raison franche et loyale

n'auront

...

*una pietra nella bocca del mondo*

## 4. l'intifada et son poème

c'est au feu des bourrus d'Éphèse

c'est au feu des démocrates d'Agrigente

que le récit \[et non l'histoire\]

fut conté

celui-ci s'est transmis

on ne sait de quelle manière

plus de deux millénaires plus tard

comme tout ce qui échappe-déborde-résiste

à la géographie sordide

à laquelle ce poème

paresseux \[je l'admets volontiers\]

se soumet également

faute de mieux

de matériaux suffisants

tout ce qui vit et crève

dans le chant des derniers Fedayin

amis de l'Europe malade

tout votre héritage

vit et crève

avec le peuple de Palestine

la Grande-Grèce et ses satellites

les fragments d'Empédocle

les osselets d'Héraclite

les premières légendes du zoroastrisme

le pesto aux orties d'un dénommé Platon

les œuvres complètes d'Aristote

et tout ce qui précéda

l'édification de la Forme

\[⚠⚠⚠ modèle de la praxis

en termes hegeliano-marxistes ? Y REVENIR ⚠⚠⚠\]


## 5. Sicile ensevelie

en l'an 2000 et des poussières

dans les rues de Syracuse

bout de terre saturé d'HISTOIRE

que l'influence du soufre et les sédiments calcaires

confondent en petites pierres jolies

non loin de la Calarossa

de son azur un peu glacé

tout proche de la casa natale d'Elio Vittorini[^5]

impeccable socialiste

représentant littéraire du néoréalisme

un vent sévère soufflant

depuis l'inimaginable Sud

que je ne souhaite plus souiller avec un rêve ingrat

dans la paume joyeuse

d'une langue disparue

dans les airs et sur les mers

en cette immensité enfin libérée

immensité sans frontière ni limite

à la confluence de l'atome et du vide

...

<span class="underline">JE RECOMPOSE OBSTINÉMENT :</span>

\- jardins de Hamilcar

\- temples païens d'Ortygia

\- fantômes de Tanit et d'Aréthuse

\- premières sectes féministes

\- traités de protosocialisme

\- insurgés d'Akragas et de Sélinonte

\- Pausanias et ses larmes & d'autres encore

\- qui n'ont pas fini de cracher dans la soupe

\- \[recherches-en-cours\]

## 6. le sourire de l'enfant aimée

tandis que je me souviens d'Archimède

mais non de *son visage*

de la seconde guerre punique

ou de l'invention du feu par réflexion optique

...

l'image ne remplit plus rien

...

l'œil se retire

tourne sur lui-même

un nombre incalculable de fois

...

par saccade \[peut-être\]

par interruption \[sans doute\]

du temps et de l'espace

d'une façon que je ne sais pas dire

...

*son espèce de sourire*

*ses yeux posés sur nos yeux émus*

...

et l'informe

également s'acharne

ou la forme défunte

dont l'éclat demeure

inatteignable par l'image

...

*ton éternelle enfance*

...

brûle-mémoire

...

etc.

 

{{%droite%}}

Syracuse, le 9 février 2024.

{{%/droite%}}

[^1]: « Le divers décroît. Là est le grand danger terrestre. C'est donc
    contre cette déchéance qu'il faut lutter, se battre, mourir
    peut-être avec beauté. » (Victor Segalen, *Essai sur l'Exotisme. Une
    Esthétique du Divers*, publication posthume en 1978.)

[^2]: Traduction littérale : « AH, AH, LA CONCURRENCE À ÊTRE UN POÈTE
    PLUS RATIONNEL QUE L'AUTRE » (Pier Paolo Pasolini, *Poesia in forma
    di rosa*, 1964.)

[^3]: Ne cherchez pas : publié à tirage limité, les derniers exemplaires
    ont été détruits dans un incendie à Marseille en 2021.

[^4]: La correspondance entre l'inventeur de la psychanalyse et
    l'écrivain français rend compte d'un différend à propos du sentiment
    religieux : si Rolland justifie poétiquement et philosophiquement
    une conception syncrétique (mêlant christianisme et sagesse
    indienne) de l'être fini faisant face à l'incommensurable, Freud
    tente de déconstruire un tel sentiment en mobilisant un système
    d'interprétation rationnel (cf. *Das Unbehagen in der Kultur*,
    1930).

[^5]: Via Vittorio Veneto, 138, Siracusa.
