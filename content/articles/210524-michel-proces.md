---
persona:
  - Louise Michel
title: "Procès"
slug: "proces"
date: 2021-05-24
echo:
  - stratégique
  - historique
images:
  - "img/210524-michel-proces.jpg"
gridimages:
  - "img/210524-michel-proces-grid.jpg"
sourceimage: "https://www.metmuseum.org/art/collection/search/702001"
notes: "Les comptes rendus des procès, qu'a subis Louise Michel à la suite de son engagement dans la Commune de Paris, figurent dans ses *Mémoires*, publiées en 1886 par F. Roy, aux pages 465 et suivantes."
summary: "La Commune n’avait pas assez pour se défendre des hommes dévoués qui composaient la garde nationale, elle avait institué des compagnies d’enfants sous le nom de \"Pupilles de la Commune\" ; elle voulut organiser un bataillon d’amazones ; et, si ce corps ne fut pas constitué, on put voir cependant des femmes portant un costume militaire, plus ou moins fantaisiste, et la carabine sur l’épaule, précédant les bataillons qui se rendaient aux remparts. Parmi celles qui paraissent avoir exercé une influence considérable dans certains quartiers, on remarquait Louise Michel, ex-institutrice aux Batignolles, qul ne cessa de montrer un dévouement sans bornes au gouvernement insurrectionnel. Louise Michel a trente-six ans ; petite, brune, le front très développé, puis fuyant brusquement ; le nez et le bas du visage très proéminents ; ses traits révèlent une extrême dureté. Elle est entièrement vêtue de noir. Son exaltation est la même qu’aux premiers jours de sa captivité, et quand on l’amène devant le conseil, relevant brusquement son voile, elle regarde fixement ses juges."
citation: "Puisqu’il semble que tout cœur qui bat pour la liberté n’a droit qu’à un peu de plomb, j’en réclame une part, moi !"
---

# Premier procès (La Commune)

Compte rendu de la *Gazette des tribunaux*.


VI<sup>e</sup>&nbsp;Conseil de guerre (séant à Versailles).


Présidence de M.&nbsp;Delaporte, colonel du 12<sup>e</sup>&nbsp;chasseurs à cheval.


## Audience du 16 décembre 1871


La Commune n’avait pas assez pour se défendre des hommes dévoués qui composaient la garde nationale, elle avait institué des compagnies d’enfants sous le nom de "Pupilles de la Commune" ; elle voulut organiser un bataillon d’amazones ; et, si ce corps ne fut pas constitué, on put voir cependant des femmes portant un costume militaire, plus ou moins fantaisiste, et la carabine sur l’épaule, précédant les bataillons qui se rendaient aux remparts.

Parmi celles qui paraissent avoir exercé une influence considérable dans certains quartiers, on remarquait Louise&nbsp;Michel, ex-institutrice aux Batignolles, qul ne cessa de montrer un dévouement sans bornes au gouvernement insurrectionnel.

Louise&nbsp;Michel a trente-six ans ; petite, brune, le front très développé, puis fuyant brusquement ; le nez et le bas du visage très proéminents ; ses traits révèlent une extrême dureté. Elle est entièrement vêtue de noir. Son exaltation est la même qu’aux premiers jours de sa captivité, et quand on l’amène devant le conseil, relevant brusquement son voile, elle regarde fixement ses juges.

M.&nbsp;le capitaine Dailly occupe le siège du Ministère public.

M<sup>e</sup>&nbsp;Haussmann nommé d’office, assiste l’accusée, qui cependant a déclaré refuser le concours de tout avocat.

M.&nbsp;le greffier Duplan donne lecture du rapport suivant :

C’est en 1870, à l’occasion de la mort de Victor Noir, que Louise&nbsp;Michel commença à afficher ses idées révolutionnaires.

Institutrice obscure, presque sans élèves, il ne nous a pas été possible de savoir quelles étaient alors ses relations et la part à lui attribuer dans les événements précurseurs du monstrueux attentat qui a épouvanté notre malheureux pays.

Il est inutile, sans doute, de retracer en entier les incidents du 18 Mars, et comme point de départ de l’accusation, nous nous bornerons à préciser la part prise par Louise&nbsp;Michel dans le drame sanglant dont les buttes Montmartre et la rue des Rosiers furent le théâtre.

La complice de l’arrestation des infortunés généraux Lecomte et Clément Thomas craint de voir les deux victimes lui échapper. "Ne les lâchez pas !" crie-t-elle de toutes ses forces aux misérables qui les entourent.

Et plus tard, lorsque le meurtre est accompli, en présence, pour ainsi dire, des cadavres mutilés, elle témoigne toute sa joie pour le sang versé et ose proclamer "que c’est bien fait" ; puis, radieuse et satisfaite de la bonne journée, elle se rend à Belleville et à la Villette, pour s’assurer "que ces quartiers sont restés armés".

Le 19, elle rentre chez elle, après avoir pris la précaution de se dépouiller de l’uniforme fédéré qui peut la compromettre ; mais elle éprouve le besoin de causer un peu des événements avec sa concierge.

--- Ah ! s’écrie-t-elle, si Clemenceau était arrivé quelques instants plus tôt rue des Rosiers, on n’aurait pas fusillé les généraux, parce qu’il s’y serait opposé, étant du côté des Versaillais.

Enfin "l’heure de l’avènement du peuple a sonné". Paris, au pouvoir de l’étranger et des vauriens accourus de tous les coins du monde, proclame la Commune.

Secrétaire de la société dite de "Moralisation des ouvrières par le travail", Louise&nbsp;Michel organise le fameux Comité central de l’Union des femmes, ainsi que les comités de vigilance chargée de recruter les ambulanciers, et, au moment suprême, des travailleuses pour les barricades, peut-être même des incendiaires.

Une copie de manifeste trouvée à la mairie du X<sup>e</sup>&nbsp;arrondissement indique le rôle joué par elle dans lesdits comités, aux derniers jours de la lutte. Nous reproduisons textuellement cet écrit.

« Au nom de la révolution sociale que nous acclamons, au nom de la revendication des droits du travail, de l’égalité et de la justice, l’Union des femmes pour la défense de Paris et les soins aux blessés proteste de toutes ses forces comme l’indigne proclamation aux citoyennes, affichée avant-hier et émanant d’un groupe de réactionnaires.

« Ladite proclamation porte que les femmes de Paris en appellent à la générosité de Versailles et demandent la paix à tout prix.

« Non, ce n’est pas la paix, mais bien la guerre à outrance que les travailleuses de Paris viennent réclamer.

« Aujourd’hui une conciliation serait une trahison. Ce serait renier toutes les aspirations ouvrières acclamant la rénovation sociale absolue, l’anéantissement de tous les rapports juridiques et sociaux existant actuellement, la suppression de tous les privilèges, de toutes les exploitations, la substitution du règne du travail à celui du capital, en un mot, l’affranchissement du travailleur par lui-même !

« Six mois de souffrances et de trahison pendant le siège, six semaines de luttes gigantesques contre les exploiteurs coalisés, les flots de sang versés pour la cause de la liberté, sont nos titres de gloire et de vengeance !

« La lutte actuelle ne peut avoir pour issue que le triomphe de la cause populaire... Paris ne reculera pas, car il porte le drapeau de l’avenir. L’heure suprême a sonné ! Place aux travailleurs ! Arrière leurs bourreaux ! Des actes ! de l’énergie !

« L’arbre de la liberté croît, arrosé par le sang de ses ennemis !...

« Toutes unies et résolues, grandies et éclairées par les souffrances que les crises sociales entraînent à leur suite, profondément convaincues que la Commune, représentant les principes internationaux et révolutionnaires des peuples, porte en elle les germes de la révolution sociale, les femmes de Paris prouveront à la France et au monde qu’elles aussi sauront, au moment du danger suprême, aux barricades, sur les remparts de Paris, si la réaction forçait les portes, donner, comme leurs frères, leur sang et leur vie pour la défense et le triomphe de la Commune, c’est-à-dire du peuple ! Alors victorieux, à même de s’unir et de s’entendre sur leurs intérêts communs, travailleurs et travailleuses, tous solidaires par un dernier effort...&nbsp;» (Cette dernière phrase est restée inachevée.) Vive la République universelle ! Vive la Commune !&nbsp;»

Cumulant tous les emplois, elle dirigeait une école, rue Oudot, 24. Là, du haut de sa chaire, elle professait, à ses rares loisirs, les doctrines de la libre pensée et faisait chanter à ses jeunes élèves les poésies tombées de sa plume, entre autres la chanson intitulée : les *Vengeurs*.

Présidente du club de la Révolution, tenu à l’église Saint-Bernard, Louise&nbsp;Michel est responsable du vote rendu dans la séance du 18 mai (21 floréal an LXXIX), et, ayant pour but :

« La suppression de la magistrature, l’anéantissement des Codes, leur remplacement par une commission de justice ;

« La suppression des cultes, l’arrestation immédiate des prêtres, la vente de leurs biens et de ceux des fuyards et des traîtres qui ont soutenu les misérables de Versailles ;

« L’exécution d’un otage sérieux toutes les vingt-quatre heures, jusqu’à la mise en liberté et l’arrivée à Paris du citoyen Blanqui, nommé membre de la Commune.&nbsp;»

Ce n’était point assez, cependant, pour cette âme ardente, comme veut bien la qualifier l’auteur d’une notice fantaisiste qui figure au dossier, de soulever la populace, d’applaudir à l’assassinat, de corrompre l’enfance, de prêcher une lutte fratricide, de pousser en un mot à tous les crimes, il fallait encore donner l’exemple et payer de sa personne !

Aussi la trouvons-nous à Issy, à Clamart et à Montmartre, combattant au premier rang, faisant le coup de feu ou ralliant les fuyards.

Le *Cri du peuple* l’atteste ainsi dans son numéro du 14 avril :

"La citoyenne Louise&nbsp;Michel, qui a combattu si vaillamment aux Moulineaux, a été blessée au fort d’Issy."


Très heureusement pour elle, nous nous empressons de le reconnaître, l’héroïne de Jules Vallès était sortie de cette brillante affaire avec une simple entorse.


Quel est le mobile qui a poussé Louise&nbsp;Michel dans la voie fatale de la politique et de la révolution ?

C’est évidemment l’orgueil.

Fille illégitime élevée par charité, au lieu de remercier la Providence qui lui avait donné une instruction supérieure et les moyens de vivre heureuse avec sa mère, elle se laisse aller à son imagination exaltée, à son caractère irascible et, après avoir rompu avec ses bienfaiteurs, va courir l’aventure à Paris.

Le vent de la Révolution commence à souffler : Victor Noir vient de mourir.

C’est le moment d’entrer en scène ; mais le rôle de comparse répugne à Louise&nbsp;Michel ; son nom doit frapper l’attention publique et figurer en première ligne dans les proclamations et les réclames trompeuses.

Il ne nous reste plus qu’à donner la qualification légale aux actes commis par cette énergumène depuis le commencement de la crise épouvantable que la France vient de traverser jusqu’à la fin du combat impie auquel elle prit part au milieu des tombes du cimetière Montmartre.

Elle a assisté, avec connaissance, les auteurs de l’arrestation des généraux Lecomte et Clément Thomas dans les faits qui l’ont consommée, et cette arrestation a été suivie de tortures corporelles et de la mort de ces deux infortunés.

Intimement liée avec les membres de la Commune, elle connaissait d’avance tous leurs plans. Elle les a aidés de toutes ses forces, de toute sa volonté ; bien plus, elle les a assistés et souvent elle les a dépassés. Elle leur a offert de se rendre à Versailles et d’assassiner le président de la République, afin de terrifier l’Assemblée et, selon elle, de faire cesser la lutte.

Elle est aussi coupable que "Ferré le fier républicain", qu’elle défend d’une façon si étrange, et dont la tête, pour nous servir de son expression, "est un défi jeté aux consciences et la réponse une révolution".

Elle a excité les passions de la foule, prêché la guerre sans merci ni trêve et, louve avide de sang, elle a provoqué la mort des otages par ses machinations infernales.

Eu conséquence, notre avis est qu’il y a lieu de mettre Louise&nbsp;Michel en jugement pour :

1<sup>o</sup>&nbsp;Attentat ayant pour but de changer le gouvernement ;

2<sup>o</sup>&nbsp;Attentat ayant pour but d’exciter la guerre civile en portant les citoyens à s’armer les uns contre les autres ;

3<sup>o</sup>&nbsp;Pour avoir, dans un mouvement insurrectionnel, porté des armes apparentes et un uniforme militaire, et fait usage de ces armes ;

4<sup>o</sup>&nbsp;Faux en écriture privée par supposition de personnes ;

5<sup>o</sup>&nbsp;Usage d’une pièce fausse ;

6<sup>o</sup>&nbsp;Complicité par provocation et machination d’assassinat des personnes retenues soi-disant comme otages par la Commune ;

7<sup>o</sup>&nbsp;Complicité d’arrestations illégales, suivies de tortures corporelles et de mort, en assistant avec connaissance les auteurs de l’action dans les faits qui l’ont consommée ;

Crimes prévus par les articles 87, 91, 150, 151, 59, 60, 302, 341, 344 du code pénal et 5 de la loi du 26 mai 1834.


## Interrogatoire de l’accusée

*M.&nbsp;le président* : Vous avez entendu les faits dont on vous accuse ; qu’avez-vous à dire pour votre défense ?

*L’accusée* : Je ne veux pas me défendre, je ne veux pas être défendue ; j’appartiens tout entière à la révolution sociale, et je déclare accepter la responsabilité de tous mes actes. Je l’accepte tout entière et sans restriction. Vous me reprochez d’avoir participé à l’assassinat des généraux ? À cela, je répondrais oui, si je m’étais trouvée à Montmartre quand ils ont voulu faire tirer sur le peuple ; je n’aurais pas hésité à faire tirer moi-même sur ceux qui donnaient des ordres semblables ; mais lorsqu’ils ont été prisonniers, je ne comprends pas qu’on les ait fusillés, et je regarde cet acte comme une insigne lâcheté !

Quant à l’incendie de Paris, oui, j’y ai participé. Je voulais opposer une barrière de flammes aux envahisseurs de Versailles. Je n’ai pas de complices pour ce fait, j’ai agi d’après mon propre mouvement.

On me dit aussi que je suis complice de la Commune ! Assurément oui, puisque la Commune voulait avant tout la révolution sociale, et que la révolution sociale est le plus cher de mes vœux ; bien plus, je me fais honneur d’être l’un des promoteurs de la Commune qui n’est d’ailleurs pour rien, pour rien, qu’on le sache bien, dans les assassinats et les incendies : moi qui ai assisté à toutes les séances de l’Hôtel de Ville, je déclare que jamais il n’y a été question d’assassinat ou d’incendie. Voulez-vous connaître les vrais coupables ? Ce sont les gens de la police, et plus tard, peut-être, la lumière se fera sur tous ces événements dont on trouve aujourd’hui tout naturel de rendre responsables tous les partisans de la révolution sociale.

Un jour, je proposai à Ferré d’envahir l’Assemblée ; je voulais deux victimes, M.&nbsp;Thiers et moi, car j’avais fait le sacrifice de ma vie, et j’étais décidée à le frapper.

*M.&nbsp;le président* : Dans une proclamation, vous avez dit qu’on devait, toutes les vingt-quatre heures, fusiller un otage ?

R.&nbsp;Non, j’ai seulement voulu menacer. Mais pourquoi me défendrais-je ? Je vous l’ai déjà déclaré, je me refuse à le faire. Vous êtes des hommes qui allez me juger ; vous êtes devant moi à visage découvert ; vous êtes des hommes, et moi je ne suis qu’une femme, et pourtant je vous regarde en face. Je sais bien que tout ce que je pourrai vous dire ne changera en rien votre sentence. Donc un seul et dernier mot avant de m’asseoir. Nous n’avons jamais voulu que le triomphe des grands principes de la Révolution ; je le jure par nos martyrs tombés sur le champ de Satory, par nos martyrs que j’acclame encore ici hautement, et qui un jour trouveront bien un vengeur.

Encore une fois, je vous appartiens ; faites de moi ce qu’il vous plaira. Prenez ma vie si vous la voulez ; je ne suis pas femme à vous la disputer un seul instant.

*M.&nbsp;le président* : Vous déclarez ne pas avoir approuvé l’assassinat des généraux, et cependant on raconte que, quand on vous l’a appris, vous vous êtes écriée : "On les a fusillés, c’est bien fait." ---&nbsp;R.&nbsp;Oui, j’ai dit cela, je l’avoue. (Je me rappelle même que c’était en présence des citoyens Le Moussu et Ferré.)

D.&nbsp;Vous approuviez donc l’assassinat ? ---&nbsp;R.&nbsp;Permettez, cela n’en était pas une preuve ; les paroles que j’ai prononcées avaient pour but de ne pas arrêter l’élan révolutionnaire.

D.&nbsp;Vous écriviez aussi dans les journaux ; dans le *Cri du peuple*, par exemple ? ---&nbsp;R.&nbsp;Oui, je ne m’en cache pas.

D.&nbsp;Ces journaux demandaient chaque jour la confiscation des biens du clergé et autres mesures révolutionnaires semblables. Telles étaient donc vos opinions ? ---&nbsp;R.&nbsp;En effet ; mais remarquez bien que nous n’avons jamais voulu prendre ces biens pour nous ; nous ne songions qu’à les donner au peuple pour le bien-être.

D.&nbsp;Vous avez demandé la suppression de la magistrature ? ---&nbsp;R.&nbsp;C’est que j’avais toujours devant les yeux les exemples de ses erreurs. Je me rappelais l’affaire Lesurques et tant d’autres.

D.&nbsp;Vous reconnaissez avoir voulu assassiner M.&nbsp;Thiers ? ---&nbsp;R.&nbsp;Parfaitement... Je l’ai déjà dit et je le répète.

D.&nbsp;Il paraît que vous portiez divers costumes sous la Commune ? ---&nbsp;R.&nbsp;J’étais vêtue comme d’habitude ; je n’ajoutai qu’une ceinture rouge sur mes vêtements.

D.&nbsp;N’avez-vous pas porté plusieurs fois un costume d’homme ? ---&nbsp;R.&nbsp;Une seule fois : c’était le 18 Mars ; je m’habillai en garde national, pour ne pas attirer les regards.

Peu de témoins ont été assignés, les faits reprochés à Louise&nbsp;Michel n’étant pas discutés par elle.

On entend d’abord la *femme Poulain*, marchande.

*M.&nbsp;le président* : Vous connaissez l’accusée ? Vous savez quelles étaient ses idées politiques ? ---&nbsp;R.&nbsp;Oui, monsieur le président, et elle ne s’en cachait pas. Très exaltée, on ne voyait qu’elle dans les clubs ; elle écrivait aussi dans les journaux.

D.&nbsp;Vous l’avez entendue dire à propos de l’assassinat des généraux : "C’est bien fait !" ---&nbsp;R.&nbsp;Oui, monsieur le président.

*Louise&nbsp;Michel* : Mais j’ai avoué le fait, c’est inutile que des témoins viennent le certifier.

*Femme Botin*, peintre.

*M.&nbsp;le président* : Louise&nbsp;Michel n’a-t-elle pas dénoncé un de vos frères pour le forcer à servir dans la garde nationale ? ---&nbsp;R.&nbsp;Oui, monsieur le président.

*Louise&nbsp;Michel* : Le témoin avait un frère, je le croyais brave et je voulais qu’il servît la Commune.

*M.&nbsp;le président* (au témoin) : Vous avez vu l’accusée un jour dans une voiture se promenant au milieu des gardes et leur faisant des saluts de reine, selon votre expression ? ---&nbsp;R.&nbsp;Oui, monsieur le président.

*Louise&nbsp;Michel* : Mais cela ne peut pas être vrai, car je ne pouvais vouloir imiter ces reines dont on parle et que je voudrais toutes voir décapitées comme Marie-Antoinette. La vérité est que j’étais tout simplement montée en voiture parce que je souffrais d’une entorse qui était la suite d’une chute faite à Issy.

La *femme Pompon*, concierge, répète tout ce qui se racontait sur le compte de l’accusée. On la connaissait comme très exaltée.

*Cécile Denéziat*, sans profession, connaissait beaucoup l’accusée.

*M.&nbsp;le président* : L’avez-vous vue habillée en garde national ? ---&nbsp;R.&nbsp;Oui, une fois, vers le 17 mars.

D.&nbsp;Portait-elle une carabine ? ---&nbsp;R.&nbsp;Je l’ai dit, mas je ne me rappelle pas bien ce fait.

D.&nbsp;Vous l’avez vue se promenant en voiture, au milieu des gardes nationaux ? ---&nbsp;R.&nbsp;Oui, monsieur le président, mais je ne me rappelle pas exactement les détails de ce fait.

D.&nbsp;Vous avez aussi déjà dit que vous pensiez qu’elle s’était trouvée au premier rang quand on avait assassiné les généraux Clément Thomas et Lecomte ? ---&nbsp;R.&nbsp;Je ne faisais que répéter ce qu’on avait dit autour de moi.

M.&nbsp;le capitaine Dailly prend la parole. Il demande au conseil de retrancher de la société l’accusée, qui est pour elle un danger continuel. Il abandonne l’accusation sur tous les chefs, excepté sur celui de port d’armes apparentes ou cachées dans un mouvement insurrectionnel.

M<sup>e</sup>&nbsp;Haussmann, à qui la parole est ensuite donnée, déclare que devant la volonté formelle de l’accusée de ne pas être défendue, il s’en rapporte simplement à la sagesse du conseil.


*M.&nbsp;le président* : Accusée, avez-vous quelque chose à dire pour votre défense ?

*Louise&nbsp;Michel* : Ce que je réclame de vous, qui vous affirmez conseil de guerre, qui vous donnez comme mes juges, qui ne vous cachez pas comme la commission des grâces, de vous qui êtes des militaires et qui jugez à la face de tous, c’est le champ de Satory, où sont déjà tombés nos frères.

Il faut me retrancher de la société ; on vous dit de le faire ; eh bien ! le commissaire de la République a raison. Puisqu’il semble que tout cœur qui bat pour la liberté n’a droit qu’à un peu de plomb, j’en réclame une part, moi ! Si vous me laissez vivre, je ne cesserai de crier vengeance, et je dénoncerai à la vengeance de mes frères les assassins de la commission des grâces...

*M.&nbsp;le président* : Je ne puis vous laisser la parole si vous continuez sur ce ton.

*Louise&nbsp;Michel* : J’ai fini... Si vous n’êtes pas des lâches, tuez-moi...

Après ces paroles, qui ont causé une profonde émotion dans l’auditoire, le conseil se retire pour délibérer. Au bout de quelques instants, il rentre en séance, et, aux termes du verdict, Louise&nbsp;Michel est à l’unanimité condamnée à la déportation dans une enceinte fortifiée.

On ramène l’accusée et on lui donne connaissance du jugement. Quand le greffier lui dit qu’elle a vingt-quatre heures pour se pourvoir en révision : "Non ! s’écrie-t-elle, il n’y a point d’appel ; mais je préférerais la mort !"


## Observations (de Louise&nbsp;Michel)

Je me bornerai à relever quelques erreurs :

1<sup>o</sup>&nbsp;Je n’ai pas été élevée par charité, mais par les grands-parents qui ont trouvé juste de le faire.

J’ai quitté Vroncourt après leur mort seulement, et pour me préparer à mon diplôme d’institutrice ; je croyais ainsi pouvoir être utile à ma mère.

2<sup>o</sup>&nbsp;Le chiffre de mes élèves à Montmartre était de cent cinquante. Ce qui a été constaté par la mairie au temps du siège.

3<sup>o</sup>&nbsp;Peut-être n’est-il pas inutile de dire que contrairement à la description de ma personne faite au commencement du compte rendu de la *Gazette des tribunaux*, je suis plutôt grande que petite ; il est bon, par le temps où nous vivons, de ne passer que pour soi-même.


# Deuxième procès (anniversaire de Blanqui)

Extrait de l’*Intransigeant* du 7 janvier 1882.

## Police correctionnelle

La première accusée appelée est Louise&nbsp;Michel. La vaillante citoyenne est très calme. C’est de sa voix lente et d’une façon très précise qu’elle répond aux questions du président.

--- Vous êtes prévenue d’outrages aux agents, lui dit M.&nbsp;Puget.

--- Ce serait plutôt à nous de nous plaindre de brutalités et d’outrages, répond Louise&nbsp;Michel, car nous avons été très calmes. Voici ce qui s’est passé et ce qui motive sans doute ma présence ici :

En arrivant chez le commissaire de police, j’ai vu en bas plusieurs agents qui frappaient violemment un homme. Ne voulant rien dire à ces agents qui étaient très surexcités, je suis montée au premier ; j’ai trouvé là deux autres agents plus calmes auxquels j’ai dit : Descendez vite, on assassine en bas.

*M.&nbsp;le Président* : Ce récit est en désaccord avec la déposition des témoins que nous allons entendre.

*Louise&nbsp;Michel* : Ce que j’ai dit est la vérité. D’ailleurs, j’ai avoué des choses plus terribles que celle-là.

Le témoin appelé est un nommé Conar, gardien de la paix. Il raconte qu’il a trouvé en arrivant chez le commissaire de police deux femmes, dont Louise&nbsp;Michel, et que celle-ci lui a dit : Vous êtes des assassins et des *feignants* (*sic*).

*Louise&nbsp;Michel* : C’est faux !

L’agent persiste à affirmer la véracité de son récit.

Louise&nbsp;Michel répète qu’elle a dit la vérité et qu’elle ne peut dire autre chose.

Malgré l’invraisemblance du récit de l’agent, le tribunal, en vertu de l’article 224 du code pénal, condamne Louise&nbsp;Michel à quinze jours de prison.


## Note (de Louise&nbsp;Michel)


Je cite ici l’*Intransigeant*, non pour étaler un compte rendu plus favorable, mais parce que le procès ne se trouve pas dans la *Gazette des tribunaux*.

Nos amis ont raison de trouver invraisemblables les paroles qui me sont attribuées. J’ai dit : on assassine ici, au lieu de la phrase d’argot qui m’est prêtée ---&nbsp;le mot *feignant* n’est pas de mon vocabulaire.

# Troisième procès (manifestation de l’esplanade des Invalides)


Extrait de La Gazette des tribunaux.

Cour d’assises de la Seine.

Présidence de M.&nbsp;Ramé.


## Audience du 21 juin 1883

Je crois inutile de donner le texte de l’acte d’accusation, dont voici les conclusions.

Louise&nbsp;Michel ; Jean-Joseph-Émile Pouget ; Eugène Mareuil, sont accusés :

1<sup>o</sup>&nbsp;D’avoir été, en mars 1883 à Paris, les chefs et instigateurs du pillage, commis en bande et à force ouverte, des pains appartenant aux époux Augereau, boulangers ;

2<sup>o</sup>&nbsp;D’avoir été, à la même époque et au même lieu, les chefs et instigateurs du pillage, commis en bande et à force ouverte, des pains appartenant aux époux Bouché, boulangers ;

3<sup>o</sup>&nbsp;D’avoir été, à la même époque et au même lieu les chefs et instigateurs du pillage, commis en bande et à force ouverte, des pains appartenant aux époux Moricet, boulangers.


## Interrogatoire de Louise Michel

D.&nbsp;Avez-vous déjà été poursuivie ? ---&nbsp;R.&nbsp;Oui, en 1871.

D.&nbsp;Il ne peut plus en être question. Ces faits ont été couverts par l’amnistie. Avez-vous été condamnée depuis ? ---&nbsp;R.&nbsp;J’ai été condamnée à quinze jours de prison pour la manifestation de Blanqui.

D.&nbsp;Vous prenez donc part à toutes les manifestions ? ---&nbsp;R.&nbsp;Hélas oui ! Je suis toujours avec les misérables.

D.&nbsp;C’est pour cela que vous avez assisté à la manifestation de l’esplanade des Invalides. Quel résultat en espériez-vous ? ---&nbsp;R.&nbsp;Une manifestation pacifique est toujours sans résultat, mais je pensais que le gouvernement userait de ses moyens habituels et qu’une manifestation serait balayée par le canon et il eût été lâche de ma part de ne pas y aller.

D.&nbsp;Vous avez recruté des adhérents pour cette manifestation, Connaissiez-vous Pouget ? ---&nbsp;R.&nbsp;J’avais rencontré Pouget dans quelques réunions.

D.&nbsp;Pouget était votre secrétaire. C’était lui qui devait distribuer en province les brochures propageant vos idées. Il recueillait le nom de vos adhérents. ---&nbsp;R.&nbsp;Ce ne sont pas à proprement parler des adhérents. Ce sont des personnes curieuses de nos idées.

D.&nbsp;Vous êtes le chef d’une petite manifestation spéciale qui a suivi la manifestation générale, mais nous devons d’abord nous occuper de celle-ci. Vous êtes allés aux Invalides et vous avez rencontré Pouget ? ---&nbsp;R.&nbsp;Oui, monsieur.

D.&nbsp;Étiez-vous d’accord avec Pouget et Mareuil pour vous rendre à l’esplanade ? ---&nbsp;R.&nbsp;Non, monsieur, nous nous sommes rencontrés par hasard.

D.&nbsp;Est-ce qu’il n’y avait à cette réunion que des ouvriers sans ouvrage ? ---&nbsp;R.&nbsp;Oui, monsieur.

D.&nbsp;Est-ce que vous croyez que cette manifestation pouvait donner du travail ? ---&nbsp;R.&nbsp;Je vous ai déjà dit que non. J’y ai été par devoir.

D.&nbsp;La manifestation a été dispersée. N’est-ce pas à ce moment que vous avez voulu faire votre petite manifestation ? ---&nbsp;R.&nbsp;Ce n’était pas une manifestation, c’était le cri des travailleurs que je voulais faire entendre.

D.&nbsp;Vous avez demandé un drapeau noir ? ---&nbsp;R.&nbsp;Oui, et on m’a apporté un chiffon noir.

D.&nbsp;Qui est-ce qui vous l’a donné ? ---&nbsp;R.&nbsp;Un inconnu.

D.&nbsp;On ne trouve pourtant pas si facilement et par hasard un drapeau sur l’esplanade des Invalides ? ---&nbsp;R.&nbsp;Il suffit d’un haillon noir et d’un manche à balai.

D.&nbsp;Il résulte de ce fait que la manifestation était préparée. Qui avait préparé ce drapeau ? ---&nbsp;R.&nbsp;Personne, et ce serait quelqu’un que je ne désignerai pas cette personne, ainsi que vous pensez bien.

D.&nbsp;N’avez-vous pas quitté l’esplanade avec l’intention de faire une manifestation ? ---&nbsp;R.&nbsp;Je me suis mise simplement à la tête d’un groupe.

D.&nbsp;Pouget et Mareuil n’en étaient-ils pas ? ---&nbsp;R.&nbsp;Oui ils se sont entêtés à me protéger.

D.&nbsp;Quel était votre but en parcourant Paris, avec un drapeau noir ? Croyez-vous que vous procureriez ainsi du pain aux ouvriers ? ---&nbsp;R.&nbsp;Non, mais je voulais faire voir qu’ils en manquaient et qu’ils avaient faim. C’est le drapeau des grèves, le drapeau des famines que je tenais.

M.&nbsp;le président ordonne à l’huissier de prendre sur la table des pièces à conviction un drapeau noir que Louise&nbsp;Michel reconnaît pour être celui qu’elle portait le 9 mars.

D.&nbsp;Vous êtes arrivée au boulevard Saint-Germain. Pourquoi vous êtes-vous arrêtée devant la boulangerie du sieur Bouché ? ---&nbsp;R.&nbsp;J’ai constamment marché. Les gamins m’ont dit qu’on leur donnait du pain, je ne me suis pas occupée de ces détails.

D.&nbsp;Vous prétendez qu’on donnait volontairement du pain. ---&nbsp;R.&nbsp;Oui, monsieur, les gamins nous ont dit qu’on leur donnait du pain et des sous. J’en ai même été très humiliée.

D.&nbsp;Et les hommes armés de gourdins, est-ce qu’on leur donnait volontairement du pain ? ---&nbsp;R.&nbsp;Nous n’avions pas avec nous de personnes armées de gourdins, ils ne sont pas au banc des accusés, ceux-là !

D.&nbsp;Vous ne pouvez pas contester le fait : le témoin Bouché vous a vue arriver à la tête d’une bande, et quinze ou vingt individus s’en sont détachés pour piller la boutique, en criant : "Du pain, du travail, ou du plomb." ---&nbsp;R.&nbsp;Ils n’étaient pas des nôtres. C’est la mise en scène de la police, cela.

D.&nbsp;Vous avez dit dans un interrogatoire que vous ne regardiez pas comme un délit de prendre du pain. ---&nbsp;R.&nbsp;Oui, mais jamais je n’en ai pris, jamais je n’en prendrai quand même je mourrais de faim.

D.&nbsp;Quand vous avez été arrêtée, place Maubert, avez-vous dit à l’officier de police : "Ne me faites pas de mal, nous ne demandons que du pain" ? ---&nbsp;R.&nbsp;Je n’ai pas dit : "Ne me faites pas de mal," mais j’ai peut être dit : "Nous ne demandons que du pain, on ne vous fera pas de mal."

D.&nbsp;En somme la boulangerie de M.&nbsp;Bouché a été complètement pillée. ---&nbsp;R.&nbsp;Je n’ai même pas vu de boulangerie, je ne connais pas M.&nbsp;Bouché.

D.&nbsp;La boutique avance sur la rue ; elle crève les yeux. ---&nbsp;R.&nbsp;Je ne pensais qu’à la misère, je ne pensais pas aux boutiques des boulangers.

D.&nbsp;Vous êtes arrivée ensuite devant la boutique de M.&nbsp;Augereau ? ---&nbsp;R.&nbsp;Je ne connais pas M.&nbsp;Augereau.

D.&nbsp;Avez-vous levé votre drapeau devant cette boutique. ---&nbsp;R.&nbsp;J’ai pu le lever et le baisser bien des fois.

D.&nbsp;Avez-vous dit : "Allez" ? ---&nbsp;R.&nbsp;J’ai pu le dire mais, j’ai dû dire bien des fois : "Allons ou marchons" ; je ne m'en souviens pas.

D.&nbsp;Combien aviez-vous de personnes devant vous ? ---&nbsp;R.&nbsp;Je ne sais pas.

D.&nbsp;Bref, la boutique de M.&nbsp;Augereau a été complètement pillée. ---&nbsp;R.&nbsp;Je ne sais pas et je m’étonne que M.&nbsp;Augereau se soit occupé de ces misères. J’ai vu piller et tuer bien autre chose.

D.&nbsp;Alors cela vous est absolument indifférent ? ---&nbsp;R.&nbsp;Oui, absolument indifférent.

D.&nbsp;Vous avez débouché ensuite sur le boulevard Saint-Germain. Vous êtes-vous arrêtée devant la boutique Moricet ? ---&nbsp;R.&nbsp;Je ne sais pas et je ne comprends pas que vous me posiez une pareille question.

D.&nbsp;Vous êtes-vous mise à rire devant la boutique ? ---&nbsp;R.&nbsp;Je ne sais pas ce qui aurait pu me faire rire ? Est-ce la misère de ceux qui m’environnaient, est-ce ce triste état de choses qui nous ramène avant 1789 ?

D.&nbsp;En somme vous vous prétendez étrangère à tous ces faits-là. ---&nbsp;R.&nbsp;Oui, monsieur.

D.&nbsp;Mais ces trois commerçants dévalisés prétendent que la foule obéissait à un signal. ---&nbsp;R.&nbsp;C’est inepte. Pour obéir à un signal il faut qu’il soit convenu ; il aurait donc fallu faire savoir dans tout Paris que je lèverais ou baisserais le drapeau devant les boulangeries.

D.&nbsp;Alors c’est un mouvement populaire instinctif. ---&nbsp;R.&nbsp;C’est l’œuvre de quelques enfants. Les gens raisonnables qui m’environnaient ne s’en sont pas occupés.

D.&nbsp;Vous avez quitté la manifestation place Maubert, laissant aux mains de la police Pouget et Mareuil qui se sont fait arrêter pour vous sauver. Vous avez disparu. ---&nbsp;R.&nbsp;Mes amis ont exigé que je ne me fasse pas arrêter ce jour-là.

D.&nbsp;Avez-vous eu connaissance de la distribution faite en province par Pouget d’une brochure intitulée : *À l’armée ?* ---&nbsp;R.&nbsp;Au moment où les d’Orléans embauchaient ouvertement contre la République, j’ai voulu embaucher pour la République, et c’est sous mon inspiration qu’a été distribuée cette brochure. C’était un cri de détresse !

D.&nbsp;Aviez-vous connaissance des études spéciales auxquelles Pouget se livrait sur les matières incendiaires ? ---&nbsp;R.&nbsp;Tout le monde aujourd’hui s’occupe de science. Tout le monde lit la *Revue scientifique* et cherche par là à améliorer le sort des travailleurs.

D.&nbsp;Nous ne sommes pas ici pour faire des théories. ---&nbsp;Étiez-vous au courant des études auxquelles se livrait Pouget ? ---&nbsp;R.&nbsp;Je ne m’occupe pas de savoir si on lit ou si on ne lit pas les revues scientifiques.

M.&nbsp;le président procède ensuite à l’interrogatoire de Pouget.


## Audition des témoins

*Bouché* (Jules), boulanger rue des Canettes : Le 9 mars, vers une heure de l’après-midi, une vingtaine d’individus ont envahi ma boulangerie. Ils étaient armés de cannes plombées et demandaient "du pain ou du travail !" Je leur ai dit : "Si vous voulez du pain, prenez-en mais ne cassez rien !"

D.&nbsp;Reconnaissez-vous l’accusée ? ---&nbsp;R.&nbsp;Non, monsieur.

D.&nbsp;Avez-vous laissé prendre votre pain, parce que vous ne pouviez faire autrement ? ---&nbsp;R.&nbsp;Il n’y avait moyen de rien faire ; toute résistance était impossible.

D.&nbsp;Était-ce des enfants qui sont entrés chez vous ? ---&nbsp;R.&nbsp;Non, monsieur ! c’étaient des gens raisonnables. (Rires.)

*Louise&nbsp;Michel* : Les gens armés de cannes plombées n’étaient pas des nôtres, je sais bien d’où ils viennent.

D.&nbsp;D’où venaient-ils donc ? ---&nbsp;R.&nbsp;De la police. (Rires.)

*Femme Augereau*, boulangère, rue du Four-Saint-Germain : J’ai vu, dans l’après-midi du 9 mars, M<sup>me</sup>&nbsp;Louise&nbsp;Michel s’arrêter devant ma porte. On a crié : "Du pain ! du pain !" Ces messieurs sont entrés et ont volé du pain, des biscuits. Ils ont cassé une assiette et deux carreaux.

D.&nbsp;Était-ce des gamins qui ont pillé votre boutique ? ---&nbsp;R.&nbsp;Oh ! il y avait plus de grandes personnes que de gamins.

D.&nbsp;Mais où était Louise&nbsp;Michel pendant qu’on pillait ? ---&nbsp;R.&nbsp;Elle était plantée juste au milieu de la rue.

D.&nbsp;Est-ce volontairement que vous avez donné votre pain ? ---&nbsp;R.&nbsp;Oh non, monsieur.

D.&nbsp;Combien y en avait-il ? ---&nbsp;R.&nbsp;Je ne puis pas vous le dire, mais il y en avait beaucoup ; c’était un véritable pillage.

*Fille Augereau* (Rosalie), rue du Four-Saint-Germain : Le 9 mars dernier, nous avons vu arriver une bande, à la tête de laquelle il y avait une femme avec un drapeau noir, arrivée devant chez nous, elle a frappé la terre avec son drapeau, quelqu’un a dit : "Allez !" On a envahi la maison et tout a été pillé.

D.&nbsp;(à Louise&nbsp;Michel) : Voilà le second témoin qui vous a vue arrêtée devant la boutique.

*Louise&nbsp;Michel* : Je ne puis prendre ces dépositions-là au sérieux. Je ne puis, devant des hommes sérieux, discuter ces choses-là. (Rires.)

D.&nbsp;(au témoin) : Est-ce une voix de femme qui a dit "Allez !" ---&nbsp;R.&nbsp;Oui, monsieur.

D.&nbsp;Y avait-il d’autres femmes dans la foule ? ---&nbsp;R.&nbsp;Je n’en ai pas vu.

*Moricet*, boulanger, boulevard Saint-Germain, 125 : Le 9 mars dernier, j’étais couché quand ma petite-fille est venue me réveiller. Il y avait du monde plein la boutique, j’ai vu une femme qui s’en allait avec un drapeau noir.

*Femme Moricet*, boulangère, boulevard Saint-Germain, 125 : Le 9 mars dernier, la foule s’est amassée devant ma boutique. Elle avait à sa tête Louise&nbsp;Michel ; cette dernière s’est arrêtée devant chez moi, a frappé la terre de son drapeau et s’est mise à rire. Ils demandaient du pain ou du travail ! Je me suis mise à leur donner du pain, mais ils n’ont pas tardé à le prendre eux-mêmes et à tout casser.

D.&nbsp;(à Louise&nbsp;Michel) : Que pensez-vous de cette déposition ? Elle est assez nette ?

*Louise&nbsp;Michel* : Tellement nette que je n’ai jamais rien vu de pareil. (Rires.) Comment ai-je pu rire ? Madame l’a complètement rêvé.

*Le témoin* : Je suis ici pour dire ce que j’ai vu.

*Louise&nbsp;Michel* : Vous êtes libre de dire ce que vous voulez, mais je suis libre de dire que vous l’avez rêvé.

D.&nbsp;(au témoin) : Ce n’est pas librement que vous donniez votre pain à ces gens-là ? ---&nbsp;R.&nbsp;Non, monsieur, c’est qu’ils arrivaient avec des gestes effrayants ; ils criaient : "Du travail et du pain !"

*Louise&nbsp;Michel* : Oh ! ils étaient bien effrayants ! J’étais aussi bien effrayante ! Ces dames étaient complètement hallucinées d’effroi ; elles regardaient Louise&nbsp;Michel comme une espèce d’hydre.

*Cornat*, officier de paix du VI<sup>e</sup>&nbsp;arrondissement : Le 9 mars dernier, apprenant qu’une bande parcourait l’arrondissement en poussant des cris séditieux, je vais à sa poursuite et je l’atteignis place Maubert. La bande était dirigée par Louise&nbsp;Michel, ayant à ses côtés Pouget et Mareuil. J’arrête ces deux derniers et Pouget me traita de lâche et de canaille. Quant à Louise&nbsp;Michel elle put s’esquiver. Tous ces gens-là criaient : "Vive la Révolution ! à bas la police !"

D.&nbsp;Louise&nbsp;Michel ne vous a-t-elle pas dit quelque chose ? ---&nbsp;R.&nbsp;Elle m’a dit : "Ne me faites pas de mal !"

*Blanc*, gardien de la paix au VI<sup>e</sup>&nbsp;arrondissement : Le 9 mars dernier, un gardien est venu prévenir l’officier de paix qu’on pillait une boulangerie rue des Canettes. Nous nous sommes mis à la poursuite de la bande et nous l’avons atteinte place Maubert. M.&nbsp;l’officier de paix a arrêté Louise&nbsp;Michel qui lui a dit : Ne nous faites pas de mal, nous ne demandons que du pain ! Pouget a traité M.&nbsp;l’officier de paix de lâche et de canaille. Mareuil criait : "À bas la police ! à bas Vidocq ! Vive la révolution sociale !" Les assaillants avaient des cannes plombées, des revolvers et des couteaux.

*Louise&nbsp;Michel* : Je n’ai jamais dit : "Ne nous faites pas de mal," mais seulement : "On ne vous fera pas de mal." Tous ces messieurs étaient dans le plus grand trouble.

D.&nbsp;(à Louise&nbsp;Michel) : Il n’y avait que vous de sang-froid ? ---&nbsp;R.&nbsp;Nous en avons tant vu ! je proteste pour l’honneur de la Révolution ! J’ai bien le droit de relever les variations des témoins. Je ne me suis jamais prosternée devant personne. Je n’ai jamais demandé grâce. Vous pouvez dire tout ce que vous voudrez, vous pouvez nous condamner, mais je ne veux pas que vous nous déshonoriez.


## Audience du 22 juin

Suite des témoins à charge

*Demoiselle Moricet* : Le 9 mars dernier, j’étais dans la boutique avec ma sœur et ma mère, quand j’ai vu arriver devant la maison une bande conduite par une femme armée d’un drapeau noir.

Cette femme s’est arrêtée devant la boutique, a frappé la terre de son drapeau et s’est *mise à rire* !

Aussitôt la bande s’est jetée dans la boutique, a pris tout le pain et les gâteaux qui étaient là, puis on a cassé les assiettes et les vitres ; j’ai été vite chercher mon père.

D.&nbsp;Vous êtes bien sûre d’avoir vu Louise&nbsp;Michel s’arrêter devant la boutique et rire en frappant la terre de son drapeau ? ---&nbsp;R.&nbsp;Oui, monsieur.

*Louise&nbsp;Michel* : Je suis honteuse de répondre à des choses comme celles-là !

Quand la petite Moricet amènerait sa sœur, sa cousine, son petit frère, et qui elle voudra, je ne m’arrêterai pas à répondre à des choses aussi peu sérieuses.

J’attends le réquisitoire pour y répondre.

*Demoiselle Moricet*, sœur de la précédente : J’étais dans la boutique avec ma mère, j’ai vu, tout d’un coup, toute une bande avec une femme à sa tête ; c’était madame. Elle s’est mise à rire en regardant la boutique et j’ai même dit à ma mère : Tiens, elle te connaît donc ! À ce moment tout le monde s’est jeté sur la boutique et l’a mise au pillage.

*Louise&nbsp;Michel* : Je répéterai ce que j’ai dit tout à l’heure : il est honteux de voir des enfants réciter ici les leçons que leurs parents leur ont apprises.

*Chaussadat*, peintre, quai du Louvre, entendu sur la demande de la défense : Le 9 mars, j’étais au coin de la rue de Seine, en face la boulangerie Moricet, j’ai vu arriver la foule de loin, M<sup>lle</sup>&nbsp;Louise&nbsp;Michel est passée sans s’arrêter ; ---&nbsp;j’ai entendu plus tard, parler du pillage de la boulangerie (ou plutôt j’ai vu jeter du pain.)

D.&nbsp;Vous n’appelez pas ça piller ?

R.&nbsp;J’ai vu qu’on jetait du pain et les malheureux le ramassaient.

*Louise&nbsp;Michel* : J’ai à remercier le témoin de rendre hommage à la vérité !

*Henri Rochefort*, publiciste : Un jour, en parlant des manifestations du mois de mars, Louise&nbsp;Michel me dit que les journaux avaient beaucoup parlé d’une somme de 60 francs environ qui avait été trouvée sur un des accusés ; elle ajouta que cette somme provenait d’une collecte faite à une réunion. Louise&nbsp;Michel m’a fait cette communication au moment où elle s’est rendue chez M.&nbsp;Camescasse pour se constituer prisonnière. Le même jour, elle m’a confirmé le caractère absolument pacifique de la manifestation à laquelle elle s’était livrée. Elle n’avait même pas voulu prendre le drapeau rouge. J’ai été très surpris de cette accusation de pillage portée contre Louise&nbsp;Michel.

*Vaughan*, publiciste : M<sup>lle</sup>&nbsp;Louise&nbsp;Michel, le soir même de la manifestation, m’a dit que son ami Pouget serait trouvé porteur d’une somme de 60 ou 70&nbsp;francs qui lui avait été remise par elle-même et qui était le produit d’une collecte faite à une réunion. Je suis heureux de manifester à la citoyenne Louise&nbsp;Michel ma très vive sympathie.

*Louise&nbsp;Michel* : Citoyen, je vous remercie et je tâcherai qu’aucun citoyen n’ait jamais à rougir de moi.

*Rouillon*, voisin de la mère de Louise&nbsp;Michel : La citoyenne Louise&nbsp;Michel n’avait aucune confiance dans les suites de la manifestation. Elle me l’a déclaré avant d’y aller. La citoyenne n’y allait que par devoir.

Le témoin entre ensuite dans d’assez longs détails sur des violences et des menaces dont auraient été l’objet Louise&nbsp;Michel et sa famille.

*Louise&nbsp;Michel* : Vous voyez bien qu’on assassine nos familles chez nous, et cela est permis !

*Meusy*, rédacteur de l’*Intransigeant*, confirme ce qu’a rapporté le témoin Vaughan à propos de la somme de 71&nbsp;francs trouvée sur l’accusé Pouget.

C’est le dernier témoin à décharge.

La parole est donnée ensuite à l’avocat général Quesnay de Beaurepaire. Puis M.&nbsp;Balandreau, avocat nommé d’office déclare que Louise&nbsp;Michel entend se défendre elle-même.


## Plaidoirie de Louise Michel

C’est un véritable procès politique qui nous est fait ; ce n’est pas nous qu’on poursuit, c’est le parti anarchiste que l’on poursuit en nous, et c’est pour cela que j’ai dû refuser les offres qui m’étaient faites par M.&nbsp;Balendreau et par notre ami Laguerre qui, il n’y a pas longtemps, prenait ici chaleureusement la défense de nos amis de Lyon.

M.&nbsp;l’avocat général a invoqué contre nous la loi de 1871 ; je ne m’occuperai pas de savoir si cette loi de 1871 n’a pas été faite par les vainqueurs contre les vaincus, contre ceux qu’ils écrasaient alors comme la meule écrase le grain ; c’était le moment où on chassait le fédéré dans les plaines, où Gallifet nous poursuivait dans les catacombes, où il y avait de chaque côté des rues de Paris des monceaux de cadavres. Il y a une chose qui vous étonne, qui vous épouvante, c’est une femme qui ose se défendre. On n’est pas habitué à voir une femme qui ose penser ; on veut selon l’expression de Proudhon, voir dans la femme une ménagère ou une courtisane !

Nous avons pris le drapeau noir parce que la manifestation devait être essentiellement pacifique, parce que c’est le drapeau noir des grèves, le drapeau de ceux qui ont faim. Pouvions-nous en prendre un autre ? Le drapeau rouge est cloué dans les cimetières et on ne doit le reprendre que quand on peut le défendre. Or, nous ne le pouvions pas ; je vous l’ai dit et je le répète, c’était une manifestation essentiellement pacifique.

Je suis allée à la manifestation, je devais y aller. Pourquoi m’a-t-on arrêtée ? J’ai parcouru l’Europe, disant que je ne reconnaissais pas de frontières, disant que l’humanité entière a droit à l’héritage de l’humanité. Et cet héritage, il n’appartiendra pas à nous, habitués à vivre dans l’esclavage, mais à ceux qui auront la liberté et qui sauront en jouir. Voilà comment nous défendons la République et quand on nous dit que nous sommes ses ennemis, nous n’avons qu’une chose à répondre, c’est que nous l’avons fondée sur trente-cinq mille de nos cadavres.

Vous parlez de discipline, de soldats qui tirent sur leurs chefs. Croyez-vous, monsieur l’avocat général, que si, à Sedan, ils avaient tiré sur leurs chefs qui les trahissaient, ils n’auraient pas bien fait. Nous n’aurions pas eu les boues de Sedan.

M.&nbsp;l’avocat général a beaucoup parlé des soldats ; il a vanté ceux qui rapportaient les manifestes anarchistes à leurs chefs. Y a-t-il beaucoup d’officiers, y a-t-il beaucoup de généraux qui aient rapporté les largesses de Chantilly et les manifestes de M.&nbsp;Bonaparte ? Non pas que je fasse le procès aux d’Orléans ou à M.&nbsp;Bonaparte, nous ne faisons le procès qu’aux idées. On a acquitté M.&nbsp;Bonaparte et on nous poursuit ; je pardonne à ceux qui commettent le crime, je ne pardonne pas au crime. Est-ce que ce n’est pas la loi des forts qui nous domine ? Nous voulons le remplacer par le droit, et c’est là tout notre crime !

Au-dessus des tribunaux, au-delà des vingt ans de bagne que vous pouvez prononcer, au-delà même de l’éternité du bagne si vous voulez, je vois l’aurore de la liberté et de l’égalité qui se lève. Et tenez, vous aussi, vous en êtes las, vous en êtes écœurés de ce qui se passe autour de vous !... Peut-on voir de sang-froid le prolétaire souffrir constamment de la faim pendant que d’autres se gorgent.

Nous savions que la manifestation des Invalides n’aboutirait pas et cependant il fallait y aller. Nous sommes aujourd’hui en pleine misère... Nous n’appelons pas ce régime-là une république. Nous appellerions république un régime où on irait de l’avant, où il y aurait une justice, où il y aurait du pain pour tous. Mais en quoi votre République diffère-t-elle de l’Empire ? Que parlez-vous de liberté de la tribune avec cinq ans de bagne au bout ?

Je n’ai pas voulu que le cri des travailleurs fût perdu, vous ferez de moi ce que vous voudrez ; il ne s’agit pas de moi, il s’agit d’une grande partie de la France, d’une grande partie du monde, car on devient de plus en plus anarchiste. On est écœuré de voir le pouvoir tel qu’il était sous M.&nbsp;Bonaparte. On a déjà fait bien des révolutions ! Sedan nous a débarrassés de M.&nbsp;Bonaparte, on en a fait une au 18 Mars. Vous en verrez sans doute encore, et c’est pour cela que nous marchons pleins de confiance vers l’avenir ! Sans l’autorité d’un seul, il y aurait la lumière, il y aurait la vérité, il y aurait la justice. L’autorité d’un seul, c’est un crime. Ce que nous voulons, c’est l’autorité de tous. M.&nbsp;l’avocat général m’accusait de vouloir être chef : j’ai trop d’orgueil pour cela, car je ne saurais m’abaisser et être chef c’est s’abaisser.

Nous voilà bien loin de M.&nbsp;Moricet, et j’ai quelque peine à revenir à ces détails. Faut-il parler de ces miettes distribuées à des enfants ? Ce n’est pas ce pain-là qu’il nous fallait, c’était le pain du travail qu’on demandait. Comment voulez-vous que des hommes raisonnables s’amusent à prendre quelques pains ? Que des gamins aient été recueillir des miettes, je le veux bien, mais il m’est pénible de discuter des choses aussi peu sérieuses. J’aime mieux revenir à de grandes idées. Que la jeunesse travaille au lieu d’aller au café, et elle apprendra à lutter pour améliorer le sort des misérables, pour préparer l’avenir.

On ne connaît de patrie que pour en faire un foyer de guerre ; on ne connaît de frontières que pour en faire l’objet de tripotages. La patrie, la famille, nous les concevons plus larges, plus étendues. Voilà nos crimes.

Nous sommes à une époque d’anxiété, tout le monde cherche sa route, nous dirons quand même : Advienne que pourra ! Que la liberté se fasse ! Que l’égalité se fasse, et nous serons heureux !


L’audience est levée à cinq heures, et la suite des débats est renvoyée à demain.

## Audience du 23 juin

La parole est donnée à M<sup>e</sup>&nbsp;Pierre, défenseur de Pouget, puis à Pouget lui-même. M<sup>e</sup>&nbsp;Pierre défend ensuite Moreau, qui avait été arrêté pendant le procès.

*M<sup>e</sup>&nbsp;Laguerre* prend la parole le dernier en faveur des trois prévenus restés libres.

Après quelques mots de réplique de M.&nbsp;l’avocat général, M.&nbsp;le président demande aux accusés s’ils ont quelque chose à ajouter pour leur défense. Louise&nbsp;Michel, seule, prend la parole en ces termes :

Je ne veux dire qu’un mot : ce procès est un procès politique ; c’est un procès politique que vous allez avoir à juger. Quant à moi, on me donne le rôle de première accusée. Je l’accepte. Oui, je suis la seule ; j’ai fanatisé tous mes amis ; mais, alors, frappez-moi seule ! Il y a longtemps que j’ai fait le sacrifice de ma personne et que le niveau a passé sur ce qui peut m’être agréable ou désagréable. Je ne vois plus que la Révolution ! C’est elle que je servirai toujours ; c’est elle que je salue ! Puisse-t-elle se lever sur des hommes au lieu de se lever sur des ruines !

À trois heures moins un quart, le jury entre dans la chambre de ses délibérations ; il n’en sort qu’à quatre heures un quart.

Le chef du jury donne lecture du verdict. Il est affirmatif, mais mitigé par des circonstances atténuantes, en ce qui concerne Louise&nbsp;Michel, Pouget et Moreau, dit Gareau ; négatif pour les autres accusés.

En conséquence de ce verdict, Mareuil, Onfroy, Martinet et la femme Bouillet sont immédiatement acquittés.

Après une demi-heure de délibération, la cour rend un arrêt par lequel elle condamne les deux accusés contumaces, Gorget et Thierry, chacun à deux ans de prison, Louise&nbsp;Michel, à six ans de réclusion, Pouget à huit ans de réclusion, et Moreau dit Gareau à un an de prison.

Louise&nbsp;Michel et Pouget sont en outre placés sous la surveillance de la haute police pendant dix années.

*M.&nbsp;le président* : Condamnés, vous aurez trois jours francs pour vous pourvoir en cassation contre l’arrêt qui vient d’être rendu.

*Louise&nbsp;Michel* : Jamais ! Vous imitez trop bien les magistrats de l’Empire.

De violentes protestations, parties du fond de la salle, ont accueilli la condamnation des accusés. Quelques cris : "Vive Louise&nbsp;Michel !" se font entendre et c’est au milieu du bruit et des cris les plus variés que l’audience est levée.

Le tumulte se continue en dehors et le citoyen Lisbonne, qui se fait remarquer par la véhémence de ses protestations, est expulsé du Palais. La foule continue à stationner pendant quelque temps sur la place Dauphine.


## Note (de Louise&nbsp;Michel)


Puisque c’est aujourd’hui à la foule que je m’adresse, je dirai ce que je n’ai pas cru devoir dire devant l’accusation ; nous ne chercherions pas à apitoyer nos juges (chose inutile du reste ; nous sommes jugés d’avance).

Non seulement je ne me suis pas mise à rire bêtement sur une porte ; mais venant de quitter ma mère qui me suppliait d’attendre qu’elle n’y soit plus pour aller aux manifestations j’avais peu envie de rire.

Quant à choisir la boulangerie Moricet pour citadelle d’un mouvement révolutionnaire, je n’ai pas besoin de me défendre de cette absurdité.

Ce n’est pas une miette de pain, c’est la moisson du monde entier qu’il faut à la race humaine tout entière, sans exploiteurs et sans exploités.
