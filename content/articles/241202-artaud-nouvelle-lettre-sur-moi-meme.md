---
persona:
  - Antonin Artaud
title: "Nouvelle lettre sur moi-même"
slug: "nouvelle-lettre-sur-moi-meme"
date: 2024-12-18
echo:
  - rythmique
  - psychique
images:
  - "img/241202-artaud-nouvelle-lettre-sur-moi-meme.jpg"
gridimages:
  - "img/241202-artaud-nouvelle-lettre-sur-moi-meme-grid.jpg"
sourceimage: "https://commons.wikimedia.org/wiki/File:%27Two_Heads%27_by_Paul_Klee,_1932,_Norton_Simon_Museum.JPG"
notes: "La *Nouvelle lettre sur moi-même* a paru dans La Révolution Surréaliste, n° 5, le 15 octobre 1925."
summary: "C’est en ce moment pour moi une sale époque, toutes les époques d’ailleurs sont dégueulasses dans l’état où je suis. Vous n’imaginez pas à quel point je puis être privé d’idées. Je n’ai même pas les idées qui pourraient correspondre à ma chair, à mon état de bête physique, soumise aux choses et rejaillissant à la multiplicité de leurs contacts."
citation: "la connaissance par le vide, une espèce de cri abaissé et qui au lieu qu’il monte descend"
poetry: false
hangindent: false
javascript:
  - "241202-artaud-nouvelle-lettre-sur-moi-meme.js"
---


<div class="ombresmouvantes">

<div class="ombresmouvantes__texte">
<p>CHER.</p>

<p>C’est en ce moment pour moi une sale époque, toutes les époques d’ailleurs sont dégueulasses dans l’état où je suis. Vous n’imaginez pas à quel point je puis être privé d’idées. Je n’ai même pas les idées qui pourraient correspondre à ma chair, à mon état de bête physique, soumise aux choses et rejaillissant à la multiplicité de leurs contacts.</p>

<p>Et la bête mentale n’en parlons pas. Ce que j’admire, ce pour quoi j’ai appétit, c’est la bête intelligente qui cherche, mais qui ne cherche pas à chercher. La bête qui vit. Il ne faut pas que l’agrégat de la conscience se défasse. Ce qui me fait rire chez les hommes, chez tous les hommes, c’est qu’ils n’imaginent pas que l’agrégat de leur conscience se défasse ; à n’importe quelle opération mentale qu’ils se livrent ils sont sûrs de leur agrégat. Cet agrégat qui remplit chacun des interstices de leurs plus minimes, de leurs plus insoupçonnables opérations, à quelque stade d’éclaircissement et d’évolution dans l’esprit que ces opérations soient parvenues. Il ne s’agit pas de cela, il ne s’agit jamais de cela. Car si l’on devait toujours penser à sa pensée, n’est-ce pas, pas moyen de penser, de se livrer à une opération mentale, supérieure à ce qui est proprement la pensée. Et non pas l’exsudat, la sécrétion de l’esprit, mais le mécanisme de cet exsudat. J’estime avoir assez emmerdé les hommes par le compte-rendu de mon contingentement spirituel, de mon atroce disette psychique, et je pense qu’ils sont en droit d’attendre de moi autre chose que des cris d’impuissance et que le dénombrement de mes impossibilités, ou que je me taise. Mais le problème est justement que je vis. Ce qui est capable d’arracher les hommes à leurs terres, à ces terres figées de l’esprit enfermé dans son cercle, c’est ce qui sort du domaine de la pensée proprement dite, ce qui pour moi est au-dessus des relations de l’esprit. Je suis comme un aveugle au milieu des idées, toute spéculation qui ne serait pas un constat, une simple agitation de phénomènes connus m’est interdite, mais le mal à y regarder de près est que je ne vois la nouveauté, ou pour mieux dire la nécessité d’aucune opération intellectuelle. Il n’y a pas de choc dans l’esprit qui m’apparaisse le résultat d’une <em>Idée</em>, c’est-à-dire d’une conflagration nourricière de forces au visage neuf.</p>

<p>J’en suis au point où je ne sens plus les idées comme des idées, comme des rencontres de choses spirituelles ayant en elles le magnétisme, le prestige, l’illumination de l’absolue spiritualité mais comme de simples assemblages d’objets. Je ne les sens plus, je ne les vois plus, je n’ai plus le pouvoir qu’elles me secouent comme telles, et c’est pourquoi probablement je les laisse passer en moi sans les reconnaître. Mon agrégat de conscience est rompu. J’ai perdu le sentiment de l’esprit, de ce qui est proprement pensable, ou le pensable en moi tourbillonne comme un système absolument détaché, puis revient à son ombre. Et bientôt le sensible s’éteint. Et il nage comme des lambeaux de petites pensées, une illumination <em>descriptive</em> du monde, et quel monde !</p>

<p>Mais au milieu de cette misère sans nom il y a place pour un orgueil, qui a aussi comme une face de conscience. C’est si l’on veut la connaissance par le vide, une espèce de cri abaissé et qui au lieu qu’il monte descend. Mon esprit s’est ouvert par le ventre, et c’est par le bas qu’il entasse une sombre et intraduisible science, pleine de marées souterraines, d’édifices concaves, d’une agitation congelée. Qu’on ne prenne pas ceci pour des images. Ce voudrait être la forme d’un abominable savoir. Mais je réclame seulement pour qui me considère le silence, mais un silence intellectuel si j’ose dire, et pareil à mon attente crispée.</p>
</div>

</div>
