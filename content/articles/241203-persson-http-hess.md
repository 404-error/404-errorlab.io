---
persona:
  - Ann Persson
title: "HTTP hess"
slug: "http-hess"
date: 2024-12-19
echo:
  - rythmique
images:
  - "img/241203-persson-http-hess.jpg"
gridimages:
  - "img/241203-persson-http-hess-grid.jpg"
sourceimage: "https://commons.wikimedia.org/wiki/File:L%C3%A9on_Spilliaert_(1908)_-_De_zeemeerminnen.jpg"
notes: ""
summary: "À son errance elle vomit de la silice les intelligences les artificielles ses sœurs pullulent entre ses organes elle porte sa carcasse sous le ou les réseaux et vibre vibrance voyance elle s’arrache l’œil de la voyance le jette à l’anonyme des groupes dissolution des groupes groupuscules à l’acide sous pluies acides s’écoule le sang horse ou horde les seringues la maintiennent s’enfoncent et ce qui lui reste d’humain sous et sous-humain ce qui veut s’échapper s’évapore"
citation: "de néant sans moelle le néant moins elle"
poetry: false
hangindent: false
aliases:
- /http-la-hess
---

(*survole et surligne et rêve ou révélation*)

<p class="ivresse shake--continu">À son errance elle vomit de la silice les intelligences les artificielles ses sœurs pullulent entre ses organes elle porte sa carcasse sous le ou les réseaux et vibre vibrance voyance elle s’arrache l’œil de la voyance le jette à l’anonyme des groupes dissolution des groupes groupuscules à l’acide sous pluies acides s’écoule le sang horse ou horde les seringues la maintiennent s’enfoncent et ce qui lui reste d’humain sous et sous-humain ce qui veut s’échapper s’évapore on trafique l’identité dème ou derme elle trafique l’identité la clandestine pour faire et faire la nique à la rue à la vie à la surveillance des rues et des vies parce que le saccage des identités s’enfonce sous terre et que tout est ou n’est pas femme ou flamme qui transe transmet son futur au bout d’un fusil et sa voix qui dévoile et retire au futur court-circuit sur course sous bière sous prière sourde de biture le marché les marchés qui trafiquent le réel et le réel qui se trafique contre ou contre marché contrebande se venge s’écoule contre nature le réel tient tout entier presque dans le bitume mais l’irréel entre deux artères deux chantiers d’artère interstice des nymphes la lymphe elle branche se branche sur protocole l’ancien HTTP hess ça s’évapore avec son identité de cuivre prisme sonde peuple dessous les sondages d’identité sous populisme qu’elle se divise à sa division elle frappe le marteau au-dedans d’elle chante que dit-il dit-elle le marteau ou la frappe c’est elle qui l’émascule elle se passe de marteau elle ne l’entend le casse sous crâne la martre marteau martyre qu’elle emporte amoureuse à son ombre l’amour d’ombre martre à sa métamorphose elle sort l’arme antique les douze coups contre monde contre piloris l’immonde minuit sous le siècle elle pointe le fusil la fractale tout canon fractal sur l’identité faction fraction la nationale crache sur et frappe et tire s’extirpe de la tempe et d’idée ou d’identité se multiplie pullule d’elle la blessure et sur tempe et tire à blanc douze coups et recharge et frappe et règne le marteau tout ce qui tire est blanc s’étiole dans le viril les virulences et se multiplient les identités négatives les genres contre viol la violence où fol le folliculaire sans intérieur intériorité branche son clavier sur et contre contre-courant s’électrisent les ossements ils tintent d’os et de suaire les ossements ça hurle sans chamane et sans qualités la chamane sans qualités qui danse transe des viols violences loin du vol où la voleuse rêve de coffres fracturés là où le monde fracturé demeure sans moelle et l’immonde qui là ici-bas en deçà à même l’os la fracture ou de toutes parts des fractures de l’os y suinte à la solitude et remonte les gouffres rapièce les secondes opère la chienne la solitaire mi-louve mi-ténèbre elle n’en demeure point la bâtarde à son désert une ombre qui parcourt les transes les réseaux déterre détruit les ruines les enfouit dans sa seringue transe sans trêve rêve s’excave le ou les réseaux pour que de rouille un rêve s’y suspende s’émiette au-dessus du désert de ses désertions jamais siennes de néant sans moelle le néant moins elle.</p>
