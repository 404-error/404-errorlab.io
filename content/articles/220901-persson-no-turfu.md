---
persona:
  - Ann Persson
title: "no turfu"
slug: "no-turfu"
date: 2022-09-08
echo:
  - rythmique
images:
  - "img/220901-persson-no-turfu.jpg"
gridimages:
  - "img/220901-persson-no-turfu-grid.jpg"
sourceimage: "https://commons.wikimedia.org/wiki/Category:Views_of_Benidorm#/media/File:Benidorm_-_29739938907.jpg"
notes: ""
summary: "hardcore d'amour de corde de cordite & de seringue j'ai le reichstag qui crame sous skin héro’ : néant héroïne : néant moins l'être & pendant ce temps à benidorm intraveineuse : sujet verbe complément iron fist d'étoiles versus alignement constellation thugs ça chancelle l'écorchure qui pousse sous scalp the divine spectacle : j'enfume l'underclass sur un air de cuillère & flamme l'âme qui se deale en envers d'orbe & d'ormes que ne reste-t-il d'ombre à laquelle s'y pendre cellular network : holidayz du parti de l'ordre bénédiction ça te remonte la veine le building funambule je mange le lichen à même la boîte sky de rouille sur minuit des go fast plongée sous acid rain toute shadow la forêt des cendres jetée sur nowhere la ténèbre tachetée sachets & migrants : flottaison"
citation: "hardcore d'amour"
poetry: true
hangindent: true
---

hardcore d'amour

de corde de cordite & de seringue

j'ai le reichstag qui crame sous skin

héro’ : néant

héroïne : néant moins l'être

& pendant ce temps à benidorm

intraveineuse : sujet verbe complément

iron fist d'étoiles

versus alignement constellation thugs

ça chancelle l'écorchure qui pousse sous scalp

the divine spectacle : j'enfume l'underclass

sur un air de

cuillère & flamme

l'âme qui se deale en envers d'orbe

& d'ormes que ne reste-t-il d'ombre à laquelle s'y pendre

cellular network : holidayz du parti de l'ordre

bénédiction ça te remonte la veine le building

funambule je mange le lichen à même la boîte

sky de rouille sur minuit des go fast

plongée sous acid rain

toute shadow la forêt des cendres

jetée sur nowhere la ténèbre tachetée

sachets & migrants : flottaison

couper la white trash

loin loin de vodka en écho des basses

bpm béton qui pèle l'impavide

pavés & pavots

sous l'exta l'extase : communisme des fosses

la série le sériel de

balkans & balconies jusqu'à

l'en bas de ligne acht-acht

road movie & void sous vomissure

j'ai les araignées qui me font un casse sous crâne

me regonflent la soldatesque

d'acides d'hacienda

& le skinhead à love rouge

qui s'émiette sous un schwarze sonne d'alcool & de mélancolie

le lyrisme is a trick de social-traître

seul traum de dass pour vriller du piston

where is the direction of das lumpenproletariat

trabajar reubeu no trap

qui d'esclave kif-kif l'astre sans pink paradise

elle est loin la guerra del rif

& ça bouffe l'orange d'europe de nuit d'hiver

ça te fait la morale après l'happy hour

faut pas polluer faut pas copuler

bock malthus pour cop brutus

néolibation l'assomption : tapis de prière & needle in the k

t'as encore la grave gravy qui goutte à la plaie

as-tu déjà vu l'œil de la vache avant qu'on ne la saigne

comme ce petit bâtard de saint paul qui

te renverse ta tchoin la cross sous sainte la ceinture

claque de bras l'eisernes kreuz

ça se plante d'avant en rade le radial

les braco 6mm & radius piqueté de songes

background : radioactive waste & big splash en mer fluo

pompe kkkrucifixxx en croix de néons

pousse-pousse seul tout d'avant d'apo

ou d'après-kkkrist gave gab gabber

la frog plantée dans l'axe l'axillaire

la haine de nation nationale

pass premium pour papiers

uber-eats-tes-morts

t'inquiète t'as le tieks slave à demeure

pour le concrete modernism

d'architectes (que l'on exécutera un jour)

remember remember ces gauchistes d'état

leur ego trip courtisan qui te pave de ciment l'urban dream

mais faim ô faim : gastro d'orient pour prémices des chiures

smartphone : dumbass sur son scooter

ça livre & ça la ferme le keubla

avec sa peau d'orange

cicatrice : le gilet de sauvetage qui vacille

la survie les life vests en far west

gardés à l'os jusqu'in the townsville

de bitume en biture

of those titistes de salon aux tits libres

qui s'empiffrent de discounts pour food delivery 

ça se dévore que dalle en street la cuisine de rue 

fancy food des pauvres : en trois pièces entre-soi

petite-bourgeoisie créa’ sur marge de gentrified area

loyer subventionné : meubles vintage & bibliothèques en attente d'autodafé

indignation sociétale : master of arts & petits-fours

party time : rien à foutre du wage labor

à la rue le bruit

un peu de rap pour l'ambiance

fuck la culture

fuck leur culture

nous le scaphandre d'archange sous md-aime

& aime-moi

& mate d'inferno white spirit

venelle à sens unique

l'à rebours portbou

ce bout d'artère dans la gomme des monkey boots

crache les teeth l'oseille la retraite à soixante-cinq balais

t'articule l'anima l'animal pour un peu d'éther

& ses très enfoncées starz de la black oil night

elles ont encore le goût birdy des fossiles

& les halogènes qui s'avalent

& qui s'effritent sub-babylone

la livre d'heures de brune

crise sublinguale : sniff d'untercyber

sac mortuaire but the body is fine sous zip biodég-dégage

d'undead pour cimeter-ter banlieue nord

born again d'o.d.

anno domini : d.o.a.

revival pillz pitiés & pituites

cul sec à gueule de faf

l'hack l'âcre & le sac plastique

serre-tête noose knot d'oreilles catz

led-lsd le sahara ça ira rien

pastel droite-droite

rechtsex & extremism-maousse

t'as le becoming qui pousse au black

thunderdome vs thunder double

d'athmos l'eva brown-sugar

en iench walou de tout

nous la peau tarba

tout à la coule la courbe la couleur

nasdaq-qatar : je vois des quasars & des guillotines

des galz & des galettes

& s'évident les ciels & les eaux des chiottes

pipe à crack pour astrolabe

millennial.sm

music pop pogo pogrom

trief trip mon aurore de cristal

ils viendront nous chercher à six

like le bon vieux temps en drei-drei

bière de slaves à la bave

dans les narines le south-med

& notre unterblood qui t'enculte

le riot des macchabées plus juicy que tout sunset bvd

slogan : de la meth pour les sans-pap

but the pollz en commodification de nos assassinats

the big lie en prime time

& ils nous aligneront tu verras

nous la lèpre lèvre ouverte

on goûtera la question d'again

délice y verrons-nous

la balle de smog

l'inespoir avant le trou

la détonation avant le noir

entendrons-nous

la cervelle du très au fond de son reptile

nos entrailles d'éventail l'avant la veille

la saurons-nous la calcination du calcaire

lime & tequila au potter's field

des fantômes likeront la picture

notification : dopamine

new comment : vous êtes du feu

mais pourtant nul ne nous brûla

id card : l'anonymous des vers

sans la brûlure aucune ne se déverse

ce peu de plomb a suffi

& nous nous kifferons dans

l'extinction la chaux la chute

memories : selfie devant l'armengrab

ein wenig blei is enough

bliss & pisse un peu

sur nos tombes

p.s. : n'oublie pas de voter pour le parti socialiste

(histoire que nos ghosts te niquent ta race)
