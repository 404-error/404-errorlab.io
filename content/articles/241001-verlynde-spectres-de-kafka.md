---
persona:
  - Marc Verlynde
title: "Spectres de Kafka"
slug: "spectres-de-kafka"
date: 2024-10-03
echo:
  - esthétique
  - stratégique
images:
  - "img/241001-verlynde-spectres-de-kafka.jpg"
gridimages:
  - "img/241001-verlynde-spectres-de-kafka-grid.jpg"
sourceimage: "https://commons.wikimedia.org/wiki/File:Photograph_by_Franz_Kafka.jpg https://commons.wikimedia.org/wiki/File:Franz_Kafka_on_a_sanatorium.jpg https://commons.wikimedia.org/wiki/File:Kafka_in_the_snow.jpg https://commons.wikimedia.org/wiki/File:Kafka_on_a_sanatorium.jpg https://commons.wikimedia.org/wiki/File:Kafka_in_Z%C3%BCrau.jpg"
notes: ""
summary: "Il paraît, aujourd'hui, impossible de ne pas parler de politique ; il semble pratiquement impossible d'en dire autre chose que le détachement, l'impuissance. Il est impossible, aujourd'hui, de parler de Franz Kafka ; on le doit. Facile paradoxe, on le veut sans refuge. Des brisures & des inachèvements où inventer Kafka — obstinée volonté de non-appartenance. On se déplace dans un intenable en deçà du commentaire. On pastiche, par athéisme, les manières dont les paraboles talmudiques font de l'herméneutique un acte, de la dissociation dialogique un instant de la compréhension. Avant que cet équilibre, ce repos, ne redevienne inacceptable. Une manière d'interpréter l'essentiel des intrigues des fragments dont les romans de Kafka sont constitués."
citation: "soyons, tel Kafka, fragmentairement inachevés"
poetry: false
hangindent: false
---

Il paraît, aujourd'hui, impossible de ne pas parler de politique ; il
semble pratiquement impossible d'en dire autre chose que le détachement,
l'impuissance. Il est impossible, aujourd'hui, de parler de Franz
Kafka ; on le doit. Facile paradoxe, on le veut sans refuge. Des
brisures <a href="#et02" id="et01">&</a> des inachèvements où inventer Kafka --- obstinée volonté de
non-appartenance.

On se déplace dans un intenable en deçà du commentaire. On pastiche, par
athéisme, les manières dont les paraboles talmudiques font de
l'herméneutique un acte, de la dissociation dialogique un instant de la
compréhension. Avant que cet équilibre, ce repos, ne redevienne
inacceptable. Une manière d'interpréter l'essentiel des intrigues des
fragments dont les romans de Kafka sont constitués.

Maintenant, dans notre idiotie, quelles interprétations peut-on encore
en faire entendre pour y écouter, hallucination fatiguée, une vision
politique pour notre maintenant ?

Athéisme : intranquille inachèvement. De ses récits, Kafka omet la fin,
rend patent la désormais impossibilité de finir un roman, de l'enfermer
dans une Loi qui, dans la sanction, trouverait sa finalité. On veut,
pour aujourd'hui, une politique qui sans trêve réactualise les
contre-jours, les contestations, d'une perpétuelle interprétation de
cette vacance de Loi où se débattent ses protagonistes. Une vision
fantastique, douteuse, d'un personnage qui toujours précipite son
déplacement, son absence de place.

Jamais, dans n'importe quelle société, tu n'auras ta place. Serait-ce
cela l'intuition politique de Kafka ? À chacun d'en tirer l'intenable
interprétation. Athéisme.

Les différents fragments des romans de Kafka, l'implosion d'une
linéarité romanesque qu'ils imposent, fomenteraient (chaque société
idéale, révolutionnaire, fomenterait ainsi son renversement) la
distanciation de ce qui jamais, précisément par son évidence visuelle,
ne se veut unilatéral.

De Kafka, aujourd'hui, on réclame le concret. Les échos d'un fragment
l'autre : "le cirque d'Oklahama" en regard de "Dans la cathédrale".
D'une intraduisible, sans traces, disparition comme l'indique le titre
du roman américain de Kafka : s'il dessine un individu politique ce
serait celui, sans exemplarité, qui part sans laisser ni trace ni fin,
qui fait du citoyen un spectre sans élection, un revenant qui hante
l'impossible bonne conscience de toute société stabilisée. On confie
tout ceci comme un doute, une pensée inachevée.

À l'évidence, tant c'est cela que met en jeu la prose de Kafka, cette
pensée de la non-appartenance ne nous appartient pas, elle est sans
possible possession. Nous sommes spectres par lancinant scepticisme,
interprétations, arguties dont un instant se réclamer comme pour mieux,
le suivant, en être chassés. Effacé reflet de ce qui anime chaque
personnage de Kafka.

Mettre ainsi en scène les vestiges d'une croyance, exaspérer la
dérisoire possibilité d'y croire encore : illustration concrète de
l'impossibilité de proposer une interprétation nouvelle de Kafka. Se
confondre alors avec l'infinité de celles existantes. La question
politique que pose Kafka serait le rapport que notre contemporain
entretient avec la difficile liberté, l'impossible <a href="#et03" id="et02">&</a> nécessaire
émancipation qu'il met, littéralement, corporellement <a href="#et04" id="et03">&</a> cocassement, à
l'épreuve dans ses récits fragmentés.

> Le tribunal était attiré par la faute, d'où il résultait que la salle
> d'instruction ne pouvait manquer de donner sur l'escalier que K.
> choisissait au hasard.

Question de lieu, de fantômes : faire revenir le spectre du judaïsme
dans une église --- "Dans la cathédrale", le célèbre chapitre en
fragments du *Procès* --- semble aussi fantastiquement burlesque ---
grinçante incarnation à laquelle on ne saurait s'abstraire --- que de
laisser apparaître l'impossible d'une citée céleste dans le cirque
terminal d'*Amerika*. Dans l'arbitraire coïncidence de ces deux lieux,
une lecture politique de Kafka serait spectral déplacement de sens : une
tangence d'inquiétude <a href="#et05" id="et04">&</a> de scepticisme, la sidération que susciterait la
survenue d'un fantôme auquel on ne croit pas entièrement, le doute que
pourtant on partage face à l'évidence, jusqu'au burlesque donc, du
flottement où nous laisse cette apparition.

Exacte <a href="#et06" id="et05">&</a> incertaine incarnation du lien, douteux <a href="#et07" id="et06">&</a> patent, qui unit
Kafka à ses personnages. Une suggestion où encore --- toujours dans
l'erreur, dans l'impossibilité désormais de la faute qui nous situerait ---
en laisser entendre les significatives discordances. Musique
chaotique jouée, comme l'auteur lui-même, par un non-musicien, un athée
qui se moque et, ainsi, aspire à l'improbable présence de Dieu dans un
contradictoire, juif, dialogue interprétatif. Ce soupçon de remords qui,
peut-être, agite les fantômes.

> Le Messie ne viendra que lorsqu'il ne sera plus nécessaire, il ne
> viendra qu'un jour après son arrivée, il ne viendra pas au dernier,
> mais au tout dernier jour.

Karl Rossman, arrivé à ce qui ne saurait être le terme de son périple,
dans l'inexistant Okhlahama, dans un cirque dans la pantomime résiste à
toute interprétation définitive, usurpant la place d'une jeune fille,
souffle dans une trompette l'impossible bonne nouvelle devenue aussi
discordante qu'inintelligible. Plus personne n'écoute, n'espère, ne
croit aux fantômes, aux messies ; chacun est trop occupé à trouver sa
place qui dans ce cirque est promise pour tous, pour personne.

Pour aujourd'hui, dans un volontaire, anachronique <a href="#et08" id="et07">&</a> fantomal, décalage,
Kafka propose outre ses spéculations sur l'élection, une radicale mise à
la question de l'ambition qui en serait une burlesque, inquiète,
caricature. Nous voilà réduits, comme ses protagonistes, à nous leurrer
sur notre place, notre position sociale.

Osons, communisme de Kafka : nous sommes tous des exilés, personne n'a
sa place, faisons secrète société à partir de cette radicale
non-appartenance. Soyons, sceptiques, spectres, ne nous laissons pas
appréhender dans une fonction durablement définie, par l'importance, le
sens, qu'autrui lui prêterait. Vivons le cauchemar comique qui sans
cesse nous chasse.

Soyons, tel Kafka, fragmentairement inachevés, pure distanciation à
cette mise en récit de Soi dans laquelle nous ne devrions reconnaître
que le flottement, le rejet passionné de discours <a href="#et09" id="et08">&</a> de diktat qui point
nous définissent, dessinent des bribes <a href="#et10" id="et09">&</a> des fantômes. Une sidérante
non-appartenance aux mythes, aux peurs, aux croyances collectives dont,
au passé, l'interprétation, la mise en discussion donneraient une
concrète incarnation.

Dans la cathédrale, Joseph K. rencontre --- dans un fragment qui
viendrait contredire la fin déjà écrite, sanctionnée, du *Procès* --- un
religieux au statut incertain, intraduisible. Ce prophète, peut-être
pour rire, sans doute arrivé trop tard, dans ce retard qui est cette
mécompréhension perpétuelle au centre des fragments kafkaïens, cet
aumônier des prisonniers, sans statut social, ne pouvant donc en donner
un à Joseph K., met en discussion, en dialectique, entre aspiration à la
soumission <a href="#et11" id="et10">&</a> contestation, une allégorie talmudique sur celui venu
d'ailleurs, de la campagne, qui, ne connaissant rien à la loi hébraïque,
croit pouvoir, y parvient dans certaines lectures, la remettre en cause.

Chaque société, suggère pour maintenant Kafka, pourrait se fonder sur
cet amalgame, cette alternance, pour ne pas dire cette dialectique du
maître <a href="#et12" id="et11">&</a> de l'esclave, entre celui qui se croit le gardé <a href="#et13" id="et12">&</a> celui qui se
pense le gardien, celui reconnu pour son savoir <a href="#et14" id="et13">&</a> celui qui l'interroge,
ne cesse de demander ce qui devrait être gardé. Absence plurivoque,
incertaine, dont nous semblons les spectres, miroirs des messagers du
*Château,* dont la fantomale fonction n'existe que par la croyance, son
partage en dialogue, que parfois, à l'ombre du doute <a href="#et15" id="et14">&</a> du burlesque, on
lui accorde. Soyons seulement arpenteurs, hantés par une herméneutique
politique.

> K. ne cessa pas d'avoir le sentiment qu'il s'égarait ou qu'il
> était parti loin dans l'inconnu, plus loin qu'aucun humain n'était
> jamais parti avant lui, dans une terre lointaine où même l'air ne
> possédait aucun des comportements de l'air natal, où l'on ne pouvait
> qu'étouffer d'étrangeté et dans les séductions insensées de laquelle
> on ne pouvait malgré tout rien faire d'autre que continuer à avancer,
> continuer à s'égarer.

Dans un geste fantastiquement politique, écrire pour n'être pas là.
Jamais très loin, cependant. *Unheimlich*. Proximité d'une extériorité
ironique, inachevée <a href="#et16" id="et15">&</a> intranquille, pour figurer l'acuité
cauchemardesque d'un regard social. Kafka outrepasse la prophétique
projection paperassière, [*Kanzleipapier*](https://shs.cairn.info/revue-raison-publique-2017-1-page-63).
Peut-être seulement comme un
reflet de cette inquiète impuissance avec laquelle on traverse cet
étrange, forcément, moment politique, on y lit l'insoutenable
disparition du rêve collectif de l'ailleurs, la figuration d'un
irrécupérable étranger. Ceux qui arrivent trop tard, en sont rejetés, le
construisent de leur sueur, suggère *Amerika*, fomentent le mirage de
l'*american way of life*.

À l'écart d'une véritable théorie politique, dans le fragment qui en
réfute la systématisation, Kafka confie le concret d'une expérience du
déplacement, dans ses grincements <a href="#et17" id="et16">&</a> inadaptations, il saisit la
contondante banalité de l'oppression. S'abstraire de toute idéalisation
de la pauvreté, montrer que tout regard sur elle la traverse dans une
momentanée usurpation d'identité puisque telle serait la moins
imparfaite de nos figurations sociales. Incertitude, ironie, biffure
presque de ce qui est peut-être une coquille corrigée par l'encombrant
Max Brod : juste avant sa disparition dans un prophétique train, Karl se
nomme lui-même *Negro*. Suspendons l'interprétation pour mieux souligner
la manière dont ce roman décrit l'émancipatrice errance d'une
soustraction à l'oppression familiale, à cet oncle capitaliste achevé, à
cet esclavagisme subalterne de l'hôtel *Occidental...*

Identification, usurpation, intraduisible métamorphose. Révélation en
retard, toujours à reprendre. Agent commercial que sa cauchemardesque
immobilité renvoie à notre fondamentale absence de statut social, Gregor
Samsa est transformé en vermine, littéralement, en allemand, ce que l'on
peut, devrait donc presque, écraser sous son pied. Arpenteur sans appel,
K. transite dans la précarité financière, dans cette fragilité d'une
identité sociale dont Kafka sans cesse rappelle la fugacité, interroge
la pulvérulente absence de bien-fondé. Joseph K., lui, est *fondé* de
pouvoir : coupable par représentation, fautif d'avoir cru se fondre dans
une reconnaissance par ambition, par une parodie d'élection seulement
sociale. Peut-être projettent-ils, maintenant, sur nous cette ombre de
mauvaise conscience qui dessine ce que l'on prend, dans un trompe-l'œil,
un discours idéologique, pour notre place dans l'absurdité sociale.

> C'est trop, ce trop ne peut pas être atteint. C'est comme si quelqu'un
> était prisonnier et avait non seulement l'intention de s'évader, ce
> qui serait peut-être possible, mais aussi et en même temps,
> l'intention de transformer la prison en gentilhommière à son usage. Or
> s'il s'évade, il ne peut pas faire de transformation, et s'il
> entreprend des transformations, il ne peut pas s'évader.

Au mieux, nous sommes sympathiques pantins par mimétique confusion.
Douteuse identification à l'indéniable plaisir romanesque auquel livrent
les romans de Kafka. Pas plus qu'il ne se reconnaît fatalement,
entièrement, dans ses personnages, on ne se retrouve dans ses différents
anti-héros. Une sorte de distance, cent ans.

Alors, au-delà des surinterprétations, dialogues <a href="#et18" id="et17">&</a> contestations,
arguties <a href="#et19" id="et18">&</a> *pilpouls*, que font entendre les fragments kafkaïens, d'une
manière assez canonique, on y lit une mise à la question --- pratique
politique s'il en est --- de l'impossibilité, de la nécessité, d'une
justice. Ou plutôt, jamais achevé puisque l'on ne peut entièrement s'en
réclamer, qu'il n'appartient que comme un mauvais rêve : un désir
inassouvi de sanction, une traduction de la Loi secrète, peut-être pure
interprétation de son anarchiste absence enfin loisible, qui
fomenterait, sans les régir, nos sociétés. Au souhait de s'y soustraire
s'ajoute la volonté, définitive disparition, de se soumettre à sa
grandeur, son sens, sans cesse repoussés. Pour maintenant, il faudrait
figurer les cauchemardesques, prophétiques <a href="#et20" id="et19">&</a> contestataires, ressorts.
Croire y échapper, affronter, comme on transite d'un fragment l'autre,
cette définitive absence de sanction dans laquelle on se débat.

Cependant, ne pas réduire la politique à ses problématiques aspects
concrets. Le roman, quand il pense par scène, lui, sait le faire.
L'hypothèse est aussi fantastique que n'importe quelle interprétation :
si Franz Kafka véhicule une politique, une fois encore elle paraît
passer également par un refus de la linéarité, une manière de
s'abstraire de l'autorité par une stricte, hasardeuse, athée,
rétractation de la paternité. On touche d'ailleurs ici à l'impossibilité
de parler de Kafka. On ne saurait réduire son œuvre à la si
désespérément lucide protestation, à l'examen de conscience
qu'obligerait une culpabilité toujours en procès, de *La Lettre au
père*. On ne saurait, non plus, en faire l'économie.

Il persiste alors cette nécessaire précision : le flottement fantomal
des fragments de Kafka ne conduit à nulle résignation, à refuser
engagement <a href="#et01" id="et20">&</a> lutte ou à une sorte de terrorisée acceptation. Peut-être
n'avoir aucune place, contester la possibilité même d'en avoir une, ne
revient pas (ou pas seulement) à s'absenter du monde, mais à sans fin,
qui sait, esquisser des façons de maintenir l'effarante vacance de tout
pouvoir.
