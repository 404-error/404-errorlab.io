---
persona:
  - Cécile Toussaint
title: "Interrogatoire"
slug: "interrogatoire"
date: 2020-10-21
echo:
  - rythmique
images:
  - "img/201021-toussaint-interrogatoire.jpg"
gridimages:
  - "img/201021-toussaint-interrogatoire-grid.jpg"
sourceimage: "https://commons.wikimedia.org/wiki/File:A_Viet_Cong_suspect,_captured_during_an_attack_on_an_American_outpost_near_the_Cambodian_border_in_South_Vietnam,_is..._-_NARA_-_542340.tif"
notes: ""
summary: "Une pièce sans fenêtres. Quatre murs, sol et plafond. Aucune porte. Parallélépipède parfait. Chaque face étant surface miroitante, l'espace est l'infini, ou, plus vraisemblablement, est l'infini d'un abîme. Aucune source de lumière. Et pourtant l'espace demeure éclairé. Comme si la lumière habitait en soi le cloisonnement. Comme si les reflets devaient répondre aux reflets. Comme s'ils devaient leur imposer de rendre compte au visible. Ne pas avoir le luxe de l'obscurité."
citation: "L'humain. Ni femme ni homme. Tout de cendre."
---

Une pièce sans fenêtres. Quatre murs, sol et plafond. Aucune porte. Parallélépipède parfait. Chaque face étant surface miroitante, l'espace est l'infini, ou, plus vraisemblablement, est l'infini d'un abîme. Aucune source de lumière. Et pourtant l'espace demeure éclairé. Comme si la lumière habitait en soi le cloisonnement. Comme si les reflets devaient répondre aux reflets. Comme s'ils devaient leur imposer de rendre compte au visible. Ne pas avoir le luxe de l'obscurité. Comme si l'abîme était la lumière même. Au pourtour de la pièce, accolée à chaque mur, une rangée de chaises qui ne laisse aucune place pour atteindre le mur, chaises dont la matière, à la façon des autres surfaces de cet espace, se contente de miroiter les rayons qui la rencontrent. Le centre de la pièce. Un tabouret. Matière identique, miroir et indistinction. Mais sur le tabouret, hiatus des perditions, une forme. Ou toutes les formes. L'humain. Ni femme ni homme. Tout de cendre. Dont la vie ne se perçoit qu'au faible battement de son pied. L'humain, par le pouvoir de réflexion de l'espace, devient l'humain à l'infini. L'humain à l'infini qui bat faiblement du pied. Et qui répond une même parole. Tout autant faiblement que le battement de l'espace, avec cette même constance de la cendre. Reflets infinis de la parole. Parole qui se résume à un mot. Mot qui se contente de la syllabe. Syllabe qui ne dit que sa négation. Non. Ou est-ce la syllabe qui désigne. Nom. Qu'importe puisque la réalité tout entière des miroitements s'efface dans la constance de cette réponse. Parce que le son est une réponse. Et que le son se dilate. Qu'il a son espace propre. Opaque. Qu'il obstrue la libre circulation des rayons. Et la libre circulation de toute autre réponse possible. Il est là comme une fragile tentative de résister. Le nom est celui de la résistance. Mais le nom demeure une réponse. Mais le nom demeure réponse à la question qui demeure l'inaudible. La question ne s'entend pas. De même que la lumière ne s'entend pas. De même que la lumière unique et totale s'étale comme l'évidence des surfaces. Dont le dialogue du même au même soumet à la question toute dissonance. L'humain répond non. L'humain se contente de la négation. L'humain se situe dans la négation. L'humain se nomme négation. Sans arriver à faire joindre son être à la négation. De même que la lumière ne s'entend pas. De même que la lumière unique et totale s'étale comme l'évidence des surfaces. La question du vide. Et la question du vide qui demeure sans réponse. La cendre, ou peut-être est-ce l'humain, répond. La cendre et l'humain répondent non. Le vide comme réponse au vide. Non. Une surface de cendre ou d'humain qui dit non. La surface qui dit. Et qui dit non.

