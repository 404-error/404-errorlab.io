---
persona:
  - René Crevel
title: "Colonies"
slug: "colonies"
date: 2020-10-11
echo:
  - rythmique
  - stratégique
images:
  - "img/201011-crevel-colonies.jpg"
gridimages:
  - "img/201011-crevel-colonies-grid.jpg"
sourceimage: "https://www.flickr.com/photos/statelibraryqueensland/8570049929/"
notes: "Ce texte a paru en juillet 1930 dans le numéro premier de la revue *Le Surréalisme au service de la révolution*."
summary: "Si l'intelligence est fonction de la vacherie, autour de tes discours, nous devinons, cher Émile, que tu es le plus malin des Pipi la Buré. Mais, que diras-tu, le beau matin que les Patagons, s'étant aperçus enfin de leur mission civilisatrice, tu devras faire le zouave dans leur charmante et lointaine Patagonie ?"
citation: "de supprimer les frontières mais de les délayer à la sauce vitriol"
---

*L'histoire coloniale n'est pas idyllique, mais à moins d'être un ignorant ou un fripon, on est bien forcé de reconnaître que l'indigène est mieux traité par le conquérant, même cruel, que par ses propres chefs. Pour ce qui est de notre pays, il a poussé jusqu'à l'imbécillité sa générosité.* Émile Buré (*L'Ordre*).

Si l'intelligence est fonction de la vacherie, autour de tes discours, nous devinons, cher Émile, que tu es le plus malin des Pipi la Buré. Mais, que diras-tu, le beau matin que les Patagons, s'étant aperçus enfin de leur mission civilisatrice, tu devras faire le zouave dans leur charmante et lointaine Patagonie ? Va donc voir, en attendant, au-dessus de Saint-Raphaël, les jolies tombes que notre beau pays *généreux jusqu'à l'imbécillité* a bien voulu offrir à ses sujets indigènes qui eurent le tort de ne point s'acclimater.

{{%asterisme%}}

Et la résignation chrétienne de nos journaux, lorsqu'ils ont à nous dire, un jour de Pentecôte, que la police de Calcutta *dut* tirer sur une foule de 1.500 indigènes qui se livraient à la fabrication du sel ?

Ça va, ça va.

Et les poursuites contre ce professeur qui n'applaudit point à notre œuvre colonisatrice ?

Et la petite guéguerre que nous mijote Mussolini, pour mieux exporter ses chemises noires (il faut de la lingerie gaie) dans le Nord de l'Afrique ?

Et les souvenirs centurionesques de toute notre chère civilisation méditerranéenne ?

Et cette vieille voyoucratie phocéenne, qui organise l'univers comme un bordel ?

Et le délire paneuropéen, qui nous barbouille d'étranges mappemondes, où s'opposent, en grandes flaques, les continents, car il ne s'agit point pour ces messieurs paneuropéens de supprimer les frontières mais de les délayer à la sauce vitriol.

Allons, courage et confiance, Pipi la Buré, les vieux gagas, les petits amis ont si bien fait joujoute avec l'univers que, bientôt, l'Océan lui-même ne sera plus qu'une eau de vaisselle patriotarde.
