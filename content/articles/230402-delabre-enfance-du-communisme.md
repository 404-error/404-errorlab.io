---
persona:
  - Pierre-Aurélien Delabre
title: "Enfance du communisme : de la littérature comme champ stratégique"
slug: "enfance-du-communisme"
date: 2023-04-25
echo:
  - stratégique
  - esthétique
images:
  - "img/230402-delabre-enfance-du-communisme.jpg"
gridimages:
  - "img/230402-delabre-enfance-du-communisme-grid.jpg"
sourceimage: "https://commons.wikimedia.org/wiki/File:Paul_Klee_~_Engelshut_~_1931.jpg"
summary: "Le langage poétique se voit engagé dans une éternelle guerre de position contre la langue du pouvoir, sa police et ses académies. Mais peut-il sortir de sa confrontation stationnaire à l'*ordre des choses*, et se doter d'une fonction active au sein d'un processus révolutionnaire ? Autrement dit : la poésie est-elle nécessairement vouée à regarder le monde pourrir ? Les mouvements révolutionnaires ont toujours activé \"des espaces dans l'espace\", espaces où des actions poétiques, plurielles, compulsives, effrénées ont su se manifester et furent le plus souvent réprimées par le pouvoir central."
notes: ""
citation: "la poésie est-elle nécessairement vouée à regarder le monde pourrir ?"
poetry: false
hangindent: false
---

Le langage poétique se voit engagé dans une éternelle guerre de position
contre la langue du pouvoir, sa police et ses académies. Mais peut-il
sortir de sa confrontation stationnaire à l'*ordre des choses*, et se
doter d'une fonction active au sein d'un processus révolutionnaire ?


Autrement dit : la poésie est-elle nécessairement vouée à regarder le
monde pourrir ?

Les mouvements révolutionnaires ont toujours activé "des espaces dans
l'espace[^1]", espaces où des actions poétiques, plurielles,
compulsives, effrénées ont su se manifester et furent le plus souvent
réprimées par le pouvoir central.

Ce fut le cas de Vladimir Maïakovski, de Sergueï Essenine, d'Ossip
Mandelstam, des poètes de langue yiddish lors de la mal nommée "Nuit
des poètes assassinés" en 1952, et la liste est si longue que nous
choisissons ici seulement d'évoquer celles et ceux que nous connaissons
et que nous aimons, celles et ceux dont les "suicides", les
humiliations, les exécutions nous pèsent encore, et dont nous estimons
qu'ils participèrent activement à l'esprit et à la lettre de la
Contre-révolution.

Tout processus émancipateur se caractérise par une éruption et un
"partage du sensible[^2]". Pour autant, il se n'agit pas de
constituer le langage poétique comme prérequis à toute action
révolutionnaire, mais de faire primer le fait positif et autonome d'une
expression poétique sur le "règne des fins", même dans le cas où ces
fins se présentent à nous comme "révolutionnaires".

Ce pourquoi nous suivons Michel Leiris quand il nous dit que l'art ne
doit aucunement être réduit à une fonction illustrative ou
représentative, qu'il ne peut avoir pour vocation d'incarner ou de
matérialiser du discours --- même communiste, même révolutionnaire ---,
que son autonomie est gage d'une société engagée dans une émancipation
totale[^3] :

> Si la Révolution appelle un art et une littérature révolutionnaires par
> leur signification immédiate, il ne faut pas que cette exigence tactique
> soit satisfaite au détriment de la stratégie et qu'on oublie que, pour
> être totalement révolutionnaires, autrement dit, pour répondre à tous
> les besoins de la Révolution, un art et une littérature ne doivent pas
> simplement viser à exalter, propager ou orienter l'esprit
> révolutionnaire, mais tendre, au moins par certains de leurs aspects, à
> transformer d'ores et déjà en préfiguration du futur "homme intégral"
> l'homme d'aujourd'hui qui commence à peine à se défaire de ses chaînes.
> D'où la valeur révolutionnaire de toutes les œuvres qui tendent à ruiner
> les stéréotypes rassurants sur lesquels l'homme aliéné croit pouvoir se
> fonder, soit qu'elles bouleversent de fond en comble la vision que l'on
> a du monde (Picasso), soit qu'elles donnent une conscience plus brûlante
> de la condition humaine (Kafka), soit qu'elles découvrent à l'homme et à
> la femme les ambiguïtés et les doubles fonds de leurs désirs
> (Bataille)[^4].

L'art poétique est expérimental par essence. Ce qui signifie qu'il
engage son créateur sur une voie qu'il ne connaît pas. Et si le poète
campe dans sa forge, ce n'est pas tant pour réaliser un objet qu'il
aurait déjà en tête (un discours joliment imagé par exemple), ou une
certaine représentation de cet objet (l'autoportrait du pouvoir), que
pour découvrir un rapport nouveau.

Nous nous situons sur un plan différent de celui du travail de l'artisan
dont Marx nous dit que c'est le fait d'avoir conçu son œuvre *dans sa
tête* avant de la réaliser *avec ses mains* qui le distingue de
l'abeille[^5]. Rabattre la création artistique sur la production
artisanale --- et bien que nous comprenions la nécessité de désidéaliser
la figure du créateur --- tend à rabattre l'acte expérimental sur l'acte
producteur d'objets *utiles* à notre vie, à circonscrire ainsi toute
forme de travail artistique à la production de valeurs.

*A contrario*, ce sont précisément les caractères expérimentaux,
heuristiques de l'acte créateur qui en font quelque chose de réellement
révolutionnaire :

> Travailler sans directives données de l'extérieur, sans idées préconçues
> --- ou presque --- et comme s'il allait à la découverte, c'est sans
> doute le meilleur moyen pour l'artiste ou l'écrivain d'échapper aux
> stéréotypes et de faire ainsi œuvre vraiment authentique et créatrice.
> Pour le créateur, il ne s'agit pas d'accomplir un "chef-d'œuvre",
> --- notion qui se réfère à l'époque ancienne où, pour être reçu maître dans
> une corporation, il fallait faire ses preuves d'habile artisan en
> exécutant une œuvre reconnue proprement magistrale. Cette notion garde
> encore un sens en société capitaliste, celui d'œuvre digne par
> excellence d'être acquise par un amateur ou par un musée, mais elle ne
> peut être admise en société révolutionnaire, puisqu'elle suppose que
> l'œuvre est traitée comme une denrée susceptible d'être plus ou moins
> recherchée. Pour le créateur, il s'agit toujours d'expérimenter et de
> s'aventurer : quand il commence un travail, il ne sait pas exactement où
> cela le mènera et c'est, justement, pour savoir où cela le mène qu'il
> travaille. Très précisément, c'est cette façon d'avancer comme on
> débroussaille à coups de machette qui s'appelle "créer"[^6].

L'art implique donc son sujet en l'excentrant, en le projetant, par
l'usage créateur de la langue (écrite, orale, cryptique) dans le langage
de la réalité. Ce langage reste à découvrir : ce n'est pas le poète qui
fait le poème lucide, mais le poème qui fait le poète voyant.

Par-delà toute morale, par-delà toute grammaire, l'acte créateur
repousse les hiérarchisations induites par le Savoir ; l'expérience
artistique se faisant ainsi l'objet d'un "connaître" qui brise les
schémas existants de la connaissance.

Par sa pratique même, dans sa contestation de l'ordre du Discours, dans
son subvertissement du régime de la Représentation, l'art est relation à
soi, aux autres, sur le mode d'une confrontation expressive à l'altérité
--- *je est un autre* : relatif notamment à ce "double fond" du désir,
à nos ambiguïtés les moins assignables dans la grammaire du connu.

Michel Leiris, encore :

> Dans le domaine artistique et littéraire, un créateur ne peut pas être
> un homme satisfait de la culture existante. Ce qui le pousse à la
> recherche, c'est le besoin de rompre avec ce qui existe et de faire
> autre chose. De sorte qu'une société, même communiste, ne peut prendre
> des mesures visant à l'"encourager", car cela tendrait *ipso facto* à
> le domestiquer. Elle ne peut que lui garantir l'exercice de son absolue
> liberté d'investigation. Cela, sans réticences, et en considérant que
> ces travaux effectués en toute liberté ne peuvent qu'aider la Révolution
> dans sa marche vers la totale liberté[^7].

L'histoire moderne repose sur un déchirement ontologique : si l'être et
son expression ne font qu'un, affirme le Marx des *Manuscrits de 1844*,
c'est que le travail, en tant que pôle central de la praxis humaine, fut
lui-même dévoyé sous régime de production capitaliste.

Ce déchirement inaugural induit un ensemble d'oppositions discursives et
d'aliénations concrètes, en premier lieu desquelles la rupture
métabolique de l'Homme et de la Nature, celle-ci ayant pour fonction de
justifier et d'accroître la division capitaliste du travail :

> L'opposition entre la ville et la campagne ne peut exister que dans le
> cadre de la propriété privée. Elle est l'expression la plus flagrante de
> la subordination de l'individu à la division du travail, de sa
> subordination à une activité déterminée qui lui est imposée. Cette
> subordination fait de l'un un animal des villes et de l'autre un animal
> des campagnes, tout aussi bornés l'un que l'autre, et fait renaître
> chaque jour à nouveau l'opposition des intérêts des deux parties[^8].

Il faut insister sur le fait que toutes les oppositions discursives qui
dérivent de cette rupture, souvent théoriquement fondées et
idéologiquement soutenues, renvoient à une série d'aliénations très
concrètes : celles-ci ont la consistance ontologique d'un camion qui
nous écrase ou d'une forêt qui brûle ; elles ne peuvent donc apparaître
comme simples distorsions de la conscience, d'autant qu'elles reposent
objectivement sur des faits matériels : propriété des grands moyens de
production, exploitation capitaliste d'une classe par une autre,
extractivisme effréné.

Le communisme vise seulement à libérer le vivant de cette opposition
inaugurale qui le déchire ; cette libération s'opérant comme un
accomplissement du devenir de l'être en tant que langage --- c'est ici
"l'énigme résolue de l'histoire" :

> Ce communisme en tant que naturalisme achevé = humanisme, en tant
> qu'humanisme achevé = naturalisme ; il est la vraie solution de
> l'antagonisme entre l'homme et la nature, entre l'homme et l'homme, la
> vraie solution de la lutte entre existence et essence, entre
> objectivation et affirmation de soi, entre liberté et nécessité, entre
> individu et genre[^9].

Sur le terrain de la praxis révolutionnaire, cette opposition devra être
surmontée par un sujet révolutionnaire en capacité de fédérer, sans les
soumettre à la Loi des grands nombres et aux structures pétrifiées de la
gauche institutionnelle, les mondes paysans, ouvriers et
sous-prolétaires --- la difficulté pratique et les apories stratégiques
rencontrées au sein des luttes révolutionnaires étant si souvent
corrélées à l'homogénéisation d'un sujet révolutionnaire qui se
reconnaît lui-même dans l'exclusion de l'*autre*.

Sur le terrain poétique, la réconciliation des termes de l'opposition
s'incarne dans le visage de l'enfance, ce dernier n'ayant pas encore
conféré au *paysage* les déterminations d'un *cadre* : l'enfant
reconnaît le monde, et se reconnaît lui-même à travers lui, non par la
médiation d'un concept ou d'une culture déterminée, mais à partir de ses
perceptions sensibles.

Est-ce à dire, si nous voulons de nouveau rencontrer un paysage, qu'il
nous soit nécessaire de destituer une part importante d'acquis sociaux
et culturels ? Oui et non. Car la connaissance spécifique de la géologie
et de l'agriculture permet évidemment au poète de comprendre un paysage,
d'en apprécier l'histoire ou d'en déplorer l'altération ; mais il lui
est tout autant nécessaire de se laisser happer, surprendre par ses jeux
de couleurs, ses reliefs, par le caractère accidentel de la roche et le
miracle de la flore se débattant avec elle.

Avant Marx et ses héritages contradictoires, Hölderlin a tenté de
composer une dialectique de la Tradition et du Paysage, sans inféoder
l'un à l'autre, et encore moins à l'Histoire --- c'est une figuration du
feu, celui de la nouvelle origine, qui intègre les éléments de
l'antagonisme (nature, tradition) et les dissout[^10] dans une image
de notre salut :

> Ainsi, osez ! votre héritage, votre acquis,\
> Histoires, leçons de la bouche de vos pères,\
> Lois et coutumes, noms des Dieux anciens,\
> Oubliez-les hardiment pour lever les yeux,\
> Comme des nouveau-nés, sur la nature divine.\
> Alors, votre esprit à la lumière du ciel\
> Embrasé, d'un souffle tendre de vie\
> Votre poitrine abreuvée comme au premier jour,\
> Quand bruiront sous leurs fruits d'or les forêts,\
> Jailliront les sources du rocher, quand la vie\
> Du monde, son esprit de paix, vous saisiront\
> Et l'âme vous berceront comme un chant sacré,\
> Qu'alors perçant les délices d'une belle aube\
> Luiront d'un éclat nouveau les verdures de la terre\
> Et la montagne et la mer, les nuages et les astres,\
> Que ces nobles forces, tels des frères héros,\
> Venant sous vos yeux vous feront battre le cœur\
> Ainsi qu'à des écuyers dans un désir de prouesses\
> Et d'un monde vôtre et beau, alors tendez-vous les mains,\
> Donnez-vous votre parole et partagez votre bien[^11].

Le nouveau-né vit dans le *langage* et non encore dans la *langue*. Ses
yeux s'ouvrent sur un monde vierge encore de *significations* : c'est un
regard offert à la lumière --- poème de feu et de cendres ; ni capture ni
hiérarchisation des perceptions, mais pur abandon dans le feu de la
*nouvelle origine*.

L'image du feu hölderlinien se conclut sur un appel au partage, à la
communion, emblématique, pensons-nous, de cette tradition cachée, de ce
communisme de l'enfance, ou enfance du communisme, visant l'unité
dialectique[^12] de l'être et du langage dans la perspective d'une
écologie qui serait à la fois sémantique et matérielle.

C'est en ce sens que cette terre, en sa myriade d'éléments qui la
composent, minéraux, végétaux, animaux, en sa totalité inachevable dans
l'enclos du discours, pourra être *nôtre* --- non dans l'esprit du
*maître et du possesseur*, mais dans celui de l'enfant, qui fait corps
avec le monde sans apriorité culturelle ni classification surplombante.

Cet usage hölderlinien de la figuration de l'enfance n'est pas un appel
à la régression, mais à la destitution de ces "leçons de la bouche de
vos pères, lois et coutumes, noms des dieux anciens" qui assignent au
monde une signification morte.

Par-delà la question du Savoir, c'est une guerre silencieuse menée
contre un certain usage du monde. Mais il n'est pas question pour autant
d'anéantir la tradition *en soi*, seulement de pulvériser le rapport
réifiant qui la place devant la nature en tant que corps étranger.

Nietzsche, dans le sillon d'Hölderlin, rend admirablement compte d'un
processus de transvaluation qui rompt avec la synthèse vide des
philosophies de l'histoire : s'il s'agit bien de se débarrasser du poids
de la Morale, cette forme pétrifiée de la tradition, il ne s'agit pas
pour autant de s'appesantir dans une posture régressive, de rugir
mécaniquement contre l'Ordre ; il s'agit de parvenir au stade de la
nouvelle Aurore, ce devenir-enfant de l'être, qui n'est pas régression,
donc, mais transformation et nouvelle subjectivation des valeurs, non
plus du point de vue du Droit, mais depuis l'être et son langage, en
leur unité conflictuelle et féconde.

Et Nietzsche d'affiner la figuration poétique de l'enfance, d'en dégager
à grands traits quasi mythologiques les principaux mouvements :

L'enfant est innocence et oubli, un renouveau et un jeu, une roue qui
roule sur elle-même, un premier mouvement, une sainte
affirmation[^13].

L'oubli, vertu cardinale de la philosophie nietzschéenne, contre une
conception totalitaire de l'Histoire, permet d'envisager le vide et donc
le possible, bifurcation et renouveau, quête d'innocence et de joie, à
l'aune de la nouvelle aurore.

Sur le terrain de la praxis poétique, du *langage*, cet oubli peut se
traduire par une méfiance à l'égard de la syntaxe, un pessimisme devant
le caractère communicationnel de la langue.

À ce jeu, il y a les bulldozers : Ghérasim Luca, Beckett, Valère
Novarina, aujourd'hui Laura Vasquez sans doute, etc.

Puis il y a ceux, Maïakovski ou Mandelstam en premier lieu, qui n'ont
jamais renoncé à ce "quelque chose d'écrit", inscrivant leur poétique
dans l'ordre des significations, mais sans jamais cesser de le
martyriser, de le distordre. Le langage n'est pas la langue, certes,
mais ce que peut la poésie est affaire de langue et de langage mêlés.
Autrement dit : le langage poétique épuise les potentialités factices de
langue, et l'engage dans un *espace ouvert* qui mêle le visible et
l'invisible, le son et le sens, l'écrit et le non-écrit.

Le différend stratégique est bien réel : si les bulldozers visent
purement et simplement une dissolution du sens et une désintégration de
l'image, les seconds constituent le langage comme un "champ
stratégique", où le sens et l'image sont arrachés à leur bienséance et
déploient leur efficacité matérielle dans l'investissement et le
dépérissement de l'ordre du discours et du régime de la représentation.

Plus proches de nous, Nathalie Quintane ou Jean-Marie Gleize, tenants
d'une prose ponctuelle et/ou circonstanciée, contribuent par exemple à
habiter l'espace de la littérature en en contestant les ors et les
colonnades. Qu'il s'agisse du réalisme ponctuel de la première
(Lautréamont via Proust), ou de la nudité et/ou littéralité du second
(Rimbaud via Ponge), tout est bon pour demeurer dans les choses mêmes,
document ou lisière, et ne jamais s'encombrer de procédés qui
recouvriraient le bruit de l'eau dans une rivière ou la forme objective
d'une chaussure.

Ainsi se profile une autre fonction politique de la "poésie" (dans le
cas de Quintane et Gleize, nous nous évertuons à appeler ça ainsi, faute
de mieux) : une mise en question permanente de la grandeur de la
littérature en tant que domaine de pratique symbolique séparé du vivant
et/ou de la vie sociale.

Si la littérature demeure bien un acte symbolique, elle se fait
conjointement l'objet d'une recherche d'efficacité matérielle --- "prose
tactique[^14]", écrit Benjamin Fouché, qui se reconnaîtrait sans
doute dans cette filiation profane. Cette exigence matérialiste n'en est
pas moins en tension permanente avec l'essence symbolique de la langue,
il serait donc imprudent de s'en remettre à une dichotomie figée entre
matérialisme et symbolisme, cette recherche d'efficacité matérielle se
déployant toujours *depuis* et *dans* le champ symbolique du langage
humain. À moins de prendre la littérature pour ce qu'elle ne sera
jamais : une rivière ou une chaussure.

Cette quête d'une langue basse et matérielle excède-t-elle la fonction
strictement communicationnelle du langage ? Distinguons deux types de
"communicabilité" : le pouvoir de nommer des objets et d'en partager
socialement l'appréhension, d'une part ; et des effets de discours, de
l'autre. Le second type s'offre assez naturellement comme la proie d'une
certaine littérature : dans son *ABC de la barbarie*[^15], par
exemple, Jacques-Henri Michot passe un grand coup de balai sur le
terrain miné des linguisteries journalistiques ; Quintane, quant à elle,
si elle s'attache à dire des choses que tout le monde voit, ou que tout
le monde est en capacité de voir dans une circonstance déterminée de la
vie sociale, elle n'hésite pas non plus à pulvériser les effets de
discours qui contribuent à recouvrir et donc à trahir sa nudité ; son
approche est donc tout à la fois réaliste, circonstanciée *et* critique.

L'approche critique ne contrevient pas à la recherche de littéralité,
l'une et l'autre soutiennent ensemble une tension dialectique qui porte
à son comble l'effraction de littéralité dans le champ du langage. Une
littéralité qui épuise donc quelquefois la "communicabilité" du
langage, et peut puiser conjointement à la source rimbaldienne sa
nécessité d'une forme objective et inaccaparable --- une forme, osons le
dire, en un sens infiniment plus concret que dans son acception
idéologique et dévoyée : "antifasciste".

La centralité du régime symbolique (le fait que le langage institué
s'organise autour de valeurs déterminées, d'une grammaire donnée, d'un
bon sens populaire ou de mythologies bourgeoises) est donc mise à mal
par la pure matérialité de la langue (nudité, littéralité, crudité) ; et
ce, alors même que l'efficacité matérielle de la langue fait retour à la
réalité, souvent la plus ordinaire, la plus universelle, ni plus, ni
moins : "*Chaussure* ne résulte pas d'un pari ; il ne présente aucune
prouesse technique, ou rhétorique. Il n'est pas particulièrement pauvre,
ni précisément riche, ni modeste, ni même banal. Ce n'était pas un
projet, mais ce n'est pas un brouillon, mais il n'a pas encore trouvé sa
fin[^16]."

En prenant le parti unique de l'efficacité matérielle ou du symbolisme
décadent, on passe sans doute à côté de la cohérence des destins
contradictoires mais complémentaires de l'héritage rimbaldien. Le
lyrisme, par exemple, que Rimbaud contribue à remettre en mouvement, est
attaqué par tous les bouts : sa supposée transparence, son exaltation du
moi ou de l'histoire, sa quête de transcendance. C'est ignorer qu'il
précède et excède la littéralité dans l'exacte mesure où la littéralité
se rappelle au lyrisme comme une exigence à la fois éthique et
esthétique de matérialité. Le lyrisme est attaqué de toute part --- sauf
peut-être par le principal théoricien de la littéralité, qui nous
rappelle ce que l'histoire poétique lui doit :


Et c'est peut-être une première et fondamentale caractéristique de la
démarche hugolienne : la poésie est ce langage qui ne peut rien dire
qu'en posant incessamment la question de sa possibilité et de sa
légitimité, la poésie n'est pas véhicule de réponses toutes faites,
mais creusement d'un espace où le sens (du moins pour Hugo) doit
pouvoir surgir ; en l'occurrence, évidemment, et c'est ce qui apparaît
si l'on observe non pas tel ou tel poème, mais la dynamique du geste
hugolien, c'est ce creusement qui est essentiel, non le surgissement,
puis la saisie du sens. Ce creusement, qui fait de la poésie un acte et
de cet acte une question toujours maintenue, une tension productrice, ne
va pas sans ouvrir, à l'intérieur du moi, une faille, une fissure, par
où pourra s'engouffrer toute la suite (jusqu'à la "disparition
élocutoire" du poète). L'essentiel est là : que, dès sa naissance
romantique, la poésie personnelle est une question pour elle-même,
qu'il n'y a jamais eu, sauf dans les Histoires de la Littérature, de
poésie lyrique transparente, qu'il y a une réflexion lyrique où se pose
simultanément la double question de l'apparition et de la disparition
du "je[^17]".

Ainsi le lyrisme ne cesse de se chercher un sujet, d'en nier la
substance dans l'éparpillement des choses ou des traditions, de se
réinventer un moi, de le voir disparaître, puis réapparaître et se
dédoubler, se démultiplier, tendre vers le monde, faire monde, etc. À ce
titre, le lyrisme a toujours à faire aux "accidents du sol", aux
anfractuosités de la vie sociale, mais il ne se résigne jamais à un
quelconque naturalisme : "Soyons prodigieusement matériels et bassement
exaltés !"

Ce pour quoi le culte de la littéralité[^18] doit également prendre la
mesure de sa potentielle naïveté : matérialisme appauvri ou art radical
étalé dans des galeries, l'immanentisme est une autre façon de coller à
la peau d'un monde qui n'existe pas ; n'hésitons pas à aiguiser notre
exigence d'une vie "totale" --- dans le sens qui est celui de Michel
Leiris[^19], et non plus celui de Hegel ---, c'est-à-dire d'une
existence qui se rapporte non seulement à l'existant mais également au
possible, à exiger un monde pour celle-ci, et non un slogan ou autre bel
objet plastique exposé dans une vitrine de Saint-Germain-des-Prés ou de
Pigneto.

Pour un jeune garçon qui grandit sous le fascisme, ce n'est pas tant la
littéralité, la nudité ou la crudité de la langue qui importe, ce retour
à la bassesse de la vie et des choses, que de faire voler en éclat toute
centralité symbolique par un excès de brisure, un nombre infini de
déplacements sémantiques, de nouvelles configurations linguistiques.
C'est ainsi que, loin de Pigneto, loin de Saint-Germain-des-Prés,
lorsque le jeune Pier Paolo découvre *Une saison en enfer*, d'instinct,
il choisit son camp :

> La lecture de Rimbaud et de la poésie symboliste et décadente m'a fait
> prendre conscience de façon mécanique, automatique, que j'étais contre
> le fascisme, et elle a donc eu une fonction politique positive[^20].

La "poésie symboliste et décadente", et pour peu qu'on la réduise à
cette désignation, a bien le pouvoir de maltraiter la fonction
strictement communicationnelle du langage institué et donc également sa
littéralité. Dans le cas particulier de Rimbaud, il s'agit de creuser le
sol vicié de sa langue nationale, d'en maltraiter les représentations,
de la tordre jusqu'à inventer de nouvelles lignes de force au cœur même
de ses fondations --- la poésie rimbaldienne, toute symboliste qu'elle
soit, mobilise donc une matérialité qui brise l'ordre du discours, en
éparpille les petits bouts de quartz et les enfonce cruellement dans les
représentations pacifiées de la vie des peuples et des nations.

Mais pourquoi Rimbaud et tant de poètes après lui s'en prennent-ils à la
langue en général, et à leur propre langue en particulier ? N'est-il pas
notre héritage commun, ce sur quoi nous serions en mesure de fonder
notre communauté humaine et nationale ?

Précisément : cette langue, à l'ère de l'État-nation et de ses
dynamiques coloniales, est devenue un instrument politique au service de
l'hégémonie linguistique des nations civilisatrices ; à ce titre, son
imposition est nécessairement liée à la relégation ou à l'exclusion des
langues subalternes, langues qui seules étaient en mesure de produire un
rapport à la réalité échappant à cette clôture.

Aux "barbares", qui par définition ne parlent pas la langue du colon
ou ne la parlent pas comme il faut, la poésie moderniste se joint en
faisant valoir sa propre sauvagerie. C'est ainsi que Rimbaud, stratège
sur le terrain de la syntaxe et militant d'une forme libre, s'attelle à
la fabrication de projectiles ayant pour destin de faire trembler les
vitrines du grand Parnasse.

Pasolini a beau reconnaître le geste rimbaldien, sa puissance
destituante, son "antifascisme" viscéral, le jeune poète de Bologne
n'a encore jamais mis un pied sur une place de Rome ou de Milan, il erre
aux marges de la modernité, dans la périphérie de Bologne, dans la
campagne frioulane, et s'intéresse donc à d'autres traditions,
dialectales, idiomatiques, vulgaires et menacées.

Marx remarque que le vieux sentimentalisme des sociétés féodales a été
noyé dans les eaux tièdes du calcul égoïste, mais il ne fait pas grand
cas du génocide culturel perpétré par la forme culturelle du capitalisme
à l'égard des traditions vernaculaires ; on peut même supposer que
l'hégémonie linguistique serait au service de ce devenir révolutionnaire
d'un prolétariat transnational et unifié. Si Pasolini affirme à
plusieurs reprises que Marx est conscient du génocide culturel perpétré
par le capitalisme, insistons donc sur le fait que le théoricien
révolutionnaire ne s'en émeut que raisonnablement, tant sa conception de
l'Histoire est encore imprégnée d'étapisme et de téléologie
progressiste. Il faut croire, ici, que Pasolini se réfère à l'insu du
discours et non à la clarté du logos.

Si Spinoza considérait que les difficultés des hommes reposaient sur
leurs difficultés à communiquer, il est peut-être temps d'infirmer le
postulat selon lequel une langue universelle devrait primer sur
l'harmonie des langues, l'homogénéité linguistique sur l'entrelacement
des dialectes --- rien ne sert de communiquer si nous n'avons plus rien à
nous dire.

Près d'un siècle après Hölderlin, dans une lettre à son éditeur, Rainer
Maria Rilke, cherchant à "expliquer" le sens de ses *Élégies de Duino*,
écrit déjà qu'il perçoit une mutation anthropologique majeure dans nos
sociétés européennes de l'entre-deux-guerres ; celle-ci ayant tout à
voir avec une fracturation de la conscience en partie liée à une
aliénation du pouvoir de désignation de la langue --- nous ne sommes plus
en mesure, dit-il, de nous reconnaître dans les choses que nous
nommons :

> Aujourd'hui, l'Amérique nous inonde de choses vides, indifférentes, de
> pseudo-choses, *d'attrapes de vie*... Une maison, au sens américain, une
> pomme ou une grappe de raisin américaines, n'ont *rien* de commun avec
> la maison, le fruit, la grappe qu'avaient imprégnées les pensives
> espérances de nos aïeux... Les choses douées de vie, les choses vécues,
> *conscientes de nous*, sont sur leur déclin et ne seront pas remplacées.
> *Nous sommes peut-être les derniers qui auront connu encore de telles
> choses*. Nous avons la responsabilité de sauvegarder non seulement leur
> souvenir (ce serait peu de choses, et bien peu sûr), mais leur valeur
> humaine et larique (au sens des divinités du foyer). La terre n'a pas
> d'autre issue que de devenir invisible : *en nous*, qui participons pour
> une part de nous-mêmes à l'Invisible, qui en possédons (au moins) des
> actions, et qui pouvons augmenter notre capital d'Invisible pendant que
> nous sommes ici, --- *en nous* seulement peut s'accomplir cette
> transfiguration intime et durable du Visible en Invisible, en une
> réalité qui n'ait plus besoin d'être visible et tangible, de même que
> notre propre destin, en nous, ne cesse *de se faire à la fois invisible
> et plus présent*[^21].

C'est une double aliénation qui se fait jour --- double du point de vue
de la langue allemande qui, contrairement à la langue française, est
dotée de deux termes différenciés pour désigner l'aliénation[^22] :
*Entfremdung*, qui renvoie au sentiment d'étrangeté de l'être qui ne
reconnaît plus son monde ; *Entausserung*, qui renvoie à un processus de
mauvaise objectivation, d'objectivation pillée et dénaturée.

Si l'être est privé de ses capacités à se reconnaître et à reconnaître
le monde par la médiation de la langue, c'est que la langue du Capital
est devenue une langue vide, purement instrumentale. Et ce qui est
valable pour le monde des objets, l'est également pour celui des idées :
que sont devenus la "Liberté", l'"Homme", le "Savoir",
l'"Universel" dans la bouche des sentinelles de l'ordre existant ?

Hegel, déjà, avait à cœur de redonner à l'universalité un contenu
concret. Mais tous les contenus de la philosophie hégélienne ne sont que
des contenus opportunistes, intégrés et donc neutralisés dans le cadre
d'un processus historique surplombant et donc également abstrait.

Hölderlin, qui fut un compagnon de Hegel, avec lequel il partagea,
dit-on, une chambre à Tübingen, avec lequel, dit-on encore, il planta un
arbre en hommage à la Révolution française, fait émerger dans sa
poétique une conception radicalement différente de l'histoire, une
conception qui l'engagera sur une voie non académique.

La folie de Hölderlin est avant tout un défaut de paiement, une
soustraction à la marche de l'Histoire, une désertion ; quant à Hegel,
aux premières inspirations géniales, de Berne, de Tübingen, de
Francfort, sur la belle totalité grecque éclatée, succédera le poids
d'un système qui n'en finira plus d'épuiser l'histoire[^23].

Pasolini, quant à lui, est l'enfant d'un siècle qui n'a plus rien
d'innocent : il ne peut s'abriter dans l'ignorance des contre-finalités
que l'histoire bourgeoise impose au monde, sa destruction. À la folie
de Hölderlin, il substitue un mode d'être tragique, engagé dans une
joute avec la réalité bourgeoise --- l'esprit tragique impliquant une
telle confrontation, fort éloignée des petites dramaturgies et des
grands systèmes de pensée totalitaires : il s'agit bien de regarder la
mort dans les yeux.

On sait que le jeune Marx était un lecteur de Hölderlin, qu'il s'initia
même à la poésie --- peu de poèmes subsistent ---, avant de plonger dans
l'étude du matérialisme antique, puis dans la lecture de Hegel, de
Feuerbach et des économistes anglais ; sans rejouer le Marx humaniste
contre le Marx critique de l'économie politique, il est cependant
pertinent d'insister sur ce "moment-Hölderlin" dans la formation
intellectuelle du jeune Marx.

C'est à cette tradition méconnue, de communistes profanes, de poètes à
l'acuité sociale extraordinaire, à la sensibilité convulsivement portée
vers une *sauvegarde* du sensible, que nous associons Pasolini ; il
s'agit d'une tradition à la fois alternative et complémentaire au
marxisme philosophique, sa condition de possibilité peut-être : la
découverte d'une forme potentielle du communisme, indissociablement
éthique et esthétique.

Nous n'avons esquissé ici que quelques traits de cette tradition à
partir du refus primordial de l'opposition Homme/Nature, mais, comme
toute tradition non institutionnalisée, sa survivance dépend de ce qu'il
nous reste à y découvrir pour un *communisme déjà existant* : une
écologie du sens couvant une écologie matérielle, une poétique
communiste donc, ou, pour le dire autrement, une *raison* poétique du
communisme.

Celle-ci ne peut se justifier par un discours sur l'Histoire, mais
seulement par la primauté du langage sur toute forme de Savoir
--- Empédocle jetant au feu "Histoires, leçons de la bouche de vos pères,
lois et coutumes, noms des Dieux anciens" en cette forge de Vulcain où
il poussa lui-même son dernier souffle --- qui fut également le
premier : celui de la *nouvelle origine* --- extase ultime, dissolution
et joie, figurant alors l'enfance comme éternel recommencement du
souffle et de la vision.

La politisation de l'art à laquelle nous enjoint le communisme
benjaminien[^24] --- qui acte la mort de l'aura (*hic et nunc*) à l'ère
de l'avènement des technologies reproductives --- repose sur le constat
que la nouvelle production culturelle --- photographies, films,
impressions à grande échelle, et désormais tout ce qui circule en boucle
dans l'immatérialité de nos réseaux --- doit être politisée du fait même
de cette reproductibilité potentiellement infinie. Mais le constat
benjaminien repose sur un principe stratégique et non ontologique : la
production artistique, d'une part, peut demeurer artisanale ; de
l'autre, la politisation de l'art demeure un concept à préciser :
s'agit-il de greffer un contenu politique à une forme standardisée ? ou
de réinventer une forme tout en se soumettant aux nouvelles conditions
techniques de la modernité capitaliste ? Si nous comprenons plutôt bien
ce que signifie un "art politique" s'agissant de Brecht, il est plus
périlleux de statuer sur le potentiel politique d'un Samuel Beckett ou
d'un Paul Celan ; ce dernier s'exprimant, dit-il, seulement du point de
vue des corneilles.

C'est ici que nous semble particulièrement intéressante l'idée d'Adorno
selon laquelle Celan ou Beckett, en se soustrayant à l'ordre du
Discours, à la structure traditionnelle du récit ainsi qu'au régime de
la représentation, donnent à voir une action sur la langue ouvrant des
brèches au cœur de nos consciences rationalistes, consuméristes et donc
nécessairement pauvres en langage.

Ce pour quoi, après avoir distingué les bulldozers et les stratèges, il
nous semble important d'envisager leur alliance : le travail du montage,
en cela qu'il se confronte aux nouvelles conditions techniques de notre
temps, est en capacité de faire jouer Beckett avec Marx --- art de la
destitution *et* recomposition d'un horizon stratégique sur le terrain
de l'art.

Longtemps nous avons cru qu'un art, afin d'être efficace politiquement,
conséquent, substantiel, devait être porteur d'un contenu (moral,
politique, idéologique) ; aujourd'hui, en cette époque de clôture
sémantique qu'induit la langue néolibérale, nous pensons au contraire
que c'est en convoquant la puissance destituante du langage au sein de
nos horizons stratégiques que nous rendrons possible la formation d'un
art émancipé de la rationalité marchande et de l'idéologie consumériste.

Il ne peut être question de demeurer rivé à une tactique circonstanciée
--- il nous faut recomposer un horizon stratégique dans le "domaine de
l'art". C'est ce que fera Pasolini, obstinément, en diversifiant sa
pratique (poèmes, articles, films), en se confrontant à la technique, en
prenant le risque de se contredire et d'inventer une langue neuve.

Se distinguant peut-être de la poétique de la pure errance
baudelairienne --- actant la cessation du possible tout en se
réconfortant dangereusement au sein de la Beauté ---, Hölderlin, Rilke
ou Pasolini, pour ne citer qu'eux, les plus emblématiques selon nous de
cette tradition inavouée, visent bien une rédemption : maintenir vivante
cette dialectique de la Terre et des Ciels, de la Ville et de la
Campagne, de la Raison et du Sensible, autant d'oppositions abstraites
que leur communisme hétérodoxe ou cryptique s'est juré de réconcilier.

Il ne pourrait être question de limiter cette tradition aux arts
littéraires : l'odieux Picasso disait qu'il avait passé sa vie à
apprendre à dessiner comme un enfant ; l'acte de figuration, dans la
peinture, s'il tend à reproduire une certaine représentation du monde,
contribue à épuiser les potentialités du regard, à figer l'enfance dans
une maîtrise de la réalité ; et c'est donc à une dialectique de la
maîtrise et de l'effacement que nous convie l'acte créateur, celle-ci
impliquant d'assumer une part de destruction, d'oubli et de cruauté.

Paul Klee, quant à lui, conçoit son œuvre comme une ontologie du
devenir, jamais comme une totalité achevée[^25] : ce caractère
d'inachèvement de ses dessins et de ses peintures, leur dynamique
ouverte aux potentialités non encore advenues de l'ouvrage, esquisse une
certaine image de l'enfance --- qui n'est pas une représentation béate
de pureté : *tous les anges sont terribles* --- songeons notamment à ces
angelots qui jouent espiègles au pied d'allégories, sur les murs de
l'*Oratorio san Lorenzo* à Palerme, en subvertissent la solennité, en
contrarient le pouvoir d'évocation.

Cette part de jeu, d'oubli, et donc de cruauté, que nous associons à la
peinture de Picasso, cette importance du mouvement et de la contingence
qui caractérise l'art de Klee, ont pour vocation de matérialiser une
certaine idée de l'innocence, d'une innocence qui n'est pas diluable
dans le laïcisme et la tolérance de la société de consommation. Il
s'agit d'une innocence indocile, exigeante, qui vise un au-delà du monde
au cœur du monde lui-même --- interstice qui, selon Rilke, serait le
lieu de l'ange par excellence :

> Il n'y a ni En-deçà, ni Au-delà, rien que la grande Unité où ces êtres
> qui nous surpassent, les "anges", sont chez eux[^26].

Cet espace ouvert recouvre la matière d'une prose versifiée où l'esprit
des morts se coltine la matérialité du poème --- espace matériel du
langage qui se confond le plus souvent avec l'enfance en tant que
puissance destituante.

C'est de façon tout à fait analogue que Pasolini investit cet espace. À
cet égard, son horizon stratégique ne cessera de se recomposer : si la
première image frioulane de l'enfance épouse encore les traits de la
pure innocence ("Fontaine de rustique amour[^27]"), celle-ci mute en
projectiles dans *Transhumaniser et organiser* (« ... sachez que je suis
ici prêt / à fournir des poésies sur commande : des engins<sup>\*</sup>. / <sup>\*</sup>Même
explosifs[^28] »), avant de faire l'épreuve d'une réécriture
désenchantée dans *La nouvelle jeunesse* ("Fontaine d'amour pour
personne[^29]"). Ainsi, tout au long de son œuvre poétique, Pasolini
ne cesse d'inscrire l'enfance et ses métamorphoses dans l'espace du
langage --- ce qui a peu de chance de laisser la littérature indemne.

Soutenir une telle visée suppose un courage éthique et créateur
remarquable : continuer à créer, continuer à agir, en assumant cette
part de destruction, d'oubli, de ce que nous avons aimé, chéri, au
sacrifice même des motifs qui nous ont conduits à persévérer dans l'art
et dans la vie ; il y aura quelque chose après tout ça, et si ce quelque
chose n'est rien, nous avons le devoir de continuer quand même, car ce
quelque chose qui n'est peut-être rien est pourtant tout ce que nous
avons.

C'est peut-être ici que l'on perçoit l'une des plus merveilleuses
manifestations de cette *vitalité désespérée* qui caractérise toute la
poésie de Pasolini :

> Je crie, en ce ciel où habita ma mère :\
> "Avec une incorrigible naïveté"\
> --- à l'âge où l'on devrait pourtant être un homme ---\
> j'oppose l'arbitraire à la dignité\
> (qui, d'ailleurs, à cessé d'intéresser nos fils).\
> Et, contre un peu de science de l'histoire, qui me fait\
> connaître\
> l'étendue de la tragédie d'une histoire qui s'achève,\
> je m'adjuge toute l'innocence de la vie future[^30] !

Cette "innocence de la vie future" s'inscrit dans une conception de
l'Histoire qui n'est déjà plus celle de Hegel ou du matérialisme
orthodoxe --- qui se fait retour et recommencement, dans le sillage de
Hölderlin peut-être, dans celui de Nietzsche également.

Dans la mesure où l'amour et la radicalité (indissociables) de Pasolini
puisent leur source dans une certaine image de l'enfance, et non dans un
verbalisme sans ancrage, ou encore dans le souvenir d'une résistance
mythifiée --- ravivée par les sursauts bolivien, cubain ou
vietnamien[^31] ---, l'espérance pasolinienne demeure donc soutenue
d'une idée de l'enfance comme langage. En conséquence, son espérance,
toute révolutionnaire qu'elle soit, n'est jamais rhétorique : elle est
*seulement* amour.

Non un idéal d'égalité et de liberté que la classe ouvrière serait
chargée de réaliser, donc, mais la croissance dans les "soleils de
l'âme" d'une exigence toujours renouvelée de Justice :

> Vous avez voulu avoir un poète sur ce ban\
> lustré par les culottes de tant de pauvres diables ?\
> D'accord, savourez. La Justice\
> devient la voix d'hirondelles aveugle, aux
> désœuvrements \
> de la Poésie. Et non parce que la Poésie aurait le
> droit\
> de délirer sur un peu d'azur, sur un misérable jour\
> sublime qui naît de la mélancolie et de la mort.\
> Mais parce que la poésie est Justice. Justice qui
> croît\
> en liberté, dans les soleils de l'âme, où
> s'accomplissent\
> en paix les naissances des jours, les origines et les
> fins\
> des religions, et les actes de culture\
> sont aussi des actes de barbarie,\
> et quiconque juge est toujours innocent[^32].

Quelle correspondance (!), ici, entre le feu Hölderlinien qui, sous le
regard du nouveau-né, embrase l'Histoire, la Culture, le Savoir, et
l'âme pasolinienne au sein de laquelle se déploie le jugement poétique
tel qu'esquissé dans ce poème : critique insolente, radicale,
désespérée, du point de vue de l'éternelle enfance, des accomplissements
et des prouesses de la modernité capitaliste.

Si nous devions associer l'enfance --- cet implacable "jugement
innocent" sur le monde --- à une fleur, comment ne pas l'associer au
genêt léopardien, qui ne cesse de croître sur les flancs dévastés du
Vésuve, à rebours de toutes ces représentations épiques de la grandeur
de l'Homme, leur exaltation dans une langue morte :

> Sur les flancs calcinés de ce mont formidable,
> Sombre exterminateur de l'homme et des cités,
> Nulle plante ne croît au souffle des étés ;
> Toi seul, sur le versant du gouffre inabordable,
> Tu fleuris et souris et parfumes les airs,
> Solitaire genêt, qui te plais aux déserts[^33].

Quant à la comparaison du langage de l'enfance au genêt, celle-ci
pourrait être étendue ici au désert de significations qui inscrit
l'enfant au cœur d'un monde vierge de Culture et d'Histoire, et dont
l'oubli --- vertu nietzschéenne par excellence --- constitue la
condition de déploiement et de redéploiement.

Entre oubli et mémoire, donc, dans le sillage involontaire de Benjamin,
Pasolini invente une dialectique nouvelle sur le terrain poétique ;
celle-ci visant une rédemption qui devra se traduire par un renversement
du capitalisme, mais également par la manifestation concrète d'un
*communisme déjà existant* --- c'est-à-dire : par une abolition de la
Politique en tant que domaine de pensée séparé du langage.

Cette (trans)dialectique est vertigineuse : elle s'inscrit au cœur de
la tradition que nous reconnaissons comme alternative et complémentaire
au marxisme philosophique --- dont nous tentons seulement ici d'esquisser
quelques traits, de façon partiale mais non arbitraire, conscients des
limites inévitables de notre tentative.

Ajoutons qu'il n'est pas question de *survivance*[^34] : nos résistances
ne sont pas assimilables à un patrimoine de vieilles représentations et
de vieux discours ; si nous avons à cœur d'honorer la mémoire des
vaincus, ce n'est pas pour les pendre une seconde fois au pilori d'une
conscience fatiguée, déclinante, mais seulement afin de rendre compte de
cette exigence de Justice qui n'a jamais cessé de croître en nous, à la
lumière de l'enfance. Cette exigence porte donc la marque d'une décision
inaugurale : elle nous engage à choisir entre le monde du langage et
celui du pouvoir.

Mais, chez Pasolini, quelle est donc cette *image* de la liberté en
son point de surgissement inaugural ? Nous pensons bien sûr à une lettre
du très jeune Pier Paolo à l'adresse de son ami Franco Farolfi :

> ... Il y a trois jours, Paria et moi sommes descendus dans les recoins
> d'une joyeuse prostitution, où de grasses mamas et l'haleine de
> quadragénaires dénudés nous ont fait penser avec nostalgie aux rivages
> de l'enfance innocente. Nous avons ensuite pissé avec désespoir. 
>
> L'amitié est une très belle chose. La nuit dont je te parle, nous avons
> dîné à Paderno, et ensuite dans le noir sans lune, nous sommes montés
> vers Pieve del Pino, nous avons vu une quantité énorme de lucioles qui
> formaient des bosquets de feu dans les bosquets de buissons, et nous les
> enviions parce qu'elles s'aimaient, parce qu'elles se cherchaient dans
> leurs envols amoureux et leurs lumières, alors que nous étions secs et
> rien que des mâles dans un vagabondage artificiel. 
 J'ai alors pensé
> combien l'amitié est belle, et les réunions de garçons de vingt ans qui
> rient de leurs mâles voix innocentes, et ne se soucient pas du monde
> autour d'eux, poursuivant leur vie, remplissant la nuit de leurs cris.
> Leur virilité est potentielle. Tout en eux se transforme en rires, en
> éclats de rire. Jamais leur fougue virile n'apparaît aussi claire et
> bouleversante que quand ils paraissent redevenus des enfants innocents,
> parce que dans leur corps demeure toujours présente leur jeunesse
> totale, joyeuse. Même quand ils parlent d'Art ou de Poésie. J'ai vu (et
> je me vois moi-même ainsi) des jeunes parler de Cézanne, et on avait
> l'impression qu'ils parlaient d'une de leurs aventures amoureuses, l'œil
> brillant et trouble. Ainsi étions-nous, cette nuit-là ; nous avons
> ensuite grimpé sur les flancs des collines, entre les ronces qui étaient
> mortes et leur mort semblait vivante, nous avons traversé des vergers et
> des bois de cerisiers chargés de griottes, et nous sommes arrivés sur
> une haute cime. De là, on voyait très clairement deux projecteurs très
> loin, très féroces, des yeux mécaniques auxquels il était impossible
> d'échapper, et alors nous avons été saisis par la terreur d'être
> découverts, pendant que des chiens aboyaient, et nous nous sentions
> coupables, et nous avons fui sur le dos, la crête de la colline. Nous
> avons alors trouvé une autre clairière herbeuse, en cercle si réduit que
> six pins à peu de distance les uns des autres suffisaient à l'entourer ;
> nous nous sommes étendus là, enveloppés dans nos couvertures, et en
> parlant agréablement entre nous, nous entendions le vent souffler et
> faire rage dans les bois, et nous ne savions pas où nous nous trouvions
> ni de quels lieux nous étions entourés. Aux premières lueurs du jour
> (qui sont une chose indiciblement belle), nous avons bu les dernières
> gouttes de nos bouteilles de vin. Le soleil ressemblait à une perle
> verte. Je me suis déshabillé et j'ai dansé en l'honneur de la lumière ;
> j'étais tout blanc, alors que les autres enveloppés dans leurs
> couvertures comme des Péons, tremblaient au vent. Nous avons ensuite
> lutté, dans la lumière de l'aube jusqu'à l'épuisement ; puis, nous nous
> sommes allongés, nous avons allumé le feu en l'honneur du soleil, mais
> le vent l'a éteint...[^35]

Et si toute l'œuvre poétique de Pasolini puisait sa source dans l'image
qui s'esquisse ici ? Avec le temps, l'image fut bien sûr affinée, sans
cesse recomposée, mais l'image est bien là, déjà là, dans cette lettre
du jeune Pasolini rédigée au tout début de l'année 1941.

Cette image n'est déjà plus tout à fait celle de l'enfance, mais celle
de l'adolescence, âge de la confrontation par excellence, de la
transition douloureuse ; cette abondance de lucioles faisant écho aux
miradors "féroces" et aux chiens, à l'ordre militarisé et aux espaces
enclos ; tout comme cette flamme, offerte au soleil, et que le vent
éteint.

Toute l'œuvre de Pasolini se déploie dans la confrontation désespérée de
cette image de la liberté et l'irréalité du monde ; cette image, qui est
donc *fonction* à la fois de sa poétique et de son marxisme hérétique,
apparaît comme la *raison* d'un type d'espérance qui n'a plus rien à
voir avec un discours sur l'Histoire[^36].

Quant aux lucioles, qui constitueront le motif à la fois réel et
emblématique des analyses de Pasolini sur les mutations
anthropologiques, politiques et culturelles des années qui viendront,
ces lucioles qui auront tout à voir avec cette image primordiale, qui en
constituent même l'un des éléments quasi prophétiques, ces lucioles qui
incarnent déjà l'insurrection lumineuse de la parole poétique dans la
nuit de l'histoire, nous les retrouverons donc plus de trente ans plus
tard, dans les *Écrits corsaires* (1975), et ce pour en déplorer la
disparition.

[^1]: "Nous sommes quelques-uns à cette époque à avoir voulu attenter
    aux choses, créer en nous des espaces à la vie, des espaces qui
    n'étaient pas et ne semblaient pas devoir trouver place dans
    l'espace." (Antonin Artaud, *Le Pèse-Nerfs*, Abrüpt, 2020.)

[^2]: Référence au titre d'un ouvrage de Jacques Rancière : *Le partage
    du sensible*, Éditions La Fabrique, 2000.

[^3]: La totalité devant être entendue ici non comme clôture mais comme
    infini processus de réalisation de soi et du monde.

[^4]: Michel Leiris, "Communication au congrès culturel de La Havane"
    (1968), in *Cinq études d'ethnologie*, Gallimard, 1969.

[^5]: Chapitre 7 du *Capital* de Karl Marx, traduction de Joseph Roy, en
    libre accès sur Wikisource.

[^6]: Michel Leiris, "Communication au congrès culturel de La Havane"
    (1968), in *Cinq études d'ethnologie*, op. cit.

[^7]: Michel Leiris, "Communication au congrès culturel de La Havane"
    (1968), in *Cinq études d'ethnologie*, op. cit.

[^8]: Karl Marx et Friedrich Engels, *L'idéologie allemande*, en libre
    accès sur le site Marxists.org.

[^9]: Karl Marx, *Les manuscrits économico-politiques de 1844*, en libre
    accès sur le site Marxists.org.

[^10]: Le sentiment de la nature est une expression contemporaine de
    l'ère industrielle et de ses ravages, anthropologiques et écocides
    (voir notamment *Le jardin de Babylone* de Bernard Charbonneau,
    Éditions de l'Encyclopédie des Nuisances, 2002). De façon analogue,
    le culte de la tradition, en un sens folklorique et donc pétrifié,
    doit être dépouillé de ses fantasmes par une analyse rigoureuse des
    impactes destructeurs du tourisme de masse. Il ne peut être question
    de faire prospérer l'économie-monde néocapitaliste sur le dos de la
    nature et de la tradition.

[^11]: Friedrich Hölderlin, *Oeuvres*, sous la direction de Philippe
    Jaccottet, Gallimard, Pléiade, 1967.

[^12]: Le fait que cette unité soit caractérisée ici de "dialectique"
    nous permet d'éviter toute pétrification identitaire de l'être, d'en
    appeler à ses métamorphoses, à la multiplicité de ses devenirs.

[^13]: Friedrich Nietzsche, *Ainsi parlait Zarathoustra*, traduction de
    Henri Albert, Société du Mercure de France et Naumann, 1898. Ici, un
    lecteur exigeant me fait remarquer à juste titre que la traduction
    d'Albert est ambiguë. Mais l'ambiguïté en question (le caractère
    "saint" de l'affirmation) n'est pas pour nous déplaire ; Pasolini
    lui-même ne cessant de produire des usages profanes de la théologie
    chrétienne.

[^14]: Benjamin Fouché, "Petit chaperon loup", Revue Pøst, en libre
    accès sur le site de la revue.

[^15]: Jacques-Henri Michot, *ABC de la barbarie*, Al Dante, 2022.

[^16]: Extrait trouvé quelque part sur les internets et tiré de
    *Chaussure* (Nathalie Quintane, P.O.L, 1997).

[^17]: Jean-Marie Gleize, *Littéralité*, Questions théoriques, 2015.

[^18]: D'autant qu'une littéralité purement formaliste risque de se
    retourner contre l'intention d'appauvrir volontairement la langue
    afin de la rendre plus communément praticable. À ce titre,
    Jean-Marie Gleize mentionne les frères mineurs comme source
    d'inspiration de cette nudité et/ou littéralité dans l'exercice
    impliqué de la littérature.

[^19]: De PNL également : "Le monde ou rien."

[^20]: Pier Paolo Pasolini, *L'inédit de New York*, traduction de Anne
    Bourguignon, Arléa, 2015.

[^21]: Rainer Maria Rilke, "Lettre à Witold von Hulewicz", in
    *Élégies de Duino*, traduction de Rainer Biemel, Allia, 2015.

[^22]: Ce à quoi nous pouvons ajouter un troisième terme,
    *Veräusserung*, qui appartient au vocabulaire juridique : "aliéner
    un bien".

[^23]: Ajoutons : à Berlin notamment, capitale du Royaume de Prusse, où
    il finira sa vie auréolé du titre de philosophe national.

[^24]: Walter Benjamin, "L'Œuvre d'art à l'époque de sa
    reproductibilité technique", in *Œuvres*, traduction de Frédéric
    Joly, Cédric Cohen-Skalli et Olivier Mannoni, Payot, 2022.

[^25]: Paul Klee, *Théorie de l'art moderne*, Gallimard, 1998.

[^26]: Rainer Maria Rilke, "Lettre à Witold von Hulewicz", in *Élégies
    de Duino*, op. cit.

[^27]: Pier Paolo Pasolini, *La nouvelle jeunesse*, traduction de
    Philippe Di Meo, Gallimard, 2003.

[^28]: Pier Paolo Pasolini, *Trasumanar e organizzar*, Garzanti, 1971.

[^29]: Pier Paolo Pasolini, *La nouvelle jeunesse*, op. cit.

[^30]: Pier Paolo Pasolini, "Poésie en forme de rose", in *Une
    vitalité désespérée, Anthologie personnelle*, traduction de José
    Guidi, Gallimard, 1973.

[^31]: "Qui nous a fourni --- à nous, vieux et jeunes --- le langage
    officiel de la protestation ? Le marxisme, dont l'unique veine
    poétique est le souvenir de la Résistance, qui se renouvelle à la
    pensée du Vietnam et de la Bolivie." ("Lettre de Pier Paolo
    Pasolini à Allen Ginsberg", in *Correspondance générale*,
    traduction de René de Ceccatty, Gallimard, 1991.)

[^32]: Pier Paolo Pasolini, *Poésie en forme de rose*, traduction de
    René de Ceccatty, Payot, 2015.

[^33]: Giacomo Leopardi, *La poésie de Giacomo Leopardi en vers
    français*, traduction de A. Lacaussade, Alphonse Lemerre Éditeur,
    1889.

[^34]: La survivance suppose la persistance d'une lumière passée, tandis
    que nous visons le communisme, soit l'unité dialectique de l'être et
    du langage. Cela suppose un rapport plus offensif à l'histoire et à
    la tradition.

[^35]: Pier Paolo Pasolini, "Lettre à Franco Farolfi", in
    *Correspondance générale*, op. cit.

[^36]: "Je te le dis parce que je suis un marxiste non officiel, et mon
    espoir n'est pas rhétorique." (Pier Paolo Pasolini, "Lettre à
    Massimo Ferretti", in *Correspondance générale*, op. cit.)
