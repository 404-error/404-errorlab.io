---
persona:
  - Lucien Raphmaj
  - Marc Verlynde
title: "Le Tiers Essai"
slug: "le-tiers-essai"
date: "2022-06-16"
echo:
  - esthétique
images:
  - "img/220602-lrmv-le-tiers-essai.jpg"
gridimages:
  - "img/220602-lrmv-le-tiers-essai-grid.jpg"
sourceimage: "https://commons.wikimedia.org/wiki/File:Nautilus_macromphalus_1.jpg#mw-jump-to-license"
notes: "L'image du <a href=\"https://commons.wikimedia.org/wiki/File:Nautilus_macromphalus_1.jpg#mw-jump-to-license\" target=\"_blank\" rel=\"noopener\">Nautilus macromphalus</a> se révèle par son *copyleft*. Et l’image dérive depuis, à l'identique de son partage, puisque l’image du monde est une dérive du monde lui-même."
summary: "Désormais le rêve et la critique se mêlent pour désigner des figures évanescentes, des ramifications inattendues, des voies non pas royales, mais inexplorées où l'on apprend à lire et à écrire la lecture à travers quelque chose de la logique du rêve, de son incertitude, de son fractionnement et de sa perpétuelle transformation, enfin quelque chose, toujours, de cette amitié confiante donnée au Tiers, afin de nous comprendre à travers ce miroir du texte, enfoncé loin sous la mer, recouvert de tant d'altérations, de points noirs, d'algues merveilleuses, de déformations et offert à tant de mutations inattendues."
citation: "Lire fait partie de cette mer, de ces armes, de ces amers, de ces larmes."
---

*Ici se lit la poursuite d’un entretien ouvert sur la critique littéraire, sur ses puissances et ses impuissances, et dont la question du rêve --- après celle de l’amitié dans [Stase-Seconde](/stase-seconde/) et celle de l’idiotie dans [À te souvenir de l’insomnie des mondes](/a-te-souvenir-de-linsomnie-des-mondes/) --- est l’onirique mutation dont il nous paraissait important de témoigner comme critique en approfondissant notre perplexité.*

---

Désormais le rêve et la critique se mêlent pour désigner des figures
évanescentes, des ramifications inattendues, des voies non pas royales,
mais inexplorées où l'on apprend à lire et à écrire la lecture à travers
quelque chose de la logique du rêve, de son incertitude, de son
fractionnement et de sa perpétuelle transformation, enfin quelque chose,
toujours, de cette amitié confiante donnée au Tiers, afin de nous
comprendre à travers ce miroir du texte, enfoncé loin sous la mer,
recouvert de tant d'altérations, de points noirs, d'algues
merveilleuses, de déformations et offert à tant de mutations
inattendues.


## rêve 1 : désormais

C'est une sorte de lumière. Peut-être une persistance rétinienne. Le
chevet d'une lumière qui subsiste, un instant encore, avant de
s'évanouir. La nuit, déjà. Jamais entière cependant l'instant d'après.
Une lumière qui te poursuit, une étoile qui éclaire un ciel connu. Dans
une dernière stase de conscience, tu sais morte cette lumière,
persistance d'une lueur que seule la distance fait encore briller. Mais
tu glisses dans un autre rêve, dans un autre état de conscience. La
lumière comme ressenti, pure sensation sans intellectualité. Un
effacement : elle est trop grande pour toi, la lumière stellaire. Elle
projette une ombre trop petite. Même si désormais, il va te falloir
faire sans celle initiale, ton rêve perçoit seulement des altérations de
formes. Un évanouissement, un sommeil animal. L'idée, à moins que ce ne
soit que le mot, d'un enroulement, un repli régressif. **Une sorte de
silence avant l'apparition de l'image**. C'est ce creux que l'on veut
laisser résonner --- étoile morte --- par le rêve, l'essai, l'essai du rêve,
le rêve de l'essai, par toutes les combinaisons, alternatives et
approches, d'une rêverie commune.

## rêve 1’’ : alors

C'est le contraire d'une décision. Pourtant s'y trouve quelque chose
d'**inéluctable**. Tu ne diras pas "fatal", car ce qui s'y joue est retiré
de tous les couples infernaux, de la vie et de la mort, de la lecture et
de l'écriture, du rêve et de la réalité. C'est autre chose encore que la
confusion d'un pays de brumes, d'un monde de nuages **stratifiés** par la
lumière. Ce qui se joue pour toi alors a le caractère de l'évidence qui
défait les partages, tentant d'ordonner selon des schémas **tensifs** que
cette expérience met en défaut. Dans ce défaut qui est qualité, qui est
lecture et invention du monde alors tu trouves ta raison d'être, ton
secret et ta puissance. C'est à ce point de bascule alors,
qu'hypnagogique tu te maintiens. Alors, toujours au bord de ce point
aveugle, de cet œil singulier du néant, ce trou noir du vertige de la
lecture tu te donnes à l'inéluctable, au fatal, et à la joie, te
disséminant alors, renonçant aux partages ordinaires du monde pour en
fréquenter ses parages, pour aborder par autre chose que le Moi,
éparpillé dans les mots, les sensations, les rivages inconnus et
familiers, la lumière et la nuit.

## rêve 2 : faux départ

C'est une sorte d'évitement. La poursuite du silence insomnieux au creux
même de tes rêves. Nos histoires désormais sont-elles condamnées à être
fragmentaires, nos rêves à être des bribes de récits, nos réveils une
dénonciation de nos incroyances à la Chute, d'heureuses suspensions de
toutes formes de conclusion ? Dans ce demi-sommeil dont s'élance une
partie de la parole qui s'essaye ici, perturbation, te voilà conscience
de ton enfermement dans un recueil de récits de rêves. Pis, tu flottes,
inconsistant, dans le souvenir de lecture lointaine de nouvelles, de
brèves histoires, d'amorces d'intrigues, d'amalgame de récits. **La
cohérence de l'imminence**, cette urgence si souvent essayée dans les
rêves, cette certitude dont nous éveillons la ferveur : une tension ici
et maintenant doit se produire. Tu t'éveilles à demi. Il te reste une
image, fausse et vraie figuration onirique : le rêve ou l'instant de
l'enrayement. Épreuve de la nécessité du saut. On voudrait te faire
toucher la manière dont on passe d'un rêve à l'autre. Au fond, nous n'en
savons rien. Il nous reste à rêver avec Toi.

## rêve 3 : reptation

C'est un glissement physique. Une sensation toute bête. Tu dors, tu
tombes. Le rêve parvient à te faire toucher ce vertige. Une sensation
idiote qui fait image : un tressaillement au seuil de l'éveil, un signe
vers autre chose. Ta jambe gauche tremble, léger déséquilibre comme si
tu tombais d'un trottoir, que tout ton corps compensait pour continuer à
avancer. Ça ne marche pas. Te voilà au royaume des métaphores
approximatives, des approximations de sens surtout. Tu sais la mollesse
ouatée du rêve, une chute sans fin. Après ce signe annonciateur, tu
saisis --- sans le comprendre --- que ta chute se poursuit sous une autre
forme, dans une autre dimension. Te voilà prisonnier d'une sorte
d'horizontalité sans contour. Une avancée insidieuse, quasi visqueuse
désormais quand tu pressens dans ta reptation appartenir à un autre
ordre, à **l'interstice des insectes**, songes-tu dans une nouvelle approche
du réveil. Une de celles qui te font comprendre que c'est d'autre chose,
toujours, que parle ce rêve. Serait-ce la peine d'en préciser les
variations métaphoriques d'une lecture ici à l'essai ?

## rêve 4 : le clavecin halluciné

C'est un cliquettement. La traduction d'un **impromptu** halètement,
peut-être. Une distorsion des cycles harmonieux du sommeil paradoxal.
Une perturbation --- un essai --- une distraction. Tu n'apparais pas
autrement. La conscience d'un bruit. Le mouvement qui, dans une
intuition, s'y associe. Un ressort. La bruyante dureté des claviers
anciens. Une pression, un rebond : une lettre s'inscrit. Un autre
picotement, ailleurs ; une autre lettre. Et toujours cette étendue
limitée, enroulée que tu te sens être, insecte des interstices. En
regard des jours, tu rêves d'un message, tu en pressens l'inscription
charnelle. Bien sûr, en continuation de ta vie diurne, dans la poursuite
de ce *double thinking* où toujours tu te reflètes, tu as aussi
conscience de l'artificialité de la métamorphose effleurée ici. Les
ressorts finissent par faire résonner un rythme. Quelqu'un, au-dessus de
toi, tape un texte. Tu n'en saisis pas le sens, les syncopes seulement,
la tentative de dire ce qui échappe. Tu te concentres, te replies dans
ta coquille (au sens aussi où le rêve relève du lapsus), tu crois
toucher les premiers mots dans un réconfortant enroulement : "C'est un
cliquettement..."

## rêve 2666 : le mal

C'est peut-être un cauchemar. *Quelque chose va mal, va terriblement
mal, va terriblement vers le mal*. Tu t'interroges. Pourquoi le monde se
réduit-il ainsi à cette ressemblance informe, à ces vertiges ? Le temps
de tous les côtés se réduit à ce présent étranglé dans ta gorge. Les
lettres passent mal. Tu déglutis mais tu n'as plus de salive. Tu
voudrais boire. Saoulerie qui ne te sauverait pas. Tout simplement car
l'idée même de salut s'est dispersée en un horizon de poussière grise
volant au gré des vents. Ici seulement tu ne peux pas lire autre chose --- et
tu ne voudrais pas lire autre chose. Tu découvres que la vérité
s'assimile mieux à la présentation du mal qu'à toute autre chose et tu
sais qu'il faut vivre --- qu'il faudra vivre, quand le temps te sera
redonné, ta lecture terminée, ton rêve achevé --- à la hauteur de ce
malheur. Peut-être est-ce qu'il y a une multiplicité dans ce mal qui
défait l'idée de Bien. Alors tu sais qu'il te faudra des mots, à
nouveau, multipliant l'obvie et la circularité pour **étrangler** à ton tour
cette sensation, ce cauchemar. Oui, à ton tour tu seras ce cauchemar, ce
cauchemar qui cependant, se défait et se sauve d'autres cauchemars :
communication.

## rêve 2666’’ : la plaie, la piscine

C'est une plaie, suintante, sidérante ligne de fuite. Question de point
de vue. Soudain, tu sais que tu rêves: tu es à la fois celui qui l'a
infligé, celui qui en souffre et qui, d'une main, en tâte prudemment les
contours pour comprendre cet impromptu et incompréhensible déchaînement,
cette latence de la souffrance. Masochisme ? Sadisme ? Souveraines
oscillations du rêve. Toutes ces stases de consciences se réunissent
dans ce constat : tout pourrait s'arrêter, comme ça. L'éclat d'un
couteau, la blancheur de la lumière. Peut-être serait-ce cela l'ultime
rêve : l'influx électrique de synapses qui se libèrent, un flash blanc
qui s'éteint. Mais le rêve, toujours, poursuit ses maléfices. Tu
t'orientes à la pulsation de ses altérations de lumière. Tant qu'il
reste un mot, une fautive dénomination, on essaiera des échappatoires.
Quelque part au Nouveau-Mexique, d'un côté ou de l'autre de la
frontière. Au bord d'une piscine, penses-tu pour ce bleu chatoyant à la
David Hockney. Trompeuse évidence sans doute. C'est celle-ci que nous
voulons essayer : la part obscure du rêve dont nous inventons,
traduisons le moins mal possible, les méandres, tient à ce que
l'évidence du Mal a déjà eu lieu, se répète. Des scènes, des visions,
des déformations, des indicibles tentations en approche. L'espoir aussi,
comme pour le rêve, d'une incessante interprétation pour se départir de
cette évidence, trompeuse comme un cauchemar : le Mal, serait-ce,
hubris, que tout fasse sens ? La voix maléfique qui te pose ces vaines
questions simultanément y répond. Tu te vois sacrificielle victime. Une
voix maléfique, sans doute toi-même, te susurre : le rêve c'est se doter
de l'illusion démiurgique pour mieux avoir le plaisir de tuer, athée que
tu es, le dieu en toi. Le rêve : les traces d'un dieu auquel tu as cru
au point de le tuer. À l'essai, reste le refus du nihilisme. Décidément,
ce rêve de meurtre t'impose de creuses songeries.

## rêve x : rêvistance

C'est peut-être la difficulté. *To sleep, perchance to dream, ay there's
the rub*. "Dormir, rêver peut-être. / Ah c'est l'obstacle !" *Rêver*,
disait-il donc, dans le futur royaume sans roi des morts. Il y a là la
même passion à laisser advenir quelque chose qui résiste, à élever cette
résistance à une autre dimension plutôt qu'à la dissoudre dans la neige
du commentaire. Quelque chose de la clarté nécessaire du rêve est
essentiel à cette ode à l'obscurité qui ne vise pas à en faire une
fascination, mais une *sublime* amitié : quelque chose passant sous les
limites de la parole révélée, parole **infraordinaire** plutôt que
spectaculaire, quelque chose qui dit sans reprendre, qui approche dans
une intimité qui préserve intacte la difficulté. Écrire pour affirmer
cette résistance, cette résistance qui s'offre comme un rêve, comme un
déplacement, un mouvement ne cessant de s'accomplir autrement.

## rêve 7 : archivies

C'est tout un ensemble disparate d'animaux. Non pas de symboles, de
représentations de vivants, mais ce qui fait la vie, quelque chose qui
se dépasse sans cesse, quelque chose de sans nom qu'on pourrait nommer
comme la ***vivance*** --- caractère peut-être obscur mais intense, ne
cherchant pas à éblouir mais plutôt quelque chose de la discrétion de la
proie.

Se faire de cette discrétion et ne pas traquer le sens, l'intention, de
ces rêves écrits, additionnés à ces métamorphoses qu'il te faut
incarner. Tu devras être de ces fabuleuses archivies, traces de vie et
archives du vivant dans ce qu'il a de plus fondamental,
démultiplications **archaïques** sous toutes ses formes. Toi aussi, tu as
été et tu seras une multitude d'animaux avant de vivre des épreuves qui
te transformeront. Peut-être parce que l'écriture a quelque chose
d'animal qui n'apparaît qu'au bout de ce cheminement où l'écriture
s'assimile à notre salive, tapisse comme des bactéries notre ventre,
faisant une étrange symbiose où l'on participe d'un même mouvement sans
jamais s'appartenir.

## rêve 8 : nautile

C'est la mer des Textes qui t'environne. Tes quatre-vingt-dix tentacules
te projettent vers l'avenir auquel tu fais dos. Avoue que c'est plus
doux que l'idée archangélique au visage déformé par les larmes du passé
et le vent éternel d'un progrès à venir dont le temps n'a plus de sens
ici. Le temps, le rêve et la mer se rencontrent et se racontent
l'histoire de tes espoirs célestes, de tes amours abyssaux pour le
*vampyrotheutis infernalis*. Une fiction où l'existence se transforme à
haute pression, où la philosophie métabolise la fiction, où l'inhumain
est la pensée, où l'animal nous fait monde. Ton être-au-monde est de ces
grandes profondeurs, de ces origines confuses. Y résistes-tu ?
Remontes-tu à la surface ? Nages-tu avec une insouciance dans les
reflets lunaires de cette eau ? Tu vis dans la légère profondeur
intermédiaire de cette mer des Textes où les reflets du soleil font
briller les éclats calcaires dont tu te nourris. Tu avales des
particules de texte. Tu métabolises les mots. Ta carapace de **nacre** se
constelle de points noirs en un texte qui t'est étranger. La mer n'est
pas un miroir pour toi. Aurais-tu la fluidité changeante de la peau de
la seiche que tu ne saurais lire ce dehors auquel tu appartiens
pourtant. Tu as besoin du Tiers, de l'Essai, de l'Ami.e, du Texte pour
te découvrir.

## rêve 8’’ : ventricules ventriloques

C'est une viscosité. La possibilité de penser, de flotter ; déjà la
nécessité d'un autre [<span id="support">support</span>](/ligne-dombre/#support). L'apprêté de la perception. Tu te dirais
méduse sans cette rugosité que tu perçois, dans un curieux amalgame
entre toucher et audition, comme une conséquence (ou ce qui en tient
lieu dans le déraisonnement, dissonance si riche en échos, du rêve) du
ressac. Mouvement en tous sens. Relativité de l'avant et de l'arrière
quand, dans le rêve ou l'essai, tes sensations tiennent à la pluralité
de tes capteurs, de collants ventricules ventriloques. Le jeu de mots
comme point cardinal : le vecteur, tu le sens, sur lequel tu flottes,
serait comme le souvenir du papier, sa virtualité pour ainsi dire.
L'espace forclos d'un essai onirique, numérique. Tu flottes sous
l'hypertexte, le codex et son défilement infini d'un livre numérique.
L'image de la mer, submergée par une nouvelle vague de rêve, n'est
pourtant pas qu'une métaphore. **Le ressac de la transparence**, la page
n'est pas blanche. Palimpsestueuse. Usure de la métaphore lunaire. Et
pourtant, soudain tu écoutes une lunaire irisation, tu te sens agité par
l'espoir de la marée, son indéfectible altération atmosphérique. Quand
la lumière change, tu la vois enfin. C'est l'éclairage critique que
nous voulons apporter.

## rêve 9 : ainsi

C'est indécidable. C'était pourtant une prophétie. Mais une prophétie
très loin enfoncée dans la mer du ciel ou de la terre --- les frontières
s'étant définitivement altérées en un vaste horizon noir et merveilleux,
une vase merveilleuse et presque gazeuse. C'est de là qu'émergeait cette
voix de prophétie, de cette confusion qui sur le moment t'apparaissait
comme le plus pur des bonheurs, dans une **modalité** nouvelle proclamant
dans un verbe sonore le tremblement plutôt que l'affirmation. Les
formules définitives des *Ainsi* avaient rejoint le pays magique des
*Peut-être*.

## rêve 10 : le labyrinthe des étoiles absentes

C'est un ciel et l'appel **aveugle** de toutes les étoiles. Là tu tentes de
reconnaître des divinités lourdes au front marqué par l'infini. Tu
cherches à t'ouvrir à la rose de tous les vents intersidéraux --- elle si
abstraite : faite d'un langage soustrait à tous les parfums. Tu cherches
idiotement des révélations en cette littérature et en ce rêve comme "en
cette nuit et en ce monde" ([a.p.](/desidentite/#ap)) quand la langue de la poésie se doit
d'être un **contre-hermétisme** et le contraire d'une révélation, concentrée
absolument mais jamais close sur elle-même, ouverte toujours, faite de
signes sans dessein, d'une main tendue, et retombant tranchée. Geste et
amitié --- esquisser des gestes et des pensées, se souvenir des étoiles
continuant à briller après l'aurore. Dans ce rêve-crâne, tu entends un
craquement. Une libération. Lignes de lignes formant un labyrinthe de
failles sur lesquelles tu danses sous ce ciel vide, politique et
physique, éthique et inconscient, dans l'imposture du langage toujours à
réinventer.

## rêve 11 : john lovecraft-carter

C'est le démon et la merveille. D'un même diamant où s'incluent des
fragments d'autres substances, d'autres temps, d'autres mondes, d'un
même cosmos. Elle t'a appris le Sans Nom. Elle qui n'est plus ni son, ni
sens, ni image --- mais couleur d'étoiles. Sans Nom pour ce monde. Sans
nom pour la vie. Sans Nom n'ayant plus ni peur ni espoir. Et quelque
chose d'autre, toujours, plus beau encore : l'appel, non pas une voix,
mais le tourbillon, ciels de **volcans subaquatiques**, sensibilité du chaos
du monde. C'est vrai, tu le sais, sans pouvoir l'articuler car tu t'es
trompé d'espèce. Tu es comme tout et tous, de multiples dimensions. Ta
vérité, tu n'en as plus, tu es dans l'inconnaissable de l'avenir. Dans
ton livre aux pages cyanosées, il y a des mers encore comme "à
l'intérieur de chaque rêve il y a d'autres rêves. D'autres planètes, des
rêves d'étoiles et de planètes. Depuis l'aube des temps, des milliards
de rêves ont vécu sur cette terre" ([j.-p.c.](/desidentite/#jpc)) Ces rêves nous relient au
monde, dis-tu. Tu n'as pas lu, jamais, non, tu as rêvé, toujours. On
peut être le ciel. Les galaxies. Les animaux. Les vivants et les morts.

## rêve 12 : limules

C'est une respiration profonde. Toujours la même mer des Textes placée
dans une lune aquatique qui n'est ni vraiment de ce monde ni vraiment
d'un autre. Tu te déplaces avec ce sang bleu et froid aux propriétés
réparatrices, reconstituant l'hémoglobine de l'eau, **la respiration des
silences**. C'est ainsi que tu écris partout, que tu voudrais écrire en
suivant les lentes pulsations de ton sang. Tu as raison, comme je te
comprends. Copules, clausules, notules, limules, ce sont des réalités
flottantes dans ton esprit s'assimilant à cette mer. Ce qui se dérobe à
nous, le point aveugle du sommeil du texte, c'est cela qu'il faut
approcher sans jamais le ramener à la lumière de la veille. Remontés à
la surface les trésors de pierres précieuses, les colliers de perles et
les bijoux ne sont plus qu'une vase sombre où tout s'est amalgamé --- une
vase cependant plus féconde, plus belle, plus riche que tous les trésors
manufacturés par le capitalisme de l'imaginaire. Alors la vase te glisse
dans les mains avec bonheur, tu ne la possèdes pas tandis qu'elle te
rend au monde, à ses odeurs entêtantes. Tu apprends les textures, les
douceurs, les écœurements de la vasière. Sa vie microbienne, ses vers et
ses gobies, et parfois tu lis encore sur le sable les traces qu'y ont
laissées les limules.

## rêve 12’’ : john lovecraft-carter II

C'est une pyramide. Une couverture pompière pour *pulp*. Image
d'enfance, aventureuse. Explorateur de Contrées, de Jardins Statuaires,
de manoirs aux bibliothèques interdites, infernales. *Necronomicon*
quelque part dans ces rayons, derrière ces persiennes, cette lumière
poussiéreuse. Un cauchemar qui ne t'appartient pas. Des rémanences
d'images quand tu y entends le nom qui, un instant, essaie d'être le
tien : John Lovecraft-Carter. Mortes-eaux du partage du rêve, la pleine
lune, forcément, qui fait se mirer le plancher, qui révèle ce que tu es :
une mite. Qui dévore les mythes te laisse entendre ton esprit diurne
qui prend ses calembours pour des révélations. Tu es repu, tu en as
dévoré des pages d'incunables, tu en as cumulé des rebuts d'encre où tu
devinais les réticules de peur, les restes de panique, les désirs de
revenir. Tu voudrais dormir, rêver dans ton rêve, mais, mite, tu crains
maintenant les cauchemars de toutes ces lectures incorporées. Au fond,
c'est peut-être ceci qui voudrait remonter de l'amalgame entre l'essai
et le rêve : la **peur panique** de ce qui pourrait y advenir, autant
d'images à notre commune crainte de l'anéantissement total,
irrémédiable. Laisser ouvert, en le redoutant, tous les possibles du
rêve, de la persistance des images au-delà de nous-mêmes. Tout ce qui
sépare de cette mort sans image, ce silence sans sentence. Une
pichenette, tu valdingues. Tu es aussi, par l'odeur --- cuir,
chesterfield, tabac froid, braises --- celui qui te chasse. Féroce
aventurier de retour, endormi, savourant d'autres rêves, compensant pour
moitié ce qui ne t'est pas entièrement arrivé. Le rêve, son essai, sont
ceci : superposition d'images où perdure l'aventure de l'ailleurs, d'une
autre lecture.

## rêve 13 : samaël

C'est l'aveuglement. Ce qui nous fait toujours tâtonner. Lire. Un destin
aveugle, un destin d'aveugle plutôt que de juge. Samaël plutôt que
Rhadamanthe. Avançant dans autre chose que la lumière, autre chose que
l'obscurité. Tu vois avec d'autres organes. Avec toute la nuit, ses
souffles, tu la sens de tous les mouvements, de tous tes tentacules de
nautile.

Nuit autre que noire. Tu comprends ? Tu dois comprendre autre chose que
l'image qui se présente à la lecture : défaire la vision, la révélation,
le jour et la nuit. En faveur d'une nuit autre-que-noire dont la qualité
est faite du mouvement même du retrait.

Et pourtant.

Dans l'image, et à la manière de la poésie : existentiellement.

Tu n'as pas à espérer autre chose que les étoiles.

Que la Lune parfois. Tu tâtonnes. Le monde ainsi a les formes de ton
imaginaire.

Oui la nuit est profonde avec les mots. Tu ne t'adosses pas à un arbre
pour rendre la justice, tu es la forêt, totale et végétale et animale.
Les fouissements mauvais des martres, les ligatures des champignons, les
ombres des feuilles mortes, les cocons des grands paons de nuit. Tout
s'apprend --- tu tâtes cette vie impossible à voir. Tu n'es pas seul,
Samaël, chacune de ces empreintes te décompose et te recompose. Tu te
guéris ainsi --- sylvothérapie des mots --- de la pathologie de la lecture
que l'on adopte en une nouvelle mythologie de la lecture :
maladie/mythologie de ne pas voir, de ne pas rendre des oracles. Libérer
des petites mites au corps blanc dans l'espace : amitologie.

Tu tâtonnes, Samaël, tu caresses ce que tu ne peux pas voir, tu acceptes
de ne pas faire de symboles.

## rêve 13’’ : saison hors saison

C'est un temps hors saison. Lisant que "rêver de la fleur bleue n'est
plus de saison" ([w.b.](/desidentite/#wb)) au temps de l'image. Au milieu des grands feux.
Au milieu du métavers. Au milieu de ta lecture. Le rêve est ainsi. Ce
milieu par lequel on surgit sans commencement ni fin. Vide intersidéral
où les printemps, les hivers, les automnes et les printemps sont
recouverts par les mêmes algues stellaires, par le voile des images et
le tremblement des mots. Tu es au milieu de la Saison Unique
Numérologique, soleil infernal et, crois-tu, éternel *jusqu'à la fin*.
Temps d'une époque sans suspens, d'une époque calculée mot à mot. Temps
mortel, nous le savons bien, nous le savons bien. Tu cherches pourtant
ce temps dans le temps --- ce temps sous le temps, ce *sustemps* du monde
fracturé, cette saison d'entre les saisons d'où rayonne le cosmos du
rêve et de l'obscurité spatiale illimitée. Le temps de la lecture où se
restitue cette absence de saison, cette absence de monde faisant
pourtant le monde et sa profusion, ses présences imaginales. C'est cette
trajectoire onirocritique bienheureuse que tu cherches, cette
trajectoire où les îles, où les étoiles sont les rêves et les mots et
les amers de ce monde où tous les animaux sont des rêves précieux, où
tous les mots sont des oiseaux sans nid voguant dans le Vide.

Saison mentale sentimentale comme le chante presque la chanson. Saison
fractionnée par le sommeil de toute langue. Saison du sommeil qui seule
rend possible un sens. Sommeil de Babel, d'une Babel ensommeillée pour
toujours, Tour endormie, aux pièces pleines de mots et de moineaux
assoupis, tandis que tout autour d'elle gravitent des soleils centaures
aux odeurs de mouches, et qu'entre les pierres poussent des cymbalaires
aux paroles confuses.

Dans ton rêve il y a là des gestes défaisant l'origine du monde,
l'originalité de l'instance d'écrire, qui te font t'acheminer vers un
autre temps de l'écriture, une autre latence entre écriture et lecture,
lecture et écriture, procédant par différentiel, reprenant d'autres
textes, d'autres morceaux de temps, de phrases se révélant non pas dans
le mélange mais dans l'étrange, dans le rêve inoriginal et originaire à
toujours poursuivre dans son principe plutôt qu'en ses images.

## rêve 14 : plongeon

Ce sont les visages différents de l'obscurité. L'envahissant silence de
ta respiration. L'approche encore de l'étrangeté de tes sensations par
l'approximation de ce que tu en as déjà ressenti. Tu entends
l'apaisement de ta respiration, un peu comme quand on plonge avec un
tuba. Premières brasses, la buée sur le masque, l'imitation de ton
rythme cardiaque comme seul bruit ne sont qu'une préparation. Il te faut
descendre. Une expulsion d'air, les joues se gonflent, le tuba prend
l'eau, tu quittes la lutte de flottaison. Dans ton rêve, tout est aussi
exactement concret, aussi rempli d'angles morts que derrière un masque.
Ainsi, soudain, le vertige des abysses. L'appel des profondeurs :
lumineux trou noir. Tu sais être dans le rêve, tu ne détournes pas le
regard. Tu te laisses aspirer. Nous sommes dans l'essai, car nous
éprouvons la fragilité de cette image faillible, un peu trop grande. La
lecture, ce serait la façon particulière dont chacun découvre des
gouffres, navigue dans les profondeurs du texte. On propose ici une
rêverie de ce que pourrait être ce mouvement. Peut-être, reprend ton
rêve, une question de respiration. Une défaillance de la métaphore pour
illustrer une altération de la perception. La libération par arythmie
cardiaque, le glissement, descendant on le saura, dans la transe
chamanique par l'imitation d'une respiration plus lente. Animale. Tu
sombres, tu ne vois pas en quel animal tu t'altères. Réconfort.

## rêve 14’’ : *néo* urashima tarō (ネオ浦島 太郎)

Ce qui se dérobe à nous, le point aveugle du sommeil du texte, c'est
cela qu'il faut approcher sans jamais le ramener à la lumière de la
veille. Remontés à la surface, les trésors de pierres précieuses, les
colliers de perles et les bijoux du Palais du Dragon se sont transformés
en une vase sombre --- et tant mieux : c'est que la vase est plus féconde,
plus belle, plus riche que tous les trésors manufacturés par le
narcocapitalisme. La vase nous glisse dans les mains, on ne la possède
pas tandis qu'elle nous rend au monde, à ses odeurs entêtantes. Nous
apprenons les textures, les douceurs, les écœurements de la vasière. Sa
vie microbienne, ses vers, et ses gobies.

## rêve 15 : onirostratégie

Ce sont les rêves comme notre chose commune et privée. Notre *paradoxa*.

Face aux Princes innombrables du Capitalisme, réapprendre à se réarmer,
comme la mer.

Lire fait partie de cette mer, de ces armes, de ces amers, de ces
larmes.

{{<asterisme>}}

Voir, nous, aveugles du rêve, peuple des livres, se confiant à
l'obscurité changeante, à la dissimulation de caméléon que l'on trouve
dans le rêve exprimé à un degré insensé, voir autre chose dans le rêve
que la répétition de toutes les déterminations, que toutes les
conditions, de tout le passé méditant la ruine de son avenir : quelque
chose de nuit qu'on appellera l'*inconditionnel*, l'*indéterminé*, la
puissance joyeuse du négatif.

Cela est faible, cela est négligeable --- cela aussi est
l'*indestructible*.

Lis.

{{<asterisme>}}

Ni divinité ni maîtrise --- il y a une sorte d'anarchisme inhérent au
rêve. Forteresses tout effondrées : le rêve est toujours ruine, forêt et
trace. S'y promener. Paysages sans image. Cela aussi : restituer les
parenthèses du sommeil, la ponctuation de nos nuits, des passages à vide
du texte.

{{<asterisme>}}

Un conquérant a eu un rêve. Il y a renoncé. S'il ne renonçait pas, il
serait mort. Dans son rêve.

Il y a quelque chose de l'errance dans la lecture, du vagabondage, du
banditisme parfois, d'un bandit qui t'embrasse avant de partir, qui
t'apprend à te défaire de l'inessentiel, pour quelque chose de brillant
comme l'aventure, de sombre comme l'existence.

{{<asterisme>}}

Dans ces eaux internationales noirement polluées par les dégazages
narcocapitalistes, grevées d'archipels d'îles en plastique, les essais
sont devenus des cœlacanthes trop énormes pour les mers et les pressions
où ils demeurent.

Il leur faut partir, il leur faut plonger dans les profondeurs du sable,
de la fosse des Mariannes jusqu'aux tréfonds de R'lyeh.

Nos essais --- ce n'en sont pas --- voudraient être des exocets sautant à la
surface des vagues. Fragments d'air, de mer et de soleil sur la mer de
l'aventure à venir.

{{<asterisme>}}

L'imaginaire, la lenteur, la lecture. Si tu fais le compte, tu verras
que cela n'est pas négligeable. Combien le rêve, combien le livre peut
aider chacun à se défaire de cette matrice qui nous produit avec nos
désirs manufacturés, nos intensités télécommandées, nos colères sans
conséquences.

Alors le rêve, la lecture, l'imaginaire sont à voir comme des éléments
d'une onirostratégie d'ensemble. Avec le reste des luttes.

## rêve 16 : après, la grotte

C'est une respiration profonde, encore ; une ponctuation, toujours.
Flottement sans Soi dans un noir abyssal. Des trouées soudaines. Le rêve
et l'essai ne sont rien d'autre. Tu aperçois une grotte, ta descente a
un sens. Précaire et faillible, à l'essai. On rêve d'**un rêve qui
invalide l'image**, en réfute les fausses évidences pour mieux tenter d'en
situer les sidérations. Tu vois une grotte, tu en rêves les symboliques
autres. Reste une attraction animale, une manière de lumière négative.
Un trou d'ombre si on ne peut faire l'économie des lieux communs du
langage. L'effleurement des algues, la seule sensation d'être happé par
un vide si concret, tu le sais seulement dans le rêve, qu'il ressemble --- comme
deux rêveurs entre eux sont extérieurement similaires et peut-être
magiquement identiques dans leurs productions oniriques (doit-on
vraiment te préciser, une fois de plus, que c'est ce qui est à l'essai
ici ?) --- à une chute dans le ciel. Une aspiration ascensionnelle, une
quête des profondeurs par un mouvement en tous sens. Sans toi, sans nous
qui tentons d'inventer quelques images, trompeuses évidences, à cette
perception fragmentaire, éthique, partagée, idiote, amicale, critique,
dont nous t'offrons ici métaphores et métamorphoses. Mouvement dans son
onirique pluralité. Te voilà au ciel au fond de l'abysse.

## rêve 17 : rebond

C'est une autre gravitation. Image avant tout, rythme et respiration et,
au dérobé, une senteur par un mot dévié ; tu ne perçois qu'une apparente
absence de gravité. Le rêve a ses trompe-l'œil ; l'essai ses
anamorphoses. Si nous parvenions à te communiquer ces rêves en absence
partagés, nous voudrions te communiquer cette panique
a-gravitationnelle. Nous en rêvons comme une autre modalité de lecture. Nous
t'en proposons une image comme issue d'un rêve. Celle qui devrait, dans
une onirique simultanéité, s'amalgamer entre ce que nous écrivons et ce
que tu rêves. Tu l'auras compris, il ne s'agit pas de colliger des
récits de rêves, mais d'une tentative de partager des rêves en les
faisant. Nous touchons alors à ce que serait l'essai : un laboratoire
qui saisit la pensée dans l'instant où elle se fait, celui où elle
devient autre chose. On dissémine (l'essai est générosité, refus
politique de la thésaurisation) cette image dans l'espoir (l'autre nom
politique de l'essai) qu'en toi, à profusion, elle fasse rebond. La
logique onirique est souvent sémantique, tel l'essai, elle s'invente des
**mots-sésames**, des déclencheurs. Nous rêvons --- t'en souviens-tu ? --- d'une
critique lunaire, de hasardeuses et révélatrices associations d'idées.
Image donc. Tu sens d'immenses cratères, des mers lunaires asséchées
telles qu'on les voit de la Terre, tu en perçois l'infranchissable.
Panique. Au rythme de ta reptation, une vie entière ne suffira pas à
t'en sortir. Tu t'enroules et te déploies. Et l'altération
gravitationnelle à toi se rappelle : sauts et gambades, bonds et
rebonds, hauteur de vue et écrasement dans l'œil-microscope, d'un
cratère l'autre dans une progression aléatoire. De quoi pourrait-ce être
l'image, l'essai ?

## rêve 18 : la matière noire

C'est une absence de fin. Une apesanteur imperceptible qui retient la
chute des corps, des récits. Le rêve et l'essai n'ont pas de fin.
Seulement des reprises, des images récurrentes, des polysémies à la
poursuite d'un autre éclairage. On te propose des itérations oniriques,
des revenances spéculaires pour (l'aurait-on déjà dit ?) cerner ce que
l'on n'ose dire. Tu te sens en une lunaire apesanteur ; tu ne t'abstrais
pourtant pas de la pesanteur. Reprises, redites, autres manières de
franchir la ligne, dialogue discontinu, partage : l'espoir que tout ceci
n'ait pas de fin. Serait-ce la peine de pointer le refoulé de ces rêves,
le morbide inconscient qu'ils scotomisent. Ombres et lumières, nous
rêvons des renversements. Passer de l'un à l'autre comme on passe d'un
rêve à l'autre, comme on invente des continuités que tout aussi bien on
pourrait nommer idiote critique amicale. Sans doute est-ce pour cela que
nous te proposons un parcours, d'autres liens. Tu vis dans le rêve
d'autres flèches, d'autres images qui font signe vers une réalité que,
bien sûr, ici nous nous contentons d'esquisser. Des ébauches de rêves
pour un essai qui se refuse à la mort de la conclusion, de la pensée
fermée. Imagine, tu te vois dans une nécropole. Sableux souvenir de
pyramide ou l'espoir de glisser sur des hiéroglyphes, d'en décrypter la
portée en t'y absorbant, de sauter --- par l'irrespirable pesanteur de ce
souterrain --- à un autre glyphe, une autre rune, un autre réservoir de
rêves. Un soudain rayon de lune, tu te vois déchiffrer des aspérités qui
ne font sens que pour toi. Tu te rendors, ailleurs.

## rêve 19 : laboratoire à vif, une enquête

C'est une table de dissection. Une table de la marque Maldoror^TM^ en
forme de texte. La table est brisée. Tant mieux, il te faut de nouvelles
tables. Regarde ce désastre, ce laboratoire du monstrueux où l'on réduit
le rêve à la chimère, la lecture à son intertextualité. Cet aventurisme
surréaliste on l'a connu tant de fois, passion feuilleton du Doctor
Jekyde dont on termine aujourd'hui (et on l'espère pour longtemps) les
folies :

« Quand vous rentrâtes dans le laboratoire du Doctor Jekyde vous aviez
en tête ce qui se cache sous le nom même de laboratoire textuel : un
bureau mal rangé, des machines vrombissantes aux paroles insectoïdes,
des feuilles volantes jonchant l'espace un peu partout et puis des
schémas placardés au mur avec des punaises, des aimants, ou du scotch,
selon.

« Tu regardais ta partenaire face à la porte en acier noir constellée
d'éraflures du laboratoire. Une porte très neutre, une porte de fond de
couloir. La porte 2501, ça, comment l'oublier ? Vous échangeâtes un
regard puis elle enfonça la porte d'un coup et je la suivis, arme au
poing, prêt à rugir un ordre quand tout s'étrangla dans ta gorge.

« Rien ne vous avait préparé.e.s à ce que vous alliez rencontrer. Ce fut
l'instant le plus étrange de votre carrière. Dans le désordre de la
pièce gisaient des structures hideuses, clairement étrangères à tous les
règnes connus et pourtant vivant, enfin bougeant, dans une semi-vie
proche d'une agonie perpétuelle si tant est qu'on puisse dire que ces
choses --- ni animales, ni végétales, ni simples constructions --- soient
“vivantes”. Vous ressentiez face à ce spectacle un malaise et un
vertige immédiat, paralysant la pensée. Vous restiez les bras ballants.

« Vous aviez eu les rapports du psy incriminant ce “critique fou”, mais
comment imaginer qu'il avait poussé si loin ses expérimentations au-delà
de ses petits collages ? Ce qu'il avait exactement fait t'échappait et
tu doutais de vouloir savoir jusqu'à quelles extrémités cette horreur
avait été (sorte d'extrémitié, tel un extrême de l'amitié). Tu savais
déjà que tu devrais désormais vivre avec ces images, celles de ces fines
poutrelles blanches suspendues s'agitant en cliquetant une symphonie
amoureuse et déréglée, araignée insensée vous fixant de ses yeux noirs
et vides, vous regardant de si haut, d'un regard qui vous traversait
avec un malheur démesuré. Sur ce visage, dans ce corps insensé,
déglingué, c'était *L'attente, l'ossuaire* te dira plus tard quelqu'un
de la police scientifique. À quoi cela te servira-t-il de le savoir ?
Même alors. Tu étais incapable de bouger. Juste assez **hébété.e** pour
passer d'une structure à une autre, à regarder par exemple la cage de ce
qui devait être *Le dernier singe* aux allures illimitées de créature
ayant dépassé la vie et éructant des noms fabuleux mais désarticulés.
Tout dans ces créations montrait qu'elles avaient échoué. Qu'elles
avaient été abandonnées à leur défaut, à l'éternelle boucle de leur
ratage, à ces formules incomplètes, à ces formes tantôt désintégrées,
tantôt atrocement déformées en autres choses qu'elles-mêmes.

« Encore aujourd'hui, tu te réveilles au milieu de cauchemars résonnant
des chants dissonants qui se répercutaient dans la pièce, opéra terrible
du manque de compassion et tu maudis l'auteur d'*Au moment radieux*, de
*La folie solo* et de *L'instant de ma nuit blanche*, car il a créé en
toi --- en moi, en nous --- des prolongements aux horreurs qu'il a
convoquées.

« Mais si c'est ainsi, comme dirait l'autre, “je le prends sur moi et je
le veux sans fin” ([m.b.](/desidentite/#mb)) »

## rêve 20 : au-delà de saturne

C'est une poussière glacée. Nous avons dépassé l'orbite de Saturne. Les
anneaux d'astéroïdes n'ayant jamais formé de lunes.

Passés irréalisés. Lunes qui n'existeront pas.

Qui nous manqueront pourtant.

Et pourtant.

Adieu les lunes, adieu mélancolie.

Adieu les textes inconnus.

Les fragments d'histoire et de lecture.

Nous, les plutoïdes de la ceinture d'astéroïde de la ceinture de Kuiper,
nous formons d'autres rapports au Kosmos.

Gravitant autour du vide, nous ne cessons de tomber vers le texte et de
graviter autour sans jamais y tomber.

Nous ne sommes pas de la nature des comètes.

Nous révolutionnons.

Nous répondons à l'appel de l'étoile.

Ouverts à la rose de tous les vents intersidéraux.

Nous révolutionnons.

Nous répondons à l'appel de l'étoile.

Inférer ces espaces flottants à la manière d'une cosmologie du sommeil,
en traçant le contour des ombres, les fluctuations de la lumière, les
effets de vortex des trous noirs. Apprendre et appréhender.

Lire par interférence. Par interpolation.

## rêve 21 : l'esprit d'escalier

C'est une enivrante proximité à la sensation. Une panique perceptive qui
t'y réduit, t'aide ou te contraint à considérer tes oniriques morphes.
Nous t'en proposons des approximations, des reprises hypothétiques de ce
qui, peut-être, se déchire dans cet oubli, en cette absence
onirocritique où se dessine la part plurielle de ce que tu ne parviens
pas entièrement à être. Ça pourrait être ceci : sans âge, le glissement
d'un escalier verni ; l'idée que tu te fais de l'odeur de l'encaustique.
Un détraquement, à l'évidence. Quelque part entre l'échelle de Jacob et
l'escalier de service de l'ascenseur social. Une panne, l'ascension.
Dans tout rêve, malgré tout, tu sais ou sens, c'est tout un, que la
marche suivante illustre l'irritation de l'illumination. Il te faudrait
grimper, dévaler. Tu te perçois seulement dans une progression
microscopique. La reprise d'une reptation, mais avec des pattes --- glissements
et boitements. Au-dessus, en dessous : promesse en
souffrance d'un autre mode de progression, de l'apprentissage d'un
mouvement qui ne définirait ni ta forme ni tes appréhensions. Tu l'as
compris, nous ne parlons plus seulement de ton rêve. Plutôt d'une
latence : sidéral [<span id="interstice">interstice</span>](/ligne-dombre/#interstice), désidentitaire béance. Le délai que nous
sommes, le rêve que nous sommes. L'idiotie, la lecture, l'amitié --- la
critique --- sont échos heureux de ce trop tard. Au rêve de l'essai, nous
te proposons des amorces, passages dérobés dans le *ping time out* du
souterrain hypertexte. Idées, images sont déjà là : elles surviennent
seulement en d'oniriques revenances, des souvenirs sans appartenance.
Une autre absence, un autre espace, une autre immédiateté. Saut sidéral
vers la marche suivante, à toi d'inventer les libres associations qui
t'y mèneront, ta vie entre tes rêves et leurs lectures.

## rêve 22 : abyme

C'est un ravissement, une abduction. Enlèvement, soulèvement :
soustraction à l'évidence de la logique du sens. On voudrait disséminer
tes images concrètes comme le rêve, à son instar inconditionnelles.
L'image d'un intermède tacite. Rêve, essai sont silence --- latence où la
fiction de nos identités diurnes (sociales ; aliénées) sont ravies.
Incarnation dans une formule qui ouvre à l'illimité de l'image : un
enlèvement vers d'héraldiques abymes. Contagion d'images qui se
croisent, stellaires parallaxes de rêves qui entre eux résonnent, se
lisent et ainsi se délient. Contre-hermétisme d'une parole critique
d'une lecture à contre-nuit. Phagocytage à tous les étages. L'incertaine
traduction du rêve comme épreuve de l'enlèvement de la lecture. Trop
tard, toujours. Ici on se lie, on se pirate, on s'amalgame. Failles et
fragments ; interprétations et extrapolations. Travail sur la tessiture
du rêve, les oniriques revenances que le texte invente. Mouvement
spiralaire : [l.r.](/desidentite/#lr) lit [m.v.](/desidentite/#mv) ; [m.v.](/desidentite/#mv) lit [l.r.](/desidentite/#lr) à l'infini comme épreuve de
l'altération. Le texte est rendu, publié, infiniment relu, on y comprend
alors ce qu'on n'y avait pas lu. On recommence autrement, toujours. Le
rêve. Abduction : lire comme si tu étais enlevé par des extraterrestres.
Absence inexplicable à laquelle tu ne saurais tout à fait croire.
Nouveau-Mexique, un champ de blé --- histoire déjà racontée. Le
ravissement d'un autre toi-même. Lecture. Fondu au blanc. Les images
d'un laboratoire à vif sont ailleurs. Tu reviens avec la sensation de
survivre à une expérience invécue, oniriquement extérieure. Tu reviens à
ton point de départ : tu témoignes de ce que tu ne sais pas. Prophétique
ignorance. Abduction : recommence à t'enlever toi-même. Le rêve.

## rêve 23 : lumière

C'est un pastiche. Façon détournée de trouver une forme à cette
déformation des jours que serait le rêve, au décalque des lectures que
serait la décantation critique. Une sempiternelle imitation : on
voudrait te faire toucher du doigt cet estrangement onirique comme
souveraine non-appartenance. Flottante et fugitive sensation d'un temps
autre. Tu en entends l'écho dans la rémanence d'une phrase dont les
sonorités orienteraient ce rêve. Lecteur, éveillé, tu y reconnais un
pastiche de ~~[m.l.](/desidentite/#ml)~~ : "un pied dans l'Éternel, préserver la lumière."
Résonance dans les replis de ton cerveau reptilien. Reptation : le
souvenir de la flamme \|\| la promesse du feu qui fera foyer.
Rétrospection et projection : tu es, ô rêve, métamorphose de la part
prophétique, passéiste, de cette parole idiotement, amicalement,
critique. Le maintenant (mise en lumière de la main tendue qui le
constitue) est futur déjà passé. Tu rêves les pastiches de ton passé,
les mimétismes de ton futur. Lueur alors comme seul le rêve sait en
produire : impromptue illumination qui survient simultanément à la
conscience que cette lueur a toujours été présente. Tu auras compris que
nous parlons aussi de lecture : une traversée de l'apparente immobilité
du présent. On la touche non tant par une insuffisance du ressenti,
l'espoir de sa compensation, de son rattrapage, mais bien comme tangence
à un manque fondamental. Celui dont s'élance notre parole, sur/dans
lequel elle ne peut que revenir, sa seule lumière serait cette question,
traduction sans doute de ce pied dans l'Éternel : qui témoignera pour le
témoin ? Tu retrouves ton enroulement, ta coquille --- secrètes et
fautives correspondances. Tu te réveilles sur le souvenir de cette
phrase d'[e.l.](/desidentite/#el) : "apercevoir les hommes en dehors de la situation où
ils sont campés, laisser luire le visage humain dans sa nudité." Aucun
lien sans doute hormis celui puissant, intuitif aperçu à l'instant du
réveil. C'est cette lumière que nous voudrions apporter à nos lectures.

## rêve 23’’ : obscurités

C'est un rêve aussi, et toujours. Un ibis lunaire qu'on distingue à
peine contre la nuit noire. Un ibis qu'on aperçoit par intermittence,
noir contre noir, dans la découpe intermittente des étoiles dans "cette
obscure clarté qui tombe des étoiles" (g.a.) qui te guide dans ta nuit,
dans cette nuit propre et impropre où bientôt tu feras une constellation
de l'ibis aperçu. Change de temps, de lumière, de point de vue. Comme si
ce qu'il fallait inventer avec la critique était ce rêve même, cette
nuit même, ce principe où les oppositions n'ont plus ce tranchant
violent d'un plein jour idéel mais plutôt cet espace-temps paradoxal qui
est le réel où lecture et écriture, obscurité et clarté, désir et
plaisir, enchantement et mélancolie, théorie et fiction, passé et avenir
s'appartiennent et se mêlent.

Toute nuit critique est cela. Ces lumières et ces obscurités : ces
pluriels infiniment recommencés. Ces passages et ces silhouettes
d'ombre. Un souvenir passe devant l'étoile qui soudain faiblit et tu
devines dans cette soudaine baisse d'intensité une planète, tu devines
ses matins et ses palmiers, ses soirs et ses halliers. Parler, **ciller**,
communiquer. Apprendre à voir dans la nuit même, à ressentir les
variations de l'obscurité, les fantômes de l'espace noir entre les
constellations, voilà la contre-nuit dans laquelle raconter comme
autrefois près de la lumière sacrée du feu des histoires pour craindre
et espérer, pour donner des formes à l'informe. C'est à ces variations
presque musicales et définitivement nocturnes que tu dois te donner en
redéfinissant la nuit, la lumière et le récit pour exprimer ta vision,
ta manière de lire dans les millions de couleurs de ton obscurité.

Rien d'occulte dans cet apprentissage de l'obscur, rien de mystagogique.
Rien de romantique non plus. Rien de contourné ni de compliqué à la
lecture. Mais la restitution de l'expérience dans son entièreté, lumière
et obscurité, clarté et mystère, opacité et respect de l'altérité --- de
l'autre dans son "visage de nuit" ([p.c.2](/desidentite/#pc2)).

Des lumières et des nuits jettent des sorts et des étoiles.

Tu lis, toi aussi.
