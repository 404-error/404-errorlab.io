---
persona:
  - Étienne Michelet
title: "Fragments pour une théorie Vampire I"
slug: "fragments-pour-une-theorie-vampire-1"
date: 2023-04-14
echo:
  - ontique
images:
  - "img/230401-michelet-fragments-pour-une-theorie-vampire-1.jpg"
gridimages:
  - "img/230401-michelet-fragments-pour-une-theorie-vampire-1-grid.jpg"
sourceimage: "Étienne Michelet"
summary: "Wonhyo, Les Révélations du Crâne. Alors qu'il médite dans une cabane abandonnée près de Séoul, le bonze Wonhyo est pris d'une soif incontrôlable. Il sort alors en pleine nuit afin de trouver de l'eau. Aux abords d'une caverne, et sans pouvoir distinguer ce qu'il touche, il croit reconnaître un bol d'eau fraîche posé sur le sol. Sans hésiter, il boit l'eau qui lui paraît alors la plus pure qui soit. Mais au matin, il découvre que le bol est en réalité un crâne poussiéreux rempli d'eau de pluie. Cette découverte, au lieu de l'affliger, lui apporte une joie indescriptible."
notes: ""
citation: "Mais le silence est un souffle."
poetry: false
hangindent: false
---

## Wonhyo, Les Révélations du Crâne

Alors qu'il médite dans une cabane abandonnée près de Séoul, le bonze
Wonhyo est pris d'une soif incontrôlable. Il sort alors en pleine nuit
afin de trouver de l'eau. Aux abords d'une caverne, et sans pouvoir
distinguer ce qu'il touche, il croit reconnaître un bol d'eau fraîche
posé sur le sol. Sans hésiter, il boit l'eau qui lui paraît alors la
plus pure qui soit. Mais au matin, il découvre que le bol est en réalité
un crâne poussiéreux rempli d'eau de pluie. Cette découverte, au lieu de
l'affliger, lui apporte une joie indescriptible. Wonhyo comprend que la
perception du réel ne dépend que du jugement de la pensée discriminante.
La représentation de l'objet est une construction mentale, qui change et
influence la vue et les sens.

{{%asterisme%}}

Le goût de l'eau croupissante vous paraîtra pur, si le mental se le
représente tel quel. La perception sensorielle elle aussi est dépendante
d'un conditionnement mental. La soif ne cherche que son assouvissement,
mais non le goût véritable de l'eau. Il ne dévoile en rien la qualité
d'une essence pure et qui serait déjà présente dans le cœur des choses.
Il y a ici une brèche, ou nous dirons plutôt une révélation sortie d'un
crâne. Nul ne peut juger de l'essence des choses. C'est un leurre issu
de la représentation, rien de plus qu'un reflet du miroir de la
subjectivation de l'objet. Elle n'est qu'une production de la pensée qui
*symbolise* le monde. Nous sommes plus que jamais dans une crise de la
représentation. Nous percevons le monde au travers de *vues* qui
prétendent donner un visage au réel. Mais nous ne sommes plus capables
de voir, parce que nous ne voyons plus l'*inenvisageable*, ou ce qui n'a
pas de goût, ce qui ne dépend pas de la volition humaine, d'une soif ou
d'un désir intéressé. Nous ne reconnaissons plus l'*insipide* par faute
de goût. Nous sommes aveugles par cette incapacité de déposséder les
choses d'un visage, d'une nature qui joue alors le rôle d'un voile.
L'image-remède n'a que la saveur de l'apaisement d'un trouble. Les
images répondent aujourd'hui à la satisfaction des pulsions
narcissiques. L'image panse le trouble du soi, mais elle ne pense rien
d'autre que soi. Elle ménage les failles de l'ego.

{{%asterisme%}}

La production des phénomènes occulte le silence. Souvent la langue se
tord, ou s'*abîme* dans des divagations. La précarité du raisonnement
vient non pas du Vide, apatride et essentiel, mais au contraire de
l'accumulation de formules. Le silence n'est plus tenu en estime par
l'humain. Le flux sans mesure de la parole est pour lui une manière
d'asseoir son autorité névrotique et nerveuse. Le bruit croit s'imposer
par la force, recouvrant le monde par la contagion des images
parasitaires. Mais le silence est un souffle. Il est aussi une
exploration, comme l'on creuse au travers de la matière frangible pour
trouver le noyau originel qui y était dissimulé. Le méditant-vampire,
celui qui pratique le *seon*[^1] en Vampire, abandonnera les pièges de
la rumination mentale, mais par là, recouvrera la vision claire du
silence. Ce silence ne l'abandonnera plus. Il fera sens comme présence
pure et centre du vivant.

{{%asterisme%}}

Qui me suit encore ? Continuons, car selon la légende, cette révélation
(du crâne) permet au bonze Wonhyo de vaincre, de maîtriser ses pulsions.
Il se détache ainsi de toute pensée analysante ou discriminante, la
considérant comme un piège. Elle n'est pas source de connaissance, mais
s'épuise dans les figures de la représentation. Ce n'est que par un
regard désintéressé que les phénomènes du réel ne seront plus perçus au
travers du miroir déformant de la volonté. Ce regard vide de toute
volition échappe à l'emprise des cercles pulsionnels. Ce retrait de
l'*être* devient paradoxalement une reconnaissance de l'*être*. Il ne
s'agit pas de répulsion, mais de détachement. Seul ce détachement permet
de voir le cœur de l'*être* qui n'a pas de visage, qui n'a pas de
langue, inconditionné, dans cette possibilité qu'engendre le Vide de
faire exister le souffle dans toute chose.

{{%asterisme%}}

La pulsion de l'ego n'en finit jamais de vouloir remplir les
interstices, de saturer le réel de phénomènes. La pulsion refuse le
Vide. Elle alimente cette saturation qui nie le souffle du Vide. Le
trouble se nourrit ainsi de trouble, pour ne plus sortir de la torpeur.
C'est sa condition d'existence, que de ne plus remédier au surgissement
d'une souffrance, sinon par une autre souffrance, en ne l'apaisant que
par le poison.

La pulsion entraîne le voilement, ou l'occultation de l'invisible dans
l'*hypervisibilité*. L'invisible est ainsi méprisé. L'occultisme réside
aujourd'hui dans le caractère adipeux de la profusion des images et des
phénomènes du mental, non dans la pratique méditative, targuée trop
souvent de mysticisme, ou encore de lubie d'époque. Le temps de la
pulsion est vibrionnant. Il s'attache à des représentations qui n'ont
aucun centre. Le temps de la pulsion décentre, c'est celui de la
rumination mentale, de l'épuisement psychique ou de la pensée
obsessionnelle. Le monde contemporain vibrionne dans les images qui
perturbent notre perception du temps. Elles sont virales, dans le sens
où elles détournent un fonctionnement et son équilibre. Le psychisme est
contaminé par les images qui sont alors *synthétisées* dans le processus
de la pensée. L'image comme représentation d'un temps différé, hors du
temps vécu, devient l'objet d'une pulsion par un phénomène de
*réplication*. Comme sous la charge d'un virus, les cellules répliquent,
le psychisme enclenche alors un nouveau mécanisme, celui de chercher à
reproduire indéfiniment cette apparition représentative d'un hors-temps,
comme s'il s'agissait d'une porte de sortie face à l'écoulement du
temps. Ce leurre tente désespérément d'échapper à l'anxiété produite par
le défilement temporel, mais il ne fait que reproduire la même erreur
dans un déroulement obsessionnel, l'installant dans la chronicité
maladive. Les images deviennent un cercle ininterrompu qui épuise le
psychisme, l'enferme dans la répétition du même.

{{%asterisme%}}

Le défilement des images est un délitement de la pensée. Les possibles
s'amenuisent dans la représentation d'un monde où le monde ne se
reconnaît plus lui-même et où le temps ne tient plus, sa trame étant
comme retournée en tout sens. Le temps n'a plus de centre. Il est tout
simplement nié, déraciné, et il ne produit plus d'avenir. Dans l'image
hypervisible, le présent n'existe plus, ou seulement comme un ex-voto où
la tension des transformations n'agit plus. Tout y est perdu d'avance.
Le passé est dépassé par le contentement d'un futur qui n'est déjà plus
le futur, mais rejoué en tant que passé. La lecture d'une image ne
rejoue pas le temps. Elle en propose une version désœuvrée en inscrivant
l'avenir dans un passé perpétuel. Ce temps consommé, filtré, absorbe
l'attention, la détache du centre, ou du cœur de la pensée. La pensée
sans ce noyau temporel est déracinée. Elle vibrionne, s'accroche à des
cendres.

{{%asterisme%}}

La pulsion paralyse l'*être* dans l'agitation et la rumination mentale.
L'immobilité de la méditation, qu'elle soit dans le zen ou le *seon*,
est un acte ontologique dans l'expérience du mouvement. Elle est une
*pratique ontologique* qui s'inscrit dans l'expérience du *devenir*.
Elle n'analyse pas les phénomènes, ne les nomme pas selon une volonté,
mais les laisse advenir sans les *envisager*. Pratiquer le *seon* ou
*zazen*, c'est être traversé par les phénomènes. C'est être capable de
les laisser surgir, de leur apparition à leur disparition, mais sans
intervenir, pour voir ainsi la chaîne d'interdépendance s'épuiser
d'elle-même.

{{%asterisme%}}

Le méditant-vampire dévisage l'*inenvisageable* en tournant son regard
vers l'intérieur, vers ce qui est à la fois le nœud d'un centre
intérieur et l'infini lointain hors de tout centre. Ce nœud d'unité, qui
n'a pas de nom mais qui produit l'*être* dans la démesure, est apatride.
C'est le Vide lui-même qui est le cœur du temps. Au cours de la pratique
méditative, la concentration mentale revient à ce cœur sans substance, à
cet embryon de la démesure, à cet enracinement dans le *devenir*. Le
*seon* est une méthode qui permet *d'avoir des yeux derrière les yeux,
de savoir les retourner vers la blancheur des intériorités*[^2]. Il
s'agit de se rapprocher de l'*épochè*, de ce noème infalsifiable, ou
*noyau de la réduction*[^3] qui approche l'*être* au plus près, et qui
dans l'acuité encore, redéfinit l'expérience du sensible comme source de
connaissance, sans plus user d'une dialectique qui tenterait de définir
ce qu'est la pensée par le recours aux procédés du langage.

{{%asterisme%}}

Les sages taoïstes enseignent sans paroles. Ils donnent à voir
l'invisible indivisible, étant l'Un qui unifie les *dix mille êtres*.
Cette action naît du non-agir. C'est une foi en l'apatride, en ce monde
qui n'a pas de langue. Le *Tao* n'a pas d'image. Toutefois, Lao Tseu le
compare à une matrice, un embryon incommensurable, mais aussi à une
source d'eau qui coule dans la cavité d'une montagne. Des figures
mythologiques viennent s'y abreuver, jusque dans l'ombre et la tourbe.
Le sage taoïste dévisage le monde, lui retire les masques de la morale.
Ainsi, il le *dévisage*.

Le crâne n'est pas un bol, et c'est à cette même source d'eau insipide
que Wonhyo boit. L'*insipidité* est une révélation, elle n'a rien à voir
avec un quelconque nihilisme ontologique. Elle ne s'oppose à rien, même
pas à une brûlure héroïque du vivant, un romantisme qui à vouloir trop
vivre ne répond plus que par la pulsion de mort.

{{%asterisme%}}

Zhuangzi[^4] rêve ailleurs d'un crâne qui lui parle et veut lui faire
entendre la joie des morts, qui est selon lui incomparable : « Les morts
n'ont ni prince au-dessus d'eux, ni sujet au-dessous d'eux, ni travaux
saisonniers. Ils laissent passer les saisons comme l'univers ». Il ne
faut surtout pas entendre *cette joie dans la mort* comme une décision
morbide ou une réfutation du vivant, mais la comprendre comme une
figuration de ce que pourrait être la libération de l'attachement aux
phénomènes. *L'essentiel est mystère*[^5]. Cet essentiel ne se dit pas
mais nous traverse dans la mutation et les cycles de transformation du
vivant. Le mystère est dans le vivant, non dans sa représentation
pulsionnelle. Le mysticisme y voit une transmigration ; le Saint ou
l'Idiot y voit sa joie ; celle de sourire dans le Vide, avec des yeux de
Vampire[^6].

[^1]: Versant coréen du Zen japonais, mais ici pratiqué dans sa *forme* Vampire et Apatride.

[^2]: *Praxis critique ontologique*, Kosmokritik, 8.4432.

[^3]: *Éternité et historicité*, Jan Patočka, Verdier.

[^4]: Tchouang-tseu.

[^5]: *Tao Te King*, 27.

[^6]: *Mudras Vampires, Traité du Vide Apatride*, à paraître, 2023.
