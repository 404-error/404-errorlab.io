---
persona:
  - Jérôme Orsoni
title: "Ruissellements Β"
slug: "ruissellements-beta"
date: 2021-03-11
echo:
  - rythmique
images:
  - "img/210311-orsoni-ruissellements-beta.jpg"
gridimages:
  - "img/210311-orsoni-ruissellements-beta-grid.jpg"
sourceimage: "https://commons.wikimedia.org/wiki/File:The_Vestibule_of_Hell_and_Souls_Mustering_to_Cross_the_Acheron_Blake.jpg"
notes: ""
summary: "Nulle épiphanie / ici / tombées dans le cosmos zéro / les ombres / (pas plus que leurs corps) / n'ont épaisseur / ce ne sont rien que bouffées qui ânonnent / en chœur la théorie des mêmes greffés / avec ou sans accent / à la chair de l'inconscience / ici / jamais rien ne se révèle / aucun souffle / pas même du bout des lèvres / que baiser de viande molle / à l'incarnat flétri / un défaut de la langue / en nos incarnations vieillies / l'émission d'un message --- / qu'est-ce qu'il a dit déjà ? / --- espoir nul de rémission / ici et puis là-contre / sur le rivage / un sentiment plus vague / brisé c'est-à-dire / à l'image incertaine / de ce bleu dilué par instants"
citation: "invente des dialectes / qui ne se prononcent pas / machine des luttes / fomente des complots / qui échouent d’avance / moi l’inventeur des combats perdus / je dis qu’il n’y a pas de gloire dans la chute / un râle tout au plus"
---

Nulle épiphanie\
ici\
tombées dans le cosmos zéro\
les ombres\
(pas plus que leurs corps)\
n'ont épaisseur\
ce ne sont rien que bouffées qui ânonnent\
en chœur la théorie des mêmes greffés\
avec ou sans accent\
à la chair de l'inconscience\
ici\
jamais rien ne se révèle\
aucun souffle\
pas même du bout des lèvres\
que baiser de viande molle\
à l'incarnat flétri\
un défaut de la langue\
en nos incarnations vieillies\
l'émission d'un message ---\
qu'est-ce qu'il a dit déjà ?\
--- espoir nul de rémission\
ici et puis là-contre\
sur le rivage\
un sentiment plus vague\
brisé c'est-à-dire\
à l'image incertaine\
de ce bleu dilué par instants\
nuance égée\
usage manifeste\
de nos antiques plongées\
liens entre la profondeur et l'attitude\
la rougeur de l'altitude\
même le léger coule\
me dis-je\
s'enfonce et se perpétue\
un maître quelconque gueule\
quelque chose à son chien\
tape sur le front\
de mer ou bien de guerre\
comment vivre la vie\
sans le pouvoir\
rien que la puissance ?\
les questions sont des suspens\
jamais des points\
failles dans la logorrhée qui est\
lui l'homme s'est tu depuis\
pas la mer\
couleur de sel\
d'algues\
et d'ailes lointaines\
quand obèse\
la femme quelconque prend le relais\
et les chiens en photographie\
comme dans une illisible grammaire\
où l'univers viendrait s'écrire\
dans le vide des phrases\
album de famille insensé\
à tour de rôle à présent\
ils posent avec les animaux\
un de chaque côté\
(à droite les garçons\
à gauche les filles\
nuance élée)\
là\
entre mes mains\
pour ce qu'il en est de moi\
je cherche que faire\
de mon sentiment grec\
nimbé dans le brouillard\
de cette indifférence toujours plus épaisse\
et au sens opaque\
les chiens aboient\
la femme marche avec difficulté\
dans le chenil de l'univers souviens-toi de ceci :\
toujours quelque chose te sera enlevé\
une part de monde ou bien d'honneur\
un pan de ciel\
et c'est à toi que tient\
de toi qu'il dépend\
de condamner l'avenir\
ou d'absoudre le destin\
cependant que le vent innocent\
lui\
ne faiblit pas\
regarde\
notre commune condition\
demeures plates de l'ancienne physique\
ici passent les ombres\
une à une\
et le regard se perd de les chercher\
depuis mon poste d'observation\
je suis oreille archipel art du principe\
guetteur d'esprits\
que dit-il le cerveau ?\
demande soudain l'enfant\
liquide nymphe\
à l'endroit où s'originent nos récits\
mythes et blasphèmes\
moins pour couper le courant\
et l'éteindre pour toujours\
que pour en connaître le flux\
et elle dit\
de sa voix haute\
je ne suis pas une dépendance\
pas une province de la violence\
je ne suis pas captive\
je peux parcourir les plaines involontaires de l'acte\
en finir avec le drame\
m'en faire le terme jeune\
et elle voudrait le crier\
plus fort\
mais quelqu'un entend-il encore quelque chose ?\
et quelqu'un comprend-il encore quelque chose ?\
chut (c'est moi qui l'ajoute)\
ne dis pas un mot\
des escarres sur la peau lisse de l'Occident\
cette surface triste de qui tourne le dos\
à l'océan\
que faire de mes prophéties muettes ?\
hygiène du hasard\
parfois je trace des signes\
pour qu'ils prennent forme\
m'enseignent quelque savoir\
qui ne m'appartient pas\
ne provient pas de moi\
invente des dialectes\
qui ne se prononcent pas\
machine des luttes\
fomente des complots\
qui échouent d'avance\
moi l'inventeur des combats perdus\
je dis qu'il n'y a pas de gloire dans la chute\
un râle tout au plus\
peut-être et pourtant\
le reste\
--- c'est-à-dire l'univers ---\
le reste\
s'entête dans l'éloge grégaire\
de notre immonde condition\
nous en informent les sens\
n'est-ce pas ?\
histoire informe de toute atmosphère\
présence\
ou autres volutes inverses\
volumes et parfums en suspens\
formes donc\
et puis couleurs que j'énumère les yeux rivés sur le vague\
bleu jaune\
blanc transparent\
bleu tirant sur le mauve\
jaune tirant sur la couleur de l'écume\
vagues mousses qui lèchent douces les pierres\
digue le sable\
un peu plus orange\
pâle ou un peu moins\
couleur d'agrumes au soleil d'hiver\
lumière chaude même quand il fait froid\
une qualité d'exposition sans pareille\
air léger qui se traverse sans effort\
nulle épaisseur entre les êtres\
semble-t-il\
sur le bourdon juste du paysage\
roulement roulis sourd\
chant plastique gamme\
avec chaque crépuscule\
les mirages disparaissent\
derrière les biens immeubles\
barres de béton qu'on dirait l'horizon\
il n'y a plus de nom\
tout ce qui se dit s'épuise\
ici\
dans le bitume des odeurs confuses\
sur la terre couverte\
recouverte la roche exaspérée\
que d'arts se sont perdus\
je ne suis pas le lieu de la déploration\
le monde est sous verrou\
plaques dalles colmatent nos comas\
semblables à l'infini\
énième adieu à l'antique\
sans même un combat\
tout passe\
et la couleur aussi\
murs gris cassés passés maintes fois ravalés\
derrière le monde continue de couler\
mais tout est aveuglé\
éclaircies reflétées à la surface du verre\
ou bien de la mer\
éclats ophtalmiques\
pas d'autre chance que migraine\
échappée\
les volets clos\
s'enfermer\
et disparaître dans le sommeil\
qu'inspirent les sens\
et puis quoi encore ?\
effluves que science\
à la reprise\
d'aucuns diraient manière de résurrection\
seins en tête\
et à la bouche\
quand la bouche elle\
hèle râle\
autrement qu'à l'envers\
et fabrique les essences\
de nos parfums de personne\
orient de l'ailleurs\
où dit-on les corps ont toute leur part\
ainsi s'inventent nos légendes\
mythes et blasphèmes\
sur les souvenirs qui planent\
aux alentours des choses\
mais sans les toucher jamais\
apprends à expirer\
me dis-je\
injonction à quelque songe indistinct encore\
apprends\
mieux que la muse\
apeurée\
tous sens en suspens\
respiration en alerte\
bronches de l'alternative\
qui halètent\
la mer bleue le ciel bleu tout\
et tes cheveux châtains\
tirant sur le blond\
bien longtemps encore après la fin de l'été\
comme des fleurs desséchées qui embaument\
un massif jaune\
la même antique histoire\
de l'être\
où tout devient\
et reste agréable l'odeur\
au creux du poignet\
du cou\
des cuisses\
figue ou pamplemousse\
citron ou mandarine\
raisin ou bien destin\
pas de point ni de note finale\
raison j'ajoute\
car toutes les fesses le savent\
que ne nous sommes-nous\
attachés\
qu'aux apparences trop sages\
nous qui errons là\
entre espoirs trop minces trop\
déçus certes mais séchés surtout\
et en lesquels plus personne ne veut croire\
surtout\
pas même nous\
qui est l'idole ?\
sauf nous\
qui traversons la route\
saluons le présent\
à quoi bon connaître son nom ?\
quand même nous le saurions\
nous ne serions jamais\
que papillons euphoriques\
éparpillés hélas\
poissons narcissiques\
mécaniques passions\
dis-moi toi comment tu t'appelles\
pour que je puisse moi\
enfin\
exister\
et il y aura encore\
me dis-je\
des moments de latence\
la distance\
l'écart entre les corps\
Icare entre les êtres\
alors que dire ?\
si tu crois encore atteindre au but\
reviens\
à quoi te sert-il de partir ?\
si tu sais qu'ici ou nulle part\
c'est idem\
quelque lieu partout le même\
uniforme je regarde mes pieds\
quand était-ce\
la fin de l'été ?\
j'estime\
espace\
trace les cimes du bout des doigts\
du bout des lèvres\
fais un plus un égale trois\
sans rien ajouter\
surtout pas d'étant\
rien n'est donné\
tout se donne\
on saute dedans à cloche-pied\
parvis des mondes désertés\
et dedans ruine\
pourtant\
commentent-ils\
pourtant les cloches\
on les fait sonner\
qui a jamais prétendu qu'il était aisé\
de comprendre ?\
je regarde l'époque\
qui fait cloque\
elle devrait éclater mais enfle\
gonflent les proportions indignes de son nom\
tu sais\
lui avais-je avoué un soir\
tu sais\
moi non plus je ne crois plus en rien\
alors à quoi bon continuer ?\
m'avait-elle demandé\
d'un air rassuré\
précisément pour cette raison\
que nul ne le sait\
encore une saison à chercher\
quelque chose qui dit\
que ce n'est pas fini\
après quoi je m'en irai\
et puis donc\
science qu'effluves\
où le monde s'évapore\
célestes fumées\
qui aveuglent nos yeux cillés\
bleu caché dans le gris\
feu qui allume nos vies\
je récite ma cosmogonie insane\
et demande pardon\
à tous\
aux fanatiques aux illuminés\
aux terroristes aux assassins\
aux menteurs et aux saints\
pardon pour les désastres planifiés\
les éruptions fictives\
de nos âmes factices\
abattues par leurs balles réelles\
pardon pour les âmes perdues\
les causes perdues\
nos corps perdus\
à leurs trahisons sacrifiés\
pardon pour les cœurs en jachère\
notre mère la misère\
et les hymnes des fous\
déclamés sous leurs électrochocs\
pardon pour les cages\
où nous demeurons entassés\
suçant des amours blafardes\
à l'ombre de leur tragédie\
pardon pour les clameurs\
cantiques des otages\
fabrique du secret\
réduit où nous sommes cloîtrés\
pardon sans pardon\
que déraison\
n'est-ce pas elle\
mémorable\
l'histoire de tout repentir ?\
faire le tour de toutes choses\
et s'admirer vieilli\
sur le pas d'une porte\
Pénélope file\
à la vitesse de la lumière\
à la vitesse de la pensée\
l'étoffe cinglante\
d'une si sanglante défaite\
qu'on dirait jour de fête\
les héros ne sont pas morts\
ce n'est pas vrai\
ils ont vendu leur épopée\
bradé leurs divinités\
c'est eux qu'on voit\
sur la scène aveuglante\
où ils se donnent à admirer\
et comme tout se consume\
dans une infinie bêtise\
traîtrise sans complexe\
quand la langue pendouille\
elle est prête à lécher\
sexes mortifères\
que restera-t-il de nous\
nous qui fûmes choses perplexes\
quoi sinon vestiges cruels\
exhibant nos ruines\
dans le foyer des vivants ?\
n'est-il pas pour nous\
ce saint sang\
que nous lapons\
telles bêtes sans nom ?\
or que demeure\
sinon l'absence ?\
et nous\
plongeurs antiques\
en nos mythiques piscines\
ne cherchons pas la source\
tarie\
en réponse tardive\
je dis à l'enfant :\
de cette Méditerranée\
ne retiens pas l'accent\
mais la lumière\
et à part moi j'ajoute :\
est-ce que je crois\
en la voix pure\
ou est-ce que je mens ?\
au bout de mes doigts\
à de fragiles oiseaux\
au soleil posés\
sur les rochers\
j'adresse ma prière :\
ô mes frères inhumains\
ce monde où nous sommes\
tombés\
serais-je prêt à lui dire\
oui\
en tout\
ou en parts légères\
et comme découpées\
par vos paroles ailées ?\
la maîtrise est une traîtrise\
ô mes frères plumés\
tout ceci qu'en faire ?\
sinon l'appeler\
*chants*.
