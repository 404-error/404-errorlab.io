---
persona:
  - Bernard Bourrit
title: "Apories vénitiennes (deux)"
slug: "apories-venitiennes-deux"
date: 2024-03-13
echo:
  - esthétique
images:
  - "img/240301-bourrit-apories-venitiennes-deux.jpg"
gridimages:
  - "img/240301-bourrit-apories-venitiennes-deux-grid.jpg"
sourceimage: "Bernard Bourrit"
notes: ""
summary: "Dans la cabine de l'Airbus me conduisant à l'aéroport San Marco de Venise, j'ai lu un auteur qui se targuait de distinguer entre curiosité féconde et curiosité oisive. Le ton péremptoire de sa démonstration avait piqué mes nerfs à vif. Chevalier servant de la vérité, il louait cette soif qui, selon lui, ne se pouvait étancher qu'à grandes rasades de savoir, tout en méprisant ouvertement la flânerie contemplative et la promenade étonnée. Pourquoi opposer l'une à l'autre ? et pourquoi hiérarchiser les termes de cette opposition ?"
citation: "Plonger dans le labyrinthe de sa vie intérieure, se baigner dans le murmure du monde"
poetry: false
hangindent: false
---

## 1

Dans la cabine de l'Airbus me conduisant à l'aéroport San Marco de
Venise, j'ai lu un auteur qui se targuait de distinguer entre
curiosité féconde et curiosité oisive. Le ton péremptoire de sa
démonstration avait piqué mes nerfs à vif. Chevalier servant de la
vérité, il louait cette soif qui, selon lui, ne se pouvait étancher
qu'à grandes rasades de savoir, tout en méprisant ouvertement la
flânerie contemplative et la promenade étonnée. Pourquoi opposer l'une
à l'autre ? et pourquoi hiérarchiser les termes de cette opposition ?
À l'arrière-plan de cette polarisation, j'avais senti tristement
poindre la division entre temps productif et improductif, négoce et
*otium*. Comment ne pas voir, en effet, que sous ses dehors reluisants
d'entreprise de salubrité intellectuelle, la défense de l'universel,
la défense de l'existence de critères universels de la raison, de
l'homme, de la vérité, etc., s'alignait, par un hasard trop
extraordinaire pour n'être qu'une coïncidence, sur les intérêts de
l'ordre productiviste mondial ? En réalité, m'étais-je dit, les
promoteurs d'une définition exigeante de la vérité font le lit de tous
ces économistes (avec lesquels ils ont des accointances) qui fustigent
l'argent de l'épargne, parce qu'ils voudraient le voir devenir, y
compris par le truchement de mesures contraignantes, de l'argent
investi.

Ici et là, même dialectique.

## 2

Par un trait acéré de style, mon auteur réservait la suprême marque de
son dédain à un pauvre imbécile qui n'avait rien demandé à personne.
Le sot, en l'espèce, était un caractère bien connu de La Bruyère :
l'amateur de tulipes. Pourquoi lui ? Parce que la raison suffisante du
bonheur de cet "amateur" était une journée entière consacrée à
l'admiration de ses fleurs. Observation inepte. Temps perdu. Vaine
curiosité.

Là où mon auteur frisait la mauvaise foi, c'est que La Bruyère
lui-même n'a jamais cru ridicule qu'on pût admirer Dieu ou la nature
dans une tulipe. Il ironisait seulement à propos du vice de ce
connaisseur qui ne se satisfaisait pas d'apprécier la délicatesse de
la corolle et du calice, mais qui, brûlant de réunir dans son jardin
de faubourg toutes les variétés en vogue, collectionnait les bulbes
avec passion. L'ineptie n'est donc pas de s'agenouiller devant un
ouvrage de la nature, mais de priser la rareté et, voilà le vice, de
fonder la valeur de la chose sur cet indice qui est esclave des cycles
absurdes de la mode.

## 3

J'allais à Venise pour flâner. J'aurais été heureux d'y voir des
tulipes. Et mon sang bouillait.

L'idée même de vie contemplative pose une cascade de problèmes. Chacun
sait qu'il y a de multiples façons d'intellectualiser les
contradictions ou les concessions qu'impose un mode de vie bâti, bon
gré mal gré, sur le repli. Mais à la fin, demeure centrale la question
du retrait de la vie, de la vie retirée, qui est comme la pierre
d'angle de cette appétence à se laisser imprégner d'un détail, d'un
lieu, d'une idée. Surtout ne pas abaisser d'office, par un parti pris
obscur, la nature soustractive de la contemplation.

## 4

La contemplation a son histoire qui court des anachorètes et autres
saints stylites de la banlieue de Palmyre jusqu'aux intoxiqués
volontaires dont les visions droguées irriguaient la pensée
d'avant-garde des années 1970, en passant par les solitaires figures
orientales du non-savoir. Seulement il arrive également que le
retranchement complet ne soit pas à proprement parler un choix, mais
un expédient. Une ruse pour s'affranchir de l'oppressante montée de la
modernité triomphante. En ce sens, l'artiste est quelqu'un qui
pourrait aussi bien ne rien faire, se satisfaire de l'immersion dans
le monde, et d'une vague rêverie associée (Houellebecq).

Du sein même de ces vies retranchées, il faudrait dégager, s'il se
peut, le profil positif de l'artiste qui crée à la marge, forgeant sa
légende à l'école de l'adversité ; du profil inverse, celui de
l'impuissant qui se marginalise sans parvenir à créer, absorbant la
vie entière dans son isolement. Baudelaire et Amiel représenteraient
schématiquement les deux extrémités de cet échiquier ; à cette réserve
près, qu'Amiel, de son vivant, fut un professeur suivi et que son
œuvre ne tomba pas tout à fait dans l'oubli. Il n'empêche,
l'antagonisme entre temps de la pensée et temps de la marchandise ne
connaît pas de résolution heureuse. En revanche, il sera pardonné
beaucoup à celui qui, par la force de son talent et de son orgueil,
transmuera la boue en or : celui-là, sauvé par ses œuvres, connaîtra
la gloire de l'esprit. L'autre, passivement reclus dans sa chambre,
dans son atelier ou son jardin, ne pèsera guère sur le monde, ce qui
serait un moindre mal, s'il n'était en plus constamment blâmé pour ne
rien donner à voir de son occupation.

## 5

Voilà donc le paradoxe : la distinction de l'actif et du passif se
retrouve à l'intérieur des vies les plus ostensiblement tournées vers
la contemplation parce que cette distinction recouvre, plus
profondément encore, une opposition de valeurs qui, elle, traverse
l'ensemble du corps social. Plonger dans le labyrinthe de sa vie
intérieure, se baigner dans le murmure du monde, sera toujours décrit
comme une paresse d'action, autrement dit une faute morale, y compris
parfois par ceux qui s'y adonnent. L'euphorie de tous ces moments
perdus, la brève libération de soi que procure cet abandon, paraît une
matière de vil prix à ses juges, toujours prompts à dénoncer la
mauvaise influence de cette redoutable faculté de séduction,
stupéfiante comme le chant des sirènes, sombre comme une joie
coupable, inutile comme les pierres. C'est pourquoi, le plus souvent,
la disposition innée à la contemplation est assimilée à quelque
faiblesse de caractère ou à quelque disposition intérieure
défaillante.

## 6

La caractéristique essentielle de ces visions passives semble être
l'impossibilité absolue d'en donner la moindre expérience
communicable. Cela est naturellement vrai de l'ascension mystique, des
songes, des hallucinations provoquées, mais aussi, ainsi que je le
crois, de la flânerie curieuse, celle-là même qui pousse le regard du
rêveur à se faufiler à la manière d'une musaraigne dans la masse des
feuillages, à bondir ou à se suspendre, le temps d'un vertige, au
point culminant d'une fibre qui forme alors sous son poids minime un
arceau aux proportions exactes, et à éprouver le rythme flottant de
cette suspension.

Voilà une loi curieuse qui se vérifie pourtant : les états les plus
intenses, l'observation du présent, l'émotion de l'amour, la
concentration de la pensée, l'ivresse, la rêverie, ne laissent, une
fois évanouis, que des regrets, non des souvenirs, des regrets
justement parce qu'ils ne laissent pas de souvenirs. Ni rien de
communicable. L'oubli de soi, l'abandon, la joie sont des moments de
grâce, d'une pureté presque céleste, peut-être même des extases, qui
s'éteignent dans l'indifférence, comme meurent les étoiles.

La première charge contre la contemplation passive émane logiquement
de la sphère libérale, ce domaine de la pensée conquérante où l'on
défend en vrac : la raison critique triomphant de l'ignorance et de la
fausseté, le temps faisant fructifier l'étude, l'argent produisant,
etc. C'est une opposition cardinale de l'actif au passif réitérée
jusque dans l'activité de la passivité même. La contemplation y est
fustigée de toutes parts comme perte, néant, destruction de la
richesse et de l'élan vital, immorale oisiveté, incurable fainéantise.
Culpabilité de ce qui ne se fait pas, de ce qui prend la place de...
(Kristof)

## 7

Mais une seconde critique déborde rapidement la première, qui provient
cette fois de la gauche antilibérale, et non plus de la droite
réactionnaire (qui parfois, va comprendre, sont les mêmes ayant changé
leur fusil d'épaule), gauche antilibérale aux yeux de laquelle le
contentement que procure la vie retirée entretient trop de similitudes
avec le confort du salon bourgeois. Retiré dans son cocon, le
contemplatif semble dire à l'humanité entière : il n'y a pas de place
pour toi (Benjamin). Et s'il entend rester maître de ses mystères, ce
n'est pas tant qu'il aime vivre caché, ou se satisfaire de peu, ainsi
qu'il le prétend, mais plutôt qu'il se désintéresse du sort des
autres, qu'il détourne les yeux de la misère, qu'il défend son odieux
privilège de rêveur dans l'abandon du plus élémentaire devoir de
solidarité. Ce n'est même pas de la paresse ; c'est de la lâcheté.

## 8

De la lâcheté, pensai-je en posant ma valise à quai, absolument ravi
que le *motoscafo* survolant si vite la lagune m'eût permis (pour un
temps) d'échapper au piétinement général. Je me présentai à la
réception avant de rejoindre ma chambre que je trouvai conforme aux
images que j'en avais vues : simple, dépouillée.

Attaquée de tous bords, cette aptitude à la contemplation passive
paraît indéfendable. Elle s'apparente par trop d'aspects à une faute
morale. En effet, n'est-on pas coupable de rester improductif quand on
pourrait créer de la valeur ? coupable de rester inactif quand on
pourrait contribuer au bien commun ? Impossible de soustraire la
contemplation de la double négativité qui la cerne. Mais ce refrain,
ne l'as-tu pas déjà entendu mille fois ? pensai-je. C'est une vieille
rengaine. Car ce sont les mêmes qui blâment le manque de puissance
chez les contemplatifs, et chez les dépressifs le manque de volonté.
Avec ce ton condescendant, le même. Le ton de celui qui ne doute pas
de sa valeur : mépris de l'homme politique vis-à-vis de ses électeurs,
arrogance du spécialiste vis-à-vis des non-spécialistes, stupidité
satisfaite du médecin.

## 9

Et si cet homme que le sentiment de sa valeur protège veut bien
concéder au fond de ces visions une larme de joie, cette joie concédée
elle-même fait l'objet de critiques acerbes : il la dira insidieuse
comme la stupeur de l'opium, ou incompréhensible comme l'hébétude de
l'illuminé, ou encore chimérique comme la douceur d'un songe éveillé.
Soyons lucides néanmoins, pensai-je. Ce n'est pas la fausseté de ces
visions, leur caractère privé, non transmissible, qui excite la
virulence en général. Il y a beau temps qu'il est perdu, ce combat
contre l'illusion. Non. Ce qu'on fustige, c'est la durée, le temps
comptable, le chapelet des heures occupées par l'expérience
contemplative. C'est le gaspillage. Le lamentable gâchis de secondes
précieuses. Comment ne pas croire que ce temps consacré à la rêverie
ne serait pas mieux employé aux affaires humaines ? à la chose
publique ? Ah, si seulement on pouvait convertir ce temps vacant en
temps social ! Convertir ce rêve si petitement égoïste qui prend la
place de tant d'autres rêves, de rêves nobles, de rêves vrais, de
rêves de solidarité générale et de profit pour tous.

À la rigueur, on tolérerait la joie, à condition bien sûr que ce fût
une joie pauvre, un faux-semblant de joie, un pis-aller sénile, mais
on la tolérerait. On irait jusque-là. Tout, pourvu que la vie rêvée ne
nuise pas à la vie souhaitable.

## 10

Je pensais encore : les termes employés pour rendre compte de la
nature de cette expérience sont porteurs d'ambiguïté. On se figure un
être infantile emmailloté dans son cocon et refusant de sortir (de
grandir) parce qu'il préfère admirer les tableaux de sa pensée dans la
solitude plutôt que de se confronter au monde. On parle de
"contemplation" ou de "vision", alors que l'expérience n'a
absolument rien d'un cinéma mental. En réalité, on se méprend sur la
nature de la vie contemplative parce qu'on la juge dans les catégories
de l'action. J'irais jusqu'à dire que la plupart des contemplatifs, au
premier rang desquels Amiel, souffrent de leur disposition solitaire
parce qu'eux-mêmes la regardent sous le jour du manque, qu'eux-mêmes
l'éprouvent comme une déclinaison, sur tous les registres, de
l'impuissance. Mais qu'a-t-elle en propre cette expérience ? À quoi
tient sa singularité ?

## 11

Prenons l'exemple d'Anton Reiser. Dans un passage important du livre
éponyme de Moritz (d'où proviennent les citations qui suivent), au
cours d'une rêverie semi-dirigée, le personnage, un décalque de
l'auteur, fait cette découverte : il lui apparaît en un éclair que sa
vie lucide tient tout entière dans le volume d'un atome, d'un noyau de
conscience, qu'il baptise aussitôt son "moi". Prélude à la modernité
romantique, la découverte de cette singularité intime ne fait écho ni
à la tradition de la substance pensante ("il y a en moi un je qui
pense") ni à celle du sujet biographiquement déterminé ("je suis,
moi qui parle, la résultante d'une conjonction de circonstances
particulières), mais elle indique une inflexion dans l'appréhension de
la sensibilité, une noirceur qui s'apparente à une variante
désenchantée du solipsisme. Pour Anton Reiser, le sujet parlant ("je")
est enfermé dans la chambre vide de sa pensée ("moi") sans
communication possible avec les autres, qu'ils existent indépendamment
ou non.

## 12

Ce n'est pas tant le corps, que le "moi" qui est un "obstacle",
une "limite" qui enclot l'individu. Pour exprimer ce pénible
sentiment d'enfermement, Anton Reiser emploie une image, qui reviendra
sous la plume de Proust, mais cette fois avec une connotation
positive, celle d'un observateur extérieur qui du dehors, depuis la
rue, contemple avec envie ("l'envie" étant la composante commune
entre ce récit et la description du restaurant-aquarium de la
*Recherche*) contemple un intérieur chaleureux, "une enfilade de
salons illuminés" dans lesquels le héros imagine "un certain nombre
de familles dont il ignorait la vie et le sort autant qu'elles-mêmes
ignoraient les siens". Tous les ingrédients du drame sont réunis : le
personnage est seul, exclu, ignoré, tapi dans l'ombre, invisible dans
la nuit. Il est réduit à sa posture d'observateur. Et ce qu'il voit
exister littéralement sans lui le fascine, ne peut que le fasciner.
Papillon nocturne butant contre le faux jour d'une plaque en verre.

## 13

Nous la tenons, pensai-je alors, abstraction faite de toutes ses
déterminations négatives, notre description de la contemplation
passive. Et comme on pouvait s'y attendre, ce n'est pas une incurable
mollesse de la personnalité que nous voyons apparaître ni un coupable
abandon, mais l'incarnation d'un sentiment d'un rejet universel sans
motifs de persécution. Quelle qu'en soit la cause, historique ou
privée, série de hasards malheureux, le rejet n'est pas seulement une
exclusion de la communauté (amicale, familiale, collégiale) à laquelle
l'individu appartient par ses liens sociaux, pas seulement une rupture
unilatérale, mais une excommunication totale, métaphysique, hors de la
société et du monde. Expulsé de la douce familiarité des contacts
humains, l'individu est voué au désert. Condamné à l'errance et à la
solitude la plus absolue. Et s'il est métaphysique, ce bannissement,
c'est qu'il est intérieur. Il s'apparente à l'exil du prisonnier jeté
dans l'obscurité de son cachot. Il s'apparente à l'exil de Sigismond
transformé en bête dans sa tour. À l'exil de celui qui, replié dans
l'intériorité de son "moi" devenu à la fois le lieu et le signe de
son exclusion, éprouve alors une curieuse sorte de nostalgie
conditionnelle, qui serait comme le regret de tout ce qui aurait pu
être vécu.

## 14

Le sentiment de "ne pouvoir échapper à son moi", comme dit Reiser,
est si indissociablement lié à l'épreuve du rejet et de l'expulsion
que le "moi" semble jouer le rôle du terrier pour un animal traqué.
S'esquisse alors une figure paradoxale : celle du contemplatif dont la
faculté immersive ne procède pas d'une prédisposition à la rêverie,
figure dans laquelle le plus intime se trouve être aussi le plus
exilé, le rejeté au plus loin.

Comment se débrouiller avec ça ? pensai-je. Les grandes lignes sont
esquissées : il y a d'un côté un exil intérieur conduisant à prendre
refuge dans le dernier bastion de son être, appelé le "moi", de
l'autre une fascination pour le "dehors" compensant l'impossibilité
de quitter son moi. La pente contemplative naît alors à la croisée
défavorable d'un mouvement double d'introversion et d'extraversion.

Néanmoins, doit-on penser que l'emprisonnement à l'intérieur de soi
est la conséquence de l'exclusion (d'être enfermé dehors) ou la même
réalité appréhendée sous deux angles différents ?

Voilà qui n'est pas clair.

## 15

En revanche il est certain que la fascination naît de l'impossibilité
de rejoindre le mode d'existence perdu. Le besoin de voir, de toucher
par la vue, de se mêler à l'objet de sa vision correspond à la
tentative désespérée de traverser le miroir qui sépare le moi du monde :
passer de l'autre côté du cadre, entrer dans l'image. Tenter de
"pénétrer peu à peu par la pensée dans l'être intime" des choses.
Pourtant, ce n'est pas un mirage qui s'offre à la vue, mais bien le
monde, cela qui nous environne et nous contient --- comment l'appeler ?
le monde, donc, qui paraît distant, vu comme un autre régime de
réalité, une "vie secrète" dont l'accès est refusé.

La fascination est le refus d'entériner cette séparation. C'est le cri
du corps sacrifié. Coupé du monde, coupé des autres. Et qui le refuse.
Dans la pointe du regard se contracte toute la puissance du corps
refoulé qui lutte encore pour exister. C'est le regard du condamné qui
vit désormais "en animal" dans ce que Reiser appelle une "paralysie
de la conscience". Où cette paralysie exprime une tension impossible
à soulager entre ne pouvoir être soi et ne pouvoir être hors de soi.

## 16

Je fus long à me décider à sortir.

Il m'avait fallu du courage pour arriver jusqu'ici et maintenant que
j'y étais, c'était comme si j'hésitais à faire le dernier pas. J'avais
envoyé de nombreux messages Hélène depuis cette pièce vide et sans
fioriture. Mais ses réponses ne venaient pas.

Ma première sortie fut pour les *Fondamente nove*. Arrivé là, je n'eus
pas le cœur de patienter dans la foule, prendre mon ticket, attendre
le vaporetto. Je restai à quai. Je scrutais l'île-cimetière.
L'enceinte de briques rouges, les cyprès, l'eau. Ça ne m'évoquait
rien. C'était juste là. Un peu fade.

Je rebroussai chemin.

## 17

À mon découragement je dus la surprise de cette rencontre.

Dans ma vie, souvent j'avais trouvé ce que je ne cherchais pas en
renonçant à chercher. Renoncer, se décourager : ces verbes traduisent
par quelque manière la sensation rampante d'être faible, fatigué,
honteux, c'est-à-dire l'impression lâche d'être mis devant l'évidence
de sa déroute. Ici, impossible de ne pas penser à Henri de Reigner :
"Il y a vraiment des soirs où l'on est au bout de sa vie et où l'on
pense à ces étranges *calli* de Venise, qui ont l'air d'être des
impasses et qui se continuent par quelque stratagème d'architecture ou
de voirie."

Car à un certain degré de hasard, le caprice prend en effet le sens
d'un calcul fatal qui, perçant d'un coup d'épingle une issue
minuscule, fait passer le mince filet d'air qu'il faut pour n'étouffer
pas. Oui, l'effet de coïncidence par lequel se manifestaient de telles
circonstances a toujours conféré à ces évènements une valeur hautement
intense, faisant d'eux quelque chose de plus, comme un salut, une
absurde prémonition, mieux : un appel zébrant le chaos.

Cette fois encore, il en fut ainsi.

## 18

Donc, j'avais mis mes pas dans la direction de la *Punta della Dogana*
quand après avoir traversé le Grand Canal, je tournai pour entrer à
*l'Accademia*. J'aimais traverser les musées au pas de charge,
n'accordant qu'une attention flottante aux chefs-d'œuvre, sans égard
pour leur virtuosité technique. Je ne les interrogeais pas. Ne les
regardais pour ainsi dire pas. Parfois l'un d'eux, épisodiquement, par
un détail, m'apostrophait. Si j'y revenais, c'était que quelque chose
insistait. Ce n'était pas dans le tableau, ce n'était pas dans mon
regard. Comme une clé qui ouvrirait peut-être une porte, peut-être une
porte familière.

En reconstituant la façon dont ça s'est passé, parce que c'est ce qui
m'intéresse, le cheminement de l'idée, la zébrure qui vient trouer
l'attention, soudain qui comble un creux, comment elle fait saillie,
cette zébrure, puis s'efface, puis alors qu'ensevelie, comment elle
revient et insiste, et pourquoi ce sont des images plutôt que des
phrases, pourquoi ce point d'accroche muet, qu'il faut aggraver de
phrases, recreuser, décaver, comme une réserve de sens pressentie
qu'il faut dégager tant bien que mal, lent travail de dépliage, comme
si au fond, très loin, il s'agissait d'accoucher un vouloir-dire, lui
permettre de remonter à la surface, mais aussi, une fois, une seule
fois pour l'exemple, en retracer l'origine, et trouver la source de
cette zébrure, et mesurer aussi la quantité de néant franchie à coups
de ruses et d'écarts, pourquoi ça accroche, d'où ça part... Baisser la
caméra (Van der Keuken). Faire entendre le souffle qui porte les
phrases. En reconstituant ce qui s'est passé, je voulais révéler ça,
je crois, le processus.

## 19

Au départ, aller à Venise, c'était faire l'expérience d'un lieu à
travers le regard d'un autre. Puis ayant renoncé, autre chose était
advenu. Autre chose qui n'était encore rien. Un point d'insistance,
tout au plus, semblable aux rémanences de rêve dont la trace perdure
comme une ombre après l'effacement du récit où il était inscrit, de
l'enchaînement où il avait sa place et son sens. Juste une image
détachée. Pour moi, cette image fut un lézard.

Une image de lézard.

Alors bien sûr, il est facile aujourd'hui de croire, de vouloir croire
au caractère prémonitoire de ce symbole qui avait accroché mon regard
dans les galeries de l'Académie traversées à un rythme infernal, tout
à fait indigne de la grandeur des talents pendus aux cimaises.
Toujours est-il que ce lézard vert, arqué par la tension musculaire de
son attention vigile, avait intrigué mon œil. Il était incongru. Pas à
sa place. Sans lien avec la scène d'intérieur figurée sur la toile.
Mais obstinément là, en bas à droite, dans son coin.

## 20

Au départ, il y a donc un lézard juché --- mais comment ? sur le
plateau d'une table ou d'une planche reposant sur deux tréteaux sur
laquelle on aurait simplement jeté une nappe. Et puis cette impression
difficile à traduire : sur la belle nappe verte aux tons froids, une
étoffe (une étole ?) bleue, moirée, aux dessins capricieux,
négligemment déroulée en cascade. Les tourbillons de l'étoffe, les
fronces dessinent nettement des vagues ou de très épurés nuages. Il se
trouve au surplus que ce tissu déborde et glisse, coule de la table
comme la matière qu'il n'est pas, ruisseau, mare, miroir métallisé. Et
au centre d'un vortex de plis, ce lézard. Un lézard vert, vert du même
vert que la nappe, au milieu d'une agitation de plis.

Mais voici ce que je me demande : le petit animal est-il pris dans le
froissement d'étoffe ou bien est-ce lui, par ses gestes saccadés, ses
départs et ses arrêts, qui crée ces remous et les propage en
frétillant. Symboliquement, il figurerait alors cette lézarde qui
ouvre, craquelle le vernis de l'image, l'insignifiant point de départ
qui engendre et répand le chaos à l'échelle globale, le battement
d'ailes du papillon, le mouvement de pattes du lézard, indiquant
allusivement que l'image est habitée par un trouble, imperceptible au
départ, oui : quelque chose remue dans le fond de l'image. Et si ce
lézard n'est pas le pendant figuratif et sonore de la zébrure évoquée
plus haut, comme je le pense, il faudra bien que quelqu'un rende
compte malgré tout de cette bizarrerie du tableau. Car ce saurien est
aussi saugrenu qu'une paire de bottes sur des draps de lit. L'animal
lui-même n'est pas sale, pourtant sa présence, fortement indésirable,
perturbe l'harmonie de la composition, son étoffe soyeuse.

## 21

L'irruption d'une aberration dans un système de représentation
rationnel définit la souillure. C'est une anomalie engendrée par le
système qui vient maintenant menacer son équilibre. Une hantise,
aussi. Quelque chose qui revient et vous juge. En ce sens, il n'y a
pas de surprise à ce que le lézard regarde le jeune homme au centre du
tableau, dont c'est le tableau, puisque c'est un portrait. Le regard
qui va de l'animal au jeune homme fend l'espace comme une flèche,
tandis que notre œil irrésistiblement fasciné se visse sur l'animal,
interceptant au passage ce trait invisible, puis remonte jusqu'au
jeune homme qui nous dévisage tristement, tout ceci dans une étrange
circularité : spectateur, lézard, jeune homme, spectateur. À ne plus
savoir qui regarde qui.

Qu'est-ce au juste que ce lézard qui trouble mon regard ? Par lui,
qu'est-ce qui m'interpelle ?

Voilà le genre de questions que je traînai jusqu'à ma chambre. Pas à
proprement parler des interrogations. Non, je ne me les posai pas, ces
questions dont la maladresse montre assez qu'elles ne sont que de
reconstitutions d'après-coup. C'était plutôt une songerie, un
embarcadère d'où l'esprit pouvait partir à la dérive. Ou encore, la
chambre d'un labyrinthe où mes pas me ramenaient sans cesse. Ce qui
est certain, c'est que je ne l'oubliai pas, comme il arrive de la
plupart du reste. J'y pensais. Régulièrement, j'y revenais pour
penser.

## 22

Résoudre l'énigme de cette figuration du point de vue de sa véracité
historique ne m'intéressait absolument pas. Mener l'enquête non plus.
Ma question était simple : qu'est-ce que "ça" dit ? Mais pour être
sincère, ce n'était pas même une question. J'essayais en effet
seulement, en me ménageant un vide intérieur, de me mettre à l'écoute
de ce vouloir-dire manifesté selon une voie oblique, fortuite, quoique
nécessaire peut-être. Pour se donner une chance de l'entendre,
pensai-je, il faudrait impérativement commencer par mettre en sourdine
l'essaim d'idées et de paroles intérieures qui sous leur rumeur
permanente étouffe le cri des choses ignorées, il faudrait suspendre,
faire taire cette incessante vocalisation de la pensée, parvenir à
tromper sa vigilance, baisser la garde, enfin.

Avec le temps, bien sûr, la pensée du lézard, s'éloigna. S'effaça
presque tout à fait, puis disparut. Sur le coup il y avait bien eu
quelque chose à la réflexion, un étonnement sans doute, un espoir
peut-être, mais à présent cela paraissait lointain, de plus en plus,
et surtout illusoire. Je crois bien que je réussis à l'oublier
complètement. À faire en sorte de retrouver le calme. Le paisible
sentiment d'inutilité qui permet de franchir les jours, le soulagement
qu'apporte la capitulation, le goût du néant. Et cela fonctionna.

Assez bien même, je crois. Jusqu'au jour où, par désœuvrement,
c'est-à-dire par un nouveau détour de l'idée, j'entrepris de retrouver
mon tableau pour en apprendre le titre. Quand je sus que le
chef-d'œuvre de Lorenzo Lotto se faisait appeler *Le Jeune malade*, ce
fut une confirmation. Là se tenait la preuve que ce lézard, comme un
coin fiché dans la toile, appelait la cognée du regard et, sitôt le
premier coup d'œil jeté, que la masse de peinture allait se fendre,
ses apparences éclater, le portrait se disjoindre.

## 23

Donc, Lotto.

Lorenzo naquit en 1480.

Lui qui huma l'aigre senteur des marais de la Lagune reporta
scrupuleusement dans l'encadrement de ses fenêtres ces lignes
tremblées bleues et vertes.

C'était un artiste itinérant, irascible, qui se tint en marge de la
peinture *mainstream* de son temps. Venise, où il vit le jour, était
alors dominée par Titien.

Les "bizarreries" qu'il avait l'habitude de glisser dans ses
tableaux paraissent correspondre à un symbolisme ésotérique dont la
clé se trouve à jamais perdue, symbolisme pour lequel la méthode
iconologique est d'un secours nul, puisque rien de comparable n'existe
ailleurs. Il faut hélas convenir que ces curiosités s'offrent
désormais à la valse des interprétations comme des métaphores en
absence. À la question : de quoi, ce lézard est-il le symbole ? chacun
est libre de répondre : froideur affective, mélancolie, immobilisme,
contemplation, mort, renaissance, etc. À ce petit jeu stérile, je
préfère encore l'anachronisme bien dosé : Gauguin peignant un lézard
dans la serre d'un étrange oiseau blanc, écrivant qu'il représente
"l'inutilité des vaines paroles". Ce dont on ne peut parler, il faut
le taire, en effet.

## 24

On pourra toujours affirmer de ce lézard tout ce qu'on veut, au prix de
la ruse suggérer mille analogies sans risque, sans crainte d'en épuiser
la signification, mais surtout, comme dans toute cabale, sans jamais
trouver la pierre de touche qui mettrait à l'essai la substitution des
signifiants. Pour moi, c'était assez d'entreprendre l'élucidation de ce
qu'il m'avait fait pour ne pas en plus devoir dire ce qu'il était.

Hélène, à qui j'avais demandé de me scanner l'article de Daniel
Arasse, disait, elle, que je m'emballais. Une fois l'excitation
retombée, je verrais tout ça : Venise, le cimetière, l'Académie, je le
verrais comme un égarement. Que je reviendrais alors manger auprès
d'elle mon lotus d'oubli.

## 25

J'avais bien essayé d'écrire à l'un des membres du comité scientifique
de la galerie pour tenter d'obtenir des éclaircissements quant à la
signification du titre qui variait selon les bibliographies, le
tableau étant aussi répertorié sous le nom de *Ritratto di giovane
gentiluomo nel suo studio* ou encore simplement *Ritratto di
gentiluomo*. De réponse, je n'en obtins pas. Considérant alors que les
variantes étaient platement descriptives, et en l'absence de tout
autre indice, je pris le parti du titre problématique, le seul à
suggérer l'épaisseur psychologique de la figure sombre qui pose devant
nous. Le seul à exiger du regard qu'il dépasse l'apparente équanimité
du modèle, son extérieur de jeune homme bien portant, modéré, retenu,
éduqué, pour concevoir le récit manquant de sa maladie.

## 26

Au mépris de la tradition iconographique, puisque rien dans le décor
ni dans l'attitude n'autorisait un tel rapprochement, je pris
l'habitude de le voir, ce jeune homme, comme un saint Sébastien. Je
savais bien qu'il n'y avait ni colonne ni flèches ; que le jeune homme
n'avait pas les mains liées, enfin que ce n'était pas une scène
religieuse. L'analogie était injustifiable. Malgré tout, l'opération
mentale consistant à appliquer ce filtre subjectif, là où n'importe
quel historien de l'art s'évertuerait avec rigueur à situer ce tableau
dans sa strate temporelle, activa de puissantes résurgences
imaginaires.

Il me sembla au passage que le fait même que cette tentation existe,
tentation de se livrer à une lecture sauvage ou une interprétation
contrefactuelle, signalait à mon jugement une cassure des cadres
sociaux de la mémoire, affligeante coupure avec ces époques qui ont
cessé de nous parler autrement qu'en langue morte, ou qui ne nous font
plus signe qu'au prix d'une érudition accablante, byzantine, sans
vitalité pour notre temps.

Quant au tableau de Lotto, je savais que les meilleurs spécialistes
s'accordaient sur deux hypothèses : ou bien le personnage souffrait
d'une rupture amoureuse (d'où le symbole des pétales de rose
éparpillés et la liasse de lettres éventée), ou bien il était sujet à
la mélancolie (car, à ce qu'en disait André du Laurens, le médecin
d'Henri IV, les mélancoliques étaient traités par l'action de pétales
de fleurs). En outre, tout le monde s'entendait sur l'anonymat du
modèle représenté.

## 27

Pour ma part, j'étais enclin à le voir comme un martyr. Son mal,
invisible, était sans symptômes. Aussi l'imaginer criblé de flèches
m'aidait à soutenir cette croyance qu'il était malade, en dépit des
apparences. Malade de quoi, alors ? Ce n'était pas en dotant de
valeurs arbitraires les symboles disséminés dans la toile, j'en étais
convaincu, qu'on parviendrait à le deviner. On peut bien dire que
comme le lézard est un animal qui fuit l'ombre et qui cherche le
soleil, il figure le passage douloureux à la vie adulte, le
renoncement aux plaisirs de la jeunesse (amour et chasse, eux-mêmes
symbolisés par les pétales et le cor accrochés à l'arrière-plan). En
vérité, on ne saisira rien par cette méthode qui s'apparente plutôt à
une clé des songes. On touche vite aux limites du symbolisme.

Le seul jugement qu'on puisse porter sans trahir ni l'intention ni
l'esthétique du peintre, toutes choses égales par ailleurs, c'est que
ce jeune homme est malade d'être *ce qu'il est*.

## 28

Mais voici ce que je pensai alors. Je compris brusquement que je
m'étais laissé embarqué, encore, une fois encore, par mon analogie
trompeuse. Faire de lui un martyr était trop pompeux, convoquait une
imagerie trop désuète, trop fière. Fausse piste. Même si ces objets,
le cor de chasse pendu à la paroi, les paquets de lettres, les pétales
disséminés, l'anneau d'or, le coffre fermé, le livre, avaient une
fonction symbolique pour Lotto, nous en avons perdu la clé. Nous ne
connaissons ni l'identité du jeune homme ni la cause de sa maladie.
Certains veulent y voir la figuration d'une peine de cœur, d'autres de
la nostalgie. Nul n'en sait rien.

Moi-même j'ai longtemps cru de manière erronée que le livre ouvert sur
la nappe verte symbolisait l'étude, la lecture, l'ascèse du lettré, le
prix de l'érudition. Si j'avais mieux regardé, j'aurais remarqué plus
tôt que cet ouvrage n'était pas relié. Qu'il s'agissait en fait du
registre à feuillets mobiles d'un commerçant et que le jeune homme,
chargé de tenir les écritures de sa maison, indiquait par là sa
condition bourgeoise.

Si donc le jeune homme regarde avec tristesse et envie le spectateur,
c'est que nous, spectateurs, avons la liberté de laisser notre regard
s'évader par l'ouverture que ménage la fenêtre au fond du tableau,
tandis que lui, malheureux fils de marchands, dans son studio, reclus
et claquemuré, il ne peut se soustraire à sa condition, à ce qu'on a
fait de lui : un jeune homme assigné au registre des recettes et des
dépenses, qu'on a obligé à faire marcher la boutique, parce qu'on lui
a répété que la boutique menait à la fortune, que la fortune menait à
tout. Un jeune homme que sa médiocrité suffoque, incapable de trouver
une voie d'expression qui lui permettrait de transcender, de sublimer
cette médiocrité, et qui, pour n'avoir connu qu'elle, sent avec
chagrin qu'il aspire à quelque chose d'autre, de plus sublime, qui
demeure cependant hors de sa portée. Sa vie, réglée sur le bon sens
plutôt que sur la beauté, s'écoule en vain. Elle s'étiole sous son
regard impuissant : ni vivante ni vivable, dévitalisée, vidée de sa
substance, "absente", comme il m'est arrivé autrefois de l'écrire
--- pour dire une absence de vie.

Ce portrait serait alors une figure de l'empêchement, une figure de la
résignation. Oui, pensai-je, c'est cela : une histoire bourgeoise de
la maladie, comment la bourgeoise rend malade par déni du beau.

Mais meurt-on de ça ?

## 29

La vérité, c'est que le "jeune malade" est un fils soumis,
honteusement, fidèlement soumis à une famille qu'il hait de toutes les
fibres de son corps. Il n'y a qu'à cette condition, à la condition
d'une telle torture, qu'on parvient à obtenir ça, le dégoût de
soi-même, l'écœurement d'être soi, la maladie de devoir porter en soi
ce dégoût. Était-ce ça, le lézard, le petit animal ? L'impossibilité
de se désolidariser, de rompre avec le fossile en soi qui, depuis le
commencement, consent à sa propre humiliation, l'impossibilité
d'empêcher cette lâcheté de revenir, de repousser comme la queue
squameuse du reptile ?

Rien d'autre ?

Ce qui revient toujours à la même place, c'est le réel, l'angoisse ;
c'est la blessure, la souillure. Vouloir se quitter, ne pouvoir le
faire. Voilà qui tue : l'incessante réitération de ce déchirement
--- la repousse de la queue.

## 30

À la réflexion, comme souvent dans les tableaux, on devine qu'il
existe un sous-texte, une référence littéraire qui vient donner de
l'épaisseur à la représentation.

Dans le poème des *Métamorphoses*, Ovide narre l'histoire de
Philomèle. Une jeune femme violée par son beau-frère, Térée, qui lui
tranche la langue pour empêcher celle-ci de divulguer le crime dont il
s'est rendu coupable. La traduction dit que ce tronçon de langue
gesticule par terre comme la "queue coupée d'une couleuvre".

Le latin, lui, met simplement : *lacertus*, "lézard". Lacéré, lézard :
même étymologie.

Si le jeune homme de Lotto nous regarde en implorant, pensai-je
désabusé, c'est qu'il ne peut pas parler. Sa langue est sur la table.
Son mutisme, c'est sa maladie. Et sa maladie, c'est de ne pas pouvoir
raconter ce qui lui est arrivé. Ou seulement à la manière de
Philomèle. En filant.

--- Oui, dit Hélène (j'avais tellement besoin de l'entendre), il faut
de l'imagination pour voir la réalité.
