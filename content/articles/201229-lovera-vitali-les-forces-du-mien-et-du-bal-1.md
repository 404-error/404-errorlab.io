---
persona:
  - corinne lovera vitali
title: "les forces du mien et du bal (1)"
slug: "les-forces-du-mien-et-du-bal-1"
date: 2020-12-29
echo:
  - rythmique
images:
  - "img/201229-lovera-vitali-les-forces-du-mien-et-du-bal-1.jpg"
gridimages:
  - "img/201229-lovera-vitali-les-forces-du-mien-et-du-bal-1-grid.jpg"
sourceimage: "corinne lovera vitali"
notes: "L’image est un détail de *Los chafarderos*, une peinture de <a href=\"https://www.fernandfernandezpeinture.org/\" target=\"_blank\" rel=\"noopener\">Fernand Fernandez</a>."
summary: "j'ai dit à quelqu'un que j'étais faite pour vivre en temps de guerre il m'a dit qu'il y avait plein de pays en guerre où je pouvais emménager on ne s'est pas du tout compris je m'imaginais être protégée par une guerre comme dans une communauté d'endeuillés et de blessés et de morts et de traumatisés de souffrance et de résistance où par exemple personne ne pense à écrire un scénario où par exemple l'enfant est abusé sexuellement ou par exemple la femme est étranglée en série je voulais être protégée par la décence d'une guerre"
citation: "je voulais être protégée par la décence d’une guerre"
---


# le mien et le bal

j'ai dit à quelqu'un que j'étais faite pour vivre en temps de guerre il
m'a dit qu'il y avait plein de pays en guerre où je pouvais emménager on
ne s'est pas du tout compris

je m'imaginais être protégée par une guerre comme dans une communauté
d'endeuillés et de blessés et de morts et de traumatisés de souffrance
et de résistance où par exemple personne ne pense à écrire un scénario
où par exemple l'enfant est abusé sexuellement ou par exemple la femme
est étranglée en série

je voulais être protégée par la décence d'une guerre comme j'ai été
protégée pendant la durée légale autorisée du deuil je comprends qu'on
ne me comprenne pas mais il y a des moments où il serait plus juste pour
moi d'être en état de guerre mondiale déclarée plutôt que de fantasmer
par exemple la 2<sup style="margin-left:0.1em">e</sup>GM que Trintignant rapporte de sa voix enregistrée
avant d'avoir perdu son enfant et même pas à cause d'une guerre

Trintignant lit Leiris se désolant que quatre mois à peine après la
Libération il se retrouve de nouveau englué dans ses marasmes sexuels à
croire que les névropathes se sont mieux portés pendant les quatre
années de guerre je le cite de mémoire

il est possible que la seule pornographie que j'accepte soit celle d'une
guerre où rien n'est acceptable

il m'est revenu que pendant la durée légale autorisée du deuil en 1995
il y avait eu une bombe la voiture de ma mère avait été mise en
fourrière d'être restée trop longtemps près d'une synagogue puis il y
avait eu une très longue grève générale la circulation était bloquée par
les routiers il n'y avait plus de trains tout le monde prenait en stop
tout le monde dans les automobiles même immobiles tout le monde avait
des discussions politiques avec tout le monde la distribution du
courrier était interrompue mon père était très contrarié qu'on ne puisse
pas répondre aux condoléances tout ce qui aurait dû circuler était
paralysé il y avait de la colère et de la frustration et aussi ce qu'on
appelle solidarité mais dont on ne bénéficie qu'en cas de coup dur et
seulement pendant la durée de décence légale autorisée

il m'était revenu autre chose qui semblait plus important je l'ai oublié
aussitôt

ce que je voudrais peut-être c'est que la guerre soit déclarée
officiellement

puisqu'il est prouvé que toujours c'est la guerre et que toujours c'est
la guerre qui gagne

car on n'a jamais vu un pays qui ne voudrait pas se battre faire plier
un pays qui veut se battre

on n'a jamais vu une personne qui veut l'aimer faire plier une personne
qui ne veut pas l'aimer

on n'a jamais vu que le bien l'emporte sur le mal

aussi je préférerais de loin qu'on appelle tout ce bordel le mien et le
bal

là je vois mieux

le grand bal a des ressources que le petit mien ne peut même pas espérer

# la tuerie des endroits

chaque fois que même brièvement on a été à un endroit on a dû le quitter

quand on est assez vieux pour pouvoir mesurer derrière soi à l'endroit
du cul qui n'est pas le même que l'endroit du nez qu'on tourne pour voir
derrière soi avec les yeux qui le suivent on voit très bien ça

partout ça fait des espèces de squames de soi ça fait de la peau morte
et des escarres on craint que ça finisse par faire de la corne

chaque fois on a trouvé le courage d'aller à un autre endroit et chaque
fois ça a recommencé et on craint qu'à force on se retrouve coincé dans
de la corne à ne plus pouvoir croire aller nulle part

parce qu'on sait bien que ce qui compte pour ne pas avoir trop peur ce
n'est pas qu'il y ait un autre endroit c'est d'y croire

on sait bien que parmi tous les endroits possibles il y a celui qui dit
qu'il ne faut pas avoir peur mais on a aussi été à l'endroit où ceux qui
n'ont pas peur n'ont pas peur parce qu'ils n'ont jamais eu à avoir peur
on n'est pas du tout d'accord avec quelque chose d'aussi idiot on n'est
pas si idiot on sait qu'il faut avoir peur d'avoir peur donc on sait
qu'on doit protester contre cette décalcomanie de tous les endroits sur
le modèle du pire

parce que n'importe quel endroit on prend ça marche

si on ne comprend pas on peut se souvenir du sexe avant le sida

on peut se souvenir de la mer avant l'amoco cadiz

on peut même s'essayer au prétendu devoir de mémoriser

par exemple la vie avant qu'on soit en vie

quand on a attrapé le coup de la mémoire même par injonction on voit
très bien ça

quel que soit l'endroit où on n'a pas vraiment décidé de naître il nous
a fallu tuer cet endroit

tous on a dû tout quitter du meilleur du présent quasi au moment où on y
était

# casino

je suis issue par ma grand-mère maternelle d'une famille de paysans
esclaves qui pour se faire deux sous supplémentaires tressaient de la
paille à chapeaux on les appelle des chapeaux de paille d'Italie ils
tressaient les pailles qu'on leur apportait et on venait les leur
chercher tressées et on fabriquait des chapeaux je ne sais même pas si
dans ma famille de paysans esclaves ils en portaient eux-mêmes

je suis issue par cette même branche et par toutes les autres aussi de
personnes pauvres qui économisaient chaque sou

à côté d'eux je mène le train de vie de leurs patrons sauf que je n'ai
pas d'esclaves

cependant lorsque j'ai un bon de réduction au casino géant désormais je
note la date sur mon agenda et je n'y vais pas évidemment

mais avant-hier il y avait une réduction de 8 euro pour 50 euro d'achat
et j'y suis allée comme j'avais la tête vide je n'ai pas pensé à acheter
des croquettes pour les chats ni des citrons ni rien de ce qui nous
manquait à la place j'ai acheté peut-être pour tenir ma tête vide un
chapeau à très large bord qui m'a rappelé les chapeaux de paille
d'Italie il coûtait 5 euro

comme il n'a pas été tressé par des esclaves italiens du 19<sup style="margin-left:0.1em">e</sup> mais par
des esclaves chinois du 21<sup style="margin-left:0.1em">e</sup> il fait un bruit de plastique dans chacune
de mes oreilles quand je marche sauf si je laisse mes oreilles à
l'extérieur et alors il glisse et il tombe

c'est une très bonne opération commerciale de la part du bordel géant

mon intention est d'y retourner en portant ce machin en plastique sonore
sur la tête et de le replacer dans le rayon et de ressortir illico sans
demander des comptes

mon intention est de me laver de mon vilain achat d'esclave

ma grand-mère maternelle m'approuverait

je me demande brièvement quelles seraient mes intentions si le chapeau
avait rempli son office

elle m'approuverait en toute chose je le sais

sans rien comprendre à rien

# mes comptes :

1898 : ma grand-mère maternelle naît elle-même de sa mère : 7<sup style="margin-left:0.1em">e</sup> d'un
pack de 11 dont 8 filles

1899 : mon bébé grand-père maternel a absorbé à peu près 1095 litres de
lait de sa mère depuis sa naissance il y a 4 ans : 1 bon gros m<sup style="margin-left:0.1em">3</sup> de
lolo du XIX<sup style="margin-left:0.1em">e</sup>

1900 : je dis des centaines mais ça pourrait être des milliers personne
n'a précisé qu'est-ce qu'ils s'en foutaient alors je dis des centaines
d'œillets rouges sont distribués dans la rue par le père paternel de mon
grand-père maternel

1920 : mes grands-parents maternels se marient : jamais que pile 40 ans
avant ma naissance

1920-1921 : un des comptes qui les font fuir de la Vallée du Pô mais
évidemment par comptes il faut entendre faits et par faits il faut bien
entendre ce que disait Mussolini qui voulait entendre justement le bruit
des crânes pilés : « Au mois de juillet 1920 les faisceaux sont au
nombre de 108 constitués ou en voie de constitution. Vers la mi-octobre,
quelques semaines après l'occupation des usines, ils sont 190 ; à la fin
de l'année, ils dépassent 800 ; ils atteignent le millier en février
1921 ; 277 faisceaux nouveaux se constituent en avril, 197 en mai ; en
novembre 1921, au Congrès du parti, on en compte 2300<sup style="margin-left:0.1em">1</sup> » : avec ce qui
suit me fait naître maternellement ici en fronce

1921 : l'historien officiel du parti qui était vétérinaire de son état a
fourni ces chiffres concernant la destruction de toutes les
organisations rouges de la Vallée du Pô : journaux et imprimeries : 3,
maisons du peuple : 40, bourses du travail : 36, coopératives : 73,
ligues paysannes : 75, sociétés de secours mutuel : 4, sections et
cercles socialistes et communistes : 29, bibliothèques populaires et
théâtres : 7, universités populaires : 1, cercles ouvriers et
amicales : 7, total : 275 : « Mais ces chiffres, très inférieurs à la
réalité, ne tiennent pas compte du fait que la destruction d'un seul
édifice, Maison du peuple ou Bourse du travail, entraîne celle de toutes
les organisations qui y avaient leur siège. En outre ils ne comprennent
pas les simples expéditions punitives, ni les violences sur les
individus, ni les simples fermetures de locaux, ni les démissions
forcées des municipalités, ni les destructions de maisons ou de magasins
privés, ni les bannissements, ni les autres formes de terrorisme<sup style="margin-left:0.1em">1</sup>. »

1910-1938 : « L'électrification de grandes lignes, que le P.-L.-M. a
expérimentée en 1910 dans la région méditerranéenne et que le Midi a
commencée à la même époque, se poursuit : Chambéry-Modane sur le P.-L.-M
en 1930, Paris-Le Mans sur l'État en 1937, Tours-Bordeaux sur le P.-O.
en 1938. Des raisons stratégiques excluent alors de toute
électrification les réseaux du Nord, de l'Est et de
l'Alsace-Lorraine<sup style="margin-left:0.1em">2</sup>. »

1928 : en 15 ans mon grand-père maternel a fabriqué plus ou moins 1643
milliards de spermatozoïdes dont 1 a été ma mère<sup style="margin-left:0.1em">1</sup>&nbsp;<sup style="margin-left:0.1em">2</sup>&nbsp;<sup style="margin-left:0.1em">3</sup>&nbsp;<sup style="margin-left:0.1em">4</sup>

<sup>1</sup> Angelo Tasca : *Naissance du fascisme*

<sup>2</sup> *Du char à bancs au TGV* : La Vie du Rail

<sup>3</sup> comme ça semble lointain 2004 : les correctrices ne bondissaient pas
en aboyant quand je prétendais que ma mère était un spermatozoïde

<sup>4</sup> tout ce temps qu'il m'a fallu : ces orgasmes certes mais toutes ces
guerres ces morts ces horreurs juste pour naître ça me paraît très
disproportionné en 2020

# capte

je me dis c'est un train d'enfants nous sommes les seuls adultes ces
enfants sont soit très bruyants quand ils sont en groupe soit très
silencieux solitaires devant leur minuscule écran je me dis c'est un
wagon on a attendu deux heures comme des moins que rien sur le quai de
la grosse gare tgv on a pris le vent

à un moment je me suis accroupie sur une rambarde de protection d'un
pilier à mes pieds il y avait une couverture de merdes de pigeon toutes
les femmes sur le quai se plaignaient et soupiraient elles étaient
sarcastiques elles étaient en colère j'étais calme j'ai voulu penser au
documentaire qu'on venait de voir sur Marceline Loridan-Ivens et ça
s'est associé dans ma tête très rétrécie ces temps à ce que j'avais cru
découvrir pour la première fois de ma vie la nuit d'avant qui était la
permanence de la lune et de la mer qu'avaient vues aussi les Romains et
ça m'avait rassurée et calmée et réconfortée mais sur le quai la même
pensée pointait de la permanence du mal qui n'a aucun rapport avec la
sncf ou les femmes plaintives mais était inscrite ou forçait le passage
dans ma tête étroite pour se fixer sur la merde des pigeons et les
wagons du train express régional dont le vrai nom est ter qui ne sont
plus des trains que de pauvres gens ou d'enfants et nous sommes parmi
eux

je suis contente d'être parmi eux je suis contente de me déplacer de
moi-même du toujours moi-même je suis restée si longtemps sans le faire
désormais je le fais sans aucun des accessoires qu'on adopte en voyage
je ne lis pas je ne regarde pas de film je n'écoute pas de musique je
n'ai pas de pensée je suis un poteau humain récepteur je me suis
protégée de ça beaucoup et longtemps et je dois encore le faire parce
que tout ce que je capte et si je le capte c'est parce que ça m'est
adressé tous ces captages ensuite m'occupent le corps les yeux les
oreilles ils me restent et ils ne sont pas toujours aussi simplets
qu'une demi-lune par le petit bout de ciel d'une terrasse de
rez-de-chaussée et ils ne sont pas toujours aussi terribles que certains
des concentrés de mort et de violence dont je n'ai jamais su quoi faire
même après des années ils ne sont pas toujours aux extrémités comme mon
esprit tente d'emblée de les placer la plupart se trouvent quelque part
entre la juste beauté et l'éternité ils sont moyens ils sont minables
ils sont déprimants désespérants irritants la plupart sont bêtes et
méchants de bêtise et de méchanceté de qui monte dans un wagon de ter
par exemple ou de qui s'installe sur la plage pour mater une femme
pendant qu'elle enlève sa combinaison de plongée ou de qui serre son sac
à main en proclamant qu'on ne sait plus comment s'habiller tandis
qu'Elena de Zviaguintsev ne se pose jamais ces questions ce n'est pas le
travail de Zviaguintsev ces questions c'est un peu le mien mais je ne
sais pas pourquoi et je ne sais pas comment ça se fait
