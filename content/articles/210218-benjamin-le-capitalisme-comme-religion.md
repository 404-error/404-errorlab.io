---
persona:
  - Walter Benjamin
title: "Le capitalisme comme religion"
slug: "le-capitalisme-comme-religion"
date: 2021-02-18
echo:
  - ontique
  - anthropique
  - stratégique
images:
  - "img/210218-benjamin-le-capitalisme-comme-religion.jpg"
gridimages:
  - "img/210218-benjamin-le-capitalisme-comme-religion-grid.jpg"
sourceimage: "https://en.wikipedia.org/wiki/File:NY_stock_exchange_traders_floor_LC-U9-10548-6.jpg"
notes: "La traduction d'Ann Persson de ce fragment, datant vraisemblablement de la fin 1921, a pour objectifs de se placer dans le domaine public volontaire et de faire découvrir l'obscurité lumineuse de Walter Benjamin, qui développe l'idée du capitalisme comme religion, et de la religion comme *Schuld*. Il s'avère nécessaire de préciser l'importance et la riche ambiguïté du terme allemand *Schuld*, dont Walter Benjamin joue, qui recoupe tant le sens du mot *dette* que celui du mot *culpabilité*. La version originale de ce texte se trouve notamment dans *Walter Benjamin, Gesammelte Schriften VI, Suhrkamp, Frankfurt am Main, 1991, fragment 74, p. 100*."
summary: "Dans le capitalisme, une religion est à découvrir, c'est-à-dire que le capitalisme sert essentiellement à la satisfaction des mêmes soucis, tourments et inquiétudes, auxquels jadis lesdites religions donnaient réponse. La justification de cette structure religieuse du capitalisme, non seulement, comme le pense Weber, en tant que formation religieusement conditionnée, mais aussi en tant que phénomène essentiellement religieux, nous conduirait aujourd'hui encore sur la fausse route d'une incommensurable polémique universelle."
citation: "L’esprit qui parle depuis l’ornementation des billets de banque."
---

Dans le capitalisme, une religion est à découvrir, c'est-à-dire que le capitalisme sert essentiellement à la satisfaction des mêmes soucis, tourments et inquiétudes, auxquels jadis lesdites religions donnaient réponse. La justification de cette structure religieuse du capitalisme, non seulement, comme le pense Weber, en tant que formation religieusement conditionnée, mais aussi en tant que phénomène essentiellement religieux, nous conduirait aujourd'hui encore sur la fausse route d'une incommensurable polémique universelle. Nous ne pouvons pas tirer le filet dans lequel nous nous trouvons. La portée de ceci sera pourtant saisie plus tard.

Trois caractéristiques sont cependant déjà reconnaissables dans le temps présent à cette structure religieuse du capitalisme. Premièrement, le capitalisme est une pure religion cultuelle, peut-être la plus extrême qu'il n'y a jamais eu. Tout ce qu'il a en lui est immédiatement en rapport avec la signification du culte, il ne connaît aucune dogmatique particulière, aucune théologie. L'utilitarisme gagne de ce point de vue sa coloration religieuse. À cette concrétion du culte s'attache une deuxième caractéristique du capitalisme : la durée permanente du culte. Le capitalisme est la célébration d'un culte *sans rêve et sans merci*. Il n'y a là aucun "jour ouvrable", aucun jour qui ne soit pas jour de fête dans le sens terrible du déploiement de toute la pompe sacrée, de la tension extraordinaire de celui qui vénère. Ce culte est en troisième lieu culpabilisant. Le capitalisme est probablement le premier cas d'un culte non expiatoire, mais culpabilisant. En cela, ce système religieux se trouve dans la chute d'un mouvement monstrueux. Une monstrueuse conscience coupable, qui ne sait pas s'expier, se saisit du culte non pas pour racheter en lui cette culpabilité, mais pour la rendre universelle, pour la marteler en la conscience et finalement et avant tout pour inclure le dieu lui-même dans cette culpabilité, pour finalement l'intéresser lui-même à l'expiation. Celle-ci n'est donc pas à attendre ici du culte lui-même, pas plus encore que dans la réforme de cette religion, qui devrait pouvoir se baser sur quelque chose d'assuré en elle, ou que dans le refus de celle-ci. Il se trouve en l'essence de ce mouvement religieux, qu'est le capitalisme, le fait d'endurer jusqu'à la fin, jusqu'à la complète et finale culpabilisation de Dieu, jusqu'à l'état mondial du désespoir atteint, qui de justesse encore est *espéré*. L'inouï historique du capitalisme se trouve dans ce fait que la religion n'est plus la réforme de l'être, mais de sa destruction. L'expansion du désespoir à l'état religieux mondial, duquel serait à attendre la guérison. La transcendance de Dieu est tombée. Mais il n'est pas mort, il est inclus dans le destin de l'humain. Ce passage de la planète humaine à travers la maison du désespoir dans la solitude absolue de son orbite est l'éthos qui définit Nietzsche. Cet humain est le surhumain, le premier qui commence, en la discernant, à accomplir la religion capitaliste. Sa quatrième caractéristique est que son dieu doit être caché, qu'il ne peut être abordé que dans le zénith de sa culpabilisation. Le culte sera célébré devant une divinité immature, toute représentation, toute pensée à son sujet lèse le secret de sa maturité.

La théorie freudienne fait aussi partie de la domination presbytérale de ce culte. Elle est pensée de façon tout à fait capitaliste. Le refoulé, la représentation pécheresse, à partir de la plus profonde analogie encore éclairante, est le capital, lequel rémunère l'enfer de l'inconscient.

Le type de la pensée religieuse capitaliste se trouve formulé de manière grandiose dans la philosophie de Nietzsche. L'idée du surhumain transfère le "saut" apocalyptique non pas dans la conversion, la réparation, la purification, la pénitence, mais dans l'augmentation apparemment continue, mais en dernier lieu discontinue, explosive. Par conséquent, l'augmentation et le développement dans le sens de *non facit saltum* sont inconciliables. Le surhumain est l'humain historique, qui est arrivé sans conversion, qui s'est accru au travers des cieux. Nietzsche a préjugé cette explosion des cieux au travers d'une humanité augmentée, qui est et reste une culpabilisation religieuse (même pour Nietzsche). Et similairement Marx : le capitalisme ne se convertissant pas devient, avec intérêts et intérêts composés lesquels sont fonction de la *dette* (voir l'ambiguïté démoniaque de cette notion), socialisme.

Le capitalisme est une religion de culte pur, sans dogme.

Le capitalisme --- comme il doit être prouvé non seulement au sujet du calvinisme, mais aussi au sujet des courants chrétiens orthodoxes restants --- s'est développé de façon parasitaire sur le christianisme en Occident, de telle sorte qu'à la fin son histoire est en substance l'histoire de son parasite, l'histoire du capitalisme.

Comparaison entre, d'une part, les images saintes de diverses religions et, d'autre part, les billets de banque de divers États. L'esprit qui parle depuis l'ornementation des billets de banque.

Capitalisme et droit. Caractère païen du droit. Sorel, *Réflexions sur la violence*, p. 262.

Surpassement du capitalisme par la migration. Unger, *Politik und Metaphysik* (Politique et métaphysique), p. 44.

Fuchs, *Struktur der kapitalistischen Gesellschaft* (Structure de la société capitaliste), ou une chose semblable.

Max Weber, *Ges. Aufsätze zur Religionssoziologie* (Essais de sociologie de la religion), 2 vol., 1919-1920.

Ernst Troeltsch, *Die Soziallehren der chr. Kirchen und Gruppen* (Les doctrines sociales des églises et groupes chrétiens), œuvres complètes, I, 1912.

Voir avant tout la bibliographie de Schönberg, sous II.

Landauer, *Aufruf zum Sozialismus* (Appel au socialisme), p. 144.

Les soucis : une maladie mentale qui qualifie l'époque capitaliste. Absence d'issue spirituelle (non matérielle) dans la pauvreté, monachisme-mendiant-errant. Un état, qui est à ce point sans issue, est culpabilisant. Les "soucis" sont l'index de cette conscience coupable de l'absence d'issue. Les "soucis" naissent en la peur de l'absence d'issue dans le cadre de la communauté, et non pas dans une absence individuelle-matérielle.

Le christianisme au temps de la Réforme n'a pas favorisé l'apparition du capitalisme, mais s'est transformé en capitalisme.

Méthodiquement, il serait d'abord à analyser quels liens avec le mythe l'argent a contractés au cours de l'histoire, jusqu'à ce qu'il puisse tirer du christianisme tant d'éléments mythiques pour constituer son propre mythe.

Wergeld / Thesaurus des bonnes œuvres / Rémunération qui est due au *prêtre*. Pluton en tant que dieu de la richesse.

Adam Müller, *Reden über die Beredsamkeit* (Discours sur l’éloquence), 1816, p. 56 et suivantes.

Rapport du dogme de la nature dissolvante du savoir, dans cette qualité pour nous à la fois rédemptrice et meurtrière, avec le capitalisme : le bilan en tant que savoir rédempteur et liquidateur.

Cela contribue à la connaissance du capitalisme comme religion de se représenter que le paganisme originel a été assurément le plus proche d'appréhender la religion non comme un intérêt "moral" "supérieur", mais comme l'intérêt pratique le plus immédiat, en d'autres termes, qu'il se soit tout aussi peu que le capitalisme actuel rendu compte de sa nature "idéale" ou "transcendante", plus précisément dans le sens de ce qu'un membre infaillible d'une communauté voyait dans l'individu irréligieux ou d'une autre foi de sa même communauté, tel ce que voit la bourgeoisie actuelle dans les bourgeois ne gagnant rien.
