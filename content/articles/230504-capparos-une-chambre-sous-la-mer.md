---
persona:
  - Olivier Capparos
title: "Une chambre sous la mer"
slug: "une-chambre-sous-la-mer"
date: 2023-05-24
echo:
  - rythmique
images:
  - "img/230504-capparos-une-chambre-sous-la-mer.jpg"
gridimages:
  - "img/230504-capparos-une-chambre-sous-la-mer-grid.jpg"
sourceimage: "Olivier Capparos"
summary: "Inhabiter est un verbe pour une naissance, pour / ceux qui naissent, celles qui attendent, ceux qui ne sont / pas encore, celles qui ont grandi depuis. Ce n'est pas / déshérité qu'on naît sur terre, mais la difficile forme de vie inhabite / tout au début, la lumière et les taches chromatiques qui nourrissent / les premiers arcs de l'attention, / et que la chair s'anime, chevrotante, tigre et agneau / chair vive de questions du bout des phalanges et de doigts tremblants. / Nous n'habitons qu'une créature qui aime couleur et lumière, / et nous inhabitons ce que langue baptise et rebaptise / de mots pour se convaincre d'une réalité. Et du réel / d'une habitation et d'un habitacle. MONDE."
notes: ""
citation: "regarde plus bas / vers le royaume des hauteurs négatives."
poetry: true
hangindent: true
---

# I. Inhabiter

Inhabiter est un verbe pour une naissance, pour

{{%retrait1%}}ceux qui naissent, celles qui attendent, ceux qui ne sont

{{%retrait2%}}pas encore, celles qui ont grandi depuis. Ce n'est pas

déshérité qu'on naît sur terre, mais la difficile forme de vie inhabite

tout au début, la lumière et les taches chromatiques qui nourrissent

les premiers arcs de l'attention,

et que la chair s'anime, chevrotante, tigre et agneau

{{%retrait1%}}chair vive de questions du bout des phalanges et de doigts tremblants.

{{%retrait2%}}Nous n'habitons qu'une créature qui aime couleur et lumière,

{{%retrait2%}}et nous inhabitons ce que langue baptise et rebaptise

{{%retrait2%}}de mots pour se convaincre d'une réalité. Et du réel

{{%retrait2%}}d'une habitation et d'un habitacle. MONDE.

{{%retrait2%}}Et dont les signes dont BAL, ONDE, BALLON,

{{%retrait2%}}et encore d'autres résonances peut-être plus limpides.

{{%retrait2%}}Quel corps nouveau se traînera vers de si limpides refus ?

Avant nous, un jeu qui nous met en fuite de nous-mêmes.

 

C'est avec l'habitude

qu'on habite <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>en prenant ses appartements du monde,

finalement, comme des fantômes qui se croient de chair

et tout habillés, clairs ou sombres

de vêtements réels.

Sans fatigue au début, ou presque jamais

c'est ainsi que cela naît.

Lorsque j'eus atteint ma première minute

j'ouvris les yeux à des visages brouillons

et des voix balbutiantes.

Et sans les yeux je sus

les os et la saigne de mon habitat sans cri.

 

J'avais la peau en surface, déficiente ---

déficiente robe de corps d'intérieur,

avec des rougeurs et des pâleurs, comme dans les cas de feux sans
preuves.

C'est une robe qui fait corps comme un linge humide de lin sans
épaisseur.

Échancrée, une blouse, grise de cœur. L'invention suffit

à se croire

habitant. Le faste des fables à la maison

qui nous laisse reposer sous des montagnes lourdes.

Déjà allant au bout du pont qui donne sur l'eau.

Dans les eaux douces le poisson-archer mesure les espaces

les moments et les angles ; sans voir

il voit.

Loin derrière moi une voix m'appelait.

Sainte Vêpre aux ongles souillés du jardin retourné --- ne reste pas si
près de l'eau.

Combien l'eau doit être inhabitable, qu'on la craigne comme la teigne
des petits animaux familiers.

Mais l'eau parlait d'une voix claire

et appelait par des syllabes mouillées de promesses.

 

La voix Fleuve, neuve hier comme ce jour, enfle

comme un dos à la surface, et te dit : viens et descends.

 

Tu lui as donné le nom de Fleuve, parfois de Ruisseau

ou de Courant

car il te semble qu'elle n'est pas la même matière et force

que l'étendue et l'horizon des eaux mêmes.

 

Et tu écoutes cette voix et sa promesse,

car tout dans l'inhabiter de l'air

est devenu irrespirable. Tout est corpuscule

envoûtements de vies frénétiques,

si dur, si tranchant

d'arbres encâblés liés par leurs crachats électriques.

 

Pourquoi hésiter ? De quoi as-tu peur ?

J'ai peur d'être la proie de ce qui me sauve.

 

Laisse décliner à tes chevilles ton oripeau qui ne protège de rien.

Je tenterai ma chance, sans grande conviction.

La lumière du matin adoucit de nuit les morsures,

lorsqu'elle se prolonge de doigts roses sans gants.

Dans le clos noir de l'une des chambres innombrables

à l'air bâti --- tu es l'hôte immobile de palais très anciens.

 

Où elle, toi et moi, s'inspirait de farces et désespoirs.

Par exemple, nos bouches et nos fronts

captent à plusieurs reprises

une joie augmentée <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>(l'onde sans front du bonheur nu).

Entre des dents, un dé de 6

entre tes incisives

et après avoir roulé entre dents et muqueuses

un nombre paraît, une face numérale

par les dents ou par recrachat,

ainsi à ce moment nous rions

de tous les nombres et faces manquantes

d'1 jusqu'à 6.

Nos actes d'enfants sont maintenant moins visibles.

Personne ne compte et ne croit plus ce qui manque.

{{%retrait1%}}Nous étions sans lune, sinon un ballon renfrogné,

{{%retrait1%}}un peu dégonflé, à force d'avoir trop attendu

{{%retrait1%}}d'être joué, de chocs

{{%retrait1%}}et de coups de pieds innocents.

{{%retrait1%}}Balle parmi les balles de toutes dimensions.

{{%retrait2%}}Un jouet lancé trop loin sur la mer,

{{%retrait2%}}emporté à la dérive, flotta

{{%retrait2%}}en girant sur lui-même, boule jaune

{{%retrait2%}}sur le bleu aux dents blanches.

{{%retrait2%}}En le voyant s'éloigner

{{%retrait2%}}tu pouvais voir l'âme des chaînes futures

{{%retrait2%}}l'ample et doux mensonge

{{%retrait2%}}de te sentir libre.

{{%retrait1%}}Souvent l'avenir ne trouve pas de prémices

{{%retrait1%}}dans le présent

mais dans le passé accompli.

 

Pourquoi hésiter ? De quoi as-tu peur ?

Les courants chauds et froids

se disputent les influences

et les vies sans bruit.

Les âmes par l'eau enchaînées

seraient plus libres

que d'être la terminaison de maillons

et d'anneaux de fer forgé ?

 

Un ami a dit :

j'irai traire la vache au matin

je boirai son lait et

je m'occuperai du fourrage

au matin,

avec un bracelet à ma cheville meurtrie.

 

Ne sois pas complice de rivages

et de riverains. J'aime,

dit-elle,

les hommes attentifs à leurs travaux,

conscients et attentifs

à leur temps imparti.

Mais comment sais-tu si telle vie

doit être épargnée

et une autre abolie ? L'amour ---

sa caresse prône l'un

ou l'autre plateau de la balance.

Mais lorsqu'il t'étreint, le dit :

sauve-les toutes et tous,

du premier au dernier

en détail et en entier.

Il a été décidé pour moi

que je prendrai plusieurs visages ---

un masque de pierre

une peau écailleuse

et d'autres physionomies que tu ne connais pas encore.

 

Une voix prévenante au discours préventif --- est-ce toi encore ?

Les paroles sacrées et les théâtres des libertés sombres et violents

n'ont pas été créés pour être lus et interprétés

par des démons coutumiers et locaux

et des auxiliaires avides et jaloux

des verbes de l'attente et de l'accomplissement.

 

De quoi as-tu peur ? À la surface

ce que les humains nomment monde ou cosmonde

n'est que région morte, où respirer

donne la mort pulmonaire. Les feuilles d'arbres

flottent et tombent en flocons de cendres.

Ce ne sont que détails et photographies partiels

de la grande nécrose d'espace.

Ils emménagèrent dans des tombes résignées --- des

tombes de survie, amputés des sens,

des arômes et des couleurs.

Ils formèrent des rangs de noms sans visages

ou des visages aux sourires et aux pleurs

brouillés à l'acide blanc de l'oubli.

Nulle lumière filtrée

ne viendra aviver leurs restes si bas.

Commence le lent veuvage de la terre d'en haut.

 

Pourquoi hésiter ? De quoi as-tu peur ?

Se perdre et céder à cette perdition --- déperdre

est le contraire de dépendre.

Cette voix, encore, qui est l'eau même

parmi les eaux mêmes ;

il faut sortir des angles aigus

et des diffractions, sortir

des réfractions qui te persuadent

qu'une surface n'a pas à être traversée.

Il faut entrer, maintenant, aller prendre l'eau

apprendre à respirer sous l'eau

n'être que silhouette avant d'oublier

qu'il y eut des silhouettes d'abord,

se laisser couler, hésiter, au début

comme une ombre de toupie,

remonter à l'air bleu pâle,

par instinct, replonger encore

sans avoir de tête bien dessinée,

s'être dévêtu dehors

et se vêtir dessous

du vert des algues sans paroles,

se laisser dévêtir, plus bas

encore, des derniers rayons du jour venus d'ailleurs.

Car sous la mer

tes yeux tournés vers les murmures blancs de la surface

tu vois que tu viens d'ailleurs.

Si tu as encore une tête et des yeux,

regarde plus bas

vers le royaume des hauteurs négatives.

# II. Le Bain --- Les eaux mêmes

Immersion --- ne pas se débattre

ou chorégraphier les vagues

et les émulsions,

l'imagination doucement bouillonnante.

 

Une fois entièrement englouti et bu

à petites gorgées ---

accepte de te laisser couler.

{{%retrait1%}}Trombes et vortex, aussi petits

{{%retrait1%}}qu'infaillibles <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>te démembrent <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>sans violence --- tu

{{%retrait2%}}ne sais plus ; es-tu déjà une

{{%retrait4%}}noyée ?

{{%retrait2%}}Une mort de plus ?

{{%retrait2%}}Un corps gravement lesté

{{%retrait2%}}coulant sans fin, sans étapes

{{%retrait2%}}ton corps creuse un berceau

{{%retrait2%}}dans des draps sans lumière.

Il y eut des seuils, des étapes pourtant ; de ce côté

 

il entendait l'inentendu qui manquait à l'appel ---

moins de 20 Hz, sous la mer

son oreille interne de méduse apprivoisée --- rhopalies.

Rhopalies d'organes compacts, récepteurs sensoriels.

Un poids t'entraîne vers des fonds informes ---

un nuage noir de suie ? Non

ce n'est que l'encre lente <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>d'une pieuvre.

 

Si tu veux connaître le froid ou le peu de feu des êtres

il te faudra leur toucher les mains et les doigts.

Ses doigts et ses orteils sont des ocelles

des yeux à facettes

des pustules transparents qui te regardent en paix.

 

{{%retrait1%}}Es-tu encore la forme de vie qu'ils ont vue naître ?

{{%retrait1%}}Es-tu encore ta forme de naissance ?

Tête et œil aux écailles soulevées

palpitantes comme des stores vénitiens,

volets et rideaux mus remuements d'amour.

 

Des lettres et des croches stagnent

et même remontent vers le clair.

{{%retrait3%}}Une bague coule

{{%retrait3%}}plus lourde que toi.

{{%retrait3%}}Car ce chiffon que tu es se dénoue en draps ralentis

{{%retrait3%}}inorganiques couleurs indolores ---

{{%retrait3%}}rouge, bleu sombre, violet

{{%retrait3%}}mangées de noir

{{%retrait3%}}et de vert absolu.

J'ai de la vision la faiblesse

de lui appartenir.

Il n'y pas les yeux, et les possibles

d'être ouverts et fermés.

 

Je suis dans la profondeur depuis si longtemps

que l'ouïe des ondes aériennes

m'est inconnue.

J'entends par et dans

le clos amniotique de ma pensée.

Ainsi, étranger par conséquent à tout

ce grand nombre d'yeux et d'oreilles étrangers

qui me devinent ---

{{%retrait3%}}couler dans

{{%retrait3%}}le gouffre incolore

{{%retrait3%}}l'anonyme qui appelle.

De la terre à la surface de ta planète

je ne vois que le bord elliptique

qui s'éloigne flou à contre-jour.

Parfois je crois deviner des radicelles

ou des roseaux un peu immergés, des ombres,

mais c'est peut-être parce que submersif je vous vois encore,

silhouettes en train de se figurer d'être en vie.

À moins que mes cils.

Le reste des franges du manteau de mes yeux.

Avant de fondre.

Aurai-je encore des lèvres et d'autres muqueuses

pour me signaler que

je ne suis pas tout à fait seul

à n'être pas.

{{%retrait3%}}Si j'étais encore vivant comme un humain vivant

{{%retrait3%}}cela voudrait dire que j'ai survécu à la noyade qui dure peu.

{{%retrait3%}}Que les poumons par une chimie inconnue

{{%retrait3%}}sont devenus réserves inépuisables d'air,

{{%retrait3%}}ou organes amphibiens.

{{%retrait4%}}Mais comment parler de corps, d'organe du moi

{{%retrait3%}}qui n'est qu'identification à tout ce qui passe,

{{%retrait3%}}devient, s'arrête ou fléchit ?

{{%retrait1%}}Je n'ai pas résisté. J'ai plongé sans m'appartenir. Je n'ai pas
plongé. Je suis né

{{%retrait3%}}en coulant, sans m'appartenir,

{{%retrait4%}}sans la forme d'être moi.

 

Sur ce chemin de l'aveuglante région

je rencontrai un poulpe ou un semblable penseur souple.

De son encre de mots en formation

il éclairait les profondeurs d'un noir plus profond.

{{%retrait3%}}Ici, les yeux sont brûlés

{{%retrait3%}}par les mêmes feux

{{%retrait3%}}que les oreilles devinent.

{{%retrait3%}}Ici je règne sans nom,

{{%retrait3%}}sans tiare ni couronne

{{%retrait3%}}sur les frontières immuables

{{%retrait3%}}cependant mobiles

{{%retrait4%}}tempéraments, changeantes

{{%retrait3%}}introphanies,

{{%retrait3%}}extinctions après extinctions,

{{%retrait3%}}de sorte que nul

{{%retrait3%}}ne peut en retenir

{{%retrait3%}}le fuseau des apparitions.

{{%retrait3%}}Ici je ne pleure plus des délivrances qui ne viendront jamais,

{{%retrait3%}}je ne pleure plus que la mer entière

{{%retrait3%}}qui ne connaît pas les yeux et les larmes,

{{%retrait3%}}ni les tiennes, ni les miennes.

{{%retrait3%}}Je suis dans la profondeur

{{%retrait3%}}depuis si longtemps

{{%retrait3%}}que je n'ai pas idée ou souvenir

{{%retrait3%}}d'avoir respiré

{{%retrait3%}}d'autres humeurs

{{%retrait3%}}d'autres mondes.

Et ces voix qui étaient miennes et non les miennes

mais les fruits sonores d'un certain arbre (par image)

qui avait rapetissé en quelque endroit de mon corps,

comme une main et des doigts

poussent ou se fanent

dans la main et les doigts réels que la nature consciente a conçus ---
qui ?

Qui parle ici de son ici

sans que se décèle un nom ? Seul trans-aberré

je clignote sous ma peau translucide

comme pour mimer mon apparaître

avant d'apparaître, ou bien après

que d'être la forme mimée, enfin en vie.

La primale se tait et se fait oublier.

{{%retrait3%}}Je ne serai plus le mendieur

{{%retrait3%}}d'arbres et de fleurs disparus.

{{%retrait3%}}Mendieur d'asphyxiantes beautés ? Je ne pleure.

 

Un soleil abstrait passe sur ton visage,

et tu sais qu'il n'existe pas.

 

Depuis longtemps je t'ai cherché

sous la paupière du monde ---

depuis tant et tant d'années, des siècles je te cherchais

dans la poussière, dans les bosses oculaires des statues infirmes, rien

ne répondait à mon regard, rien

dans l'immobile arrondi d'un globe de calcaire.

Bien loin sous les vagues

des statues aux chevilles et aux cuisses fendues --- derniers

restes d'oracles anciens ou de tombeaux.

Pourquoi as-tu gardé ces yeux

alors qu'ils ne sont jamais faits pour voir ?

À l'air libre les monuments

t'envisageaient sans te voir.

On en balayait les socles,

époussetant les titres sculptés et les orteils de pierre lente.

Tu vois que tu n'as pas oublié.

Il y a eu des sculpteurs de titans aveugles.

Tu y tiens encore, à ces temples rudes

que le soleil baignait souvent.

Tu te souviens ? Qu'à l'ère des élévations antiques

les hommes chuchotaient devant des pieds de géants.

En d'autres temps d'effarantes libations de lumière et de sang

suffisaient à animer les statues de grognements, assourdis prestigieux.

Maintenant que tu es aux vestiges

engloutis dans les fonds indisponibles déesses et rois,

tu les baiseras d'une bouche humide et salée

et tu verras la mort dans leurs yeux immobiles.

{{%retrait2%}}Ne fuis pas.

{{%retrait2%}}Ne fuis jamais.

{{%retrait2%}}Il n'y rien à fuir.

{{%retrait2%}}Rien dont tu t'échapperais,

{{%retrait2%}}si l'envie t'en prenait.

 

Tu te laisses couler, encore

jusqu'à une phase prochaine ---

arc-en-mer sans iris.

Sans appel, aucun signal

bien que survive

un "nous" muet et aveugle.

Se rêver scaphandre,

enfin, se remémorer

un corps et ses limites,

au moins un corps et ses limites.

{{%retrait1%}}Suis-je encore en mesure de dessiner

{{%retrait1%}}redessiner les aspérités, arcs et bords d'angles

{{%retrait1%}}s'ils se présentent ?

{{%retrait2%}}Loin dedans, loin dans le vaste <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>et la profondeur,

{{%retrait1%}}suis-je encore ? Je, encore ? Vivre sans se penser vivant

{{%retrait1%}}sans y croire, ou presque, <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>simple masse

{{%retrait1%}}d'être-descendant, sous, dedans, de nulle part,

{{%retrait1%}}descendant de nulle part.

{{%retrait2%}}Loin de lumières, lampes-torches

{{%retrait2%}}ou fastes ors d'empires, (ici le cuivre brille plus loin que l'or)
sans l'emprise

{{%retrait1%}}du soleil qui traversait la poussière en lignes droites

{{%retrait1%}}et obliques --- traversait les terres acides, inclément de chaleur ---

{{%retrait1%}}traversait l'humain anéanti

{{%retrait1%}}par ses propres inclémences.

Nous voici --- nous ?

À gravité lente

aspirés

vers un fond

un fond noir sans trésor.

Qui de nous attend encore

que se dispersent

les eaux mêmes

et le noir ?

Qu'une lumière

vienne

de l'invisible ? Si

si on la sent de dos, une chaleur

ou une main rapide,

une caresse d'oxygène

libérée des ongles souples d'algues.

{{%retrait1%}}Inutile de se débattre sans corps, sans maintenant ---

{{%retrait1%}}maintenant

{{%retrait3%}}sans corps de mots

{{%retrait3%}}S.A.N.S. C. lueurs de lettres noires. A.

{{%retrait3%}}N.O. Anneau a plongé plus bas

{{%retrait3%}}plus vite

{{%retrait3%}}plomb ou cuivre.

{{%retrait3%}}Coulent dans l'opaque

{{%retrait3%}}la pesanteur et les liens. Eschyle.

{{%retrait3%}}Le sang des eaux mêmes

{{%retrait3%}}n'est pas un sang humain.

Les arcs de l'aurore

{{%retrait1%}}et les constellations invisibles --- obscurément me visitent

{{%retrait4%}}des silences sans images,

{{%retrait1%}}sans rester, elles regardent.

Ne te cherche pas de témoin.

Ne te cherche pas de foi frauduleuse.

L'inexistence est le cadeau que fait la vie.

Certains ont été promus rois et reines, déesses et dieux ;

c'est qu'ils ont dû tricher

à un jeu dont ils croyaient

connaître les règles.

L'Occident nous endort de règles incomprises.

 

Me vient comme un vif et bref coup de rasoir la pensée : et si je ne
ressentais rien ?

Et si je ne ressentais plus ; et si je n'avais rien éprouvé

qu'allusions timides à des formes de vies ?

{{%retrait2%}}(la signification de ---

{{%retrait4%}}l'inhabitation du monde ?)

 

Un peu plus haut, avant ces dernières paroles,

j'assistais au spectacle de deux adversaires ; diatomées

algues monocellulaires

armatures fines de silice, mais

comme si le mot disait

qu'elles se recoupaient après avoir été une première fois sectionnées.

Elles ne réclamaient rien de plus que de la lumière

mais elles s'étaient convaincues

et alliées

de combattre pour

une seule lumière

dont une et une seule aurait le triomphe et la garde.

 

{{%retrait3%}}Sans patrie, sans île ni découverte

{{%retrait3%}}la cartographie perd le rêve de méridiens

{{%retrait3%}}et de rhumbs. Les compas

{{%retrait3%}}s'ennuient de navigateurs

{{%retrait3%}}depuis longtemps disparus.

Pas de pas. Sapé par les fonds,

comment appelleras-tu cette danse

maintenant que tu es sans corps ?

Je l'appellerai. Mais comment ? Sans langue sans cerveau sans pied

de cadence, de sol et de levée.

Tu l'appelleras VERBE (des lointains profonds).

Répète. Je ne t'ai pas bien entendu.

{{%retrait4%}}DES LOINTAINS PROFONDS

où les lancinants fantômes

se parlent entre eux

sans s'obstruer d'horizons absurdes.

{{%retrait1%}}Un murmure continu des eaux mêmes

{{%retrait1%}}fait danser les dernières formes de vies --- qui

{{%retrait1%}}sont ironiquement des forces sans effets,

{{%retrait1%}}qui se perdent en silence

{{%retrait1%}}avant de recommencer,

{{%retrait1%}}inguérissables pourvoyeuses de lointains profonds.

Ce sont de pleins lointains que tu appelles "proches". C'est

l'absence de dimensions que tu appelles "vide", ou

"espace vide".

{{%retrait1%}}Non, tu scrutes encore l'obscurité

{{%retrait1%}}à la recherche d'une lumière fossile.

{{%retrait1%}}Il n'y a pas de paysage, il n'y en a plus

{{%retrait1%}}dans le noir de plus en plus noir

{{%retrait1%}}où tu n'as plus de corps

{{%retrait1%}}mais des limbes qui te défont sans douleur.

Les vibrations anonymes se multiplient

{{%retrait1%}}et traversent les verbes

{{%retrait2%}}qui en retiennent les semences des NOMS.

{{%retrait3%}}Ici encore ma voix s'abrège

{{%retrait3%}}en tant qu'elle n'est pas moi.

{{%retrait1%}}Je me laisse tomber, sans force

{{%retrait1%}}comme une bouche

{{%retrait1%}}sur son baiser.

# III. La Chambre

Est-ce une buée, un voile de

gouttelettes insonores --- un scellé

posé sur une porte (sur le mot "porte", ainsi perçu P.OR.T.E.ROR.TE).

Ma voix intérieure n'est pas mienne

mais un appel

qui vient de plus bas. Scellé bas.

Est-ce une porte ? Une porte qui grince ?

Une porte scellée sur une lumière seule.

{{%retrait1%}}Un instant de lueur nouvelle

{{%retrait1%}}et je retrouvai la souffrance d'avoir été.

{{%retrait2%}}Là, sous mes yeux métaphoriques

{{%retrait2%}}une épave comme un ventre de boule d'acier

{{%retrait2%}}à rivets, et soudée de main d'homme, aucun doute,

{{%retrait2%}}mais de quelles mains ? Et de quels hommes ?

De la buée sur des vitres épaisses cependant de consistance liquide ---

{{%retrait1%}}c'est une chambre insomniaque

{{%retrait1%}}quelque part, depuis toujours, un vaisseau coulé

{{%retrait1%}}dans une fosse sans repères ---

{{%retrait2%}}une cellule,

{{%retrait1%}}une grappe de minuscules points de lumière

{{%retrait1%}}et une vallée, et des bagues et d'autres joyaux pauvres en or.

{{%retrait1%}}L'anneau est tombé parmi les grappes, je ne sais

{{%retrait1%}}que voir et comprendre --- un panier de roses,

{{%retrait1%}}un bain dans un bain

{{%retrait1%}}et plus, des perles de rosée que l'immensité égare vite ---

{{%retrait1%}}l'anneau signifiait l'annulation ?

Fratrie de sons graves et lourds

{{%retrait1%}}qui survivent aux basses pressions

{{%retrait1%}}et aux températures.

Qui a voulu ce crâne accueillant,

{{%retrait1%}}de ces tempes de métal --- de ces hublots

{{%retrait1%}}qui marmonnent comme des pluies

{{%retrait1%}}chahutant la flaque ou les toits anciens.

Laissé pour mort, tu

l'es --- tu l'étais.

Mon œil fut noyé d'un baiser <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>sang

{{%retrait1%}}de lèvres, une fleur noire

{{%retrait2%}}parmi des jardins de fleurs noires, œil sans défense

{{%retrait4%}}parmi des roseraies aveugles.

Entre. Voici qu'il entre. Elle aussi.

Les roches aussi se sont inventé des histoires

et les gouffres ont trouvé des fabulistes habiles et efficaces. La
cellule :

{{%retrait1%}}est-ce encore ce que colportent les morts d'en haut ?

Cette chambre n'a rien d'une poche d'air ; j'ai vu

{{%retrait1%}}la forme et le métal forgé, soudé et riveté par coupes

{{%retrait1%}}comme si j'avais été forgeron et ferronnier. Crâne,

{{%retrait1%}}ai-je dit, tempes et épave sous-marine, et pourquoi ?

{{%retrait1%}}Change ta vision et regarde dedans par dedans,

{{%retrait1%}}et le dehors par le dehors ;

{{%retrait1%}}c'est membrane --- membrane

{{%retrait1%}}dans une membrane.

(Dans l'inopérable il laissait son paroxysme à une phrase, puis une
autre.

Comme si au détour d'une

tubérosité, d'un nœud, d'un cartilage obtus,

il guettait son ombre parfumée ; le soupçon

de n'avoir jamais été).

{{%retrait1%}}Membrane de fibres sur fibres, très fine

{{%retrait1%}}et très complètement illuminée.

{{%retrait1%}}Au-delà, des ombres pâles

{{%retrait1%}}commençaient à se faire deviner.

{{%retrait1%}}Du bout de son ongle il déchira la paroi.

# IV. S.A.M.

Des boucles de cheveux et des poils odorants ---

deux frères, ou bien une

ou deux sœurs

dans un lit endormi et des souffles.

Une chambre sur terre ? Je respire,

il est encore possible que l'air à la surface réveillée

{{%retrait1%}}encore possible, possibles : ces boucles de cheveux ? Au réveil ?

{{%retrait2%}}Je respire par les oreilles, et je me soigne par l'écoute.

 

Des rubans de senteurs

dont le rêve avait privé mes sens.

{{%retrait1%}}Je reviens des profondeurs,

{{%retrait3%}}et des lointains profonds.

Réveillé au musc caprin, réveillé --- ibex

{{%retrait1%}}dans l'implexité de la coriandre, du jasmin,

{{%retrait1%}}rose de Turquie, et au fond du lit

{{%retrait1%}}mousse de chêne, oud

{{%retrait1%}}et cuir, une peau humaine

{{%retrait1%}}un bois de santal ou de cachemire allusif.

L'amour est exact ; les sensations, imprécises.

Les visages sont absents

regardés de si près.

Le grain de la peau, les taches de rousseur

l'iris vert, hétérochromique. Plus près

le signe ↔ de ta pupille ;

tu es l'étoile du Cocher dans un troupeau impatient.

Tout ce qui aurait désespéré mes sens

qui imaginaient

{{%retrait2%}}imaginaient <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>vivre <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>la <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>descente
<span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>infinie<span style="word-break:break-all; line-break:anywhere;">.......................................................................................</span>

<span style="word-break:break-all; line-break:anywhere;">....................................</span>Elle béguète des vocales

{{%retrait4%}}pulsatiles insensées,

<span style="word-break:break-all; line-break:anywhere;">....................................</span>Sam à la surface du matin.
Qui a rêvé ces rêves ?

Qui a rêvé tes
rêves ?<span style="word-break:break-all; line-break:anywhere;">.......................................................................................</span>Souvent
plus vrais

{{%retrait4%}}par leur impression plus forte

{{%retrait4%}}que par leur véracité et le vraisemblable.

Je réponds à la fable réelle par la fable de ma mémoire.

{{%retrait1%}}Je reviens des profondeurs,

{{%retrait3%}}des profondeurs des lointains profonds.

{{%retrait2%}}Regarde-moi, dit-elle, le lainage gris de ma peau,

Sam à la surface du matin.

{{%retrait2%}}Tu ne rêves pas. Tu as trouvé l'habitacle ?

{{%retrait3%}}Ta chambre sous la mer ?

{{%retrait4%}}C'est ça ?

{{%retrait2%}}Tu as entendu les voix ?

{{%retrait2%}}Je ne sentais rien des parfums et d'autres senteurs.

{{%retrait2%}}Tu as vu les voix ? Les émanations

{{%retrait2%}}déroutantes sonorités. Et la chambre ?

{{%retrait2%}}Avait-elle une mâchoire et des dents ?

{{%retrait2%}}Non, je ne fus pas Jonas ingéré

{{%retrait2%}}par son propre esprit.

Je reviens des profondeurs,

{{%retrait3%}}et des lointains profonds.

Elle me caresse de l'épaule au garrot

des pointes de l'onglon et du pied.

{{%retrait2%}}Est-ce N.A.S.E.A.U.

{{%retrait2%}}Que tu as pris pour anneau ?

Elle chevrote, plaisante et rit maintenant.

{{%retrait1%}}Dehors, des balles de terre ou de crottin

{{%retrait1%}}tombent et roulent

{{%retrait1%}}à faible bruit.

 

Le lit des amants est de paille

et de racines fraîches.

Le fourrage attend, le fourrage vert

et le fourrage sec. <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>Ourania

{{%retrait4%}}m'a ramené sur la terre.

{{%retrait4%}}Je ne compte pas

{{%retrait4%}}les larmes

{{%retrait4%}}salées et pleines

{{%retrait4%}}de l'océan qui manque à la vie.

Un jour, pendant un rêve <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>ta peau

{{%retrait1%}}elle aussi sera écrite <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>comme à Pergame

{{%retrait2%}}de ces mots : une chambre sous la mer.
