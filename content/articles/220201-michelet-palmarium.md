---
persona:
  - Étienne Michelet
title: "Palmarium"
slug: "palmarium"
date: 2022-02-01
echo:
  - rythmique
images:
  - "img/220201-michelet-palmarium.jpg"
gridimages:
  - "img/220201-michelet-palmarium-grid.jpg"
sourceimage: "Étienne Michelet"
notes: ""
summary: "DÉLIRE TROUBLE, qui va lire-lire ? On s'en tamponne, bouclier, boulier, on s'éclate le front, le cône dans la jungle à coups de sarbacane... Coupe-coupe, rendons grâce aux bêtes, quadrupédies suspendues aux crochets du camion frigorifique, le roulis, la danse du cobra drogué au kamagra, les chauves-souris papillons, épinglées, roulées dans la farine animale saupoudrée dans les mangeoires des génisses génitrices, les carcasses brûlées une fois la peau tannée."
citation: "avec un sourire de sang"
---

DÉLIRE TROUBLE, qui va lire-lire ? On s'en tamponne, bouclier, boulier,
on s'éclate le front, le cône dans la jungle à coups de sarbacane...
Coupe-coupe, rendons grâce aux bêtes, quadrupédies suspendues aux
crochets du camion frigorifique, le roulis, la danse du cobra drogué au
kamagra, les chauves-souris papillons, épinglées, roulées dans la farine
animale saupoudrée dans les mangeoires des génisses génitrices, les
carcasses brûlées une fois la peau tannée.

Cuisinez, mijotez les animaux, les métamorphoses ovidiennes en soupière
pied-de-poule, le confit de canard en sauce turquoise. Vole qui
pourra... l'agriculture raisonnée des bétails... la chasse à l'ourse
dans les Pyrénées... Bétail endolori, les mauvais réveils à l'aube, de
carotide en carotide, saignées, la précision de l'incision hémorragique,
le lyrisme instantané après mort rituelle. Cuisinons les animaux servis
sauce silicone dans des plateaux à cornes, des soupes d'oiseau spatule,
le bec taillé en cuillère, les couverts d'ivoire, la dent de narval en
guise de cure-dent. Les amoureux se zieutent, mastiquant la viande
nappée-grillée avec un sourire de sang. L'amant fébrile pense : ce soir,
je te fais la même, je te cuis le vagin, je te le truffe de truffes, de
clous de girofle, ma giroflée, ma femme girafe. Pendant ce temps, les
renards, eux, défèquent dans les pièges, la braconnière, elle, est une
conne. Le braconnier minutieux prépare la glu, avant de
l'enfiler-dilater... ce lyrisme ! Coquins-pinsons à l'agonie sur la
branche avant de rentrer au nid. Le chien *jindo* attaché à la niche, un
mètre de corde, lyrisme ! Pendant que l'on dorlote les caniches, lyrisme
! Mords-les *Spitz*, pas de quartier aux races domestiquées, fais-leur
ravaler leur carnation... Le *Nureongi* derrière les barreaux, chair
affable aux yeux cerclés de noir, un air de caramel jaune...

Les hommes, les femmes lugubres, ces poisons du climat cuisinent à tour
de bras une jument aux anchois, portent des chaussures en cuir de crabe
ou des colliers en dents de loutre. Pendant ce temps, l'angor
glougloute, c'est l'euphorie, l'acide lactique à pleins gaz, leur
langue-viande liée au cervelet. Ils sont gavés aux hormones malignes,
aux champignons gris. Ils sirotent l'ayahuasca à la paille en faisant
des bulles dans le liquide, se prennent pour des gorilles. Ils pleurent
sous les effets des hallucinations colorées, s'aiment comme des
insectes, copulent comme des coccinelles. Le chaman se lève, il a la
gerbe, il a bu trop de café la veille. Il vomit sur les adeptes, sur les
vacanciers du séminaire... Bile polychrome du grand loup-gourou sur vos
blancs habits, mes bons amis, ceci est pour vous, alors priez pour
nous...

Alors quoi ? La drogue ? Démultiplication du moi-moi-moi, on a remué la
matière du rêve, un tourbillon, nous disions... une farce plutôt... Et
si nous donnons une réponse, et si nous parlons... nous brisons le
silence, nous amplifions alors le mal de l'époque. Alors ?... Rien... Et
vous, que mettez-vous dans votre cervelle ? Du poème ? de la littérature
? ou plutôt de la vidéo ? du piano ? du dodécaphonisme ? de la
postmodernité, des plateformes ? des contes ? peut-être, ou de la
politique rance, du débat glacial, faisandé... Du complotisme composite,
ou des grimaces, des rougeurs cutanées ? Qu'y mettez-vous alors ? De la
lumière ? du silence ? des muqueuses ? Nous devrions aimer la poésie des
mangeurs de vertèbres, des penseurs de glaires ? Voici l'intellectuel
nourri à la viande d'ourse... Ils, elles glaviotent. L'actualité est
sans cesse réactualisée, le vacarme suit sa ligne. Dodo mon gros, et
flatule dans ton lit. On voudrait les assommer ces faces de vieux
lubriques, leurs rides qui débordent de squames. Il ne veut donc pas la
boucler, ce vieux, avec sa face de gargouille rose ? La lame découpe des
morceaux de chair, élève des monceaux de chair animale pour les griller
en comité d'abrutis malades. La fumée diaprée des festins, la fureur de
la déglutition, des glandes salivaires. Les cerfs métamorphosés en
décorations murales, en porte-manteaux. La taxidermie cocotte, voici des
flûtes en os de tortue, en ivoire, des rostres, les cordes en boyaux
félins pour l'orchestre de la mort, les vertèbres marimba, le
synthétiseur de l'anesthésie. Ils cherchent le Salut, mais qui voudrait
sauver ce qui pue ? Les ténèbres, c'est l'humain dans ses mauvais jours,
la tête dans le trou anal. Comme c'est banal. Faisons du poème, de la
rime bâtarde à genoux pour le coït, la nouvelle femme, les nouveaux
porcs, des colliers de perles de nacre à s'étrangler. On suce des
huîtres, on pratique le cunni à l'envers à la mer. Maudite
ostréiculture, culte papaïen, la messe est dite, orgue de barbarie, le
pasteur dominical prêche aux platines, sonnez les matines, le pécheur
pêche aux plates-bandes, puisqu'il bande.

La baise est courte, la ficelle est un peu grosse, elle *fait
ficelle*. C'est tordu comme plan, plan à trois, à quatre, pour les
valeurs du partage, les plans cul matchent. Face de violette, n'oublie
pas de mettre ta voilette. Coup tordu, une louche de
gamma-hydroxybutyrate. Aiguisez la lame comme un coupe-queue, on en
coupe une, sept repoussent, comme le renard à sept queues, ils bandent
sur les brancards, à tour de bras. Les ministres-porcs chassent la
croupe, l'escorte morte sur les quais de Montevideo, pendant que les
mouettes dans le ciel chient sur leur costume. Évidemment, les oiseaux
marins n'en ont rien à foutre de leurs caprices de riches, des rires
obèses, des parties carrées, du Marché du Périnée. Le devoir d'écrire
n'existe que dans un cône tordu. C'est un bordel qui sent la fesse, la
ration cocktailisée. La poisse bulle, bulle, bulle... pendant ce
temps... l'orang-outan, le paisible orang-outan est en
danger-critique-d'extinction, non pas à cause d'une maladie quelconque,
nerveuse, mais à cause de la déforestation, de l'exploitation minière.
Oui, tes grimaces on s'en cogne. N'en parlons plus de toi, de tes
grimaces fongiques, de tes yeux lugubres comme deux fosses septiques. La
forêt est coupée pour les palmes. On ne va pas jouer aux apprentis
chamans, aux exilés de la carte. Qui va lire-lire jusqu'ici ? Le
spectacle est vivant ! La culture de l'huile palmiste, les orangs-outans
qui dansent, décrochés de force des branches. *Homme de la forêt*,
l'homme te le rend bien. La honte est unanimement humaine. C'est nous
qui descendons du singe, pas l'inverse. Nous, ils, elles, ielles, sommes
sans ciels et nous sommes les singes de la ville qui crache son cancer
sur la forêt.

On te met la conque, la viande crue dans la bouche, les gâteaux
gélatineux décorés aux poils de mustélidé, l'hermine des neiges dans
l'assiette, alors plutôt mourir que de se souiller. Anne de Bretagne
saute dans la mare. Le *black serval cat* broie du noir. Les toxicos de
la tourbe s'enfoncent des aiguilles dans les paupières. C'est un jeu
d'enfant de s'y casser les dents, sur des os de chameau, sur des os de
chamois. Leurs dents font de la mousse, du coton vahiné. Tout est
transparent, débilitant, la technologie enfume les tempes. Plus de
secret, de buisson ardent, d'animaux sauvages. On vénère la langue du
boucher, le Marché de la mort, et les objets du kyste. Il sort de leurs
yeux débiles une soudaine hystérie, comme une rage de dents. Les poètes
mâles déballent leur sensiblerie, agitent leurs bras fragiles,
cotonneux, font feu de tout bois, de leurs râteaux sentimentaux, d'une
voix prépubère passée la quarantaine... foutus poètes aux jambes
arquées, un chapeau de croque-mitaine et la cabale de leur sexe mou...
Le kaléidoscope est un mensonge des nerfs.

Allons au *Palmarium*, dans une serre *Walipini,* une serre pour avoir
la paix, pour que l'on nous laisse en paix... au milieu des parfums, des
plantes muettes, de leur vibration haute, contenue dans la structure.
Triple chaleur dans le cœur. Plantes fugueuses, les variations des
feuilles, le contrepoint chlorophyllien. Un doux refuge pour le
non-producteur, le grand buveur d'infusions d'agave sirotant ses
décoctions de cactus. Parmi les piliers d'écorces filamenteuses se
dresse très haut la colonne osseuse des rêves. Ses vertèbres comme une
autre plante de la serre *Walipini*. C'est la colonne spirituelle... Les
deux serpents *Kundalini* en forment la colonne autour des vertèbres.

*la métaphysique n'est qu'un moyen*\
*la métaphysique n'est qu'une lame*\
*LE SERPENT N'A PAS DE PAUPIÈRE*\
*la langue ne dit rien*\
*sinon l'énergie de la langue*
