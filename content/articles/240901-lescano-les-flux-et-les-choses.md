---
persona:
  - Alfredo Lescano
title: "Les Flux et les Choses"
slug: "les-flux-et-les-choses"
date: 2024-09-19
echo:
  - esthétique
images:
  - "img/240901-lescano-les-flux-et-les-choses.jpg"
gridimages:
  - "img/240901-lescano-les-flux-et-les-choses-grid.jpg"
sourceimage: "https://commons.wikimedia.org/wiki/File:GISP2D1837_crop.jpg"
notes: ""
summary: "Certains mouvements se constituent en évitant toute figure fondatrice ainsi que tout porte-parolat attitré. Ce sont ceux qui, plutôt que de définir des lignes à suivre, par des interventions qui n'ont un appui que transitoire, estompent leurs propres bords. Les individus qui y participent sont plus le support temporaire d'un flux qui les dépasse que des membres identifiés d'une structure socialement reconnue. Se dérobant, avec plus ou moins de succès, aux institutions, ils s'introduisent volontiers dans les interstices et les fissures du social --- qui se déclare, de ce fait, et avec raison, en danger."
citation: "Le flux, lui, fait peur."
poetry: false
hangindent: false
antilivre: "/etc/240901-lescano-les-flux-et-les-choses.html"
noblink: true
affiche: true
---

 

{{%epigraphe%}}

Texte plutôt solide et subalterne à propos de la recherche d'une
certaine liquidité et de quelque autonomie.

{{%/epigraphe%}}

 

Certains mouvements se constituent en évitant toute figure fondatrice
ainsi que tout porte-parolat attitré. Ce sont ceux qui, plutôt que de
définir des lignes à suivre, par des interventions qui n'ont un appui
que transitoire, estompent leurs propres bords. Les individus qui y
participent sont plus le support temporaire d'un flux qui les dépasse
que des membres identifiés d'une structure socialement reconnue. Se
dérobant, avec plus ou moins de succès, aux institutions, ils
s'introduisent volontiers dans les interstices et les fissures du social
--- qui se déclare, de ce fait, et avec raison, en danger. Ces collectifs
sans forme exercent ainsi la force pressante des liquides, ce qui les
rend d'ailleurs insaisissables, à moins qu'on leur applique, de
l'extérieur ou de l'intérieur, une forme contenante qui fixe, malgré
eux, leur mouvement.

Pendant leur déploiement, ces collectifs, ces liquidités, produisent,
avant, pendant et après leurs actions, des textes (des affiches, des
livres, des pamphlets, des sites Internet, des émissions radio, des
nouvelles du front, des comptes rendus à la destination des
sympathisants, des plans d'action pour la base rapprochée de militants,
des discussions en assemblée, des slogans, des mots d'ordre, des poèmes,
des chansons...).

Or tout texte, en tant que nécessaire concrétisation renouvelée d'un
dispositif, impose lui-même la forme des rapports qu'entretiennent
celles et ceux qui, à travers lui, se regardent (qu'ils le fassent comme
auteurs ou comme récepteurs) --- c'est ce qu'on peut appeler la violence
formelle du texte. Tout le problème est que les dispositifs textuels
connus ne savent générer que les rapports sociaux qu'ont chosifiés les
institutions qui les ont engendrés. Il est par conséquent nécessaire de
penser des formes de textualité qui soient des corrélats acceptables de
l'expérience de la fluidification du collectif.

 

 

Il n'y a pas le temps.

 

 

Un texte autonome est un texte dont la source n'a pas d'importance, mais
qui s'associe pour cette raison précisément à une forme non pas
individuelle mais multiple ou, mieux, collective. Cette forme n'est donc
pas vraiment une origine, car le texte autonome ne se déploie ni à
partir d'un point qui lui est extérieur ni grâce à une voix qui le
profère ; le texte autonome ne se prétend pas l'expression d'une
subjectivité énonçante.

Un texte autonome n'est pas un texte anonyme, car le texte anonyme se
présente comme associé à un manque, alors que le texte autonome se
présente comme produisant de lui-même les modes d'action qui vont
caractériser l'entité individuelle ou collective à laquelle
indéfectiblement on va l'associer. Le texte autonome est insolent, s'il
lui arrive, contre son gré, d'être signé, il dit "me voilà et voici mon
excroissance, ma malformation : mon signataire".

 

 

Être à l'affût.

 

 

Le texte autonome s'oppose au texte subalterne. Celui-ci dépend d'une
source qui donne au texte, par la concentration de significations dans
le nom propre (la biographie de l'auteur, son statut, ses filiations,
ses prises de position), une fixation dans l'histoire des institutions
et des jeux intersubjectifs.

Un texte liquide est un texte toujours inachevé. Mais inachevé non pas
par les aléas de la vie ou de la mort : intrinsèquement inachevé. Un
texte reposant sur le présupposé de l'impossibilité de sa propre
fixation. Un texte liquide est, par conséquent, et par définition, en
cours de construction. Au moment où on le déclare fini, il se solidifie.

Un texte liquide n'est pas daté, n'a pas d'histoire : c'est le résultat
précaire d'interventions multiples. Et peut-être même invisibles, car
l'intervention en elle-même ne compte pas vraiment (à quel moment cette
modification a-t-elle été produite, par qui, pourquoi ?) --- un texte
liquide est réfractaire à sa propre archéologie, n'offre aucune prise à
son étude génétique. Un texte liquide n'est pas parcouru : on s'insère
dans ses mouvements de flux et de reflux, ses profondeurs et sa surface.

 

 

Le texte liquide idéal, bien entendu, est autonome.

 

 

Un texte liquide est corrélatif d'un flux sémantique. On peut dire que
le sens fait flux lorsqu'il est pris dans des processus de
confrontation, ce qui le rend instable. Fixer un flux lui donnera sa
forme, mais ce sera toujours une contention et un acheminement de la
force qu'il porte, d'où le fait que le flux n'ait pas de support
privilégié (tout lui convient, mais tout lui convient imparfaitement).
La nature du flux sémantique est paradoxale : il n'aura de possibilité
d'être saisi qu'à condition qu'on le fixe, malgré lui, car le flux
appelle toujours à son propre débordement. Le flux sémantique s'oppose
au sens figé, au sens devenu chose.

 

 

Travaillons comme si nous étions éternels.

 

 

Les institutions "langue française", "langue castillane",
"langue..." sont des dispositifs de combat. Mais leur rôle n'est pas de
défendre, de maintenir ou de renforcer des États-nations. Elles font la
police des flux au nom de la chose.

Le texte-racine est parcouru de bas en haut, de haut en bas. Le
texte-rhizome est tentaculaire, il ne prévoit aucune direction pour son
parcours. Le métro parisien est en ce sens un texte-rhizome. Un
texte-flux n'est pas parcouru : il est fait de mouvements qui nous
sautent dessus.

Un texte solide est rattaché à un sens devenu chose. Les textes solides
émergent en tant qu'incorporés, c'est-à-dire associés au corps d'un
individu qui l'aurait produit. Même là où le texte ne se donne pas à
voir explicitement comme le geste effectué par un corps, on cherchera
systématiquement la signature cachée : il nous est insupportable de ne
pas reconnaître le corps qui nous parle, d'être confrontés à une parole
autonome.

 

 

Oui, on manque de préparation. Et en même temps nous nous préparons depuis des millénaires.

 

 

Un texte stratifié est un texte constitué d'une multiplicité de strates
ou couches superposées. Chaque strate d'un texte concrétise d'une
manière singulière des éléments des autres. Les textes stratifiés
s'opposent aux textes plats, dans lesquels tout est disposé de sorte à
éviter l'émergence de niveaux différents : ordonnances, modes d'emploi,
appels d'offres, réglementations.

Les rapports entre les strates d'un texte non plat sont variables. Une
strate peut tirer des fils que l'autre suggère, se concentrant sur ce
que l'autre laisse en filigrane. Faire émerger ce que l'autre cache,
donnant à voir ses angles morts. Les strates peuvent fonctionner en
parallèle jusqu'au moment où ils se rencontrent. Ou bien commencer d'un
point commun pour, progressivement, diverger.

Le nombre de strates n'est déterminé que par la nécessité (contingente)
de déployer des sous-textes parallèles : mutuellement interdépendants,
mais comportant des degrés variables d'une relative autonomie. La
lecture d'un texte stratifié n'est pas commandée strictement par la
matérialité langagière, mais elle n'est pas non plus laissée au libre
arbitre. Elle consiste à agir sur des surfaces qui sont elles-mêmes
agissantes. À agir, non pas au sens de la théorie sémiotique, pour
construire une interprétation personnelle, mais au sens presque physique
du terme : prendre les surfaces une à une pour les mettre en rapport les
unes avec les autres, établir des points de contact, frotter une surface
contre l'autre. Combiner les surfaces en une seule entité et faire
émerger leur action commune.

 

 

Faire la liste des apports à une pratique réelle du flux.

 

 

Les types de textes sont des dispositifs, et comme tout dispositif, ils
préfigurent matériellement des manières d'affecter et d'être affecté.
Pense aux relations interaffectantes préfigurées dans une sentence
judiciaire, dans un avis de recherche, dans une lettre d'amour.

Les milliards de textes subalternes, solides et plats qui donnent forme
à nos relations (et donc à nous-mêmes) nous rendent subalternes, solides
et plats. Autrement dit, en lisant, en écrivant, en parlant, en
écoutant, nous devenons fatalement des choses.

Les infos crient au danger : l'intelligence artificielle dominera le
monde. Son pouvoir secret ? Produire, grâce à des algorithmes, des
textes qui ressemblent comme deux gouttes d'eau à ceux que nous écrivons
de nos petites mains. Mais la vraie réussite du pouvoir chosifiant est
autre, et elle est bien plus vaste : c'est de nous avoir modelés depuis
des siècles pour qu'on parvienne au point que les textes que nous
produisons soient reproductibles à l'identique par des algorithmes. Nous
avons donc désormais la preuve irréfutable que l'intelligence
artificielle, nous l'incarnions déjà quotidiennement à l'école, au
travail, au cabinet médical, dans nos programmes politiques, dans nos
relations amoureuses et dans nos articles scientifiques. La fascination
que le dispositif produit chez certains et le dégoût qu'il provoque chez
d'autres ne sont pas sans rappeler les sentiments que provoqua cette
autre confrontation soudaine à une image monstrueusement vraisemblable
de nous-mêmes que fut l'irruption de l'inconscient freudien.

 

 

Avoir une morale guerrière --- c'est-à-dire sélective.

 

 

L'expérience d'un collectif en train d'avoir lieu est irréductible à la
somme des expériences individuelles. Si ce collectif aspire à "se dire",
à exister dans le langage, il lui faut un type de textualité qui lui
corresponde. Or la production textuelle a été dominée par les
institutions, donc constituée pour rendre productives des forces
chosifiantes. Prends la science : son rôle n'est pas de décrire les
choses du monde, mais de capter des flux pour les transformer en choses
(quelle serait une méthode scientifique qui allierait la production de
textes liquides, autonomes et multistratifiés à la prolifération de flux
sémantiques ?). L'écriture elle-même apparaît comme concrétisant dans
chaque mot, dans chaque phrase, une nouvelle emprise de la chosification
sur l'expérience. Tout se passe comme si en parlant ou en écrivant,
telle la jeune fille ensorcelée qui ne peut que cracher des diamants, on
n'avait d'autre choix que de vomir des choses.

Produire des choses est rassurant. Mais comme tout ce qui rassure, la
chose ment. Car elle expulse en dehors de ses limites, d'un seul et même
geste, l'insubordination, la pluralité et le mouvement. Le flux, lui,
fait peur.

 

 

Comment faire parler les flux sans les chosifier ?

 

 

S'il faut agir maintenant, alors on ne va pas se soucier des détails.
Faire flux se fait avec les moyens du bord. Certains avancent, d'autres
reculent, d'autres volent sans être vus, d'autres sont pris en flagrant
délit : quand il s'agit de battre les choses en brèche, la règle
consiste à aller vite, tout faire sauf attendre. Il faut y aller à la
hache, d'autres affineront plus tard. Si on trouve, on prend sans
demander. Fini la politesse, l'inviolabilité des héritages et les
limites des écoles de pensée. Nous ne savons pas encore ce que nous
pouvons.

 

 

Cours.

 

 

Même si des commentateurs s'acharnent souvent à trouver les individus
ayant participé au collectif ayant produit un texte autonome, même si
les individus eux-mêmes dévoilaient leur participation à l'écriture des
manuscrits, les textes autonomes, eux, se présentent comme surgissant
d'eux-mêmes, comme produisant d'eux-mêmes le collectif qu'ils sécrètent,
qui est au demeurant nommé ici "collectif" non pas par opposition à
"individu" mais plutôt en référence à une pure forme dont la substance
est la somme d'actions portées par le texte et dont l'identité des êtres
qui l'occupent est anecdotique. Ou encore : parmi les actions portées
par le texte on trouve l'engendrement d'une forme-signataire, c'est le
texte au bout du compte qui produit l'agencement collectif
d'énonciation. Certaines productions qualifiées habituellement
d'anonymes sont en réalité autonomes (les récits populaires transmis
oralement). Lascaux est autonome, liquide et stratifié : des strates
intrinsèquement inachevées (on pourrait les continuer à l'infini) ne
s'associant ni à une source ni à une voix et se rencontrant d'une
manière singulière pour déployer une force propre à leur articulation.
Lascaux est corrélatif d'un flux sémantique. Les cahiers de doléances de
1789 : textes autonomes (face à l'un de ces cahiers, peut-on se demander
vraiment qui l'a "fait" ? cette question n'a simplement aucun sens),
liquides (voués à être continués *ad libitum*), stratifiés (les
exigences diverses s'interconnectent ; connecter les cahiers entre eux
produit de nouvelles instances de sens) : les cahiers de doléances font
flux.

La maîtrise de la liquidité textuelle et de la fluidité du sens exige
des efforts de coopération (celui qui s'affranchit des choses en
solitude accomplit un type spécifique d'échec du flux, et en cela, en
dernière instance, sert la cause des choses), de composition et
d'appropriation démesurés. Cette démesure rend la tâche apte à
dénaturaliser les processus d'action collective disponibles. Autrement
dit, la confrontation au risque et au hasard que comporte la production
nécessairement collective d'un texte liquide devrait affaiblir, au moins
temporairement, les mécanismes par lesquels nous répétons encore et
encore des patrons de sociabilisation.

 

 

Un jour j'ai rêvé que je fluais et j'ai ressenti un bonheur violent presque vertigineux.

 

 

Non, le réel n'est pas un texte à interpréter. Mais les textes ne sont
pas là non plus pour être interprétés. Faire flux, c'est prendre acte de
l'illusion du signe, car le but, ce n'est pas d'être compris, mais de
déchaîner des variations.

 

 

Quel est le diagnostic ? C'est simple. Regarde-toi. Te reconnais-tu ?

 

 

La question qui nous est imposée est donc s'il est possible de rester
dans le texte, voire dans le langage --- nous y semblons tout de même
condamnés ---, et passer d'une (re)production de choses à la création de
flux, qu'il nous revient d'explorer en même temps qu'on les engendre,
comme un nouveau-né qui découvrirait le monde au fur et à mesure qu'il
le construit de toutes pièces avec ses propres mains. Sauf que le monde
n'est pas une page blanche, qu'il est saturé de choses et que nos mains
sont elles-mêmes des choses ne comprenant que le code de la
chosification. Toute la difficulté est qu'il faut à la fois déchosifier
l'outil, le support, la matière et le but, et que chacune de ces
instances comporte son propre principe de résistance à sa
déchosification, c'est-à-dire à sa mise à mort. Une fois parue, toute
chose tend, par elle-même, à persévérer dans son être : la chose sait
qui elle est et entend le rester. Le flux est incapable de décliner
honnêtement sa propre identité.

Tout cela a été dit et redit. Mais il faut toujours et encore tout
recommencer. Les interstices ne s'ouvrent qu'à force d'insistance. Pour
faire quoi ? La question est mal posée. Il semble juste impensable de
rester immobile face à l'avancée implacable des choses. C'est un appel à
une recherche conceptuelle et à une expérimentation formelle, qui ne
seraient que les deux faces d'une même démarche d'investigation. D'un
côté, une recherche qui porte ses efforts sur les aspects ontologiques
et les implications politiques des problèmes ici ébauchés. De l'autre,
une expérimentation méthodique de modes d'autonomisation, de liquidité
et de stratification textuelle et, partant, de déchosification du sens.
