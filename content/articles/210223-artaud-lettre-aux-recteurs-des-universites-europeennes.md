---
persona:
  - Antonin Artaud
title: "Lettre aux Recteurs des Universités Européennes"
slug: "lettre-aux-recteurs-des-universites-europeennes"
date: 2021-02-23
echo:
  - stratégique
images:
  - "img/210223-artaud-lettre-aux-recteurs-des-universites-europeennes.jpg"
gridimages:
  - "img/210223-artaud-lettre-aux-recteurs-des-universites-europeennes-grid.jpg"
sourceimage: "https://commons.wikimedia.org/wiki/File:010891_R435-006_MITIN_ESTUDIANTES._MULTITUDES_NOCHE_AGOSTO_27_1968_(32655689204).jpg"
notes: "Ce texte d’Antonin Artaud --- bien que Michel Leiris semble avoir apporté quelques ajustements en son début, la paternité de ce texte, en son esprit et sa totalité, paraît bien être celle d’Antonin Artaud selon les analyses de Louis Aragon --- a paru dans le numéro 3 de *La Révolution surréaliste*, le 15 avril 1925."
summary: "Dans la citerne étroite que vous appelez \"Pensée\", les rayons spirituels pourrissent comme de la paille. Assez de jeu de langue, d’artifices de syntaxe, de jongleries de formules, il y a à trouver maintenant la grande Loi du cœur, la Loi qui ne soit pas une loi, une prison, mais un guide pour l’Esprit perdu dans son propre labyrinthe. Plus loin que ce que la science pourra jamais toucher, là où les faisceaux de la raison se brisent contre les nuages, ce labyrinthe existe, point central où convergent toutes les forces de l’être, les ultimes nervures de l’Esprit."
citation: "Au nom même de votre logique, nous vous disons : La vie pue, Messieurs."
---



<span style="margin-left: 10%; display: inline-block;">Monsieur le Recteur,</span>

Dans la citerne étroite que vous appelez "Pensée", les rayons spirituels pourrissent comme de la paille.

Assez de jeu de langue, d’artifices de syntaxe, de jongleries de formules, il y a à trouver maintenant la grande Loi du cœur, la Loi qui ne soit pas une loi, une prison, mais un guide pour l’Esprit perdu dans son propre labyrinthe. Plus loin que ce que la science pourra jamais toucher, là où les faisceaux de la raison se brisent contre les nuages, ce labyrinthe existe, point central où convergent toutes les forces de l’être, les ultimes nervures de l’Esprit. Dans ce dédale de murailles mouvantes et toujours déplacées, hors de toutes formes connues de pensée, notre Esprit se meut, épiant ses mouvements les plus secrets et spontanés, ceux qui ont un caractère de révélation, cet air venu d’ailleurs, tombé du ciel.

Mais la race des prophètes s’est éteinte. L’Europe se cristallise, se momifie lentement sous les bandelettes de ses frontières, de ses usines, de ses tribunaux, de ses universités. L’Esprit gelé craque entre les ais minéraux qui se resserrent sur lui. La faute en est à vos systèmes moisis, à votre logique de 2 et 2 font 4, la faute en est à vous, Recteurs, pris au filet des syllogismes. Vous fabriquez des ingénieurs, des magistrats, des médecins à qui échappent les vrais mystères du corps, les lois cosmiques de l’être, de faux savants aveugles dans l’outre-terre, des philosophes qui prétendent à reconstruire l’Esprit. Le plus petit acte de création spontanée est un monde plus complexe et plus révélateur qu’une quelconque métaphysique.

Laissez-nous donc, Messieurs, vous n’êtes que des usurpateurs. De quel droit prétendez-vous canaliser l’intelligence, décerner des brevets d’Esprit ?

Vous ne savez rien de l’Esprit, vous ignorez ses ramifications les plus cachées et les plus essentielles, ces empreintes fossiles si proches des sources de nous-même, ces traces que nous parvenons parfois à relever sur les gisements les plus obscurs de nos cerveaux.

Au nom même de votre logique, nous vous disons : La vie pue, Messieurs. Regardez un instant vos faces, considérez vos produits. À travers le crible de vos diplômes, passe une jeunesse efflanquée, perdue. Vous êtes la plaie d’un monde, Messieurs, et c’est tant mieux pour ce monde, mais qu’il se pense un peu moins à la tête de l’humanité.
