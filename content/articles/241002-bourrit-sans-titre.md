---
persona:
  - Bernard Bourrit
title: "Sans titre"
slug: "sans-titre"
date: 2024-10-09
echo:
  - rythmique
  - ontique
images:
  - "img/241002-bourrit-sans-titre.jpg"
gridimages:
  - "img/241002-bourrit-sans-titre-grid.jpg"
sourceimage: "Bernard Bourrit"
notes: "Les photographies sont de Bernard Bourrit."
summary: "en feuilletant l'herbier d'Anaïs Tondeur, Tchernobyl herbarium, composé de radiogrammes de plantes cueillies à l'intérieur de la zone d'exclusion de la centrale — ces images très troublantes parce qu'elles ne sont pas des photographies, mais comme du temps de Nicéphore Niépce, des images de contact, agissant dans le cas présent par l'action des isotopes radioactifs assimilés au sein de la plante qui, par la force des événements, a absorbé et restitue visiblement (cela fait sens) le contaminant radioactif déposé dans le sol, faisant voir par sa propre lumière interne le devenir de la catastrophe."
citation: "le regard vide devant ce chaos de sens et de non-sens"
poetry: false
hangindent: false
---

en feuilletant l'herbier d'Anaïs Tondeur, *Tchernobyl herbarium*,
composé de radiogrammes de plantes cueillies à l'intérieur de la zone
d'exclusion de la centrale --- ces images très troublantes parce qu'elles
ne sont pas des photographies, mais comme du temps de Nicéphore Niépce,
des images de contact, agissant dans le cas présent par l'action des
isotopes radioactifs assimilés au sein de la plante qui, par la force
des événements, a absorbé et restitue *visiblement* (cela fait sens) le
contaminant radioactif déposé dans le sol, faisant voir par sa propre
lumière interne le devenir de la catastrophe.

en feuilletant ce livre halluciné (--- où cette autre lumière qu'est la
radioactivité permet de révéler la chair mutée des organismes, à la
manière de ces images médicales que les médecins osent appeler
"images" alors qu'elles ne sont que le pauvre calcul d'un algorithme
de reconstitution)

il y a dans ce livre que je feuilletais une proposition, une définition
peut-être, de ce qui constitue "en propre" un événement, --- et Michael
Marder, le philosophe, dit à peu près ceci : l'événement "c'est ce qui
a lieu comme s'il n'avait pas lieu", et tandis que refermant pour un
instant les empreintes scintillantes des plantes contaminées formant un
florilège d'images uniques ayant la puissance affective des reliques,
tandis que je fermai le livre, la phrase de Marder insistait et ne me
quittait pas malgré ---

malgré le procédé dialectique dont elle use, consistant à nier l'essence
de la locution verbale antéposée pour faire miroiter une formule obscure
et agrandie, malgré cela, malgré, disons un certain scepticisme quant au
langage, j'emportai avec moi cette proposition, je la semai dans mon
esprit, je l'enfouis, et puis naturellement, très vite je l'oubliai.

![](/img/plus/sans-titre/06.jpg)

et soudain l'aventure ou l'étrange tentative d'herbier que Rousseau
constitua dans sa fuite me revint en mémoire, Jean-Jacques persécuté par
la vindicte et l'humaine méchanceté allant chercher davantage de
solitude en l'île Saint-Pierre, davantage de solitude en s'ouvrant à la
compagnie des herbes.

l'herbier de Rousseau n'était rien de moins que l'amorce du recensement
exhaustif de la flore *petri insularis*, c'est-à-dire, selon une logique
propre à tout herbier, l'examen à des fins classificatoires d'individus
déracinés ou sectionnés, hautement décontextualisés et acquérant par
voie d'exemplarité la signification de se montrer conciliant avec le
goût de l'ordre qui à tout veut une place, ainsi l'herbier passe-t-il la
spontanéité foisonnante d'une île au crible de la méthode.

et tandis donc
que je songeais à l'effort de systématisation qui hante l'esprit
occidental, à cette pulsion catégorique de la raison rationnelle --- et
tandis que j'y songeais, j'arpentais un de ces trottoirs inutiles entre
ville et campagne qui ne mènent nulle part ou vous raccompagnent à votre
point de départ, observant les herbes perçant la croûte d'asphalte, si
dignes et pourtant si dépourvues de personnalité qu'on ne les voit pas.

![](/img/plus/sans-titre/01.jpg)

des plantes auxquelles la nomenclature dérivée du système de Linné et
désormais universellement adoptée donne volontiers l'épithète spécifique
de *communis, simplex, vulgaris,* ou encore *trivialis*, le choix de
l'adjectif signalant (si l'on n'avait pas compris) que jamais aucun
classement, description, ni système de signes ne sont neutres --- et
penché dessus je tentai de les regarder.

à la vérité, je ne pensai nullement à Rousseau ni à Linné, j'étais
seulement sorti poussé par l'angoisse vespérale, le serrement qui
m'étreint chaque fois que la lumière disparaît ainsi qu'elle étreint,
paraît-il, les nouveau-nés, eux qui sont encore dans l'ignorance de
l'alternance du jour et de la nuit, et mû par cette inquiétude
familière, je tournai mes pas dehors fuyant ---

fuyant une
humanité dont le bruit m'accable parce qu'il indique toujours un
intolérable surcroît de présence --- sorti et passant devant ce vaste
champ inculte, riche en pierres et abandonné à la jachère ou aux
moutons, ce champ portait, à la lisière du crépuscule, montées sur de
robustes tiges une mer d'assiettes blanches, comme d'étranges coupelles
oscillantes et piquée (la mer) ça et là d'épis bruns, charnus, d'une
consistance floconneuse, presque farineuse, et sous un ciel vide, à la
fois bleu et très noir ---

![](/img/plus/sans-titre/02.jpg)

cela faisait une scène irréelle --- je veux dire que la radicale altérité
de cette scène apparaissait avec une vivacité singulièrement frappante
et inattendue --- altérité de formes, altérité de buts --- et
vraisemblablement j'ai dû me dire quelque chose comme : que ce qui avait
lieu devant moi, et que ne voyaient pas les automobilistes hagards,
piégés dans leurs propres phares comme dans leurs propres angoisses, ce
qui était là n'était pas ce qui avait lieu, parce que la connaissance
des cardères, des rumex, des mauves et autres chicorées empêchait
d'avoir lieu ce qui devait avoir lieu.

les belles coupes tenaient dans le souffle du soir --- les coupes
tenaient miraculeusement en équilibre comme, au bout des baguettes
tournoyantes de bambou, les assiettes de porcelaine blanche avec
lesquelles jonglent les artistes circassiens chinois, et devant
l'envoûtement procuré par cet équilibre de rêve, destiné à stupéfier
d'autres yeux, sensibles à d'autres vibrations, à d'autres rayons,
captivé par la blancheur en suspension de ces plateformes d'atterrissage
d'une conception parfaite.

![](/img/plus/sans-titre/03.jpg)

et je pensai devant cette mer de tiges coriaces qu'en distinguant
l'individuel, qu'en atomisant --- atomisant l'universel en myriades de
grains de poussière dénombrables --- pensai qu'en tenant compte de
l'individu on s'excluait du tout, et je pense encore --- et je n'arrive
pas à faire le lien entre cette capacité de nommer le monde permettant
de faire surgir dans la masse indistincte de la création l'invisible
noyé dans la colle --- *cette* ombellifère, *cette* oseille --- d'un côté
et, pour ainsi dire de l'autre, la fragmentation, l'effritement tragique
de ce qui doit fatalement retourner à la poussière.

et je voudrais sans y parvenir faire le pont entre cette notion
d'effritement, telle qu'elle apparaît par exemple à la lecture de
Sebald, dans *Les Anneaux de Saturne*, entre l'effritement de la
mémoire, et celui, matériel, des falaises crayeuses de la côte du
Suffolk, car il y a une voie à méditer entre --- entre ce sol truffé de
nids d'hirondelles sur lequel l'auteur s'allonge le temps de prendre un
repos, les falaises avalant les maisons bâties sur leur crête, et la
mémoire, et les souvenirs, et les phrases hantées par leur propre
incomplétude, et le sentiment de la mélancolie enfin.

alors je repensai au commentaire de Sebald au sujet de la gravure de
Dürer, *Melancolia I*, où l'on voit l'ange saturnien fléchir un coude au
milieu des instruments éparpillés du savoir et de l'artisanat humain, et
il me revient à l'esprit que Sebald pour désigner ces instruments de la
connaissance et de la technique (--- compas, rabot, cadran, sablier,
polyèdre, etc.), que Seblad dit : l'ange se tient assis au milieu des
"instruments de la destruction" --- comme si la pulsion de
connaissance atomisant le tout en parties mesurables, et individuelles,
et ductiles, par le moyen de ses instruments ne produisait nullement de
la connaissance, mais seulement la destruction de la connaissance, comme
si ce geste foncièrement catastrophique et entêté dans l'échec était la
source d'une intarissable mélancolie.

remarquant que penser la catastrophe en termes d'effritement, d'érosion,
de disparition était devenu inopérant en l'espace de seulement vingt ans
puisque la disparition (--- des espèces, des fleurs, de tout ce qui
remplit le ciel, la mer et la terre) a lieu exactement comme si elle
n'avait pas lieu.

je cheminais sur le trottoir où les rayons du soir, en rasant la terre,
détouraient les arcs, les flûtes, les bols floraux et transformaient
ainsi en théâtre exubérant ce champ inculte et caillouteux dont le nom
même, m'avait révélé un jour, je m'en souvins en un éclair,
l'agriculteur au détour d'une discussion qui s'était avérée pénible au
regard de l'incompréhension réciproque qui nous séparait, moi le
flâneur, lui le laboureur, occupé à conduire dans ce pré ses moutons
parce qu'étaient épuisées leurs maigres réserves de fourrage, le nom
même du pré évoquait un amas de pierres stériles, --- je cheminais, les
idées éparses, en ayant la triste impression de me trouver sur le chemin
d'un exil immobile où, inexorablement, sécheresse après sécheresse, la
vie se désolidariserait de ses propres soutiens.

![](/img/plus/sans-titre/04.jpg)

les herbes étaient sèches, cassantes, armées de sabres, d'épines aiguës
et formaient un broussailleux maquis, et repensant à la forêt rousse de
Tchernobyl brûlée par l'action invisible du feu atomique et dont j'avais
appris en lisant *La* *Supplication* que les fûts ne se décomposaient
plus, faute d'organismes vivants, de champignons pour le faire, restant
pétrifiés debout pour l'éternité sans que puisse se réamorcer le cycle
de la vie, et qui furent enfouis au bulldozer sous la terre, comme fut
enfouie la terre elle-même qui portait ces arbres ---

repensant à Tchernobyl, je songeai que certaines choses arrivaient dont
l'expérience manquait cruellement.

et naturellement ce dont on n'a pas l'expérience, c'est comme si
--- alors même qu'il y aurait à essayer tellement de manières de rester
debout, et tellement de manières de tomber, et ce n'est, je crois, ni
par manque, ni par défaut, d'imagination ou de sensation, ni par
nihilisme, c'est juste que tu ne peux pas.

ou peut-être, comme le pensait Benjamin, quelque chose s'est-il
historiquement brisé au début du XX^e^ siècle quant au désir commun,
quant au fait de pouvoir relater, d'inventer à l'expérience une forme et
finalement de la représenter, nous nous serions "appauvris en
expérience communicable".

il est des
épreuves au demeurant, pensai-je en avançant dans le crépuscule pesant,
il est des épreuves dont la consistance échappe d'emblée --- encore que
nous soyons désormais restreints, que nous ne sachions plus ni raconter
ni faire d'images qui ne soient pas de piètres reconstitutions, si bien
que tout désormais se tient sous nos yeux, exactement là et parfaitement
sec, insignifiant.

![](/img/plus/sans-titre/05.jpg)

et pendant que j'avançais en affrontant les phares éberlués des
automobilistes rasant le pavé, je baissai les yeux, le cœur agité,
songeant que les épis dressés, jaillis de leur fabuleuse nécessité,
existaient peut-être à la façon des idées dans le champ de l'esprit ou
quelque chose de cet ordre --- aussitôt cela me parut conceptuellement
pauvre, sec, insignifiant, et je crois que j'aurais aimé plus que tout
le reste à ce moment-là, marchant sur ce trottoir-là, à cette heure-là,
à vrai dire je crois, et c'est triste, que j'aurais aimé marcher en
compagnie.

et sur le seuil où me reconduisirent mes pas, où je fis entrer la
tristesse, je me demandai de quoi au juste pareille tristesse était
tissée, et si ce sentiment ne présentait pas quelque parenté avec la
mélancolie de l'ange, regard vide, épuisé, parmi l'opulence des moyens
de comprendre, tandis que son ciel traversé par une puissante comète
aussi éblouissante, aussi insignifiante, aussi menaçante qu'un feu de
Mercedes --- le regard vide devant ce chaos de sens et de non-sens,
incapable de savoir si l'apocalypse a eu, ou va avoir lieu puisque c'est
comme si --- et finalement seul le chien roulé en boule, assoupi aux
pieds de son compagnon semble fidèle à son instinct.

![](/img/plus/sans-titre/07.jpg)

l'image de ce chien roulé en boule aux pieds de son compagnon désorienté
et las, faisant défection à --- à quoi ? la recherche du salut ? cette
image m'accompagna un temps, puis franchissant le seuil, une autre
pensée fleurit, la pensée que cette fidélité exprimait aussi l'extrême
désarroi de l'être venu se placer (sans qu'il en eût conscience) sous la
garde de cet ange destructeur --- puis songeant à Blanqui qui, du fond de
son cachot, se voyait à perpétuité, debout pour l'éternité sur d'autres
astres, d'autres comètes, se voyait rejouer toutes les variations de sa
vie à l'identique, du fond de cette solitude cosmique, comment ne pas
céder en effet à l'évidence, se coucher aux pieds de sa déesse,
tristesse, et ne plus bouger.
