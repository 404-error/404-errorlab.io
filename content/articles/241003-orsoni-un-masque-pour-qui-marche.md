---
persona:
  - Jérôme Orsoni
title: "Un masque pour qui marche"
slug: "un-masque-pour-qui-marche"
date: 2024-10-15
echo:
  - esthétique
images:
  - "img/241003-orsoni-un-masque-pour-qui-marche.jpg"
gridimages:
  - "img/241003-orsoni-un-masque-pour-qui-marche-grid.jpg"
sourceimage: "https://commons.wikimedia.org/wiki/Category:Old_maps_of_Rio_de_Janeiro#/media/File:Cópia_do_Mapa_de_Fls._4_do_Livro_que_da_Rezão_do_Estado_do_Brasil_F·eita_em_1612_-_1,_Acervo_do_Museu_Paulista_da_USP_(cropped).jpg https://commons.wikimedia.org/wiki/Category:Old_maps_of_Recife#/media/File:Planta_da_Cidade_de_Recife_-_1,_Acervo_do_Museu_Paulista_da_USP_(cropped).jpg https://commons.wikimedia.org/wiki/Category:Old_maps_of_Salvador_(Bahia)#/media/File:Planche_XXXIV.jpg https://commons.wikimedia.org/wiki/File:Planta_da_cidade_de_Porto-Allegre_(Cartogr%C3%A1fico)_%E2%80%94_com_a_linha_de_trincheiras_e_fortifica%C3%A7%C3%B5es_que_lhe_tem_servido_de_defesa_desde_o_memoravel_dia_15_de_junho_de_1836,_com_as..._por_L._P._Dias.jpg https://upload.wikimedia.org/wikipedia/commons/3/3e/Mapa_Curitiba_1894_CR493.jpg https://commons.wikimedia.org/wiki/Category:Old_maps_of_Belo_Horizonte#/media/File:Planta_BH.jpg"
notes: ""
summary: "Tout à l'heure, si l'on m'avait interrogé à ce sujet, j'eusse juré que c'était vrai, mais à présent, je ne sais plus très bien : ai-je inventé cette espèce d'apparition ? ai-je voulu voir seulement ce que je voulais voir ? ou bien la réalité offre-t-elle, à qui daigne encore s'y montrer quelque peu attentif, au lieu de passer son temps plongé dans l'obsession de soi-même, des confirmations inespérées, lesquelles nous disent : \"Non, tu ne te trompes pas\" ? Non vraiment, je ne sais pas, mais tout à l'heure, si l'on m'avait interrogé à ce sujet, j'eusse répondu sans hésiter que là, à la fossette où naît le cou, en capitales d'imprimerie, la jeune femme que je venais de croiser à l'instant même avait ces trois lettres tatouées : A R T."
citation: "l'écriture n'est pas et ne peut pas être une marchandise comme les autres"
poetry: false
hangindent: false
---

{{%droite%}}

--- Sur *Mes deux mondes*, de Sergio Chejfec, traduit de l'espagnol
(Argentine) par Claude Murcia, Bordeaux, éditions do, 2024.

{{%/droite%}}

Tout à l'heure, si l'on m'avait interrogé à ce sujet, j'eusse juré que
c'était vrai, mais à présent, je ne sais plus très bien : ai-je inventé
cette espèce d'apparition ? ai-je voulu voir seulement ce que je voulais
voir ? ou bien la réalité offre-t-elle, à qui daigne encore s'y montrer
quelque peu attentif, au lieu de passer son temps plongé dans
l'obsession de soi-même, des confirmations inespérées, lesquelles nous
disent : "Non, tu ne te trompes pas" ? Non vraiment, je ne sais pas,
mais tout à l'heure, si l'on m'avait interrogé à ce sujet, j'eusse
répondu sans hésiter que là, à la fossette où naît le cou, en capitales
d'imprimerie, la jeune femme que je venais de croiser à l'instant même
avait ces trois lettres tatouées : A R T.

Dans *Impressions de Kassel*, le livre où il raconte son invitation et
sa participation à la Documenta 13, Enrique Vila-Matas relate une
anecdote qui mérite d'être ici rapportée. Lors de la manifestation, il
est invité à écrire dans un restaurant chinois du nom de Dschingis Khan
où, bien évidemment, quand il est l'heure pour lui de se mettre à
l'œuvre, personne ne se trouve pour le regarder en train de faire.
Personne, si ce n'est une grosse Allemande qui, le prenant dans ses
bras, s'exclame à plusieurs reprises : "*Writer !*" Vila-Matas, on le
devine aisément --- ce ne devait pas être ainsi qu'il envisageait sa
performance au prestigieux plan quinquennal de l'art contemporain et
l'on sent bien à la lecture que, rétrospectivement, il se délecte à
l'idée de narrer son obèse déception ---, ne goûte guère le débordant
enthousiasme que la grosse Allemande manifeste en présence de
l'écrivain, et il lui réplique, laconique (fait suffisamment rare dans
la bibliographie vilamatasienne pour ne pas manquer de le souligner) :
"Oui, je suis un *writer*, ai-je rétorqué mal à l'aise. Et alors ?"

À mesure que j'avançais dans ma lecture, j'avais de moins en moins la
certitude d'avoir déjà lu une première fois *Mes deux mondes* de Sergio
Chejfec. Pourtant, je me souvenais exactement des circonstances dans
lesquelles j'avais connu ce livre : le conseil de mon ami d'alors,
Samuel Monsalve, l'exemplaire corrompu de l'ouvrage, ma lettre adressée
à Pierre-Olivier Sanchez (l'éditeur de la première édition française de
l'ouvrage au Passage du Nord-Ouest), le dernier chapitre de mon livre
*des Monstres littéraires*, et peut-être que, en effet, je m'étais
imaginé l'avoir lu, peut-être que, comme le tatouage à la naissance du
cou de la jeune femme croisée tout à l'heure en remontant la rue de
Rennes en direction de la tour Montparnasse, j'imaginais tous ces
événements et les histoires qui vont avec. Car, plus j'avançais dans le
livre, et moins j'avais le sentiment d'être en terrain connu. À aucun
moment, pourtant, je ne me suis dit que c'était l'effet du livre
lui-même, que c'était la nature propre du livre qui était la cause que,
le lisant, j'avais l'impression de ne l'avoir jamais lu, ne me sentais
pas chez moi, en terrain connu. Et ce sentiment plus général qui
s'étendait à la nature même du livre que j'étais en train de lire, cela
aussi, j'y pense à présent, était peut-être l'effet du livre sur moi.

*Mes deux mondes* ne raconte presque rien : c'est l'histoire du
narrateur (on peut supposer qu'il s'agit de Sergio Chejfec lui-même,
écrivain argentin décédé en 2022 à l'âge de soixante-six ans, mais outre
le "je" de la narration, rien ne nous l'assure indubitablement) qui,
se trouvant dans une ville du sud du Brésil pour assister à une
conférence sur la littérature, décide de sortir faire une promenade. À
proprement parler, il ne se passe rien dans le livre, et toutefois, il y
a des chances qu'il soit aussi vaste que l'univers.

J'ai eu le sentiment de ne vraiment pas me retrouver dans livre de
Sergio Chejfec quand j'y ai rencontré le nom de Borges. Dans mon esprit,
ce que j'étais sûr de ne pas trouver dans un tel livre, c'était bien ce
nom-là, bien ce personnage, bien cette figure tutélaire, quasi la statue
du commandeur de la littérature fantastique, que je m'imaginais bien
trop pesante, bien trop lourde, bien trop écrasante pour un écrivain
argentin qui écrit un livre où il ne se passe rien, ou presque, sur
aucun sujet, ou presque, où l'on dérive sans fin, un peu comme dans un
labyrinthe. J'avais la carte de l'esprit ouverte devant moi grand comme
l'application sur mon téléphone portable et j'avais beau m'orienter dans
tous les sens successivement, je ne m'y retrouvais pas.

Au départ, je voulais commencer cette recension par une brève histoire
de la marche en littérature. J'avais même prévu de placer en exergue du
texte la fameuse citation de Nietzsche, extraite du *Crépuscule des
idoles*, où il dit ceci : "On ne peut penser et écrire qu'assis
(G. Flaubert). --- Damit habe ich dich, Nihilist! Das Sitzfleisch ist
gerade die Sünde wider den heiligen Geist. Nur die ergangenen Gedanken
haben Werth." C'est-à-dire, un peu trop littéralement, peut-être : "Je
t'y prends, nihiliste ! Le derrière est le vrai péché contre le saint
esprit. Seules les pensées promenées ont quelque valeur." Dans ma
recension, il devait être question de Balzac, de Baudelaire, de
Benjamin, de Walser et de Sebald, des filiations, des points communs et
des différences entre les uns et les autres, mais plus j'avançais dans
ma lecture de Chejfec et plus je trouvais cette idée absurde, parce que
l'ouvrage, s'il était en effet le récit d'une déambulation dans une
ville, pouvait tout aussi bien ne l'être pas tant il semblait flotter
dans une atmosphère d'indétermination : chaque pas que le narrateur
faisait dans la ville semblait être de nature à le conduire en n'importe
quel autre lieu du globe, à n'importe quel moment du temps, dans la
réalité ou dans la fiction sans distinction aucune, et ce sans que
jamais telle ou telle dérivation du périmètre en apparence bien établi
du récit ne soit prévisible. Par le lecteur, certes, mais prévisible par
le narrateur, non plus. Et j'allais ajouter, appliquant une distinction
de catégorie qu'il n'y a peut-être pas lieu de faire ici, par l'auteur,
même. Malgré la chaleur qui règne dans la ville du sud du Brésil où le
récit est supposé se dérouler, tout y semble enveloppé d'un épais
manteau tissé d'étrangeté : des liaisons semblables aux premiers temps
d'internet qu'évoque le narrateur au début de son récit semblent
conduire la narration, au gré, sinon du hasard, du moins de connexions
qui, avec une sorte d'esprit de système, s'acharnent à échapper
totalement à la logique ordinaire, celle avec laquelle on aborde
d'habitude les livres qui, comme chacun s'y attend, doivent avoir un
début, un milieu et une fin. *Mes deux mondes*, plus je le lisais et
plus cela me paraissait évident, n'obéissait pas à cette logique
linéaire, obéissait peut-être à une logique, oui, mais laquelle ? Cela,
je ne pouvais le dire. Et quand, pour donner de la consistance à ma
lecture, afin de donner un nom à la chose que j'avais sous les yeux, je
cherchai sur quelque carte numérique dans quelle ville du Brésil le
récit pouvait bien se dérouler, je compris que cela n'avait aucun sens :
dans cette littérature-là, les villes n'ont pas de nom, les liaisons
entre les événements échappent aux lois normales de la causalité, les
aiguilles des montres tournent dans le sens inverse des aiguilles d'une
montre, les lacs empruntent des canaux souterrains pour surgir de terre
dans une ville allemande dont le nom restera à son tour inconnu, et les
cygnes sont des animaux aussi bien que des pédalos.

Peu après que j'ai lu le nom de Borges, je me suis senti très mal à
l'aise. Et c'est peut-être à cause de ce malaise que j'en suis venu à
modifier entièrement mon approche de l'ouvrage. Parce que, soudain, mon
histoire personnelle se trouvait impliquée dans l'ouvrage et que je ne
savais plus si ce que je lisais était réel ou une sorte de projection de
ma mémoire à moi sur le livre, de mes idées à moi dans le livre,
exactement comme, j'y reviens de nouveau, tout à l'heure, en marchant
dans la rue, j'ai vu les lettres A R T tatouées en capitales
d'imprimerie sur le cou de la jeune femme que je venais de croiser. Pas
plus que je ne me souvenais d'avoir trouvé le nom de Borges dans
l'ouvrage la première fois que je l'avais lu, je ne me souvenais pas que
le nom de Kentridge y figurât. Mais peut-être parce que, à cette époque,
je ne connaissais pas William Kentridge, cet artiste contemporain
sud-africain dont, pour mon plus grand malheur de piètre traducteur,
j'ai indirectement croisé la route, un jour, il y a quelques années de
cela. Ainsi, au milieu de cette narration, surgissait un épisode de ma
propre existence que j'eusse préféré oublier. Et c'est probablement pour
cette raison que je m'y suis particulièrement attardé, convaincu peu à
peu que la présence de ces deux noms propres quasi l'un à la suite de
l'autre (d'abord Borges ensuite Kentridge), indépendamment de l'histoire
de ma vie que j'aborde en passant parce que c'est l'histoire de celui
qui lit et qui dit qu'il lit, mais dont il ne saurait être réellement
question ici, n'était pas anodine, mais avait un sens qu'il convenait de
mettre au jour.

"Je connais assez la succession fatale des nuits, je crois que c'est
Borges qui le dit, pour comprendre qu'aucune distraction ou idée
n'empêche le temps de passer et le futur d'advenir."

"J'ai été un spectateur accidentel des dessins animés de Kentridge.
Quand je voulus m'en procurer un jour pour le regarder à loisir, cela me
fut impossible. Une fois je réussis à trouver un compact disc, qui me
coûta très cher, ce qui me semble une plaisanterie car il contient à
peine 40 ou 50 secondes de quelques-unes de ses œuvres. Comme il était
ancien, il devait fonctionner avec des logiciels qui n'existent plus.
Quand je le mis, mon ordinateur commença à faire un bruit nouveau, comme
un aspirateur, qui s'entendait dans toute la maison. Ce n'est qu'à force
de beaucoup de patience que je réussis à voir le disque, avec les
résultats mentionnés."

Exactement comme les villes n'ont pas de nom et les aiguilles des
montres tournent dans le sens inverse des aiguilles d'une montre, les
citations sont approximatives et les dessins animés ne se laissent pas
regarder sur des cd trop chers pour ce que c'est : tout semble
constamment dévier de sa trajectoire supposée, être plongé dans une
sorte d'anonymat irréversible, se perdre dans des glissements sensoriels
imperceptibles, s'offrir au regard en s'échappant, changer de forme
quand on croit l'avoir définitivement saisi.

Un peu plus loin dans le récit, vers la fin, s'attablant à la terrasse
d'un café, le narrateur décrit le trouble qui le gagne soudain : "À
force d'adopter une attitude d'écrivain, j'avais fini par en être un ;
et à présent, par une sorte de panique rétrospective, j'étais terrifié à
l'idée qu'on me découvre, justement au moment où je pouvais considérer
presque tous les dangers comme écartés. Et la peur se reflétait dans ce
qu'il y a de plus basique, comme toujours, le travail manuel et la
circonstance anonyme. Je ne craignais plus de n'être pas publié, ni de
vivre loin du succès ou de la reconnaissance, je savais déjà que ces
choses seraient toujours à ma portée, pour le meilleur ou pour le pire ;
je craignais que quelqu'un, en passant à côté de mon cahier ouvert, me
démasque comme un simple et conscient imposteur. Les feuilles de mon
cahier ne contiendraient pas de phrases, ni même des mots, seulement des
dessins cherchant à imiter des calligraphies, ou des pages couvertes du
mot “quoi” et surtout “comment”, ou de syllabes déconnectées qui ne
faisaient jamais sens." Et tout de suite après, cherchant à se souvenir
des raisons qui avaient bien pu le pousser à écrire en public, il
ajoute : "La réponse résidait dans le fait que j'étais un écrivain
public dans un sens littéral : écrire en public, comme un pianiste qui
interprète devant le public ou un acteur au moment de jouer. Peu
m'importait les circonstances, qu'on me regarde ou non, ou même que
quelqu'un s'approche discrètement de mon cahier. Il y avait là quelque
chose d'un pari sur ma vocation, une sorte d'anxiété à la fois floue et
superconcentrée, jusqu'au moment où, après un temps d'intimité avec le
développement de mes idées et le papier colonisé, je sentais qu'un léger
tremblement m'enveloppait."

Il est évident que, dans le passage que j'ai cité d'*Impressions de
Kassel*, livre écrit plusieurs années après *Mes deux mondes*, Enrique
Vila-Matas fait référence aux différentes scènes d'écriture publique qui
y sont relatées, et c'est à cela que j'ai pensé immédiatement en lisant
les passages que je viens de citer, notamment le second où il est
question de l'écrivain public. Mais il me semble qu'il y a une logique
plus profonde à l'œuvre : la succession des deux seuls noms propres que
l'on trouve dans l'ouvrage, --- Borges et Kentridge. Dans l'économie du
récit, pour ne pas dire dans l'économie du monde réel, tout simplement,
ces deux noms propres incarnent des figures symboliques plus qu'elles ne
désignent des personnes ayant existé ou existant réellement : celle de
l'écrivain, d'une part, Borges, et celle de l'artiste contemporain,
d'autre part, Kentridge. Et le mouvement va de l'un à l'autre : à mesure
qu'il avance --- ou s'enfonce --- dans son récit, je crois que le
narrateur cesse d'être un écrivain pour devenir un artiste contemporain.

Il y a quelques semaines de cela, lors de l'une de nos conversations,
Hilda Nord m'avait déjà dit quelque chose de cet ordre : comment dans un
monde où il n'y a plus de place pour l'écriture, l'écrivain doit en
quelque sorte se faire artiste contemporain pour exister. N'était-ce pas
déjà ce à quoi Vila-Matas s'était résolu, dans un geste que Chejfec
avait esquissé quelques années avant lui, sans en faire toutefois la
théorie explicite, à devenir une sorte de personnage public donnant
l'écriture moins pour une attention au monde, aux choses et aux êtres
qui le peuplent, que comme une performance ? Et n'était-ce pas un
mouvement comme celui-là que moi-même j'avais décrit dans mon histoire
"Tout est de l'art, tu sais, c'est comme ça, il n'y a rien que tu
puisses y faire", où un artiste contemporain ornemental est invité en
résidence à demeure chez le narrateur dont il finit par entreprendre
d'écrire les livres ?

Dans un monde où la valeur ne peut plus se parer du masque symbolique de
l'art, dans un monde où ce qui ne se vend pas ne vaut rien, où --- pour
le dire de manière on ne peut plus claire --- l'art a été absorbé sans
résidu par la culture et où la culture, après qu'elle a été assaillie
par des décennies de sociologie "de gauche", est devenue
fondamentalement "de droite", il n'y a plus de place pour l'écrivain
tel que la figure de Borges --- nonobstant le poids écrasant avec lequel
il a pu peser sur la littérature mondiale --- pouvait encore l'incarner
au XX^e^ siècle. Dans de telles conditions, l'écrivain se trouve face à un
dilemme : ou bien l'anonymat contraint de l'absence de succès ou bien la
métamorphose en une figure radicalement différente et, comme l'avait
très bien compris Chejfec, totalement publique, l'époque ayant, en
avalant la sublimation dans le gosier de la valeur, aboli par là même la
frontière qui sépara jadis le public et le privé, le succès et le
secret, pour laisser régner en absolue maîtresse la valeur, mesure de
toutes les choses qui sont et qui vont à la surface de la terre.

Là où, jusqu'au milieu du XX^e^ siècle, le masque symbolique dont se
pare la valeur aux yeux de la bourgeoisie permet encore à
l'écrivain d'acquérir une valeur qui lui est propre par la sublimation,
le tombé de ce masque ne laisse désormais plus à ce dernier aucun
espace où exister. Dès lors, il doit se transformer ou disparaître, se
condamner au silence ou devenir performance, œuvre à laquelle peut enfin
se voir attachée une valeur marchande. Métamorphose que, dans
*Impressions de Kassel*, avec le brio qui lui est propre, Vila-Matas
incarnait à la perfection.

À la fin de *Mes deux mondes*, attablé à la terrasse du Café do Lago
dans sa ville anonyme du sud du Brésil, le narrateur de Chejfec évoque
une dernière fois son "rêve, n'être personne, écrivain redevenu secret,
à nouveau réalisé..."

Jusqu'au bout, maintenant l'indétermination qui fascine dans son livre,
Chejfec me semble toutefois se refuser à accepter comme définitive sa
métamorphose en écrivain contemporain. Il évoque ainsi "l'immobilité,
l'attente et toutes les situations qui y étaient liées, d'un côté, et
les actions et les échanges avec le prochain, de l'autre. Je cherchais
la frontière fragile entre les deux, comme si je vivais à mon corps
défendant dans chacun des deux mondes. Comme il m'est arrivé en d'autres
occasions, inutile de dire que je n'aboutis à aucune conclusion durable,
et moins encore évidente. L'alternative était sans doute modeste, mais
elle semblait être la seule à laquelle je pouvais aspirer."

Tandis que, quelques années plus tard, dans ce récit cabotin au possible
qu'est *Impressions de Kassel*, et qui ne pourrait être que cela,
cabotin, parce que l'écrivain y jouerait un rôle qu'en vérité il ne
tiendrait déjà plus depuis longtemps, celui précisément de l'écrivain,
Enrique Vila-Matas signerait sans condition l'acte officiel par lequel,
pour reprendre l'expression consacrée par Walter Benjamin dans *Paris
capitale du XIX^e^ siècle*, le flâneur se rend au marché, Sergio
Chejfec se refusait dans *Mes deux mondes* à accepter purement et
simplement cette mutation, l'indétermination n'étant pas dans son
ouvrage un moment de déséquilibre transitoire, mais un réel climat. Et
c'est cela, je crois, ou du moins en partie, qui en fait le génie.
Contrairement à ce que notre époque affirme avec une prétentieuse
suffisance, l'écriture n'est pas et ne peut pas être une marchandise
comme les autres, elle ne peut pas obéir purement et simplement à la loi
absolue de la valeur. Elle engage des passions secrètes, privées, qui
lui échappent par nature. Elle est un art de l'égarement dans le
labyrinthe dont elle dessine les dédales. Et, quel que soit le nom que
l'on donne ou ne donne pas à tel ou tel lieu, à la fois cartographie
publique de l'univers et géographie intime de son parcours.

Une fois tombé le masque de la sublimation et réduit à la valeur toute
nue, l'art n'a guère plus de sens qu'un tatouage sur une peau qui se
flétrit. Mieux vaut les envoyer promener, dit qui écrit.
