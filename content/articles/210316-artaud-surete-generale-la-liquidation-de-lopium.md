---
persona:
  - Antonin Artaud
title: "Sûreté générale : La liquidation de l'opium"
slug: "surete-generale-la-liquidation-de-lopium"
date: 2021-03-16
echo:
  - stratégique
images:
  - "img/210316-artaud-surete-generale-la-liquidation-de-lopium.jpg"
gridimages:
  - "img/210316-artaud-surete-generale-la-liquidation-de-lopium-grid.jpg"
sourceimage: "https://commons.wikimedia.org/wiki/File:K%C3%B6hler%27s_Medizinal-Pflanzen_in_naturgetreuen_Abbildungen_mit_kurz_erl%C3%A4uterndem_Texte_(Plate_37)_BHL303636.jpg"
notes: "Ce texte d'Antonin Artaud a paru le 15 janvier 1925 dans le numéro 2 de la revue *La Révolution Surréaliste*. Il est également à noter, d’après les commentaires des Œuvres complètes tenant compte du manuscrit original d’Antonin Artaud, que la phrase \"Malheureusement pour la médecine, la maladie existe\" devrait être entendue ainsi : \"Malheureusement pour la maladie, la médecine existe.\""
summary: "J'ai l'intention non dissimulée d'épuiser la question afin qu'on nous foute la paix une fois pour toutes avec les soi-disant dangers de la drogue. Mon point de vue est nettement anti-social. On n'a qu'une raison d'attaquer l'opium. C'est celle du danger que son emploi peut faire courir à l'ensemble de la société. OR CE DANGER EST FAUX."
citation: "Nous ne nous suicidons pas tout de suite. En attendant qu'on nous foute la paix."
---


J'ai l'intention non dissimulée d'épuiser la question afin qu'on nous foute la paix une fois pour toutes avec les soi-disant dangers de la drogue.

Mon point de vue est nettement anti-social.

On n'a qu'une raison d'attaquer l'opium. C'est celle du danger que son emploi peut faire courir à l'ensemble de la société.

OR CE DANGER EST FAUX.

Nous sommes nés pourris dans le corps et dans l'âme, nous sommes congénitalement inadaptés ; supprimez l'opium, vous ne supprimerez pas le besoin du crime, les cancers du corps et de l'âme, la propension au désespoir, le crétinisme-né, la vérole héréditaire, la friabilité des instincts, vous n'empêcherez pas qu'il n'y ait des âmes destinées au poison quel qu'il soit, poison de la morphine, poison de la lecture, poison de l'isolement, poison de l'onanisme, poison de coïts répétés, poison de la faiblesse enracinée de l'âme, poison de l'alcool, poison du tabac, poison de l'anti-sociabilité. Il y a des âmes incurables et perdues pour le reste de la société. Supprimez-leur un moyen de folie, elles en inventeront dix mille autres. Elles créeront des moyens plus subtils, plus furieux, des moyens absolument DÉSESPÉRÉS. La nature elle-même est anti-sociale dans l'âme, ce n'est que par une usurpation de pouvoirs que le corps social organisé réagit contre la pente *naturelle* de l'humanité.

Laissons se perdre les perdus, nous avons mieux à occuper notre temps qu'à tenter une régénération impossible et pour le surplus, inutile, ODIEUSE ET NUISIBLE.

Tant que nous ne serons parvenus à supprimer aucune des causes du désespoir humain, nous n'aurons pas le droit d'essayer de supprimer les moyens par lesquels l'homme essaie de se décrasser du désespoir.

Car il faudrait d'abord arriver à supprimer cette impulsion naturelle et cachée, cette pente *spécieuse* de l'homme qui l'incline à trouver un moyen, qui lui donne *l'idée* de chercher un moyen de sortir de ses maux.

De plus, les perdus sont par nature perdus, toutes les idées de régénération morale n'y feront rien, il y a un DÉTERMINISME INNÉ, il y a une incurabilité indiscutable du suicide, du crime, de l'idiotie, de la folie, il y a un cocuage invincible de l'homme, il y a une friabilité du caractère, il y a un châtrage de l'esprit.

L'aphasie existe, le tabès dorsalis existe, la méningite syphilitique, le vol, l'usurpation. L'enfer est déjà de ce monde et il est des hommes qui sont des évadés malheureux de l'enfer, des évadés destinés à recommencer ÉTERNELLEMENT leur évasion. Et assez là-dessus.

L'homme est misérable, l'âme est faible, il est des hommes qui se perdront toujours. Peu importent les moyens de la perte ; ÇA NE REGARDE PAS LA SOCIÉTÉ.

Nous avons bien démontré, n'est-ce pas, qu'elle n'y peut rien, elle perd son temps, qu'elle ne s'obstine donc plus à s'enraciner dans sa stupidité.

Et enfin NUISIBLE.

Pour ceux qui osent regarder la vérité en face, on sait, n'est-ce pas, les résultats de la suppression de l'alcool aux États-Unis.

Une super-production de folie : la bière au régime de l'éther, l'alcool bardé de cocaïne que l'on vend clandestinement, l'ivrognerie multipliée, une espèce d'ivrognerie générale. BREF, LA LOI DU FRUIT DÉFENDU.

De même, pour l'opium.

L'interdiction qui multiplie la curiosité de la drogue n'a jusqu'ici profité qu'aux souteneurs de la médecine, du journalisme, de la littérature. Il y a des gens qui ont bâti de fécales et industrieuses renommées sur leurs prétendues indignations contre l'inoffensive et infime secte des damnés de la drogue (inoffensive parce qu'infime et parce que toujours une exception), cette minorité de damnés de l'esprit, de l'âme, de la maladie.

Ah ! que le cordon ombilical de la morale est chez eux bien noué. Depuis leur mère, ils n'ont, n'est-ce pas, jamais péché. Ce sont des apôtres, ce sont les descendants des pasteurs ; on peut seulement se demander où ils puisent leurs indignations, et combien surtout ils ont palpé pour ce faire, et en tout cas qu'est-ce que ça leur a rapporté.

Et d'ailleurs, là n'est pas la question.

En réalité, cette fureur contre les toxiques, et les lois stupides qui s'ensuivent :

1<sup>o</sup> *Est inopérante contre le besoin du toxique*, qui, assouvi ou inassouvi, est inné à l'âme, et l'induirait à des gestes résolument anti-sociaux, MÊME SI LE TOXIQUE N'EXISTAIT PAS.

2<sup>o</sup> *Exaspère le besoin social du toxique*, et le change en vice secret.

3<sup>o</sup> *Nuit à la véritable maladie*, car c'est là la véritable question, le nœud vital, le point dangereux :

MALHEUREUSEMENT POUR LA MÉDECINE, LA MALADIE EXISTE.

Toutes les lois, toutes les restrictions, toutes les campagnes contre les stupéfiants n'aboutiront jamais qu'à enlever à tous les nécessiteux de la douleur humaine, qui ont sur l'état social d'imprescriptibles droits, le dissolvant de leurs maux, un aliment pour eux plus merveilleux que le pain, et le moyen enfin de repénétrer dans la vie.

Plutôt la peste que la morphine, hurle la médecine officielle, plutôt l'enfer que la vie. Il n'y a que les imbéciles du genre de J.-P. Liausu (qui est pour le surplus un avorton ignorant) pour prétendre qu'il faille laisser des *malades macérer dans leur maladie*.

Et c'est ici d'ailleurs que toute la cuistrerie du personnage montre son jeu et se donne libre carrière : AU NOM, PRÉTEND-IL, DU BIEN GÉNÉRAL.

Suicidez-vous, désespérés, et vous, torturés du corps et de l'âme, perdez tout espoir. Il n'y a plus pour vous de soulagement en ce monde. Le monde vit de vos charniers.

Et vous, fous lucides, tabétiques, cancéreux, méningitiques chroniques, vous êtes des incompris. Il y a un point en vous que nul médecin ne comprendra jamais, et c'est ce point pour moi qui vous sauve et vous rend augustes, purs, merveilleux : vous êtes hors la vie, vous êtes au-dessus de la vie, vous avez des maux que l'homme ordinaire ne connaît pas, vous dépassez le niveau normal et c'est de quoi les hommes vous tiennent rigueur, vous empoisonnez leur quiétude, vous êtes des dissolvants de leur stabilité. Vous avez d'irrépressibles douleurs dont l'essence est d'être inadaptable à aucun état connu, inajustable dans les mots. Vous avez des douleurs répétées et fuyantes, des douleurs insolubles, des douleurs hors de la pensée, des douleurs qui ne sont ni dans le corps ni dans l'âme, *mais qui tiennent de tous les deux*. Et moi, je participe à vos maux, et je vous le demande : qui oserait nous mesurer le calmant ? Au nom de quelle clarté supérieure, âme à nous-mêmes, nous qui sommes à la racine même de la connaissance et de la clarté. Et cela, de par nos instances, de par notre insistance à souffrir. Nous que la douleur a fait voyager dans notre âme à la recherche d'une place de calme où s'accrocher, à la recherche de la stabilité dans le mal comme les autres dans le bien. Nous ne sommes pas fous, nous sommes de merveilleux médecins, nous connaissons le dosage de l'âme, de la sensibilité, de la moelle, de la pensée. Il faut nous laisser la paix, il faut laisser la paix aux malades, nous ne demandons rien aux hommes, nous ne leur demandons que le soulagement de nos maux. Nous avons bien évalué notre vie, nous savons ce qu'elle comporte de restrictions en face des autres, et surtout en face de nous-mêmes. Nous savons à quel avachissement consenti, à quel renoncement de nous-mêmes, à quelles paralysies de subtilités notre mal chaque jour nous oblige. Nous ne nous suicidons pas tout de suite. En attendant qu'on nous foute la paix.

<p style="margin-right:15%;text-align: right;">1<sup>er</sup> <em>janvier</em> 1925.</p>

