---
persona:
  - Marc Verlynde
title: "La mort du roman : éternels débuts"
slug: "la-mort-du-roman-eternels-debuts"
date: 2021-09-23
echo:
  - esthétique
images:
  - "img/210923-verlynde-la-mort-du-roman-eternels-debuts.jpg"
gridimages:
  - "img/210923-verlynde-la-mort-du-roman-eternels-debuts-grid.jpg"
sourceimage: "https://commons.wikimedia.org/wiki/File:Chewing_Gum_on_Berlin_Wall_(1).JPG"
notes: ""
summary: "Des fragments, une pensée en attente de partage, sans appartenance, s'élançant d'une impossibilité, dérivant d'une interrogative intuition à propos des éternelles mutations de la fin du roman. Une fin qui n'a pas eu lieu. La perpétuation du récit romanesque pour en repousser attente, imminence. Dans une histoire linéaire intuitive, la fin du roman naît dans l'immédiat passage du siècle, quelque part entre les années 1990 et 2000. Apogée terminal du post-modernisme, la fin du roman mime celle de la fin de l'Histoire, l'ouverture à la pluralité par la Chute du Mur, l'effondrement concomitant des grandes suites narratives."
citation: "comme une fiction en coma"
---

Des fragments, une pensée en attente de partage, sans appartenance,
s'élançant d'une impossibilité, dérivant d'une interrogative intuition à
propos des éternelles mutations de la fin du roman. Une fin qui n'a pas
eu lieu. La perpétuation du récit romanesque pour en repousser attente,
imminence.

Dans une histoire linéaire intuitive, la fin du roman naît dans
l'immédiat passage du siècle, quelque part entre les années 1990 et 2000.
Apogée terminal du post-modernisme, la fin du roman mime celle de
la fin de l'Histoire, l'ouverture à la pluralité par la Chute du Mur,
l'effondrement concomitant des grandes suites narratives. Affirmation
aussi de l'individualisme par l'élaboration de la figure du penseur
intempestif, celui qu'on ne tarde pas --- sur le moule de Baudelaire ---
à qualifier d'antimoderne. Intuition aussi d'une certaine confusion
politique : on ne pourrait plus croire au progrès. Le romancier feint de
s'interroger pour la première fois : et si l'innovation formelle avait
atteint ses limites, n'avait plus comme horizon que redite ou non-sens ;
reprendre les formes canoniques du récit ou s'égarer, qui sait, dans le
roman non figuratif ?

Histoire littéraire instinctive, au hasard des formules : écrire, ce
serait toujours repartir de rien, s'exprimer après le désastre, la
déconstruction. Sous sa forme bassement prosaïque, on pourrait revenir à
l'inconscient à l'œuvre. Un romancier aligne des mots, une partie (la
meilleure ?) de lui-même n'est pas entièrement consciente des formes, de
leur stratégie éditoriale, voire de leur conception d'emblée
marchandisée. Mais ne serait-ce pas réduire l'écriture à son essence, à
emprunter la pensée de Blanchot qui anime (peut-être paralyse aussi)
cette fin de siècle.

Aujourd'hui, penser la fin du roman, la manière dont sans cesse elle
recommence, se fait à partir de cette évidence : tout roman est
collectif. Au minimum, il est relu, amendé par son éditeur. Puis, plus
important par chacun de ses lecteurs qui en fait instrument ou dérivatif
de ce qu'il s'approprie, usurpe aussi, comme illusion de l'air du temps.

La fin du roman comme médium de son perpétuel début s'initie en
interrogeant la conception de l'auteur qui informe et déforme toute
époque. Moment intéressant, le roman se désintéresserait non tant du
progrès social que de la possibilité d'inventer une forme qui rentre en
résonance --- en prophétie --- avec ses revendications les plus
avancées. La mort du roman serait celle de l'acceptation par une
savante, cynique, dérision. Exigence de l'époque, le roman se fait
divertissant, il devient drôle par obligation. Son auteur se réduit
alors à un personnage, moins il a d'importance, voire d'implication,
collective, plus il se sent contraint de nous raconter sa vie, de
manière plaisante, comme une fiction en coma, faute de lui adjoindre de
l'imagination, cette absence à soi où mettre en scène ce qui dépasse.

Dans le même temps, il devient entendu (à force d'être répété) qu'on ne
lit plus les auteurs sérieux, intemporels, indépassables. Repartons
autrement : le livre univers, systémique, total, manque peut-être moins
de lecteurs que d'auteurs. Notons d'ailleurs que, comme un
pressentiment, ceux qui surnagent écrivent des œuvres, de Rodrigo Fresán
à Roberto Bolaño, hantées par la disparition de l'auteur. Ou plus
simplement, l'auteur n'est pas mis à mort par ses personnages mais par
celui, public, qu'il ne peut plus, sincèrement, jouer. *Cela
transformait un récit barbare d'injustices et d'abus, un hululement
incohérent sans début ni fin, en une histoire bien structurée où il y
avait toujours la possibilité de se suicider.* L'auteur ne serait plus
celui d'une œuvre achevée, reconnue, mais celui de l'œuvre en train de
se faire, à l'écoute de la mort entre ses mots. Héler des spectres,
rejoindre *les morts* qui *continuent de vivre dans les endroits les
plus insoupçonnés, presque toujours très proches. Dans le bref soupir de
silences entre un mot et un autre, par exemple.*

S'il s'agissait que le roman continue à renaître, on devrait l'inventer,
sérieusement, comme le statut mort de l'auteur. On se demanderait alors
si le romancier n'est pas remplacé par un ajustement automatisé de mots
aléatoires uniquement pour préserver un folklore : le rôle de l'artiste,
la valorisation de son aspiration singulière, ferment d'un
individualisme qui confine dans une comédie, une autofiction.

À son meilleur, le roman serait un écart, un emprunt aussi des théories,
des renouveaux de son moment historique. Simultanément mise à la
question de l'individualisme et du progrès. À son apogée, du moins sous
sa forme telle que nous la connaissons et peinons à dépasser, le roman
atteint à la pointe extrême de l'instant par son aspect intempestif, son
rejet craintif d'une avancée technologique ou sociale dont il enregistre
les soubresauts. Le roman a par essence un aspect réactionnaire,
ressuscite un paradis perdu. Pensons ici à Balzac et sa Restauration.
Prenons garde de ne pas en faire un modèle unique, contesté puis dépassé
jusqu'à ce qu'il paraisse impossible de se conformer à son exigence de
dire une réalité sociale, politique, qui dépasse le strict individu.

Temps d'inventer une virtualité a-linéaire du progrès, surtout
esthétique. Caricaturons cette vision pour s'en départir : chaque
romancier irait plus loin que ses prédécesseurs. Savoir alors si la
matière première d'un roman, sa source d'inspiration (expression
révélatrice du romantisme de cette conception) serait de diverger de ce
qui a été fait. Sous-entendu de faire mieux, de se distinguer, souligner
sa singularité, s'inventer comme auteur de mots uniques.

On le sait : tout a déjà été dit. La mort prétendue du roman laisserait
alors surgir ses instants d'apothéose. Les années folles avec Proust,
Joyce, Kafka, Musil, Broch, Jahnn aussi par coquetterie. On pourrait
alors tracer ce cercle : ce climax aurait été inventé comme
justification à cette prétendue fin du roman au seuil de l'an 2000. La
meilleure excuse pour ne rien tenter (ne pas se lancer dans le livre
numérique) serait de prétendre que tout a déjà été fait. Comme s'il ne
fallait pas répéter, reprendre, altérer, transformer en mythe, qui sait,
affronter l'immuable, le sacré --- bref les virtualités dont s'alimente
le roman.

Si le roman reflète une époque, il en réfléchit les mythes. Il ne peut
sans cesse en répéter --- comme pour s'y conformer --- l'absence
d'inscription collective. À chaque fois que l'on parle de la fin du
roman, de son épuisement dans la caricature de ses formes les plus
vendeuses, le malaise dans la civilisation pointe son ombre. La première
mort du roman aurait alors été orchestrée par le surréalisme. La
marquise sortit à cinq heures, la suite viendrait automatiquement.
Pourtant du *Mont Analogue* à *Mon corps et moi*, d'*En joue !* à
*Aurora*, le surréalisme a trouvé son achèvement dans le roman. La mort
du roman : une fiction, l'ébauche d'un roman, le rappel de l'exigence
d'autre chose.

On compose avec ses fantômes ; on hèle des spectres ; on rejoue son
sacrifice. Le roman serait un genre fini : sa mise en mort continue à
agiter quelques promesses. On épuise un discours, on en caricature les
excès, on en pastiche les immuables structures. La fin du roman
coïnciderait avec l'épuisement de la survivance d'une pensée
messianique.

> *et nous l'évoquerons encore demain, tant que nous le pourrons, même
> si ce n'est que sous la forme d'un pâle écho de l'originel, un écho
> affaibli et de plus en plus incertain, un malentendu d'année en année,
> de décennie en décennie, un souvenir disloqué dont l'univers a
> disparu, et qui ne fait plus exploser le cœur des hommes*

*Et à quoi bon cette grande incertitude ?* Ultime refuge, miroir d'un
plat individualisme, sa religiosité confuse, mondialisée ?
L'effondrement devient un lieu commun, une esthétique, un dernier
message qui sera dit, détourné, dans différentes couches de langage avec
une certaine brillance par Krasznahorkai.

La fin serait une annonciation, un retour, un tango. Le roman tournerait
sur lui-même, jusqu'au vertige. Il ne lui resterait plus que le souvenir
inquiet, absent, des grandes suites narratives. Les différentes étapes
bibliques sur lesquelles pourrait se fonder un récit. Entendre une
cloche qui n'existe pas serait le signe annonciateur de la dissolution
d'une coopérative ; la dernière baleine mystique serait celle empaillée
par un --- possiblement seulement --- diabolique montreur de monstres.

À la lueur de l'époque, le roman préserve une attente inquiète, retrouve
les chemins de la ruse et de l'exil. On peut trouver que les romans
hongrois de Krasznahorkai versent dans la déliquescence : les récits de
fin du monde accompagneraient la fin du roman ; l'apocalypse serait
révélation d'un renouveau. Dans *Le tango de Satan* ou *La mélancolie de
la résistance*, opportunément, l'auteur suspend le sens terminal, le
repousse dans une interprétation compliquée.

L'échappatoire serait d'exporter ce sens manquant, d'aller voir ailleurs
si son immuable interrogation se perpétue. Toujours dans cette ironie
joueuse, dans toute son ambiguïté, rejoue la crainte, fin de siècle,
donc d'une fin du monde, *comme cette vie absurde qui se précipite vers
l'incompréhensible destruction collective. Guerre & guerre* ; un type
trimballe un manuscrit, veut faire connaître au monde entier
l'hermétique message, les histoires inachevées, les romans en miniatures
qui rejouent la chute de l'empire, de notre monde connu, *la limite*,
border of the world, *du monde, et par conséquent, des certitudes,
des thèses démontrables, de l'ordre et de la clarté, autrement dit : les
limites de la réalité, la frontière entre d'un côté la réalité et de
l'autre l'incertitude, l'irrésistible pouvoir des thèses non
démontrables, l'insatiable soif d'obscurité, d'opacité, la quête
d'impossible, d'irrationnel*. On passe à autre chose avant qu'il ne soit
trop tard. Le roman n'a peut-être rien d'autre à offrir.

À l'instar de l'itinéraire de Krasznahorkai dans ses romans japonais, la
fin du roman, le prétendu effondrement d'une pensée collective, laisse
perdurer le manque qui --- selon la célèbre formule de Bataille ---
serait ce qui définit l'homme. Plutôt que dans une spiritualité confuse,
croire que cela passe par l'expérience, intérieure, du sacré.

Dans *Seiobo est descendue sur terre* comme dans *Au nord par une
montagne, au sud par un lac, à l'ouest par des chemins, à l'est par un
cours d'eau*, l'inquiétude dite métaphysique laisse place à une
fascination, panique bien sûr, pour l'immuable d'une création artistique
rendue à sa matérialité. Là aussi, non sans une certaine ironie.
Marqueur de son époque, Krasznahorkai l'est quand il incarne cette fin
du roman : on ne peut vraiment le lire que dans l'incroyance, une
certaine distanciation au récit auquel, coupablement, on se laisse
totalement prendre. Question de style, bien sûr : virevoltes haletantes
de ses phrases, suspensions de sa syntaxe. Le sacré heureusement
continue à manquer, à chacun de l'interpréter. Maintenant, sans doute
peut-il sortir de ses modalisations passées. Sans doute en recommençant
exactement au même point : en se demandant, tout en l'écrivant, comment
et pourquoi tenter de chasser la peur, de faire communauté, en racontant
une histoire.

L'échappatoire se trouve dans un roman, par hasard, par une formule aux
allures de pirouette : le roman n'est pas mort ; il n'est jamais né.
Dans une lecture intuitive de l'histoire littéraire, il paraît
difficile, peu souhaitable, de dater ou de situer le premier à avoir
voulu se cantonner à une identité générique. Suite ininterrompue
d'altérations formelles, de modifications techniques : chaque roman,
conscient de ses enjeux, devrait informer son support, renvoyer dans
l'ombre tout ce qui auparavant s'appelait roman. Laisser aux cuistres la
joie d'appeler roman ce qui n'est qu'écriture d'un récit, création de
cette imminence, de cette tangence au sacré, de cette reconnaissance en
dépassement qui font que le lecteur pourra y voir un miroir fractal de
son moment historique.

La mort du roman serait toujours à recommencer ou plutôt à effacer. Par
un simple changement d'optique : refusons le linéaire qui ferait une
histoire littéraire où des auteurs, voire des époques, vaudraient mieux
que d'autres. La valeur d'un roman tient à ce que le lecteur y trouve,
peut-être aussi à ce qu'il parvient à en dégager, à en écrire ou en
diffuser, comme révélateur de l'époque. L'auteur ne fait pas seul son
livre, il dépend aussi d'une parole critique, des intuitions et du sens
qu'elle y reconstruit. Dans une version optimiste, penser la mort du
roman ne serait-ce une façon de s'abstraire de passer au crible ses
éléments révélateurs, de les recomposer --- les amalgamer à une autre
lecture --- pour construire la part romanesque de l'Histoire. Tant qu'il
n'est pas piraté, approprié, détourné sans doute de son sens, un roman
n'est pas mort mais jamais né. Par quel moyen technique, sous quelle
forme, se pratique ce piratage restera le marqueur le moins incertain de
ce qu'est le maintenant.
