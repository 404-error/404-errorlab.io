---
persona:
  - Étienne Michelet
title: "Fragments pour une théorie Vampire II"
slug: "fragments-pour-une-theorie-vampire-2"
date: 2023-05-03
echo:
  - ontique
images:
  - "img/230501-michelet-fragments-pour-une-theorie-vampire-2.jpg"
gridimages:
  - "img/230501-michelet-fragments-pour-une-theorie-vampire-2-grid.jpg"
sourceimage: "Étienne Michelet"
summary: "La pratique méditative tibétaine dite de la contemplation du squelette blanc n'a rien de macabre. Bien au contraire, elle tente de percevoir les manifestations du vivant en sondant l'invisible, en retirant la peau des choses. Le squelette est ainsi découvert, il devient médium du vivant. À cent lieues de la mort, son masque blanc est celui de la vie."
notes: ""
citation: "Le Vampire se voit être en étant voyant de l’être."
poetry: false
hangindent: false
---

## La Contemplation du Squelette Blanc


La pratique méditative tibétaine dite de la *contemplation du squelette
blanc* n'a rien de macabre. Bien au contraire, elle tente de percevoir
les manifestations du vivant en sondant l'invisible, en retirant la
*peau des choses*. Le squelette est ainsi découvert, il devient *médium*
du vivant. À cent lieues de la mort, son masque blanc est celui de la
vie.

{{%asterisme%}}

Comment ne pas "puer le zen[^1]" ? L'aurait-on dit de John Cage à son
époque ? Mais voilà, aujourd'hui tout est méfiance. Le discours moral
s'est amplifié, et les illuminés éteignent des lampes plus qu'ils n'en
allument.

{{%asterisme%}}

Le Vampire ne se reconnaît plus dans le miroir que lui tend le monde.
Parce que les voix manquent de dissonance. L'apatride n'a pas de
manifeste.

{{%asterisme%}}

Le Vampire reconnaît seulement le *devenir*. Il considère la révolution
cyclique des retours ou la réitération des cycles dans un seul présent
qui, par ses mutations, ne sera jamais tout à fait le même. Le cœur est
irrigué de sang, sa pulsation est celle d'une révolution. Le souffle
vascularise le vivant. La révolution est comprise ici dans son sens
cardiaque[^2]. Le *devenir* est le présent *révolutionné* dans son
mouvement de mutation perpétuelle.

{{%asterisme%}}

Cette révolution silencieuse et invisible n'a nul besoin de faire
communauté. Elle ne cherche pas l'assentiment ou la visibilité.

{{%asterisme%}}

*Et dans cet abandon*\
*Ne demeure pas abandonné*[^3]

La méditation *seon* n'est pas une pratique de développement personnel,
ou une simple régulation des états émotionnels. L'abandon est l'un des
principes du *devenir*. L'abandon dans le *devenir* a pour centre le
Vide apatride. Pour le dire autrement, l'abandon dans le *devenir*
n'abandonne jamais celui qui accepte d'y dériver.

{{%asterisme%}}

Le Vampire s'en tient à cet abandon. Car il réaffirme à chaque instant
la possibilité d'un changement. L'immobilité du *seon* est la promesse
d'embrasser le mouvement, d'en reconnaître le centre. L'informe contient
dans son noyau la possibilité d'engendrer toutes les formes. Il ne
limite plus la production ou la naissance du monde visible à une forme
prédéfinie. Cet impossible neutre, ce noyau sans nom, qui *est* avant le
langage, n'en demeure pas moins le cœur du monde et le centre du vivant.
Son énergie est celle qui engendre toutes les mutations.

{{%asterisme%}}

Parler de méditation comme de développement personnel, de sophrologie du
bonheur, de pleine conscience est encore un avilissement. Le méditant du
*full consciousness* est un sujet-limite, en proie aux maux de son
époque qu'il cherche seulement ici à *antidoter*, avant de retomber dans
ses pulsions. Ces *temps morts* alimentent le cycle des pulsions, leur
donnant un rythme.

Mais le Vampire s'y refuse. Sa méfiance est intuitive mais jamais
analysante.

{{%asterisme%}}

Le Vampire a pour seul précepte de n'en avoir aucun. L'humain veut
retenir l'humain dans l'humain, pour le préserver du *devenir*. La
pratique du *seon* est une pratique du *devenir*. Elle libère le corps
et le mental dans un mouvement qui s'accorde au mouvement invisible du
monde. Elle inscrit aussi le Vampire dans la mutation des phénomènes qui
a lieu en silence, sans l'analyse du langage. La pensée n'est plus
cernée par un sujet, elle s'écoule, touche l'ensemble des phénomènes, en
prend connaissance, mais sans en être assujetti en retour. Ainsi, le
*seon désassujettit* le Vampire, il lui permet d'être dans ce qu'il
n'*est* pas, c'est-à-dire de *devenir* dans l'autre. Il lui donne cette
possibilité d'être au cœur même des transformations du vivant. Car il
n'y a de *soi* possible que dans l'autre. La mutation est alors une
libération dans l'inhumain. Elle est aussi une raison d'espérer. C'est
une foi dans la mutation, dans le *devenir* de l'être. Elle est cette
foi dans l'incertitude. L'œil du Vampire est un œil noir. Sans précepte,
il est tourné vers l'*inenvisageable*.

{{%asterisme%}}

Ce *devenir* n'est pas l'image d'un lointain inaccessible. Il est
incarné dans le sang, dans une métaphysique de l'intracorporel. Le
cosmos est perceptible dans la vascularisation du souffle. Il est
présent dans la salive, dans le squelette.

{{%asterisme%}}

Lors de la méditation, les yeux ne sont pas fermés mais restent
entrouverts. Sans aucune tension, le regard ne se porte sur rien. Il n'y
a plus d'objet de vision. D'une certaine manière, c'est le regard qui
est *regardé*, comme la pensée est *pensée*, jusqu'à être creusée,
jusqu'à ce que l'ossature apparaisse, débarrassée des représentations du
visible. Le regard est vidé de tout sujet. Il n'y a plus de
*re-présentation*, mais seulement du présent. L'œil ne cherche plus à
voir ce qui l'environne, il est vu de l'intérieur. Ou mieux, l'œil est
vu du point de vue de l'œil.

{{%asterisme%}}

Le *seon* permet ainsi de voir, de *se voir* sans passer par la
représentation ou par une image de soi qui prendrait la fonction de
double, comme le ferait un miroir, créant par là une distance avec
l'*être*. Le miroir n'est qu'une image distanciée et extérieure. Elle
*re-présente*, mais n'est pas le présent de l'*être*. La méditation
*seon* est un moyen de se *voir être* tout en *étant*. Elle rend
possible cette unité, cette simultanéité de la vision et de l'*être*. Le
Vampire se voit *être* en étant voyant de l'*être*.

{{%asterisme%}}

Être Vampire dans le *seon*, c'est voir au-delà de la représentation.
Cette perception ou cette acuité est le principe même de reconnaissance
des phénomènes. Nous percevons le monde en étant au monde, mais nous ne
percevrons véritablement les phénomènes qu'en prenant une distance avec
les mouvements du monde. Ce retrait, ce détachement encore une fois, est
ce qui révèle l'*être*.

{{%asterisme%}}

La méditation *seon* donne à voir le monde au-delà des représentations.
Elle donne à voir la présence dans ce Vide apatride qui agit sans agir,
dans le silence de l'absence des phénomènes.

{{%asterisme%}}

Le Vide apatride est le centre de l'être, il en est le cœur, et c'est à
partir de lui que naissent les phénomènes. L'acuité totale pour le cœur
de la viduité dévoile le noyau silencieux du monde. Ce magnétisme sans
visage traverse le Vampire, le relie ainsi à la totalité du monde.

{{%asterisme%}}

*À quelle distance je me trouve de la pensée quand je pense ainsi,
au-dedans du squelette, quand je tente de percevoir le centre de la
pensée ?*

*Au-dedans du squelette blanc, je perçois le squelette blanc qui est
l'enveloppe, qui est la substance sans substance, et ma pensée naît elle
aussi à l'intérieur du squelette blanc.*

*Dans la cage thoracique du squelette blanc, le cœur bat et le souffle et
le sang circulent. Ma pensée devient fluide comme le sang, et ma pensée
est devenue le sang.*

*Le sang me traverse, comme le souffle, comme la pensée, à l'intérieur du
squelette blanc.*

*Je souris ainsi de savoir que le sang est dans mon sourire, de percevoir
dans le sourire le sang qui est dans le sourire.*

*Le squelette blanc est visible dans l'invisible. La colonne du souffle
s'élève comme la colonne vertébrale du squelette blanc.*

*Le squelette blanc est une île de vigilance. Le squelette blanc est une
île de silence.*

[^1]: Selon l'école japonaise du Zen Rinzai.

[^2]: La révolution cardiaque est l'ensemble des mécanismes permettant le fonctionnement du cœur.

[^3]: Tao Te King, 2, Lao Tseu.
