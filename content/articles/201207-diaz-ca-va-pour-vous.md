---
persona:
  - loan diaz
title: "Ça va pour vous"
slug: "ca-va-pour-vous"
date: 2020-12-07
echo:
  - rythmique
images:
  - "img/201207-diaz-ca-va-pour-vous.jpg"
gridimages:
  - "img/201207-diaz-ca-va-pour-vous-grid.jpg"
sourceimage: "https://commons.wikimedia.org/wiki/File:The_Mark_of_Cain.jpg"
notes: "Ce texte est dédié au domaine public. Il est mis à disposition selon les termes de la Licence Creative Commons Zero (CC0 1.0 Universel)."
summary: "Ça va pour vous les assassins purs à l'âme exaltée par le fracas et les gouffres fumants, esclaves de la lave des attaques quand le tocsin bat, tous exultent parmi les pleurs ; Voulez-vous vivre ? Impossible --- on n'a pas le droit. Ils tondent les recrues dont les yeux s'embuent --- disparus déjà dans le feu, la sueur, les crevasses des plages salines et comme une immense masse de cris et de colère, ils hennissent endiablés, tourbillonnent ou se vautrent dans l'aveuglement et la ouate ;"
citation: "des bruits barbouillés de poussière courent sur la pointe des couteaux"
---

Ça va pour vous les assassins purs à l'âme exaltée par le fracas et les
gouffres fumants, esclaves de la lave des attaques quand le tocsin bat,
tous exultent parmi les pleurs ;

Voulez-vous vivre ?

Impossible --- on n'a pas le droit.

Ils tondent les recrues dont les yeux s'embuent --- disparus déjà dans le
feu, la sueur, les crevasses des plages salines et comme une immense
masse de cris et de colère, ils hennissent endiablés, tourbillonnent ou
se vautrent dans l'aveuglement et la ouate ;

Ils rampent sur les cités, font sauter l'écorce de l'agonie, et pour que
la mort s'excite, des wagons se ruent dans l'enflure de la nuit sous les
entassements d'étoiles, rongées par les lueurs d'obus...

{{%asterisme%}}

Des bruits barbouillés de poussière courent sur la pointe des couteaux,
et d'une main noire, réunissent sous une même charge mille autres mains
levées comme des armes au pas --- au pa-ta-tras des mitrailleuses

Pour le Roi aux dents arrogantes et au sourire de tranchées, de tueries,
de monstres, d'hyperboles de cendre...

Le monde a brûlé dans un ordre terrible de la tête aux talons dans
l'arène terrestre

Éternels arbitres, les océans en flammes serrent les pôles exorbités

Retiennent leur souffle...

L'instant

vole

en

éclats

bascule --- se bouscule

La fusillade écume en feu d'artifice monstrueux, dévore le silence et le
printemps par l'éclair des batteries qui se bâfrent de leur fosse
chauffée à blanc

et les airs

et les eaux

en larmes comme les morts

et encore une aube sacrifiée dans la constellation des poudrières où le
premier sang fut versé

goutte à

goutte dans l'eau pour faire le thé des fossoyeurs, aux pattes rougies
de fouler des centaines de têtes, comme à domicile dans la canonnade,
heureux d'un coup de crosse, anges sans dieu ;

Sur la baïonnette se replient et se tuent les charognes

au vent pourrissant

et personne

ne prie...

{{%asterisme%}}

Voici le papier blessé, troué et tordu de la mémoire

et des corps --- cimetière au cœur des vers,

langue des braseros éventrés par les dernières bouffées

d'air...

<span style="display:inline-block;margin-left:7em;"></span>Le temps s'écrie dans le craquement des vies achevées...

C'est moi ! L'idole décapitée, le rugissement ivre de l'amour disloqué ;

Je suis seul la peau du monde, les monceaux de sang coagulé en briques ;

C'est moi ! Le fil rouge jeté hors des ténèbres jusqu'à ce qu'il se
casse ;

Je recueille des morceaux altérés et je deviens tout entier un ---
extrait...

&nbsp;

Je suis un reste de couleur, de quoi combattre l'Avalanche et le
désespoir futur,

Veux-tu la Fleur ? J'éclaterai parmi les bosquets !

{{%asterisme%}}

Ils disent que nous devons être reconnaissants ?

Ne dis plus merci à la main qui te nourrit

Ni aux quelques dollars qui agressent le monde

Ni aux prisons des titres de propriété

Ni à ces illusions qui se déroulent en tapis de goudron sous nos pieds
entaillés ;

&nbsp;

Fuis les fictions suprêmes et les prophètes du Grand Soir,

Mais n'oublie pas de t'émouvoir

des émeutes, des aventures absurdes et de la possibilité d'un Isthme,

&nbsp;

Sois simplement reconnaissant de vivre ce jour

Tel qu'il est ;

&nbsp;

Multiplie l'enthousiasme,

Édifie dans l'aurore --- la joie dans le flanc ouvert des canons,

Dresse un drap d'or sur le lit des mers ébréchées par les mines,

Étends-toi jusqu'aux sables et soulève en un cri d'océan les os
ensevelis,

Et les corps des tréfonds remonteront à la nage

Ensemble, parmi les débris d'arcs et de ciels...

&nbsp;

Un écho roule : c'est le jour qui se déroule tel un soleil

La Terre ouvre son cœur taillé par les siècles

À chaque vivant ;

&nbsp;

Nous pouvons enfin dire :

Ça va pour nous !
