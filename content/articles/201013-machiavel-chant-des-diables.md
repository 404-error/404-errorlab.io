---
persona:
  - Nicolas Machiavel
title: "Chant des diables"
slug: "chant-des-diables"
date: 2020-10-13
echo:
  - rythmique
images:
  - "img/201013-machiavel-chant-des-diables.jpg"
gridimages:
  - "img/201013-machiavel-chant-des-diables-grid.jpg"
sourceimage: "https://commons.wikimedia.org/wiki/File:Le_combat_de_Carnaval_et_de_Car%C3%AAme_Pieter_Brueghel_l%27Ancien.jpg?uselang=fr"
notes: "Ce *chant de carnaval*, rédigé par Machiavel en 1524, est publié dans la traduction de Périès."
summary: "Nous fûmes autrefois des esprits heureux, aujourd'hui nous ne le sommes plus ! Notre orgueil nous a précipités des hauteurs du ciel, et nous avons pris le gouvernement de votre cité, parce qu'on y voit régner, plus que dans l'enfer même, et la discorde et la perfidie."
citation: "Nous avons peu à peu introduit dans ce monde, et donné en partage à chaque mortel, la faim, la guerre, le sang, et la glace, et le feu."
---

Nous fûmes autrefois des esprits heureux, aujourd'hui nous ne le sommes plus ! Notre orgueil nous a précipités des hauteurs du ciel, et nous avons pris le gouvernement de votre cité, parce qu'on y voit régner, plus que dans l'enfer même, et la discorde et la perfidie.

Nous avons peu à peu introduit dans ce monde, et donné en partage à chaque mortel, la faim, la guerre, le sang, et la glace, et le feu. Nous venons, durant ce carnaval, demeurer avec vous, parce que nous avons été et que nous serons toujours le principe de tout mal.

Celui-ci est Pluton, celle-là est Proserpine : c'est elle qui est assise à ses côtés, et dont la beauté l'emporte sur celle de toutes les femmes de l'univers. Amour, qui triomphe de toute chose, a su triompher de lui ; car jamais il n'a de repos jusqu'à ce que tout le monde fasse ce que lui-même a fait.

Tout plaisir, tout chagrin d'amour, est engendré par nous, et les pleurs, et les ris, et les chants, et la douleur. Quiconque est amoureux obéisse à nos lois, et il sera satisfait ; car notre unique plaisir est de faire le mal.
