---
persona:
  - Jérôme Orsoni
title: "Ruissellements Γ"
slug: "ruissellements-gamma"
date: 2021-12-10
echo:
  - rythmique
images:
  - "img/211210-orsoni-ruissellements-gamma.jpg"
gridimages:
  - "img/211210-orsoni-ruissellements-gamma-grid.jpg"
sourceimage: "https://commons.wikimedia.org/wiki/File:Cezanne_-_Still_Life_With_Skull.jpg"
notes: ""
summary: "Imagine une vie sans rien dedans / sans peine ni haine ni rien dedans / c'est vrai / combien de vies perdues de ces vies perdues dans la mienne / combien de vies perdues à écouter les voix / des bouches qui n'ont rien à dire / dont le creux est éternel / et que ne remplit jamais que le réel amer / d'un mur de pierres / quand il s'écroule ? / une vie sans rien dedans / sans peine ni haine ni rien dedans / un cri peut-être / d'amour certes / cette nuit un peu avant deux heures du matin / quand je me suis couché dans mon lit / j'ai pris une décision / ou plutôt une idée m'est venue"
citation: "l’être en néant / et le néant en être"
---

Imagine une vie sans rien dedans\
sans peine ni haine ni rien dedans\
c'est vrai\
combien de vies perdues de ces vies perdues dans la mienne\
combien de vies perdues à écouter les voix\
des bouches qui n'ont rien à dire\
dont le creux est éternel\
et que ne remplit jamais que le réel amer\
d'un mur de pierres\
quand il s'écroule ?\
une vie sans rien dedans\
sans peine ni haine ni rien dedans\
un cri peut-être\
d'amour certes\
cette nuit un peu avant deux heures du matin\
quand je me suis couché dans mon lit\
j'ai pris une décision\
ou plutôt une idée m'est venue\
qui était pour moi décidée\
et ce matin j'ai pris une paire de ciseaux\
et j'ai coupé les mèches de mes cheveux\
que j'avais longs jusqu'aux épaules\
et puis le reste je l'ai rasé\
tant qu'il n'est rien resté\
qu'une mince pellicule de poils sur mon crâne\
comme un vernis plus ou moins sombre\
plus ou moins opaque en fonction des endroits\
j'ai regardé mon visage\
et j'ai vu que c'était une version plus vraie de moi\
non ce n'est pas cela\
mais celui que j'ai vu là et que je connaissais\
je me suis dit que je pourrais avoir envie de lui ressembler\
encore dans une centaine d'années\
quand il sera trop tard\
que ne resteront plus de cette vie que la peine et la haine\
qu'on aura mises dedans\
autant de vies vécues que de vies gâchées\
les cheveux rasés j'ai caressé le marbre de mon crâne\
sous le vernis animal\
et je me suis senti vivant\
comme il y avait longtemps que je ne m'étais senti vivant\
et je me suis dit\
imagine une vie sans rien dedans\
une vie sans peine sans haine ni rien dedans\
serais-tu prêt à la vivre maintenant\
sans plus nul délai\
cette vie ?\
j'ai senti un souffle d'air sur mon crâne\
comme un envol quand il y a trop de marques de ponctuation\
trop de signes trop de messages trop de sens\
j'ai mis la masse de mes cheveux coupés dans un sac\
j'ai aspiré le reste\
rien n'avait disparu de la haine ni de la peine\
rien n'avait été effacé par ce geste intime\
et insignifiant presque\
mais il avait été décidé\
inscrit\
je n'ai plus pensé à rien\
enfin j'ai essayé\
et j'ai imaginé une vie sans rien dedans\
une vie sans haine ni peine ni rien dedans\
elle venait de commencer ---\
toutes nos épiphanies sont négatives ---\
que faire des nuages\
où ranger les gens ?\
quand tu essaies de t'en saisir\
(des nuages)\
ils te glissent entre les doigts\
laissant après leur passage\
un sentiment humide\
souvenir qui préfigure la vie\
au bout de son doigt tendu\
il désigne un immeuble d'habitation\
que l'on ne voit pas\
il le dit\
il est caché par un autre\
il l'explique\
et pourtant moi\
j'ai un goût de béton dans la bouche\
je pense à des phrases\
à des lèvres arrondies autour de mots\
qu'on aurait mieux aimés nondits\
et puis d'autres au contraire\
qu'on aurait mieux fait d'expulser\
au lieu de s'excuser\
regardant les nuages au-dessus de la falaise\
et pensant à l'immeuble qu'on ne voit pas\
derrière l'immeuble que l'on voit\
je pense à tout cet air comprimé\
et à l'injonction de s'exprimer\
je garde le silence une seconde de plus\
que vaut-il mieux\
se pendre ou se répandre ?\
quel sentiment dire ou quel sens donner ?\
quand la communion avec l'être les êtres l'univers est impossible\
que faire des rêves ou des souvenirs\
pourquoi garder des mots\
dans le creux de la main\
comme coquilles vides\
et nos idéaux périmés ?\
quelle chance donner\
à la langue de parler\
sinon oublier\
tout ce qui nous précède\
et jeter un regard sauf neuf\
un regard veuf\
sur l'univers ?\
il y a un peu d'air ce soir dans l'air\
je respire\
hier une femme a hurlé\
au beau milieu de la nuit\
quelque insanité\
rêve délirant\
des horreurs à un homme\
qui s'est enfui\
la fille criait aussi\
et je ne comprenais rien\
ensuite je me suis rendormi\
dans un lit moite\
où je ne me suis pas senti chez moi\
comme si un songe ennemi avait pénétré dans ma vie\
et avait pris possession de ce sur quoi\
l'autre régnait en maître\
dans ma maison\
il n'y a plus rien dans l'air à présent\
que le ventilateur qui vrombit\
pâle copie du vent\
il n'y a plus rien dans l'air\
j'entends des voix qui viennent\
de l'au-delà\
cris et puis sirènes aussi\
véhicules qui hurlent à leur tour\
il n'y a plus rien dans l'air\
j'attends un choc qui ne vient pas\
comme l'air dans les pièces\
où l'on respire\
le souffle muet de l'existence\
contre toute vraisemblance\
un pigeon qui entre par la fenêtre\
est un pigeon perdu\
on n'a le droit de rien\
sinon de le libérer\
j'entends le bruit que font les oiseaux quand ils vivent\
il n'y a plus rien dans l'air\
qu'une forme de paix mortelle\
comme après un trop long silence\
que quelqu'un rompt\
par un mensonge\
dans ma main la pierre\
et je me souviens qu'il faisait chaud\
cet après-midi-là dans ce village méditerranéen\
cépie en juillet\
j'avais roulé pour venir\
et puis roulé encore pour ne pas repartir\
et quand j'avais croisé cet homme\
avec son chien qu'il promenait sous le soleil\
je lui avais demandé mon chemin\
il était gros et avait fait\
une remarque sur l'état de santé\
qu'il fallait pour monter\
jusque là-haut\
je l'avais écouté et remercié\
un peu obséquieux\
mais ce n'était pas lui que je remerciai\
mais l'univers je crois\
j'avais rendez-vous avec un mort\
je suis remonté dans ma voiture\
et je me suis garé devant le cimetière\
que le gros homme au chien m'avait indiqué\
j'ai parcouru le cimetière\
mais la tombe ne s'y trouvait pas\
preuve qu'on peut vivre dans un village\
et ne pas le connaître\
j'ai marché quelques mètres\
et j'ai vu qu'il était là cimetière nouveau\
j'ai ouvert la grille qui ne ferme jamais et j'ai marché encore\
jusqu'à me trouver là où je voulais être là où je ne voulais pas être\
j'avais rendez-vous avec un mort\
et je lui ai parlé et je lui parle encore\
lui pose des questions auxquelles il n'y a pas de réponses\
n'y a plus de réponses n'y aura plus de réponses\
est-ce pour y répondre moi-même\
que je lui pose ces questions ?\
moi qui n'aime que les questions sans réponses\
que les questions en réponse\
j'avais pris rendez-vous avec un mort\
et j'ai ramassé cette pierre\
parce que je l'ai trouvée belle\
j'ai cueilli un rameau d'olivier\
à l'ombre duquel la tombe reposait\
je m'en souviens aussi\
mais il a séché trop vite\
les journées et les nuits étaient si chaudes\
cet été-là dans l'aude\
ne reste que cette pierre\
grise tirant sur le rose tirant sur le rond tirant sur la vie\
j'ai posé la pierre pour écrire à la main\
que je tords rotations pour écrire\
elle est lourde assez\
mais épouse la forme de ma main\
il y a une veinure\
peu profonde comme une ride\
qui la parcourt dans le sens de la longueur\
elle délimite on dirait deux hémisphères asymétriques\
genre de cerveau difforme\
pourtant quand je la tiens cette pierre\
ou quand je la regarde cette pierre\
je n'y vois nul sens nulle irrégularité\
elle me semble chose parfaite\
c'est l'image que je m'en fais\
zones de couleur plus ou moins nettes plus ou moins franches\
gris rose blanc\
encore plus visibles sous l'eau\
reliefs comme en une planète miniature et irrégulière\
maintenant que je pose cette pierre sur ma tête je sais que\
si la pierre pouvait parler je la comprendrais\
mais elle demeure muette\
et moi qui ne sais le langage hiératique des choses monolithiques\
suis-je voué à demeurer oreille tendue\
abasourdie de ne rien entendre\
à la langue secrète des défunts ?\
fait-on des stries dans le destin\
comme des signaux de fumée des signes de croix\
quand tout a brûlé ?\
je caresse la surface accidentée de ma relique improvisée\
je rêve de fous de saints de maîtres de devins\
et j'imagine que ce rêve naïf et inutile n'a pas de fin\
ce matin quand je l'ai regardée\
la pierre m'a semblé plus rose que d'habitude\
ou est-ce que d'habitude je ne la regarde\
que le soir la nuit ?\
j'ai posé la pierre\
et j'ai regardé le paysage\
devant moi un peu au-dessus de la ligne de mon regard\
ce paysage me suis-je demandé ce paysage si je le rêvais\
serait-il différent de celui que je vois là\
ou non ?\
j'ai posé la pierre\
et j'ai pensé à tous les désirs qui se trouvent autour de cette pierre\
qui ne représente rien d'autre qu'elle-même\
et donne pourtant une forme\
la forme de la vie\
qui n'est pas semblable à une pierre\
qui n'est pas semblable à n'importe quelle pierre\
non\
ce n'est pas cela que je dis\
nul ne vit comme une pierre\
mais la pierre elle concentre la forme\
attire autour d'elle l'énergie de la vie qui ne s'y dépose pas\
mais irradie d'elle\
la pierre est la forme de la vie\
la pierre est la forme de ma vie\
j'ai levé les yeux vers le paysage une nouvelle fois\
et je me suis demandé combien de nonsens pareils à celui-là\
je serai encore capable d'inventer\
avant de trouver une phrase dont le sens me satisfasse\
dont le sens me fasse\
et puis tournant le regard du dehors vers mon carnet\
il m'a semblé de manière assez nette qu'il ne fallait pas chercher\
à résumer la vie\
la réduire à une phrase\
peut-être me suis-je dit\
peut-être que chaque nonsens\
que nous inventons nous rapproche du sens\
ou d'un sens qui ne se réduise pas à une chose trop simple\
un slogan\
un mort d'ordre\
une injonction\
mais les contienne tous\
et dès lors les rende caducs\
combien de phrases écrirai-je avant d'avoir\
non le sentiment mais la certitude\
que je suis sorti du pays du nonsens\
et que j'ai pénétré\
en territoire de sens ?\
j'écris des choses étranges\
et plus elles me semblent étranges\
et plus elles me paraissent étranges\
et plus il me semble qu'elles me rapprochent de la forme\
d'une forme qui ne serait pas unique\
monolithique\
mais suffisamment ample et vaste et souple\
pour accueillir toutes les phrases\
une forme définie de son informe\
la pierre est là\
posée à côté de moi\
avec ses reflets roses et ses reflets gris et ses reflets blancs\
et je n'ai pas besoin\
non\
de la regarder pour savoir qu'elle est là\
et pourtant\
je la regarde pourtant\
et c'est une pierre qui n'a rien d'extraordinaire\
une pierre de cimetière nouveau\
inachevé\
trop vide et qui nous montre dès lors\
toute la mort\
la mort encore à venir\
la mort qui ne cesse de venir\
les vies tout entières pour la mort\
et je me revois dans ce désert de pierres\
sous le soleil de juillet\
cépie en juillet\
dans ce désert d'attente\
l'espace destiné à accueillir les morts\
les beaux et vieux cimetières nous masquent cette réalité\
ils s'offrent à nous comme des curiosités\
mensonges pour touristes\
mouroirs photogéniques\
on se recueille sur la tombe d'un inconnu célèbre\
on prend la pose le cliché\
nul ne sent la mort en ces contrées\
l'espace vide qui est la mort\
le creux dans la vie qui se forme\
la forme de la vie toujours nette\
bien délimitée\
nous nous mentons à nous-mêmes\
touristes en visite\
cimetière plein de tombes sans trous\
dans le désert de pierres\
sous le soleil de juillet\
cépie en juillet\
dans la distance des astres\
les vides laissés clairs\
les béances de l'être\
j'ai vu la mort\
qu'est-ce que ma mort ?\
le temps ouvert\
offert\
si je trace un cercle dans le ciel\
disque sans épaisseur abstraite de la figure\
j'ai le sentiment d'avoir dessiné\
une limite autour de quelque chose\
que je puis cerner ainsi et voir ainsi\
à travers ce petit trou\
percé dans la masse de l'être\
mais je pourrais tout aussi bien rêver\
et n'en jamais rien savoir\
affairé comme je le suis\
autour de mon trou dans le néant\
j'ai beau chercher un sens\
je bute toujours sur quelque chose\
mais faut-il que le sens soit infini\
un infini ou une infinité de finis\
un plus un plus un fini ?\
je bute toujours sur quelque chose de plus dur\
que moi\
de plus fort que moi\
le roc solide indivisible qu'on m'oppose\
dure réalité de la vie\
pure extériorité crise\
éternelle crise\
éternelle ? peut-être pas\
à moins de se demander où se trouve\
la crise\
dans le monde insaisissable\
ou dans les idées maladroites que l'on s'en fait ?\
qu'est-ce que ma mort ?\
ce n'est peut-être pas la bonne question\
ou alors si afin de déjouer sans cesse\
les pièges que ceux qui parlent la langue\
nous tendent\
tire la langue pour voir\
jusqu'où elle pend\
percée dans l'être la bouche cousue\
combat contre elle-même\
un demi-siècle de crise\
autant dire une éternité\
trouée dans le néant\
je fixe un pan de mur blanc\
qui me ressemble\
pas une question de couleur même si\
non je dirais sans doute d'impassible\
d'impossible\
devenir impassible de l'impossible\
et réciproquement\
les légendes que l'on fabrique\
les mythes par lesquels on m'assassine\
ne valent pas ce petit pan de mur\
blanc\
j'ouvre une brèche dedans\
le regardant\
autre dimension de la réalité\
facture de l'immixtion\
dedans\
le regardant\
les choses sont trop réelles\
il appert\
elles engloutissent le réel\
qui se perd\
pourtant ne sont-ils pas nombreux à avoir cru\
que l'événement aurait lieu\
qui changerait tout\
l'être en néant et le néant en être\
ou tout en rien ?\
et nous alors\
qui restons de cet espoir\
les autres\
nous nommerais-je ?\
nous les autres\
qui vivons sur le tas de ruines\
tombées des corps harassés\
de qui a cru bon de nous précéder\
les autres nous dis-je\
avec quoi nous faire\
une voix\
un chant\
avec quoi nous faire une attente\
avec quoi nous faire un hymne\
avec quoi nous faire une joie\
un peu moins de désarroi\
un ordre qui ne nous détruise pas\
non\
mais nous permette de respirer\
le temps qu'il nous est donné de vivre ?\
qu'est-ce que ma mort ?\
sinon cela que je chante en attendant qu'elle vienne\
la fin qui me hante et que je fais mienne\
des bouts de rimes tombées des corps harassés\
de qui a cru bon de devoir me précéder\
des silences et des nuances de blanc.
