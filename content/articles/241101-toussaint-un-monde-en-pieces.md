---
persona:
  - Cécile Toussaint
title: "Un monde en pièces"
slug: "un-monde-en-pieces"
date: 2024-11-15
echo:
  - rythmique
images:
  - "img/241101-toussaint-un-monde-en-pieces.jpg"
gridimages:
  - "img/241101-toussaint-un-monde-en-pieces-grid.jpg"
sourceimage: "https://commons.wikimedia.org/wiki/File:Aralship2.jpg?uselang=fr"
notes: ""
summary: "— Pardon. / — Pardon. Je ne suis pas d'ici. / — D'où venez-vous ? / — Je ne sais plus vraiment ces temps. / — Pardon. J'ai de la cendre dans la bouche. / — Je comprends. C'est à cause de moi. / — C'est vous. Et c'est la poursuite du vent. / — J'ai remarqué sur mon chemin que mon chemin était de cendre. Je n'arrivais plus à dire. Si ma fuite en était la cause ou l'effet. Alors je ne disais plus. / — Il y avait de la cendre avant votre arrivée. Avant que vous ne disiez la cendre. Malgré les croyances. La cendre ne se multiplie pas. Mais se divise par elle-même. Par le zéro d'elle-même. On peut s'y tenir quelques instants. Parfois quelques siècles. Mais on finit toujours par la laisser habiter en nous. / — Je ne resterai pas longtemps. Comme vous je poursuis. Le vent. La cendre aussi maintenant. Mais en sens inverse. Paraît-il que la cendre ne supporte pas ce qui lui est étranger. Tout comme le vent. Alors j'essaie de me dissimuler. De disparaître en moi-même."  
citation: "Un ciel qui nous bombarderait par rancune."
poetry: true
hangindent: false
---

# [...]

— Pardon.

— Pardon. Je ne suis pas d'ici.

— D'où venez-vous ? 

— Je ne sais plus vraiment ces temps.

— Pardon. J'ai de la cendre dans la bouche.

— Je comprends. C'est à cause de moi.

— C'est vous. Et c'est la poursuite du vent.

— J'ai remarqué sur mon chemin que mon chemin était de cendre. Je n'arrivais plus à dire. Si ma fuite en était la cause ou l'effet. Alors je ne disais plus.

— Il y avait de la cendre avant votre arrivée. Avant que vous ne disiez la cendre. Malgré les croyances. La cendre ne se multiplie pas. Mais se divise par elle-même. Par le zéro d'elle-même. On peut s'y tenir quelques instants. Parfois quelques siècles. Mais on finit toujours par la laisser habiter en nous.

— Je ne resterai pas longtemps. Comme vous je poursuis. Le vent. La cendre aussi maintenant. Mais en sens inverse. Paraît-il que la cendre ne supporte pas ce qui lui est étranger. Tout comme le vent. Alors j'essaie de me dissimuler. De disparaître en moi-même.

# [...]

— Ils m'ont pris aujourd'hui.  

— Vous le méritiez sans doute.

— Oui. Et ils m'ont pendue.  

— C'est ce qu'il arrive quand ils vous prennent.  

— J'ai volé le pain.

— Vous ne devriez pas voler le pain.  

— Je voulais savoir son bruit.  

— Le bruit du vent devrait vous suffire.

— Mais ils ne m'auraient pas pendue si j'avais volé le bruit du vent.

— À vrai dire. Je n'en sais rien. Ces jours. Ils pendent comme ça. Sans raison. Mais nous aurions été débarrassées de tout ce bruit. De toute cette fuite bruyante loin de nous-mêmes.

# [...]

— Le réel a fondu.

— Oui. Un peu sur vos chaussures.

— J'aimais bien quand les choses se tenaient. Loin de mes chaussures.

— C'était avant que le soleil s'épaississe. Mais vos chaussures ont toujours été sales. Même lorsque nous vivions parmi le suif.

— Je préférais la graisse du monde. Mes chaussures aussi.

— Le désert déborde maintenant comme une vieille baignoire. Ils disent que la couleur brune du soleil a quelque chose de rassurant.

— Je ne comprends pas ce qu'il y a de rassurant là-dedans. Tout semble se fondre dans tout. C'est un fondu au noir de l'existence. On n'arrive plus à distinguer ce qui est de ses chaussures de ce qui n'en est pas. Moi j'aimerais voir la glace tomber du ciel. Simplement. Encore une fois. Un ciel qui nous bombarderait par rancune.

— Je mangerais bien un peu de neige moi aussi.

— Pas de la neige. De la glace qui tombe. Mais pas comme de la neige. Comme un corps qui chute.

— Cela ferait un bruit d'enfer dans ce désert. Pire que le vent. Un bruit à renverser le monde. Comme un caillou dans une flaque. Une flaque qui serait la seule flaque d'une prison vide.

# [...]

— Ils ont abattu les enfants ce matin.

— Sont-ils arrivés à les aligner ?

— Non. Ils rêvaient tout au-dedans de leurs pas.

— Ils ont dû tirer dans tous les sens.

— Oui. Ils ont même abattu un soldat.

— Par erreur ?

— Non. Je crois qu'ils pensaient que c'était un enfant.

— C'était donc par erreur.

— Si on pense de vous une telle chose. On ne vous abat pas par erreur. C'est comme tirer sur les nuages. On se venge comme on peut.

— Je vais effacer leurs pas. Il ne faut pas se souvenir. De leurs pas. De ce qu'il reste de rêve dans leurs pas.

# [...]

— Ma chambre s'est effondrée dans mon cerveau.

— C'est normal. Il reste encore des bombes dans vos soupirs.

— J'ai l'impression d'être devenue un peu moins que l'humanité tout entière.

— Il est vrai que vous avez mauvaise mine.

— J'ai pourtant effacé tout ce qu'il restait de rire entre mes soupirs.

— Vous vous surestimez. Vous n'avez jamais été très drôle. C'était plutôt que l'on riait de vous.

— J'aimerais habiter entre deux respirations. Et ne plus en sortir. Mais c'est comme si je ne pouvais plus fabriquer cet interstice. Comme si je ne pouvais plus qu'expirer. Détruire sans fin tout ce qui se maintient en moi. Comme si mes poumons devaient se dessécher plus vite que le réel.

— L'asphyxie peut avoir du bon. Rien ne nous inspire plus. Et on aspire moins à. 

— Et il y a moins de cendre dans nos bronches. 

— Moins de cendre. Et moins de tout ce qui y pousse. On se maintient sans souffle sous le réel. Comme la bactérie qui en détricote la trame.

# [...]

— Vous avez encore bu.

— Non. Je ne bois qu'avec des amies. Et je n'ai pas d'amies.

— Vous pourriez en inventer rien que pour boire.

— C'est vrai. Mais ma solitude me semble trop parfaite pour l'altérer. Surtout pour cet alcool d'oignon.

— Je vous ai vue l'autre jour avec une ombre. Malgré la brume. Cette ombre était bien réelle. Et elle sentait l'oignon.

— C'était une huissière qui venait me visiter.

— Vous buvez et vous ne payez pas votre boisson.

— Elle me racontait qu'elle a dû un jour apposer des scellés sur un rêve. Elle pensait que c'était le rêve d'une autre. Mais c'était son propre rêve. Si délabré qu'il était devenu vide. Inhabité. Elle n'y reconnaissait plus rien.

— Ça se boit le rêve ?

— Comme la javel. D'un seul trait. En souriant. Jusqu'à ce que l'on ne sourie plus.

# [...]

— Il faudra annuler le monde.

— Trop tard. J'ai coulé hier une dalle de béton sur le monde.

— Vous auriez pu attendre qu'on l'annule.

— Ne vous inquiétez pas. J'ai enfoui le rêve sous un tronc d'arbre.

— Était-ce un arbre mort ?

— Y a-t-il encore des troncs d'arbres qui ne sont pas des arbres morts ?

— Je ne le crois pas. J'en ai vu en plastique. Il y a quelques jours. Mais je ne suis pas sûre. Et je ne me souviens plus vraiment. Je dis ça comme ça. Pour croire à une différence entre le réel et le réel rêvé.

— Longtemps j'ai mangé des fruits vomitifs. Et un jour il n'y a plus eu de fruits. J'en mange encore parfois lorsque je rêve.

— Disparus. Comme les arbres. Mais peut-être quelques heures après les derniers arbres. Une histoire de causalité inversée. Quelques heures ou quelques jours. Avec la radioactivité. J'ai vu le dernier fruit pourrir. Il a résisté à ce qui revient de l'arbre en lui. Longtemps. Beaucoup trop.

— Cela devait être très beau. Surtout lorsqu'il est devenu très noir. Très absent à lui-même.

# [...]

— Vous ne serez pas remboursée.

— Je n'ai pourtant rien acheté.

— Voilà. Si en plus vous ne faites pas d'efforts.

— Quelle importance. Je n'achète jamais rien.

— Le remboursement n'a alors pas la même valeur. L'absence de remboursement non plus.

— J'ai acheté une fois un objet.

— Et vous n'avez pas été remboursée ?

— J'ai perdu l'objet avant de pouvoir y penser.

— Votre rapport au réel doit vous rendre bien suspecte à leurs yeux. 

— C'est-à-dire que j'essaie toujours de soustraire au réel. Un objet. Ma présence. Le monde.

# [...]

— Foutue guerre.

— Vous êtes en retard. De quelle guerre parlez-vous ? Je n'ai pas vu de soldats.

— Je suis navrée. J'avais l'impression d'être en avance. C'est que ma montre s'est brisée. Je la porte quand même. Vous comprenez. Pour me souvenir. Parfois il est l'heure que l'on attendait sans qu'on le sache vraiment.  

— J'ai mangé le bouquet en vous attendant. Mais vos soldats. Où sont-ils ?

— M'en avez-vous laissé un peu ? Les soldats. Ils sont tous morts avant la guerre. 

— Non. Il n'y avait pas grand-chose sur ce bouquet. Du papier surtout.

— Tant pis. Les fleurs. Ça ne se partage pas. Et puis j'ai volé un biscuit à un soldat sur la route. 

— Mais j'ai cru que tous les soldats étaient morts. C'est amusant votre guerre sans soldats. On ne sait plus vraiment qui tire sur qui.

— Le biscuit appartenait à un soldat mort. Mais il était encore bon. Il avait encore ce petit goût de carton humide. Le goût qui rappelle le goût de la guerre. Lorsqu'il pleuvait encore sur la guerre et sur les décombres de la guerre. Vous vous souvenez ? Toute guerre finit par s'effacer dans son image. Mais le goût reste. Toute guerre finit par devenir une guerre sans soldats. Ce n'est qu'une question de temps. 

— Je me souviens. Il fallait avoir la patience de laisser la mort passer. Pour entendre le bruit de la pluie. Encore. Quelquefois. Je cherche le bruit de la pluie. Pour oublier. Qui tire. Qui tire sur qui. On ne cherche plus aujourd'hui à séparer la balle du trou qu'elle cherche à faire. Il n'y a plus que le bruit de la balle. Suspendu en nous. Sans savoir si c'est le bruit du canon ou du corps qui se troue. Sans savoir le bruit de la pluie. La guerre n'est plus qu'une question de musique.

# [...]

— Il y a moins de vent ce matin.

— C'est qu'il ne reste plus rien. Le vent ne sert plus à rien.

— Il reste un peu d'ordures. Un peu de réel sous les ordures. Le vent aurait pu aider. On va encore devoir s'y coller.

— Le vent ne peut rien y faire. Il y a trop de béton. Trop de morts sous le béton.

— Le béton. C'est du sable. Le vent pourrait bien. Il sait faire avec le sable.

— On a volé au sable son mouvement. Il a fallu couler un monument aux morts avec.

— Le vent. C'est comme un vieux soldat. Peu de chair. Peu de bonne volonté. On devrait l'abattre. Avec les autres.

— Ça ne changerait rien. On a mis les morts dans le vent. Comme ça. Un jour. Par ennui. Et rien. Ça n'a rien donné. J'ai tiré sur le vent un jour aussi. Comme ça. Par ennui. Et puis rien. Non plus. 

— L'indifférence. Parfois. Peut-être que c'est comme la sagesse. Une sorte de seringue vide au milieu d'un terrain vague. Un terrain vague au milieu d'un désert. Surtout si le vent laisse les ordures nous engloutir. On n'aura plus à s'en débarrasser. Ce sera plus simple. 

— Tirer sur le vent. C'est plus simple. Cela reste une manière comme une autre de poursuivre le vent. Il ne répond pas. On lui tire dessus. Et c'est peut-être mieux ainsi. Il ne répond pas. Mais tout n'est pas mort en lui. Tout n'est pas mort. Mais tout meurt. Et c'est peut-être mieux ainsi aussi. Du vent. Des ruines. Des balles de fusil qui s'y perdent. Et le bruit des balles qui dure plus longtemps que le jour. Insensément.

