---
persona:
  - Walter Benjamin
title: "Fragment théologico-politique"
slug: "fragment-theologico-politique"
date: 2021-02-04
echo:
  - ontique
  - stratégique
images:
  - "img/210204-benjamin-fragment-theologico-politique.jpg"
gridimages:
  - "img/210204-benjamin-fragment-theologico-politique-grid.jpg"
sourceimage: "https://commons.wikimedia.org/wiki/File:Paul_Klee_~_Armer_Engel_~_1939.jpg"
notes: "Le *Fragment théologico-politique* de Walter Benjamin date vraisemblablement du début des années 1920, selon Gershom Scholem. Cette nouvelle traduction d'Ann Persson a pour objectifs de se placer dans le domaine public volontaire et de faire sentir la saine obscurité entourant la pensée de Walter Benjamin. Le texte allemand peut être retrouvé dans *Walter Benjamin, Gesammelte Schriften II, Suhrkamp, Frankfurt am Main, 1991, p. 203.*"
summary: "Seul le messie lui-même accomplit tous les événements historiques, et ce au sens que lui seul délivre, accomplit, crée le rapport de ces événements au messianique même. C'est pourquoi aucun événement historique ne peut par lui-même vouloir se référer au messianique. C'est pourquoi le royaume de Dieu n'est pas le telos de la dunamis historique ; il ne peut pas être posé comme but. Historiquement, il n'est pas vu comme un but, mais comme une fin."
citation: "Car messianique est la nature par son éternelle et totale évanescence."
---

Seul le messie lui-même accomplit tous les événements historiques, et ce au sens que lui seul délivre, accomplit, crée le rapport de ces événements au messianique même. C'est pourquoi aucun événement historique ne peut par lui-même vouloir se référer au messianique. C'est pourquoi le royaume de Dieu n'est pas le *telos* de la *dunamis* historique ; il ne peut pas être posé comme but. Historiquement, il n'est pas vu comme un but, mais comme une fin. C'est pourquoi l'ordre du profane ne peut pas être construit sur l'idée du royaume de Dieu, c'est pourquoi la théocratie n'a pas de sens politique, mais seulement un sens religieux. Le fait d'avoir nié avec intensité la signification politique de la théocratie est le plus grand mérite de *L'Esprit de l'utopie* de Bloch.

L'ordre du profane doit s'ériger sur l'idée du bonheur. Le rapport de cet ordre avec le messianique est l'une des pièces didactiques essentielles de la philosophie de l'histoire. À savoir qu'il est conditionné par elle à partir d'une conception mystique de l'histoire, dont le problème se laisse exposer en une image. Lorsqu'une flèche désigne le but vers lequel la *dunamis* du profane agit, une autre le sens de l'intensité messianique, la recherche du bonheur de l'humanité libre se détourne alors certes de ce sens messianique, mais comme une force peut, à travers sa voie, promouvoir une autre force en une voie opposée, l'ordre profane du profane peut aussi promouvoir la venue du royaume messianique. Le profane n'est donc certes pas une catégorie du royaume, mais une catégorie, à savoir l'une des plus pertinentes, de son approche la plus feutrée. Car dans le bonheur tout le terrestre aspire à son déclin, mais le déclin ne lui est destiné que dans le bonheur. --- Alors que bien sûr l'intensité messianique immédiate du cœur, du seul humain en son for intérieur, passe par le malheur, au sens de la souffrance. À la spirituelle *restitutio in integrum*, laquelle introduit à l'immortalité, correspond une séculière *restitutio in integrum*, qui conduit à l'éternité du déclin, et le rythme de cette sécularité éternellement évanescente, évanescente en sa totalité, évanescente en sa totalité spatiale, mais aussi temporelle, le rythme de la nature messianique, est le bonheur. Car messianique est la nature par son éternelle et totale évanescence.

Aspirer à cette évanescence, même pour ces degrés de l'humain qui sont nature, est la tâche de la politique mondiale, dont la méthode doit s'appeler nihilisme.
