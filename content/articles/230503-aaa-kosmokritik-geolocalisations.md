---
persona:
  - AAA
  - Kosmokritik
title: "Géolocalisations : un milliard de milliards de paradoxes pour une ontologie minérale"
slug: "geolocalisations"
date: 2023-05-17
echo:
  - ontique
  - graphique
images:
  - "img/230503-aaa-kosmokritik-geolocalisations.jpg"
gridimages:
  - "img/230503-aaa-kosmokritik-geolocalisations-grid.jpg"
sourceimage: "AAA"
summary: "Le virtuel devient une contre-virtualité de nos mondes, lorsqu’il laisse entrevoir une passerelle au travers des opacités de nos réalités de la marchandise. L’œuvre Géolocalisations, contemplation tridimensionnelle de l’insignifiant rocheux de nos quotidiens, propose, à chaque interaction avec celle-ci, un paradoxe parmi un milliard de milliards de paradoxes, soit 10 à la puissance 18 propositions contredisant l’évidence de nos conceptions du réel, pour que puisse émerger une ontologie minérale invitant à se saisir avec conscience des puissances de ce qui existe autrement, en deçà de nos agitations. La pollution de l’invisible déploie une visibilité des espaces ignorés. L’infinité des possibles ouvre une métaphysique du mouvement, dont le sens ne cesse de se renouveler pour maintenir dans une distance critique toutes les métaphysiques qui imposent à l’être une manière d’être."
notes: "L'œuvre <a href=\"https://www.antilivre.org/geolocalisations/\" target=\"_blank\" rel=\"noopener\">Géolocalisations</a> est aussi un *antilivre* qui invite à la contemplation tridimensionnelle de l'insignifiant rocheux de nos quotidiens. Le virtuel devient une contre-virtualité de nos mondes, lorsqu'il laisse entrevoir une passerelle au travers des opacités de nos réalités de la marchandise. La pollution de l'invisible déploie une visibilité des espaces ignorés. L'infinité des possibles ouvre une métaphysique du mouvement, dont le sens ne cesse de se renouveler pour maintenir dans une distance critique toutes les métaphysiques qui imposent à l'être une manière d'être."
antilivre: "https://www.antilivre.org/geolocalisations/"
citation: "de l'ouverture au néant"
poetry: false
hangindent: false
javascript:
  - "230503-aaa-kosmokritik-geolocalisations.js"
---


*Voici un paradoxe parmi un milliard de milliards de paradoxes, soit 10 à la puissance 18 propositions contredisant l'évidence de nos conceptions du réel, pour que puisse émerger une ontologie minérale invitant à se saisir avec conscience des puissances de ce qui existe autrement, en deçà de nos agitations.*


<p class="nomargin">
<button class="button button--sonnet button--action"><span>un paradoxe</span></button>
</p>

<div class="melange">

<div class="text text--show" style="border: var(--border-width) solid var(--color-fg); padding: var(--line-space);"></div>

<div class="text text--content text--t1">

<span class="sentence sentence--s01">L'être</span>
<span class="sentence sentence--s02">d'une roche</span>
<span class="sentence sentence--s03">représente</span>
<span class="sentence sentence--s04">l'état liminal</span>
<span class="sentence sentence--s05">d'une réalité close,</span>
<span class="sentence sentence--s06">indépendamment de l'inexpérience</span>
<span class="sentence sentence--s07">humaine</span>
<span class="sentence sentence--s08">d'excéder</span>
<span class="sentence sentence--s09">l'intuition de l'être</span>
<span class="sentence sentence--s10">par le dépassement</span>
<span class="sentence sentence--s11">du concept de permanence,</span>
<span class="sentence sentence--s12">or sa conscience</span>
<span class="sentence sentence--s13">porte la mue du soi</span>
<span class="sentence sentence--s14">loin de l'éclaircie de</span>
<span class="sentence sentence--s15">sa fixité,</span>
<span class="sentence sentence--s16">pour que survienne une stratégie</span>
<span class="sentence sentence--s17">inhumaine</span>
<span class="sentence sentence--s18">de l'érosion de l'être.</span>

</div>

<div class="text text--content text--t2">

<span class="sentence sentence--s01">La forme</span>
<span class="sentence sentence--s02">des sédiments</span>
<span class="sentence sentence--s03">révèle le signe qui traverse</span>
<span class="sentence sentence--s04">les déformations</span>
<span class="sentence sentence--s05">de l'unicité humaine,</span>
<span class="sentence sentence--s06">dans l'ombre de l'inaptitude</span>
<span class="sentence sentence--s07">moderne</span>
<span class="sentence sentence--s08">de s'affranchir de</span>
<span class="sentence sentence--s09">la granularité de l'être</span>
<span class="sentence sentence--s10">par une herméneutique</span>
<span class="sentence sentence--s11">des modes d'existence,</span>
<span class="sentence sentence--s12">alors une révolution épistémique transparaît et</span>
<span class="sentence sentence--s13">façonne une trouée parmi les évidences langagières</span>
<span class="sentence sentence--s14">aux antipodes de</span>
<span class="sentence sentence--s15">l'institution de toute unité,</span>
<span class="sentence sentence--s16">en cette tentation d'une éthique</span>
<span class="sentence sentence--s17">remuante</span>
<span class="sentence sentence--s18">de sédimentation des anomalies de l'être.</span>

</div>

<div class="text text--content text--t3">

<span class="sentence sentence--s01">L'existence</span>
<span class="sentence sentence--s02">tellurique</span>
<span class="sentence sentence--s03">érode les mondes par</span>
<span class="sentence sentence--s04">les paradoxes</span>
<span class="sentence sentence--s05">des stases du social,</span>
<span class="sentence sentence--s06">à l'insu de l'inhibition</span>
<span class="sentence sentence--s07">conservatrice</span>
<span class="sentence sentence--s08">de défaire</span>
<span class="sentence sentence--s09">la chosification des mondes</span>
<span class="sentence sentence--s10">grâce à l'analyse critique</span>
<span class="sentence sentence--s11">d'une économie centrée sur les seuls objets,</span>
<span class="sentence sentence--s12">et la généralisation du principe de sa subjectivité</span>
<span class="sentence sentence--s13">inspire à l'être son étrangeté</span>
<span class="sentence sentence--s14">à rebours des lumières de</span>
<span class="sentence sentence--s15">sa plénitude,</span>
<span class="sentence sentence--s16">en entretenant l'espérance d'une poétique</span>
<span class="sentence sentence--s17">excentrique</span>
<span class="sentence sentence--s18">de l'enclave.</span>

</div>

<div class="text text--content text--t4">

<span class="sentence sentence--s01">Le devenir</span>
<span class="sentence sentence--s02">des laves</span>
<span class="sentence sentence--s03">recompose sans cesse</span>
<span class="sentence sentence--s04">une fragmentation</span>
<span class="sentence sentence--s05">de l'ontologisation de l'autorité,</span>
<span class="sentence sentence--s06">en dépit de l'incapacité</span>
<span class="sentence sentence--s07">du capitalisme</span>
<span class="sentence sentence--s08">de détruire</span>
<span class="sentence sentence--s09">l'absolue contingence du réel</span>
<span class="sentence sentence--s10">en sondant l'orthodoxie</span>
<span class="sentence sentence--s11">des mécaniques de sa valorisation,</span>
<span class="sentence sentence--s12">parce que son miroitement nucléaire</span>
<span class="sentence sentence--s13">engendre l'accident en tant que nécessité</span>
<span class="sentence sentence--s14">au mépris des apparences de</span>
<span class="sentence sentence--s15">ses états moléculaires,</span>
<span class="sentence sentence--s16">afin d'attiser une dynamique</span>
<span class="sentence sentence--s17">non vivante</span>
<span class="sentence sentence--s18">d'affleurement.</span>

</div>

<div class="text text--content text--t5">

<span class="sentence sentence--s01">L'identité</span>
<span class="sentence sentence--s02">du fer</span>
<span class="sentence sentence--s03">signale son extravagance dans</span>
<span class="sentence sentence--s04">les marges</span>
<span class="sentence sentence--s05">d'un rapport productiviste au réel,</span>
<span class="sentence sentence--s06">tout autre que l'improbabilité</span>
<span class="sentence sentence--s07">de l'esprit citoyen</span>
<span class="sentence sentence--s08">de contrevenir à</span>
<span class="sentence sentence--s09">sa mondéité propre</span>
<span class="sentence sentence--s10">à travers une critique</span>
<span class="sentence sentence--s11">de la normalisation de son environnement,</span>
<span class="sentence sentence--s12">et la subsomption élémentaire de ses parties</span>
<span class="sentence sentence--s13">modèle une ouverture</span>
<span class="sentence sentence--s14">contre la certitude de</span>
<span class="sentence sentence--s15">son atomicité,</span>
<span class="sentence sentence--s16">pour qu'émerge une pratique</span>
<span class="sentence sentence--s17">signifiante</span>
<span class="sentence sentence--s18">de l'ouverture au néant.</span>

</div>

<div class="text text--content text--t6">

<span class="sentence sentence--s01">Le sujet</span>
<span class="sentence sentence--s02">magmatique</span>
<span class="sentence sentence--s03">apparaît à l'entendement par</span>
<span class="sentence sentence--s04">les fissures</span>
<span class="sentence sentence--s05">d'un matérialisme des semblances,</span>
<span class="sentence sentence--s06">au-delà de l'inexpérience</span>
<span class="sentence sentence--s07">séculière</span>
<span class="sentence sentence--s08">de surmonter</span>
<span class="sentence sentence--s09">l'immuabilité du matérialisme</span>
<span class="sentence sentence--s10">par la transgression</span>
<span class="sentence sentence--s11">de l'axiome de causalité,</span>
<span class="sentence sentence--s12">car la communion chimique qui peut y avoir lieu</span>
<span class="sentence sentence--s13">forge une diffraction jusque dans l'être</span>
<span class="sentence sentence--s14">à la frontière de</span>
<span class="sentence sentence--s15">sa cristallisation,</span>
<span class="sentence sentence--s16">de manière à induire une agentivité</span>
<span class="sentence sentence--s17">cosmique</span>
<span class="sentence sentence--s18">de décantation du dire-être.</span>

</div>

<div class="text text--content text--t7">

<span class="sentence sentence--s01">La virtualité</span>
<span class="sentence sentence--s02">de la silice</span>
<span class="sentence sentence--s03">évide toute injonction à l'immobilité pour rejoindre</span>
<span class="sentence sentence--s04">la dissolution</span>
<span class="sentence sentence--s05">d'une idéologie des invariables,</span>
<span class="sentence sentence--s06">malgré l'impuissance</span>
<span class="sentence sentence--s07">du sujet négociant</span>
<span class="sentence sentence--s08">d'infirmer</span>
<span class="sentence sentence--s09">l'infinitude de la logique</span>
<span class="sentence sentence--s10">en contredisant les acquis</span>
<span class="sentence sentence--s11">d'une logique de vérité,</span>
<span class="sentence sentence--s12">et une approche cybernétique de ses éléments</span>
<span class="sentence sentence--s13">gouverne à une déviation des réalités qui en émanent</span>
<span class="sentence sentence--s14">contre la pensée de</span>
<span class="sentence sentence--s15">toute persistance des traditions,</span>
<span class="sentence sentence--s16">en faveur d'une méthodologie</span>
<span class="sentence sentence--s17">poreuse</span>
<span class="sentence sentence--s18">des propagations du désordre.</span>

</div>

<div class="text text--content text--t8">

<span class="sentence sentence--s01">La temporalité</span>
<span class="sentence sentence--s02">du charbon</span>
<span class="sentence sentence--s03">infère</span>
<span class="sentence sentence--s04">l'affaissement</span>
<span class="sentence sentence--s05">d'une grammaire de la facticité,</span>
<span class="sentence sentence--s06">outre l'incompétence</span>
<span class="sentence sentence--s07">des civilisations marchandes</span>
<span class="sentence sentence--s08">de nuancer</span>
<span class="sentence sentence--s09">la langue des universaux</span>
<span class="sentence sentence--s10">au moyen d'une exégèse</span>
<span class="sentence sentence--s11">de sa naturalité même,</span>
<span class="sentence sentence--s12">et la relativisation de son intégrité</span>
<span class="sentence sentence--s13">réorganise le commun</span>
<span class="sentence sentence--s14">à contre-courant des flagrances de</span>
<span class="sentence sentence--s15">la stratification des croyances,</span>
<span class="sentence sentence--s16">pour présager une politique</span>
<span class="sentence sentence--s17">fissile</span>
<span class="sentence sentence--s18">de subsidence de toutes les valeurs.</span>

</div>

<div class="text text--content text--t9">

<span class="sentence sentence--s01">La situation</span>
<span class="sentence sentence--s02">météoritique</span>
<span class="sentence sentence--s03">refuse la surface des signes pour dégager</span>
<span class="sentence sentence--s04">les profondeurs</span>
<span class="sentence sentence--s05">d'une vision orthogonale du réel,</span>
<span class="sentence sentence--s06">à l'inverse de l'inhabileté</span>
<span class="sentence sentence--s07">des républiques libérales</span>
<span class="sentence sentence--s08">de répondre à</span>
<span class="sentence sentence--s09">l'idée de transcendance</span>
<span class="sentence sentence--s10">par l'exploration</span>
<span class="sentence sentence--s11">de ses hiérarchies structurelles,</span>
<span class="sentence sentence--s12">et présuppose une transversalité de la physique qui</span>
<span class="sentence sentence--s13">arrange une voie vers l'éloigné</span>
<span class="sentence sentence--s14">face à l'actualisation de</span>
<span class="sentence sentence--s15">l'invisible de sa morphologie,</span>
<span class="sentence sentence--s16">laissant imaginer une théorie</span>
<span class="sentence sentence--s17">gravitationnelle</span>
<span class="sentence sentence--s18">d'accrétion de l'insignifiant.</span>

</div>

<div class="text text--content text--t10">

<span class="sentence sentence--s01">L'événement</span>
<span class="sentence sentence--s02">calcique</span>
<span class="sentence sentence--s03">engage un soi conscient vers</span>
<span class="sentence sentence--s04">un métamorphisme</span>
<span class="sentence sentence--s05">des constances du visible,</span>
<span class="sentence sentence--s06">en dehors de l'insuffisance</span>
<span class="sentence sentence--s07">des processus d'individuation</span>
<span class="sentence sentence--s08">d'examiner</span>
<span class="sentence sentence--s09">l'objectivation de la perception</span>
<span class="sentence sentence--s10">à la faveur d'une négation</span>
<span class="sentence sentence--s11">de l'univocité de la durée,</span>
<span class="sentence sentence--s12">car la fluctuation de ses apparences</span>
<span class="sentence sentence--s13">nécessite la réévaluation des catégories de l'intuition</span>
<span class="sentence sentence--s14">autant que l'altération de</span>
<span class="sentence sentence--s15">sa cohérence formelle,</span>
<span class="sentence sentence--s16">de sorte que puisse se planifier une dialectique</span>
<span class="sentence sentence--s17">plastique</span>
<span class="sentence sentence--s18">des vacuoles en ce qui est.</span>

</div>

</div>

