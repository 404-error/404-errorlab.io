---
persona:
  - Heinrich Heine
title: "Pauvres tisserands"
slug: "pauvres-tisserands"
date: 2025-02-17
echo:
  - rythmique
images:
  - "img/250201-heine-pauvres-tisserands.jpg"
gridimages:
  - "img/250201-heine-pauvres-tisserands-grid.jpg"
sourceimage: "cyberpoétique"
notes: "Le poème *Die schlesischen Weber* (Les tisserands silésiens) de Heinrich Heine a paru pour la première fois le 10 juillet 1844 sous le titre *Die armen Weber* (Les pauvres tisserands) dans le journal *Vorwärts!*, édité notamment par Karl Marx. Le poème, qui figure sur cette page, a été revu par Heine en 1846. Il s'inscrit dans le cadre du *Vormärz*, mouvement littéraire politique, et tente de saisir la misère des tisserands en Silésie, qui se révoltèrent, durant l'année 1844, contre la violence imposée par l'industrialisation nouvelle. Nous proposons ici une traduction qui prend le parti de respecter le rythme et la rime du poème allemand, en choisissant des décasyllabes ainsi que des rimes suivies. Cette traduction se place dans le domaine public volontaire, sous la licence Creative Commons Zero."
summary: "Dans l'œil sombre ne tombe aucun sanglot, / Face à leur métier, ils montrent les crocs : / Allemagne, nous tissons ton sindon, / Y tissons la triple malédiction --- / Nous tissons, nous tissons ! / Malédiction sur le dieu que louèrent / Nos prières dans la faim et l'hiver ; / En vain, nous avons attendu et cru, / Il nous a moqués, dupés et perdus --- / Nous tissons, nous tissons !"
citation: "Dans l'œil sombre ne tombe aucun sanglot"
poetry: true
hangindent: true
affiche: true
---

# Les tisserands silésiens

Dans l'œil sombre ne tombe aucun sanglot,

Face à leur métier, ils montrent les crocs :

Allemagne, nous tissons ton sindon,

Y tissons la triple malédiction&nbsp;---

Nous tissons, nous tissons&nbsp;!

 

Malédiction sur le dieu que louèrent

Nos prières dans la faim et l'hiver&nbsp;;

En vain, nous avons attendu et cru,

Il nous a moqués, dupés et perdus&nbsp;---

Nous tissons, nous tissons&nbsp;!

 

Malédiction sur le roi, roi des riches,

Dur qui avec notre misère triche,

Qui nous ravit jusqu'à nos derniers biens,

Et nous fait abattre comme des chiens&nbsp;---

Nous tissons, nous tissons&nbsp;!

 

Malédiction sur la fausse patrie,

Où seules croissent honte et infamie,

Où chaque fleur si vite touche terre,

Où l'ordure et l'infect gorgent le ver&nbsp;---

Nous tissons, nous tissons&nbsp;!

 

Le métier craque, la navette vole,

Et jour et nuit, nous tissons sans paroles&nbsp;---

Vieille Allemagne, nous tissons ton sindon,

Y tissons la triple malédiction&nbsp;---

Nous tissons, nous tissons&nbsp;!

# Die schlesischen Weber

Im düstern Auge keine Thräne,

Sie sitzen am Webstuhl und fletschen die Zähne:

Deutschland, wir weben Dein Leichentuch,

Wir weben hinein den dreifachen Fluch&nbsp;---

Wir weben, wir weben!

 

Ein Fluch dem Gotte, zu dem wir gebeten

In Winterskälte und Hungersnöthen;

Wir haben vergebens gehofft und geharrt,

Er hat uns geäfft und gefoppt und genarrt&nbsp;---

Wir weben, wir weben!

 

Ein Fluch dem König, dem König der Reichen,

Den unser Elend nicht konnte erweichen,

Der den letzten Groschen von uns erpreßt,

Und uns wie Hunde erschießen läßt&nbsp;---

Wir weben, wir weben!

 

Ein Fluch dem falschen Vaterlande,

Wo nur gedeihen Schmach und Schande,

Wo jede Blume früh geknickt,

Wo Fäulniß und Moder den Wurm erquickt&nbsp;---

Wir weben, wir weben!

 

Das Schiffchen fliegt, der Webstuhl kracht,

Wir weben emsig Tag und Nacht&nbsp;---

Altdeutschland, wir weben Dein Leichentuch,

Wir weben hinein den dreifachen Fluch,

Wir weben, wir weben!


