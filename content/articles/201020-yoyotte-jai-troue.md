---
persona:
  - Simone Yoyotte
title: "J'ai troué le drapeau de la République"
slug: "jai-troue-le-drapeau-de-la-republique"
date: 2020-10-20
echo:
  - rythmique
images:
  - "img/201020-yoyotte-jai-troue.jpg"
gridimages:
  - "img/201020-yoyotte-jai-troue-grid.jpg"
sourceimage: "https://commons.wikimedia.org/wiki/File:Basse_Pointe_YORYM-TA0306.jpg"
notes: "Ces textes ont paru en 1932 dans le numéro unique de *Légitime Défense*, revue regroupant de jeunes écrivains marxistes martiniquais, qui marqua les prémices du mouvement de la négritude."
summary: "Mon bel oiseau dans l'éternel les gouttières t'appellent mais ne songe pas à revenir. Les plumes d'un surnom agréable ne manqueront pas de faire passer la peur la peur du vent dans les glaciers."
citation: "mon oiseau inutile qui ne sait que peupler la révolte"
---

# Pyjama-vitesse

Mon pyjama doré d'azur et Bois-Colombes\
Les atmosphères tranquilles --- et danse\
La pavane du silence et juif. --- Je m'émeus\
--- soit --- mais non et si je partais douce\
et le pays rivière de moi même légère\
et je souris. --- Mon pyjama doré et brodé\
de moi (lance) et pis que tout doré d'azur\
mon pyjama benjoin marteau doré d'azur\
dit Bois-Colombes et juif et vous y êtes.

# Ligne bleu clair dans un épisode de commande, j'ai troué le drapeau de la République

Mon bel oiseau dans l'éternel les gouttières t'appellent mais ne songe pas à revenir. Les plumes d'un surnom agréable ne manqueront pas de faire passer la peur la peur du vent dans les glaciers. Mon bel oiseau le tonnerre de tous mes désirs la satisfaction du soleil couché et de toutes mes épines entremêlées dans l'angoisse indistincte d'un séjour que je n'ai pas voulu t'imposer à toi mon oiseau feu mon oiseau sang mon désespoir en manches courtes au satin dégradé couleur de ma témérité tes plumes tes ailes en plumes sur la patte de derrière mon oiseau contre-énigme dissipons la clarté de tes lignes bleu clair mon blanc goujon vorace tu es mon bel oiseau mon bel oiseau zéphyr dans la nuit et que toutes les lampes s'éteignent au cuir de ma souple agonie. J'ai volé dans les digues et dans les peupliers j'ai vendu des soucis au rentier débonnaire j'ai parcouru les temples de désolation de nuit de jour à la tombée de toutes les grandes douleurs et partout bel oiseau je t'ai vu dans les pierres et tu ne peux savoir que l'esprit ne traverse pas la rivière car sur le pont que tu m'as jeté c'est en vain que j'ai lapidé toutes les rides. L'appel des rhomboèdres à la lisière d'avril ressemble à la musique de ton ombre à toi mon oiseau inutile qui ne sait que peupler la révolte de tous les grands arbres des avenues et de tous les boulevards lorsque la trompette des salles de banquet résonne sous les fenêtres de celle que tu n'aimes pas encore.
