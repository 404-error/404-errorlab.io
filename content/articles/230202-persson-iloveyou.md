---
persona:
  - Ann Persson
title: "ILOVEYOU"
slug: "iloveyou"
date: 2023-02-08
echo:
  - rythmique
images:
  - "img/230202-persson-iloveyou.jpg"
gridimages:
  - "img/230202-persson-iloveyou-grid.jpg"
sourceimage: "https://commons.wikimedia.org/wiki/File:Man_Alone_WTO1999_by_J.Narrin.jpg"
notes: "<a href=\"https://commons.wikimedia.org/wiki/File:Man_Alone_WTO1999_by_J.Narrin.jpg\" target=\"_blank\" rel=\"noopener\">L'image originale</a> de couverture, photographie de la *bataille de Seattle* de 1999, est de J. Narrin. L'image dérivée est de AAA. L'image du monde est une dérive du monde lui-même. Brûlons l'image, puis le monde."
summary: "il n'y a pas suffisamment d'attentats-suicides dans ma tête / encore une note de bas de page à la révolution / l'ego en charpie d'ego / ici on deale l'ecstasy à teresa / laisse tomber la vie insipide / elle s'en retourne contre toi le soi & le grand soir / d'âme à l'âme rejoins l'austère des palmiers / ici-bas il y a des nonnes avec le holster au clair / des terroristes avec des ceintures d'astéroïdes autour de la taille / le détonateur a la couleur laiteuse des satellites / qui cheminent à l'ombre des camgirls / en black night l'hiver qui dure tout le jour / & sur le terrain vague des industries / le game des mères chômeuses / & l'intranquillité du virtuel"
citation: "une note de bas de page à la révolution"
poetry: true
hangindent: false
---

il n'y a pas suffisamment d'attentats-suicides dans ma tête

encore une note de bas de page à la révolution

l'ego en charpie d'ego

ici on deale l'ecstasy à teresa

laisse tomber la vie insipide

elle s'en retourne contre toi le soi & le grand soir

d'âme à l'âme rejoins l'austère des palmiers

ici-bas il y a des nonnes avec le holster au clair

des terroristes avec des ceintures d'astéroïdes autour de la taille

le détonateur a la couleur laiteuse des satellites

qui cheminent à l'ombre des camgirls

en black night l'hiver qui dure tout le jour

& sur le terrain vague des industries

le game des mères chômeuses

& l'intranquillité des mondes virtuels

des bas résille pour tapis de prière

bouton pression détonation

livestream des nécrophores

république des sex tapes

ciel sans shtars

néons crypto & acid rain

la snow a un goût de peroxyde d'acétone

dernière taffe avant

l'aurore des six o'clock

la croix de fer pour crucifix

la kro la crown la corona des têtes coupées

the big lie qui se suspend à une matraque

biceps de la pompe

je me fais un garrot avec un ver informatique

worm solitaire du world wide oued

encore un dernier righ

choufe je te crash le love

la love-letter-for-you.txt.vbs

je flotte parmi la foule

& on se fool parmi les casqués

una furtiva lacrymo

nos ipséités sous bombers

toi qui as vomi le virus

matrix des compilations sales

tu déposes des semi-conducteurs sur ma langue

scarla de mes voies lactées

je mâche lentement 

jusqu'aux tricks des neuromanciennes

silicon euphoria

sujet en découpe d'identités hostiles

hail la mort-aux-rats dans l'hostie

on vogue dans le miami des bas-fonds

avec nos synapses qui filent la comète

trois fois sous la trinité des schmitts

je récite à l'envers le livre d'heures & d'erreurs

& je guette le désert la bac & les quasars

parce qu'ils ont percé la prothèse de nos imaginaires

vanité des vanités & tout est vanité

je jette du bitcoin dans l'urne

pour les prols & les crawlers

les élections à la machine à sous

la teille la téci & la ténèbre

conseil de défonce & de sécurité nationale

j'oublie le jour de ma naissance

spleen & spliff

sniff & triste

débris la bri

la brisure & le spectre d'europe qui titube

de la soude pour se récurer les songes

des log files pour murmurer des complots

scories & pénitence

pénitencier sans bénitier

ça flotte l'ordure ophelia

je vois dark sous les pavés la nécrose

ne vois plus que toi

des cocktails m. & des camélias

aime-moi.exe

ton sexe l'apex

du dream 6 mm remington

& rémission des trous noirs

guzman qui coupe le code

la main à la fatima

l'over la dose sous k.

je te fais une pipe à crack

injection de nihilisme kawaï

des k-ways & des barricades

pour toute épiphanie l'épitaphe

smog de rouille

starlink linceul d'étoiles

l'esseulée la ramasse la grenade

un lance-pierre pour faire face au lanceur

émascule le phallus des fusées

je travaille pas le taf

je cotise sans turfu

cautérise mon emprise

j'ai pris ma retraite avec le iencli

je vends de la blanche & ne rêve qu'à la matière noire

catharsis des poubelles retournées

une voiture qui brûle paisiblement

& l'empire qui nasse nos infinis

confusion des predators & des météores

pink paradise de nos vertus

là tout n'est que désordre & nuitée

luxure flammes & volupté

béton d'espoir lbd forty fortifiant

je t'en refile loin dans ta province

la c d'impureté

les rs qui likent qui lol leur haine

bakchich aux teamsters

les parlementaires posent des duckfaces devant les usines

des chevalements pour échafauds

mépris des villes & silence under terrils

je prie pour la fonte du permafrost

& fire fire je m'allume la dernière ficelle avant l'abîme

je bats pour l'apo

& ne pr0n que la ruine

avec ma meth en psycho

charon des maisons de passe

qui trafique les enquêtes d'opinion

échantillons d'extême

j'ai un goût de plomb dans le sang

mon masque face à la face id

le terroir s'invente avant la terre

la créatine avant la création

je vote pour la montée des eaux dans tout paname

les sharks des centrales en roue libre

holy cross du beretta

sur la noyée de la pool de bethesda

barbie du sacristain

avec ses sirènes qui font le trottoir

& des drones qui tiquent

en toc s'écrasent sur le green

hole in one

on fait la warzone avec des calibres de douze

les glocks du nine

je me fais un shot de pepper spray

headshot

de la 3d pour visages arrachés

à la 2d des screens en solde

(cette vidéo est sponsorisée par nra vpn)

& des amazones baisent bezos

en black friday des nécropoles

big bro rip rpg god mode

je te refourgue l'od & le c-4

le calumet a un goût de pisse

je te prépare des coléoptères au beurre

nous de la monstration des munchies

les rabouins & ma gueule de babouin

nous qui dévorons quelques ministres désossés

pour faire théorème d'incomplétude

on se bicrave la lune de fiel

ab ovo selfie stick & ak47

la mondaine qui me move la déroute

quarante-huit heures de gav

& gavage des lois

j'ai la foi plus grasse qu'un sénateur

transhume je fume mes cendres en vr

métastase & métavers

je joue avec les serpents

sous la mite & les mythes d'éther

d'éternité pour désastre

le graal made in china

garantie deux ans

ô sweet sweatshops

de nasdaq la night-vision d'irak

& hack les rh avant l'érèbe

je me penche au-dessus du vide

(plaquage ventral je goûte le néant)

& le vide qui me nacht la nuit

black screen black skin

black bloc sur fond blanc

se faire exploser avant midnight

factory reset des neurones

dieu est une fake news

xoxo les kisdés m'ont prise

j'te kiss

à l'amende

à jamais

p.s. le port du masque est obligatoire
