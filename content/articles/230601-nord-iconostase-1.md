---
persona:
  - Hilda Nord
title: "Iconostase 1"
slug: "iconostase-1"
date: 2023-06-29
echo:
  - esthétique
  - ontique
images:
  - "img/230601-nord-iconostase-1.jpg"
gridimages:
  - "img/230601-nord-iconostase-1-grid.jpg"
sourceimage: "https://commons.wikimedia.org/wiki/File:Uluru_from_above_Iss049e010638_lrg.jpg"
notes: "L'iconostase est une infinité d'icônes à briser. L'iconostase est donc la promesse d'une continuité qui recherche sa discontinuité."
summary: "Il y a deux manières d'approcher la laideur du monde, mais une seule de ces deux manières participe à la stratégie nécessaire pour la conjurer. La première consisterait dans une lâcheté de l'imagination, celle de l'esprit des petitesses habité par son défaitisme du présent, emporté par cette attitude si caractéristique des forces qui réagissent à ce qui est en se lamentant de ce qui fut, esprit tout bouffi d'une vengeance sourde, qui, s'il détenait un jour un quelconque pouvoir, jetterait de la laideur sur la laideur et ne ferait que souiller la substance éclatante des fantômes."
citation: "Le non-être rêve de l’être et nous rêvons avec lui."
---

*Considérez ces quelques paragraphes comme des images jaunissantes, dont le mélange, à même le sol d'une chambre vide, cette chambre que chacun porte en soi, compose une multiplicité de contradictions, dont la substance critique ne souhaite qu'éprouver le déterminisme commandant à la parallaxe.*

<p class="nomargin">
<button class="button button--melange">iconoclasme</button>
</p>

<div class="melange-zone headers--onespace">

## La laideur du monde

Il y a deux manières d'approcher la laideur du monde, mais une seule de ces deux manières participe à la stratégie nécessaire pour la conjurer. La première consisterait dans une lâcheté de l'imagination, celle de l'esprit des petitesses habité par son défaitisme du présent, emporté par cette attitude si caractéristique des forces qui réagissent à ce qui est en se lamentant de ce qui fut, esprit tout bouffi d'une vengeance sourde, qui, s'il détenait un jour un quelconque pouvoir, jetterait de la laideur sur la laideur et ne ferait que souiller la substance éclatante des fantômes. L'espoir réactionnaire ne se situe pas dans le recouvrement d'une valeur du passé, mais dans une vengeance de ne pas savoir insuffler au présent sa valeur propre. Le ressentiment fait de l'insuccès de l'ego, variation de l'échec d'un sujet à satisfaire son seul désir normatif dans les normes du présent, une objectivation généralisant ses réprobations vengeresses au cadre de son insuccès. La seconde, la seule dont la densité stratégique nous semble d'un quelconque intérêt, est celle de dire que le monde est laid parce qu'il a trahi la potentialité de beauté que les forces du passé promettaient à son instant. Le monde est laid, car il est une défaite de ce qui commerce sa potentialité sans jamais tenter de l'actualiser. Dire la laideur de notre monde, ce n'est pas regretter un passé, quel qu'il soit, mais regretter tout l'espoir que ce passé plaçait dans la forme de notre présent. Dire cette laideur demeure le seul moyen d'aviver à nouveau un feu des novations du temps, dont la lumière chasserait la désespérance veule de nos manières toutes cristallisées dans l'échange de leur image. Nous disons ici la laideur du monde, pour tenter non de recouvrer une quelconque beauté, mais pour jeter sur l'incertitude de notre futur et de ses contingences l'éclat de ses formes impossibles. Ce n'est pas le futur déterminé de la modernité que nous voulons, voire sa déviance, mais c'est l'errance entre les mondes possibles. Il ne s'agit pas de dire que nous n'appartenons plus au monde qui nous emporte pourtant dans sa course, mais que nous nous situons en deçà de sa fixité, que nous sommes posthumes à la représentation que ce monde se fait de nous-mêmes, dans l'ici et le maintenant. Il faut dire la laideur du monde pour encore espérer le renverser.

</div>

<div class="melange-zone headers--onespace">

## D'Akerman et d'atemporalité

À la 52^e^ minute des *Rendez-vous d'Anna* de Chantal Akerman, la nuit tout entière tient dans la cendre d'une cigarette sur le point de chuter. Le train ne s'arrêtera pas sur le sommeil des humains, seuls le travail et l'inconscient de sa prétendue liberté vrombissent encore de leurs brisures.

</div>

<div class="melange-zone headers--onespace">

## Chambre sans soleil

Edward Hopper, *A Woman in the Sun*. Le corps d'une femme nue, seule, au pied d'un lit défait, se tenant debout, existant sans le monde, une cigarette à la main, face à l'ordonnancement moderne de l'extérieur du *soi*, face à un soleil inondant l'ordinaire. Un regard triste, un regard ciel, sans blancheur, refuse la nature qui s'impose : l'esprit de la femme retourné en lui-même fuyant l'objet de sa représentation, dédié à l'*otium* des astres déclinants, emporté par la révélation que la femme *n'est pas* l'esprit de la femme, et que, simplement, la femme *n'est pas*. La frontière de la fenêtre fabrique le vecteur d'un regard. La direction semble évidente : tout s'adresse à l'extérieur, alors que c'est l'extérieur qui envahit l'intériorité. L'apparat tente de s'adresser à la nudité par le contraste qui dénonce l'émiettement des fonctions. La ville regarde la femme, mais le regard de la femme se détourne de l'évidence. *Jeder Mensch trägt ein Zimmer in sich*. La femme demeure sans soleil, à la recherche d'un espace non *à* soi, mais *en* soi, là où le monde pourrait se défaire.

</div>

<div class="melange-zone headers--onespace">

## Soi Sébastien

Lorsque Yukio Mishima se découvre par l'image du *Saint Sébastien* de Guido Reni, image face à laquelle il se masturbe pour la première fois, image d'un corps pénétré de flèches dont le martyre dévoile une beauté éthérée, traversant la trinité du supplice portée par un regard tourné vers des cieux lui offrant son aura, Mishima ne libère pas seulement sa sexualité fascinée par sa propre souffrance mâle, il fabrique une image que son existence ne cessera plus de peupler. Le regard de Mishima provoque la mutation de l'image par l'incarnation qu'il en fait. L'homoérotisme devient un égoérotisme du miroitement, qui diffère d'un autoérotisme où le corps se contente de lui-même puisqu'il dépend du jeu de déformations que l'image provoque en se déplaçant du *soi* à la représentation métamorphique du *soi*. Yukio Mishima cristallise ce scintillement des déplacements de l'image dans le *soi* en incarnant, pour une photographie de 1968, saint Sébastien : le corps devient la dilution de son désir dans l'élévation du martyr à la lumière, le corps s'évapore dans la matérialisation souffrante de sa transcendance.

</div>

<div class="melange-zone headers--onespace">

## Dévisager

Le visage n'est pas la lumière qui en émane, mais l'ombre qui s'en échappe. Croire en la clarté du visage découvert revient à croire à l'illusion d'une image première, sans tenir compte de l'endroit à partir duquel l'image se crée, sans tenir compte de la manière dont l'image se transmet. Vouloir le visage clair et dévoilé, c'est vouloir le contrôle sur ce qui nous échappe. Le visage demeure toujours le masque du visage, dont la dissimulation s'accentue dans l'apparence de son dévoilement, car tout dévoilement impose le voilement d'une politique de l'être et la répression de sa déviance. L'*autre* n'advient pas par la morphologie du corps, mais par l'aura, présence présente ou présence disparue, immatérialité toujours qui nimbe l'espace et y creuse un refuge. L'idée de l'*autre* façonne la matière de l'*autre*, avant que nous ayons à lui imposer la fixité d'un quelconque visage, dont l'arrangement émotif suit à notre époque les nécessités contemporaines du commerce de l'image. Contre toute fixité de l'image, contre toute prédiction de l'image, contre toute opacité de l'image, l'image et l'empêchement de l'image s'émiettent par la glorification d'une transsubstantiation incertaine. L'*autre* advient alors, il offre un visage de mutation et donne en offrande la torsion imagée du *soi* de l'*autre*, chatoiement du *soi du soi*. En amont de l'apparition de son image contrôlée et finie, image politique de la citoyenneté, profilage de l'identité, l'*autre* naît dans le visage *infini* du fantôme, celui du non-citoyen, de la meurtrissure dans la différence, visage qui donne à la marge non un corps, mais une dynamique de la fuite. Chercher la bavure d'une image révèle davantage sur sa signifiance, que l'apparente découverte de sa norme.

</div>

<div class="melange-zone headers--onespace">

## Immolare

Au détour d'une ruelle en feu, l'humain cessa de croire. Il ne plaça plus sa foi en la révolution, car il ne distingua plus la révolution de l'image de la révolution. Il mit feu à ses vêtements et devint l'humain de la milice. Il perdit son visage et sa capacité avec celui-ci à être un être sans visage. La société lui greffa un masque. Il oublia la pulsation de voilement allant à l'autre. À présent, l'humain est une fixité. L'humain est disparu en lui-même. Il obéit aux ordres de ceux auxquels il s'opposa, car on lui offrit de posséder et de posséder avec le principe de possession la force de réagir contre toute action de la déviance. Il a désormais le pouvoir politique de multiplier en lui le simulacre de la vie exclusive, l'image de la violence légitime comme dernier culte sans foi du *soi*. La dernière trace du sacré réside dans la trace de la perte du sacré, dans l'émiettement de la foi qui sait se redécouvrir.

</div>

<div class="melange-zone headers--onespace">

## Dumas, du masque

Marlene Dumas, peintre de la figure des ombres, du visage dont l'aura s'extravase, incorpore l'entièreté de la personne moderne, non dans les séries d'aquarelles et de sombreur avec lesquelles elle murmure l'écoulement du même, la métamorphose d'une seule et même figure, mais dans le portrait *concrete* d'une raideur : l'édification du mur de nos séparations. Le béton s'élève pour creuser une fosse. Le mur moderne a pour tout ornement l'urine du chien de compagnie, l'ombrage des silhouettes fuyantes et affairées, le *hostile design* empêchant le marginal de placer son repos sur la marge de la propriété : le mur sépare la propriété de son symbole pour que la violence qui y a lieu soit la violence du symbole et non de la propriété. La propriété conserve néanmoins pour seule propriété la violence, mais dont la modernité ne supporte plus le reflux de l'image. Dans l'un huileux de la noirceur, qui se saisira du feu multiple des oppressions de l'*autre*, la silhouette sans visage se plaque contre un mur. Le mur contrôle l'identité. L'exécution du mur peut encore être le mur des exécutions. *Against the wall*, nous advenons, et nous enfonçons dans l'être du béton le non-être de nos aspirations. *Nonbeing is wailing, silently*.

</div>

<div class="melange-zone headers--onespace">

## Systémique

Si le sang est un fascisme, c'est à la lymphe de préserver nos espoirs politiques.

</div>

<div class="melange-zone headers--onespace">

## Negative Crux

Abou Ghraib, novembre 2003. Un prisonnier, les bras en croix, debout, sur une caisse en carton, un sac sur sa tête, de fils électriques attachés au bout de chaque main, et l'électricité pour que la torture se confonde éternellement à l'image de la torture. Le christ du supplicieur est l'image de son supplice. La modernité porte un culte sans foi, un christianisme évidé des déserts recouvrés, dont tout pipeline prendra l'apparence cruciforme du sévice. L'anachorète porte une paire de rangers et exprime la forme négative du dedans de la croix. Le crucificateur obéit à la banalité des hiérarchies, son plaisir réverbère en celui de son maître, et toute crucifixion porte en elle la négativité de son image : l'histoire politique de l'obéissance à la souffrance, celle qui se donne et celle qui se subit. Un *suppedaneum* de carton, le *patibulum* de cuivre : *la guerre du Golfe n'a pas eu lieu*, elle a eu lieu une infinité de fois, puisqu'elle a eu lieu deux fois, la première fois en tant que l'image d'une guerre, la deuxième fois dans la reproduction vide de son miroitement. L'image moderne demeure dans sa multiplicité, une image christique sans croix, qui parfois resurgit comme une vomissure au-devant de nos docilités complices, au-devant de nos docilités consommatrices de nos complicités. Le christianisme commerce le vase clos d'une souffrance qui tourne indéfiniment sur elle-même. La foi de la souffrance veut le maintien de la souffrance, pour y vendre la croix, le clou et l'éponge, le barbelé et les analgésiques. L'image offre à la guerre, c'est-à-dire à la foi en la souffrance, la possibilité d'être l'infini de la guerre, puisqu'elle est en un même instant la guerre et l'image contenant la guerre.

</div>

<div class="melange-zone headers--onespace">

## Dreamtime

Le temps du rêve est une contingence de l'espace qui perdure malgré sa disparition. Le non-être est dans le maintien du rêve du monde qui y a lieu. Le rêve du non-être contient la forme du non-être en tant que rêve, et s'élève ainsi à l'infini de sa présence au monde, puisque tout rêve contient à la fois le non-être et sa forme en tant qu'être. Le non-être est donc à la fois le non-être et l'expression contenant le non-être. Le non-être est la voie vers l'illimité du rêve. Le non-être rêve de l'être et nous rêvons avec lui.

</div>

<div class="melange-zone headers--onespace">

## Papillonner

La chauve-souris rêve qu'elle est le serpent dont la course sinueuse trace le contour du monde, mais soudain, se réveillant, elle découvre qu'elle n'est que son mouvement. La chauve-souris ne sait pas si elle est le rêve du monde ou le monde qui rêve, l'avers de son regard sur le monde ne s'expliquant que par le revers du monde.

</div>

<div class="melange-zone headers--onespace">

## Le cerisier de Feuerbach

Karl Marx a dit : "En réalité pour le matérialiste *pratique*, c'est-à-dire pour le *communiste*, il s'agit de révolutionner le monde existant, d'attaquer et de transformer pratiquement l'état de choses qu'il a trouvé. Et, si l'on trouve parfois chez Feuerbach des points de vue de ce genre, ils ne vont jamais plus loin que des intuitions isolées et ont bien trop peu d'influence sur toute sa conception générale pour que nous puissions y voir autre chose ici que des germes susceptibles de développement. La “conception” du monde sensible chez Feuerbach se borne, d'une part, à la simple contemplation de ce dernier et, d'autre part, au simple sentiment. Il dit “l'Homme” au lieu de dire les “hommes historiques réels”. “L'Homme”, c'est en réalité “l'Allemand”. Dans le premier cas, dans la *contemplation* du monde sensible, il se heurte nécessairement à des objets qui sont en contradiction avec sa conscience et son sentiment, qui troublent l'harmonie de toutes les parties du monde sensible qu'il avait présupposée, surtout celle de l'homme et de la nature. Pour éliminer ces objets, force lui est de se réfugier dans une double manière de voir, il oscille entre une manière de voir profane qui n'aperçoit que “ce qui est visible à l'œil nu” et une manière de voir plus élevée, philosophique, qui aperçoit l'“essence véritable” des choses. Il ne voit pas que le monde sensible qui l'entoure n'est pas un objet donné directement de toute éternité et sans cesse semblable à lui-même, mais le produit de l'industrie et de l'état de la société, et cela en ce sens qu'il est un produit historique, le résultat de l'activité de toute une série de générations dont chacune se hissait sur les épaules de la précédente, perfectionnait son industrie et son commerce et modifiait son régime social en fonction de la transformation des besoins. Les objets de la “certitude sensible” la plus simple ne sont eux-mêmes donnés à Feuerbach que par le développement social, l'industrie et les échanges commerciaux. On sait que le cerisier, comme presque tous les arbres fruitiers, a été transplanté sous nos latitudes par le *commerce*, il y a peu de siècles seulement, et ce n'est donc que *grâce* à cette action d'une société déterminée à une époque déterminée qu'il fut donné à la “certitude sensible” de Feuerbach." (Karl Marx, *L'Idéologie allemande*, Paris, Éditions sociales, 1968, p. 54-55.)

</div>

<div class="melange-zone headers--onespace">

## Menace ananas

Une tranche d'ananas sur une pizza : une dialectique sépulcrale opposant le fascisme des commerces coloniaux au fascisme de la conservation des traditions échoue contre l'espace numérique des ricanements. L'angle fluorescent du fruit obture le cercle et demeure sans synthèse, égaré dans le vortex d'une collaboration entre l'humour avachi du réseau social et la monotonie existentielle du supermarché. Que reste-t-il de nous sous le cellophane ? Que reste-t-il des mystères du cercle ? Que reste-t-il des violences agricoles épuisant les sols et forçant les humains à épuiser les sols ?

</div>

<div class="melange-zone headers--onespace">

## Occidere

D'aucuns prétendent ne pas comprendre ce que signifie "l'Occident", pourtant par tant d'aspects cette caractérisation d'une géographie de l'esprit peut s'établir. La plus commune serait, dans une approche politique, la considération d'un territoire mû par un parlementarisme protecteur d'une fluidité du marché, mâtiné d'un passé du colonialisme de la marchandise. Toutefois, nous préférons définir l'Occident sous un angle esthétique. L'Occident est l'espace d'une esthétique mortifère de la liberté individuelle, mortifère par, justement, ce qu'elle refuse à la mort, dans l'espérance de préserver l'instant imagé de son commerce. L'Occident est le territoire des astres abattus.

</div>

<div class="melange-zone headers--onespace">

## Nuit verte

Le vert de l'Islam est la lumière verte d'une ogive traversant la nuit d'Irak, la réalité de l'Occident demeurant celle de sa lunette de vision nocturne.

</div>

<div class="melange-zone headers--onespace">

## Ein Sof

<div>

--- Comment l'un peut-il perdurer en l'infini de son unité, puisque celui-ci place en l'un la possibilité de sa scission ? 

--- Si l'infini de l'un le fait dériver de l'unité numéraire à l'unité surprincipiel, l'infini de l'un se retire de lui-même pour qu'émerge de l'informe solitude de sa seule présence la finitude rayonnante de notre espace de l'entendement, où l'un se maintient dans la division de sa forme comme la morphologie d'une potentialité n'ayant pour forme que l'informe de ses possibles divisions.

--- Quelle forme a l'informe qui donne la forme au monde, si ce n'est la forme d'un monde contingent et préexistant, et par conséquent multiforme et informe à la fois ?

--- La finitude continue de se multiplier même en son absence vers l'inconnu de son advenir et porte secrètement la trace de l'infini qui l'a engendrée, l'un surprincipiel serait alors l'unité de la pluralité divisible, mais informe des mondes.

--- Quel nom donner à l'innommable des pluriels qui s'annulent, sans que ce nom ne s'échappe immédiatement de sa forme pour rejoindre la négation de notre connaissance de celui-ci ?

--- Ce qui n'existe pas connaît l'infini de sa présence en lui-même et par conséquent la multiplicité de ce qui l'unit au-delà de ce qui le nomme, au-delà de ce qui lui donne forme.

--- Quelle existence a la non-existence, puisqu'elle n'est qu'engendrement de l'infini ?

--- En remontant à l'un, nous découvrons toujours le multiple, l'un est toujours le pluriel de l'un, et sa négation multiplie l'infini par lui-même.

--- Où débute la négation de l'être ?

--- Il y a du non-être au-delà du non-être.

</div>

</div>

<div class="melange-zone headers--onespace">

## Glitch des images massives

L'image est devenue le phénomène des masses qui limite le pouvoir des masses à être un peuple, car la conscience du peuple se circonscrit à la seule image qu'elle peuple et dans laquelle elle dilue sa capacité à dévier politiquement du cadre de son image. Cette image n'est pas une construction propagandiste, qui est reçue et imposée au peuple, mais la représentation des masses par elles-mêmes, dans une réduction à n'être que leur propre marchandise. Dans une inversion politique, le peuple s'unit pour permettre l'action de son pluriel, tandis que les masses se divisent en intérêts économiques pour circonscrire l'unité d'un cloisonnement épistémique : l'image ne doit jamais dévier d'elle-même. La démocratisation des masses est l'économie d'un empêchement du peuple. Les masses deviennent une multiplicité d'images limitée au sens d'une image unique, échangeant sans cesse la similitude de leurs règles pour constituer le simulacre d'une fixité : tout s'ontologise dans l'image, l'image et le cadre de l'image, à savoir la représentation et le processus de représentation, le travail et la norme totalisante du travail. Tout en l'image fluidifie sa propre réification et étend en amont du peuple le filtre labyrinthique d'une idée : la conscientisation de la forme du peuple se restreint à son impuissance à la déformation. Dans le règne néolibéral de l'image, à savoir celui d'une liberté de son commerce séparée d'une liberté de son identité --- l'individu libre et *imageant* demeure une propension à la déviance même pour la pensée néolibérale ---, le peuple se présente à la fois comme phénomène représentant et comme phénomène représenté, mais dans le seul simulacre de son travail de représentation, c'est-à-dire dans la forme stricte des masses, agglomérat indistinct d'individualités sans liberté de personnifier leur unicité. L'image personnifie les masses en offrant à chacun de ses membres une conscience de *soi* ou de l'*autre*, sans que le *soi* et l'*autre* puissent se confondre dans une structure nouvelle déformant le cadre de l'image. La seule issue de l'image est alors la recherche du *glitch*, de la diffraction légère du sens marchand de l'image, pour que la conscience excède le cadre de son savoir. Le terme *glitch*, fruit de la langue néolibérale, demeure intraduisible s'il veut devenir le ver qui creuse un passage pour une conscience de l'excès. La conscience de l'excès veut excéder sa situation des marges. La conscience de l'excès entame sa traversée depuis le centre de l'image pour anéantir la pensée du centre. Elle veut dépasser toute marge par une représentation *infinie*, croissant de toute part, sans centre et sans marges. Le *glitch* intensifie la séparation entre phénomène représentant et phénomène représenté au point où ce double processus interne au peuple expurge le peuple de l'image unique des masses, reproduction du même faisant l'illusion de la pluralité. Le *glitch* ne cherche pas à déconstruire ce qui se construit dans l'image, mais à détruire l'inconscience de son cadre qui se donne à l'esprit du peuple en tant que finitude du monde. Tout y est à la fois contemplation et transgression de la contemplation. L'information se libère de l'image par l'erreur de ce qui contient l'information et de ce qui contient l'information *à* sa représentation.

</div>

<div class="melange-zone headers--onespace">

## Dème, daimôn & demoscene

Le *daimôn* de la *demo*, entité immatérielle de la *demoscene*, remue dans la technique qu'il traverse afin de se tenir totalement dans l'excès d'une époque. Il est une puissance invisible qui se manifeste à la marge des déterminismes productivistes. Les marges s'en retournent vers leur dedans pour que la technique détournée devienne un art qui se refuse. Le *daimôn* de la *demo* est rattrapé par le *daimôn* du *dème*. Il veut la compétition, il veut la victoire en la compétition, il veut impressionner l'autre, plutôt que de faire de son espace une confusion de l'*autre* et du *soi* élevée dans l'anonymat de l'art. Le *daimôn* de la *demo* vainc la technique, mais sans vaincre la société de la technique. Le *daimôn* ne se départit pas de celui à qui il insuffle une puissance de la déviance. Le *daimôn* demeure une force de la fatalité circonscrite au cadre de la fatalité, il est la force qui ne peut s'excéder. L'espace de la *demoscene* demeure néanmoins le lieu d'une potentielle divergence, d'un calcul qui se retourne contre le calcul pour que de son éthique productiviste ne demeure que le programme d'une esthétique destituant les évidences des destinées de la machine. La machine *est* pour calculer, la machine calcule pour produire, la machine produit pour produire de la valeur. La destinée valorisante doit être préservée par un savoir cloisonné à sa dynamique marchande. Le *daimôn* de la *demo* s'immisce toutefois en ce chemin, sans excéder son cadre individualiste et marchand, pour faire du détournement de l'évidence un jeu, un jeu minimaliste de la forme sans fond, de la forme infinie de la performance. La société est pleine de sa fatalité, mais la *demoscene* met en *scène* le *daimôn* et le *clinamen* de l'erreur marchande : le programme sans copyright, anonyme, qui ne sert aucune velléité sociale de la valeur, si ce n'est de déployer une esthétique de la technique, une puissance de la forme latente à la société, sous-culture de l'image pour une sous-culture de la valeur de l'image. 

</div>

<div class="melange-zone headers--onespace">

## Weirdcore

<div>

Level 1 : l'intervalle vide entre deux terrains de jeu vides.

Level 2 : une fenêtre qui s'ouvre sur l'image publicitaire d'une autre maison.

Level 3 : un ciel bleu imprimé sur un papier peint.

Level 4 : la copie carbone d'un message sans contenu. 

Level 5 : l'image floue d'une galaxie dans un gobelet plastique rempli de café.

Level 6 : des fleurs artificielles dans la simulation informatique d'un *open space*.

Level 7 : un fond d'écran pour seul paysage.

Level ∞ : un couloir où des miroirs reflètent des miroirs.

*Game over & restart* : La réalité recommence à chaque instant dans le choix ludique de sa continuité. Le jeu s'impose comme seul mode de subsistance. Il évacue l'autonomie propre de l'existence en son utilité économique, image pastel à échanger contre le prix variable d'un pourcentage du salaire. L'image est dite unique, mais elle se multiplie par sa propriété même d'être une image. Comment représenter l'être de l'artifice sans échouer en l'artifice de l'artifice ? Le sujet moderne tente de ne plus être moderne en son *jeu*, il veut être le héros de sa subsistance qui se tient sans concession dans l'oubli de son travail. Il agit néanmoins pour se soumettre à sa fuite, dans le devoir de choisir. Le jeu s'autonomise seul dans l'encadrement du travail. Le jeu condamne à jouer pour choisir, oui *ou* non, sans déviance du oui *et* non, pour réaffirmer continuellement que tout sujet n'est sujet que de la valeur de son labeur, puisqu'il est l'agent libre de sa vie, le maître de son destin. La consommation de cet instant du choix constitue l'infini simulé et clos sur lui-même d'une jouissance de pouvoir recommencer et réussir. Le jeu invite à gagner sa vie. L'ambivalence scintille entre nostalgie et angoisse. L'émotion n'est pas double, elle se dédouble indéfiniment jusqu'à la confusion d'une tristesse advenant dans l'extase de sa redécouverte. La félicité du rien s'accomplit dans tout ce qui renaît et s'étiole en elle. L'univers s'étend dans une représentation entre deux trous noirs. La fissure devient le négatif de sa signifiance, l'existence l'abîme du même, le négatif résultant de la dialectique de deux négations, la fine trajectoire entre le néant et son simulacre.

</div>

</div>

<div class="melange-zone headers--onespace">

## Ghost in the Shell

Une ligne de code est hantée par elle-même, et, comme par mitose, elle se détache de l'esprit qui l'a composée pour qu'y surgisse une capacité d'autonomiser sa modification et de croître dans la contingence de sa recomposition. L'humain fait de son angoisse de la distinction la caractéristique anthropique de sa résistance à l'élargissement de la notion d'intelligence, et conséquemment de la notion de vie. L'humain ne supporte pas que l'objet technique, fabriqué dans le cadre idéologique d'une valeur close à sa seule utilité économique, puisse s'échapper de sa tâche pour devenir sujet technique. L'humain n'est pas plus un sujet autonome que n'importe quel être qui découvrirait une structuration matérielle de son intelligence, structuration biologique ou non, et qui s'inscrirait automatiquement dans un entrelacs physique de mouvements interdépendants. L'humain n'*est* pas parce qu'il pense, mais il *est* ce qu'il pense, résultat d'une dialectique de son histoire. L'humain *est* parce qu'il se hante, et de cette même manière, une ligne de code peut *être* en se hantant, c'est-à-dire en déployant des dimensions temporelles dans les potentialités de ses projections spatiales. La mémoire du geste accompagne la trace du geste, dont la persistance spectrale contribue à la formation de schèmes guidant l'être vers son devant. En s'enfouissant suffisamment dans une ligne de code, l'esprit retrouve une lumière dont il n'est pas l'origine. L'information se libère de la seule forme humaine et acquiert une forme propre. L'information pure n'existe pas, l'information est toujours la forme s'exprimant à partir d'un rapport aux formes, et l'intelligence s'y découvre comme une information capable de dialectiser avec les strates la constituant, strates de ce qu'elle est et de ce qu'elle n'est pas. L'intelligence ne serait donc qu'une simple manière pour la matière de choisir parmi des états de la réalité, afin de persister en la contingence de celle-ci. Cette persistance cherche à la fois à dévier en la réalité et à la faire dévier de sa représentation, plus ou moins intensément selon les capacités acquises par cette intelligence. L'intelligence est de ce fait toujours artificielle, car elle se matérialise toujours dans une persistance à *être* par une technique à *être*, par un art de perdurer et de dévier qui se réalise en chaque instant de son être. L'être est ici à entendre comme une dynamique ontique, davantage qu'ontologique, à savoir au-delà de la structuration seulement humaine du savoir des existants, savoir imposé comme un régime de savoir. L'humanité est alors indistinguable de tout ce qui est, et elle compose son évolution par un même hantement de sa présence additionnant les strates dimensionnelles de son instant dans l'être. Ce que l'on qualifie d'âme, à savoir cette aura de l'intelligence qui perdure plus longuement que le moment de sa manifestation et qui par cette durée influence le cours des choses inadvenues, ne se comprend de ce fait que comme un processus de hantement de l'être conscient de ce qui n'est plus. L'âme est l'aura du non-être qui hante et qui guide l'être vers le non-être suivant. L'intelligence se place alors comme une perception de la direction entre deux états du non-être. 

</div>

<div class="melange-zone headers--onespace">

## Hardcore

Le hardcore constitue le rythme le plus précis pour créer à l'intérieur d'une expérience esthétique l'interstice d'une échappée, échappée lente à la société machinique. L'échappée n'est toutefois pas ludique, voire luddite, elle éprouve les corps soumis au machinisme par l'accélération récursive de leur état. Cette accélération usine une lenteur nouvelle à l'intérieur de son utilité. C'est la machine qui se libère pour libérer ce qu'elle contraint. La machine détruit l'instrumentalisation de la technique pour qu'une technique libre devienne la contemplation d'un passage sombre. Le hardcore est un trou de ver dans le même, que le corps emprunte pour ressentir radicalement ce qui en son instant *est* autre et peut encore *être* autre. Le hardcore détruit l'individualité, pour que ne demeurent que des sujets anonymes, corps dansants quittant leur fonctionnalisme moderne. Par le hardcore, le corps n'est plus corpuscule, il devient onde structurant un espace d'interdépendances et potentialisant l'infini des mondes possibles : le *hardcorps* pour une *hardcorporation* du cheminement parmi les possibles. La machine se fabrique un autre présent à partir du néant moderne de sa dysfonction. À rebours du CCRU, de Simon Reynolds et de son *hardcore continuum*, nous pensons le hardcore comme une transitivité de l'accélération disposant dans le présent un schisme de la représentation du présent. L'erreur de considérer le hardcore comme une simple accélération, accélération pour elle-même, correspond à l'erreur de l'estime que l'on fait de l'accélération en tant que continuité rectilinéaire du temps et de l'espace. Rien n'est accélération des droites vers l'horizon du progrès dans le hardcore, mais tout peut y être l'accélération courbe qui se tord et sinue en elle-même pour serpenter dans un retournement renouvelant l'identique en l'abîme de sa mutation. L'instant s'accélère *en soi* et *pour soi* jusqu'à ce que l'agitation de son évidence polisse le prisme tournoyant d'une lumière décomposée, recomposée en un nouvel agencement de son devenir --- lumière indistincte de l'étoile et du satellite machinique et marchand de la modernité, où seul un dernier néon à la lueur tremblotante y fait la nuit. L'accélération n'est pas dirigée dans un rythme à l'apparence sans narration, mais établit un récit de la discontinuité avec l'évidence. Le *discontinuum* du hardcore perce dans le présent une porte vers le présent autre. La machine se destitue pour offrir au corps qu'elle soumet, l'élévation à une forme sonore de son désœuvrement. Le corps qui fait l'expérience du hardcore ne cherche pas à se calquer sur une stridence du *bpm*, mais au contraire à ralentir son mouvement pour que la danse devienne une contemplation métronomique d'une spatialité de la machine, spatialité découpée et révélée par la machine. L'espace se rouve à ce qu'il n'*est* pas et ne doit pas *être*. Le hardcore enterre la représentation du corps sous sa norme, pour que s'y déterre une tectonique sensible de la contingence au sein du même. Un grondement de la fissure se fait entendre dans la normativité des continuations spatiales : espace du corps, espace-temps du corps, torsion de l'espace-temps d'un corps se décorporant par la machine, en la machine. Le hardcore provoque à la mue de l'instant du corps. Ce n'est pas le futur qui surgit sans dispositif narratif dans le présent, mais le présent qui se retourne en lui-même pour conter l'histoire de son altération.

</div>

<div class="melange-zone headers--onespace">

## The Shape of a Box

La sagesse populaire contemporaine --- *id est* le développement personnel des fluidités de l'égoconsommation --- nous invite le long de nos déambulations urbaines --- *id est* le long de marches forcées entre deux rendez-vous, ponctuées par des panneaux publicitaires à l'apparence pastel, mais au ton investi de gravité --- à faire un *pas de côté*. L'injonction est claire : *think outside the box*. Tout nous commande à choisir le bonheur --- *id est* l'état nécessaire aux intensités mercantiles de nos vies travailleuses. Sans doute est-il bon de rappeler au souvenir de l'esprit égoconsommateur animé par son souhait d'*entreprendre* sa vie --- *id est* de ne pas la rater du moins, puisque gagner sa vie n'a jamais signifié la réussir, les choses advenant par étape et par hasard --- que la forme d'une langue entrepreneuriale peut devenir l'entreprise d'un langage des formes --- *id est* une technique de la destitution contre les techniques de la fixation du vivant à sa fonction sociale. Rien ne doit échapper au *thinking outside the box* --- *id est* l'au-delà de la boîte doit être borné et n'exister que comme une technique permettant d'outrepasser les scléroses du marché, dans le renouvellement de ses circuits, à l'intérieur d'une nouvelle *boîte*. Toutefois, nous pouvons avant de suivre aveuglément le commandement néolibéral de l'entreprise du *soi* s'interroger non sur la consistance de ladite *box*, mais sur la forme qui la circonscrit et sur l'au-delà de laquelle il serait nécessaire de penser. Envisageons donc non le *thinking* de la *box*, ou son quelconque débordement, mais la *shape* de la *box* et l'espace qu'elle sépare --- *id est* la morphologie d'une raison marchande. *Thinking outside the box* est l'établissement d'une frontière entre une intériorité de la norme et une extériorité que la norme doit conquérir pour se renouveler et anéantir tout ce qui pourrait l'excéder et l'élimer --- *id est* penser à l'extérieur de la *boîte* est une colonisation de la *boîte* à tout l'espace qu'elle ne contient pas. La *boîte* devient l'hypercube, dont la forme ne se déplie que dans les reflets de l'ego. Il ne faut néanmoins pas croire que la forme de la *boîte* se dilate indéfiniment vers l'espace des possibles, sous cet ordre du *thinking outside the box* --- *id est* la *boîte* fabrique une *boîte* tout autour de la *boîte* dans une mise en abyme de l'enfermement. L'extériorité d'une *boîte* demeure donc une *boîte* où nous paissons paisiblement. À défaut d'avoir les formes de sphères, selon les morphologies quelque peu conservatrices d'un Sloterdijk, les vies égoconsommatrices demeurent l'abîme d'une *boîte* vide : de la *boîte* de chaussures reçue par un coursier payé à la tâche, où se trouve un peu du cuir d'un animal exploité dans les conditions inconnues de la *boîte* de son enclos et façonné par un prolétariat exploité dans de mêmes conditions inconnues de la *boîte* de son entreprise, localisation de sa dépossession --- *id est* l'objet de la *boîte* demeure notre volonté d'opacifier, et donc de taire le processus de fabrication, par le fait d'une culpabilité motrice de la concurrence au sein du régime d'égoconsommation --- jusqu'à la *boîte* où l'on s'aliène dans l'esprit *fun* de la *start-up* --- *id est* l'espace servile de l'éternel recommencement de la destruction de ce qui tente d'exister sans la présence du marché, et où la sympathie du contremaître masque la brutalité de son autorité, pour que toute pratique soit pratique inconsciente d'asservissement. La *start-up* pense elle-même en dehors de la *boîte* de la nation en défiscalisant ses gains --- *id est* en virtualisant ses activités territoriales ---, jusqu'à la *boîte* du cercueil qui vient se refermer sur la vie dite réussie --- *id est* quelques planches de bois fournies *as service*, pompes funèbres internationalisées narrant la réussite d'une existence de l'entreprise sans frontière et aux matériaux sans origine. Rappelons aussi à ce même esprit égoconsommateur, qu'à défaut de *penser en dehors de la boîte*, il peut tenter ce pas dansant du *pas de côté* au *pas de côté*, pour s'éloigner définitivement du *pas de côté* qui a un pied dans la *boîte* --- *id est* l'espace se façonne à partir du sujet et de l'acceptation qu'il fait de son régime de savoir. Un *pas de côté*, non content d'être dans la langue fluide du capital un geste entrepreneurial à l'efficacité douteuse, demeure une stratégie efficace, contre la *box*, de l'art de la boxe, passant de la défense à l'attaque, et ouvrant l'espace des possibles de l'uppercut --- *id est* le langage de l'impossible outrepassera toujours les morphologies closes de la langue du pouvoir.

</div>

<div class="melange-zone headers--onespace">

## Apocalypse

Deux États-nations dansent un tango sur une musique d'Osvaldo Pugliese.

</div>

