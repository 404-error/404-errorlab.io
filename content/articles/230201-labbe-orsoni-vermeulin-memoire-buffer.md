---
persona:
  - Olivier Labbé
  - Jérôme Orsoni
  - Nicolas Vermeulin
title: "Mémoire Buffer"
slug: "memoire-buffer"
date: 2023-02-04
echo:
  - graphique
images:
  - "img/230201-labbe-orsoni-vermeulin-memoire-buffer.jpg"
gridimages:
  - "img/230201-labbe-orsoni-vermeulin-memoire-buffer-grid.jpg"
sourceimage: "Nicolas Vermeulin"
notes: "Mémoire Buffer est un état de la vidange d’une mémoire visuelle. L'ordinateur se souvient, avec sa caméra ouverte, face à nous, face au monde, et filme malgré l'erreur. Mémoire Buffer est un instantané instable, une courte période d’un « crash memory », une mise en mémoire de la rupture soudaine de nos attentes. Mémoire Buffer est l'errance de la technique, l'événement « dump » où vacillent encore quelques traces du souvenir. Mémoire Buffer, une vidéo de Nicolas Vermeulin, avec une composition sonore d'Olivier Labbé et un texte de Jérôme Orsoni."
summary: "Mémoire Buffer est un état de la vidange d’une mémoire visuelle. L'ordinateur se souvient, avec sa caméra ouverte, face à nous, face au monde, et filme malgré l'erreur. Mémoire Buffer est un instantané instable, une courte période d’un « crash memory », une mise en mémoire de la rupture soudaine de nos attentes. Mémoire Buffer est l'errance de la technique, l'événement « dump » où vacillent encore quelques traces du souvenir. Mémoire Buffer, une vidéo de Nicolas Vermeulin, avec une composition sonore d'Olivier Labbé et un texte de Jérôme Orsoni."
citation: "j'ai tout oublié"
---

{{< youtube H3O8ymM-ZV4 >}}

J’ai tout oublié\
tout enfoui\
dans la préhistoire des sciences\
miroir tendu sous nos basques agiles\
qu’il est haut le soleil\
stocks brûlés\
de l’énergie des fossiles\
la pierre sous la pierre\
et le ciel de nos misères\
sa vérité atone\
loin\
au-dessus de la loi\
noir\
dis-tu\
et tu fermes les yeux\
éclaircies en série\
qui laissent tout glisser sur ta peau\
immaculée sauf\
le désir intact\
qui garde le cap\
tout contre toi\
à même la chair\
contact\
on me dira encore dans mille ans encore\
regrette\
mais cela ne me fera rien\
moi qui ne suis ni n’existe\
car ma physique en effet\
règne maîtresse\
elle\
qu’on lèche et adule\
encense et\
porte en triomphe\
tombale des vaincus\
nous qui fûmes vainqueurs\
et réciproquement\
aussi\
j’ai tout effacé\
et fait le chemin\
à l’envers\
les pieds nus dans le chaos\
qui entendit mon chant de déroute ?\
des milliers de pas dans le silence\
exultent toujours\
et nous sommes nous\
enfants trop bien nés\
muets comme nos alphabets\
usés\
que le mol honneur d’être\
ne sauvera pas\
qui en a le souci ?\
qui le soin ?\
je pleure\
et ces larmes\
gorgées du sang des autres\
mes ancêtres dégénérés\
ces larmes inconnues\
ruissellent\
dans le vide\
monumental.
