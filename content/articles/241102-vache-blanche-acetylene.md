---
persona:
  - Jacques Vaché
title: "Blanche Acétylène"
slug: "blanche-acetylene"
date: 2024-11-26
echo:
  - rythmique
images:
  - "img/241102-vache-blanche-acetylene.jpg"
gridimages:
  - "img/241102-vache-blanche-acetylene-grid.jpg"
sourceimage: "cyberpoetique"
notes: "Le 26 novembre 1918. Ô la putain de guerre, t’as raté Jack ! Voilà Vaché tout à sa fureur. Et avec lui, la blanche acétylène plein les narines, on se faufile parmi nos beaux whiskys, ça se pâme. Embrasements. Eh quoi, le siècle à la renverse."
summary: "Vous tous ! — Mes beaux whiskys — Mon horrible mixture ruisselant jaune — bocal de pharmacie — Ma chartreuse verte — Citrin — Rose ému de Carthame — Fume ! Fume ! Fume ! Angusture — noix vomique et l'incertitude des sirops — Je suis un mosaïste."  
citation: "Vous tous ! — Mes beaux whiskys"
poetry: true
hangindent: false
affiche: true
---

26 novembre 1918

<p class="centre">&mdash; Blanche Acétylène !</p>

 

<p>Vous tous ! &mdash; Mes beaux whiskys &mdash; Mon horrible mixture ruisselant jaune &mdash; bocal de pharmacie &mdash; Ma chartreuse verte &mdash; Citrin &mdash; Rose ému de Carthame &mdash;</p>

<p><span style="display:inline-block;width:3em;"></span>Fume !</p>

<p><span style="display:inline-block;width:6em;"></span>Fume !</p>

<p><span style="display:inline-block;width:9em;"></span>Fume !</p>

 

<p>Angusture &mdash; noix vomique et l'incertitude des sirops &mdash; Je suis un mosaïste.</p>

<p>…«&nbsp;Say, Waiter &mdash; You are a damn'fraud, you are &mdash;&nbsp;»</p>

<p>Voyez-moi l'abcès sanglant de ce prairial oyster ; son œil noyé me regarde comme une pièce anatomique ; Le barman me regarde peut-être aussi, poché sous les globes oculaires, versant l'irisé, en nappe, dans l'arc-en-ciel.</p>

<p>OR</p>

<p>l'homme à tête de poisson mort laisse pendre son cigare mouillé. Ce gilet écossais ! &mdash;</p>

<p><span style="display:inline-block;width:6em;"></span>&mdash; L'officier orné de croix &mdash; La femme molle poudrée blanche bâille, bâille, et suce une lotion capillaire &mdash; (ceci pour l'amour.).</p>

<p><span style="display:inline-block;width:6em;"></span>&mdash;&nbsp;«&nbsp;Ces créatures dansent depuis neuf heures, Monsieur.&nbsp;» &mdash; Comme ce doit être gras &mdash; (ceci pour l'érotisme, voyez-vous.).</p>

<p>&mdash; alcools qui serpentent, bleuis, somnolent, descendent, rôdent, s'éteignent.</p>

<p><span style="display:inline-block;width:3em;"></span>Flambe !</p>

<p><span style="display:inline-block;width:6em;"></span>Flambe !</p>

<p><span style="display:inline-block;width:9em;"></span>Flambe !</p>

 

<p class="centre">MON APOPLEXIE !!</p>

 

<p>N.B. &mdash; Les lois, toutefois, s'opposent à l'homicide volontaire &mdash; (et ceci pour morale… sans doute ?).</p>

 

<p class="droite">(.<em>Harry James</em>.)</p>
