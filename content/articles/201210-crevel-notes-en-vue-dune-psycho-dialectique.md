---
persona:
  - René Crevel
title: "Notes en vue d'une psycho-dialectique"
slug: "notes-en-vue-dune-psycho-dialectique"
date: 2020-12-10
echo:
  - psychique
images:
  - "img/201210-crevel-notes-en-vue-dune-psycho-dialectique.jpg"
gridimages:
  - "img/201210-crevel-notes-en-vue-dune-psycho-dialectique-grid.jpg"
sourceimage: "https://commons.wikimedia.org/wiki/File:1933-may-10-berlin-book-burning.JPG"
notes: "Ce texte a paru en mai 1933 dans le numéro 5 de la revue *Le Surréalisme au service de la révolution*."
summary: "D'un point de vue dialectique, du point de vue du matérialisme dialectique, il s'agit de réduire à l'état d'incompréhensible souvenir les cloisons si arbitrairement étanches qui n'ont pas encore cessé de séparer le monde extérieur du monde intérieur. Quand ce ne serait que pour préciser, objectiver l'objet, il importe de savoir *comment*, avec quelles déformations, chemin faisant, ses reflets aboutissent au sujet, au subjectif."
citation: "cul-de-sac métaphysique"
---

D'un point de vue dialectique, du point de vue du matérialisme dialectique, il s'agit de réduire à l'état d'incompréhensible souvenir les cloisons si arbitrairement étanches qui n'ont pas encore cessé de séparer le monde extérieur du monde intérieur.

Quand ce ne serait que pour préciser, objectiver l'objet, il importe de savoir *comment*, avec quelles déformations, chemin faisant, ses reflets aboutissent au sujet, au subjectif. D'autre part, afin d'étudier les sentiments de l'homme, il faut ne jamais oublier que l'humanitarisme sentimental cache, à l'ombre de généralités bêlantes, un individualisme dont les appétits sournois décident des ruses oratoires du masque.

La loi d'universelle réciprocité qui préside aux rapports des choses entre elles, des hommes entre eux, et, dans chaque homme, des images, des idées entre elles, ne règle pas d'un mouvement moins perpétuel les relations de choses à hommes, d'hommes à choses, des choses les plus lourdement quotidiennes aux images, aux idées dont les surprises saugrenues ou bouleversantes n'ont point d'abord permis de soupçonner la place qu'elles occupent, d'où elles rayonnent dans la ruche aux associations.

La méthode fait la science. Mais à la lumière des découvertes de la science, la méthode, à son tour, doit accepter de se laisser faire et refaire.

Ainsi, les immenses progrès des sciences naturelles, au cours des siècles derniers ont été dus à la méthode analytique. Mais cette habitude de travail passée dans la philosophie produisit l'étroitesse spécifique d'une époque, la méthode métaphysique dont Marx, Engels, Lénine ont dénoncé les méfaits.

Misère de la philosophie, misère générale et très prompte à menacer, contaminer, stériliser tout champ de recherches particulières. Grâce à la dialectique les matérialistes reprennent contact avec la vie, l'étudient dans ses manifestations contradictoires.

Retour de l'abstrait au concret. Le mouvement est rendu aux parties désintégrées, sclérosées, paralysées. L'homme retrouve la possibilité d'agir sur son univers. Tout s'anime. Quel bond en avant ! Révolution. Sur le sixième du globe où sévissaient plus cruellement, plus mortellement qu'ailleurs les institutions d'un monde tombant en ruine, en pourriture (et ces institutions aidaient à la ruine, à la pourriture d'une société dont la ruine, la pourriture les avaient instituées) se poursuit l'édification du socialisme.

Du considérable retard des sciences psychologiques sur les sciences naturelles.

Il n'y a pas longtemps qu'une analyse conséquente a mis enfin quelque clarté dans l'obscurité taboue. Mais si Freud a réglé son compte à l'homme normal, cette entité chère à nos classiques obscurantins, la psychanalyse --- par la faute justement de l'incapacité dialectique de la quasi-unanimité des analystes qui n'ont pas su aller du particulier au général, du général au particulier, qui n'ont pas su voir que le particulier était aussi le général et le général aussi le particulier --- la psychanalyse, dis-je, apparaît prête à faire du complexe le plus complexe un uniforme pour mannequin abstrait.

La mode décide ces messieurs dames de la psychiatro-psycho-philosophie à se vanter de considérer sans arrière-pensée nouménale les phénomènes dans l'étude desquels ils se sont spécialisés.

Et cependant, la phénoménologie, malgré la promesse incluse dans le nom qui la désigne, a été droit au cul-de-sac métaphysique.

"Phénoménologie de l'angoisse", annonce Heidegger mais, au lieu d'étudier le *comment* de l'angoisse, il se contente d'en constater, répéter le *pourquoi surgi du mystère de l'Être qui nous oppresse*.

Nous voilà bien avancés.

Secouons un peu ce pourquoi. Secouons-le comme un prunier. En tombe l'o, la plus prune des voyelles. Donc, reste un "pour qui ?". Ce diseur de "pour qui ?" est dans l'angoisse, parce qu'il ne peut, n'ose se répondre par un "pour moi".

Et même, pourrait-il, oserait-il, qu'il se gâcherait encore son affirmation, rien que de sentir confusément qu'elle ne vise qu'à encourager, rassurer son incertitude.

Ainsi, le petit égocentrique, dans son trop cher milieu, trouve une amande à deux noyaux dont l'une est de miel et l'autre d'amertume.

Ambivalence.

Bleuler a décrit la coexistence dans un même sujet de sentiments contraires, contradictoires pour un même objet. Objet simultané d'amour et de haine. La violence des sentiments est celle de leurs mouvements d'un pôle à l'autre.

Le pont des reflets qui fait la navette du sujet à l'objet permet au premier de métamorphoser le second, pour, à son tour, se métamorphoser lui-même de la métamorphose dont il est l'auteur. Phénomène bien connu, élémentaire de sympathie. Mais les choses se compliquent. La contagion du contradictoire ou même plus simplement du complexe d'un individu à l'autre ne va pas sans conflit.

L'individu narcisse, celui qui demeuré au stade oral a mangé l'univers et parce qu'il a dévoré, supprimé les objets se joue à lui-même le rôle d'objet, non seulement ne se suffit pas à lui-même, mais, de lui-même, se détruit. Dans l'île dont le contour est celui de sa petite personne cet isolé succombe au miroir interrogé --- interrogateur des eaux les plus médiocres, les plus vaines, les plus superficielles.

Narcissisme de Heidegger quand, lors d'une leçon inaugurale, à la question : "Qu'est-ce que la métaphysique ?" il se répond : "Chacune des questions métaphysiques ne peut être posée que si celui qui la pose est, comme tel, inclus dans la question, c'est-à-dire se trouve lui-même mis en question."

On ne va pas vite à ce train-là. Le métaphysicien inclus dans sa question comme l'escargot dans sa coquille sera d'autant plus lent que le souci de priorité l'arrête à la moindre poussière d'ombre.

Du scepticisme.

L'intellectuel français, produit et porte-parole d'une bourgeoisie avare, doute de son doute, donc croit en soi.

Le conscient, thèse. L'inconscient, antithèse. À quand la synthèse ? Tiré à hue et à dia l'individu ne surmonte pas son dualisme psychique. Il ne se sent pas lui-même en présence des autres. Il reste en deçà ou s'égare au-delà. Errements photogéniques et pathétiques. Inquiétude, départs. La littérature et les littérateurs chérissent ces thèmes dont les arabesques les mènent dans la gadoue bondieusarde.

Le bonhomme qui au lieu de courir la prétentaine reste à la maison, ne se sent pas non plus lui-même avec lui-même. Il voudrait s'élaborer, mais, dans le laboratoire de sa sacrée salope de petite vie intérieure, il se laisse encore éparpiller par mille courants d'air.

La tour d'ivoire, simple façade en demi-cercle vis-à-vis du public. Un équateur aussi sordide qu'inexorable a coupé en deux la circonférence. Chaos de vents coulis. Les prétentieux parlent de tempêtes. Ça fait plus riche.

Encore et toujours les questions.

Pour le poseur de questions, se mettre soi-même en question n'est-ce point, avant tout, un moyen, le meilleur, d'éviter les questions embarrassantes ou même simplement un peu moins propices à la bonne opinion qu'il tient, coûte que coûte, à garder de soi. Il y a du calcul jusque dans la bien peu apparente économie du délire d'auto-punition, l'auto-punisseur se réservant le bénéfice d'une offensive qui, si elle est contre lui-même, ne lui en vaut pas moins l'avantage de réduire les autres à la défensive, à la défensive pour lui-même.

Les questions pourraient bien être comparées aux clous du fameux dicton. L'un chasse l'autre. Le chasseur, même et surtout s'il feint le détachement et se réduit à l'état, au rôle de l'inconnue algébrique, ce monsieur X ne tient guère à revenir bredouille.

Par un goût très lâche du confort au moins moral, le petit inquiet, le grand angoissé finissent toujours par se décider au transfert de leurs tourments sur la voie de garage des aspirations religieuses.

Honteux pragmatisme, des mystiques et autres.

Le surréalisme, par l'expérimentation qui lui est propre multiplie et précise ces correspondances génialement entrevues par Baudelaire du jardin de la perception.

Quel lien existe-t-il entre la pensée à son moment le plus désincarné et une sensation, à ces minutes où l'épithélium apparaît assez joyeux pour qu'il ne soit question de songer à le doubler d'un écho ? Aujourd'hui nous savons que l'abstrait le plus implacablement géométrique révèle, prolonge, dans toute leur énergie, de très concrets désirs. À Freud revient le mérite de l'avoir découvert.

Déterminisme complexe, complexe déterminant. Somme ou, mieux, résultante de tant de déterminismes que le déterminé, à son tour, détermine.

Dans quelle mesure et comment ces déterminismes se déterminent-ils, l'un l'autre, s'accordent-ils ou entrent-ils en conflit ?

Les réponses nous seront données par la science encore naissante de la personnalité, aux progrès de laquelle n'est pas sans avoir amplement contribué la très récente thèse du Dr Lacan : *De la psychose paranoïaque dans ses rapports avec la personnalité*.

Dans une première partie consacrée à la position théorique et dogmatique du problème, Lacan différencie, afin d'en mieux connaître les rapports, ce qui a été subjectivement éprouvé et ce qui peut être objectivement constaté. À l'ordinaire ou bien le subjectivo-introspectif tombait dans les chausse-trapes métaphysiques ou bien l'observateur soi-disant objectif, sous prétexte de psychologie scientifique, réduisait le sujet à l'état de ficelle, le condamnait à n'être plus rien que le lien d'une succession de sensations, de désirs et d'images. En vérité, il s'agit d'éclairer aussi bien le dedans que le dehors. Il n'y a point à opter pour une lumière contre une autre, car il n'y a point trop des rayons de l'une et de l'autre contre cette obscurité si longtemps faite au centre et autour d'un problème vital. Les données de ce problème se trouveront, à proprement parler, non point posées mais agitées dans tous leurs mouvants détails à l'occasion de la psychose paranoïaque qui affecte toute la personnalité, la prolonge, la développe, lui sert de miroir grossissant et précisant. Grâce à ce très sensible microscope nous constaterons l'interdépendance des phénomènes internes et externes.

La deuxième partie de la thèse du Dr Lacan est consacrée à l'étude d'un cas type de paranoïa d'auto-punition.

La malade, Aimée, a été arrêtée après avoir tenté de poignarder une actrice connue. Elle avait, auparavant, essayé d'étrangler un éditeur qui n'avait pas voulu publier ses romans dont les passages cités sont d'un extrême intérêt, d'abord, parce que permettant de saisir sur le vif certains traits de son caractère, des complexes affectifs et des images mentales qui l'habitent. Mais surtout, la verve, la grande et subtile allure de ses écrits témoignent d'une valeur poétique assez intransigeante pour que ne puisse qu'aller s'exagérant le désaccord initial entre la créature et le monde qu'elle juge assez détestable pour vouloir le recréer.

On sait en quels profits d'argent et de succès les littérateurs professionnels convertissent ce désaccord initial, indispensable à la moindre inspiration. Aimée ne s'arrête pas, ne s'arrange pas en chemin. Elle va jusqu'à un admirable état convulsif affolé, affolant. Ses élans se heurtent à un bloc abominablement incompréhensif. Ses besoins de solidarité morale, intellectuelle ont été bafoués à tous les coins de rue. Elle a cru "devoir aller aux hommes". Elle a cherché à satisfaire "la grande curiosité qu'elle avait de leurs pensées". Mais les pensées de ces passants l'ont entraînée dans des hôtels meublés où il lui a fallu s'exécuter bon gré mal gré. Cette femme est un levain qui fermente condamné à soi-même. Sa colère brasse les mouvants trésors des profondeurs. À la surface c'est marée basse. La voici seule, abandonnée sur une plage déserte, dans le silence mortel d'une vie, de toute une vie à l'orée d'une activité dont le libre exercice est pratiquement interdit au double prolétariat des femmes du peuple, parce que femme, parce que du peuple.

Aimée. Dérision du nom, calembour du destin. Elle se sentait de force à faire lever des hommes. Les hommes ont cru qu'elle ne cherchait qu'à les lever. Elle rêve d'être reçue garçon. Elle veut aimer la femme, elle veut s'aimer. Est-ce la tendance homosexuelle qui décide du malheur ou le malheur de la tendance homosexuelle. Avant de se frapper soi-même dans la personne d'une actrice connue, son idéal, Aimée a esquissé un geste homicide chez l'éditeur qui ne lui a pas accordé la possibilité de se faire entendre.

"Elle veut faire parler d'elle", dira le flic qui la conduira au commissariat après l'attentat.

"Psychologie de sergent de ville", constatera dédaigneusement et justement Aimée.

Au cas d'un exhibitionnisme sexuel ou meurtrier, comment en juger, sans remonter au refoulement qui est à son origine ? La beauté de certains attentats à la pudeur ou à la vie c'est qu'ils accusent de toute leur violence la monstruosité des lois, des contraintes qui font les monstres.

La victime qui accepte d'être victime, le martyre, a dans la vie une répugnante attitude de cadavre ambulant. C'est la mort. C'est la dégradation de l'énergie, la seule, la vraie, au sens le plus précisément scientifique du mot.

La nature de la guérison nous fixant sur la nature de la maladie, quand l'auteur nous parle des guérisons spontanées qui ne le sont guère, qui ne le sont pas du tout, puisqu'elles *surviennent à la suite d'une résolution des conflits générateurs et dépendent aussi éventuellement de toutes les* conditions extérieures *de nature à atténuer ce conflit, changements de milieu, principalement*, nous voici fixés par le caractère général, social des psychoses qui semblaient les plus particulières, les plus hermétiquement individuelles, dans leur expression et dans leurs causes premières et dernières.

Guérison par satisfaction de la pulsion auto-punitive.

Vertu curative du trauma moral, du choc et de la maladie organique.

Mais la maladie organique elle-même ne satisfait-elle point la pulsion auto-punitive ? Et n'est-ce point d'ailleurs une inconsciente auto-punition qui a décidé d'elle dans bien des cas ? Pour celui qui a cherché, obtenu les conditions matérielles de la maladie, cette maladie du physique sera la chance de guérison de la maladie morale.

Faute d'avoir serré l'examen clinique de certains cas types, comme Lacan a fait de celui d'Aimée, faute d'avoir socialement situé leurs malades ou, plutôt, telle ou telle famille de tel ou tel de leurs malades (puisque le malade avait été si pertinemment situé dans sa famille), faute d'avoir étudié les rapports de telle famille particulière avec la société en général et, ainsi, plus ou moins déformés par la connivence avec les parents ou en réaction contre eux, les rapports entre l'individu et son espèce, la psychanalyse ou plutôt les psychanalystes n'ont pas donné ce qu'on était en droit d'attendre.

La science matérialiste, pour sa psycho-dialectique, a besoin de monographies détaillées, précises, complètes.

Sinon ?

Sinon ce sera la rechute dans un matérialisme mécanique dont Freud lui-même apparaît singulièrement menacé quand, parlant d'un homosexuel atteint de tuberculose des testicules, il constate, sans émettre la moindre vue psychanalytique, qu'après la greffe d'un testicule cryptique, l'homosexuel en question se comporta en mâle et dirigea sa libido vers la femme.

Pourtant du point de vue psychanalytique l'étude précise d'une tuberculose testiculaire, l'examen psycho-clinique de celui qui en est atteint, eût risqué de nous apprendre davantage que toutes les si peu concrètes hypothèses avancées à propos du repas totémique.

On sait que Freud oppose, dans l'individu, les instincts de conservation du moi à l'instinct sexuel, selon lui instinct de conservation de l'espèce. D'où conflit entre l'individu et l'espèce. Il n'y a pas à croire à l'innéité non plus qu'à la génération spontanée de ce conflit dont la résolution (momentanée dans l'amour, quand l'homme rayonnant sur lui-même et sur l'univers du centre de son désir souhaite si fort d'avoir un enfant de la femme aimée) croissante constituerait le progrès à proprement parler.

Dualisme non surmonté.

Nous voyons percer la sourde oreille d'âne de l'idéalisme, quand Freud écrit : "Tout ce qui travaille au développement de la culture travaille aussi contre la guerre."

Mais de quelle culture, de quelle civilisation s'agit-il ?

Et qui donc travaille à leur développement.

En 1933 il semble que la culture, la civilisation de l'Europe capitaliste ne soient plus concevables que par antiphrase. Ce que l'on peut imaginer que Freud entend par culture et civilisation n'a travaillé qu'à la préparation de la guerre. Il est donc singulièrement inconséquent de les féliciter d'être contre ce à quoi elles aboutissent.

Et voilà pourtant toute la conclusion à une correspondance Einstein-Freud luxueusement --- et inoffensivement pour la culture, la civilisation capitalistes et leur petite guéguerre --- publiée par le bureau international de coopération intellectuelle, sous le titre *Pourquoi la guerre ?*.

Einstein qui assume le rôle d'interrogateur constate que la S.D.N. son éditrice n'a évidemment point affranchi les hommes de la menace de guerre et que, d'autre part, la guerre fait la fortune des marchands de canon. On s'en serait douté.

La minorité des dirigeants (c'est Einstein qui les appelle fort poliment ainsi. Nous dirions plus volontiers la minorité des exploiteurs, des profiteurs, profiteurs de guerre) a d'abord dans la main l'école, la presse et toutes les organisations religieuses (voyez culture, voyez civilisation, Freud).

"C'est par ces moyens, selon Einstein, qu'elle domine la grande masse dont elle fait son instrument aveugle."

Instrument aveugle, on a scrupule à faire sienne cette expression quand ce ne serait que le temps de la rejeter.

Cet instrument n'est en tout cas pas de bois. Il souffre et mesure à l'aune de sa souffrance les guimauves empoisonnées, empoisonneuses de la religion, les filandres sanguinolentes du patriotisme. On lui parle de sacrifice, mais il est toujours le même sacrifié du même sacrificateur. L'étendue, le nombre des misères décide du passage de la quantité à la qualité. La meilleure qualité d'un prolétariat c'est sa conscience de classe. Grâce à elle, d'objet, il deviendra sujet. Il n'est plus un instrument dans une main étrangère. Il est l'instrument de lui-même. Des yeux lui poussent, il voit clair. Il ne cherche qu'à démolir les murs qui bornent sa vie, sa vue pour construire des maisons de lumière.

Guerre civile. Elle n'est cruelle que par la cruelle obstination de la minorité favorisée qui s'oppose au devenir de la masse.

Révolution prolétarienne en pays capitaliste défait. L'armée au service de la bourgeoisie victorieuse devient l'alliée de sa bourgeoisie vaincue, son ennemie d'hier. Tout comme Bismarck avait collaboré avec Thiers contre les communards, Clemenceau et Foch rendirent aux généraux du kaiser en fuite canons et mitrailleuses pour réduire les spartakistes. Mais, après avoir couvert de sa complicité une répression qui compte au nombre de ses victimes Karl Liebknecht et Rosa Luxembourg, la France libérale, quinze années durant, imposa à l'Allemagne entière des conditions de vie qui étaient de véritables conditions de mort. Un nationalisme exaspérant l'autre, l'ignoble triomphatrice de 1918, en 1933, doit répondre de l'hitlérisme.

Einstein et Freud qui n'ont pourtant point à se louer de l'antisémitisme qui exile le premier d'Allemagne et demain peut-être menacera le second dans son Autriche et bien qu'on s'attende à ce que de ce chef ils condamnent le capitalisme au moins dans ses manifestations sociales, ne posent pas, un seul instant, le problème sur le terrain convenable.

Einstein déplore, bien abstraitement, ne sait par quel bout prendre cette entité, "l'agressivité humaine qui se manifeste aussi sous forme de guerre civile".

La psychologie ce n'est pas sa spécialité. S'il a choisi la question et l'interlocuteur avec qui en traiter, il est trop clair que c'est parce qu'il est hanté par le très actuel et encore plus angoissant qu'actuel problème et n'arrive point à en penser de façon claire et satisfaisante.

Freud non plus d'ailleurs. Il n'y a qu'à citer parallèlement, pour en être convaincu, ses affirmations dont les antagonismes divers seraient sans doute d'une psychanalyse fort instructive quant aux complexes du grand âge. Il semble, en tout cas, que ce qu'il y a de jeunesse et de vie dans le monde soit odieux à ce grand vieillard. Que valent les défenses d'interprétation religieusâtre qu'il faisait de sa science, dans *L'Avenir d'une illusion*, dès que, sans une ombre de culture marxiste à ce qu'il semble et à coup sûr sans le moindre souci dialectique, il traite d'illusion le très concret et de plus en plus proche espoir donné au prolétariat des cinq sixièmes du globe par l'édification du socialisme sur le sixième que l'on sait ?

L'usage, l'abus du mot *illusion* semblent bien témoigner ici d'un agnosticisme confus et hargneux dont la cause serait, sans nul doute, à rechercher du côté de l'incapacité à l'érection. Œdipe devenu père tombe-t-il en aveuglement même sans se crever les yeux ? Et si nous parlons des yeux de Freud, l'habitude que nous lui devons de savoir nous y retrouver dans la symbolique sexuelle nous autorise à traduire ce mot en objets très précisément érotiques qui ne doivent pas se sentir trop glorieux dans les pantalons du père de la psychanalyse.

Quoi qu'il en soit, de sa psychanalyse bien close, métamorphosée en maison d'illusions, Freud écrit : "Les bolchevistes eux aussi espèrent arriver à supprimer l'agression humaine en assurant l'assouvissement des besoins matériels, tout en instaurant l'égalité entre les bénéficiaires de la communauté. J'estime que c'est là une illusion. Ils sont, pour l'heure, minutieusement armés et la haine qu'ils entretiennent à l'égard de ceux qui ne sont pas les leurs n'est pas le moindre adjuvant pour s'assurer la cohésion de leurs partisans."

Non seulement Freud ignore ou feint d'ignorer quelle nécessité vitale contraignit de s'armer l'URSS entourée d'ennemis, mais encore on s'étonne de constater le peu de logique dont il témoigne puisque dix pages plus tôt, il écrivait : "On commet une erreur de calcul, en négligeant le fait que le droit était à l'origine la force brutale et qu'il ne peut encore se dispenser du concours de la force."

Incroyable déperdition d'énergie de la pensée chez Freud, au cours de cette pourtant très courte lettre à Einstein.

Il commence par trouver insuffisant le mot *force* employé par Einstein qui constate que "droit et force sont inséparablement liés". Il le remplace par violence. Mais lui-même de la violence il retombe dans la force et de la force dans une marmelade pas même humanitariste, car, avoue-t-il, l'idée des dégradations esthétiques, plus que toute autre, lui rend la guerre odieuse.

Il est assez fatigué pour tenir à ses bibelots. On l'excuse. Mais quel jeune psychanalyste prendra la parole ?
