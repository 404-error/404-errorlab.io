---
persona:
  - Marc Verlynde
title: "L'émeute : biffures"
slug: "lemeute-biffures"
papier: "zéro"
date: 2021-02-10
echo:
  - stratégique
  - esthétique
images:
  - "img/210210-verlynde-lemeute-biffures.jpg"
gridimages:
  - "img/210210-verlynde-lemeute-biffures-grid.jpg"
sourceimage: "https://commons.wikimedia.org/wiki/File:Battle_strike_1934.jpg?uselang=fr"
notes: ""
summary: "L'émeute ne mérite aucune définition figée. Une pratique seulement. Une approche par court-circuit pourrait proposer ce point de départ : l'émeute ou l'invention d'un lieu, la fiction d'un endroit habitable, d'un carrefour où l'individu s'abolirait dans un collectif heureux, mouvant. Si, comme disait l'autre, le réel c'est l'impossible, l'émeute ne se déploierait jamais mieux que dans le vide du roman."
citation: "croire à une émeute poétique"
---

L'émeute ne mérite aucune définition figée. Une pratique seulement. Une
approche par court-circuit pourrait proposer ce point de départ :
l'émeute ou l'invention d'un lieu, la fiction d'un endroit
habitable, d'un carrefour où l'individu s'abolirait dans un collectif
heureux, mouvant. Si, comme disait l'autre, le réel c'est
l'impossible, l'émeute ne se déploierait jamais mieux que dans le vide
du roman.

Le roman compris ici comme la réitération du récit d'un échec, la
compréhension aussi, hélas, de la part d'imposture de l'émeute. On peut
l'entendre ainsi : le roman est un art bourgeois ; assis sur sa
certitude de connaître la fin, il ne propose que de minimales variations
autour de sa linéarité. Le roman ou l'épreuve de l'incroyance. On
voudrait croire à une émeute poétique, une autre façon de dire le monde
qui en change le cours. On se contente de témoigner de la permanence de
cette tentation. Il reste, malgré tout, un lieu, palpitant, où se
préserve non la dépassée possibilité d'un renversement, mais l'idée
qu'il reste loisible d'inventer un espace clos, vide, où les possibles
ne seraient pas écrits. Figés comme un spectacle vécu de loin par un
auteur observateur.

Il ne saurait s'agir d'en épuiser les manifestations et de croire
ainsi prouver qu'il existerait dans le roman une tentation de
l'émeute. Approche trop convenue : la véritable émeute serait plutôt
l'invention d'un événement auquel on serait certain d'assister, de
percevoir comme un lieu réel. On pourrait trouver ça un rien bourgeois :
l'auteur égaré dans sa tour d'ivoire déplore le peu de réalité de son
vécu confortablement séparé des sursauts du monde. On pourrait alors
proposer une biffure de l'émeute : elle devient une expérience quand
elle permet de savoir ce qu'il y a non pas derrière (n'est-il pas
temps d'en finir avec le poète voyant, seul à même de percer les
apparences ?) les mots, mais devant les formules, quels devenirs portent
les manifestes, slogans et déclarations.

On s'élance dans l'émeute à partir de l'instant où l'on reconnaît
son imposture. Elle advient dans la certitude que tu n'y participes
pas, ce serait un autre toi-même qui le fomenterait, en décrirait,
intouchées, les promesses d'altérations. Une autre occupation ne
prémédite-t-elle pas une absence, un sujet saisi dans son vide qui enfin
se soulève ?

Ou y assister par écran interposé. Maintenir ainsi le fantasme d'une
appréhension directe de la réalité. Occasion de faire fi des dualismes :
vivre et comprendre, écrire et participer. Sous-tendre un autre
rapport : l'émeute s'écrit à l'imparfait, pourquoi le déplorer ? On
voudrait rendre la chance de l'aspect spéculaire de l'émeute : miroir
de toutes les spéculations, impossibilités, impostures et inachèvements,
de notre saisine de l'être ici, maintenant. Un peu d'ironie aussi
comme seule manière de dire autre chose. Manière aussi d'énoncer cette
platitude : l'événement est un lieu et un lien. L'émeute comme miroir
de ce qui a déjà eu lieu. On passe sur la préliminaire mémoire de la
lutte pour tracer, *devant* Ben Lerner, la mise à la question de ce que
non pas l'auteur a pu en vivre (vérification de basse police), mais la
possibilité d'en restituer l'autre vécu, l'altération, la
superposition de faits susceptibles de rendre intelligibles ce qui, dans
l'émeute, échappe. Les événements, leurs restitutions romanesques, se
répondent : ici les attentats d'Atocha adviennent en écho à *Occupy
Wall Street.* On contemple ce qui advient derrière nos écrans. Tentons
d'en faire autre chose que le signe d'une impuissance.

L'espoir d'une po-éthique est perdu. Comprendre, il faut continuer à
la pratiquer comme interstices d'une réalité immédiatement
compréhensible, réduite à un avis que l'on pourrait livrer sans délai,
distance, déperdition. L'émeute, c'est de continuer à ne rien
comprendre, suspendre le sens, le genre, l'espoir aussi. Une
littérature dite contemporaine recharge le façonnage de ce vécu
collectif, en écrit la contre-narration par fragmentation. Toutes ces
émeutes que tu n'as pas vécues te deviennent alors des épreuves du
réel. L'impossible de la procuration, la compréhension de ce que tu
n'as pas vraiment vécu : on appellera ça le roman.

Une génération en creux, mouvance d'une identité fluctuante dont *Le jeu
de la musique* rend si bien compte, entre Gênes et Vancouver, cet
impératif qui guide toute émeute, toute littérature : un autre monde est
possible. Seulement si on le squatte tant l'émeute paraît la preuve que
toute occupation --- accaparation transitoire, piratage pour en faire
autre chose --- est temporaire. Reflet de cette époque dont, en
l'écrivant, il s'agit de raturer toute nostalgie. On s'y contemple, s'y
reconnaît souvent dans une parole qui ne nous appartient pas, dans cette
*Saison en friche* de Sonia Ristić.

Émeute spéculaire surtout quand elle invente une solidarité avec tout ce
qui aurait pu arriver, c'est de cela que nous voulons garder une mémoire
contestataire. Vivante tant sa pratique suppose une biffure, une
hypothèse, une façon de raconter qui laisserait comprendre comment nos
histoires s'amalgament, deviennent hôtes les unes des autres. À la
lettre, se confondent et s'indiffèrent, comme dans le si variable *If*
de Marie Cosnay.

On pourrait s'en sortir par une pirouette : plutôt qu'une pensée de
l'émeute, il semble que dans l'écrit persiste une émeute pensive. Tout
ce qui soudain contraint à se penser en marge de soi-même, loin de toute
appartenance, voire de toute adhésion à ce que l'on vit. Il faudra,
toujours, relire l'émeute, la relier à autre chose.

Continuer à interroger ce qui réellement est à elle : rien ou pas
grand-chose. Tel serait l'espoir de l'émeute. Pourtant, l'émeute de
l'événement de l'écriture retient ton adhésion. Au prisme de l'écrit,
l'émeute se surchargerait de ratures, repentirs. Image alors peut-être
du monde rêvé, du réel, qu'elle met en jeu : une *biffure* au sens où
l'entendait Leiris, une relecture de l'émeute pour la faire bifurquer
vers autre chose, garder trace, même biffée, des possibles entrevus, des
ratés. Retarder sans doute aussi.
