---
persona:
  - Union des femmes pour la défense de Paris et les soins aux blessés
title: "Appel aux citoyennes"
slug: "appel-aux-citoyennes"
date: 2021-05-21
echo:
  - stratégique
  - historique
images:
  - "img/210521-union-des-femmes-appel-aux-citoyennes.jpg"
gridimages:
  - "img/210521-union-des-femmes-appel-aux-citoyennes-grid.jpg"
sourceimage: "https://commons.wikimedia.org/wiki/File:Commune_de_Paris_barricade_Place_Blanche.jpg?uselang=fr"
notes: |
    L'*Appel aux citoyennes*, affiché sur les murs de Paris le 11 avril 1871, a été rédigé sans doute à l’initiative d’Élisabeth Dmitrieff, militante de l’*Association internationale des travailleurs*, et a été à l’origine de la création de l’*Union des femmes pour la défense de Paris et les soins aux blessés*, créée par Élisabeth Dmitrieff et Nathalie Lemel. Cet appel figure notamment dans le *Journal des journaux de la Commune : Tableau résumé de la presse quotidienne du 19 mars au 24 mai 1871*, publié en 1872 chez Garnier Frères, p. 336-339.

    Le *Manifeste du Comité central* fut, quant à lui, affiché le 6 mai 1871 dans les rues de Paris. Il est une réponse du Comité central de l’*Union des femmes pour la défense de Paris et les soins aux blessés* à un appel lancé par un groupe de citoyennes dans le journal *Le Siècle*, exhortant à la paix et à la compréhension du camp ennemi.
summary: "Paris est bloqué, Paris est bombardé... Citoyennes, où sont-ils nos enfants, et nos frères et nos maris ?... Entendez-vous le canon qui gronde et le tocsin qui sonne l’appel sacré ? Aux armes ! La patrie est en danger !... Est-ce l’étranger qui revient envahir la France ? Sont-ce les légions coalisées des tyrans de l’Europe qui massacrent nos frères, espérant détruire avec la grande cité jusqu’au souvenir des conquêtes immortelles que depuis un siècle nous achetons de notre sang et que le monde nomme liberté, égalité, fraternité ?... Non, ces ennemis, ces assassins du peuple et de la liberté sont des Français !..."
citation: "Non, ce n’est pas la paix, mais bien la guerre à outrance que les travailleuses de Paris viennent réclamer !"
---

# Appel aux citoyennes de Paris

Paris est bloqué, Paris est bombardé...

Citoyennes, où sont-ils nos enfants, et nos frères et nos maris ?... Entendez-vous le canon qui gronde et le tocsin qui sonne l’appel sacré ?

Aux armes ! La patrie est en danger !...

Est-ce l’étranger qui revient envahir la France ? Sont-ce les légions coalisées des tyrans de l’Europe qui massacrent nos frères, espérant détruire avec la grande cité jusqu’au souvenir des conquêtes immortelles que depuis un siècle nous achetons de notre sang et que le monde nomme liberté, égalité, fraternité ?...

Non, ces ennemis, ces assassins du peuple et de la liberté sont des Français !...

Ce vertige fratricide qui s’empare de la France, ce combat à mort, c’est l’acte final de l’éternel antagonisme du droit et de la force, du travail et de l’exploitation, du peuple et de ses bourreaux !...

Nos ennemis, ce sont les privilégiés de l’ordre social actuel, tous ceux qui toujours ont vécu de nos sueurs, qui toujours se sont engraissés de notre misère...

Ils ont vu le peuple se relever en s’écriant : "Pas de devoirs sans droits, pas de droits sans devoirs !... Nous voulons le travail, mais pour en garder le produit... Plus d’exploiteurs, plus de maîtres !... Le travail est le bien-être pour tous, --- le gouvernement du peuple par lui-même, --- la Commune, vivre libres en travaillant, ou mourir en combattant !..."

Et la crainte de se voir appelés au tribunal du peuple a poussé nos ennemis à commettre le plus grand des forfaits, la guerre civile !

Citoyennes de Paris, descendantes des femmes de la grande Révolution, qui, au nom du peuple et de la justice, marchaient sur Versailles, ramenant captif Louis XVI, nous, mères, femmes et sœurs de ce peuple français, supporterons-nous plus longtemps que la misère et l’ignorance fassent des ennemis de nos enfants, que père contre fils, que frère contre frère, ils viennent s’entre-tuer sous nos yeux pour le caprice de nos oppresseurs, qui veulent l’anéantissement de Paris après l’avoir livré à l’étranger ?

Citoyennes, l’heure décisive est arrivée. Il faut que c’en soit fait du vieux monde ! Nous voulons être libres ! Et ce n’est pas seulement la France qui se lève, tous les peuples civilisés ont les yeux sur Paris, attendant notre triomphe pour à leur tour se délivrer. Cette même Allemagne, --- dont les armées princières dévastaient notre patrie, jurant la mort à ses tendances démocratiques et socialistes, --- est elle-même ébranlée et travaillée par le souffle révolutionnaire ! Aussi, depuis six mois est-elle en état de siège, et ses représentants ouvriers sont au cachot ! La Russie même ne voit périr ses défenseurs de la liberté que pour saluer une génération nouvelle, à son tour prête à combattre et à mourir pour la république et la transformation sociale !

L’Irlande et la Pologne, qui ne meurent que pour renaître avec une énergie nouvelle, --- L’Espagne et l’Italie, qui retrouvent leur vigueur perdue pour se joindre à la lutte internationale des peuples, --- l’Angleterre, dont la masse entière, prolétaire et salariée, devient révolutionnaire par position sociale, --- l’Autriche, dont le gouvernement doit réprimer les révoltes simultanées du pays même et des pouvoirs slaves, --- cet entrechoc perpétuel entre les classes régnantes et le peuple n’indique-t-il pas que l’arbre de la liberté, fécondé par les flots de sang versés durant des siècles a enfin porté ses fruits ?

Citoyennes, le gant est jeté, il faut vaincre ou mourir ! Que les mères, les femmes qui se disent : "Que m’importe le triomphe de notre cause, si je dois perdre ceux que j’aime !" se persuadent enfin que le seul moyen de sauver ceux qui leur sont chers, --- le mari qui la soutient, l’enfant en qui elle met son espoir, --- c’est de prendre une part active à la lutte engagée, pour la faire cesser enfin et à tout jamais, cette lutte fratricide qui ne peut se terminer que par le triomphe du peuple, à moins d’être renouvelée dans un avenir prochain !

Malheur aux mères, si une fois encore le peuple succombait ! Ce seront leurs fils enfants qui paieront cette défaite, car pour nos frères et nos maris, leur tête est jouée, et la réaction aura beau jeu !... De la clémence, ni nous ni nos ennemis nous n’en voulons !...

Citoyennes, toutes résolues, toutes unies, veillons à la sûreté de notre cause ! Préparons-nous à défendre et à venger nos frères ! Aux portes de Paris, sur les barricades, dans les faubourgs, n’importe ! soyons prêtes, au moment donné, à joindre nos efforts aux leurs ; si les infâmes qui fusillent les prisonniers, qui assassinent nos chefs, mitraillent une foule de femmes désarmées, tant mieux ! le cri d’horreur et d’indignation de la France et du monde achèvera ce que nous aurons tenté !... Et si les armes et les baïonnettes sont toutes utilisées par nos frères, il nous restera encore des pavés pour écraser les traîtres !...


{{%epigraphe%}}
Un groupe de citoyennes.
{{%/epigraphe%}}

# Manifeste du Comité central de l’Union des femmes pour la défense de Paris et les soins aux blessés

Au nom de la Révolution sociale que nous acclamons, au nom de la revendication des droits du travail, de l’égalité et de la justice, l’Union des Femmes pour la défense de Paris et les soins aux blessés proteste de toutes ses forces contre l’indigne proclamation aux citoyennes, parue et affichée avant-hier, et émanant d’un groupe anonyme de réactionnaires.

Ladite proclamation porte que les femmes de Paris en appellent à la générosité de Versailles et demandent la paix à tout prix...

La générosité de lâches assassins !

Une conciliation entre la liberté et le despotisme, entre le Peuple et ses bourreaux !

Non, ce n’est pas la paix, mais bien la guerre à outrance que les travailleuses de Paris viennent réclamer !

Aujourd’hui, une conciliation serait une trahison !... Ce serait renier toutes les aspirations ouvrières acclamant la rénovation sociale absolue, l’anéantissement de tous les rapports juridiques et sociaux existant actuellement, la suppression de tous les privilèges, de toutes les exploitations, la substitution du règne du travail à celui du capital, en un mot, l’affranchissement du travailleur par lui-même !...

Six mois de souffrances et de trahison pendant le siège, six semaines de lutte gigantesque contre les exploiteurs coalisés, les flots de sang versés pour la cause de la liberté sont nos titres de gloire et de vengeance !...

La lutte actuelle ne peut avoir pour issue que le triomphe de la cause populaire... Paris ne reculera pas, car il porte le drapeau de l’avenir. L’heure suprême a sonné... place aux travailleurs, arrière à leurs bourreaux !...

Des actes, de l’énergie !...

L’arbre de la liberté croît arrosé par le sang de ses ennemis !...

Toutes unies et résolues, grandies et éclairées par les souffrances que les crises sociales entraînent toujours à leur suite, profondément convaincues que la Commune, représentante des principes internationaux et révolutionnaires des peuples, porte en elle les germes de la révolution sociale, les Femmes de Paris prouveront à la France et au monde qu’elles aussi sauront, au moment du danger suprême, --- aux barricades, sur les remparts de Paris, si la réaction forçait les portes, --- donner comme leurs frères leur sang et leur vie pour la défense et le triomphe de la Commune, c’est-à-dire du Peuple !

Alors, victorieux, à même de s’unir et de s’entendre sur leurs intérêts communs, travailleurs et travailleuses, tous solidaires, par un dernier effort anéantiront à jamais tout vestige d’exploitation et d’exploiteurs !...

VIVE LA RÉPUBLIQUE SOCIALE ET UNIVERSELLE !...

VIVE LE TRAVAIL !...

VIVE LA COMMUNE !...

{{%droite%}}
Paris, le 6 mai 1871.
{{%/droite%}}

{{%alignement-d%}}
*La Commission exécutive du Comité central,*

LE MEL,<br>
JACQUIER,<br>
LEFÈVRE,<br>
LELOUP,<br>
DMITRIEFF.
{{%/alignement-d%}}
