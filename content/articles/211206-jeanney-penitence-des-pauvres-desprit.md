---
persona:
  - C Jeanney
orthopersona:
  - name: C. Jeanney
    link: C Jeanney
title: "Pénitence des pauvres d'esprit"
slug: "penitence-des-pauvres-desprit"
date: 2021-12-06
echo:
  - rythmique
images:
  - "img/211206-jeanney-penitence-des-pauvres-desprit.jpg"
gridimages:
  - "img/211206-jeanney-penitence-des-pauvres-desprit-grid.jpg"
sourceimage: "C. Jeanney"
notes: ""
summary: "Pardon / De n'avoir pas assez / D'esprit / Nous simples / D'esprit / Esprits si mal tournés / Si mal fichus mal embouchés / Pardon / Nous / Venons / Ici / En pénitence / Nous T'implorons / Que Ton nom sonne / Que Ta Fondation / Vienne soutenir nos fondations / Pardon / Égarement / Perdition aveuglement où nous étions / Nous étions dans l'erreur / Guide-nous / Que Ta lumière arrive / Que Tes bracelets jonc en or jaune / Dont les courbes polies interagissent avec la ligne graphique de Tes / initiales créant ainsi une sorte de pulsation rythmée / Nous éclairent"
citation: "nos lames ne sont pas des armes"
---

{{< soundcloud 1172749315 >}}

*Écrite pour être lue à voix haute*

 

Pardon

De n'avoir pas assez

D'esprit

Nous simples

D'esprit

Esprits si mal tournés

Si mal fichus mal embouchés

Pardon

 

Nous

Venons

Ici

En pénitence

 

Nous T'implorons

Que Ton nom sonne

Que Ta Fondation

Vienne soutenir nos fondations

 

Pardon

Égarement

Perdition aveuglement où nous étions

Nous étions dans l'erreur

Guide-nous

Que Ta lumière arrive

Que Tes bracelets jonc en or jaune

Dont les courbes polies interagissent avec la ligne graphique de Tes
initiales créant ainsi une sorte de pulsation rythmée

Nous éclairent

 

Nous sommes pris dans Tes sacs que nous confectionnons sur mesure
habillés de toile autographe boréale contemporaine à garnitures en
cuir de vaches noires

Tu nous poses sur un chariot du Dinosaur Mountain at Gold Canyon Golf
Resort

Tu nous portes à la main ou à l'épaule

 

À Megève Tu apparais

À Saint-Moritz Tu apparais

À Saint-Barth Tu apparais

À Courchevel Tu apparais

Tu apparais à Saint-Jean-Cap-Ferrat

Aux îles Vierges Bahamas Caïman

Caraïbes Casablanca

Luberon Montana Tu multiplies les apparitions

 

Miracle

Nous Te suivons

Ton chemin se déploie en nous

Car

De la confection de la poignée au choix du cuir à la personnalisation à
la frappe au fer c'est un voyage en lui-même qu'offre Ton sac

 

Tu es comme une signature sur nos fronts

Tu es l'Ordre qui repousse le chaos dans l'ordonnancement parfait du
cosmos tel qu'il a été voulu

 

Tes pouvoirs sont tels

Que les parapluies se déploient au-dessus de Ta tête

Que les portes s'ouvrent à Ton approche

Que Tes verres se remplissent seuls et que de la nourriture surgit
soudain et soudain et soudain dans Ton assiette comme dans le conte des
frères Grimm Tu n'as qu'à penser « Petite table sois mise ! » pour
qu'aussitôt sortilège elle se couvre d'une nappe d'où surgissent
assiettes couverts festins

Tes valises se garnissent instantanément avant de Te suivre où que Tu
ailles

Sans Tes mains sans Tes mains sans Tes mains sans Tes mains

Qui restent préservées des germes

Quant à nous

Nous n'avons pas de telles aptitudes

 

Mais sous Ton impulsion

Nos mains imparfaites et calleuses

Taillent

Et

Retaillent

 

Nos lames ne sont pas des armes

Elles sont là pour Te célébrer

Jamais pour Te blesser

 

Nos lames cutters ciseaux couteaux rotatifs couteaux à main à lames
fixes couteau tranchet couteau demi-lune couteau à pied couteaux à parer
refendeuses coupes lacets coupes lanières emboutir forger graver
diamanter laminer limer poinçons découpoirs polissage tréfilage fondre
souder nos mains T'appartiennent depuis la nuit des temps

Depuis le premier arceau de crinoline

Depuis la première couronne blanche en forme de mitre allongée

Depuis la première peau de jaguar du premier roi aztèque

Depuis le premier masque funéraire lapis-lazuli

Depuis la première queue de taureau fixée à l'arrière du premier pagne

 

Nos mains tressent cisèlent

Forgent

Lavent frottent

Servent rangent

Grattent trois étoiles sur le ticket gagnant tentent la chance à la
Grosse Roue payent un forfait téléphonique internet profitent à chaque
instant des meilleurs plans adorent être clients bénéficient d'un débit
jusqu'à un gigabyte par seconde pour recevoir en haute définition Tes
images

 

Images enluminées

Dorure Cristal Lustre montgolfière empire abat-jour plaqué or
vingt-quatre carats

Images de Toi sur fond de Tour Eiffel scintillante

Images de Tes fesses calées dans des fauteuils capitonnés théâtres
opéras subventionnés argent public

Images de Tes bras autour d'un présentoir rehaussé d'un flacon radical
coiffé d'un cabochon facetté eau de parfum twist and spray bouquet
floral aldéhydé sublimé par un réceptacle iconique aux lignes
minimalistes parfum mythique intemporel

Ton bras frôle celui de Rihanna

Ta main serre celle de Donald Trump

 

Tu es amour

Ton amour pour les Hommes pour les Femmes descend sur nous tombe comme
la pluie bienfaisante sur les vastes étendues stériles

 

Aimant les Hommes Tu caresses leur cou de Ton écharpe graphique qui
marie motif à damier classique et initiales oversize à des rayures
remarquables créant un effet saisissant décor adouci par l'élégance de
couleurs discrètes et la rusticité de la bordure à franges à la fois
douce et confortable de cet accessoire original confectionné en pur
cachemire ce qui souligne son esthétique

 

Aimant les Femmes Tu les fais défiler printemps-été pyramide du Louvre
dotées d'un parka à capuche qui ne manque pas d'audace poignets
ajustables taille élastique créant un effet blousant une signature de la
saison sublime la manche d'un détail graphique complété par une tirette
en toile autographe classique et Tu chausses les Femmes d'une pièce
phare du défilé automne-hiver bottine toréador qui revisite la santiag
classique une version en cuir de veau glacé noir sublimée par des
chaînes et des clous dorés et dont la bride en toile autographe à
l'arrière est pourvue d'une boucle qui permet de l'enfiler aisément Tu
aimes les Femmes Tes bottines s'enfilent aisément

Et si ce n'était pas le cas

Tu peux compter sur nous

Tu peux compter sur nos petites mains fripées pour venir en aide

 

Nos mains sont toutes fripées

Car nous n'alignons pas deux idées

Par un cruel manque de logique nous n'utilisons pas Tes crèmes
ingrédients d'exception venus des quatre coins du monde texture
onctueuse pour une sensorialité hors pair nous n'utilisons pas Tes
crèmes aux composants extraordinaires comme le géto secret de jeunesse
venue d'Okinawa développé par un professeur expert en cellules souches
création sur mesure technicité exceptionnelle

 

Quelle incurie

Il faut dire que nous ne savons pas comme Toi implémenter des critères
objectifs de sélection à toutes les étapes de nos processus pardon

 

Nous n'avons pas le sens commun

Pauvres d'esprit esprits chafouins esprits inconséquents

Aussi Conduis-nous

Sur l'océan démonté

Montre-nous la voie de la Réussite

Tu évites les récifs les écueils de la négativité Tu rebondis séance
historique montée fulgurante des bénéfices Tu positives c'est la clé du
succès Tu sais t'ouvrir à d'autres viviers et canaux de sourcing

Nous arriverons à bon port

Grâce à Toi

Notre Captain

Ô Captain

 

Conduis-nous sur Ton navire grand comme le Yacht Octopus cent-vingt-six
mètres de long quarante-et-une suites une piscine trois hélicoptères une
salle de cinéma un terrain de basket un studio d'enregistrement des
motos-marines un sous-marin de poche

Conduis-nous sur Ton navire grand comme le Motor Yacht-A quatre-cents
millions de dollars trois piscines dont une avec un sol en verre

Conduis-nous sur Ton navire grand comme le Rising Sun cent trente-huit
mètres de long estimé à cinq cent quatre-vingt-dix millions de dollars
grand aussi grand que celui actuellement en construction qui se verra
aménagé d'une discothèque de douze jacuzzis robinets de douche à
dix-huit carats

 

Au large des Seychelles

Ta musique s'entend depuis la côte

 

Entends à Ton tour notre liturgie des heures

Minuit trois heures du matin cinq heures du matin sept heures du matin

Nos prières vers Toi commencent la nuit

Nous Te prions en branchant l'aspirateur dans le bureau vide

Nous Te prions dans l'huile de moteur de l'atelier

Dix heures midi quatorze heures seize heures dix-neuf heures

Nous Te prions à chaque pomme pourrie ôtée du tapis roulant

Nous Te prions à chaque tête de poisson coupée

Nous Te prions à chaque boyau éviscéré

Nous Te prions au rythme du goutte-à-goutte qui suinte du plafond de la
chambre d'hôpital

Nous Te prions en équilibre sur chaque planche d'échafaudage

Et le soir tandis que Tom Cruise sauve le monde nous Te prions en
entrant doucement dans le sommeil

 

Ronde des heures

Signe de l'infini qui travaille à la chaîne chaîne étant synonyme de
chapelet

Nous Te prions et répétons nos prières comme nous répétons nos gestes
comme nous répétons nos demandes de pardon c'est malgré nous que nos
poumons respirent difficilement pardon

 

Nous qui n'entendons rien

Aux termes de déstockage et marge intéressante

Nous qui ne comprenons rien aux solutions à valeur ajoutée

 

Brise notre zone de confort

Nous sommes

Si confortables

Avec nos mains tachées de fioul

Et notre dos trempé

 

Nous Te prions dans chaque agio de chaque compte débiteur où nous devons
régulariser notre situation très rapidement faute de quoi

Ô Reçois notre obole

Nous Te prions et Te remercions pour chaque compte passif et capital
propre que Tu optimises et rentabilises sur le marché par des actions
investissements

 

Pardon

Nous ne sommes pas à Ta hauteur

S'il te plaît Implémente nos process

Nous ne sommes que petitesse étroite et nos pantalons nous boudinent car
nous ne mangeons pas équilibré nos chips huilées hydrogénées oignon en
poudre maltodextrine émulsifiant E471 saturent notre chair et nos veines

Nous ne savons pas manger sainement comme Toi

Avant-plat

Foie gras des trois Empereurs

Caviar osciètre de Sologne

Sole Cardinal

Caneton Challandais maturé

Poire Charpini

Nous ne sommes que maigreurs pâleurs et surcharges pondérales

 

Tu es si beau

Tu es si beau grâce à ta nutrition parfaite toutes les vitamines
déboulant instantanément au coeur de Ton organisme

 

Montre-nous comme Tu Te régales

Quand Tu Te régales Tu nous régales

Tu nous flunches Tu nous ubereates

Remplis nos yeux

 

Tu es si beau grâce à Ton sommeil réparateur matelas de coton et crin de
cheval où du lin placé entre chaque ressort prévient des frottements
matelas accessoirement ourlé estampillé d'une plaque d'or numérotée prix
à six chiffres

 

Tu es si beau

Tes narines ne coulent jamais

Ton front n'est jamais rouge

Ni transpirant

Ta température corporelle est étrangère

Aux contrastes entre dedans et dehors

Puisque

Pour Toi

Tout est dehors

Tout est dedans

 

Que Tu sois en Arctique au North Pole Igloos Hotel
quatre-vingt-quinze-mille euros la nuit

Ou au centre du désert émirati dans une luxueuse suite bâtie sur le
modèle d'authentiques campements bédouins

Tu Te

Stabilises

À vingt degrés

 

Tes oreilles ourlées délicatement

Repoussent grincements stridulations alarmes jingles chuintements coups
de boutoir souffles d'air chaud chocs bocaux palettes fenwicks grilles
crissements pilonnages rouages d'emballages sirènes palans et
bétonnières

 

Tu sens si bon

Laisse-nous Te respirer

Tes cheveux sentent bon

Ta peau sent bon

Ton élégant costume associant détails traditionnels et tissus nobles
pour en souligner la modernité et présenter un revers miroir et des
poches passepoilées sent bon

Ta chemise crêpe de Chine cent pour cent soie bleu ciel col en pointe
boutons recouverts sent bon

Tes chauffeurs sentent bon

Tes jardiniers sentent bon

Tes cuisiniers sentent bon

Tes hélicoptères sentent bon

Tes salles de bain sentent bon et Tes toilettes sentent bon malgré leur
nombre car il T'est mathématiquement impossible en vingt-quatre heures
de faire pipi dans chaque WC que Tu possèdes Ô père de tendresse et de
miséricorde Pardonne-nous notre urine grégaire

 

Les effluves qui frôlent Tes narines ne peuvent être que délicieux

Tu échappes à toutes les émanations nauséabondes Gloire à Toi

Tu ne transpires pas mais Ton message en nous transpire

 

Tu nous conduis par la main comme un Père

Pater noster

C'est avec joie que nous signons tes clauses

Clauses de non-concurrence clauses d'exclusivité

Ne T'en va pas

 

Reste avec nous

Ne T'en va pas

Résiste à toutes ces procédures lourdeurs administratives droit du
travail congés et absences pour enfants malades hausses des salaires
grèves protections de la santé réaménagements des lieux de production
prélèvements obligatoires libertés syndicales réparations financières de
préjudices sanctions pénales et/ou administratives tout ce qui freine Ta
créativité

 

Résiste à la conjoncture

Ne Cesse pas d'implanter développer amplifier véhiculer des perspectives

Ne nous Quitte pas

Nous Te parerons d'Or et de Lumières venues de pays où il ne pleut pas

 

Si Tu partais que ferions-nous

Si Tu partais nos petites mains deviendraient molles d'inaction il ne
nous resterait que nos doigts à manger dans le rien sidéral de nos
frigidaires dans le rien carcéral de nos bouges dans nos entrepôts
boueux où s'alignent rangées de cannettes de bière et de sodas
successions de nutriscore D et tranches de pain de mie à dates de
péremption rapides ou périmées

Ne Pars pas

 

Ne Pars pas en Belgique Australie Portugal États-Unis Grande-Bretagne
Canada Ne nous Abandonne pas pour une propriété située sur un versant
sud-ouest ensoleillé du Ricken un magnifique complexe qui exprime ses
espaces avec une intensité innovante dans le respect de l'extraordinaire
paysage environnant sur environ mille-quarante-cinq mètres carrés un
plain-pied privatisant tout le dixième étage de cet immeuble avec
concierge et vue exceptionnelle un pavillon sa grotte en pierre
naturelle et le ruisseau à proximité promettant paix et détente un
atrium de neuf mètres de haut décoré d'une balustrade forgée à la main
et inondé de lumière qui passe à travers la verrière d'environ soixante
mètres carrés et tombe sur le revêtement de sol un marbre égyptien
taillé localement ainsi que sur la surface du mur le stuc Veneziano
distribuant une luminosité incroyable lui conférant une légèreté à
l'ouest d'une salle de fitness menant à l'aile bien-être nouvellement
construite équipée de trois saunas cabine infrarouge sauna finlandais ou
bio et bain de vapeur d'un spa de nage de cinq mètres de long avec
système à contre-courant d'un espace détente et d'une salle de bain avec
douches et toilettes d'une piscine extérieure d'une terrasse d'environ
quatre-cents mètres carrés avec une piscine d'eau salée de douze mètres
sur six jets de massage et couverture solaire notons qu'une fontaine
avec une source artésienne et un bain à remous avec hydrothérapie
complètent l'espace bien-être

Pardon

Notre inconscience et notre peu de discernement nous font habiter nos
voitures

 

Tu nous donnes de quoi nous lever le matin

Tu réponds au pourquoi se lever le matin

 

Ne nous Abandonne pas

Remplis nos attentes nos caddies nos gosiers

Remplis nos gobelets en plastique jetables

Nous ne sommes pas comme Toi écoresponsables

Toi qui trempes Tes lèvres dans une flûte à champagne livrée dans son
écrin pourvue d'un certificat d'authenticité verre légendaire habitué de
toutes les soirées d'exception

Alors que nous

Pauvres d'esprit

Esprits chagrins

Nous buvons dans du polymère

 

Toi qui connais la valeur

De l'inanimé et de l'animé

Tu sais réveiller en nous ce qui est sain

Et ce qui est sain est compétitif

 

Que Tes paroles soient sanctifiées programme de fidélité disponible sur
application

Au futur au présent au passé

 

Raconte-nous Ton Histoire Ta Story Ta chanson de geste Ta Légende

Que nous puissions la recopier dans les manuels les cahiers d'écriture
écoles collèges lycées et toutes sortes d'établissements scolaires où de
nombreuses conventions sont signées avec les acteurs entreprises
branches associations pour mettre en œuvre des actions de découverte du
monde économique Ta générosité sponsorise nos écoles

Apprenons à nos enfants à lire et écrire grâce à Tes partenaires
préférentiels

Les mots invariables Moët & Chandon la table de sept Krug le complément
d'objet direct Veuve Clicquot le gérondif Hennessy le Château d'Yquem
plus-que-parfait soutien scolaire en sus dans Les Échos Le Parisien
Connaissance des Arts usages du pronom relatif en partenariat avec les
Maisons Cheval Blanc qui invitent à vivre de précieux moments en famille
ou en tribu restauration rapide de la voix active et de la voix passive
conjugaison de crèmes glacées grammaire de magazines défricheurs de
tendances spécialisés en régime brûle-graisse cent pour cent ananas

 

Partage avec nous Ton récit perpétuel

Fais-nous entrevoir

Le meilleur des mondes qui soit

Le meilleur d'entre tous les mondes

Celui où Tu existes

Celui que Tu écris chapitre après chapitre

Toi l'hôte invisible

Tu es au commencement

 

Tu seras là demain

Le futur T'appartient

Tu testes entrepreneurial la faisabilité technique et commerciale du
tourisme spatial de masse six cents clients ont déjà acheté des billets
à hauteur d'au moins deux-cent-mille dollars chacun Tu fais des
repérages d'engins en orbite dotés d'un écran pixelisé sur lequel seront
projetées des publicités et depuis la surface basse de la terre nous
lèverons les yeux pour recevoir d'entre les étoiles l'image de Ton
canapé nommé carrancas d'après l'artisanat traditionnel brésilien pour
la collection objets saltimbanques un régal de lignes voluptueuses et
arrondies coque doublée d'une peau de veau souple accueillant huit
coussins moelleux qui s'emboîtent comme un puzzle aussi beau qu'une
sculpture abstraite aussi confortable qu'un nuage découvrir inventer
repousser les limites

 

En Toi nous croyons

En Toi et en Ta Fondation

Foundation

Qui ancre

Son engagement

Dans la création actuelle

 

HORAIRES & TARIFS

PLAN & ACCÈS

PRÉPAREZ VOTRE VISITE

 

Merci à Toi

Animée par une mission d'intérêt général Ta Fondation Foundation
s'engage à rendre l'art et la culture ouverts à tous Ô numéro un mondial
du luxe avec un portefeuille de soixante-dix marques de prestige

 

Merci à Toi

Ta Fondation Foundation explore méthodes & expérimentations & créations
de surfaces reliant ainsi rencontres organiques et situations actuelles
dans le contexte urgent du discours écologique Ô racheteur d'une filiale
de trente-quatre palaces sept trains et deux croisières fluviales

 

Merci à Toi

De mettre en lumière dans Ta Fondation Foundation les images
emblématiques de la guerre du Vietnam et de l'enfant du ghetto de
Varsovie bras en croix telle une figure christique sa nudité son cri
concourant à la puissance de l'image et affirmant leur appartenance à la
mémoire collective billet famille à trente-deux euros disponible et
valable pour deux adultes et jusqu'à quatre enfants de moins de dix-huit
ans heures d'ouverture le mardi dix heures dix-neuf heures

 

Merci à Toi

Du clin d'œil amusé que Ta Fondation Foundation lance à la société de
consommation visage et corps composés de deux piles superposées de
cartons d'emballages dont les logos des marques sont apparents 
simplement compactés et ficelés avec des fils de fer pour maintenir une
fragilité qui semble contraster avec les six pieds identiques ancrés au
sol dont la multiplicité crée un trouble au cours des visites exclusives
en compagnie des commissaires d'exposition devenez membres privilégiés
pour cent quatre-vingt euros

 

Merci à Toi

De montrer dans Ta Fondation Foundation une suite d'œuvres subtiles et
poétiques rassemblant des dictons misogynes  maladroitement  brodés qui
dévoilent ainsi de caustiques points de vue la cause des Femmes Te
remercie pour cela ainsi que de la livraison dès ce soir ou demain de
Ton iris magnétique pour un jeu amoureux entre légèreté et sensualité

 

Merci à Toi

De dévoiler dans Ta Fondation Foundation une série emblématique de près
de trois-cents portraits de femmes capturées à différents moments de
leur vie photographiées selon le même principe en buste de face ou de
trois-quarts cadrées à distance égale en intérieur ou en extérieur en
noir et blanc sans artifice ni décor ni costume merci de révéler leur
présence et de leur offrir la possibilité de s'affirmer dans leur
différence et leur singularité merci car au-delà du documentaire social
ces images s'imposent dans un face à face d'une rare intensité qui n'a
d'égal que l'intensité de Ton parfum que tisse l'iris pallida de
Florence fleur précieuse emblématique de la séduction féminine

 

Merci à Toi

Qui mets l'accent avec Ta Fondation Foundation sur l'ambition sociale le
désir d'amour et la difficulté somme toute humaine à le trouver sans se
soustraire à la critique implicite des contraintes subies par les Femmes
Femmes par ailleurs reconnaissantes de pouvoir compter dans leur
garde-robe Ton collier cardinal accessoire incontournable bijou
intemporel et facile à porter du fait de ses proportions féminines

 

Tu nous ouvres Ta Fondation Foundation

Tu nous ouvres à la

Culture

 

Tant de mots nous échappent tant de mots dont nous ne comprenons ni le
sens ni les effets le mot mécénat par exemple dont nous ignorons qu'il
découle de Gaius Maecenas protecteur des belles-lettres une
méconnaissance nous plaçant dans l'incapacité déplorable de mécéner ceci
nous privant d'avantages nombreux comme une déductibilité de cent pour
cent du prix d'une œuvre acquise pauvres de nous qui allons ici et là
sans rien savoir sans rien savoir

 

Nous avons faim de tout comprendre

Nous avons faim de T'écouter

 

Nous voulons Te rencontrer

Rencontre exclusive

 

STUPÉFIANTE

 

Voir Ton image

Entendre Ta voix

On Te tend le micro journalistique d'une station de radio généraliste
nationale publique

On Te pose la question : Est-ce que Votre Fondation Foundation sauvera
le monde de l'Art ?

On avait envie d'en discuter avec Toi

Tu as accepté

 

On rappelle que Tu as posé un vaisseau spatial aux portes de la capitale

Que Tu as déployé pour l'inauguration des stars de la mode de l'art et
de la politique avec les honneurs de la République

 

On qualifie Ta Fondation Foundation de « morceau d'humanité qui montre à
tous que le rêve peut à force de génie et de volonté devenir réalité »

On brosse rapidement Ton portrait

Images d'archives en noir & blanc

Toi jeune

Faisant de l'équitation

Buvant du thé dans de la porcelaine de Fürstenberg

Puis Toi

Aujourd'hui

À la tête d'un des plus grands musées au monde

 

Tu dis :

--- L'art est un sujet qui me passionne je peux en parler sans risquer
les critiques parce que vous savez dès que quelque chose réussit il y a
des critiques dans tous les sens et mon objet n'est pas de répondre aux
critiques je ne suis pas toujours critiqué mais c'est le lot des hommes
d'affaires d'incarner quelquefois de manière totalement injustifiée des
critiques d'ambiance puisque la mentalité est un peu anti-entreprise et
quand en plus une entreprise réussit qu'elle se développe mondialement
et qu'elle embauche du personnel ça c'est pas normal c'est contraire à
la logique ambiante de l'esprit comment dirais-je socialo-marxiste qui
prédomine dans l'opinion publique d'un certain nombre de commentateurs
il y a cette connotation donc on est critiqué enfin tout ça n'a rien à
voir avec l'art quand j'ai commencé à m'intéresser à l'art j'ai eu la
chance de voir des expositions il y avait des artistes à cette époque-là
qui n'étaient pas encore très connus qui étaient très critiqués et qui
donc ne valaient pas grand-chose et j'ai eu la chance d'en acheter pas
mal ce qui fait qu'aujourd'hui j'ai une collection obtenue à des
conditions très intéressantes dans ces années-là --- Celui-là il coûtait
combien ?

--- Ah ça je ne m'en rappelle plus aujourd'hui ça vaut des millions dans
mon bureau il y a ce Basquiat deux œuvres de Giacometti un Mondrian et
Yves Klein bon vous avez des gens qui sont imperméables à l'art à la
musique par exemple Victor Hugo n'aimait pas la musique l'art qui me
touche le moins est le cinéma il est intéressant mais il prend beaucoup
de temps je préfère jouer au tennis que d'aller au cinéma

 

--- Vous avez tenu à avoir un piano près de votre bureau ?

--- Oui cet instrument est très important à mes yeux très important

--- On a le droit de vous demander de nous jouer deux notes comme ça
impromptues ?

--- Je ne sais pas s'il est accordé

 

Tu joues *La Marche Turque* Tu rates un peu les touches et Finis version
free jazz pour le masquer intelligemment

Tu dis : Ça m'arrive de jouer parfois encore une fois je serais trop
critiqué dans ce pays mais à l'étranger ça m'arrive de faire de petits
concerts

 

Tu dis : J'aime Kanye West

Tu dis : J'aime son côté artiste unique il a fait quatre soirs de suite
à la Fondation c'était rempli

 

Tu dis : Je ne suis pas vacciné contre la sensibilité dans un métier où
la créativité est primordiale il faut être sensible

 

Tu précises : Je ne suis pas déconnecté de la réalité

Tu dis : Je suis en contact direct avec le monde réel hier soir par
exemple j'ai fêté les Catherinettes chez Dior il y avait
mille-cinq-cents personnes on a dîné ensemble et je leur ai parlé je les
ai vues et j'étais en contact direct

 

Tu dis : *Le déjeuner sur l'herbe* ça c'est une version magnifique
regardez les détails

Tu dis : Gauguin sa période Tahiti est la plus forte

Tu dis : Picasso quelle puissance la période bleue

 

Gloire à Toi

Toi qui sais lier matériel & esthétisme

Nous ne sommes que gratitude

Nous croyons en Toi et en tous Tes apôtres winners winners tous des
winners

 

Lorsqu'une balle entre dans un filet la joie nous submerge car c'est Ta
parole qui la guide

Lorsqu'une balle entre dans le sixième trou la joie nous submerge comme
la félicité abreuve l'oiseau birdie condor eagle albatros

Lorsqu'une balle entre dans le panier la joie nous dunke Tu nous dunkes

 

Lorsqu'une voiture gagne la course

ou un cheval un lévrier un athlète un cycliste un motard lorsqu'un
pilote gagne la course c'est l'ordre du mérite qui triomphe l'ordre du
meilleur winner

Ton ordre du mérite

Ton ordre du mérite gagne et surfe et remporte le ruban doré la médaille
1^er^ choix le trophée innovation challenge

Ton ordre du mérite pilote le jet-ski fend la surface des eaux d'une
réserve marine protégée

Ton ordre du mérite dépose son équipement d'alpiniste emballages
alimentaires et sacs plastiques en particulier tentes cordes bouteilles
d'oxygène excréments au sommet de l'Everest

 

Vive la Victoire des winners

Vive la Victoire fille du fight et vive Toi qui l'inspires

Ta vision saine du challenge pari avec soi-même nous améliore

En nous mettant en concurrence entre nous Tu nous mets en concurrence
avec nous-mêmes Sois béni entre toutes choses non égales par ailleurs

 

Parce que Tu aimes la compétition

Nous aimons la compétition

Sous toutes ses formes

Sur le bandeau rouge de la jaquette du livre primé

Sur le disque récompense de platine

Dans les records atteints chez Sotheby's

La compétition est désirable

 

Tu dis : Toute entreprise si elle veut rester dans le trio de tête doit
améliorer à long terme sa désidérabilité aura-t-on une désidérabilité
plus forte dans dix ans voilà mon objectif toujours penser à long terme

Tu dis : Et pour cela il faut être plus rapide plus efficace en matière
d'emplacements de magasins et d'achats d'espaces publicitaires il faut
ouvrir un large spectre d'actions

 

Tu dis : Mon entreprise est sans doute parmi toutes la plus proche de la
nature avec ses vignobles et ses ateliers la nature j'y suis très
attaché

 

Tu dis *une* antidote et nous croyons en Toi

Lorsque nous disons *un* antidote c'est nous qui sommes en faute

Tu dis *un* icône et là aussi Tu as raison

Nous sommes bêtes et ce que nous voulons sans Ton éclairage n'a aucun
sens

 

Gloire à Toi et à Ton récapitulatif boursier

En charge des dossiers d'acquisition

Tu gères des phases d'intégration comme personne

 

Nous pauvres idiots

Avec notre sac à dos déglingué notre chien sans race notre étui à violon

Nous ne comprenons rien

Pardon pardon pardon pour notre ignorance

 

Le fauteuil sur lequel Tu t'assieds est d'une beauté que nous ne
captions pas il est certain que sans faire preuve d'aucun discernement
nous l'aurions revendu pour l'échanger contre deux ans de loyer

 

Le tableau accroché dans Ton bureau parce que nous n'avons aucune idée
de rien notre indigence mentale étant démesurée nous l'aurions échangé
dans notre aveuglement contre cent machines à laver ajoutées au salaire
d'une vie ou de plusieurs

 

Pardon nous sommes si bêtes que nous parlons de diamants de sang trafics
qui fournissent en armes et en munitions les groupes armés qui
exploitent les mines mines de diamants de sang travail d'enfants mines
de diamants de sang Angola Liberia Côte d'Ivoire milices République du
Congo pardon nous parlons de diamants de sang sans savoir de quoi nous
parlons nous n'y connaissons rien et n'avons pas le temps d'en apprendre
beaucoup sur le sujet car la piscine requiert notre attention doit être
nettoyée il faut y passer l'épuisette récurer les feuilles mortes qui
bouchent le siphon -- mais savoir qu'un pavage de plus de cent diamants
s'illustre par son éclat et que la chaîne peut être portée à trois
différentes longueurs ou doublée afin de créer un ras-de-cou nous
réconforte

Nous sommes si bêtes que nous parlons de peaux de bêtes sauvages
trucidées sans savoir de quoi nous parlons nous n'y connaissons rien en
sciences de la biodiversité et en préservation des espèces et puis nous
n'avons de temps à consacrer à ce domaine il y a la rampe d'escalier
d'architecte un peu acrobatique à nettoyer et il faut vérifier si les
poulies qui permettent d'accéder à la baie vitrée panoramique constellée
de cadavres d'insectes fonctionnent -- heureusement que les vestes en
fourrure de renard naturelle offrent une version luxueuse d'un modèle
classique se distinguant par un intarsia en vison rasé qui réinvente les
codes de la veste en denim l'intérieur révélant une doublure en tissage
autographe constellation détails en nubuck cuir provenant de la peau de
vaches qui étaient en fin de vie quel soulagement

 

Nous sommes si bêtes que dans notre malveillance improductive nous nous
infiltrons dans Ton défilé qui aime les Femmes avec des draps tendus où
nous avons écrit ce qui sort en éructations disgracieuses de nos
cervelles comme Climate is a Fashion victim No Fashion on a dead planet
surconsumption = extinction nous n'avons pas le sens commun

 

Si bêtes que nous bloquons des routes avec des globes de papier mâché
géants tagués There is no Planet B mais quels enfantillages

 

Si bêtes que nous occupons le tarmac d'un aéroport en phase de
modernisation une dizaine de projets d'extensions d'aéroports étant
prévus malgré la menace climatique heureusement qu'un article créant un
nouveau délit d'intrusion sur les pistes d'aéroport a été inséré dans la
Loi nous sommes si bêtes

 

Nous sommes si bêtes si peu esthètes que nous ne savons pas apprécier
les somptueuses couleurs turquoises de la plage de Rosignano sur la côte
toscane couleurs superbes dont l'origine se trouve dans les
deux-cent-cinquante-mille tonnes de déchets issus de la production de
l'usine de carbonate de soude située juste derrière les dunes notre
bêtise est crasse

 

Nous sommes bêtes à pleurer

Si bêtes que nos dents se déchaussent et que nos cabinets dentaires low
cost nous arrachent celles qui auraient encore pu servir

 

Si bêtes que nos bébés naissent sur le bord des routes nos maternités se
trouvant éloignées de nos ventres en gestation quelle idiotie

Si bêtes que nos troubles musculo-squelettiques se traduisent
principalement par des douleurs et des gênes fonctionnelles plus ou
moins importantes le plus souvent quotidiennes provoquées ou aggravées
par le travail

 

Nous sommes si bêtes que le stress au travail phénomène fréquent
entraîne régulièrement chez nous des troubles comme les insomnies ou les
difficultés d'endormissement avec à la longue des conséquences sur notre
santé prise ou perte de poids fatigue difficultés de concentration
hypertension artérielle facteurs de risque pour certaines maladies
cardiovasculaires qui peuvent favoriser les AVC

 

Nous sommes si bêtes que nous attrapons le cancer dans les secteurs de
l'industrie du bois de la métallurgie de la chimie de la plasturgie du
bâtiment travaux publics activités minières activités de maintenance
nettoyage dépannage travail de désinfection en milieu hospitalier ou
dans l'agroalimentaire travail dans un laboratoire de recherche
notamment en cas d'exposition à la radioactivité

 

Nous sommes si bêtes que notre peau se couvre de dermatoses ce qui est
courant dans les professions manuelles métiers du nettoyage travaux de
peinture coiffure soins infirmiers métallurgie imprimerie élevage
agriculture

 

Nous sommes si bêtes que des agents infectieux nous intoxiquent plus
particulièrement dans certains secteurs santé services vétérinaires
gestion des eaux usées et laboratoires certains agents biologiques
pouvant également nuire à l'enfant à naître

 

Nous sommes si bêtes que dans un grand mélange infécond discutable une
sorte de bouillabaisse d'amalgames insensés nous décrétons que la
domination sous toutes ses formes est capable et coupable de viols de
violences aux plus faibles coupable et capable d'infliger la mort aux
corps laminés et exploités si bêtes si bêtes alors que pour vivre mieux
il suffirait de vivre comme Toi il nous suffirait de vivre selon Tes
principes

 

Pardonne-nous notre

Incapacité

À

Suivre

Ton exemple

 

Incapables incapables

Incapables que nous sommes

D'hériter

De boire du thé dans de la porcelaine de Fürstenberg

De nous faire baptiser par un Cardinal intègre qui nous mettra en garde
contre le mariage des homosexuels et sa quantité de conséquences
innombrables parce qu'après ils vont vouloir faire des couples à trois
ou à quatre et après un jour peut-être je ne sais pas l'interdiction de
l'inceste tombera c'est ce qui arrive quand les repères majeurs
s'effondrent

 

Incapables que nous sommes de chasser au son des cors l'écume aux lèvres
des chevaux meutes de chiens courants rouge des livrées cavaleries entre
les arbres de Sologne

 

Incapables que nous sommes de soigner nos vignobles bouquet floral
fruité boisé bel équilibre au cœur d'un château rénové avec beaucoup de
goût élégance et modernité étant les maîtres mots dans cette propriété
de moines négociants maires familles d'industriels armateurs consortium
groupes

 

Incapables d'acheter les dessins d'un artiste en amont de sa notoriété
et de conclure *J'ai eu de la chance*

D'acquérir des hôtels patrimoines domaines vallonnés villas romaines
manoirs architecture baroque contemporaine imposante citadelle
bénéficiant d'une vue panoramique alors qu'il suffisait de passer par
hasard devant en ajoutant *C'est un achat coup de cœur*

 

Incapables nous sommes incapables de savourer comme Tu la savoures cette
chimie extraordinaire du hasard des coups de cœur du hasard de la chance
du hasard des rencontres Deauville Biarritz Le Cap Ferret

 

Tu dis :

Il faut vouloir pour pouvoir

Nous voulons nous voulons nous voulons

 

Tu dis : c'est important d'incarner des valeurs aujourd'hui il faut voir
que derrière les produits magnifiques que l'on offre il y a plus il y a
autre chose

Tu dis : Vivre en contact avec des artistes c'est toucher l'Éternel car
l'art est *une* antidote à l'éphémère

 

Cette quête de sens qui est la Tienne

Est aussi la nôtre

Il nous arrive à nous aussi d'éprouver sur notre temps libre les
tourments inhérents à la brièveté de la vie

 

Quand cela nous arrive

Pardon

Nous rêvons

 

Pardon

Nous perdons notre temps

Et Ton temps

À rêvasser

Au lieu de découper des têtes de poisson

Au lieu d'évider des boyaux

Entre deux offres soumises à condition

Dans la mesure des places disponibles

 

La vie est courte et nous rêvons

Nous rêvons de n'importe quoi

 

Nous rêvons de pisser dans Tes toilettes

Toutes

De découper Tes chemises pour en faire des ponchos des cerfs-volants

De voir Tes parapluies sans mains se retourner laisser la pluie tomber
sur Tes épaules et enfoncer leurs pointes dans Tes bottines

 

Nous rêvons d'installer à l'aide d'une foreuse et par des crochets de
métal Tes tableaux dans les rues

De convertir Tes porcelaines pilées en mosaïque

D'en tapisser les murs et les plafonds pour colmater

Pardon de se représenter que des bulles de lave remontent des jacuzzis
en éclatant

Pardon de réaffecter Tes diamants à la découpe du verre nous avons tant
besoin de vitres et de fenêtres

De répandre dans l'air Tes parfums pour masquer l'odeur des fumées par
leur sublime technicité

 

Pardon de chercher le moyen de recycler Tes cors de chasse en pommeaux
de douche 

 

Et puis Tes sacs

Pardon d'avoir cette pulsion de les fixer solidement à nos porte-bagages
dans l'optique empirique de les remplir de pommes de terre et de savon

 

Pardon d'avoir envie de prendre Ton piano

Nous possédons quelques talents sonores

Nous en jouerions

Les mains tachées de fioul le dos trempé

Nous simples

D'esprit

Esprits si mal tournés

Si mal fichus mal embouchés

Nous danserions sur son couvercle

Sac à dos déglingué chien sans race et étui à violon

Nous danserions

 

Pardon d'élucubrer ces rêves en guise de pénitence

Tu dis ce ne sont pas des rêves

Pardon

Le sens des mots n'est pas notre point fort

Tu dis ce sont des cauchemars

Nous sommes incorrigibles et notre vocabulaire est comme nous

Indécrottable

 

Tu dis que

« Le rêve à force de génie et de

Volonté

Peut devenir réalité »

 

La volonté nous en avons

Nous les pauvres écervelés

Simples d'esprit si mal tournés

On ne peut pas nous l'enlever

Nous voulons nous voulons nous voulons nous voulons
