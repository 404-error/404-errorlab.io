---
persona:
  - Bernard Bourrit
title: "Zibold"
slug: "zibold"
date: 2025-01-16
echo:
  - rythmique
images:
  - "img/250101-bourrit-zibold.jpg"
gridimages:
  - "img/250101-bourrit-zibold-grid.jpg"
sourceimage: "Bernard Bourrit : https://commons.wikimedia.org/wiki/File:Takako_Kusumoto.jpg"
notes: "L'image animée a été découverte [au milieu des Internets](https://www.youtube.com/shorts/DO-NcnWWmZ4)."
summary: "au délié d'un lacet conduisant dans la montagne vers les hauteurs vides et rosies par les rayons rasants du couchant, mais au pied, surtout, d'une ligne de pente abrupte dessinée par l'explosion violente d'un cône comme ceux que parsèment en réduction, de-ci, de-là, dans la campagne, au milieu des caissons que trace la culture du riz en prairies inondables, mais là, sèches, jaunies comme le crin à l'extrémité de la queue touffue du buffle qui ne laboure pas la terre, puisque des buffles, il n'y en pas, la terre se retournant à la main, et le couvert forestier sur les cônes sans fumeroles qui dans le décor font de drôles de pipes, disons qu'il (le couvert) est hirsute, mal peigné, un enchevêtrement de lianes pendues ou serpentant à travers un ramas de branches extrêmement fourchues, aussi finement ramifiées et polies qu'une arche de gorgone suspendue à l'aplomb de la fosse de Nankai"
citation: "bute la langue sur le nom de l'être"
poetry: false
hangindent: false
---

{{%epigraphe%}}

"... cet assemblage mélancolique de choses venues de loin, dégagées
de leur milieu. Le vieux Siebold lui-même avait l'air d'en faire
partie."\
--- *Alphonse Daudet*.

{{%/epigraphe%}}

*Zibold* --- puisque c'est ainsi qu'on doit prononcer

au délié d'un lacet conduisant dans la montagne vers les hauteurs vides
et rosies par les rayons rasants du couchant, mais au pied, surtout,
d'une ligne de pente abrupte dessinée par l'explosion violente d'un cône
comme ceux que parsèment en réduction, de-ci, de-là, dans la campagne,
au milieu des caissons que trace la culture du riz en prairies
inondables, mais là, sèches, jaunies comme le crin à l'extrémité de la
queue touffue du buffle qui ne laboure pas la terre, puisque des
buffles, il n'y en pas, la terre se retournant à la main, et le couvert
forestier sur les cônes sans fumeroles qui dans le décor font de drôles
de pipes, disons qu'il (le couvert) est hirsute, mal peigné, un
enchevêtrement de lianes pendues ou serpentant à travers un ramas de
branches extrêmement fourchues, aussi finement ramifiées et polies
qu'une arche de gorgone suspendue à l'aplomb de la fosse de Nankai,
d'une blancheur squelettique, on n'a pas idée, au délié du lacet, tandis
que le chemin d'ombre sinue entre deux tranchées de bambous
prodigieusement élancés, car la lumière baisse et rapidement, tandis
qu'au délié d'un lacet s'ouvre une porte qu'annonce d'abord une volée de
marches, la porte est rouge, abstraitement tracée par deux poteaux
surmontés de deux linteaux incurvés, abstraite, la porte, déchirante si
tu veux, crevant le continuum halluciné de la réalité comme ferait le
terrier où bascule et disparaît Alice, et toujours, je m'en blâme, le
connu, l'analogue vient fatiguer le mystère, alors, cette porte, n'étant
rien moins que le terrier d'Alice, ouvre la lisière, traverse la
doublure, et inversant le vie en rêve, te conduit au cœur hirsute de la
forêt, les rayons ont chu, le souffle des vents s'est tu, les bruits se
sont estompés, tout à coup l'acuité, la conscience, comme des griffes,
les sens se rétractent, la respiration s'abrège, quelques dalles dictent
le rythme du pas jusqu'au centre de l'éclaircie, les dalles mènent à une
sorte de clairière au centre de laquelle existe cet être inconcevable
qui se tient à la même place et qui semble le centre décentré de
l'univers, car assurément c'en est le pivot, la bonde ou la clé, il est
là, le *sugi* --- on ne sait même pas ce que c'est qu'un *sugi* : un
cèdre ? un cryptoméria ? bute la langue sur le nom de l'être qui se
tient au centre de l'éclaircie où mène la porte rouge qui ouvre la
lisère au cœur du couvert impénétrable fécondé par la violence des cônes
éruptifs plongeant leurs racines jusqu'au fond de la fosse de Nankai et
devant lui, devant le *sugi* qui tient le monde sur son axe depuis trois
millénaires, trois fois mille, comme l'Histoire, et qu'on a ceint d'une
corde épaisse et alanguie formant un nœud lâche autour de sa force, sa
vitalité peut-être, dont on a, dans un geste évident de révérence,
comblé, soutenu par des coulures d'or, les cassures, comme une pauvre
pièce de céramique, devant cet être qui respire et pense depuis le temps
où la nécessité se fit sentir de couler la respiration et la pensée dans
des signes qui la refléteraient, qui lui faisait face, il y avait un
homme dans un costume gris terne, exactement le genre de costume que
porterait un employé de Toshiba, s'il travaillait chez Toshiba, puis
fustigeant mon imagination corrompue par le *soft power* des images,
disant seulement : un homme en costume gris, raide, droit, s'inclinant
en silence à trois reprises, élevant les mains au-dessus de sa tête à
trois reprises, à jamais étrange la nature de l'échange entre cet homme
surgi d'on sait où, par la porte rouge, et le *sugi* qui, dans ses
cernes innombrables, conserve la mémoire de l'onde de choc des deux
bombes larguées à quelques encablures de là, pas très différente,
peut-être, à son échelle, des éruptions des petits cônes nerveux posés
comme des guirlandes autour des villages aux toits incurvés, rappelant
les linteaux de la porte

et j'ai pensé à Zibold

pensé 

que Zibold, l'aristocrate et savant naturaliste docteur en médecine,
s'était fait passer pour un hollandais afin d'accoster au rivage de
Dejima alors seule concession aux Européens, seule brèche praticable par
où ---

que Zibold avait amassé des collections par cargaisons entières de
végétaux et d'artefacts et qu'à cause de son insatiable ambition
scientifique pour une carte maritime avait été accusé de trahison et
expulsé ---

que Zibold le nez cramoisi par la bière et l'haleine fétide à force de
bouffer des rondelles de radis noir à la croque-au-sel avait sollicité
une audience auprès de Napoléon III afin de lever des fonds ---

que Zibold avait buté sur la langue japonaise et qu'il s'était servi
d'interprètes pour les cours qu'il dispensa dans l'école qu'on lui
permit de fonder et les collectes qu'il organisa ---

mais Zibold dans le costume gris terne de Philipp Franz Balthasar
von Siebold n'est qu'une épave de l'histoire et ce n'était pas à lui que
j'avais pensé

pas à lui

mais à Zibold entravé par les épineux points de réglementions du
shogunat de Nagasaki, lequel s'évertuait à brimer les ardeurs
commerciales des compagnies orientales sans nullement, naturellement,
empêcher les trafics de prospérer, confinant le tumulte européen
derrière une forte muraille surveillée, redoutant la contamination du
génie nippon par des manières et des sciences venues d'outre-mer et
comptant, chaque soir, en les enfermant à double tour dans leur maison à
deux étages, les quelque six cents résidents de l'îlot artificiel
parqués là comme dans une zone de transit aéroportuaire, Zibold enfermé,
qui se faisait un jeu de contourner les restrictions en vertu de sa
renommée de chirurgien, s'octroyant d'abord des excursions à Nagasaki,
puis, par le truchement d'un prête-nom, l'achat d'une maison en bois à
Narutaki en périphérie de la ville, alors que l'on comptait au bas mot
vingt-cinq espions pour un hollandais et qu'il fallait pour sortir
adresser une pétition au gouverneur Takahashi Shigetaka, puis encore la
faire tamponner aux points de passage, Zibold qui par ses contacts se
faisait livrer tant d'échantillons de plantes et de fleurs inconnues
qu'il ne pouvait --- trop dans cette boulimie d'extraire et de
collectionner

l'unique fleur pour lui cependant fut celle qu'il aperçut en déambulant
dans le quartier des plaisirs, les alignements de maisons vertes telles
qu'Utamaro les avait peintes d'un pinceau délié dont on perçoit encore
les tendres élans d'ivrogne et les soupirs d'impuissance, aux devantures
ouvertes en vitrine, au jeu de panneaux coulissants donnant sur une
enfilade de pièces vides jonchées de tatamis tressés, et, sous un nuage
de tissu admirablement noué laissant apercevoir son immobile minois, une
jeune fille assise en tailleur --- dont le regard baissé derrière les
paupières tendues, les joues fardées, la chevelure aux sombres reflets
laqués, une main en éventail devant la bouche dissimulant un sourire
ambigu ou, mieux, exprimant, en cet art, la vanité et l'imperfection de
la parole, mais peut-être fût-ce elle qui vint à lui en faisant timbrer
son laissez-passer du sceau infamant de courtisane, et alors Zibold ne
connut pas le frisson de courir dans ces rues où le troc de quelques
jetons de métal exaucent les souhaits profonds et les plus profondément
inhabitables

car on faisait ainsi : c'était "une vieille coutume hollandaise"

que faisait-on ?

on faisait l'amour à de "délicieuses Japonaises de seize ans, qu'il
aurait été difficile d'échanger contre des Européennes"

pensant à Zibold frissonnant d'émotion devant le corps solide de cette
guerrière exquisément pudique, qui, sous un éclairage diminué par la
jalousie de soie baissée, laisse glisser les brasses du tissu chamarré,
et c'est comme si ayant franchi la porte rouge Zibold se tenait devant
un être inconcevable face auquel il ne devait même pas oser étendre la
main, et demandant la permission avec un sentiment mêlé d'adoration, de
désir et de honte, au carrefour de la souillure, et quand elles
s'effleurent, la main et la cuisse, tristement, elles ne se
rencontrent pas

au cours de son premier séjour, Zibold, empêché par les agents du
shogunat, n'eut guère l'occasion de cheminer sous le couvert forestier
foisonnant d'êtres innommés

le long des dalles, à travers l'allée bordée de lanternes

jusqu'au cœur de la clairière

ne sentit point que chaque lanterne réservait un abri à ce qui n'a pas
de nom, et qui flotte et qui foisonne dans l'air, dans l'ombre, au
milieu des frondaisons délicates

vu d'outre-mer l'univers est un *sugi* devant lequel on s'incline
pudiquement --- quoi que cela veuille dire

un *Toi* auquel on s'adresse

auquel on dit : *puisses-tu* --- quoi que cela veuille dire

Zibold n'a pas vu qu'Otaki, la courtisane de Nagasaki, était un *sugi*,
et il l'a traitée exactement comme il aurait traité ce *sugi* : en la
profanant

faisant d'Otaki une fleur, celle qu'il tenait pour la plus sublime de
toute : *Hydrangea otaksa*

ainsi l'hortensia reçut-il son nom

ainsi Otaki reçut-elle son nom

et loué fut celui de Zibold

songeant enfin à Zibold, à son érudition, à l'étendue de sa culture
qu'irriguaient encore vivaces les sources latines et grecques, à son
œuvre monumentale et inachevée, me revient à l'esprit la parole de cet
Athénien aux pieds nus, qui, dans un geste inaugural n'ayant peut-être
aucun équivalent dans l'Histoire, nomma et forgea d'un bloc la
philosophie : "j'aime m'instruire, disait l'indolent Silène, je m'en
souviens à présent, mais les champs et les arbres ne veulent rien
m'apprendre", et j'y vois

dans cette opposition je vois

dans ce clivage

déchirant si tu veux

je vois

une autre porte, qui celle-là n'est pas rouge

<video autoplay loop muted playsinline>
  <source src="/img/plus/zibold/01.mp4" type="video/mp4" />
</video>
