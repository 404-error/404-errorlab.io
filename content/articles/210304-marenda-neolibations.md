---
persona:
  - suf marenda
title: "NÉOLIBations"
slug: "neolibations"
date: 2021-03-04
echo:
  - rythmique
images:
  - "img/210304-marenda-neolibations.jpg"
gridimages:
  - "img/210304-marenda-neolibations-grid.jpg"
sourceimage: "https://commons.wikimedia.org/wiki/File:Charles_Marville,_Urinoir_envelopp%C3%A9_%C3%A0_6_stalles,_Jardin_de_la_Bourse,_ca._1865.jpg"
notes: ""
summary: "30 ans de sauces-dem', à nous repaver les dessus. / puis, / ILS ont couvert nous-l'étendard de leur chaude-pisse à la glaciaire. / L'ÉTAT *golden showant*, le sans répit. la pause pipi-*pending*. / voici venu l'ÉTAT de permanence des édenteurs, qui, au oui-non, ne s'est / pas ni, / ni retiré. c'est le contraire ! l'état d'adroit de la main gauche et de / l'ÉTAT !"
citation: "ILS ont couvert nous-l'étendard de leur chaude-pisse à la glaciaire. L'ÉTAT *golden showant*, le sans répit."
---

30 ans de sauces-dem', à nous repaver les dessus.

puis,

ILS ont couvert nous-l'étendard de leur chaude-pisse à la glaciaire.
L'ÉTAT *golden showant*, le sans répit. la pause pipi-*pending*.

voici venu l'ÉTAT de permanence des édenteurs, qui, au oui-non, ne s'est
pas ni,

ni retiré. c'est le contraire ! l'état d'adroit de la main gauche et de
l'ÉTAT !

et lévitant, il s'est mis, *food porn*, à repisser, et puis encore. Et
puis nous, nous de dessous, à nous limer comme de la glace, tout du plus
fort que lui le sait.

IL nous "invite" maintenant, nous, granulaire "SOCIÉTÉ CIVILE !", à
court de tout et de miracles, à nous sortir de nous, les doigts. Et
prends par toi, par toute la main, chacun·e, en tant que le gourdin
liquide se RUISSELLERA en STYLE, et des plus beaux vers nos plus riches.

Et il y a nous, encore, parmi le peuple, tant les preneurs, les
ENTREPRIS au sein du peuple ; il y a cette joie PRIVÉE des subventions
publiques ; il y a l'enfer de tous les PROJETS dans le mythage de
l'associel ; il y a nos PARTENARIATS AVEC et puis la BANQUE-elle et le
cadrage materné dans nos "AGENCES et DE MOYENS" ;

Et il y a des INDIVIDUS, AUTOmystifiés, qui ne me voient jamais. Ils se
répètent : "je, RESPONSABLE" ; "je, VEUX, LIBRE et MÉRITANT". Ils se
répètent : "je, VEUX m'informer OBJECTIVEMENT". Qu'ils se répètent :
"je ne sais pas si mon je, il est de DROITE" ; "je, VOTE" ; "je, ne
VOTE pas pour un parti, mais pour cet homme" ; "je, VOTE pour des
IDÉES, pas pour un homme". Ils répliquent, de fait : "je, RÉPubLIQUE".
Et, c'est ainsi qu'après, il y a nous, aussi, et parmi nous,
pauvre-Corinne, la spécialiste du montage généreux et des sages PROJETS
européens (Corinne, la "halte au feu ! et pour le peuple !", la "il
faut bien vivre, prendre l'initiative au moins à deux, ENTREPRENDRE avec
des amigos, mais plus en marge, et à côté du constaté CAPITALISME, le
NÉOLIB --- on le dira ensuite") ; et il y a ces malheureuses escouades,
sur des scooters racisés, qui distribuent de la *forn pood* à de moins
pauvres qu'eux, à tou·te·s les incité·e·s à posséder le peu du crédit
dans le périurbain. À contresens d'un monde. À se serrer l'aide à la
gorge des AUTO-ENTREPRIS et les aides aux soi-même-un-peu-plus-loin,
des aides-soignantes et des chômeurs à langue duveteuse.
<br><br>

que feront-on possiblement ?
<br><br>

plusieurs communes mutuellistes ?
<br><br>

Quoi de plus simple pour EUX, sinon.
<br><br>

Au milieu du tunnel, ILS nous pisseront souvent encore dessus,
l'ÉCONOMIQUE, la RÉPUBLIQUE (LAÏQUE, la UNE ET INVISIBLE), des ÉLECTIONS
AU SUFFRAGE UNIVERSALE, et de la LOI, et des DÉCRETS, des ÉBORGNEURS,

et puis, à peine, si quelques JUGES

ou en

PRISON,
<br><br>

Plaignons-les, tous nos pisseux,

Mais, non merci de tous LEURS PAUVRES.
