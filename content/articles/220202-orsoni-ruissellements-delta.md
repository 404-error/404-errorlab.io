---
persona:
  - Jérôme Orsoni
title: "Ruissellements Δ"
slug: "ruissellements-delta"
date: 2022-02-03
echo:
  - rythmique
images:
  - "img/220202-orsoni-ruissellements-delta.jpg"
gridimages:
  - "img/220202-orsoni-ruissellements-delta-grid.jpg"
sourceimage: "https://commons.wikimedia.org/wiki/File:Arnold_Boecklin_-_Island_of_the_Dead,_Third_Version.JPG"
notes: ""
summary: "Mer d'huile ou bien de sang / plage de vide ou d'excréments / tout semble incandescent / --- calanques / je ne regarde pas le paysage / non je suis le paysage / et le paysage est détruit / à l'attaque de l'écume / sauvages qui ont tant bu / qu'avalée jusqu'à la brume / je cherche un second souffle / dans la sueur qui coule de mon front / de guerre / et envahit mes yeux / de pierre / toujours au bord de l'évanouissement / à quoi ressemblerait une chose / qui contiendrait sa négation / ne se détruirait pas ? / une élégie optimiste / une utopie défaitiste / une épopée en temps de paix / tout semble incandescent / --- calanques / j'ai du sang rouge / sur la plante de mes pieds / noirs / accumule les radiations / zèles citron / rougis au soleil / pour l'avenir des nuits / sans sommeil / au loin --- non un peu en dessous / de mes yeux --- / ce vert si profond / qu'il devient bleu / pierres sculptées par les éléments / temples votifs / ruines sublimes de rien du tout"
citation: "mégalithes d’ossements / d’errance / de science muette"
---

Mer d'huile ou bien de sang\
plage de vide ou d'excréments\
tout semble incandescent\
--- calanques\
je ne regarde pas le paysage\
non je suis le paysage\
et le paysage est détruit\
à l'attaque de l'écume\
sauvages qui ont tant bu\
qu'avalée jusqu'à la brume\
je cherche un second souffle\
dans la sueur qui coule de mon front\
de guerre\
et envahit mes yeux\
de pierre\
toujours au bord de l'évanouissement\
à quoi ressemblerait une chose\
qui contiendrait sa négation\
ne se détruirait pas ?\
une élégie optimiste\
une utopie défaitiste\
une épopée en temps de paix\
tout semble incandescent\
--- calanques\
j'ai du sang rouge\
sur la plante de mes pieds\
noirs\
accumule les radiations\
zèles citron\
rougis au soleil\
pour l'avenir des nuits\
sans sommeil\
au loin --- non un peu en dessous\
de mes yeux ---\
ce vert si profond\
qu'il devient bleu\
pierres sculptées par les éléments\
temples votifs\
ruines sublimes de rien du tout\
pourtant foulées\
heures de marche\
pour cinq minutes de solitude\
histoire de la modernité\
et puis d'après\
quantité d'efforts qui toujours s'accumulent\
tout semble incandescent\
--- calanques\
sans le comprendre\
je répète ce mot\
il est un talisman\
caché dessous la langue\
sèche\
ici la terre est dure\
et l'eau n'est pas potable\
territoire de déesses\
impassibles à la divination\
leur désir exorbite\
avant que de se jeter du haut de la falaise\
viens chante vague\
leur désir exorbite qui distingue\
l'écueil de l'échec\
mats les éclats de verre\
les éclats de lumière\
où nous aveugles\
cherchons l'origine de notre ressentiment\
guettons la faille\
tout semble incandescent\
--- calanques\
dans l'ombre ou bien l'écart\
détroit passe et emporte sombre\
le futur lucide de nos yeux\
sel au bout d'un cil\
ici plus que terre\
sèche comme notre histoire\
plantes basses qui poussent\
malgré l'appel du vent\
au levant plus doute aucun\
rite emphatique de l'eau\
plus claire que nos transports\
tout semble incandescent\
--- calanques\
ma langue est morte\
je ne suis plus que feu\
métamorphoses consumées\
et brûlés les palais et brûlés les temples\
des idoles jonchent le sol\
que nous foulons aux pieds\
il n'y a plus que flammes pour rédimer\
nos culpabilités bleu folie\
j'ai le regard vide\
des millénaires qui me précédèrent\
et dont je ne sais plus que faire\
que faire ?\
que devient l'idiome dont on n'a plus nul usage ?\
idiot musée des formes mortes\
où nos récits s'entassent\
loin des oreilles des peuples indifférents\
je pourrais parler cent ans encore\
je pourrais parler jusqu'à la fin du temps\
qui s'en apercevrait ?\
je suis loin\
à l'espèce défunte\
pourtant ne m'arrive-t-il pas\
moi aussi en de certaines occasion\
de jouir encore\
et puis de rire encore ?\
un peu honteux certes de ne porter pas\
en toutes circonstances\
le deuil de ma parole\
le deuil de mes origines\
je n'ai plus de sang dans les veines\
plus un souffle dans la poitrine\
tout semble incandescent\
--- calanques\
claque la langue\
à l'alentour du détroit\
j'ai des hosties versicolores plein la bouche\
avec lesquelles de lutte grâce\
je versifie les données\
falsifie les rimes\
et donne des preuves étonnées\
aux rameurs de mes rêves\
à l'or comme au soleil\
une affaire de sainte rage vermeille\
craque la peau sous les ans\
rides sur la mer aride\
désir d'insecte qui nous façonne\
à l'ombre les arbres raisonnent\
on s'abreuve de légendes lapidaires\
au pays des invertébrés\
quelqu'un croit découvrir un sens dans une roche\
elle s'effrite et tout s'efface\
j'ai des fourmis qui rampent\
à même la peau de mes os\
maigreur de l'air à la lumière\
élégiaque un peu\
ou en silence\
rare missive bizarre\
qui fait des drames de malentendus\
un mot pour un autre\
rivages confus sommes étourdies\
de tous ces mystères\
au grand jour\
où suis-je pour mentir ?\
mais est-ce bien moi qui interroge\
ou l'idée que je me fais de l'acte\
brave mais maladroit ?\
éboulis à n'en plus finir\
à en perdre la vue\
ici qu'on crie pour s'entendre\
pour se faire discret qu'on s'absente\
aux regards de l'universelle inquisition\
c'est le même cirque panoptique\
moins de spectacle que de haine\
à la chaîne œuvrent nos tristesses infinies\
plaques sur la mer\
ΗΕΛΙΟΣ est la somme de nos reflets\
quand brûlent nos larmes\
de choses sans distance\
et notre fragile prose pour seule défense\
rachitique le rempart\
de nos habitats rampants\
j'ai les pieds qui enflent et j'ai les pieds qui saignent\
telle est la théorie de mes crampes\
en ligne nos esprits s'éteignent\
invention du peloton\
amertume de plancton\
exécution générale\
un corps qui marche n'est pas perdu\
(pas perdus)\
même quand il se sauve\
il fonde l'édifice de son salut\
bâti de rien que du vent\
de l'air entre les pierres entre la terre de l'air entre les choses\
je goûte avec ma langue morte\
ce souffle qui rend fou\
ou gagne la fuite\
nous partons parlons lézardes\
petits animaux en déroute\
anfractuosité de la retraite\
toujours le sens du mouvement\
j'entends *la sensation*\
infime comme une aurore\
perceptible à peine\
c'est le corps qui émerge\
ô notre mère des algues\
déesse des vérités\
étoile muette\
dans la jungle humide des êtres\
notre chair calcaire\
millénaires aveugles\
d'où est venue l'éclaircie\
quelque vague se dissipe\
comment vivions-nous alors\
comment sinon confus ?\
tout semble incandescent\
--- calanques\
je tombai dans un trou\
une faille\
et là crevasse ou anfractuosité encore\
dans cette toute relative humidité\
passais un certain temps\
à contempler les vivants\
hommes ou bêtes\
mâles et femelles\
--- toutes sont indifférentes\
tous font l'indifférence ---\
des bruits d'ailes battant de désespoir\
attirèrent mon attention\
dans cette pénombre à demi\
un oiseau était occupé\
avec la patience la plus sublime au monde\
à en tuer un autre pour le manger\
oiseau de mer contre oiseau de terre\
avant que d'assister à ce spectacle\
on ne se doute pas\
du temps qu'il faut\
à une bête quelconque pour en achever une autre\
si long que d'ailleurs\
cet acte durant\
une autre bête semblable\
venue du ciel au-dessus de la mer\
passa quelque temps là\
assistant à la représentation\
piaillant intermittent\
pour dire dans ce langage que je ne compris pas\
en restera-t-il seulement un peu pour moi ?\
un petit bout ?\
une becquée ?\
et puis constatant qu'il n'y en aurait pas\
partit pour d'autres cieux\
d'autres bêtes à avaler\
quand l'autre toujours affairée\
s'acharnait pour enfin manger\
qui n'en aurait fait de même ?\
pensai-je dans mon espèce en retrait\
qu'est-ce que l'amour\
sinon cette chair qui nous pend au nez ?\
plutôt que de fermer les yeux\
je regardais cette bête qui en mangeait une autre\
avec cette application si violente\
que seule l'absence la plus parfaite de morale\
peut expliquer\
le pur mouvement\
la vie\
l'innocence\
la faim\
l'envie\
le désir\
quand ensuite on retrouve un os\
au petit matin\
trace toute nue de cette effusion passée\
et passionnée\
c'est que tout le sang fut absorbé\
dévorés les corps\
courant ultime de la vitalité\
ultime c'est-à-dire sempiternel\
comme le vent la terre\
l'espace à l'avenant\
acte comme preuve de l'infini\
combien de temps demeurais-je là\
tout au fond de mon trou ?\
je ne saurais le dire\
le temps qu'il faut sans doute pour faire\
une expérience\
se garder de la méfiance\
se perdre dans la chair des autres\
des bêtes du roc du monde\
se laisser engloutir par la grande bouche\
la fin de la mer se confond toujours avec la fin de la terre\
et l'histoire des êtres\
dont je ne sais plus que faire\
tout semble incandescent\
--- calanques\
mégalithes d'ossements\
d'errance\
de science muette\
raide comme nature\
instinct\
increvable navigateur\
quand même à l'arrêt\
j'entends encore les coups de bec\
voraces et méthodiques\
croyance spontanée en la nécessité\
qui s'en écarte court à sa perte\
ignore le drame et l'ignorant\
le perpétue\
c'est la vie même qui tue\
et la nescience où nous sommes\
depuis notre naissance\
égarés dans nos élévations abstraites\
d'où jamais rien ne retombe\
que les tombes toutes prêtes\
pour des peuples d'êtres\
destinés à l'ennui destinés à l'oubli destinés à eux-mêmes\
oh comme tout semble incandescent\
--- calanques\
qu'est-ce que l'amour\
sinon le retour sur terre ?\
qu'est-ce que l'amour\
sinon ce lien acéré ?\
la jouissance a la couleur de la mort\
regarde-la\
écarlate et fascinante\
elle ne laisse pas de traînées\
une fois passée\
se propage par giclées\
explosions et vérités\
hémoglobine et stupre\
et la douleur de l'un n'est que\
l'image dans le miroir\
de la puissance de l'autre\
qu'est-ce que l'amour\
sinon cette force absurde ?\
à laquelle nous autres\
cosmonautes spectateurs\
sommes sommés de croire\
images à la surface de nos rétines\
au fond de l'anfractuosité toujours\
sur l'écran\
diffusion de sang\
tout semble incandescent\
--- calanques\
brûle mes pleurs\
iode\
j'ai l'os tendu vers les sommets\
les hauteurs\
mille ardeurs au labeur\
du bout de mon isthme\
je crève la stratosphère\
pas besoin de navette\
tu sais\
pour voler\
tapis d'étoiles sans goutte de pluie\
je fais des rêves sans dormir\
visite la sainte étendue\
des vagues\
une île n'est pas un corps perdu\
elle ne connaît pas le repos\
émerge sans cesse\
dans le flux\
physique des flots\
mets ta parole en doute\
en joue de l'écoute\
arrache-moi oh arrache-moi\
à la terre perdue\
la terre malsaine d'où je proviens\
arrache-moi à la cité\
la police des sens partout veille sur moi\
éloigne-moi\
ignore-moi de la nature\
et des règnes en illusion\
dont nous ne serons jamais revenus\
la roche est le dédale\
où se perdent nos songes binaires\
et tout rêve d'y échapper\
quelque part au-delà de l'un et du multiple\
des calculs dont nos méthodes sont pleines\
bien au-delà de l'être calculant\
et de ses machinations basiques\
natura denaturata natura denaturans\
puisque tel est le cirque\
où résonne l'abolition du sens\
écho des vérités à venir qu'on extirpe aux forceps\
de nos certitudes perdues\
mais pourquoi\
la parole de demain vaudrait-elle mieux que celle d'hier ?\
tout est incandescent\
--- calanques\
pas de refuge ni de passage\
quand je mets un pied devant l'autre c'est déjà loin\
loin des arts maladifs\
aux bataillons de haineurs les yeux rivés sur le bien\
consacrés\
temples pour les peuples de peu de foi\
dans ma demeure aérienne\
j'ai léché les parois du labyrinthe\
pour y trouver mon chemin\
telle fut mon ardeur pariétale\
au premier signe tracé\
à la marque laissée\
sur la face du mur invisible\
à la face du monde déjà-vu\
toutes nos histoires prêtes-à-porter\
j'ai tiré un trait dessus\
je fus l'insecte\
qui se mit à chanter\
aux premières chaleurs\
j'ai encore la trace des bêtes sur le corps\
des morsures de morts\
des déserts au fond des yeux\
quelque chose qui s'injecte\
quelque chose qui s'infecte\
au coin du supplice\
quand l'air sec aura fini de dissoudre\
l'éternité de nos préjugés\
ici tout va si vite\
les faces blêmes bronzent\
et il s'en trouve encore\
pour objecter aux rayons du soleil\
passion abjecte de l'unicité\
il y a tant de vérités\
qu'il faut se satisfaire de dire *vrai*\
que faire ? qu'admettre ?\
l'absence de preuves de l'existence des cieux ?\
il n'y eut jamais de silence\
mais souffle mais souffle mais souffle\
le vent s'est engouffré sous les plis de ma peau\
le sexe maritime\
droit à la cime qui la touche\
qui l'enfourche\
passe le col\
dépasse les profondeurs\
l'aurore agite les atomes\
nous ne sommes rien\
que déviations bizarres\
tout doit être laissé au hasard\
tout est incessant\
--- calanques\
je chanterai\
\: adieu.
