---
persona:
  - Étienne Michelet
title: "Les lèvres invisibles"
slug: "les-levres-invisibles"
date: 2020-11-07
echo:
  - rythmique
images:
  - "img/201107-michelet-les-levres-invisibles.jpg"
gridimages:
  - "img/201107-michelet-les-levres-invisibles-grid.jpg"
sourceimage: "Étienne Michelet"
notes: ""
summary: "Du jour où j'ai commencé à refuser de manger les yeux des animaux, de manger leurs yeux noirs comme des mondes noirs, n'y avait-il pas déjà et très tôt en définitive un choix politique, dans une intuition silencieuse qui ne se dément pas avec les années ?"
citation: "vivre dans la lumière des pays hantés"
---

# La lumière des pays hantés

Du jour où j'ai commencé à refuser de manger les yeux des animaux, de
manger leurs yeux noirs comme des mondes noirs, n'y avait-il pas déjà et
très tôt en définitive un choix politique, dans une intuition
silencieuse qui ne se dément pas avec les années ? Du jour où j'ai
décidé, plus tôt encore, de vivre dans la lumière des pays hantés, mes
gestes ont pris plus de lenteur, et chacun de ces gestes était sur le
fil, tendu comme jamais aux nerfs souples... Je m'accordais ainsi à
cligner des yeux devant le soleil, mais le sang ne faisant qu'un avec la
lumière et les mots qui tournaient alors dans mon esprit comme des
pierres précieuses, colorées et magiques. Je me suis dit dans une langue
secrète (qui ne regarde que moi) que je serais bien un enfant doué d'une
sensibilité phénoménale, de celle qui touche sans interruption au monde
par le recours des sens invisibles. Et je me suis dit aussi qu'il n'y
aurait plus de limites entre le silence du ciel et la circulation des
flux sanguins dans mon cœur affolé. Que voulez-vous... un positionnement
politique ?... dans un monde qui oublie l'invisible, alors que toute ma
vie est conduite par l'invisible ?...

Une fois que le monde ne tient plus, on peut alors le cribler de
fantômes mille fois, des animaux-fantômes pourtant plus réels que les
grimaces molles de leurs yeux vidés, à trop pleurer des cristaux de sel
brun... Animaux-amis, cette compagnie céleste jusqu'à la mort, quand
Gautama couché s'éteint au milieu des pleurs, la girafe, le singe eux se
taisent, et comprennent l'essence d'une âme dans sa profondeur, dans sa
hauteur qui ne meurt jamais ! Singe trois fois... la vue, l'ouïe, la
parole pesées... et l'ossature de mon esprit, sa colonne de dernière
chance, demeure stable...

Au fond, ces îles noires qui me bouleversent à chaque fois sont une part
de mon âme qui tremble ; des amas d'esprit échoués à la surface d'un
miroir opaque. Et ces lèvres invisibles n'ont pas cessé d'exister
depuis l'enfance, et je les nomme... et j'y crois encore !... Les
lèvres du commencement, qui brûlent toujours d'un baiser... ou d'une
seule morsure dans le souffle du ciel... Ces lèvres-là je les touche,
aériennes et intimes... cet invisible qui visite et ouvre le cœur comme
une fleur, ou un œil... je le touche.

Que mon cerveau ait brutalement commencé à jouer des soubresauts, des
fantasmagories, ça me regarde... Je me suis habité de cette manière, et
je n'ai pas menti... En ce moment même, je me lèche la main et c'est mon
seul plaisir, de voir briller la salive sur ma peau... Il n'y a pas
d'esprit parce qu'il n'y a plus dans l'humain ni esprit ni temps... Il
est dévoré, l'humain, par ses images inertes, qui le renvoient à son
propre émoi, capricieux et vide, étanche à l'esprit du monde invisible.
Le jardin fantôme n'est pas une vaine fantasmagorie, une figure
hallucinée. J'y tiens des mouvements qui viennent se répercuter en
profondeur dans le tissu de mes organes. Le monde se tient là, dans le
remuement du feuillage, comme dans le remuement infime d'une lèvre
aimée, et les signes sont des signes de silence, mais qui font alors
entendre dans ce cœur qui brûle une musique de sang, une circulation de
feu. L'incendie intérieur, ce secret, naît de ce presque rien, qui est
tout, qui est le centre.

Ma débilité mentale, ma lâcheté me poussent à voir des couleurs là où il
y en a, *invraisemblablement*, à la lisière des apparences peut-être,
mais dans les apparences tout de même, parce que le nœud s'y trouve, je
le dis encore, dans un mouvement qui se mérite. Il est immédiat et ne
tergiverse pas dans des vêtures ineptes ou des sous-entendus, ces
mensonges stériles... Je suis lucide, à la lisière, mais lucide ! Et ces
lèvres j'y crois, j'y mettrais mon âme à brûler, ou ma langue à couper,
voilà... parce que ces lèvres... ce sont celles qui donnent le mouvement
éternel, comme dans la succion, et font tourner les corps, et le temps
atomisé dans l'amour, son mouvement dans un seul temps, et tous les
temps enfin réunis, dans ces lèvres, ces muqueuses ; dans le sacrement
des lèvres entrouvertes... Je la goûte encore, l'humidité qui brûle et
qui donne les couleurs... Ma dévotion est immense ! Elle n'a plus de
frontières... Tes dents sont un collier de jeunesse vernissé que
j'adore, alors souris... Entrouvre tes lèvres rouges dans le ciel
blanc... Ma débilité mentale me fait affirmer que les croyants se
prosternent devant des bouddhas qui ne sont que des sexes entrouverts
comme des lèvres... Ils sont les dévots des sexes et des bouches, des
pulpes du souvenir utérin. Je ne vois pas de moine, de figures
anthropomorphes dans le rocher de Seonbawi, mais les muqueuses
constellées des yeux de divins sexes féminins étoilés, des fleurs
délicates encore, dans le ciel des ciels de mes nuits.

Je ne dis pas qu'il y ait un sens sacré dissimulé dans le monde et que
l'esprit le déchiffre à travers un symbolisme creux et vacillant, guidé
selon ses humeurs (une chimie cérébrale instantanée)... Je veux affirmer
seulement (et le plus maladroitement possible) une fantaisie qui permet
d'admettre le monde dans la multiplication des sens. C'est comme si
l'araignée tissait sa toile avec des ornements capricieux mais délicats,
des motifs à ravir tous les cœurs (et surtout le mien), qui explosent à
la vue de ce canevas merveilleux... Et comme Saint François, afin de
remercier comme il se doit ce miracle du jour, je vais goûter la
réalité, et pour la goûter véritablement, poser ma langue dans la
viscosité même de la toile, peut-être sur le point central, ou l'abdomen
de l'araignée, l'outil de son langage... sa production salivaire qui
rencontre ma salive, la contamine... Comme je crois à cet acte-là !...
de goûter le monde, pour le reconnaître dans cette vie, d'une langue
exacerbée, mais qui s'affirme dans tout son éclat, une langue délicate
et ouverte... passagère oui, mais mémorisant les parfums... pour les
tourner, les retourner dans des circonvolutions infinies, et créer ainsi
des parfums nouveaux, improbables et lumineux, des incandescences de
phosphore...

Si je vois du sang sur mon cuir chevelu, des taches de sang se former,
c'est que je garde des stigmates dispersés comme une nuée d'étoiles de
sang, dispersées elles aussi par la pensée qui s'est joué de mauvais
tours sous de mauvais jours... Oui la pensée s'est prise au sérieux...
Voyez-vous, la sexualité, la seule en laquelle je crois véritablement et
par-dessus tout, est le contrepoint de cette époque maquillée en
sourires, en grimaces purulents... La sexualité mimant les pilons des
machines, et la succion sans âme des organes érectiles qui dépossède les
corps et les esprits... Le corps est lourd de ces yeux qui sont des yeux
sans regard. L'esprit baigne dans la pornographie aveugle sans goût. Les
corps prennent, et prennent, sans même se regarder une seule fois, sans
même se sourire. Je peux affirmer qu'une autre spiritualité peut exulter
dans les corps ! Et cent fois !... Exulter dans un sourire de communion
qui renverse le monde et qui perfore la nuit ! Ce sourire né d'une
intuition de connaissance commune, c'est cela posséder ! Posséder par
la proximité, par la distance d'un regard et d'un sourire, en une seule
fois... Je dis posséder, dans l'union d'un sourire et d'un regard qui
effleurent l'épiderme des esprits ainsi réunis... Car tout le reste
n'est rien en définitive, ou seulement un désir de soi obscène et
opaque... une vie d'érotomane de soi... une vie de boucher, une vie
d'absolument rien... où le sourire-grimace mène au scorbut...

Gautama au sexe fleuri, je t'admire et tu mérites bien toutes ces
prosternations, génuflexions car tu as su rester indifférent à la
tentation des démons et par là garder ton centre intact... Intact ! Ton
plexus solaire n'a pas tremblé une seule fois face aux tentatrices, et
tu n'as même pas ouvert l'œil, ou porté un seul regard sur les filles de
Māra. Tu es allongé sur le flanc droit, la tête reposant dans la paume
de ta main comme dans une coquille ouverte, dans l'écrin de ton
visage-perle de coquillage... Les démons soufflent autour de toi des
paroles découpées en volutes, en filaments d'écailles. Mais toi tu
restes indifférent, centré, dans la perle-coquille nacrée de ton esprit
blanc et silencieux. Les animaux lunaires sont sortis de la forêt du
rêve pour se recueillir près de toi. Moi je voudrais passer des jours à
caresser lentement sous mes doigts le front d'ivoire des éléphants
blancs. Cette nuit je fais tourner dans ma main le singe sculpté à trois
figures rapporté du temple Waujeongsa.

{{%asterisme%}}

<span style="margin-left: 10%; display: inline-block;"></span>Chère amie,

Oui, je prétends en savoir long sur les fondements de l'esprit. C'est un
principe, c'est une couronne que je m'octroie volontiers, et sans façon.
Les confusions sociales du monde peuvent ravager les pensées des hommes,
je reste de mon côté sauf de toute pensée qui trempe dans les
phénomènes... Qu'on me laisse... Car oui il faut bien regarder à
l'envers pour voir à l'endroit. Je mets un point final aux échanges qui
nient la valeur de l'esprit, et sa simplicité... Pourquoi s'être éloigné
de l'esprit qui nous tient au monde, qui élève d'un cran, au-dessus de
tout, en voulant parler comme les autres, et à ne pas savoir se parler
au-dessus... Pourtant j'y ai cru, et je n'en sors pas... De cette
pyramide de préjugés qui s'est établie comme une montagne, un montage
des autres dans nos propres bouches... On ne peut reprendre une
substance perdue. Je ne comprends pas le langage des larves, des têtes
qui subissent le temps, qui s'en désespèrent... Mettre toute sa vie dans
le désir, jusqu'à la mort, et ne pas arriver à parler avec les mots
qu'il faut ?... Des mots qui se ramassent paisiblement comme des
fumées, des paysages sans lumière, larvaires... J'aurais tant aimé...
contrebalancer, parce que, oui... je sais définitivement vivre... même
si l'esprit est sorti de ses gonds, je le reconnais, mais je sais
vivre... authentiquement, et je ne pense rien du côté affectif,
seulement à un absolu qui me sert de rythme... Voudrais-tu que nous
parlions encore de cela ? Je veux me sortir, quitte à augmenter les
vertiges, de la prise des comprimés, quitte à sentir le cœur me brûler
dans la poitrine, et une pression se loger dans les tempes. Je veux en
sortir rapidement, car cela a trop duré... Mes peurs, je les absorbe,
quitte à crever, et je m'en bas l'œil, tu m'entends ? Si cela tient
vraiment à cœur, si cela tient durablement au cœur, pourquoi ne pas y
donner une réalité, dans l'immédiat... Pourquoi avoir creusé des
tombeaux pour les mouches ? Pourquoi avoir mâché des filets de viande et
de poisson pendant des mois sans jamais une seule fois se dire que le
vide qui était là, et qui résonne dans le ventre, sans les mots et les
stations des autres, sans leur langage morbide... qu'au fond, et
fondamentalement, dans les viscères, quelque chose résiste, et que ce
quelque chose qui résiste, c'est toi, c'est ta présence, et qu'elle ne
peut pas échouer dans le monde des phénomènes, et qu'on ne peut abdiquer
face à l'autre... même face à soi, et à ses peurs... Me comprends-tu
enfin ? Que l'on ne peut abdiquer face à l'autre, même face à soi...
C'est maintenant l'heure des fleurs, et je suis ton ami sincère...

<p class="droite--signature">Étienne</p>

# Totémisme

Le serpent se met à remuer dans l'abdomen. Peut-être
réussira-t-il par ce mouvement à calmer les brûlures coronaires... Sa
langue irriguera les alvéoles et ainsi irriguera la pensée elle-même. La
forêt intérieure, humide, bouge elle aussi subrepticement. Les écailles
du reptile brassent le végétal et la terre, et tous les minéraux en
particules sur les parois du foie, de l'aorte... Aorte magique,
crosse-totem qui s'élève, nourrie par la pensée verticale... le voici
l'arbre qui saigne...

Et puis, et puis quoi ?... Ma colonne segmentée, comme les
segments de la chenille, en rhizomes mobiles, habités... ma colonne
verticale, habitée, articulée, les ligaments habités, par quoi ?... La
vie ! Mobilité du totem qui coulisse, segmenté, et le serpent qui
continue de tourner, et le sang fluide qui continue sa course folle dans
les vaisseaux... Totem de nuit, totem rouge de nuit. Les yeux des
animaux du totem invisible... Invisible ? Totem intraorganique,
intraoculaire... les globes-lanternes, les membranes du rêve en
éclaireuses... Je m'invente des caresses, et je les imagine très
bien, à l'intérieur du corps, des caresses intracorporelles, plus douces
qu'une langue de serpent, plus douces qu'une lame...

J'aurais bien besoin de quelques crabes... qui viendraient
comme cela s'agiter, et par pure amitié, par une étrange fraternité,
afin de désengorger mon cœur et le nettoyer des crasses avec leurs
pinces... mon cœur, le nettoyer des alluvions de paroles décomposées...
ces douleurs poitrinaires qui perdurent et qui restent et que je ressens
comme des brûlures folles... Je voudrais le recracher ce limon, le
recracher sur une face de monstre féminin aux lèvres barbouillées de
rouge carmin-cochenille, et sur ses yeux, ses sourcils en arcs
boursoufflés, un jour fins comme deux poils de loup, un autre plus
épais, comme des ailes de libellules carbonisées... Mais je les vois ses
yeux cerclés en orbites gonflées, proéminents... ses yeux de biche
camouflés, fumeux, avant qu'ils ne tombent dans l'oubli comme des
insectes broyés par le temps...

Je crois, moi, que la pensée doit venir avant les actes, et
non que l'esprit s'ébranle par réaction à la matière. L'esprit n'est pas
une réponse. Cette ménagerie que je porte secrètement influe sur mes
mouvements, et non l'inverse... Mes actes, même les plus bruyants, ne
font que répondre aux couleurs fomentées en silence, ces ferments qui
déterminent ce que je touche, et décident jusqu'à la vitesse de mes
gestes. Au fond, c'est une affaire de sang contaminé par les rêves, par
des totems nocturnes qui se greffent aux vertèbres, qui articulent et
meuvent la volonté dans la souche même, jusqu'à la moelle épinière, cet
agrégat du langage secret, de l'enfance et de ses parfums humides...

Je crois qu'il y a une vérité dans le sang. Le bouton
aortique manifeste une vérité dans sa dilatation, son gonflement
ascendant. Les parois de la crosse artérielle sont constellées d'un
bestiaire en mouvement, de clignements de paupières qui répondent aux
phases lunaires. Les serpents sont immobiles, serpents de porcelaine,
leur œil répond aux lunaisons consécutives. Notre diaphragme doit
s'accorder, s'épaissir dans le feu de l'astre de nuit. Nous avons ignoré
ce principe fondamental, bornés à ne pas reconnaître ce qui nous habite
verticalement, et dans tous les sens... ce souffle qui coordonne tous
les influx sanguins et qui est intimement lié à la pensée primordiale, à
la nuit, au désir de l'astre, à sa lumière de soleil blanc. L'être
humain s'est vidé... il n'est plus qu'un automate creux, un cadavre qui
passe sur la rivière. Je veux tenir cette flamme d'intelligence pure...
en faire une marqueterie délicate, colorée... Car une profondeur
insoupçonnable est là, dans le silence... Tous les autres sont morts,
cent fois ! Et les tigres n'ont même pas voulu s'intéresser à l'odeur de
leur cadavre... parce qu'ils ne maudissent pas les ténèbres, eux... et
ne sont pas vidés, par ce manque de sincérité qui, en l'homme, le
condamne à se parer... à s'afficher avec ennui, avec ses médailles !...
Tout animal est dans l'homme, mais tout homme n'est pas dans l'animal...
Plus besoin de parures ni de batailles... Je vis ma mort et ainsi, je
connais infiniment ma vie... et ma salive équarrit les angles, elle
devient argile... et modèle...

Peut-on enfin comprendre que ces vérités-là adviennent non pas
dans les hallucinations provoquées par l'ennui, mais bien par les
manques, par les sensations de perte et d'absence ?... Mais qui veut
aujourd'hui s'y confronter véritablement ?... Alors que l'on abdique si
facilement, quand le sens voulu par les besoins primaires n'est pas
atteint... L'esprit ? On le fuit. Moi je veux jouer de la harpe et
trouver les accords dans les saignements de doigts. Après cela, toute
mélodie gagne sa profondeur.

Les délimitations habituelles des sentiments ont des limites
si étroites ! Mais comment accepter de vivre dans si peu d'espace ?
Comment l'esprit peut-il se conformer à cette compression, tant cette
époque nous voile de ces petites angoisses morales ? Et quand il s'agit
d'aimer, d'amour, peut-on abdiquer ainsi, à deux, face à un système où
rien ne brûle ?... Et ne pas trouver les mots, et se taire ? Quitte à se
fourvoyer en utilisant les mots des autres. L'infamie, qui est ultime,
c'est de ne se préoccuper que de savoir si nous baiserons bien... Et il
faut faire confiance ? Vraiment ? Dans ces sentiments iniques, à reflets
de miroirs opaques... Cette morbidité, elle ne sort pas de ma bouche...
mais de la leur, et à plein régime... Elle écrase tout, et l'enfance en
premier lieu, cette liberté que l'on n'attend plus... et de personne...

Mes animaux totémiques se multiplient et leurs langues
tournoient dans mes poumons. Leurs yeux sont des phares, et leur
fourrure, leurs écailles et leurs plumes sont plus douces que des
flammes. La carapace de la tortue est émouvante parce que fragile, et le
monde, dans sa fragilité, est le plus digne d'être habité.

Il n'y a que cet absolu silencieux qui brûle véritablement,
tout le reste est à oublier définitivement... Il faut mâcher les
racines, et cracher au sol les résidus, le jus des gencives... Il faut,
et je le crois encore, brûler les bibliothèques, et par amour, gifler
l'humain, encore et encore... par amour... Il faut se parler, il faut
que l'on se parle... avec des mots qui creusent, et non pas ceux des
livres, mais ceux de ton enfance fragile, quand ta mâchoire accrochait
encore les syllabes, jusqu'à faire vaciller les heures... Quand tu
soulevais l'araignée par l'une de ses pattes, dans cet instant où elle
s'immobilisait, toi tu plaçais ce trésor de vie en l'air, juste
au-dessus de tes yeux, pour voir sa silhouette se fondre dans les
nuages...

{{%asterisme%}}

<span style="margin-left: 10%; display: inline-block;"></span>Chère amie,

je veux revendiquer l'accomplissement d'une intuition
primitive. La psychiatrie est une erreur grotesque. Il y a des
intuitions, et ces intuitions sont très souvent des lucidités
supérieures. Tu me connais, je suis capable de tout. Et les intuitions
ne m'ont jamais trompé. Mais on me perd facilement, parce qu'on ne
transige pas avec l'esprit. Pas à ce point. C'était une descente
brutale, oui, pour se trouver être à ce point... sur le fil... que
dis-je... Être... quand c'est l'Être lui seul qui a été touché en plein
cœur... à voir la tempête de l'intérieur, et le sang qui oublie la
réalité... c'est ainsi *faisable*... Et qu'est ce qu'il en sort
finalement ? Tu le vois bien, ce qu'il en sort... parce que je parle !
Accuse-moi, de tous les torts possibles... et de tous les crimes...
accuse-moi en bloc... je l'accepte... je suis une crapule injuste... et
je préfère devenir fou maintenant... être un aliéné, oui, mais
authentique ! Ignoble sexualité des pierres... Je pense que le nerf a
assez joué... Il faut du repos, de la lumière, et parler ! Tu vois,
quand j'écris, quand je parle, de cette manière, je suis un âne, je suis
un rat... Et le sang se met à circuler correctement... et je recouvre
une bonne coordination dans le regard... Et je louche comme il faut...
Et je suis maladroit comme il faut... Et je deviens si adorable... Parmi
tous les corbeaux, choisis l'or ! Enfin ! Décide-toi bon sang ! Je ne
connais pas d'acte inerte. Même dans l'immobilité, quelque chose remue
en moi... Je n'occulte pas ces phénomènes, je les écoute, et c'est... le
remuement du rêve animal dans la moelle... Ce sont les étoiles
dispersées dans le nerf... La voici mon amie, la vie que je peux
t'offrir, ce monticule, cette dénudation épidermique... Il n'y a point
d'hermétisme inutile... je veux bien ouvrir ma tête pour toi, pour que
tu puisses mieux voir mon esprit funambulesque, penses-tu le trouver
ailleurs, quelque part dans cette vie ?... J'ai des couleurs à te
confier, des images inénarrables... Une vie météoritique, une
psychurgie... Peut-être garde-t-on ainsi trace de nos vies antérieures,
quand cette mémoire animale vient *s'organiser* dans la matière, dans le
tissu organique des vertèbres... Je pose délicatement cette magie
psychique dans tes mains pour que tu puisses y goûter, y poser ta
bouche, et que dans un effleurement tes lèvres en prennent la couleur,
de cette couleur de nacre, celle que tu peux boire, quand tu entrouvres
les lèvres, pour goûter infiniment la couleur... C'est mieux que les
images ! Oui... c'est mieux que partouzer dans les images, et y revenir,
pour encore et encore s'en repentir... de cet étalage nauséabond... La
mémoire des couleurs, elle ne trahit pas ! La totémisation de mes vies
antérieures est substantielle, elle se partage par le baiser, par le
regard, par le coït si tu veux, bien que je préfère les succions ! Je
veux te nourrir de rêves, et comme cela agrandir quelque chose dans
cette vie... cette vie insupportable dans laquelle le monde s'engouffre
tête baissée... Je veux te dire une dernière fois que ces vérités-là, je
les tiens des manques, et des heures creusées dans la matière du vide...
quand le vertige s'est manifesté en soubresauts, dans les mauvais
jours... Non ne désespère plus, et ne souffre plus maintenant, je tiens
cette vérité en moi, qui est une fleur d'amour... je suis à toi...

<p class="droite--signature">Étienne</p>
