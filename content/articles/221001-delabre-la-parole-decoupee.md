---
persona:
  - Pierre-Aurélien Delabre
title: "La Parole découpée"
slug: "la-parole-decoupee"
date: 2022-10-10
echo:
  - stratégique
  - rythmique
images:
  - "img/221001-delabre-la-parole-decoupee.jpg"
gridimages:
  - "img/221001-delabre-la-parole-decoupee-grid.jpg"
sourceimage: "https://commons.wikimedia.org/wiki/File:2015-05-01_NO_EXPO_(17332069232).jpg"
notes: "<a href=\"https://commons.wikimedia.org/wiki/File:2015-05-01_NO_EXPO_(17332069232).jpg\" target=\"_blank\" rel=\"noopener\">L'image originale</a>, de la manifestation *No Expo* à Milan en 2015, est de FEDRA Studio. L'image dérivée est de AAA. L'image du monde est une dérive du monde lui-même. Brûlons l'image, puis le monde."
summary: "[compte-rendu d'une AG de lutte se déroulant dans les tribunes d'un petit stade de football à Torpignattara, quartier périphérique de Rome] / PALMIRO TOGLIATTI / rompant / solennellement / le silence / à la veille / de sa mort / NE / PERDEZ / PAS / ESPOIR, / CAMARADES ! / IL / FAUT / SOUTENIR / LE / CHANT / DES MASSES / DRAPEAU ROUGE / EST NOTRE UNIQUE BALISE / AVEC / PROBITÉ DE NOTRE COMITÉ CENTRAL / AVANTI POPOLO ! / LE VENT EST AVEC NOUS / C'EST UNE QUESTION DE COURAGE ET DE TEMPS / NOTRE DOCTRINE DEMEURE / INVARIANTE / JE RÉPÈTE : / IN-VA-RI-AN-TE"
citation: "POUR CROIRE EN LA RÉVOLUTION IL SUFFIT DE L'IMAGINER"
hangindent: false
---

\[compte-rendu d'une AG de lutte se déroulant dans les tribunes d'un
petit stade de football à Torpignattara, quartier périphérique de Rome\]

{{%alignement-d%}}
PALMIRO TOGLIATTI\
rompant\
solennellement\
le\
silence\
à la veille\
de\
sa mort
{{%/alignement-d%}}

{{%alignement-c%}}
NE\
PERDEZ\
PAS\
ESPOIR,\
CAMARADES !

IL\
FAUT\
SOUTENIR\
:

LE\
CHANT\
DES MASSES

DRAPEAU ROUGE EST\
NOTRE UNIQUE BALISE

AVEC PROBITÉ\
DE NOTRE COMITÉ CENTRAL

*AVANTI POPOLO !*

LE VENT EST AVEC NOUS\
C'EST UNE QUESTION\
DE COURAGE ET DE TEMPS

NOTRE DOCTRINE\
DEMEURE\
INVARIANTE

JE RÉPÈTE :\
IN-VA-RI-AN-TE

SEULES\
LES ALLIANCES TACTIQUES\
SONT CONJONCTURELLES

QUANT À LA STRATÉGIE\
QUANT AU PROGRAMME\
NOUS DEVONS NOUS CONTENTER\
DE LES PRÉCISER
{{%/alignement-c%}}

{{%alignement-d%}}
CHŒUR\
représentant\
sceptique\
de\
la Raison\
prolétarienne
{{%/alignement-d%}}

{{%alignement-c%}}
*exfiltrant prosodie des vainqueurs noces brunes*\
*un refrain millénaire et bien qu'indéchiffrable*\
*soleils fourbes crachés dévalant nos massifs*\
*nos guerres ou la famine d'un peuple imaginaire*\
*d'une ancienne carence l'Histoire et ses ficelles*
{{%/alignement-c%}}

{{%alignement-d%}}
PALMIRO TOGLIATTI\
avec\
tellement\
de\
tristesse\
dans les yeux
{{%/alignement-d%}}

{{%alignement-c%}}
NE\
PERDEZ\
PAS\
ESPOIR,\
CAMARADES !\
\[redite\]

POUR CROIRE EN LA RÉVOLUTION\
IL SUFFIT DE L'IMAGINER

ALORS\
FERMONS LES YEUX,\
MES FRÈRES

ET IMAGINONS\
:

« PLAINE DU PÔ LIBÉRÉE\
NORD ET SUD RÉCONCILIÉS\
EXPROPRIATION\
DE TOUTE L'INDUSTRIE\
LOMBARDE ET PIÉMONTAISE\
QUANT À LA PETITE-BOURGEOISIE\
DU VENETO\
ELLE DEVRA SE SOUMETTRE\
PARTICIPER\
À L'EFFORT DE GUERRE\
OU DISPARAÎTRE »

IMAGINONS,\
MES FRÈRES\
IMAGINONS

ET SI JE NE SUIS PLUS LÀ\
CONTINUEZ\
À IMAGINER SANS MOI

IMAGINEZ\
POUR NE PAS PERDRE ESPOIR

LA BEAUTÉ DU MONDE FUTUR\
LA BEAUTÉ DU MONDE\
QUI VIENDRA

LIBÉRÉ DU CAPITAL\
LIBÉRÉ DE L'EXPLOITATION\
LIBÉRÉ DE LA DÉMOCRATIE-CHRÉTIENNE\
ET DE SA MAIN-D'ŒUVRE FASCISTE

CE MONDE EST CELUI\
QUE VOUS BÂTIREZ

AVEC VOS MAINS\
AVEC VOTRE CŒUR

ET IL SERA\
PLUS BEAU ENCORE\
QUE NOTRE RÊVE

MAINTENANT :\
ORGANISEZ-VOUS !

L'AVENIR EST SEULEMENT\
ENTRE VOS MAINS
{{%/alignement-c%}}

{{%alignement-d%}}
ELSA MORANTE\
agressive\
malgré\
la\
pureté\
évidente\
de son cœur
{{%/alignement-d%}}

{{%alignement-c%}}
JE N'AI D'AMOUR\
NI POUR LES MASSES INFORMES\
NI POUR LES APÔTRES\
DE LA VIOLENCE PETITE-BOURGEOISE

« MON COMMUNISME EST SANS CLASSE ET SANS PARTI »

ET JE CHOISIS\
PÉNIBLEMENT LE MONDE

« LE MONDE SAUVÉ PAR DES GAMINS »

GAMINS DE NAPLES OU DE CASARSA\
ET D'AUTRES GAMINS ENCORE\
D'AUTRES GAMINS QUI VIENDRONT\
QUE NOUS NE CONNAISSONS PAS\
QUE NOUS NE CONNAÎTRONS PEUT-ÊTRE JAMAIS

C'EST POUR EUX QUE NOUS LUTTONS

ALORS\
AYONS LE COURAGE\
DE RENONCER

AU DRAPEAU DÉCHIRÉ DE LA MÉMOIRE\
À LA DOCTRINE\
AUX VIEILLES RENGAINES\
À CE THÉÂTRE DES RADICALITÉS\
AU DISCOURS SURPLOMBANT ET STÉRILE

ET\
HONORONS\
IMMÉDIATEMENT\
:

LE CRI\
D'UN SEUL\
ASTRE\
PLOMBÉ

LA TERRE EST EN DANGER\
NOTRE SOLEIL SE MEURT

SOYONS RÉALISTES :\
L'URGENCE EST ÉCOLOGIQUE
{{%/alignement-c%}}

{{%alignement-d%}}
FRANCO FORTINI\
levant\
le\
doigt\
d'un air\
menaçant
{{%/alignement-d%}}

{{%alignement-c%}}
NE\
PAS\
FAIRE

LA RÉVOLUTION\
SEULEMENT\
AVEC DES IDÉES

CE SONT NOS CORPS\
DE POÈTES ET DE PROLÉTAIRES\
QUI GISENT

AU MILIEU DES ORDURES\
NUCLÉAIRES CARBONÉES\
MONTICULES DE PLASTIQUE\
DE SPERME ET D'OR

IL FAUT TOUT FAIRE SAUTER

DÉTROUSSONS LES PATRONS\
LES PROFS LES CONTREMAÎTRES

TOUT RECOMMENCE\
ET TOUT COMMENCE\
AVEC LE PRINTEMPS

IL FAUT TOUT FAIRE SAUTER

LA POÉSIE NE DEMANDE QUE ÇA
{{%/alignement-c%}}

{{%alignement-d%}}
CHŒUR :\
ne sachant\
plus\
manifester\
que\
négativement\
sa Raison
{{%/alignement-d%}}

{{%alignement-c%}}
*mais il s'agit de brûler Rome et non d'aménager*\
*supplice croix délices en terre conquise*\
*ce n'est pas la revanche des fils qui nous sauvera*\
*mais bien la lutte des classes inépuisable en beauté*\
*qui de toute éternité sera fonction de ton poème*
{{%/alignement-c%}}

{{%alignement-d%}}
ALBERTO MORAVIA\
sans\
que l'on sache\
malgré\
sa voix\
cristalline\
ce qu'il pense vraiment
{{%/alignement-d%}}

{{%alignement-c%}}
QU'IMPORTE

LE CHOIX DES MOTS

TOUT EST BIEN RELATIF

NOTRE AMOUR\
EST UN GRAND DÉTACHEMENT

QUANT À MOI\
J'ÉCRIS DES FABLES\
EN SONGEANT\
À LA DÉGRADATION DE MON DÉSIR

QUE VOULEZ-VOUS ?

NOUS N'EN FINIRONS JAMAIS\
DE HURLER DANS UN DÉSERT\
...\
(sourire cristalin)
{{%/alignement-c%}}

{{%alignement-d%}}
CHŒUR\
lucide\
et\
désespéré
{{%/alignement-d%}}

{{%alignement-c%}}
*quand il y a larme d'un seul crevard dans mon désert*\
*petit-bourgeois sauvage cliniquement désespéré*\
*son cri formel n'a pas le poids de classe ouvrière*\
*mais masse informe désincarnée syndicalement structurée*\
*non moins promise à la déroute que le margoul’*
{{%/alignement-c%}}

{{%alignement-d%}}
ROBERTO BOLAÑO\
avec l'air\
de vouloir\
installer\
son Église\
sur un rond-point
{{%/alignement-d%}}

{{%alignement-c%}}
LUMPEN\
=\
MALADIE INFANTILE DE LA LITTÉRATURE ?

NOUS ASSUMONS

C'EST AUSSI LIÉ AU PRIX DES LOYERS

NOS REVENUS\
NE SONT PAS SUFFISANTS\
POUR VIVRE\
DANS UN QUARTIER CHIC

EN ATTENDANT\
LE COMMUNISME

NOUS PISSONS\
SUR LA VIEILLE EUROPE

NOUS ÉCRIVONS\
DES ODES\
AUX CHIENS ROMANTIQUES

CLARTÉ MESQUINE

NOTRE AMOUR\
SE LOGE\
DANS UN ULCÈRE

DÉSESPOIR\
&\
OBSTINATION

NOUS N'EN FINIRONS\
JAMAIS\
D'

A\
B\
O\
L\
I\
R

L\
E

P\
O\
È\
M\
E

E\
N

L\
E

R\
É\
A\
L\
I\
S\
A\
N\
T
{{%/alignement-c%}}

{{%alignement-d%}}
GEORG LUKÁCS\
tirant\
névrotiquement\
sur\
sa cigarette
{{%/alignement-d%}}

{{%alignement-c%}}
NON, NON, NON\
DISTRACTION BOURGEOISE

LA POÉSIE NE SAUVERA PAS LE MONDE

HORS DE QUESTION\
D'INSTALLER NOTRE POLITBURO\
SUR UN ROND-POINT

IL FAUT ASSUMER\
GRANDEUR & VÉRITÉ\
DE NOTRE PROJET

SINON NOUS SOMMES PERDUS

NOUS,\
ENFANTS DE LA RAISON\
CESSONS DE CROIRE\
AUX ARTS DÉTRAQUÉS DE LA RÉSISTANCE

ARMONS NOTRE SENS HISTORIQUE\
AIGUISONS NOTRE ACUITÉ STRATÉGIQUE\
REFONDONS THÉORIQUEMENT LE MARXISME

NOUS LIRONS KAFKA ET JOYCE PLUS TARD\
QUAND NOUS IRONS À LA PLAGE

IL NE FAUT PAS LAISSER\
CETTE POÉTIQUE DE PUNKS ET DE GILETS JAUNES\
NOUS DÉTOURNER DE NOTRE BEAU PROJET

JE RÉPÈTE :\
L'HISTOIRE N'EST PAS\
...\
(brouhaha)
{{%/alignement-c%}}

{{%alignement-d%}}
CHŒUR\
petite foule\
de\
livreurs Uber\
sortant\
d'un cours du soir\
intitulé :\
« LA MORT DU ROMAN :\
à partir d'une lecture\
radicale\
d'*Ulysse* de Joyce »
{{%/alignement-d%}}

{{%alignement-c%}}
*ne plus écrire ne plus écrire ne plus écrire*\
*ne plus écrire ne plus écrire ne plus écrire*\
*ne plus écrire ne plus écrire ne plus écrire*\
*ne plus écrire ne plus écrire ne plus écrire*\
*ne plus écrire ne plus écrire ne plus écrire*
{{%/alignement-c%}}

{{%alignement-d%}}
WALTER BENJAMIN\
sifflant\
comme une hirondelle\
depuis\
son\
invincible printemps
{{%/alignement-d%}}

{{%alignement-c%}}
JETER BRAISE\
DANS NOS INVENTAIRES

\[...\]

COLLECTIONS\
IMAGES & CITATIONS

\[...\]

UNE PARTIE D'ÉCHECS\
AVEC BERTOLT BRECHT\
AU PIED D'UN GRAND BÛCHER

\[...\]

DIALECTIQUE EN SURSIS

\[...\]

RÉÉCRIRE SANS CESSE\
LE MÊME FRAGMENT

\[...\]

ÉPUISER NOS MYSTÈRES

\[...\]

MÉNAGER UNE PLACE\
OBSTINÉMENT\
À LA POSSIBILITÉ\
DES GRANDS RENVERSEMENTS

\[...\]

PROGRÈS CONTRE LANGAGE

\[...\]

LANGAGE CONTRE PROGRÈS

\[...\]

ORIGINE DU LANGAGE

\[...\]

LANGAGE DE L'ORIGINE

\[...\]

ETC.
{{%/alignement-c%}}

{{%alignement-d%}}
CHŒUR\
arborant\
un tee-shirt\
où\
est écrit\
au feutre noir :\
« La littérature est\
morte.\
Vive la littérature ! »
{{%/alignement-d%}}

{{%alignement-c%}}
NE PLUS ÉCRIRE NE PLUS ÉCRIRE NE PLUS ÉCRIRE\
NE PLUS ÉCRIRE NE PLUS ÉCRIRE NE PLUS ÉCRIRE\
NE PLUS ÉCRIRE NE PLUS ÉCRIRE NE PLUS ÉCRIRE\
NE PLUS ÉCRIRE NE PLUS ÉCRIRE NE PLUS ÉCRIRE\
NE PLUS ÉCRIRE NE PLUS ÉCRIRE NE PLUS ÉCRIRE
{{%/alignement-c%}}

{{%alignement-d%}}
ANONYME\
jeune\
femme\
au visage\
brûlé\
par un\
soleil\
précoce
{{%/alignement-d%}}

{{%alignement-c%}}
QUE FAIRE,\
MES SŒURS

DU RÉSIDU\
QUI CHANTE\
DANS NOS TÊTES

EN FERONS-NOUS\
DES JEUX DE MOTS\
DES COMPTINES\
POUR NOS GOSSES

COMMENT METTRE DE L'ORDRE\
DANS CE FOUTOIR

ET PENSEZ-VOUS\
QUE NOUS SOYONS ARMÉES\
POUR NOUS SOUVENIR

VIEUX MANUSCRIT\
VIEILLE RHÉTORIQUE

NOUS N'AVONS PLUS LA FORCE\
D'OUVRIR\
LE GRAND LIVRE DU TEMPS

D'INVENTER UN LANGAGE

TOUTES LES PLACES SONT PRISES\
LA MARCHANDISE RÈGNE EN MAÎTRE

ET NOS AMOURS\
N'ONT JAMAIS ÉTÉ\
AUSSI TRISTES
{{%/alignement-c%}}

{{%alignement-d%}}
FRANCO FORTINI\
prêt\
à dénoncer\
toute cette clique\
à la prochaine AG\
du prochain mouvement\
de la prochaine lutte\
décisive
{{%/alignement-d%}}

{{%alignement-c%}}
STOP\
AU ROMANTISME DE LA DÉFAITE

AUCUNE POTENTIALITÉ\
PRATIQUE ET SALUTAIRE

IL Y A URGENCE

AMAS DE GAZ ET DE MUSCLES\
CORTÈGE D'HOMMES ATOMISÉS

EN FINIR AVEC\
LES INJONCTIONS BUREAUCRATIQUES

IL FAUT\
TENIR LA LIGNE

LOIN DES COMITÉS CENTRAUX\
LOIN DES MASSES ATOMISÉES\
LOIN DES MANIFESTATIONS ORGANISÉES\
SELON UN SCHÉMA ÉTABLI

LOIN DU PACIFISME D'ELSA\
LOIN DE LA VOIX CRISTALLINE D'ALBERTO\
LOIN DU DANDYSME PUNK DE BOLAÑO\
LOIN DES ANGES PERFIDES DE WALTER\
LOIN DE LA MÉMOIRE DE TOGLIATTI\
LOIN DES BRUMES LUKÁCSIENNES\
LOIN DE TOUT ÇA

DÉ-ROMANTISER LA LUTTE

REPRENDRE LES TEXTES\
ENRICHIR NOTRE COMPRÉHENSION\
DES NOUVEAUX RAPPORTS DE POUVOIR

APRÈS MARX :\
IL Y A FRANCFORT ET FOUCAULT\
ET TOUS LES AUTRES

CONTINUER LA RÉSISTANCE\
MUTUALISER\
NOS EXPÉRIENCES DE LUTTES

Ô DIABLE LA LUCIDITÉ HISTORIQUE !

\[PA' NE DOIT PAS VOUS DÉCOURAGER\]

L'HISTOIRE N'EST PAS MORTE

ELLE BRÛLE\
ENCORE\
ENTRE NOS MAINS\
...\
(silence)

\*\
\*\
\*
{{%/alignement-c%}}

Durant toute l'Assemblée Générale, nous n'avons pas entendu Pasolini
(que tout le monde appelle Pa', ici), ce dernier ayant privilégié une
partie de football, quelques mètres plus loin, sur un terrain sec et
parsemé de gros cailloux. Entre l'équipe d'Accattone et un groupe de
jeunes bourgeois milanais, il n'hésita pas un instant : en petit veston
populaire et muni de fines chaussures en cuir, il prit place au cœur du
jeu. À la mi-temps, jetant un œil amusé vers les tribunes où débattaient
ses camarades, on rapporte qu'il balbutia : « Le rire est le frère non
reconnu du silence. » Puis, à la fin du match, se dirigeant vers les
vestiaires afin de saluer ses vieux amis, Franco, Walter et les autres,
on rapporte qu'il aurait chuchoté, de sa voix douce et musicale, dans un
mélange profane et un peu maladroit de frioulan et de romanesco : « Ici,
tout le monde semble beaucoup trop sérieux pour articuler un discours
sur la réalité. Je commencerai donc mon propre discours sur la réalité
quand j'aurai réglé mes comptes avec la grammaire. En attendant, il faut
que je prenne une douche. Ce fut un beau match. Malgré le fait que nous
l'ayons encore perdu. *Odio milanese !* »
