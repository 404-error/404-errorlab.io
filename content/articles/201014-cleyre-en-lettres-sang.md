---
persona:
  - Voltairine de Cleyre
title: "En lettres sang"
slug: "en-lettres-sang"
date: 2020-10-14
echo:
  - rythmique
images:
  - "img/201014-cleyre-en-lettres-sang.jpg"
gridimages:
  - "img/201014-cleyre-en-lettres-sang-grid.jpg"
sourceimage: "https://commons.wikimedia.org/wiki/File:Mexican_Revolution_(199).jpg"
notes: "*Written-In-Red* est le dernier poème de Voltairine de Cleyre. Cette nouvelle traduction d'Ann Persson tente de respecter la forme versifiée du texte anglais."
summary: "En lettres sang leur révolte se tient / Pour se faire aux dieux du Monde connaître ; / Sur le mur en ruine sans corps leurs mains / Ont inscrit \"Upharsin\", leurs fers carmins / Ornent le verbe : \"Prenez les terrains ! / Ouvrez les prisons ! Libérez les êtres !\""
citation: "Ouvrez les prisons ! Libérez les êtres !"
---

(À nos morts vivants de la lutte mexicaine)

En lettres sang leur révolte se tient\
Pour se faire aux dieux du Monde connaître ;\
Sur le mur en ruine sans corps leurs mains\
Ont inscrit "Upharsin", leurs fers carmins\
Ornent le verbe : "Prenez les terrains !\
Ouvrez les prisons ! Libérez les êtres !"\
S'embrasent vivants les mots des mourants,\
Écrits en lettres sang.

Vous, dieux du Monde ! Leur bouche est muette !\
Vos fusils ont parlé, ils sont poussière.\
Mais les vivants, en linceul, cœurs en dette,\
Ont senti du tambour le rythme net\
En eux la langue des morts qui s'apprête ---\
L'appel : "Brisez la rouille millénaire !"\
Et voir "Resurrexit", mot des mourants,\
Écrit en lettres sang.

Au plus haut, porte-le, flamme importune !\
Plus haut des cieux, que tous le voient paraître.\
Serfs du Monde ! Notre cause est commune ;\
Une est l'ancienne honte d'infortune ;\
Une est la lutte, et dans la raison Une ---\
Humanité --- nous libérons les êtres.\
"Vengeons la Terre", ardents mots des mourants,\
Écrits en lettres sang.

