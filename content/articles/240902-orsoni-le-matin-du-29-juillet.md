---
persona:
  - Jérôme Orsoni
title: "Le Matin du 29 juillet"
slug: "le-matin-du-29-juillet"
date: 2024-09-26
echo:
  - esthétique
  - ontique
images:
  - "img/240902-orsoni-le-matin-du-29-juillet.jpg"
gridimages:
  - "img/240902-orsoni-le-matin-du-29-juillet-grid.jpg"
sourceimage: "https://illiers-combray.com/je-decouvre/histoire-patrimoine/"
notes: ""
summary: "Le matin du 29 juillet, quelques jours après une vague de chaleur
intense que, dans le jargon de mes contemporains, on appelle canicule,
je m'assis à une table, qui devint ma table d'écriture, pour écrire.
Quoi ? Je ne le savais pas alors. Je n'avais d'autre projet qu'écrire.
Ce qui, évidemment, n'était pas un projet du tout, mais plutôt une sorte
de projet négatif, d'antiprojet. Et, m'asseyant à ma table d'écriture,
il me sembla que c'était cela qu'il fallait faire et cela qu'il fallait
poursuivre, ou chasser, ou fuir, je ne sais pas, mais la notion même de
projet. Ne souffrons-nous pas d'être toujours en avance sur l'avenir ?
me suis-je demandé. --- Et en retard sur nous-mêmes."
citation: "Le réel a déjà eu lieu."
poetry: false
hangindent: false
---

1. Le matin du 29 juillet, quelques jours après une vague de chaleur
intense que, dans le jargon de mes contemporains, on appelle *canicule*,
je m'assis à une table, qui devint ma *table d'écriture*, pour écrire.
Quoi ? Je ne le savais pas alors. Je n'avais d'autre projet qu'écrire.
Ce qui, évidemment, n'était pas un projet du tout, mais plutôt une sorte
de projet négatif, d'antiprojet. Et, m'asseyant à ma table d'écriture,
il me sembla que c'était cela qu'il fallait faire et cela qu'il fallait
poursuivre, ou chasser, ou fuir, je ne sais pas, mais la notion même de
*projet*. Ne souffrons-nous pas d'être toujours en avance sur l'avenir ?
me suis-je demandé. --- Et en retard sur nous-mêmes.

2. Désormais, toute table sera ma table d'écriture.

3. Allant dans le jardin de la maison que nous avions louée pour les
vacances afin d'y étendre sur le dossier d'un banc la serviette avec
laquelle je venais de me sécher, ce matin-là, à demi nu, une sensation
passa tout d'abord inaperçue. Je ne le compris qu'après, quand je la
perçus réellement. Quand elle arriva à ma conscience (mais est-ce ainsi
qu'il faut le dire ?), je faisais quelques pas dans l'herbe mouillée du
jardin. La rosée sous mes pieds, je ne l'avais pas sentie, mais elle
était là. N'est-ce pas le destin des sensations de venir en retard, me
suis-je encore demandé, et ce que nous appelons du nom de *conscience*,
est-elle autre chose qu'un enregistrement de la latence entre le temps
et nous ? Du retard. Nous vivons une existence qui a déjà eu lieu sans
nous. Mais nous ne lui courons pas après, non, nous la remarquons trop
tard. Après coup. C'est tout.

4. J'avais commencé d'écrire depuis quelques minutes à peine quand
Daphné vint me réclamer un *câlin*[^1]. Je lui répondis que j'étais en
train d'écrire. Elle insista, m'interrogea, je perdis mon sang-froid.
Elle retourna dans sa chambre. Je me remis à écrire, cherchant à renouer
le fil de mes phrases, rompu, comme il arrive toujours qu'il se rompe.
Et nous avec lui.

5. Il ne faut pas craindre les interruptions, il faut vivre avec.

6. Je décris ceci --- j'entends : cette scène avec l'enfant ---, qui
n'est pas à mon avantage, à ma gloire moins encore, pour montrer que je
ne cache rien. Ainsi, je serai ici sans dissimuler, sans mentir, sans
rien cacher, avec ma langue sincère. Et désormais, le serai toujours.

7. Je n'ai que faire de ma gloire.

8. Notre conscience est toujours en retard sur nous-mêmes. Elle nous
poursuit. Ce retard est-il à l'origine de la signification morale que
l'humanité a donnée à la conscience ? Ce retard est-il à l'origine du
remords ? Ce remords est-il notre origine ?

9. Le réel a déjà eu lieu. Et ce que nous appelons *la réalité* n'est
en vérité que le résidu immobile de tout ce qui bouge, de tout ce qui
passe, de tout ce qu'il se passe, tout le temps. On pourrait évoquer *le
passé*, mais il n'y a de passé qu'enregistré, perçu et aperçu. Le passé
égale la conscience. *Passé*, *conscience*, *réalité* --- tous ces mots
sont des synonymes.

10. Là où j'étais assis, devant un mur bleu gris, j'entendis un chien
qui hurlait, pleurait, me semblait malheureux. J'écoutai sa plainte
déchirante, et en fus ému. Non pas aux larmes, --- plus profondément.
Alors, l'écoutant, je notai : tout se détermine dans la peur, la peur
d'être seul, de mourir, de ne plus rien avoir à dire, de n'être pas
entendu. De n'exister plus. Est-ce la peur qui nous incite à nous
projeter dans l'avenir, à ne vivre jamais là où nous sommes, quand nous
sommes, mais à toujours chercher au contraire un temps d'avance ?
Pensant à moi-même, c'est-à-dire à tout le monde, j'ajoutai encore :
qu'est-ce qui me distingue de ce chien qui hurle à la mort, à la lune, à
ses maîtres et possesseurs, à l'absence ?

11. Nous n'aurons jamais un temps d'avance sur la mort.

12. Là où j'étais assis, devant ce mur bleu gris, je songeai à cette
peur qui est la mienne, la peur des chiens, ma peur de chien, cette peur
que personne ne m'entende plus, que personne ne veuille plus jamais
m'entendre, cette peur aussi de n'avoir été qu'un imposteur, d'avoir
fait illusion quelque temps, avec mon imposture, et d'être à présent
démasqué, et donc seul, les choses étant enfin rentrées dans l'ordre
(mais quelles choses ? mais quel ordre ?). Et alors, il me sembla que je
venais de retrouver un état que je n'aurais jamais dû quitter :
quelqu'un parle tout seul et personne ne l'entend --- personne n'a envie
de l'entendre, mais il parle, non, il écrit quand même personne ne
l'entendrait jamais, ne le lirait jamais. Qu'est-ce sinon, *écrire* ?

13. Quoi qu'il arrive, j'écris.

14. Qui suis-je, sinon qui parle au mur ?

15. "Un jour, tout ce que j'écris sera à toi", me dis-je, m'imaginant
que je parlais à Daphné. Et m'interrogeai : Est-ce terrible ou
merveilleux ?

16. Toutes les questions peuvent paraître absurdes. Comment s'en poser
une dont l'absurdité recèle quelque profondeur ?

17. Mais ne fais pas comme nous faisons toujours, --- ne fais pas comme
s'il n'y avait qu'une seule question. Et ne fais pas non plus comme si,
cette question, tu devais un jour la trouver enfin. Si jamais, par
impossible, tu la devais trouver, tu te rendrais compte qu'elle
n'appartient pas à ce genre de questions auxquelles on sait répondre.

18. Cherche ou ne cherche rien. Attends ou n'attends pas. Sois ou ne
sois pas. Impératif. Fais quelque chose.

19. Une nuit, comme gisant lisant je ne dormais pas, j'entendis des pas
dans la rue sur laquelle donnait la maison où nous passions les
vacances. Je ne me levai pas pour vérifier, mais il me sembla que
c'était les pas d'une femme qui portait des talons. Entendant cela, je
me dis qu'ils faisaient un bruit étrange, c'est-à-dire : un bruit hors
du temps. Ce bruit-là de ces talons-là aurait pu être le bruit
qu'auraient fait des talons de femme dans un roman policier du milieu du
XX^e^ siècle, mais ce jour-là, même dans cette petite ville de province,
un peu en retard, elle aussi, sur son temps, ils ne me parurent pas à
leur place. Ils faisaient un bruit étrange, mais ce n'était pas leur
bruit qui était étrange --- non, ils faisaient simplement un bruit de
talons ---, c'était autre chose. Je détournais mon attention de ces
talons pour me remettre au livre que j'étais en train de lire[^2]
quand, quelques pages plus loin, je fus soudain arrêté par une
perception que la lecture avait rejetée au second plan. Je m'arrêtai de
lire sans comprendre tout de suite ce que j'étais en train d'entendre.
Peut-être parce que ce n'était pas logique, ou pas normal, ou je ne sais
pas très bien comment le dire, même aujourd'hui, peut-être parce que je
n'aurais pas dû avoir cette perception-là. Je m'adressai à Nelly à voix
basse (Daphné dormant dans la pièce à côté) pour lui demander : "Tu
entends ce bruit ?", mais elle s'était déjà endormie. J'écoutai ce
bruit avec attention et cela ne fit aucun doute : c'était le bruit des
talons que j'avais entendu plus tôt. Je crus d'abord que c'était la même
femme qui repassait dans l'autre sens, mais ce que la lecture avait
caché à ma conscience m'apparut soudain : c'était le même bruit de
talons et, pendant tout ce temps, il n'avait pas changé, il ne s'était
pas arrêté, il avait toujours été là. Le bruit des talons ne s'était pas
éloigné pour revenir, comme quand quelqu'un passe dans une rue pour
aller quelque part et puis, après y être parvenu, s'en revient en
reprenant la rue, mais dans l'autre sens, ou arpente la rue, allant et
venant sans arrêt, dans l'attente de quelque chose ou de quelqu'un ou de
rien ni personne. Non, c'était le même bruit des mêmes talons au même
endroit. Il n'y avait eu aucune variation. Je me redressai dans mon lit,
et prêtai l'oreille avec encore plus d'attention. Le plus étrange, ce
n'était pas ce bruit *ne varietur* --- un tel bruit pouvant s'expliquer
par le fait rationnel que quelqu'un était en train de faire du surplace
sous mes fenêtres sur le trottoir d'en face, après tout, comme je ne
suis pas du coin, j'ignore peut-être que tous les lundis ou tous les
jeudis, je ne me souviens plus du jour que c'était, une femme fait du
surplace à cet endroit-là de cette rue-là ---, mais qu'on aurait dit que
quelqu'un était en train de descendre ou de monter la rue constamment.
Constamment, mais sans bouger. Ce bruit de pas, c'était le bruit de pas
de quelqu'un qui traversait continuellement la rue sans pour autant
bouger, de quelqu'un qui marchait sans marcher, se déplaçait sans se
déplacer. Comment le dire ? Comment dire l'impossible ? Moi, je ne sais
pas. Pour le dire de manière moins impossible, de l'endroit où je me
trouvais allongé à moitié dans mon lit, j'avais l'impression d'entendre
le bruit des talons d'une femme en train de monter la rue --- ou de
descendre mais, pour décrire exactement ce que je percevais depuis mon
poste d'observation, je dirais qu'elle était en train de monter la rue,
de ma gauche à ma droite en quelque sorte, même si le lit est parallèle
à la fenêtre, encore une fois, c'est la description la plus simple que
je puis faire --- et de me déplacer avec elle dans la mesure où je
n'entendais pas le bruit s'éloigner --- ce qui devrait être le cas quand
on entend quelqu'un passer sous ses fenêtres cependant qu'il monte la
rue où se trouvent ces fenêtres ---, mais continuellement le même bruit,
avec la même résonance, la même intensité, la même hauteur, toujours le
même bruit sans crescendo ni decrescendo, sans la moindre variation, ne
serait-ce que la plus infime. Or, comme je ne me déplaçais pas, moi,
enfin, du moins, je n'en avais pas la sensation, elle tourne, c'est
vrai, mais ce n'est pas ce que je veux dire, comme je ne me déplaçais
pas, moi, même en situant les choses de mon point de vue,
c'est-à-dire celui de l'observateur, le phénomène n'en était pas moins
impossible. Il était tout aussi possible. Finalement, mettant mon
courage sur mes pieds, je décidai de me lever et d'aller à la fenêtre.
Je crois que si j'avais tardé tant à le faire, préférant analyser la
possibilité d'un semblable phénomène impossible depuis mon poste
d'observation allongé, ce n'était pas à cause de la peur que je
ressentais --- j'avais bel et bien peur, mais comment n'aurait-on pas
peur devant un phénomène qu'on ne parvient pas à expliquer parce qu'il
est, selon toute apparence, inexplicable ? --- ou, du moins, pas cette
peur-là, mais parce que j'avais peur d'être déçu, et pire encore : je
savais que j'allais être déçu. Tant pis, me dis-je. Et je me levai.
J'ouvris les volets en tâchant de ne faire aucun bruit. Dans la rue,
parfaitement éclairée par le lampadaire qui se trouvait à main gauche de
l'autre côté de la rue (le côté pair), je vis ce que je redoutais de
voir : personne. Comme ce n'était pas un phénomène optique, mais
acoustique, inférai-je inconsciemment, il n'est peut-être pas étonnant
que je ne voie rien. Aussi fermai-je les yeux et écoutai-je avec la plus
grande attention. Rien. Plus le moindre bruit dans la rue. J'aurais
voulu m'assurer auprès de Nelly que je n'étais pas fou, que je n'avais
pas fait un rêve, mieux : que je n'étais pas en train de faire un rêve,
ni un cauchemar, non plus, mais, comme on l'a vu précédemment dans ce
bref récit, Nelly dormait. Respectant son sommeil, je ne dis rien. Je me
contentai de refermer les volets et retournai me coucher. J'éteignis la
lumière. Pendant quelques minutes, ainsi, dans le noir, ne bougeant pas,
respirant le moins possible, je demeurai l'oreille tendue, attentif à
déceler le moindre bruit, le moindre claquement infime du plus léger des
talons. Rien. Personne.

20. Là où j'étais assis, devant ce mur qui me parut moins bleu que
gris, ce jour-ci, sans doute était-ce l'effet de la pluie, je ne
m'efforçais pas de surmonter la peur que j'avais exprimée la veille,
j'essayais de l'abandonner, à pourrir dans un recoin du temps, un recoin
de l'univers. Peu importait où, du moment que c'était loin de moi.

21. La nuit, j'avais rêvé que je mourrais de morts dont d'autres
étaient morts déjà.

22. Question impie : Chanter faux à la gloire de Dieu, est-ce chanter
la gloire de Dieu ? C'est toute une civilisation, de fait, qui s'est
effondrée, et nous avons beau en admirer les ruines --- elles sont là,
tout autour de nous, et chaque jour qui passe, nous comprenons un peu
moins ce dont elles sont les ruines, et chaque jour qui passe, nous
devenons un peu plus des ruines ---, nous n'avons pas la moindre idée de
ce que nous pourrions faire d'autre avec. C'est là, mais à quoi bon
est-ce là, à quoi bon est-ce ? Personne ne le sait, et qui affecte le
contraire ment. Ces débris sont trop jeunes encore. Ils sonnent creux.
Et un Chinois y est plus à sa place que n'importe lequel des Européens.

23. Le matin, tout en me changeant (même modèle, différente couleur),
j'avais déplacé les petits fétiches de pierre ramassés en chemin les
jours précédents dans la poche intérieure de la poche droite de mon
bermuda. Et ces gestes --- ramasser d'insignifiants petits cailloux, les
glisser dans une poche où je pouvais les toucher avec mes doigts et les
porter avec moi --- me paraissaient avoir une importance considérable.

24. Fais attention au temps. Oui, sois tout ouïe pour lui.

25. Tous les soirs, à peu près à la même heure, un chien hurlait. Cela
durait une heure ou deux. Et puis, il s'arrêtait. Un soir, par hasard,
je n'étais pas sorti pour cela, j'aperçus le chien qui hurlait derrière
le portail en bois d'une maison. C'était une sorte de chien de traîneau,
je ne m'y connais pas en matière de chien, mais c'est ce que je dirais
de lui si on me le demandait, de la race de ceux que l'on s'attendrait à
trouver aux alentours du pôle Nord, mais pas dans une petite ville de
province à moins de deux heures de Paris, plus ou moins à l'abandon,
plus ou moins maltraité. D'un blanc tirant sur le gris, des yeux très
clairs, quand je le vis à travers deux planches un peu plus écartées que
les autres, au-dessus de l'endroit où les deux portes du portail se
verrouillaient, je tapai spontanément dans mes mains, deux ou trois
fois. Soudain, le chien cessa de hurler. Moi aussi, comme en réponse, de
mon côté du portail, je m'arrêtai un instant. Il n'y avait plus le
moindre bruit. Je compris que le chien retenait sa respiration. Et la
mienne. Comportement probable de qui attend une présence qui ne vient
pas, ou trop tard, de qui ressent la solitude, et la tristesse. Pose une
question à laquelle personne ne sait répondre. Je repris mon chemin. Et
très vite, l'entendis qui recommençait de pleurer.

26. Pense à tout ce que l'on s'inflige, s'il est vrai que, ironie de la
morale, et quand même on ne le voudrait pas croire, tout ce que l'on
inflige à l'autre, c'est à soi-même avant tout qu'on l'inflige.

27. Il m'a toujours semblé étrange, je veux dire : infiniment loin de
moi, d'acheter un chien ou de faire un enfant pour s'évertuer ensuite à
le faire souffrir. Même si je ne sais pas très bien comment l'on passe
du chien à l'enfant sans solution de continuité, je me demande : D'où
vient ce besoin de s'en prendre au plus faible pour exercer son pouvoir
de faire le mal --- pure destruction ?

28. Questions naïves, certes --- y en a-t-il d'autres ?

29. Les nuages au loin semblaient des automates digitaux. Ils
flottaient sur l'écran du néant. Meublaient le fond du champ perceptif
comme échoués sur le récif infini. Où ai-je lu un jour que, pour les
Grecs anciens, le ciel et la mer ne faisaient qu'un ? Est-ce moi qui ai
inventé cette antique croyance ? La connais-je par ouï-dire ? L'ai-je
rêvée ? Lever les yeux à la mer et s'immerger dans le ciel des idées.
Décision d'embrasser toutes les valeurs. Et de les congédier toutes.

30. Y a-t-il des questions autres que naïves ?

31. Dans la chambre à côté de celle où je m'étais installé pour écrire,
j'entendais Daphné qui jouait, enfouie en plein été sous un épais
édredon aux motifs cachemire. Je ne distinguais pas ce qu'elle disait.
Pour cela, il m'aurait fallu arrêter d'écrire, perdre le fil des phrases
que je m'efforçais de renouer. Je n'entendais de ses histoires, de ses
discours, de ses dialogues inventés, que les \[ʃ\] et les \[ʒ\]
qu'elle ne prononçait correctement que depuis peu et avec une
intonation que je trouvais si belle que, chaque fois, elle me faisait
sourire de ravissement. Dehors, dans la rue, les gens sortaient déjà les
poubelles qui seraient ramassées quelque douze heures plus tard, au
petit matin. C'était la fin de l'après-midi, et je venais de me poser la
question que voici : Si je devais n'écrire plus qu'une ligne par jour,
une ligne par mois, une ligne par an, une ligne de toute ma vie,
serais-je encore un écrivain ?

32. Y a-t-il des questions autres que pétrifiées d'angoisse ? Mais les
questions ne médusent pas, elles mettent en mouvement, ne crois-tu pas ?

33. M'en étant allé courir, comme tous les jours, pour tenter
l'impossible, à savoir : battre la mort, ou (plus prosaïquement) lutter
contre le mal par excellence de nos temps très modernes : *l'obésité*
--- trop de graisses, trop de références, trop de sucres, trop de
sources, trop de produits chimiques, trop de distance, trop d'idées,
trop de mensonges, trop de vérités, trop de tout, trop de rien, et
caetera ---, m'en étant allé courir, je revenais en longeant la rivière
qui coule non loin de la maison, quand je croisai un homme assis, les
yeux rivés sur son téléphone. Quand il m'entendit, ou sentit ma
présence, il me jeta un regard furtif, puis replongea dans son écran. Un
chiot lui était attaché par une corde. Lui aussi, il me regarda, mais il
réagit à mon apparition en allant se cacher entre les jambes de son
maître, qui lui lança un sévère et menaçant : Eh oh, tu fais quoi, là ?
que je trouvai déplacé. Le chiot aussi, qui eut peur, me sembla-t-il, et
quitta le refuge qu'il avait trouvé. Il n'y aura pas d'abri pour toi sur
cette terre, pauvre petite bête. Ce n'est pas ce que je pensais sur le
moment, mais plus tard, en écrivant seulement. Après avoir dépassé cet
étrange couple, j'eus l'intuition que l'homme, parlant ainsi à son
chien, devait parler ainsi à tout le monde, femme et enfants, s'il en
avait (pourvu que ce ne soit pas vrai), *comme à des bêtes*.

34. Nous sommes de pauvres petites bêtes.

35. Un éclair de lucidité ou une élucubration, je ne sais. Quelle
différence cela fait-il ?

36. Je connais les explosions de l'enfant, sa violence incontrôlable,
l'éruption, le refus d'un monde qui ne se plie pas à sa volonté, pire :
qui ne ressemble pas à ses désirs, qui nie là où elle s'affirme ; --- ce
sont les miennes, toutes. Et je me déteste en elle. Et je m'aime en
elle. Et cela, c'est l'existence.

37. Pas un livre pour convertir ni un livre pour divertir. Pas un juste
milieu. Un livre pour exister. Un livre impossible et inclassable, tel
que toute vie devrait l'être. Un livre classable[^3] est un livre rendu
possible par un autre, autorisé, qui peut avoir des mérites --- pourquoi
n'en aurait-il pas ? tout ne mérite-t-il pas d'exister ? ---, mais sauve
toujours les apparences, cherche à reconstituer avec lui-même et les
autres une unité, à renouer avec une existence passée, morte, finie. En
retard, tous les livres classables, toutes les vies possibles le sont.
Pas un livre pour convertir ni un livre pour divertir, un livre pour
inventer, tout inventer, rivé sur le néant.

38. Assis à ma table d'écriture, j'entendis les cloches de l'église
sonner. Église déserte, délabrée, célèbre pourtant, enfin, un peu,
hangar à touristes, enfin un peu, médiocre support de communication,
vestige fragile d'une civilisation finie, pas accomplie, comme une bête
qui souffre trop : achevée. Les civilisations s'achèvent. Elles
s'accomplissent dans la forme que prend leur anéantissement. Et ensuite,
elles passent. On s'en souvient, comme de jolies histoires à raconter
auxquelles personne ne croira plus jamais.

39. Quelle est ma civilisation ? (Pense à la civilisation de qui ose
chanter faux à la gloire de Dieu, voire de qui ne chante pas du tout.)
Me posant la question, je savais ce que j'allais y répondre : *De
civilisation, je n'en ai pas*. Ma question était donc malhonnête, mais
la réponse, elle, non pas moins vraie.

40. Le paysage tout autour de la route défilait aussi vite que les
idées, rien dans l'esprit que ce que les yeux voyaient, rivés sur
l'asphalte, le bitume fondu, durcissant ensuite, qui avait gardé traces
des passages successifs. Depuis mon habitacle vitré, je pouvais
apercevoir de petits animaux morts --- lièvres, hérissons, divers
oiseaux écrasés ou, plus gros encore, un marcassin renversé, figé là,
sur ma gauche, impotente statue, figure hiératique de l'abandon.
Découverte en passant, la nature est hostile. Et la nature humaine, ---
pire.

41. Je suis un civil sans civilisation.

42. Et tout ce que je ne peux pas dire.

43. Est-ce qu'un corps qui tombe est plus lourd que ce même corps qui
ne tombe pas ? Ou bien s'affranchira-t-il, à la faveur d'un paradoxe
grave --- le paradoxe des graves --- des lois de la pesanteur ?
Qu'est-ce que l'échec ? La chute ou le renoncement, l'exercice de
l'effondrement ou notre manque d'imagination ?

44. La nuit durant, je m'étais réveillé, saisi par la certitude de mon
échec, l'obsession d'être non seulement un écrivain raté, ce qui passe
encore (a-t-on vraiment besoin d'écrivains, réussis ou pas, a-t-on
réellement besoin d'un écrivain de plus ?), mais --- qui plus est ---
d'être un raté tout court, une sorte de parasite dont les victimes,
consentantes ou non, dormaient ici, dans mon lit même ou non loin de
moi : mon enfant et sa mère, mon épouse. J'en voulus soudain à la terre
entière, et à moi-même plus en particulier. Et puis, me rendormis. Dieu
merci, me dis-je au réveil, je ne souffre pas d'insomnie.

45. Dans le jardin public, l'enfant jouait. Je la regardais de loin.
Pour ne pas la déranger.

46. L'enfant, notai-je aussi dans mon cahier --- et cette fois, par
*enfant*, j'entendais une manière d'enfant en général, un concept, et
non pas pas une personne donnée, nommée Daphné ---, l'enfant commence
par prendre ses désirs pour des réalités. C'est ensuite, vieillissant,
que nous oublions. Et recommençons avec notre mortifère sérieux d'êtres
rassis.

47. Existe-t-il être autre que rassis ?

48. Par terre, restes de jeux d'argent, du genre de ceux que l'on
gratte pour découvrir une certaine somme ou, plus probablement, rien,
déchirés et jetés par terre. Là aussi, mégots de cigarettes écrasés, au
même endroit, à côté du jardin d'enfants qui ne voient pas encore le
monde que nous sommes en train de leur préparer. Petites fleurs jaunes
et tilleuls sur la droite. Comment supposer qu'un tel ensemble
--- bigarré s'il en est --- puisse posséder le moindre sens ? Aucun sens
n'est donné. Ne crois pas ainsi que quoi que ce soit te précède
--- c'est une illusion --- ; il n'y a que des bouts éparpillés à la
surface de ce que tu inventes. N'est-ce pas bien assez ?

49. Est-ce bien assez ? Qu'est-ce que cela, le *bien-assez* ?

50. C'était le matin. Le calme était dans la maison. Je fis couler un de
ces cafés en capsule qui peuplent désormais les cuisines de tout
l'Occident et que je ne bois que, par hasard, par malheur, par défaut,
hors de chez moi, ou pendant les vacances[^4]. J'allais penser à
quelque chose, mais au moment de, ne trouvai plus quoi. Pourquoi
employer un tel mot ? Un mot double qui, d'un certain point de vue, peut
sembler énorme, obèse, surpeuplé et, d'un autre, parfaitement vide.
Lourd, mais donc creux. C'est la capsule qui fait l'Occident, me dis-je.
C'est une communauté d'objets possédés, pas d'idées partagées. Plus
personne ne partage d'idées, on ne fait plus que prier, crier, pleurer,
ou se taire. Et mourir.

51. Vaines déplorations. Comment en serait-il autrement ?

52. Odeur d'un feu sur lequel on aurait jeté de l'eau croupie pour
l'éteindre. Depuis quelques jours, quand j'ouvrais les fenêtres, c'est
ce que je sentais, et partout autour de moi, dans le nez qui me
poursuivait, le parfum d'une terre fraîchement humide après qu'elle aura
été sèche trop longtemps, d'une aridité prolongée, et inhabituelle,
laquelle monte jusques au ciel, et tout pénètre. Odeur défunte, manière
de souterrain omniprésent. Il faut qu'il y ait de la mort pour qu'il y
ait de la vie, ce que dit une vieille croyance, là où il n'y a pas de
mort sans vie.

53. Faudrait-il alors ne pas vivre, et prôner le non-être, ne pas
écrire ?

54. Je courais à travers champs, sur les routes, vent de face, ou bien
rabattant les cheveux par derrière sur le visage, légèrement sur le
côté, de moins en moins châtaines, mais pas tout à fait blanches encore,
épaisses mes mèches, brunes encore, que je repoussais du bout des
doigts, je courais, et quand je n'en pouvais plus de courir, quand
j'étais non pas fatigué de courir, mais fatigué tout court, je
m'arrêtais, buvais l'eau fraîche, et m'asseyais à ma table d'écriture.

55. J'écrivais.

56. Dans une ville de taille moyenne, sans bien m'en rendre compte tout
d'abord, j'avais redécouvert le bruit. Bruit qui, sous la forme de
l'excès, était absent de la petite ville de province où nous résidions,
Daphné, Nelly, et moi. Ce fut l'oreille tendue, soudain, un plissement
des yeux qui contracte les sourcils quand Nelly m'adressa la parole, qui
me rappela ce qu'était le bruit, l'excès sonore. Des voitures passaient
non loin de l'endroit où nous nous étions attablés pour déjeuner,
fenêtres ouvertes, c'était encore l'été, laissant se propager les
infrabasses qui semblent être l'alpha et l'oméga de toute musique
populaire. Je me posai alors une de ces questions qu'on n'a pas tout à
fait le droit de se poser, parce qu'elles n'ont pas l'apparence de la
*démocratie*, quand même elles le seraient à défaut de le sembler :
Est-ce que l'omniprésence des infrabasses dans la musique populaire
contemporaine n'est pas en grande partie responsable de l'effondrement
de l'intelligence ? Et, en réponse à cette question, j'eusse peut-être
aimé me voir opposer un grand silence, mais il n'en fut rien.
Rien, --- que plus de bruit encore.

57. Comme il n'est pas possible de se libérer des idées des autres, et
comme il ne vaut pas la peine d'avoir des idées si ce sont les idées des
autres, avais-je écrit le matin, assis à ma table d'écriture face à la
fenêtre qui donne sur le jardin, il faut sans cesse les métaboliser,
assimiler en transformant pour assurer le fonctionnement de l'organisme
qui pense, qui vit, qui écrit.

58. Un peu après, j'avais vu la photographie d'une petite fille lisant
la constitution d'un pays devant des militaires ennemis qui la
regardaient faire, et je m'étais dit, le problème de l'instantanéité, de
l'immédiateté informatique, c'est que les légendes n'ont plus le temps
de naître. Si quelque acte héroïque avait lieu (à supposer qu'il existe
quelque chose comme "un acte héroïque"), il serait immédiatement connu
du monde entier, et aussitôt chassé par un autre, et aussitôt chassé par
un autre, et aussitôt chassé par un autre, et caetera ad infinitum. Il
n'y a plus de légende, il n'y a plus que du passé immédiat, de l'oubli
automatique, obligatoire, forcé, forcené --- qui pourrait bien se
souvenir d'une masse infinie d'informations ?

59. Il pleuvait désormais.

60. J'étais assis à ma table d'écriture, au même endroit que deux
semaines auparavant, pas très bien réveillé. Je levai les yeux et
cherchai dans le ciel au-dessus du grand marronnier au fond du jardin
une raison de continuer ou une raison de m'arrêter. Est-ce que je la
trouvai ? Sur le moment, je n'en eus pas la moindre idée.

61. Les petits animaux, sur le bord de la route, morts, finiront-ils
par se dissoudre, et faire corps avec elle, la mort avec la non-vie, les
écrasés avec le bitume, en découvrira-t-on les fossiles dans *n* milliards d'années,
ou bien n'y aura-t-il plus personne plus jamais pour
les observer ?

62. Suis-je le dernier ?

63. Qu'est-ce qu'une question, sinon une façon d'envisager le passé
depuis un avenir non advenu ?

64. Un peu comme quand on dit à un enfant rebelle à tout, *Mais
qu'est-ce qu'on va bien pouvoir faire de toi ?* Rien, justement. Du
moins, est-ce notre espoir : que, surtout, on ne fasse rien de nous.

65. Découvris-je la discipline pendant cette période de vacance ? Sa
pratique, peut-être, ce qui est la même chose.

66. --- Vacance de quoi ? --- Vacance de moi.

67. Émouvantes statues de pierre dans le parc du château, nymphe au
drapé lapidaire, ménade délicate, sphynge comme un pastel de La Tour,
souriant monstre énigmatique, petit ruban noué autour du cou, et ses
seins qui pigeonnent, délicieuse bien qu'elle soit rongée par les
lichens, vieille de tant de siècles de désirs, le regard rejeté en
arrière, qui dit oui, qui dit oui, oui ou qui nie.

68. Dans le parc du château, il y avait un banc, *le banc du
philosophe*, où je voulus m'asseoir mais ne le pus --- il pleuvait à
verse. Je m'abritai sous l'arbre qui abritait le banc. L'enfant Daphné,
juchée sur mes épaules, chantait. Ni l'orage ni le tonnerre ni les
éclairs ne semblaient l'impressionner : elle dominait l'univers,
aurait-on pu dire, ce qui n'était ni tout à fait exact ni tout à fait
absurde.

69. L'époque rase qu'il nous est donné de vivre, où les étoiles
étiolées, les élites délitées, le peuple dépeuplé, ne laissent subsister
qu'une horde sans tête qui mord, geint, se plaint, réclame, attaque,
meurt, détruit, adule indifféremment, l'époque, --- l'époque, qu'espérer
d'elle ?

70. Un espace, quelques dizaines de mètres carrés, une pièce quelque
part, pleine de livres, une table d'écriture, un lit où se reposer, d'où
ne sortir que pour jouer avec l'enfant, l'aider à grandir, faire
l'amour, exercer le corps, s'enivrer, et non plus ultra.

71. Est-ce un espoir minimaliste ? --- Comment pourrait-il en être
autrement ?

72. Il n'y a pas de phare dans le noir. Qu'un banal fanal.

73. Après la perte, se nourrit un désespoir qui n'est ni triste ni
larmes, mais doux et calme quelquefois. Il ne faut pas renoncer à la
colère, c'est-à-dire, mais comme instrument, pas comme arme de poing ni
arme de fin.

74. Qu'est-ce qui distingue une intention de son contraire ? Quand
est-ce qu'une intention devient son contraire ?

75. Le but n'est pas le but.

76. J'avais roulé deux heures, peut-être, pour me rendre à ce jardin.
Une fois arrivé, voyant le petit peuple des voitures assemblées à
l'entrée, je ne pus que renoncer. Je ne m'étais pas trompé, c'était bien
là que je voulais aller. Mais ce n'était pas cela que je voulais, ou
mieux : ce n'était pas ce là-là que je voulais, je voulais un autre là,
peut-être pas exactement un au-delà (y en a-t-il ? n'y en a-t-il pas ?
je ne le sais pas, et puis, c'est une question trop vaste ou trop
restreinte, je crois, pour qu'on y réponde comme ça, *forse che sì forse
che no*), mais pas cette chose-là, où les gens se pressent en masse.
Faire comme tout le monde, non, on ne peut pas y échapper. Chacun
partage avec le reste de l'humanité l'immense majorité de ses propriétés
--- raison pour laquelle on fait bien de parler de *nos semblables* ---,
mais ce n'est peut-être pas une raison suffisante pour pratiquer cette
cohabitation agglutinée. Le paradoxe de ce pays, aurais-je pu penser à
ce moment-là, quand je décidai, sinon de rebrousser chemin, du moins
d'aller voir ailleurs, c'est qu'il est un désert, mais qu'on y retrouve
toujours tout le monde au même endroit. Or, un lieu à habiter, un lieu à
visiter, de même, bien que cela puisse se trouver n'importe où, ce n'est
pas n'importe où. Parfois, il vaut mieux laisser place libre. Aussi, je
partis.

77. Je l'observais quelque temps. À quelque distance silencieuse. Quand
je lui demandai enfin pourquoi, tous les jours, il hissait le drapeau
tricolore dans ce mince jardin potager, il me répondit que c'était son
droit, *On est encore en France, bordel*, ajouta-t-il dans son patois,
je crois. *Vous avez raison*, lui dis-je sur mon ton à la politesse
contenue, *mais vous n'êtes peut-être pas obligé d'en souligner
l'évidence*. Ce à quoi il répliqua en pointant sur moi le canon de son
fusil. Je le saluai. Nous étions un 15 août. Pas une journée pour
mourir. Surtout pas pour une chose aussi voyante et futile qu'une
banderole bariolée.

78. Pourquoi y a-t-il tant de bêtise ? n'avais-je de cesse de me
demander. Et qu'il n'y ait pas de réponse, ce fait en était-il une
réponse en soi ? Ou bien ceci allait-il être ma façon de répondre à la
question ?

79. Est-ce bien vrai que les réponses importent moins que les
questions ?

80. Mais on ne peut pas vivre sans réponses. --- Crois-tu que tu
puisses vivre sans questions ?

81. Esprit es-tu là ?

82. Le matin du 30 juillet, je ne cherchai pas de formules. Pas la
formule. Rien de définitif. Je ne cherchai pas la vérité. J'essayai de
me lever, le plus simplement du monde. Gardai mon calme une seconde de
plus. Fus le calme même là où, la veille encore, je ne l'avais pas été,
n'avais pas su l'être. Je regardai par la fenêtre, assis à ma table
d'écriture, les collines derrière les bâtis de béton, plus ou moins
jaunes, plus ou moins roses, plus ou moins blancs, et au-dessus le ciel.
Imperméable.

83. Je repensai aux paroles de l'enfant qui m'avait demandé la veille à
peine pourquoi on ne pouvait pas *escalader le ciel*. Et ce rêve
ancestral, ce rêve enfantin, ce rêve de l'humanité, Icare, Jacob, et
compagnie, je n'avais pas envie de le laisser se détruire. J'avais envie
de le maintenir en vie.

84. Cela serait-il mentir alors ? Je ne voulais pas détruire le rêve de
l'enfance. Je ne voulais pas lui dire qu'*on aurait beau escalader le
ciel, mon amour, on n'y trouverait rien de ce que l'on y cherche*. Cela
serait-il qu'il n'y a rien à trouver alors ? Sans doute pas, non. Plutôt
qu'on cherche dans le ciel quelque chose qui ne s'y trouve pas, quelque
chose qui ne se trouve nulle part, et dont on a tant besoin pourtant, un
lieu, unique --- pourquoi en faudrait-il plusieurs ? ---, on cherche
dans le ciel ce qui ne s'y trouve pas, et ne se trouve nulle part.

85. Je me demandai : Qu'est-ce qui n'est pas mort ? Et cela voulait
moins dire : Qu'est-ce qui subsiste ? que : Qu'est-ce qui flotte
encore ?

86. Je me dis : Imaginons un cadavre --- quoi d'autre imaginer à la
surface ? ---, verrions-nous, au-dessus de sa chair pas encore putride
en tout, quelque chose survoler ? Exister là-dessus ?

87. Exister là-dessus, qui peut le souhaiter ?

88. Est-ce là ce qu'on appelle *exister* ?

89. Qu'appelle-t-on *exister* ?

90. N'attends pas de moi quelque réponse définitive. (Ni même quelque
définition.) Nous en avons fini de poser des choses dans l'être. Nous
préférerions les ôter plutôt, mais à quoi bon ? Et puis, qui sommes-nous
pour vouloir, pour poser ou ôter, pour parler d'exister ?

91. Qui sommes-nous pour être ?

92. Il faisait chaud, comme éternellement chaud. Dans les yeux de
l'enfant, je vis une vie que je n'aurais pu prévoir sans elle, sans sa
présence étrange, évidente et inouïe, banale et inédite, comme tout ce
que font les humains, sans le savoir souvent. Et c'était pour cela, pour
cette vie, pour cet imprévisible-là, pour cette vie imprévisible que je
l'aimais, et que, si haïssable fût-elle, j'aimais la vie.

93. Qui sommes-nous, nous qui ne sommes personne ?

94. Pas un geste. (La chaleur, peut-être.) Personne ne bouge. Tout le
monde garde le silence. Est-ce le fantasme du temps mort, une image
bizarre que tu projettes sur un espace décharné, le charnier du monde,
le charnier qu'est le monde ?

95. Je n'ai pas de vision, tu sais, dis-je à l'enfant en réponse à
l'une de ses innombrables questions. Toute mon énergie se concentre sur
une seule activité, ou non-activité, ou moins-activité, je ne sais
comment dire, bref, je le dis : ne pas trop manger. Garder l'estomac
léger pour ne pas sombrer trop vite, ne pas gonfler, enfler, éclater
comme un ballon plein de graisse.

96. --- Pourquoi est-ce que tu racontes cela ? Qui crois-tu
intéresser ? --- Ceux qu'intéresse le destin de l'humanité. (Assertion
modeste.)

97. En un clin d'œil, un battement d'ailes, un tour sur le grand
cadran, l'être humain est passé de la sous-alimentation à la
suralimentation, de la question : Où vais-je trouver de quoi subsister ?
à la question : Comment vais-je faire pour ne pas trop manger ?

98. Et moi qui suis là, écris, quel rôle est-ce que je joue dans cette
étrange économie ?

99. Économie de l'étrange.

100. Deux façons de mourir de faim : trop ou pas assez.

101. Qu'est-ce qui subsiste ? Qu'est-ce qui subsiste sinon ce qui
flotte ? Le reste est destiné à s'enfoncer, couler, toucher le fond,
sans chance d'un coup de pied qui relance vers la surface.

102. Qu'est-ce qui subsiste sinon flotter ?

103. Les choses qui flottent ne se définissent pas, ne se saisissent
pas, ne se contemplent pas, ne s'admirent pas, --- on s'y baigne, c'est
l'atmosphère, la mer, le monde élevé à la condition légère.

104. Le contraire de l'époque.

105. Je regardais l'époque et me disais : Mais que fais-je ici ? Et, la
posant, je ne savais pas si cette question, si ce sentiment, si cette
façon d'être au monde, c'était le monde qui la causait ou si je l'aurais
vécue quand même quelle que fût l'époque ?

106. --- Tout cela, n'est-ce pas moi ? --- Mais c'est qui, moi ?

107. Plus tard, l'enfant expliquerait comment tout cela, désignant d'un
geste ample le vaste univers autour d'elle, et les Égyptiens et les
Grecs, comment tout allait revenir, mais que ce serait triste parce que
nous ne serions pas là pour les voir. Et quand moi, je lui demanderais :
Et toi, reviendras-tu aussi ? L'enfant, le regard perdu dans l'horizon,
me répondrait : Oui. Alors, ce ne sera pas triste, pensai-je, sans le
lui dire.

108. À présent, je voudrais appeler *légèreté de l'esprit* --- un état
atmosphérique, une météo sans logis, le temps qu'il fait sans nul besoin
de prévisions, une manière d'aimer la continuité. Une attention.

109. À présent, je voudrais appeler *légèreté de l'esprit* --- cet état
des choses quand elles s'allègent, le mode conscient du monde qui sait
qu'il n'existe pas. Exactement comme n'ont jamais existé les êtres qui
ont peuplé les fables, les romans, les traités, les essais, les codes,
les théories, les constitutions, toutes les œuvres de l'esprit.

110. Les œuvres de l'esprit sont-elles l'œuvre des esprits ? (Ne
cherchons pas à répondre à la question.)

111. Ou comme si tous les êtres allaient revenir encore et encore et à
l'infini.

112. Pourquoi vouloir mettre des êtres à la place des êtres qui ont
cessé d'être crus ? Faut-il donc que les mentalités soient aussi peu
primitives ? Ou bien qu'elles ne changent jamais ?

113. *Légèreté de l'esprit*, ce ne sont que des mots parmi d'autres.

114. Et tout n'est que mot parmi d'autres.

115. Qu'est-ce qui fait que nous choisissons des mots parmi d'autres ?
Des mots plutôt que d'autres ? Des mots plutôt que zéro ? Des mots
plutôt que le silence ?

116. De quoi nous autorisons-nous pour rompre le silence ? (Écoute bien
cette expression : *rompre* le silence.)

117. La plupart du temps, le problème vient de cela que tu ne penses
pas avec tes oreilles. Imagine au contraire la pensée localisée dans ton
esprit, ton cerveau, que sais-je ? Comme si tu t'arrêtais avec la
terminaison de tes doigts.

118. Élargis-toi.

119. Et ces mots de l'enfant qui grandissait, plus sauvage, plus
profonde, plus civilisée, ne valait-il pas mieux que n'importe quel
silence ?

120. Chut.

121. Ce n'est pas que le silence soit particulièrement beau --- le
silence n'est pas plus beau que le bruit ---, non, mais qu'est-ce qui
fait que nous nous décidions à le rompre ? Pourquoi nous disons-nous, à
cet instant, qu'il ne doit plus y avoir de silence ? Parce que le simple
fait de le dire, c'est le faire et qu'on peut le faire sans le dire,
sans penser à mal, je veux dire, sans penser du tout, je veux dire, sans
même ne rien dire ?

122. Au loin, un type fait hurler le moteur de sa moto en pleine nuit.
À ce moment précis.

123. Pourquoi ?

124. Pourquoi sa machine plutôt que ta voix, mon amour ?

125. Des morceaux tombés de l'univers, des pièces rapiécées de
l'universel, dirait-on. Cela est-il bien vrai ? Sommes-nous seulement
des atomes ? Avons-nous quelque forme ? Ne sommes-nous pas que nuages,
formations gazeuses, apparitions brumeuses avant disparition prochaine ?

126. Pourquoi faut-il toujours que quelqu'un brise le silence ?

127. Pourquoi faut-il toujours que quelqu'un rompe la suite dans les
idées ?

128. Pourquoi faut-il toujours que quelqu'un ait une idée ?

129. Les nuages sont propices au rêve.

130. Est-ce à dire qu'il faut renoncer à la clarté ?

131. Je n'aime rien tant que ce ciel, bleu pur, l'hiver, sur les rives
de la Méditerranée. On croit alors pouvoir toucher au fond, le fond du
ciel, le bout du monde, la fin de l'univers. Sauf qu'il n'y en a pas.
C'est très clair. Le regard se perd, sur la surface parfaite de
l'étendue de l'univers[^5].

132. Les nuages sont des apparitions.

133. Le ciel bleu, la toile, la surface --- perfection où tout se peut
manifester.

134. Le ciel bleu, le voilà, mon ethnocentrisme.

135. Peuple des nuages.

136. Ne regarde pas les choses en face. Préfère les surfaces, les
manifestations, les apparitions, les disparitions, les mouvements aérés,
qui n'ont presque l'air de rien. Préfère quelque chose qui te fait du
bien. Ou bien rien.

137. Y a-t-il simplement des choses ? Rien n'est moins sûr.

138. À présent, je voudrais appeler *légèreté de l'esprit* --- une
sensation dans l'estomac pas trop plein, pas trop vide non plus, un
corps en équilibre.

139. Nous sommes trop ancrés, plantés ; non que nous possédions trop de
choses --- là n'est pas le problème ---, non que ce soient les choses
qui nous possèdent, --- nous sommes obsédés par les choses mêmes,
l'authentique, le réel, le vrai, l'en soi.

140. N'importe quoi.

141. Nous sommes trop proches des êtres. Si proches que c'est à se
demander si nous en sommes. N'en sommes-nous pas plutôt le parasite
capricieux, la bestiole qui se nourrit de l'être, lequel en vérité nous
est étranger, des non-êtres acharnés à dévorer la dépouille de l'être
que nous venons d'assassiner ?

142. Un tiens vaut mieux que deux tu l'auras.

143. Et jusqu'à quel point comprenons-nous quelque chose ?

144. Et si tout, depuis le début, se fondait sur une vaste
incompréhension ? Vaste comme l'univers.

145. Et si tout était parti, au début et puis après aussi, chaque jour
renouvelé, d'une incompréhension fondamentale ?

146. Qu'est-ce qu'il a dit ? Je n'ai pas compris. Je vais faire comme
si. Et ainsi de suite, à l'infini, et aujourd'hui encore.

147. L'infini n'existe pas. Il s'arrête aujourd'hui. C'est simplement
ce qu'il se passe quand on ne cesse de faire n'importe quoi.

148. Histoire naturelle de l'infini en acte. À la différence de
l'infini en puissance que personne n'expérimentera jamais.

149. La réalité est composée à 80% de bêtise. La plupart du temps, tu
es trop occupé pour faire la différence. Alors, tu fais semblant, tu
composes, tu t'accommodes, tu fais comme si. Alors la réalité est
composée à 80% de bêtise. Et ainsi de suite. À l'infini.

150. Mais 20% de quoi ?

151. Si tu regardes dans le vide, le vide, en échange, lui, ne
s'intéresse pas à toi.

152. Le regard de l'enfant porterait loin quand elle adresserait son
chant pour les étoiles, la nuit, fenêtre sur un dehors ouvert à elle. Et
je me demande : Qu'est-ce que j'appelais *légèreté de l'esprit*, déjà ?

153. La réalité est furtive. L'image figée que l'on s'en forme est une
illusion, une simplification, une exagération, dépend du point auquel
nous sommes disposés à aller pour faire semblant. La réalité passe
inaperçue. Quelquefois, quelqu'un en aperçoit une mèche de cheveux.
Alors, on s'excite, s'extasie, exagère, montre du doigt, crie : *Là, là,
là, le réel ! Il était là ! Je l'ai vu !* Mais en vérité, c'est déjà
passé. On a un temps de retard, trop long. Nous sommes trop lents, nous
qui poursuivons quelque chose qui n'existe pas, mais en quoi nous
croyons toutefois. Comment disais-tu, déjà ? --- Bêtise ? --- Je ne sais
plus.

154. Le vide n'a pas d'œil.

155. Ce retard, quand commence-t-il ? Quand nous nous apercevons que
les choses se font sans nous, quand nous percevons avec une netteté
parfaite toutes ces choses qui n'ont pas besoin de nous, qui se passent
de nous --- et que, de toutes les choses, aucune n'a besoin de nous ?

156. --- Et moi, tout ce que je fais, n'est-ce rien ? --- Pas
grand-chose, en effet.

157. Voir l'étendue des possibles se réduire, c'est adopter le point de
vue du monde, --- une certaine étroitesse, si vaste qu'il puisse
paraître. Et accepter que l'étendue des possibles se réduise, n'est-ce
pas cela qu'on appelle *vieillir* ?

158. Le point de vue du monde, c'est le réalisme : les choses sont
comme elles sont, et il faut faire avec.

159. Comme quelqu'un qui s'imaginerait avoir fait quelque chose après
avoir décrit les choses.

160. Décrire les choses, c'est-à-dire : énoncer devant soi le point de
vue qui est le sien.

161. Le réalisme est par essence tautologique.

162. --- Est-ce à dire que tout autre point de vue est par essence
contradictoire ? --- Faut-il donc que tu sois toujours si binaire ?

163. Pour échapper au réalisme, celui qui ne s'en satisfait pas, celui
qui ne se satisfait pas que les choses soient comme elles sont, un point
c'est tout, que les choses soient des choses, répondra, par exemple, aux
questions qu'il se pose par d'autres questions. À la façon d'un moine
bizarre.

164. Écris des poèmes comme autant de questions supersoniques qui
ralentissent le mouvement.

165. Écris des contes fantastiques qui détraquent les récits linéaires.
Milite pour l'étrangeté. Que tes slogans soient des énigmes. Manifeste
pour l'indétermination. Reste suspendu aux lèvres de la sphynge.
Embrasse-la avec la langue.

166. Le monde uniforme, lisse, et morne. Je voulais dire : le monde
difforme.

167. Puisque tout coule, sois une source, pas un récipient.

168. Sois une source, même tarie. Car rien ne saurait te protéger de la
défaite.

169. Sur le mur en face de toi, du bout du doigt, trace un carré.
Prends un peu de recul, fais un pas en arrière ou exécute un mouvement
de retrait du buste, regarde : Que vois-tu là ? Rien ? Un espace vide ?
Une *terra incognita*, un territoire inexploré ? Tu vois, nos notions
sont excessives, si nous devions leur donner une personnalité, nous
dirions peut-être qu'elles sont "imbues d'elles-mêmes". Sans parler de
notre incurable exotisme postiche, aux relents terreux de spiritualité
pour Occidentaux bronzés. Tu peux tout voir, tout dessiner, tout
inventer, ici et maintenant. Mais alors, pourquoi ne le fais-tu pas ?
Combien de fois par jour t'autorises-tu à prendre une initiative ? Tu
attends des ordres nouveaux pour vivre une vie nouvelle. Avec des ordres
anciens, ta vie sera toujours la même. La preuve ? Que suis-je en train
de faire sinon de te donner un ordre ? Pense par toi-même, espèce de
minable ! Aie le courage de te servir de ton propre entendement ! Éteins
la lumière ! Ferme ta gueule !

170. Dans le ciel, ce matin après l'orage, je n'avais pas l'impression
qu'il y eut des nuages, plutôt une épaisse couche de brume humide, un
tapis gris collé au plafond de l'esprit du monde, la réponse
méditerranéenne à la pureté du ciel bleu.

171. Nuages, mon peuple.

172. Tout est une question de climat. Ne crois pas qu'il soit toujours
le même. Tout temps est variable. C'est toi qui fabriques l'uniformité,
de peur de ne plus t'y retrouver. Il y a aussi peu d'ennui dans le ciel
bleu que dans le ciel gris.

173. Des poèmes déguisés en aphorismes. Des aphorismes travestis en
vers. Et le monde, à l'envers.

174. L'artifice de l'intelligence.

175. La seule logique, c'est qu'il n'y a pas de logique.

176. Des conceptions te précèdent, oui, mais cette précession n'est pas
une préséance. Tu n'étais pas là de toute éternité, et ne seras pas ici
pour toujours.

177. Tout change, tout le temps. C'est déprimant, c'est vrai. Mais
c'est surtout vrai.

178. Tu peux être une source, ou un puits sans fond.

179. Qu'il y ait une logique, c'est ce que je veux dire, qu'il y ait
une logique ou qu'il n'y ait pas de logique, qu'est-ce que cela change ?
Tout dépend pour quoi tu penses : pour avoir raison, pour démontrer
quelque chose, pour persuader quelqu'un de la vérité, ou bien parce que
tu ne peux pas t'en empêcher ?

180. La logique n'anticipe pas ; la logique dérive de l'activité.

181. La logique ne prendra pas soin de toi. La logique ne prend soin de
rien ni de personne. Ce n'est pas la logique qui pense. Ce ne sont pas
les règles qui pensent.

182. Ce ne sont pas les lois qui font la loi.

183. L'inauthenticité du langage est inéluctable.

184. Il y aura toujours quelqu'un pour te voler la vedette, le dernier
mot, le premier rôle. Si tu tends l'oreille, tu l'entendras : tout le
monde parle pour ne rien dire.

185. Le silence est plein de bruit.

186. Pourquoi continuer de parler ?

187. Tombe amoureux du nihilisme, pas comme une doctrine, c'est-à-dire,
mais une personne avec laquelle tu pourrais vivre, et ne plus jamais te
sentir seul.

188. Tombe amoureux du nihilisme, et puis quitte-le.

189. Sans regret. Comme toujours tu l'as fait. Comme tout le monde le
fait. Sans toi.

190. Il faut le voir pour le croire.

191. Tout le langage est annulé. On l'a trop parlé.

192. Il ne sert à rien d'inventer un nouveau langage, qui serait plus
neuf, mieux à même de dire tout ce que nous ressentons. Le langage est
usé. C'est ainsi qu'il fonctionne, par usage, par usure. On peut faire
tous les efforts que l'on veut pour se l'approprier, il n'appartient à
personne.

193. À quoi bon parler si c'est pour parler une langue qui n'est pas la
mienne ? À quoi bon parler si c'est pour parler la langue des autres ?

194. La langue commune, la langue du grand nombre, impersonnelle.

195. Dis-toi bien ceci pourtant : chaque fois que tu accuses le
langage, tu cherches le coupable idéal. Mais il n'y en a pas. C'est
comme si tu reprochais à ta mère, laquelle n'est plus, tout ce que toi,
tu n'es pas.

196. Fais avec ce que tu as. Mais fais-le bien.

197. L'absence d'originalité n'est pas causée par un outil défectueux,
ni même par un défaut d'outil, elle est culturelle ; les gens n'aiment
peut-être pas marcher au pas --- disent-ils ---, mais ils en ont besoin.
Sinon, ils ne sauraient pas où aller. Tel est le plus profond de leurs
désirs.

198. Aussi, l'usure du monde (des paroles et des choses) est-elle une
aubaine. Déjà dites, racontées, et décrites, les choses n'ont plus qu'à
être répétées. Ainsi, tout, tout le monde finit par se ressembler. C'est
la naissance des nations, l'invention de l'humanité, à force
d'uniformité.

199. Y a-t-il un esprit dans les marges de l'utilité ?

200. Pourquoi y a-t-il toujours quelqu'un pour parler quand personne
n'a plus rien à dire ?

201. Tu peux croire que tout est une question d'équilibre. Mais il est
toujours rompu. Cours-tu, par suite, après quelque chose qui n'existe
pas ? T'épuises-tu en vain à fabriquer quelque chose qui se voie sans
cesse détruit ?

202. Les choses ne perdent jamais la face.

203. On a beau pousser toujours plus loin la limite du semblable,
qu'est-ce que cela change si, quand tu te regardes, tu ne vois rien ?

204. Quand il n'y aura plus d'autre, cela reviendra au même.

205. Qu'à la fin, il n'y ait plus d'autre, tel est le sens de
l'histoire.

206. Le sens de l'histoire, le sentiment, c'est-à-dire, qu'une fois
accomplie la tâche que nous nous sommes fixée, nous pourrons enfin nous
reposer.

207. Depuis que mon chien est devenu une personne comme une autre,
est-ce que je me sens mieux ? Puis-je enfin me reposer ? Et pourtant, ne
l'entends-tu pas, là dehors, qui hurle à la mort ? C'est sa façon à lui
de t'aimer, et toi, tu n'as pas envie de l'écouter.

208. Le repos, voilà le sens de l'histoire. (Comme la retraite, le sens
de la vie.)

209. Que veut qui ne veut pas se reposer ?

210. La légèreté de l'esprit ?

211. Les matins ressemblaient à des matins. Je me levais, j'écrivais.
J'aurais voulu disparaître, je crois, dans l'épaisse couche de texte sur
laquelle je me tenais, j'aurais voulu qu'elle m'absorbe, le corps qui se
putréfie. Et puis, plus tard, des siècles peut-être, pousser comme un
champignon.

212. Les matins ressemblaient aux matins, mais je n'avais pas envie de
mourir.

{{%alignement-d%}}

*Combray, 29 juillet 2019 - 13 août 2024*

{{%/alignement-d%}}

 

[^1]: À cette époque, Daphné encore toute jeune, elle n’avait que trois ans et demi, ne disait pas "un câlin", mais simplement "câlin", et il me semble qu’il lui arrive encore de s’exprimer ainsi, dans une sorte de langage minimaliste, augustinien.

[^2]: Cet été-là que, comme cet été, nous avons passé à Illiers-Combray, je m’en souviens, j’avais mis un point d’honneur à ne pas lire Proust. J’avais emporté avec moi plusieurs romans de Dostoïevski, si je ne dis pas de bêtises : *les Démons* et *les Frères Karamazov*, que donc j’ai lus, cet été-là.

[^3]: Qu’est-ce qu’un "livre classable" ? Un livre dont on sait déjà, avant même de l’avoir ouvert, l’usage que l’on en fera. C’est-à-dire : un livre pour lequel on n’a pas d’usage, mais un livre usager. Évidemment, si seuls les livres souffraient de ce genre de défaut, nous n’aurions pas à nous plaindre, mais les livres ne sont qu’une image partielle de notre existence tout entière.

[^4]: Cette année, cinq ans plus tard, confronté de nouveau à cette sensation de l’infect, et au sentiment qui va avec, j’ai renoncé à boire du café sous cette forme. (Est-ce à dire que le goût s’affine, la personnalité s’affirme ?) Mieux vaut se priver de la chose plutôt que de se contenter de son ersatz. De toute façon, je le note en passant, peut-être un peu à la légère, elle nous échappe, elle n’est pas pour nous, la chose même.

[^5]: Depuis, j’ai quitté la Méditerranée. Mais je crois, cette couleur, ce n’était pas une question de géographie, c’était, si j’ose employer cette expression quelque peu triviale, une question d’état d’esprit, dont *la légèreté de l’esprit* n’est pas le nom de code, mais la version explicite. Dans l’air pur de l’hiver méditerranéen, on a l’impression de voir le bout du monde, traversant la mer au regard, l’autre rive.
