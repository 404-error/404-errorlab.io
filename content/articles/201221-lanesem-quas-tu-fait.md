---
persona:
  - Joséphine Lanesem
title: "Qu'as-tu fait ?"
slug: "quas-tu-fait"
date: 2020-12-21
echo:
  - rythmique
images:
  - "img/201221-lanesem-quas-tu-fait.jpg"
gridimages:
  - "img/201221-lanesem-quas-tu-fait-grid.jpg"
sourceimage: "https://commons.wikimedia.org/wiki/File:The_Body_of_Abel_Found_by_Adam_and_Eve_by_William_Blake_c1826_Tate.jpg"
notes: ""
summary: "Qu'as-tu fait de tes frères ? Des poissons, myriades et minimes, en circonvolutions, fugaces et fluides, dissemblance dans l'argenté, égalité dans le bariolé, les yeux toujours ouverts et jamais fixes, écailles pareilles à des papilles, respiration pourvoyant propulsion, qu'as-tu fait de leur curiosité creusant le récif, poursuivant le sillage, de leur connaissance d'un langage plus profond, d'échos et de répercussions ? Empalés, asphyxiés, égorgés."
citation: "Parqués, torturés, exterminés."
---

Qu'as-tu fait de tes frères ?

Des poissons, myriades et minimes, en circonvolutions, fugaces et
fluides, dissemblance dans l'argenté, égalité dans le bariolé, les yeux
toujours ouverts et jamais fixes, écailles pareilles à des papilles,
respiration pourvoyant propulsion, qu'as-tu fait de leur curiosité
creusant le récif, poursuivant le sillage, de leur connaissance d'un
langage plus profond, d'échos et de répercussions ?

Empalés, asphyxiés, égorgés.

Qu'as-tu fait de tes frères ?

Des oiseaux, de leur ciel dans tes veines, de leur appel au meilleur de
toi-même, de leur odeur d'averse réveillant l'allégresse et de leur
fulgurance vers l'énigme d'aller, voici tes maîtres en aviation et en
divination, tes ancêtres dans l'art de construire et séduire, qu'as-tu
fait de ces frères aussi oisifs qu'affairés, tout émerveillés d'être ici
et déjà voulant être partout ailleurs, bondissant à tes miettes,
effrayés par tes pas, répondant à ta voix ?

Fusillés.

Qu'as-tu fait de tes frères ?

Des essaims, des meutes et des troupeaux, familles maintes et multiples,
de leur entente subtile et synchrone du danger et du jeu, du pelage de
la mère, des culbutes des cousins, de la lutte entre pères, des pattes
et des sabots qui partageaient ta terre, des langues ruminantes ou
rapaces, des queues qui orientent l'oreille et des dos épousant
l'horizon, et de leurs territoires, humus de leurs histoires, et de leur
solitude aux confins, comblée d'immensité ?

Parqués, torturés, exterminés.

N'entends-tu pas leurs gémissements ? N'entends-tu que les mots ?

Qu'as-tu fait de tes sœurs ?

Des plantes qui, pour toi, transmuèrent l'eau et la lumière en chair,
des fleurs qui s'offrirent fruits, du bois qui devint toit, de l'air qui
se fit souffle. Leur tendresse émut la pierre, leur grâce ajoura la
pesanteur. Dis-moi, qu'as-tu fait de tes sœurs, harmonieuses jusque dans
l'anarchie ? Des coraux, des algues, des bulbes et des graines, des
herbes bonnes et mauvaises, des arbres grands et petits, qu'as-tu fait ?
Je ne les vois plus. Seule à l'étoile, tu n'as pu faire de mal. Pas
encore. L'espace porte déjà ta marque.

Que reste-t-il d'elles ? De leur sagesse d'être ensemble, en réseaux et
rhizomes, d'être fines, complexes et plurielles ? As-tu oublié leur
intelligence qui te précède, qui t'invente ? Retrouve-la dans tes nœuds
et tes méandres, dans l'amour qui t'écartèle, dans toute pensée qui te
déploie au-delà de toi-même.

Et de tes enfants, qu'as-tu fait ?

De leur espérance, de leur confiance, de l'avenir que leur promettait
ton sourire, de leur prescience que n'occultait aucune présomption ? Et
l'enfant que tu fus et qui savait, par conviction native, que tout ce
qui est en a le droit, la destinée, la dignité, l'as-tu abandonné ?

Es-tu assez seul maintenant pour commencer à penser ? Es-tu allé assez
loin dans le désert pour arrêter de te distraire ? Ne te tourmente pas,
l'œil de Dieu ne te poursuit pas, et bientôt plus aucun regard ne
répondra au tien, qui à son tour s'éteindra, consumé par le soleil, tout
ce qui restera. Aucun châtiment ne t'attend que celui que tu t'es
préparé. Ce néant que tu répands autour de toi t'encercle lentement.
