---
persona:
  - Marco Denevi
title: "Un chien dans la gravure de Dürer intitulée Le Chevalier, la Mort et le Diable"
slug: "un-chien"
date: 2024-07-17
echo:
  - esthétique
  - rythmique
images:
  - "img/240701-denevi-un-chien.jpg"
gridimages:
  - "img/240701-denevi-un-chien-grid.jpg"
sourceimage: "https://commons.wikimedia.org/wiki/File:Albrecht_D%C3%BCrer,_Knight,_Death_and_Devil,_1513,_NGA_6637.jpg"
notes: "Ce texte de 1966 de Marco Denevi a été traduit par [Bernard Bourrit](/persona/bernard-bourrit). Après avoir contacté les maisons d'édition détentrices des droits des textes de Denevi, et constaté une apparente déshérence quant à celui-ci, nous nous permettons de proposer ici sa traduction libre. La licence de libre diffusion ne s'applique qu'à cette traduction et non au texte original."
summary: "Le chevalier (on le sait tous) revient d'une guerre, celle de Sept Ans, celle de Trente Ans, celle des Deux Roses, celle des Trois Henri, une guerre dynastique ou religieuse, ou peut-être galante, dans le Palatinat, aux Pays-Bas, en Bohême, peu importe où, peu importe quand, toutes les guerres ne sont que les éclats d'une unique guerre, toutes les guerres forment la guerre sans nom, la guerre tout court, la Guerre, ainsi le chevalier revient d'un périple à travers l'un des éclats de la guerre"
citation: "la guerre manque d'imagination et recycle ses vieux tours"
poetry: false
hangindent: false
---

Le chevalier (on le sait tous) revient d'une guerre, celle de Sept Ans,
celle de Trente Ans, celle des Deux Roses, celle des Trois Henri, une
guerre dynastique ou religieuse, ou peut-être galante, dans le
Palatinat, aux Pays-Bas, en Bohême, peu importe où, peu importe quand,
toutes les guerres ne sont que les éclats d'une unique guerre, toutes
les guerres forment la guerre sans nom, la guerre tout court, la Guerre,
ainsi le chevalier revient d'un périple à travers l'un des éclats de
la guerre, mais c'est comme s'il avait traversé toutes les guerres et
toute la guerre, car toutes, bien que différentes de près, répètent, de
loin, les mêmes horreurs et les mêmes fureurs, ne nous embarrassons donc
pas des dates ni des noms, n'ayons pas de scrupules à tenir les
Plantagenêt et les Hohenstaufen pour une même famille indisciplinée, pas
de scrupules à mélanger lansquenets et grenadiers, arbalétriers et
arquebusiers, ou à brouiller la géographie en amalgamant les villes avec
les villes, les châteaux avec les châteaux, les tours avec les
tours --- retournons au chevalier qui, comme je l'ai dit, revient d'une
guerre, revient d'un maillon de cette chaîne appelée « la guerre », il
croit que c'est le dernier maillon, mais il ignore que la chaîne est
infinie, ou finie mais circulaire, et que le Temps l'égraine sans fin,
parti jeune et fringant, la guerre l'a rendu vieux, chauve et maigre, et
cela n'est pas surprenant, la guerre manque d'imagination et recycle
ses vieux tours, en sorte que le chevalier, comme tous les chevaliers
ayant esquivé la mort, porte une longue barbe, pue la crasse, dégage une
odeur de sueur, de sang et de boue, et ses aisselles sont infestées de
poux, et sa peau entre ses cuisses l'irrite comme une brûlure, il crache
sans cesse une salive verdâtre striée de filaments violacés, parle
d'une voix enrouée par le froid, par les feux, les ivresses, les
jurons, les cris de terreur et de courage, et ne peut prononcer deux
mots sans que l'un soit un blasphème, ayant depuis longtemps oublié le
langage fleuri qu'il employait lorsqu'il était enfant et servait en
tant que page à la cour d'un margrave ou d'un archevêque, il a oublié
les gestes élégants et les gracieuses révérences dont il gratifiait les
dames autrefois, et désormais il ne demande plus d'amour aux femmes,
mais du vin, de quoi manger, un lit, et tandis que les soldats violent
les jouvencelles, lui, il boit, seul et taciturne, et quand les soldats
réapparaissent en bâillant, il tape du poing sur la table et maudit les
roitelets qui s'enfuient, pâles et en haillons, sur leur coursier fumant
à peine la bataille terminée et qui ressurgissent richement vêtus, sous
un dais d'or, au milieu d'un cortège d'oriflammes et d'étendards, il
maudit les papes vêtus d'hermine qui, du haut de leur trône, aspergent
d'eau bénite les cachets écarlates des alliances et des coalitions, il
maudit l'empereur qu'il a vu une fois marcher entre des lances
dressées comme des phallus à la vue de cette donzelle qu'est la guerre,
enfin, le chevalier se lève, renverse la chaise, la table, les verres et
la cruche de vin, une grande bagarre éclate, la taverne ou quoi que ce
soit prend feu, le propriétaire est roué de coups, le régiment,
chevalier en tête, reprend la marche, et maintenant ils traversent le
bois sous un rayon de lune, et le chevalier ne maudit plus, ne vitupère
plus, il avance, muet, les yeux fixés sur la nuit, et peu à peu, les
soldats deviennent moins bavards, s'assoupissent sur leurs montures,
rêvent, la tête pendante sur leur cuirasse, l'un croit entendre une
musique lointaine, celle de son enfance dans quelque village du Milanais
ou de Catalogne, un autre croit reconnaître des voix qui le hèlent, la
voix de sa mère, celle de sa femme ou de sa fiancée, un autre encore
pousse un hurlement et se réveille en sursaut, pourtant le chevalier ne
s'arrête pas, ne se retourne pas pour voir qui a crié, ne prêtant pas
plus d'attention à ce cri que si ç'avait été celui d'un oiseau dans les
taillis, il continue, les yeux ouverts, fixés sur la nuit, et la lune
fait reluire son armure, et le soldat qui le suit, le plus proche, celui
qui porte un drapeau effiloché et brûlé par la poudre, qui pend
maintenant sur la croupe du cheval comme un linceul miteux, ce soldat,
un jeune homme blond à l'apparence de jongleur, a tout à coup un rêve
étrange, il imagine que l'armure du chevalier est vide, que le
chevalier a disparu, ne laissant derrière lui qu'une marionnette de
fer, ou, peut-être, que l'armure a pris possession du chevalier, l'a
absorbé comme une éponge, lui a sucé le sang, liquéfié les os, et
désormais l'armure n'est plus qu'une écorce creuse, dépourvue de
chair, il imagine cela parce qu'il n'a jamais vu le chevalier
autrement qu'enveloppé dans cette cuirasse, parce qu'il ne connaît du
chevalier que cette armure portant une lance, ces brassards et gantelets
qui indiquent la direction de la guerre, ce casque bourguignon qui
hurle, et sous ce casque, une crinière enchevêtrée, d'ailleurs,
peut-être, cette crinière n'est-elle qu'une barbe sans visage, un
rembourrage de crins, et cette idée, ce songe fait rire le soldat blond,
car il pense qu'il s'est sans doute écoulé beaucoup de temps depuis
que le chevalier a disparu, beaucoup de temps depuis que l'armure
s'est vidée sans qu'ils ne s'en soient aperçus, eux, les soldats, qui
ont suivi cette cuirasse creuse de bataille en bataille, défiant la mort
parce qu'ils croyaient que le chevalier les protégeait, et tandis le
porte-étendard blond rit comme un somnambule ou un ivrogne, le chevalier
se dresse sur les étriers et s'indigne, comme s'il avait deviné la
raison de ce rire, comme s'il voulait lui prouver qu'il était toujours
vivant, ou lui reprocher sa rêverie, alors le soldat blond recule
d'effroi, car il se rend compte que le chevalier ne s'est pas offusqué,
ne l'a pas maudit à cause de son rire, mais parce que les arbres de la
forêt qui, jusqu'alors, semblaient gelés sous la lune comme en hiver,
se chargeaient brusquement de fleurs et de fruits, je veux dire, même si
la métaphore est éculée et que tout le monde aura compris, je veux dire
que les arbres se couvraient de ces fleurs que la chaleur de la guerre
fait éclore en toute saison, par beau comme par mauvais temps, dans les
plaines fertiles comme dans les plaines arides, et ils se couvraient de
fruits à maturité, toujours mûrs pour la moisson et la récolte, je veux
parler de l'ennemi, de ces ennemis inexpugnables qui nous attendent
patiemment, obstinément, dissimulés dans l'ombre, fondus dans le
brouillard et la fumée, alors les cavaliers somnolents se transforment
en --- mais tout cela appartient au passé, tout cela est déjà arrivé, et
maintenant le chevalier retourne seul à son château, sans la mêlée de
fer, de chevaux et d'hommes qui l'accompagnait dans son périple à
travers les provinces de la guerre, il a laissé derrière lui tout ce
fracas, s'est libéré à jamais des bivouacs, des pillages, des
embuscades, de la faim, de la terreur, de l'insomnie, et il ne conserve
de la guerre que son cheval, son armure, sa lance pourvue à une
extrémité d'une peau de renard pour empêcher le sang de couler et de
tremper sa main, il conserve l'odeur de la crasse et de la sueur, les
poux, la brûlure, la fatigue, la maigreur, la vieillesse, et les
souvenirs, les souvenirs découpés dans le grand tableau criard de la
guerre, il se rappelle ce jeune homme tombé sur l'herbe, face au ciel,
enfonçant les deux jambes dans la rivière --- le Main, le Tage, l'Arno ---
les enfonçant jusqu'aux genoux dans la rivière indifférente, et
l'eau, en passant, lui ramollissait les chairs et les défaisait, les
emportait en aval, transformées en charpie d'abord violacée, puis
rosée, puis grise et ocre, il se rappelle les dix potences sur une place
noire et déserte, et sur chacune un pendu, dix pendus la langue dehors
que le vent faisait gémir, que le vent faisait se tordre, tandis que le
clocher donnait la même heure toujours, hors du Temps, il se rappelle le
vieillard accroupi pour vider ses entrailles sur le sol glacé, couvert
de neige, et qui s'effondre sitôt fini, au cœur d'une fleur de sang et
d'excréments, la rose de la dysenterie, et la tour très haute, carrée,
en briques, et plus loin, une rangée de cyprès, et le jet de poix
ardente tombant des créneaux de la tour sur les chevaliers vêtus de
tuniques blanches avec une croix rouge sur la poitrine, tombant sur les
chevaliers beaux et nobles qui, un peu plus tôt, avaient assisté à la
messe célébrée pour eux par un archevêque couvert de pierreries, et le
cratère noir creusé par la poix brûlante, le trou qui fumait et
crépitait comme une poêle sur le feu --- lui, le chevalier, perçut un
parfum douçâtre, une odeur de friture et de torchon brûlé, sentit une
brûlure sur la main et vit qu'un petit rognon de chair s'y était posé,
un rognon de chair de l'un de ces chevaliers qui, un instant plus tôt,
écoutaient la messe et s'en remettaient à Dieu, car c'était cela, pour
lui, la guerre, et peut-être avait-elle une autre signification pour les
roitelets, et une autre encore pour les papes et les empereurs, une
sorte de partie d'échecs qu'ils jouaient à distance, chacun enfermé
dans une ville, dans une forteresse, un palais, et quand la partie
serait finie, ils sortiraient à la rencontre l'un de l'autre, se
serreraient la main comme de loyaux adversaires et se partageraient les
provinces dont les fruits étaient dorénavant moissonnés et récoltés, de
même le chevalier a quitté à présent l'échiquier des papes et des
empereurs, il retourne à présent à son château où l'attend sa femme,
qu'il a laissée jeune et qu'il espère retrouver aussi jeune qu'alors,
où l'attend la table somptueusement dressée et le lit chaud préparé, où
l'attend le faucon qui se tenait sur son poing les matins de chasse, où
l'attend le luth dont il s'était accompagné autrefois pour chanter, dans
une cour de Provence ou de Sicile, les vers de Cino da Pistoia, dans ce
château enfin, il pourra se débarrasser de son armure comme d'une
croûte sèche, retirer le casque bourguignon semblable à une tête
étrangère ne sachant que blasphémer, que suivre la trace de l'ennemi,
et dans ce château, les roitelets qu'il avait sauvés de l'ignominie de
la défaite le combleraient d'honneurs, et le pape et l'empereur qui
avaient déplacé leurs pièces sur l'échiquier le feraient duc ou comte
palatin, et puis, au détour d'un chemin, il voit sur la colline intacte
son château intact, il voit autour la campagne et les paysans courbés
sur la terre, il voit un chien, un chien domestique, un chien sans
maître, peut-être, qui vagabonde, un chien qui trotte parmi les pierres
et s'arrête ici et là pour renifler les traces d'autres chiens, et
devant ce tableau presque idyllique du château, des laboureurs et du
chien, le chevalier pense que, de même que lui échappent les véritables
clés de la guerre, qui sont en possession des papes et des empereurs et
que les roitelets convoitent avec rage, ces paysans courbés sur les
sillons sont incapables de comprendre cet effroyable labeur qu'est la
guerre, et qu'il a, lui, enduré si longtemps, la guerre n'aura été pour
les paysans qu'une rumeur lointaine, le rougeoiement d'un incendie à
l'horizon, le défilé de régiments sur le chemin, quant au chien, pense
le chevalier, il ne conçoit même pas l'existence de la guerre, des
pillages et des massacres, des traités bénis par le pape, de cet
empereur dressant des lances comme des phallus, le chien aura continué à
manger, à dormir, à s'accoupler avec une chienne, ignorant que là-bas,
où le chevalier guerroyait, les frontières se redessinaient sans cesse,
le chien ne saura jamais que le pape, vicaire du Christ, a été traîné
dans les rues, que, jour et nuit, l'empereur s'agenouillait nu devant
une porte qui ne s'ouvrait jamais, il ne saura pas non plus que la fleur
de la chrétienté a été frite dans de la poix et de l'huile, et qu'un
carillon de pendus sonnait l'heure sur une place déserte et noire, non,
pour le chien, le tonnerre de la guerre aura fait exactement le même
fracas que le tonnerre de l'orage, et s'il avait croisé cette donzelle
qu'est la guerre, il lui aurait aboyé contre, comme contre une
étrangère, ou il aurait remué la queue s'il l'avait trouvée aimable, ou
si elle l'avait nourri, et le chevalier éprouve à présent de la fierté à
être un chevalier, à avoir été l'une des pièces de l'échiquier, à
appartenir à l'Histoire, même si son nom ne figure nulle part, et que
seuls figurent les noms des papes et des empereurs, et en lettres plus
petites, les noms des roitelets, et le chevalier éprouve de la pitié
pour ces paysans qui ne font pas l'Histoire, et il éprouve une sorte
d'étonnement devant ce chien contemporain des papes et des empereurs qui
ne saura jamais qu'ont existé des papes et des empereurs, qui ne saura
jamais qu'ont existé des chevaliers, il éprouve une sorte de perplexité
devant ce chien qui vient à sa rencontre comme il pourrait venir à la
rencontre d'un paysan ou de l'empereur, sans faire la différence, qui
vient à sa rencontre sans deviner les désastres et les prouesses qui
nimbent l'armure du chevalier, et poursuivant ce raisonnement, cette
association d'idées initiée par le chien, le chevalier pense que les
derniers chaînons ne sont peut-être ni le pape ni l'empereur, de même
que le chien ignore ce que savent les paysans, que les paysans ignorent
ce que sait le chevalier, que le chevalier ignore ce que savent les
roitelets, que ceux-ci ignorent ce que savent les papes et les
empereurs, de la même manière les papes et les empereurs ignorent ce que
seul Dieu connaît dans sa totalité et dans la perfection de la vérité,
et ces réflexions appliquées à la guerre, cette croyance que pour Dieu
aussi, la guerre est différente de ce qu'elle est pour les papes et les
empereurs, font naître chez le chevalier l'espoir que, pour Dieu,
l'Histoire inclura son nom, l'espoir que si le pape et l'empereur,
qui dominent le jeu de la guerre, font de lui, le chevalier, un duc ou
un comte en reconnaissance de son héroïsme, Dieu, qui domine le jeu des
papes et des empereurs, l'absoudra des morts, des viols et des pillages
en reconnaissance de sa souffrance, de sa faim et de ses insomnies, et
l'accueillera au paradis, et cet espoir fait sourire le chevalier, cet
espoir le réconforte et rachète tous les maux soufferts à la guerre, et
au moment précis où cet espoir réconforte le chevalier et le fait
sourire, le chien, qui courait à sa rencontre, s'arrête comme devant un
mur, plante ses pattes dans le sol, hérisse le poil, entrouvre la
gueule, montre les dents et se met à hurler lugubrement, mais le
chevalier attribue cette attitude à une circonstance banale, il se dit
que le chien ne le connaît pas, qu'il est effrayé par le cheval,
l'armure, la lance avec la queue de renard, il n'est pas étonnant
qu'un chien de paysans soit effrayé par un chevalier caparaçonné de fer
et un cheval bardé de chanfreins et de poitrails, ainsi, le chevalier
n'accorde aucune importance à l'attitude du chien, il continue
d'avancer sur le chemin, en direction de la colline au sommet de
laquelle se dresse le château, et les pattes du cheval manquent
d'écraser le chien qui s'écarte d'un bond et continue d'aboyer, de
grogner et de montrer les dents, tandis que le chevalier se remet à
penser à sa femme, à son faucon et à son luth d'amour, oubliant le
chien, le chien est déjà derrière lui, comme la guerre, mais ce que le
chevalier ne saura jamais, c'est que le chien flaire autour de
l'armure la puanteur de la mort, parce que le chien sait déjà ce que le
chevalier ne sait pas, il sait que dans l'aine du chevalier un bubon a
commencé à répandre le venin de la peste, et qu'au pied de la colline la
mort et le diable attendent d'emporter le chevalier, et si le chevalier
pouvait lire ce que j'écris, il penserait peut-être, suivant un
raisonnement analogue mais inverse, il penserait que, de même que le
chien s'arrête là où lui, le chevalier, continue, de même les chevaliers
s'arrêtent peut-être là où les papes et les empereurs continuent,
signifiant que les papes et les empereurs ignoreront peut-être ses actes
héroïques, et selon cette même logique, le chevalier pourrait penser
que, peut-être, les papes et les empereurs s'arrêtent là où Dieu
continue, que la partie d'échecs qu'ils jouent n'a aucune importance
pour Dieu, que peut-être Dieu ne regarde pas, que peut-être Dieu ne voit
pas l'échiquier et que le sacrifice des pièces ne compte pour rien à ses
yeux, et que, peut-être, le chevalier ne sera pas absous de ses péchés
ni admis au paradis, et si le chevalier raisonnait ainsi, il penserait
que pour Dieu, les illusions qui piègent les hommes forment un filet
auquel Dieu ne se prend pas, tout comme le chevalier a traversé, sans
les voir, les rets dans lesquels le chien s'est empêtré, bien que ces
rets fussent destinés au chevalier et non au chien, tout comme les
prières, les espoirs et la souffrance des hommes sont destinés à Dieu,
mais jamais le chevalier ne lira ce que j'écris, et déjà il atteint le
pied de la colline, heureux d'avoir tissé, par son courage, une toile
dans laquelle tomberont la mouche-pape, la mouche-empereur, heureux que
les papes et les empereurs aient tissé une autre toile dans laquelle
tombera la mouche-Dieu, et pendant ce temps, en bas, sur le chemin, le
chien qui confond le tonnerre de la guerre avec le tonnerre de l'orage
continue de mener sa guerre, une guerre dans laquelle le chevalier
confond l'aboiement de la mort avec celui d'un chien.
