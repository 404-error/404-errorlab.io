---
persona:
  - Pier Lampás
title: "À la compagnie des tigres"
slug: "a-la-compagnie-des-tigres"
date: 2020-12-14
echo:
  - rythmique
images:
  - "img/201214-lampas-a-la-compagnie-des-tigres.jpg"
gridimages:
  - "img/201214-lampas-a-la-compagnie-des-tigres-grid.jpg"
sourceimage: "https://commons.wikimedia.org/wiki/File:TamarackMiners_CopperCountryMI_sepia.jpg"
notes: ""
summary: "À la compagnie des tigres, / Au dernier cri / De la Horde / Qui gonfle nos veines, / À cette étrange beauté / Que je lis / Dans les replis du temps, / Et sur ton front / Qui a cogné les siècles"
citation: "Cette guerre, épique,\\\\Qui n’aura plus de visage."
---

À la compagnie des tigres,\

Au dernier cri\
De la Horde\
Qui gonfle nos veines,\

À cette étrange beauté\
Que je lis\
Dans les replis du temps,\

Et sur ton front\
Qui a cogné les siècles,\

Et même si\
Le velours de l’absence\
Rogne\
Le tout dernier vertige,\

Nous continuerons de chanter\
Cette guerre, épique,\
Qui n’aura plus de visage.\
