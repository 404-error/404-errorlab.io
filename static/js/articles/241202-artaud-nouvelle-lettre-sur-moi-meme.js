// Source inspiration : a pen by Milica, codepen.io/micikato/pen/PpaXMb
var hero = document.querySelector(".ombresmouvantes");
var text = hero.querySelector(".ombresmouvantes__texte");
var walk = 100; // px

function shadow(e) {
  var width = hero.offsetWidth,
    height = hero.offsetHeight;
  var x = e.offsetX,
    y = e.offsetY;

  if (this !== e.target) {
    x = x + e.target.offsetLeft;
    y = y + e.target.offsetTop;
  }

  var xWalk = Math.round((x / width) * walk - walk / 2);
  var yWalk = Math.round((y / height) * walk - walk / 2);

  text.style.textShadow =
    "\n      " +
    xWalk +
    "px " +
    yWalk +
    "px 0 rgba(0,0,0,0.05),\n      " +
    xWalk * -1 +
    "px " +
    yWalk +
    "px 0 rgba(0,0,0,0.07),\n      " +
    yWalk +
    "px " +
    xWalk * -1 +
    "px 0 rgba(0,0,0,0.09),\n      " +
    yWalk * -1 +
    "px " +
    xWalk +
    "px 0 rgba(0,0,0,0.11)\n    ";
}

hero.addEventListener("mousemove", shadow);
