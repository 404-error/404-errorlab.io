// Musique

function seolseol() {
  var audio = document.getElementById("fond-sonore");
  // var musiqueJoue = "pause";
  function fondSonore() {
    if (audio.paused) {
      audio.play();
    } else {
      audio.pause();
    }
  }
  const musique = document.querySelector(".music");
  musique.addEventListener("click", (e) => {
    e.preventDefault();
    document.querySelector(".music--on").classList.toggle("hidden");
    document.querySelector(".music--off").classList.toggle("hidden");
    fondSonore();
  });

  document.querySelectorAll(".tilt").forEach((el) => {
    el.addEventListener("mouseenter", function(event) {
      event.target.style.opacity = 0.6;
    });
    el.addEventListener("mouseleave", function(event) {
      event.target.style.opacity = 0.8;
    });
  });

  VanillaTilt.init(document.querySelectorAll(".tilt"), {
    perspective: 700,
    speed: 100,
    glare: true,
    "max-glare": 0.7,
    gyroscopeMinAngleX: -5,
    gyroscopeMaxAngleX: 5,
    gyroscopeMinAngleY: -5,
    gyroscopeMaxAngleY: 5,
  });
}

seolseol();
