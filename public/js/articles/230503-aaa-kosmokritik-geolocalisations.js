function paradoxe() {
  const randomNumber = (min, max) => Math.floor(Math.random() * (max - min + 1) + min);

  if (document.querySelector('.article__content--javascript')) {
    //
    // Shuffle some parts
    //
    if (document.querySelector('.button--action')) {
      const melangeZone = document.querySelector('.melange');
      const melangeBtn = document.querySelector('.button--action');

      const texts = [...document.querySelectorAll('.text--content')];
      const textShow = document.querySelector('.text--show');
      const sentences = [];
      const syntagma = 18;
      texts.forEach((el) => {
        el.style.display = 'none';
        sentences.push([...el.querySelectorAll('.sentence')]);
      });

      const showText = () => {
        textShow.innerHTML = '';

        for (let i = 0; i < syntagma; i++) {
          const randomSentence = randomNumber(0, sentences.length - 1);
          const group = sentences[randomSentence][i].cloneNode(true);
          textShow.appendChild(group);
          textShow.appendChild(document.createTextNode(' '));
        }
      };

      showText();

      melangeBtn.addEventListener('click', () => {
        melangeZone.animate([
          { transform: 'translate(0,0)' },
          { transform: 'translate(5px,0)' },
          { transform: 'translate(0,0)' },
          { transform: 'translate(5px,0)' },
          { transform: 'translate(0,0)' },
          { transform: 'translate(5px,0)' },
          { transform: 'translate(0,0)' },
          { transform: 'translate(5px,0)' },
          { transform: 'translate(0,0)' }
        ], {
          duration: 300,
          iterations: 1
        });
        showText();
        melangeBtn.blur();
      });
    }
  }
}
paradoxe();
