function newHaiku() {
  const haiku = {
    // Mots-clés
    metaphysique: [
      "l’être", "le rien", "le temps", "l’espace", "la matière", "l’énergie",
      "la conscience", "l’inconscient", "la réalité", "la vérité",
      "l’existence", "l’inexistence", "la présence", "l’absence", "le néant",
      "l’unité", "la pluralité", "la totalité", "l’infini", "l’éternité",
      "la fatalité", "le destin", "le hasard", "la nécessité", "la contingence",
      "la liberté", "le déterminisme", "la responsabilité", "la mort", "le soi",
      "la vertu", "l’injustice", "l’égalité", "l’inégalité", "l’identité",
      "la différence", "l’altérité", "la simulation", "la représentation", "la révélation",
      "le signe", "le sens", "la vérité", "la fausseté", "l’illusion",
      "la raison", "l’image", "la logique", "l’absurdité", "le paradoxe",
      "la contradiction", "la dialectique", "la synthèse",
      "la métaphysique", "le savoir", "la monade", "l’entité",
      "le phénomène", "l’existence", "la substance", "la langue", "le langage",
      "la réalité", "l’idée", "la matière", "l’esprit", "le chaos",
      "l’unité", "la multitude", "l’objet", "l’absolu", "l’universel",
      "l’endurance", "l’individu", "le nombre", "l’humanité", "le sujet",
      "le posthumanisme", "la transcendance", "le centre", "l’écocentrisme",
      "le biocentrisme", "la physique", "le cosmos", "le microscopique", "le monde"
    ],
    meontologie: [
      "le rêve", "l’être spectral", "l’être immonde", "la vacuité", "l’étrangeté", "le simulacre",
      "la désidentité", "la différence", "la désunion", "le plurivers", "la totalité moins l’un",
      "le négatif", "la transsubstantiation", "l’abîme", "la destitution", "la cyberprésence",
      "le sous-être", "la traversée", "l’antimatière", "la masse", "le sous-peuple",
      "la division", "l’universel divisé", "le zéro", "le pluriel de l’un", "le grouillement", "la mutation",
      "l’actualité", "le virtuel", "le fatum", "la nanoparticule", "l’antiparticule", "le contingent",
      "le réel", "l’idéal", "la concrétude", "la pulvérisation", "la déformation",
      "l’atome", "l’immatériel", "l’inspiration", "l’incorporel", "l’expiration",
      "le photon", "la spatialité", "la causalité", "la finalité", "l’efficience",
      "l’informe", "le sans-fond", "la dissonance", "l’irréel", "la postvérité",
      "la désinformation", "l’erreur", "le terrain vague", "l’errance", "le vague",
      "l’incertitude", "l’oubli", "l’inquiétude", "l’incroyance", "la facticité",
      "le non-être", "l’infime", "la volonté", "la puissance", "l’infinitude",
      "l’immonde", "l’inhumain", "l’impersonnel", "la stase", "le flux", "l’accélération", "la parallaxe",
      "l’eccéité", "l’entéléchie", "l’inhérence", "le spectre", "le zombie", "l’hantologie", "le hantement", "le baroque", "le grotesque", "la fadeur", "le sublime", "la tragédie", "l’univocité du non-être", "l’hypostase", "la persistance",
    ],
    crise: [
      "guerre nucléaire", "frappe nucléaire", "bombardement", "monoculture",
      "avalanche", "extinction massive", "ravinement", "réplique", "éruption solaire", "biomasse",
      "hypercyclone", "typhon", "incendie", "averse", "pluie acide", "pluie torrentielle", "écophagie",
      "accident nucléaire", "naufrage", "transport routier", "turbidité", "saturnisme", "mégot", "neige noire",
      "pollution des sols", "climatisation", "monde persistant", "ferme de calcul",
      "grappe de serveurs", "cluster", "métavers", "glissement de terrain", "terraformation",
      "réchauffement", "catastrophe", "extractivisme", "écocide", "extinction",
      "désastre", "cataclysme", "apocalypse", "smog",
      "changement climatique", "pollution", "dégradation", "désertification",
      "inondation", "sécheresse", "ouragan", "tornade", "feu de forêt",
      "fonte des glaciers", "pergélisol", "banquise", "effet de serre", "tempête",
      "cyclone", "tsunami", "séisme", "éruption volcanique", "la cendre",
      "nuage de condensation",
      "radioactivité", "toxicité", "poison", "empoisonnement", "contamination",
      "déforestation", "biodiversité", "écosystème", "environnement", "écologie",
      "épuisement", "érosion", "salinisation", "acidification", "eutrophisation",
      "surconsommation", "surproduction", "surémission", "surpollution",
      "développement durable", "transition écologique", "économie verte", "écologie politique",
      "justice climatique", "carbone", "marché", "compensation", "schisme",
      "adaptation", "mitigation", "résilience", "décarbonation", "écotaxe", "le nucléaire", "neutralité carbone",
      "charbon", "fracturation hydraulique", "huile de schiste", "pétrole", "pétrochimie", "industrie",
      "industrie pétrolière", "industrie verte", "marée noire", "pétrolier", "boue", "boue de forage",
      "forage", "dégazage", "oléoduc", "pipeline", "colonisation de l’espace", "colonialisme", "satellite",
      "anthropocène", "bombe carbone", "empreinte carbone", "vortex de déchets", "conquête spatiale", "expansion",
      "zone morte", "bitume", "goudron", "cloud", "information", "câble sous-marin", "singularité technologique", "insubordination",
      "plastique", "surpêche", "surpâturage", "matière première", "minerai", "colombite", "tantalite", "évasion",
      "hydrocarbure", "roche-mère", "gaz naturel", "supertanker", "citerne", "méthanier", "butanier",
      "asphalte", "chimie", "agrochimie", "pesticide", "pandémie", "ciment", "cimentier", "cargo", "fret",
      "transport aérien", "fusée", "zone rouge", "agent", "polymère", "bioplastique", "défoliation", "insecticide", 
    ],
    capitalisme: [
      "stratégie", "stratagème", "guerre de position", "empire", "société liquide",
      "prix du baril", "dérégulation", "main invisible", "l’offre", "la demande",
      "capitalisme vert", "capitalisme tardif", "krach boursier", "transhumanisme",
      "exploitation", "aliénation", "accumulation", "profit", "rente",
      "marchandisation", "liquidation", "illibéralisme", "néolibéralisme", "conservatisme", "fascisme",
      "mondialisation", "privatisation", "déréglementation", "délocalisation", "licenciement",
      "flexibilité", "précarité", "inégalité", "discrimination", "exclusion",
      "concentration", "monopole", "oligopole", "trust", "cartel", "société par actions",
      "spéculation", "négoce", "l’argent", "capital", "rente", "vente", "dette",
      "travail", "salariat", "exploitation", "aliénation", "domination", "société anonyme",
      "pouvoir", "hégémonie", "idéologie", "propagande", "censure", "cotation boursière",
      "résistance", "lutte", "révolution", "contre-révolution", "réforme", "trading haute-fréquence",
      "démocratie parlementaire", "participation", "citoyenneté", "migration", "papier-valeur",
      "démocratie", "capitalisme", "publicité", "cryptomonnaie", "dévaluation", "valeur", "inflation", "déflation",
      "libéralisation", "réforme", "réforme agraire", "flux financier", "abolition", "contrat à terme",
      "accumulation", "fétichisme", "appropriation", "prolétariat", "bourgeoisie", "swap d’inflation",
      "actionnariat", "classe ouvrière", "conscience de classe", "dépérissement", "obligation",
      "dictature", "productivisme", "mode de production", "lutte", "infrastructure", "contrat",
      "superstructure", "praxis", "esclavage", "petite bourgeoisie", "lumpenprolétariat",
      "survaleur", "réification", "subsomption", "sujet automatique", "valeur d’usage", "indexation",
      "valeur d’échange", "libre marché", "enclosure", "libertarianisme", "expropriation",
      "dépossession", "crédit", "taux de change", "concurrence", "marchéisation",
      "ruissellement", "planification", "zone franche", "neutralité du marché", "subordination",
    ],
    adjectifs: [
      "acide", "funeste", "incommensurable", "extrême", "fugace", "éphémère", "immuable", "de négation",
      "chaotique", "inextricable", "du hasard", "d’incertitude", "aléatoire",
      "nécessaire", "et contingence", "possible", "quasi", "idéaliste",
      "sans concrétude", "d’abstraction", "sans forme", "et matière", "immatérialiste",
      "du spirituel", "des corporéités", "de l’incorporel", "l’atemporalité", "d’espace",
      "sans cause", "finaliste", "d’efficience", "d’informe", "sans fond",
      "des apparences", "réaliste", "de la vérité", "sans fausseté", "d’immanence",
      "sans erreur", "sans vérité", "par certitude", "sans incertitude", "par doute",
      "sans foi", "autonomiste", "ontologique", "agnostique", "sans dieux", "sans être",
      "nihiliste", "ubiquiste", "dualiste", "moniste", "perspectiviste",
      "pluraliste", "démoniaque", "absolutiste", "universaliste", "particulariste",
      "du secret", "d’artificialité", "authentique", "automatique", "aveugle",
      "axiomatique", "unitaire", "classiste", "classique", "complexe", "simple", "contradictoire", "critique",
      "du danger", "de l’indéfini", "dense", "vide", "drastique", "durable", "dynamique",
      "du lointain", "d’embarras", "d’émotion", "énigmatique", "erratique", "sans essence", "sans existence",
      "féerique", "fertile", "fidèle", "du fini", "variable", "fragile", "funeste",
      "géométrique", "grave", "endogène", "exogène", "hétérogène", "historique", "homogène", "hostile", "de l’humain",
      "inhabitable", "terrestre", "hybride", "hypothétique", "imaginaire", "mobile", "immobile",
      "imperceptible", "imprévisible", "inaccessible", "incroyable", "incurable",
      "indispensable", "indigne", "sans inconnue", "bizarre", "étrange", "insolite",
      "instable", "d’instant", "d’institution", "intense", "apocalyptique", "générique",
      "sans intériorité", "sans vie", "intraitable", "inutile", "invariable", "invivable", "invisible",
      "invraisemblable", "d’irréel", "irresponsable", "irréversible", "sans valeurs",
    ],
    verbes: [
      "se délite", "se désintègre", "se consume", "dévore", "se liquéfie",
      "se réduit", "se simplifie", "se complexifie", "se transforme", "se métamorphose",
      "se développe", "croît", "se multiplie", "se reproduit", "se maintient",
      "se conserve", "se préserve", "se protège", "se défend", "résiste",
      "se soumet", "se plie", "se courbe", "se brise", "se casse",
      "se fissure", "craquelle", "se désagrège", "se désintègre", "se disloque",
      "se décompose", "se putréfie", "annihile", "infecte", "se corrompt", "se dégrade",
      "se détériore", "se délabre", "se ruine", "ruine", "se détruit", "se perd",
      "se disperse", "disperse", "se dissipe", "se dissout", "se désintègre", "inhabite",
      "se radicalise", "déséquilibre", "désespère", "détériore", "dérègle",
      "se désaccorde", "se désorganise", "démembre", "déstructure", "se démolit",
      "ravage", "dévaste", "dépeuple", "délabre", "se figure", "défigure", "se reforme", "dénature",
      "se dépersonnalise", "déshumanise", "se désocialise", "se déconnecte", "déracine",
      "déterritorialise", "déclasse", "se déconsidère", "décrédite", "s’altère", "déforme",
      "dégénère", "se déleste", "se délie", "démobilise", "démolit", "démonétise",
      "se nucléarise", "se dépare", "se peuple", "se dépersonnalise", "se programme",
      "se protège", "se destitue", "désœuvre", "se déracine", "s’arme"

    ],
    adverbes: [
      "lentement", "si rapide", "l’inexorable", "irréversiblement", "irrémédiablement",
      "silencieusement", "sans bruit", "invisiblement", "moins la perception",
      "graduellement", "par croissance", "continûment", "incessamment", "l’ininterruptible",
      "l’uniforme", "l’irrégularité", "la norme", "sans exception", "sans norme", "sans choix",
      "par fatalité", "sans espoir", "toujours", "souvent", "accelerando", "extrêmement",
      "avec force", "sans puissance", "en parallèle", "par principe", "calmement",
      "sans paix", "sereinement", "si brusque", "par à-coups", "soudain", "subitement", "inopinément",
      "par la loi", "la rigueur", "sans nom", "sans raison", "follement", "hardiment",
      "indécisément", "irrésolument", "l’inébranlable", "l’inflexible", "sans volonté",
      "spontanément", "sans nature", "d’instinct", "inconsciemment", "l’intention",
      "délibérément", "sans futur", "l’accident", "inexplicablement", "l’inconcevable",
      "éternellement", "la perpétuité", "incessamment", "infiniment", "sans fin",
      "sans borne", "sans limites", "ici", "à contrecœur", "à contre-courant", "à contretemps",
      "à demi-mot", "ad nauseam", "ad libitum", "à flots", "à la fin", "de toutes les fins",
      "à rebours", "a priori", "à l’infini", "à l’unisson", "à l’évidence", "alentour", "à l’avenir",
      "assez", "au-dedans", "au-dehors", "à vide", "bout à bout", "cà et là", "néanmoins",
      "comme il convient", "côte à côte", "coûte que coûte", "crescendo",
      "d’abord", "dans l’ensemble", "dans le temps", "sans espace", "dans l’intervalle",
      "dans un dernier temps", "à la fin des temps", "d’aucune manière", "de bout en bout",
      "de-ci de-là", "de proche en proche", "à la dérobée", "de justesse", "decrescendo", "désormais",
      "de temps à autre", "d’urgence", "en abondance", "en aparté", "en dernier lieu", "en dessous",
      "etc.", "en tout temps", "en toute situation", "en désordre", "ex nihilo", "fin", "ici et maintenant",
      "in situ", "sans détour", "un rien", "si peu", "sourdement", "dans l’ombre"
    ],
    separation: [
      ", ", " sans ", " — ", " : ", " ; ", " et ", ", ",
      " et ", " avec ", " : ", " : ", " : ", " et ",
      " sauf ", ", vu ", " vers ", " avant ", " après ",
      " dans ", " depuis ", " dès ", " sous ", " si ", " hors ",
    ],
    separationAdverbe: [
      ", ", " — ", " : ", " ; ",
      " : ", ", ",
    ],

    randomWord: function(selection) {
      return selection[Math.floor(Math.random() * selection.length)];
    },

    // Structure des haikus
    generate: function() {
      const haiku = {
        line1: "",
        line2: "",
        line3: ""
      };

      // Sélection aléatoire de la structure du haiku
      const structure = Math.floor(Math.random() * 10);
      const virguleRandom = Math.random() < 0.4 ? true : false;
      const virgule = virguleRandom ? ", " : "";
      const parenthese = Math.random() < 0.2 ? true : false;
      const parentheseOn = parenthese ? " (" : "";
      const parentheseOff = parenthese ? ")" : "";
      switch (structure) {
        case 0:
          haiku.line1 = `${this.randomWord(this.metaphysique)}${!parenthese ? this.randomWord(this.separation) : ""}${parentheseOn}${this.randomWord(this.meontologie)}${parentheseOff}`;
          haiku.line2 = `${this.randomWord(this.crise)}${virguleRandom ? virgule : " "}${this.randomWord(this.adjectifs)}`;
          haiku.line3 = `${this.randomWord(this.verbes)} ${this.randomWord(this.capitalisme)}`;
          break;
        case 1:
          haiku.line1 = `${this.randomWord(this.capitalisme)}${parenthese ? parentheseOn : this.randomWord(this.separationAdverbe)}${this.randomWord(this.adverbes)}${parentheseOff}`;
          haiku.line2 = `${this.randomWord(this.metaphysique)}${!virgule ? this.randomWord(this.separation) : ""}${virgule}${this.randomWord(this.meontologie)}`;
          haiku.line3 = `${this.randomWord(this.crise)} ${this.randomWord(this.verbes)}`;
          break;
        case 2:
          haiku.line1 = `${this.randomWord(this.crise)}${parenthese ? parentheseOn : " "}${this.randomWord(this.adjectifs)}${parentheseOff}`;
          haiku.line2 = `${this.randomWord(this.metaphysique)}${!virgule ? this.randomWord(this.separation) : ""}${virgule}${this.randomWord(this.meontologie)}`;
          haiku.line3 = `${this.randomWord(this.capitalisme)} ${this.randomWord(this.verbes)}`;
          break;
        case 3:
          haiku.line1 = `${this.randomWord(this.meontologie)}${!parenthese ? this.randomWord(this.separation) : ""}${parentheseOn}${this.randomWord(this.metaphysique)}${parentheseOff}`;
          haiku.line2 = `${this.randomWord(this.crise)}${virguleRandom ? virgule : this.randomWord(this.separationAdverbe)}${this.randomWord(this.adverbes)}`;
          haiku.line3 = `${this.randomWord(this.verbes)} ${this.randomWord(this.capitalisme)}`;
          break;
        case 4:
          haiku.line1 = `${this.randomWord(this.adjectifs)}${parenthese ? parentheseOn : " "}${this.randomWord(this.crise)}${parentheseOff}`;
          haiku.line2 = `${this.randomWord(this.metaphysique)}${!virgule ? this.randomWord(this.separation) : ""}${virgule}${this.randomWord(this.meontologie)}`;
          haiku.line3 = `${this.randomWord(this.capitalisme)} ${this.randomWord(this.verbes)}`;
          break;
        case 5:
          haiku.line1 = `${this.randomWord(this.verbes)}${parenthese ? parentheseOn : " "}${this.randomWord(this.crise)}${parentheseOff}`;
          haiku.line2 = `${this.randomWord(this.capitalisme)}${virguleRandom ? virgule : this.randomWord(this.separationAdverbe)}${this.randomWord(this.adverbes)}`;
          haiku.line3 = `${this.randomWord(this.meontologie)}${this.randomWord(this.separation)}${this.randomWord(this.metaphysique)}`;
          break;
        case 6:
          haiku.line1 = `${this.randomWord(this.capitalisme)}${parenthese ? parentheseOn : " "}${this.randomWord(this.adjectifs)}${parentheseOff}`;
          haiku.line2 = `${this.randomWord(this.crise)}${virguleRandom ? virgule : " "}${this.randomWord(this.verbes)}`;
          haiku.line3 = `${this.randomWord(this.metaphysique)}${this.randomWord(this.separation)}${this.randomWord(this.meontologie)}`;
          break;
        case 7:
          haiku.line1 = `${this.randomWord(this.meontologie)}${!parenthese ? this.randomWord(this.separation) : ""}${parentheseOn}${this.randomWord(this.metaphysique)}${parentheseOff}`;
          haiku.line2 = `${this.randomWord(this.crise)}${virguleRandom ? virgule : this.randomWord(this.separationAdverbe)}${this.randomWord(this.adverbes)}`;
          haiku.line3 = `${this.randomWord(this.verbes)} ${this.randomWord(this.capitalisme)}`;
          break;
        case 8:
          haiku.line1 = `${this.randomWord(this.adjectifs)}${parenthese ? parentheseOn : " "}${this.randomWord(this.crise)}${parentheseOff}`;
          haiku.line2 = `${this.randomWord(this.meontologie)}${!virgule ? this.randomWord(this.separation) : ""}${virgule}${this.randomWord(this.metaphysique)}`;
          haiku.line3 = `${this.randomWord(this.capitalisme)} ${this.randomWord(this.verbes)}`;
          break;
        case 9:
          haiku.line1 = `${this.randomWord(this.verbes)}${parenthese ? parentheseOn : " "}${this.randomWord(this.crise)}${parentheseOff}`;
          haiku.line2 = `${this.randomWord(this.capitalisme)}${virguleRandom ? virgule : this.randomWord(this.separationAdverbe)}${this.randomWord(this.adverbes)}`;
          haiku.line3 = `${this.randomWord(this.meontologie)}${this.randomWord(this.separation)}${this.randomWord(this.metaphysique)}`;
          break;
      }

      return haiku;
    }
  };

  if (document.querySelector('.article__content--javascript')) {
    //
    // Shuffle some parts
    //
    if (document.querySelector('.button--action')) {
      const melangeZone = document.querySelector('.melange');
      const melangeBtn = document.querySelector('.button--action');
      const textShow = document.querySelector('.text--show');


      const showText = () => {
        textShow.innerHTML = '';
        const haikuText = haiku.generate();
        textShow.innerHTML = `${haikuText.line1}<br>${haikuText.line2}<br>${haikuText.line3}`;
      };

      showText();

      melangeBtn.addEventListener('click', () => {
        melangeZone.animate([
          { transform: 'translate(0,0)' },
          { transform: 'translate(5px,0)' },
          { transform: 'translate(0,0)' },
          { transform: 'translate(5px,0)' },
          { transform: 'translate(0,0)' },
          { transform: 'translate(5px,0)' },
          { transform: 'translate(0,0)' },
          { transform: 'translate(5px,0)' },
          { transform: 'translate(0,0)' }
        ], {
          duration: 300,
          iterations: 1
        });
        showText();
        melangeBtn.blur();
      });
    }
  }

  // const haikus = [];
  // while (haikus.length < 99) {
  //   const newText = haiku.generate();
  //   if (!haikus.includes(newText)) {
  //     haikus.push(newText);
  //   }
  // }
  // console.log(haikus);
}

newHaiku();
