text = require 'text'
return {
  {
    Str = function (elem)
      --- Épigraphe
      if string.find(elem.text, "{{%%epigraphe%%}}") then
        return pandoc.RawInline('latex', '\\begin{epigraphe}')
      elseif string.find(elem.text, "{{%%/epigraphe%%}}") then
        return pandoc.RawInline('latex', '\\end{epigraphe}')

      --- gauche
      elseif string.find(elem.text, "{{%%gauche%%}}") then
        return pandoc.RawInline('latex', '\\begin{gauche}')
      elseif string.find(elem.text, "{{%%/gauche%%}}") then
        return pandoc.RawInline('latex', '\\end{gauche}')

      --- droite
      elseif string.find(elem.text, "{{%%droite%%}}") then
        return pandoc.RawInline('latex', '\\begin{droite}')
      elseif string.find(elem.text, "{{%%/droite%%}}") then
        return pandoc.RawInline('latex', '\\end{droite}')

      --- droitecit
      elseif string.find(elem.text, "{{%%droitecit%%}}") then
        return pandoc.RawInline('latex', '\\begin{droitecit}')
      elseif string.find(elem.text, "{{%%/droitecit%%}}") then
        return pandoc.RawInline('latex', '\\end{droitecit}')

      --- Astérisme
      elseif string.find(elem.text, "{{%%asterisme%%}}") then
        return pandoc.RawInline('latex', '\\asterisme')

      else
        return elem
      end
    end,
  }
}
