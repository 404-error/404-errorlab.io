// Include Gulp
var gulp = require('gulp'),
    critical = require('critical'),
    browserSync = require('browser-sync'),
    del = require('del'),
    cache = require('gulp-cache');
    gulpif = require('gulp-if');
    filter = require('gulp-filter'),
    imageResize = require('gulp-image-resize');
    changed = require("gulp-changed");
    rename = require("gulp-rename");
    imagemin = require('gulp-imagemin'),
    exec = require('child_process').exec; // For running a local machine task

// Compress and transform all images
gulp.task('images', function(done) {

  [2400,1200,800,400].forEach(function (size) {
    gulp.src( ['src/img/**/*.{png,gif,jpg}', '!src/img/**/*.mp3', '!src/img/**/*.mp4'])
      .pipe(changed('static/img/'))
      .pipe(imageResize({ width: size }))
      .pipe(cache(imagemin([
        imagemin.gifsicle({interlaced: true, optimizationLevel: 3}),
        imagemin.mozjpeg({quality: 70, progressive: true}),
        imagemin.optipng({optimizationLevel: 5}),
        imagemin.svgo({
            plugins: [
                {removeViewBox: true},
                {cleanupIDs: false}
            ]
        })
      ], {
          verbose: true
      }
      )))
      .pipe(gulpif(size == 2400, rename(function (path) { path.basename = `${path.basename}`; })))
      .pipe(gulpif(size == 1200, rename(function (path) { path.basename = `${path.basename}-large`; })))
      .pipe(gulpif(size == 800, rename(function (path) { path.basename = `${path.basename}-medium`; })))
      .pipe(gulpif(size == 400, rename(function (path) { path.basename = `${path.basename}-small`; })))
      .pipe(gulp.dest('static/img/'));
    });
  gulp.src( ['src/img/**/*.mp4','src/img/**/*.mp3'])
     .pipe(gulp.dest('static/img/'));
  done();
});
// gulp.task('images', () => {
//   const f = filter(['src/img/**', '!src/img/favicon/*'], { restore: true });
//   return gulp.src('src/img/**')
//       .pipe(f)
//       .pipe(cache(imagemin([
//         imagemin.gifsicle({interlaced: true, optimizationLevel: 3}),
//         imagemin.mozjpeg({progressive: true}),
//         imagemin.optipng({optimizationLevel: 5}),
//         imagemin.svgo({
//             plugins: [
//                 {removeViewBox: true},
//                 {cleanupIDs: false}
//             ]
//         })
//        ], {verbose: true}
//        )))
//        .pipe(f.restore)
//        .pipe(gulp.dest('static/img/'));
// });

// Clean : clear cache and delete images
gulp.task('clean', function () {
  return del(['static/img/*']);
  cache.clearAll();
});

// Generate & Inline Critical-path CSS
gulp.task('critical', function (done) {
  critical.generate({
  base: 'public-dev/',
  src: 'index.html',
  css: ['public-dev/css/style.*.css'],
  target: {
    css: '../assets/css/critical.css',
  },
  dimensions: [
    {
      height: 600,
      width: 300,
    },
    {
      height: 800,
      width: 1300,
    },
  ],
});
  done();
});

// Hugo
gulp.task(
  'hugo',
  gulp.series(
  function (cb) {
  exec("hugo --minify --cleanDestinationDir --environment development", function (err, stdout, stderr) {
    console.log(stdout);
    console.log(stderr);
    cb(err);
  });
  })
);

gulp.task(
  'hugo-prod',
  gulp.series(
  function (cb) {
  exec("hugo --minify --cleanDestinationDir --environment production", function (err, stdout, stderr) {
    console.log(stdout);
    console.log(stderr);
    cb(err);
  });
  })
);

// Browser-sync
gulp.task('browser-sync', function(cb) {
  browserSync({
    server: {
      baseDir: "public-dev"
    },
    open: false
  }, cb);
});

function reload(done) {
  browserSync.reload();
  done();
}

gulp.task('pdf-compare', function (cb) {
  exec('sleep 5 && ./makePDF.sh compare', function (err, stdout, stderr) {
    console.log(stdout);
    console.log(stderr);
    cb(err);
  });
})

// Watch
gulp.task('watch', function () {
  gulp.watch([
    'content/**',
    'assets/**',
    'data/**',
    'layouts/**',
    'static/**',
    'archetypes/**',
    'config/**'
  ], gulp.series('hugo', reload));
  gulp.watch('src/img/**/*', gulp.series('images', reload));
});

gulp.task(
  'default',
  // gulp.series('images', 'hugo', 'critical', 'browser-sync', 'watch')
  gulp.series('hugo', 'critical', 'browser-sync', 'watch')
);

gulp.task(
  'deploy',
  gulp.series('images', 'hugo-prod', 'pdf-compare', 'watch')
);
