#!/bin/bash

INPUT=$1
TEX=$2
LAST=`ls -tb ./content/articles/* | head -1`

if [[ ${INPUT} = "compare" ]]; then
  ARTICLES=`ls -1q ./content/articles/* | wc -l`
  PDFS=`ls -1q ./static/etc/*.pdf | wc -l`
  ADDS=3
  PDFS=$((PDFS - ARTICLES - ADDS))
  if [[ $ARTICLES != $PDFS ]]; then
    echo "---------------------------------"
    echo "|                               |"
    echo "| Attention ! Il manque $((ARTICLES - PDFS)) PDF ! |"
    echo "|                               |"
    echo "---------------------------------"
  fi

elif [[ ${TEX} = "tex" ]]; then
  rm -rf ./tmp/*
  PDF=$(basename -- ${INPUT%.md}.pdf)
  TEXFILE=$(basename -- ${INPUT%.md}.tex)
  BOOKLET=$(basename -- ${INPUT%.md}-zine.tex)
  TEXSOURCE=./tmp/$TEXFILE
  pandoc -s --metadata lang:fr --lua-filter ./tools/replace.lua --lua-filter ./tools/pandoc-quotes.lua -f markdown -t latex --pdf-engine=lualatex --top-level-division=chapter --template=tools/template.tex --toc --toc-depth=3 -o $TEXSOURCE $INPUT
  latexmk -lualatex -output-directory=./tmp $TEXSOURCE
  NBPAGES=$(pdfinfo ./tmp/$PDF | grep Pages | awk '{print $2}')
  pandoc -s --metadata lang:fr --metadata numberofpages:$NBPAGES --metadata pdfsource:./$PDF -f markdown -t latex --pdf-engine=lualatex --top-level-division=chapter --template=tools/booklet.tex --toc --toc-depth=3 -o ./tmp/$BOOKLET $INPUT
  cd tmp
  sed -i -- 's/{src/{\.\.\/src/g' $TEXFILE
  nvim $TEXFILE

elif [[ ${INPUT} = "last" ]]; then
  PDF=$(basename -- ${LAST%.md}.pdf)
  BOOKLET=$(basename -- ${LAST%.md}-zine.pdf)
  PDFSOURCE=./static/etc/$PDF
  pandoc -s --metadata lang:fr --lua-filter ./tools/replace.lua --lua-filter ./tools/pandoc-quotes.lua -f markdown -t latex --pdf-engine=lualatex --top-level-division=chapter --template=tools/template.tex --toc --toc-depth=3 -o $PDFSOURCE $LAST
  NBPAGES=$(pdfinfo $PDFSOURCE | grep Pages | awk '{print $2}')
  pandoc -s --metadata lang:fr --metadata numberofpages:$NBPAGES --metadata pdfsource:$PDFSOURCE -f markdown -t latex --pdf-engine=lualatex --top-level-division=chapter --template=tools/booklet.tex --toc --toc-depth=3 -o ./static/etc/$BOOKLET $LAST

elif [[ -n $INPUT ]]; then
  PDF=$(basename -- ${INPUT%.md}.pdf)
  BOOKLET=$(basename -- ${INPUT%.md}-zine.pdf)
  PDFSOURCE=./static/etc/$PDF
  pandoc -s --metadata lang:fr --lua-filter ./tools/replace.lua --lua-filter ./tools/pandoc-quotes.lua -f markdown -t latex --pdf-engine=lualatex --top-level-division=chapter --template=tools/template.tex --toc --toc-depth=3 -o $PDFSOURCE $INPUT
  NBPAGES=$(pdfinfo $PDFSOURCE | grep Pages | awk '{print $2}')
  pandoc -s --metadata lang:fr --metadata numberofpages:$NBPAGES --metadata pdfsource:$PDFSOURCE -f markdown -t latex --pdf-engine=lualatex --top-level-division=chapter --template=tools/booklet.tex --toc --toc-depth=3 -o ./static/etc/$BOOKLET $INPUT

else
  for i in ./content/articles/* ; do
    test "$i" = ./content/articles/_index.md && continue
    PDF=$(basename -- ${i%.md}.pdf)
    BOOKLET=$(basename -- ${i%.md}-zine.pdf)
    PDFSOURCE=./static/etc/$PDF
    pandoc -s --metadata lang:fr --lua-filter ./tools/replace.lua --lua-filter ./tools/pandoc-quotes.lua -f markdown -t latex --pdf-engine=lualatex --top-level-division=chapter --template=tools/template.tex --toc --toc-depth=3 -o $PDFSOURCE $i
    NBPAGES=$(pdfinfo $PDFSOURCE | grep Pages | awk '{print $2}')
    pandoc -s --metadata lang:fr --metadata numberofpages:$NBPAGES --metadata pdfsource:$PDFSOURCE -f markdown -t latex --pdf-engine=lualatex --top-level-division=chapter --template=tools/booklet.tex --toc --toc-depth=3 -o ./static/etc/$BOOKLET $i
  done
fi


exit 0
