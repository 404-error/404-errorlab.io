// Scripts

// Variables
const menuBtn = document.querySelector('.button--menu');
const menu = document.querySelector('.header__nav');

// Begin init for swup
function init() {
  //
  // Go top of page when change
  //
  if (window.location.hash) {
    const elScroll = document.querySelector(""+decodeURI(window.location.hash)+"");
    if (elScroll) {
      window.scrollTo(0, 0);
      elScroll.scrollIntoView();
    }
  }
  else {
    const elScroll = document.querySelector('.content');
    if (elScroll) {
      window.scrollTo(0, 0);
      // elScroll.scrollIntoView({behavior: "smooth", block: "start", inline: "nearest"});
    }
  }

  //
  // Reload page if same page link
  //
  const links = document.querySelectorAll('a:not([href^="#"])');
  links.forEach((link) => {
    link.addEventListener('click', () => {
      if (link.pathname === window.location.pathname) {
        swup.loadPage({url: window.location.pathname});
      }
      link.blur();
    });
  });

  //
  // Lazyload
  //
  const lazyLoadImgs = new LazyLoad({
    elements_selector: 'img',
    // Native lazy loading
    // use_native: true
  });
  // // Without native lazy loading
  // const lazyLoadOthers = new LazyLoad({
  //   elements_selector: '.lazy',
  // });

  //
  // Baffle headers
  //
  let b = baffle('.baffle')
    .start()
    .set({
      characters: '0x000x01',
      speed: 150,
    })
    .reveal(1000, 1000);

  //
  // Correct 100 Vh - Source : https://codepen.io/team/css-tricks/pen/WKdJaB
  //
  // First we get the viewport height and we multiple it by 1% to get a value for a vh unit
  let vh = window.innerHeight * 0.01;
  // Then we set the value in the --vh custom property to the root of the document
  document.documentElement.style.setProperty('--vh', `${vh}px`);

  // We listen to the resize event
  window.addEventListener('resize', () => {
    // We execute the same script as before
    let vh = window.innerHeight * 0.01;
    document.documentElement.style.setProperty('--vh', `${vh}px`);
  });

  // Remove menu when reload
  menu.classList.remove('header__nav--show');

  //
  // Site Title
  //
  const siteTitle = document.querySelector('.header__home a');
  siteTitle.addEventListener('click', () => {
    if (window.location.pathname === '/') {
      window.location.reload();
    }
  });

  // Random numbers
  function randomNb(min, max) {
    return Math.floor(Math.random() * (max - min + 1) + min);
  }

  //
  // Button Hack
  //
  const piraterie = ['la piraterie', 'littéraire', 'n’est', 'jamais', 'finie'];

  const btnHack = document.querySelector('.button--hack');
  let btnHackActive = false;
  // const hack = document.createElement('div');
  const hack = document.querySelector('.hack');
  hack.classList.add('hidden');
  hack.style.zIndex = '900';
  hack.style.position = 'fixed';
  hack.style.top = '0';
  hack.style.height = '100vh';
  const mediaQuery = window.matchMedia('(min-width: 50em)');
  if (mediaQuery.matches) {
    hack.style.left = 'var(--first-column-width)';
    hack.style.width = 'calc(100vw - var(--first-column-width))';
  } else {
    hack.style.left = 'calc(var(--first-column-width) + var(--second-column-width))';
    hack.style.width = 'calc(100vw - calc(var(--first-column-width) + var(--second-column-width)))';
  }
  let hackTime;

  btnHack.addEventListener('click', (event) => {
    event.preventDefault();
    btnHack.blur();
    if (!btnHackActive) {
      document.body.appendChild(hack);
      hack.classList.toggle('hidden');
      btnHack.classList.toggle('button--anim');
      btnHackActive = !btnHackActive;
      hackTime = setInterval(() => {
        shuffleArray(piraterie);
        let popup = document.createElement('div');
        popup.style.position = 'absolute';
        popup.style.top = randomNb(0, 100) + '%';
        popup.style.left = randomNb(0, 100) + '%';
        popup.style.fontSize = '1em';
        popup.style.color = 'var(--color-fg)';
        popup.style.whiteSpace = 'nowrap';
        popup.innerHTML = piraterie[0];
        hack.appendChild(popup);
      }, 100);
    } else {
      hack.innerHTML = '';
      hack.classList.toggle('hidden');
      btnHack.classList.toggle('button--anim');
      btnHackActive = !btnHackActive;
      clearInterval(hackTime);
    }
  });

  //
  // Shuffle some parts
  //
  if (document.querySelector('.button--melange')) {
    const melangeZone = document.querySelector('.melange-zone');
    const headers = document.querySelectorAll('.melange-zone h2');
    headers.forEach((el) => {
      let divMelange = document.createElement('div');
      divMelange.classList.add('melange');
      let p = el.nextElementSibling;
      divMelange.appendChild(el);
      divMelange.appendChild(p);
      melangeZone.appendChild(divMelange);
    });
    const melangeEl = Array.from(document.querySelectorAll('.melange'));

    const melangeBtn = document.querySelector('.button--melange');
    melangeBtn.addEventListener('click', (event) => {
      melangeZone.animate([
        { transform: 'translate(0,0)' },
        { transform: 'translate(5px,0)' },
        { transform: 'translate(0,0)' },
        { transform: 'translate(5px,0)' },
        { transform: 'translate(0,0)' },
        { transform: 'translate(5px,0)' },
        { transform: 'translate(0,0)' },
        { transform: 'translate(5px,0)' },
        { transform: 'translate(0,0)' }
      ], {
        duration: 300,
        iterations: 1
      });
      melangeZone.innerHTML = '';
      shuffleArray(melangeEl);
      melangeEl.forEach((el) => {
        melangeZone.appendChild(el);
      });
    });
  }

  //
  // Sententiae
  //
  if (document.querySelector('.button--sententiae')) {
    const sententiaeSelect = document.querySelector('.select--sententiae');
    sententiaeSelect.addEventListener('change', (event) => {
      const words = document.querySelectorAll('.verbe');
      words.forEach((w) => {
        const word = w;
        word.textContent = event.target.value;
      });
    });

    const sententiaeBtn = document.querySelector('.button--sententiae');
    sententiaeBtn.addEventListener('click', (event) => {
      const sententiae = Array.from(document.querySelectorAll('.sententia'));
      const txt = document.querySelector('.txt--sententiae');
      txt.innerHTML = '';
      shuffleArray(sententiae);
      sententiae.forEach((el) => {
        txt.appendChild(el);
      });
    });
  }

  // Hypertexte (function to go to a random anchor in the page)
  function hypertexte() {
    const links = [...document.querySelectorAll('span[id]')];
    const i = parseInt(Math.random() * links.length);
    location.href= '#' + links[i].id;
  }
  if (document.querySelector('.button--hypertexte')) {
    const hypertexteBtn = document.querySelector('.button--hypertexte');
    hypertexteBtn.addEventListener('click', (e) => {
      e.preventDefault();
      hypertexte();
      hypertexteBtn.blur();
    });
  }

  // Repères avec colonnes
  function findRepere() {
    const reperes = document.querySelectorAll('.colonnes__gauche .repere');
    reperes.forEach((el) => {
      const posYGauche = el.offsetTop;
      const id = el.dataset.repere;
      const colonneDroite = el.closest('.colonnes').querySelector('.colonnes__droite');
      const posYColonneDroite = colonneDroite.offsetTop;
      const elDroite = colonneDroite.querySelector(`[data-repere=${id}]`);
      const posYElDroite = elDroite.offsetTop;
      const parElDroite = colonneDroite.querySelector(`[data-repere=${id}]`).closest('p') || colonneDroite.querySelector(`[data-repere=${id}]`).closest('div');
      parElDroite.style.transform = '';
      const shift = posYGauche - posYElDroite;
      parElDroite.style.marginTop = 'calc(var(--line-space) + ' + shift + 'px)';
    });
  }

  if (document.querySelector('.colonnes')) {
    findRepere();
    var doit;
    window.onresize = function(){
      clearTimeout(doit);
      doit = setTimeout((e) => {
        const reperesDroite = [...document.querySelectorAll('.colonnes__droite .repere')];
        reperesDroite.forEach((el) => {
          const p = el.closest('p') || el.closest('div');
          p.style.marginTop = '';
        });
        findRepere();
      }, 1000);
    };
  }

  //
  // For revue page
  //
  if (document.querySelector('.revue')) {
    //
    // Revue Menu
    //
    const revueMenuBtn = document.querySelector('.button--revue');
    const revueMenu = document.querySelector('.revue__menu');
    const infoTableBtns = document.querySelectorAll('.button--item');
    let revueMenuOpen = false;

    const contentRevue = document.querySelector('.revue');
    contentRevue.addEventListener('touchstart', (event) => {
      this.allowUp = this.scrollTop > 0;
      this.allowDown = this.scrollTop < this.scrollHeight - this.clientHeight;
      this.slideBeginY = event.pageY;
    });

    contentRevue.addEventListener('touchmove', (event) => {
      const up = event.pageY > this.slideBeginY;
      const down = event.pageY < this.slideBeginY;
      this.slideBeginY = event.pageY;
      if ((up && this.allowUp) || (down && this.allowDown)) {
        event.stopPropagation();
      } else {
        event.preventDefault();
      }
    });

    // Menu Height when open

    revueMenuBtn.addEventListener('click', (e) => {
      e.preventDefault();
      revueMenuOpen = !revueMenuOpen;
      revueMenu.classList.toggle('hidden');
      if (revueMenuOpen) {
        document.documentElement.style.overflow = 'hidden';
        document.body.scroll = 'no';
      } else {
        document.documentElement.style.overflow = 'auto';
        document.body.scroll = 'yes';
      }
    });

    //
    // Revue buttons
    //
    infoTableBtns.forEach((btn) => {
      btn.addEventListener('click', (e) => {
        e.preventDefault();
        const info = btn.parentNode.querySelector('.revue__info');
        info.classList.toggle('revue__info--show');

        // if (info.classList.contains('.revue__info--show')) {
        //   zIndex += 1;
        //   info.style.zIndex = zIndex;
        // } else {
        //   info.style.zIndex = '10';
        // }

        // const border = btn.parentNode.nextElementSibling.querySelector('.article__border');
        // if (border) {
        //   border.classList.toggle('article__border--show');
        // }
      });
    });

    //
    // Random position of articles
    //
    function randomNb(min, max) {
      return Math.floor(Math.random() * (max - min + 1) + min);
    }

    const revueItems = document.querySelectorAll('.revue__item');
    const revueItemsMeta = document.querySelectorAll('.revue__item--meta');
    revueItems.forEach((el) => {
      const nbX = randomNb(-100, 100);
      const nbY = randomNb(-100, 100);
      el.style.top = randomNb(0, 80) + '%';
      el.style.left = randomNb(0, 80) + '%';
      // el.style.transform = 'translate(' + nbX + 'px, ' + nbY + 'px)';
      el.style.zIndex = randomNb(1, revueItems.length);
      // el.setAttribute('data-x', nbX);
      // el.setAttribute('data-y', nbY);
      revueItemsMeta.forEach((meta) => {
        meta.style.zIndex = revueItems.length + 1;
      });
    });

    //
    // Interact.js
    //
    let zIndexDrag = 0;

    interact('.drag').draggable({
      listeners: {
        start: function (event) {
          event.target.style.zIndex = parseInt(new Date().getTime() / 10 ** 11) + zIndexDrag;
          zIndexDrag += 1;
          event.target.style.pointerEvents = 'none';
        },
        move: dragMoveListener,
        end: function (event) {
          event.target.style.pointerEvents = '';
        },
      },
      inertia: true,
      modifiers: [
        interact.modifiers.restrictRect({
          restriction: '.revue--table',
          endOnly: true,
        }),
      ],
      autoScroll: false,
    });

    function dragMoveListener(event) {
      let target = event.target;
      // keep the dragged position in the data-x/data-y attributes
      let x = (parseFloat(target.getAttribute('data-x')) || 0) + event.dx;
      let y = (parseFloat(target.getAttribute('data-y')) || 0) + event.dy;

      // translate the element
      target.style.webkitTransform = target.style.transform = 'translate(' + x + 'px, ' + y + 'px)';

      // update the posiion attributes
      target.setAttribute('data-x', x);
      target.setAttribute('data-y', y);
    }

    // this function is used later in the resizing and gesture demos
    window.dragMoveListener = dragMoveListener;
  }

  //
  // For search page
  //
  if (document.querySelector('.explore__input')) {
    // get dom elements
    searchInput = document.querySelector('.explore__input');
    searchResults = document.querySelector('.explore__results');
    let urlJson = '/index.json';

    currentContent = document.querySelectorAll('.explore__description');

    // request and index documents
    fetch(urlJson, {
      method: 'get',
    })
      .then((res) => res.json())
      .then((res) => {
        // index document
        idx = lunr(function () {
          // issue with wildcard
          // https://github.com/olivernn/lunr.js/issues/454
          this.pipeline.remove(lunr.stemmer);
          this.pipeline.remove(lunr.stopWordFilter);

          this.ref('url');
          this.field('title');
          this.field('content');
          this.field('summary');
          this.field('date');
          this.field('tags');
          this.field('authors');

          res.forEach(function (doc) {
            this.add(doc);
            documents[doc.url] = {
              title: doc.title,
              content: doc.content,
              summary: doc.summary,
              date: doc.date,
              tags: doc.tags,
              authors: doc.authors,
            };
          }, this);
        });

        // data is loaded, next register handler
        registerSearchHandler();
      })
      .catch((err) => {
        searchResults.innerHTML = `<p>${err}</p>`;
      });
  }
}
// End init for swup

//
// Menu
//
// Menu Height when open
let menuOpenHeight = document.querySelector('.header__nav').offsetHeight;
document.documentElement.style.setProperty('--menu-height-open', `${menuOpenHeight}px`);

menuBtn.addEventListener('click', (e) => {
  e.preventDefault();
  menu.classList.toggle('header__nav--show');
  menuOpenHeight = document.querySelector('.header__nav').offsetHeight;
  document.documentElement.style.setProperty('--menu-height-open', `${menuOpenHeight}px`);
});

//
// Display articles informations
//
// const infoBtns = document.querySelectorAll('.button--item');
// const infoTableBtns = document.querySelectorAll('.button--item');
// let zIndex = 10;

// infoBtns.forEach((btn) => {
//   btn.addEventListener('click', (e) => {
//     e.preventDefault();
//     const info = btn.parentNode.querySelector('.article__info');
//     info.classList.toggle('article__info--show');

//     // if (info.classList.contains('article__info--show')) {
//     //   zIndex += 1;
//     //   info.style.zIndex = zIndex;
//     // } else {
//     //   info.style.zIndex = '10';
//     // }

//     const border = btn.parentNode.nextElementSibling.querySelector('.article__border');
//     if (border) {
//       border.classList.toggle('article__border--show');
//     }
//   });
// });

//
// Scroll to top
//
const btnScroll = document.querySelector('.scroll-to-top');
const scrollToTop = () => {
  window.scroll({ top: 0, left: 0, behavior: 'smooth' });
};
btnScroll.addEventListener('click', (e) => {
  e.preventDefault();
  scrollToTop();
});

//
// Dark mode
//
const btnDark = document.querySelector('.dark-mode');
let darkModeToggle = false;
const darkModeTheme = localStorage.getItem('theme');

if (darkModeTheme) {
  document.documentElement.setAttribute('data-theme', darkModeTheme);

  if (darkModeTheme === 'dark') {
    darkModeToggle = true;
  }
}

function darkModeSwitch() {
  if (!darkModeToggle) {
    document.documentElement.setAttribute('data-theme', 'dark');
    localStorage.setItem('theme', 'dark');
    darkModeToggle = true;
  } else {
    document.documentElement.setAttribute('data-theme', 'light');
    localStorage.setItem('theme', 'light');
    darkModeToggle = false;
  }
}

btnDark.addEventListener('click', darkModeSwitch);

//
// Shuffle article
//
const btnShuffle = document.querySelector('.shuffle');

function shuffleArray(array) {
  for (let i = array.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    [array[i], array[j]] = [array[j], array[i]];
  }
}

function ShuffleArticle() {
  // let arrayNb = 0;
  fetch('/index.json')
    .then((response) => response.json())
    .then((data) => {
      shuffleArray(data);
      // for (let i = 0; i < data.length; i++) {
      //   if (data[i].section != "meta") {
      //     arrayNb = i;
      //     break;
      //   }
      // }
      window.location = data[0].url;
    });
}

btnShuffle.addEventListener('click', ShuffleArticle);

//
// Swup - Page transition
//
const options = {
  plugins: [new SwupScriptsPlugin({
    head: false,
    body: false,
    optin: true
  })],
  containers: ['.swup'],
  linkSelector:
    'a[href^="' +
    window.location.origin +
    // '"]:not([data-no-swup]), a[href^="/"]:not([data-no-swup]), a[href^="#"]:not([data-no-swup])'
    '"]:not([data-no-swup]), a[href^="/"]:not([data-no-swup])',
};
const swup = new Swup(options);
init();
swup.on('contentReplaced', init);
