//
// Search Engine with Lunrjs
// This code from Hugo Lithium Theme - Janik von Rotz - MIT License
// https://github.com/janikvonrotz/hugo-lithium-theme/blob/master/exampleSite/content/page/search.md
// https://janikvonrotz.ch/2019/06/10/2019-06-10-simple-hugo-page-search-with-lunrjs/
//
// Plus search script inside init function
//
// define globale variables
let idx;
let searchInput;
let searchResults = null;
let documents = [];

let currentContent = document.querySelectorAll('.explore__description');

function renderSearchResults(results) {
  if (results.length > 0) {
    // show max 10 results
    // if (results.length > 9){
    //     results = results.slice(0,10)
    // }

    // reset search results
    searchResults.innerHTML = '';

    // append results
    results.forEach((result) => {
      // create result item
      let article = document.createElement('article');
      article.classList.add('explore__result');
      article.innerHTML = `
        <h2 class="explore__title explore__title--result"><a href="${result.ref}">${documents[result.ref].title}</a></h2>
        <p class="explore__summary">${documents[result.ref].summary}<span class="read-more"><a href="${result.ref}">
        [&hellip;]</a></span></p>`;
      searchResults.appendChild(article);
    });

    // if results are empty
  } else {
    searchResults.innerHTML = '<p class="noresult">&hellip;pour que le vide ne soit jamais <em>l’errance</em>&hellip;</p>';
  }
}

function registerSearchHandler() {
  // register on input event
  searchInput.oninput = (event) => {
    // remove search results if the user empties the search input field
    if (searchInput.value == '') {
      searchResults.innerHTML = '';

      searchResults.style.display = '';

      currentContent.forEach((el) => {
        el.style.display = '';
      });
    } else {
      searchResults.style.display = 'block';

      // Hide current content
      currentContent.forEach((el) => {
        el.style.display = 'none';
      });

      // get input value
      let query = event.target.value;

      // run fuzzy search
      let results = idx.search(query + '*');
      // let results = idx.search(query)

      // render results
      renderSearchResults(results);
    }
  };

  // set focus on search input and remove loading placeholder
  // searchInput.focus()
  // searchInput.placeholder = ''
}

// window.onload = function () {
//   // get dom elements
//   searchInput = document.querySelector('.explore__input');
//   searchResults = document.querySelector('.explore__results');
//   let urlJson = '/index.json';

//   // request and index documents
//   fetch(urlJson, {
//     method: 'get',
//   })
//     .then((res) => res.json())
//     .then((res) => {
//       // index document
//       idx = lunr(function () {
//         // issue with wildcard
//         // https://github.com/olivernn/lunr.js/issues/454
//         this.pipeline.remove(lunr.stemmer);
//         this.pipeline.remove(lunr.stopWordFilter);

//         this.ref('url');
//         this.field('title');
//         this.field('content');
//         this.field('summary');
//         this.field('date');
//         this.field('tags');
//         this.field('authors');

//         res.forEach(function (doc) {
//           this.add(doc);
//           documents[doc.url] = {
//             title: doc.title,
//             content: doc.content,
//             summary: doc.summary,
//             date: doc.date,
//             tags: doc.tags,
//             authors: doc.authors,
//           };
//         }, this);
//       });

//       // data is loaded, next register handler
//       registerSearchHandler();
//     })
//     .catch((err) => {
//       searchResults.innerHTML = `<p>${err}</p>`;
//     });
// };
//
//
